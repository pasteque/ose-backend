package com.ose.backend.controller.products;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.controller.api.products.ProductsController;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.XlsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.service.products.IProductsServiceOSE;

@RestController
@RequestMapping(value = ProductsController.BASE_URL)
public class ProductsControllerExtended {

    @Autowired
    private IProductsServiceOSE productsService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "id", required = false) String id, @RequestParam(value = "code", required = false) String code,
            @RequestParam(value = "reference", required = false) String reference, @RequestParam(value = "_type", required = false) String _type) {
        return new ServerResponse(productsService.getByCriteria(id, code, reference, _type));
    }

//    @RequestMapping(value = "/getCategory", method = RequestMethod.GET)
//    public ServerResponse getCategory(@RequestParam(value = "id", required = true) String id) {
//        return new ServerResponse(productsService.getByCategory(id));
//    }

    @RequestMapping(value = "/getLinks", method = RequestMethod.GET)
    public ServerResponse getLinks(@RequestParam(value = "id", required = true) String id) {
        return new ServerResponse(productsService.getLinks(id));
    }

    
    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public Object getCriteria(UsernamePasswordAuthenticationToken token, @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "category", required = false) String category, @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "composition", required = false) Boolean composition, @RequestParam(value = "_format", required = false) String _format) {
        if ((reference == null || "%".equals(reference)) && category == null) {
            reference = "";
        }
        if (_format != null && _format.equals("xls")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "products");
            model.put("content", CsvHelper.getCSVContent(productsService.getByCriteria(reference, category, limit, composition)));
            model.put("content_type", ProductsResultDTO.types);

            return new ModelAndView(new XlsResponse(), model);
        }
        List<ProductsResultDTO> products = productsService.getByCriteria(reference, category, limit, composition);
        return new ServerResponse(products);
       
    }
        

    @PreAuthorize("hasAuthority('" + Permissions.Produits.PERM_PRODUIT_EXTRACT + "')")
    @RequestMapping(value = "/getCriteria/download", method = RequestMethod.GET)
    public Object download(UsernamePasswordAuthenticationToken token, @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "category", required = false) String category, @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "composition", required = false) Boolean composition, @RequestParam(value = "_format", required = false) String _format) {
        return getCriteria(token, reference, category, limit, composition,_format);
    }
}
