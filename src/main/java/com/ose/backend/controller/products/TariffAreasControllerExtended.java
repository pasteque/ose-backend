package com.ose.backend.controller.products;

import org.pasteque.api.model.application.ServerResponse;

import java.util.Date;

import org.pasteque.api.controller.api.products.TariffAreasController;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.products.ITariffAreasServiceOSE;


@RestController
@RequestMapping(value = TariffAreasController.BASE_URL)
public class TariffAreasControllerExtended {

    public static final String BASE_URL = "/tariff-areas";

    @Autowired
    private ITariffAreasServiceOSE tariffAreasService;


    @RequestMapping(value="/getCurrentLinks", method = RequestMethod.GET)
    public ServerResponse getCurrentLinks(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "tariffAreaId", required = false) String tariffAreaId,
            @RequestParam(value = "categoryId", required = false) String categoryId,
            @RequestParam(value = "from", required = false) String from, 
            @RequestParam(value = "to", required = false) String to) {

        Date dFrom = null;
        Date dTo = null;
        if (from != null) {
            dFrom = new Date(Long.parseLong(from));
        }
        if (to != null) {
            dTo = new Date(Long.parseLong(to));
        }
        return new ServerResponse(tariffAreasService.getCurrentTariffAreasLinks(locationId, tariffAreaId , categoryId , null , dFrom , dTo , TariffAreasLocationsDTO.class));
    }



    //    @RequestMapping(value="/getCurrent", method = RequestMethod.GET)
    //    public ServerResponse getCurrent(@RequestParam(value = "locationId", required = false) String locationId,
    //            @RequestParam(value = "indexFormat", required = false) Boolean indexFormat) {
    //        if(indexFormat) {
    //            return new ServerResponse(tariffAreasService.getCurrentTariffAreas(locationId, TariffAreasIndexDTO.class));
    //        } else {
    //            return new ServerResponse(tariffAreasService.getCurrentTariffAreas(locationId, TariffAreasDTO.class));
    //        }
    //    }

    @RequestMapping(value="/saveLink", method = RequestMethod.POST)
    public ServerResponse saveLink(@RequestBody TariffAreasLocationsDTO TariffArea) {
        return new ServerResponse(tariffAreasService.saveLink(TariffArea));
    }

    @RequestMapping(value="/deleteLink", method = RequestMethod.POST)
    public ServerResponse deleteLink(@RequestBody TariffAreasLocationsDTO TariffArea){
        return new ServerResponse(tariffAreasService.deleteLink(TariffArea));
    }


}
