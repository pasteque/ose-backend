package com.ose.backend.controller.locations;

import org.pasteque.api.controller.api.locations.LocationsController;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.saleslocations.ILocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.locations.ILocationsServiceOSE;

@RestController
@RequestMapping(value = LocationsController.BASE_URL)
public class LocationsControllerExtended {

    @Autowired
    private ILocationsServiceOSE locationService;

    @Autowired
    private ILocationsService locationServiceOrig;

    /**
     * Retourne l'objet location associé à ce serveur si il ne s'agit pas du siège ( dans ce cas on force un retour à null
     * @return locationDTO ou null
     */
    @RequestMapping(value = "/getDefaultSaleLocation", method = RequestMethod.GET)
    public ServerResponse getDefaultSale() {
        POSLocationsDTO location = locationServiceOrig.getDefault() ;
        if(location != null && Locations.SIEGE_ID.equals(location.getId())) {
            location = null;
        }
        return new ServerResponse(location);
    }
    
    @RequestMapping(value = "/getSubLocations", method = RequestMethod.GET)
    public ServerResponse getSubLocations(@RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(locationService.getSubLocations(locationId));
    }
    
    @RequestMapping(value = "/getSistersLocations", method = RequestMethod.GET)
    public ServerResponse getSistersLocations(@RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(locationService.getSistersLocations(locationId));
    }

    @RequestMapping(value = "/getParentLocations", method = RequestMethod.GET)
    public ServerResponse getParentLocations(@RequestParam(value = "excludeBase", required = false) Boolean excludeBase,
            @RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "avecFermes", required = false) Boolean avecFerme,
            @RequestParam(value = "avecVirtuels", required = false) Boolean avecVirtuels) {
        return new ServerResponse(AdaptableHelper.getAdapter(locationService.findAllParentLocations(excludeBase == null ? false : excludeBase, 
                locationId, avecFerme == null ? false : avecFerme , 
                avecVirtuels == null ? false : avecVirtuels) , LocationsInfoDTO.class ));
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
        return new ServerResponse(locationService.getAll(POSLocationsDTO.class));
    }

    @RequestMapping(value = "/getAllInfo", method = RequestMethod.GET)
    public ServerResponse getAllInfo() {
        return new ServerResponse(locationService.getAll(LocationsInfoDTO.class));
    }

    @RequestMapping(value = "/getRemoteLocations", method = RequestMethod.GET)
    public ServerResponse getRemoteLocations(@RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(locationService.getRemoteLocations(locationId));
    }

    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public ServerResponse getCriteria(@RequestParam(value = "reference", required = false) String search,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "category", required = false) String categoryId,
            @RequestParam(value = "parentOnly", required = false) Boolean parentOnly) {
        return new ServerResponse(locationService.getCriteria(search , categoryId , limit , parentOnly));
    }



}
