package com.ose.backend.controller.locations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.locations.ToursDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.service.locations.IToursService;

@RestController
@RequestMapping(value = ToursController.BASE_URL)
public class ToursController {

    public static final String BASE_URL = "/tours";
    
    @Autowired
    private IToursService toursService;
    
    @RequestMapping(value="/getAll", method = RequestMethod.GET)
    public ServerResponse getAll(@RequestParam(value = "minimal", required = false) Boolean minimal) {
        if(minimal == null || !minimal) {
            return new ServerResponse(toursService.getAllTours(ToursDTO.class));
        } else {
            return new ServerResponse(toursService.getAllTours(TourIndexDTO.class));
        }
    }
    
    @RequestMapping(value="/getCurrent", method = RequestMethod.GET)
    public ServerResponse getCurrent(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "minimal", required = false) Boolean minimal) {
        if(minimal == null || !minimal) {
            return new ServerResponse(toursService.getCurrentTours(locationId, ToursDTO.class));
        } else {
            return new ServerResponse(toursService.getCurrentTours(locationId, TourIndexDTO.class));
        }
    }
    
    @RequestMapping(value="/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam(value = "tourId", required = false) String tourId ,
            @RequestParam(value = "tourName", required = false) String tourName) {
        ToursDTO tour = null;
        if(tourId != null) {
            tour = toursService.getTourById(Integer.parseInt(tourId)) ;
        }
        if(tourName != null) {
            if(tour == null) {
                tour = toursService.getTourByName(tourName);
            } else {
                if(tour.getName() != tourName) tour = null;
            }
        }
        return new ServerResponse(tour);
        
    }
    
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ServerResponse create(@RequestParam(value = "tour", required = false) String tour,
            @RequestParam(value = "tours", required = false) String tours) throws JsonParseException, JsonMappingException, IOException{
        List<ToursDTO> l = new ArrayList<ToursDTO>();
        if (tours != null) {
            l = new ObjectMapper().readValue(tours, new TypeReference<List<ToursDTO>>() {
            });
        }
        if (tour != null) {
            l.add(new ObjectMapper().readValue(tour, ToursDTO.class));
        }
        return new ServerResponse(toursService.save(l));
    }
    
    @RequestMapping(value="/saveLink", method = RequestMethod.POST)
    public ServerResponse saveLink(@RequestBody POSToursLocationsDTO tour) {
        return new ServerResponse(toursService.saveLink(tour));
    }
    
    @RequestMapping(value="/deleteLink", method = RequestMethod.POST)
    public ServerResponse deleteLink(@RequestBody POSToursLocationsDTO tour){
        return new ServerResponse(toursService.deleteLink(tour));
    }
    
    @RequestMapping(value="/getCurrentLinks", method = RequestMethod.GET)
    public ServerResponse getCurrentLinks(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "tourId", required = false) String tourId,
            @RequestParam(value = "futur", required = false) Boolean futur,
            @RequestParam(value = "from", required = false) String from, 
            @RequestParam(value = "to", required = false) String to) {
            return new ServerResponse(toursService.getCurrentToursLinks(locationId, tourId ,futur ,from ,to ,POSToursLocationsDTO.class));
    }
    
}
