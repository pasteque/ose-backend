package com.ose.backend.controller.locations;

import org.pasteque.api.controller.api.locations.InseeController;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.locations.IInseeServiceOSE;

@RestController
@RequestMapping(value = InseeController.BASE_URL)
public class InseeControllerExtended {
    
    @Autowired
    private IInseeServiceOSE inseeService;
    
    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public ServerResponse getCriteria(@RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "limit", required = false) Integer limit) {
        return new ServerResponse(inseeService.getDTOByCriteria(reference, limit));
    }
    
}
