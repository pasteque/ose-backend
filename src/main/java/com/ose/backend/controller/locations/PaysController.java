package com.ose.backend.controller.locations;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.dto.locations.PaysDTO;
import com.ose.backend.service.locations.IPaysService;

@RestController
@RequestMapping(value = PaysController.BASE_URL)
public class PaysController {
    public static final String BASE_URL = "/pays";
    
    @Autowired
    private IPaysService paysService;
    
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ServerResponse getAll() {
            return new ServerResponse(paysService.getAll(PaysDTO.class));
    }
    
    @RequestMapping(value = "/{paysId}", method = RequestMethod.GET)
    public ServerResponse get(@PathVariable(value = "paysId") Long paysId) {
        return new ServerResponse(paysService.getById(paysId, PaysDTO.class));
    }
}
