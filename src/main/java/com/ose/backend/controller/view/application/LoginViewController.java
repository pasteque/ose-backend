package com.ose.backend.controller.view.application;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = LoginViewController.LOGIN_URL)
public class LoginViewController {

    private static final String ERROR_SESSION = "error";

    private static final String LOGIN_VIEW = "/login.html";
    public static final String LOGIN_URL = "/login";
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView setupForm(@RequestParam(value = "error", required = false) String error, ModelMap model, HttpServletRequest request) {
        String referrer = request.getHeader("Referer");
        if(referrer!=null){
            request.getSession().setAttribute("url_prior_login", referrer);
        }
        ModelAndView modelAndView = new ModelAndView(LoginViewController.LOGIN_VIEW);
        if (error != null) {
            model.addAttribute(LoginViewController.ERROR_SESSION, "L'identifiant/mot de passe est incorrect.");
        }
        return modelAndView;
    }
    
}
