package com.ose.backend.controller.view.application;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/authentication")
public class AuthenticationController {

    @ResponseBody
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Object getConnectedUser(AbstractAuthenticationToken token) {
        try {
            if(token instanceof PreAuthenticatedAuthenticationToken) {
                Object[] credentials = (Object[]) token.getCredentials();
                token.setDetails(credentials[2]);
                return token.getDetails();
            }
            return token.getPrincipal();
        } catch (Exception exception) {
            return "logout";
        }
    }

    @ResponseBody
    @RequestMapping(value = "/alive", method = RequestMethod.GET)
    public Object getAlive(AbstractAuthenticationToken token) {
        if (token != null) {
            return true;
        }
        return false;
    }

}
