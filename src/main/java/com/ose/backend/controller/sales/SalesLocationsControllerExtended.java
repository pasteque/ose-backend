package com.ose.backend.controller.sales;

import org.pasteque.api.controller.api.locations.SalesLocationsController;
import org.pasteque.api.dto.locations.SalesLocationsDetailDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.locations.ISalesLocationsServiceOse;

@RestController
@RequestMapping(value = SalesLocationsController.BASE_URL)
public class SalesLocationsControllerExtended {

    @Autowired
    private ISalesLocationsServiceOse salesLocationsServiceOse;

    @RequestMapping(value = "/{salesLocationId}", method = RequestMethod.GET)
    public ServerResponse get(@PathVariable(value = "salesLocationId") Long salesLocationId) {
        return new ServerResponse(salesLocationsServiceOse.getById(salesLocationId, SalesLocationsDetailDTO.class));
    }


}
