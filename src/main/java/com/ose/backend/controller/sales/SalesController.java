package com.ose.backend.controller.sales;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.pasteque.api.response.XlsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.sales.TopXDTO;
import com.ose.backend.service.sales.ISalesService;

@RestController
@RequestMapping(value = SalesController.BASE_URL)
public class SalesController {

    public static final String BASE_URL = "/sales";

    @Autowired
    private ISalesService salesService;

    @RequestMapping(value = "/getDetails", method = RequestMethod.GET)
    public Object getDetails(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "dimension", required = false) String dimension, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "_format", required = false) String _format) {
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "detailVente");
            model.put("content", CsvHelper.getCSVContent(salesService.getSalesDetails(locationId, dimension, from, to)));
            return new ModelAndView(new CsvResponse(), model);

        }
        return new ServerResponse(salesService.getSalesDetails(locationId, dimension, from, to));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Ventes.PERM_VTE_PV_EXTRACT + "')")
    @RequestMapping(value = "/getDetails/download", method = RequestMethod.GET)
    public Object downloadDetails(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "dimension", required = false) String dimension, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "_format", required = false) String _format) {
        return getDetails(locationId, dimension, from, to, _format);
    }

    /**
     * Pour avoir une représentation globale des ventes avec comparaison N-1
     * 
     * @param locationId
     *            - Le Lieu de vente considéré
     * @param dimension
     *            - comparaison N N-1 : D pour Day - W pour Week RAF ...
     * @param from
     *            - date début
     * @param to
     *            - date fin
     * @param _format
     *            - export CSV - XLS ... RAF
     * @return
     */
    @RequestMapping(value = "/getDetailsOverview", method = RequestMethod.GET)
    public Object getDetailsOverView(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "dimension", required = false) String dimension, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "_format", required = false) String _format) {

        return new ServerResponse(salesService.getSalesDetailsOverView(locationId, dimension, from, to));
    }

    @RequestMapping(value = "/getTopX", method = RequestMethod.GET)
    public Object getTopX(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "tourId", required = false) String tourId, @RequestParam(value = "pricelistId", required = false) String pricelistId,
            @RequestParam(value = "dimension", required = false, defaultValue = "units") String dimension, @RequestParam(value = "x", required = true) int x,
            @RequestParam(value = "from", required = false) String from, @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "products", required = false) String products, @RequestParam(value = "_format", required = false) String _format)
            throws JsonParseException, JsonMappingException, IOException {
        List<ProductsResultDTO> l = new ArrayList<ProductsResultDTO>();
        List<POSCategoriesDTO> cat = new ArrayList<POSCategoriesDTO>();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String fromDateFormatted = from != null ? "-" + df.format(new Date(Long.parseLong(from))) : "";
        String toDateFormatted = to != null ? "-" + df.format(new Date(Long.parseLong(to))) : "";
        String name = "top" + x + (tourId == null ? "" : "-tournee_" + tourId) + (locationId == null ? "" : "-location_" + locationId) + fromDateFormatted
                + toDateFormatted;
        if (products != null) {
            // l = new ObjectMapper().readValue(products, new
            // TypeReference<List<ProductsResultDTO>>() {
            // });

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(products);
            for (int i = 0; i < node.size(); i++) {
                try {
                    ProductsResultDTO item = mapper.readValue(node.get(i).toString(), ProductsResultDTO.class);
                    l.add(item);
                } catch (Exception e) {
                    POSCategoriesDTO item = mapper.readValue(node.get(i).toString(), POSCategoriesDTO.class);
                    cat.add(item);
                }
            }
        }

        Map<String, Object> model = new HashMap<>();
        model.put("filename", name);
        model.put("content", CsvHelper.getCSVContent(salesService.getTopX(locationId, tourId, pricelistId, x, l, cat, dimension, from, to)));

        if (_format != null && _format.equals("csv")) {
            return new ModelAndView(new CsvResponse(), model);
        }
        if (_format != null && _format.equals("xls")) {
            model.put("content_type", TopXDTO.types);

            return new ModelAndView(new XlsResponse(), model);
        }
        return new ServerResponse(salesService.getTopX(locationId, tourId, pricelistId, x, l, cat, dimension, from, to));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Ventes.PERM_VTE_PDT_EXTRACT + "')")
    @RequestMapping(value = "/getTopX/download", method = RequestMethod.GET)
    public Object downloadTopX(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "tourId", required = false) String tourId, @RequestParam(value = "pricelistId", required = false) String pricelistId,
            @RequestParam(value = "dimension", required = false, defaultValue = "units") String dimension, @RequestParam(value = "x", required = true) int x,
            @RequestParam(value = "from", required = false) String from, @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "products", required = false) String products, @RequestParam(value = "_format", required = false) String _format)
            throws JsonParseException, JsonMappingException, IOException {
        return getTopX(locationId, tourId, pricelistId, dimension, x, from, to, products, _format);
    }

    @RequestMapping(value = "/getPaymentsInfo", method = RequestMethod.GET)
    public Object getPaymentsInfo(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "from", required = false) String from, @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "_format", required = false) String _format, @RequestParam(value = "saleLocationId", required = false) String saleLocationId,
            @RequestParam(value = "tourId", required = false) String tourId) {
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "paiementInfo");
            model.put("content", CsvHelper.getCSVContent(salesService.getPaymentsInfo(locationId, from, to, saleLocationId, tourId)));
            return new ModelAndView(new CsvResponse(), model);

        }
        return new ServerResponse(salesService.getPaymentsInfo(locationId, from, to, saleLocationId, tourId));
    }

    @RequestMapping(value = "/getDetailsPayment", method = RequestMethod.GET)
    public Object getDetailsPayment(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "payment", required = false) String payment, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "_format", required = false) String _format,
            @RequestParam(value = "saleLocationId", required = false) String saleLocationId, @RequestParam(value = "tourId", required = false) String tourId) {
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "detailsPaiement");
            model.put("content", CsvHelper.getCSVContent(salesService.getDetailsPayment(locationId, payment, from, to, saleLocationId, tourId)));
            return new ModelAndView(new CsvResponse(), model);
        }
        return new ServerResponse(salesService.getDetailsPayment(locationId, payment, from, to, saleLocationId, tourId));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Ventes.PERM_VTE_COMPTA_EXTRACT + "')")
    @RequestMapping(value = "/getDetailsPayment/download", method = RequestMethod.GET)
    public Object downloadDetailsPayment(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "payment", required = false) String payment, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to, @RequestParam(value = "_format", required = false) String _format,
            @RequestParam(value = "saleLocationId", required = false) String saleLocationId, @RequestParam(value = "tourId", required = false) String tourId) {
        return getDetailsPayment(locationId, payment, from, to, _format, saleLocationId, tourId);
    }

}
