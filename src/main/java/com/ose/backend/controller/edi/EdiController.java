package com.ose.backend.controller.edi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.utils.ExportConstraintsDTO;
import com.ose.backend.service.edi.IEdiService;

@RestController
@RequestMapping(value = EdiController.BASE_URL)
public class EdiController {

    public static final String BASE_URL = "/edi";

    @Autowired
    private IEdiService ediService;

    @Autowired
    private HttpServletRequest request;

    /**
     * 
     * @param file : une URL par exemple : ftp://CAMION00:CAM0105@192.168.42.2/test.txt 
     *          ou file:///c:/essai/test.txt 
     *          (noter le / avant c: pour accéder à un fichier local sur une machine sous windows)
     * @param content : une description du contenu attendu - voir DTO de description des contenus
     * @return filecontent +  informations
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @RequestMapping(value="/importFile", method = RequestMethod.POST)
    public ServerResponse importFile( @RequestParam(value = "file", required = false) String file ,
            @RequestParam(value = "content", required = false) String filecontent ,
            @RequestParam(value = "locationId", required = false) String locationId
            ) throws JsonParseException, JsonMappingException, IOException {

        return importFile_local(  file ,filecontent ,locationId );
    }

    /**
     * 
     * @param file : une URL par exemple : ftp://CAMION00:CAM0105@192.168.42.2/test.txt 
     *          ou file:///c:/essai/test.txt 
     *          (noter le / avant c: pour accéder à un fichier local sur une machine sous windows)
     * @param filecontent : le contenu peut être passé en direct
     * @return filecontent
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @RequestMapping(value="/exportFile", method = RequestMethod.GET)
    public ServerResponse exportSalesFile( @RequestParam(value = "file", required = true) String file ,
            @RequestParam(value = "fileContent", required = false) String filecontent
            ) throws JsonParseException, JsonMappingException, IOException {

        ExportConstraintsDTO dto = new ExportConstraintsDTO();

        //On parse les contraintes
        if(filecontent != null) {
            dto = new ObjectMapper().readValue(filecontent , ExportConstraintsDTO.class);
        }

        return new ServerResponse( ediService.exportFile(file , dto));
    }

    @RequestMapping(value = "/upload" , method = RequestMethod.POST)
    public ServerResponse upload(@RequestParam("file") MultipartFile file,
            @RequestParam(value = "locationId", required = false) String locationId) 
                    throws JsonParseException, JsonMappingException, IOException {

        if (!file.isEmpty()) {
            String uploadsDir = "/uploads/";
            String realPathtoUploads =  request.getServletContext().getRealPath(uploadsDir);
            if(! new File(realPathtoUploads).exists())
            {
                new File(realPathtoUploads).mkdir();
            }

            String orgName = file.getOriginalFilename().replaceAll("[^\\p{ASCII}]", "");;
            String filePath = realPathtoUploads + "/" + orgName;
            File dest = new File(filePath);
            file.transferTo(dest);

            if(filePath.startsWith("/")) {
                filePath = "file://".concat(filePath);
            } else {
                filePath = "file:///".concat(filePath);
            }
            //store file in storage
            //On sauve puis on importe 
            System.out.println(String.format("receive %s saved here %s", orgName , filePath));
            return importFile_local(  filePath ,null ,locationId );
        }

        return new ServerResponse(null);
    }


    @SuppressWarnings("unused")
    private ServerResponse importFile_local( String file ,
            String filecontent ,
            String locationId
            ) throws JsonParseException, JsonMappingException, IOException {

        if(file != null ) {
            //On commence par le cas d'une ressource
            URL url = new URL(file);
            if("res".equals(FilenameUtils.getExtension(url.getPath()))) {
                return new ServerResponse( ediService.importRessource(url , locationId));
            } else {

                if("role".equals(FilenameUtils.getExtension(url.getPath()))) {
                    return new ServerResponse( ediService.importRole(url , locationId));
                } else {
                    Workbook wb = null;
                    InputStream is = url.openStream();
                    try {
                        wb = WorkbookFactory.create(is);
                        return new ServerResponse( ediService.importWorkbook(wb , locationId));
                    }
                    catch(IOException ex ) {
                        //Cas d'un autre fichier
                        //ex.printStackTrace();
                        if(wb!= null) {
                            wb.close();
                        }
                        InputStreamReader isR = new InputStreamReader(is);
                        Scanner scanner = new Scanner(isR);
                        filecontent = scanner.useDelimiter("\\Z").hasNext() ? scanner.next() : "" ;
                        is.close();
                        isR.close();
                        scanner.close();
                    }
                    catch(Exception ex) {
                        ex.printStackTrace();
                        is.close();
                        if(wb!= null) {
                            wb.close();
                        }
                        filecontent = ex.getMessage();
                    }
                    finally {
                        is.close();
                        if(wb!= null) {
                            wb.close();
                        }
                    } 
                }
            }
        }
        return new ServerResponse( ediService.importFile(filecontent , locationId));
    }    

}
