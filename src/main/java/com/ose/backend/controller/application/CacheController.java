package com.ose.backend.controller.application;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = CacheController.BASE_URL)
public class CacheController {

    public static final String BASE_URL = "/cache";
    
    public static final String CLEAN_OK = "Cache was cleaned";

    @PersistenceContext
    protected EntityManager entityManager;
    
    @RequestMapping(value = "/invalidate", method = RequestMethod.GET)
    public ServerResponse invalidateCache() {
        entityManager.getEntityManagerFactory().getCache().evictAll();
        return new ServerResponse(CLEAN_OK);
    }

}
