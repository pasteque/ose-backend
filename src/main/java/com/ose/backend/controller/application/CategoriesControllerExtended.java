package com.ose.backend.controller.application;

import org.pasteque.api.controller.api.application.CategoriesController;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.products.ICategoriesServiceOse;


@RestController
@RequestMapping(value = CategoriesController.BASE_URL)
public class CategoriesControllerExtended {
    
    @Autowired
    private ICategoriesServiceOse categoriesServiceOse;
   
    
    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public Object get(@RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "limit", required = false) Integer limit) {
        return new ServerResponse(categoriesServiceOse.getByCriteria(reference, limit));
    }
}
