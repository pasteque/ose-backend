package com.ose.backend.controller.application;


import java.io.IOException;
import java.security.Principal;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ose.backend.dto.application.MessagesDTO;
import com.ose.backend.dto.application.MessagesListDTO;
import com.ose.backend.service.application.IMessagesService;

@RestController
@RequestMapping(value = "/messages")
public class MessagesController {
    
    
    @Autowired
    IMessagesService messagesService;
    
    @RequestMapping(value = "/updateMessages", method = RequestMethod.POST)
    public ServerResponse updateMessages(@RequestBody MessagesListDTO messageList) throws JsonParseException, JsonMappingException, IOException {
        
        return new ServerResponse(messagesService.updateMessages(messageList.getMessages(),messageList.getLocationId(),messageList.getProductId(),messageList.getUserId(),messageList.getTypeId()));
        
    }
    
    @RequestMapping(value = "/getMessages", method = RequestMethod.GET)
    public ServerResponse getMessages(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "productId", required = false) String productId,
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "typeId", required = false) String typeId,
            @RequestParam(value = "activeOnly", required = false) Boolean activeOnly,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        return new ServerResponse(messagesService.getMessages(locationId,productId,userId,typeId,activeOnly,from,to));
    }
    
    @RequestMapping(value = "/initMessage", method = RequestMethod.GET)
    public ServerResponse initMessage(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "productId", required = false) String productId,
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "typeId", required = true) String typeId,
            @RequestParam(value = "content", required = true) String content) {
        return new ServerResponse(messagesService.initMessage(locationId,productId,userId,typeId,content));
    }
    
    @RequestMapping(value = "/createRenflouement", method = RequestMethod.POST)
    public ServerResponse createRenflouement(@RequestBody MessagesDTO messagesDTO) {
        return new ServerResponse(messagesService.createRenflouement(messagesDTO));
    }
    @RequestMapping(value = "/updateRenflouement", method = RequestMethod.POST)
    public ServerResponse updateRenflouement(Principal principal,@RequestBody MessagesDTO messagesDTO) {
        return new ServerResponse(messagesService.updateRenflouement(principal.getName(),messagesDTO));
    }
}
