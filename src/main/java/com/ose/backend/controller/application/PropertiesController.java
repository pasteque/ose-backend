package com.ose.backend.controller.application;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.application.IPropertiesService;

@RestController
@RequestMapping(value = PropertiesController.BASE_URL)
public class PropertiesController {

    public static final String BASE_URL = "/properties";

    @Autowired
    private IPropertiesService propertiesService;

    @RequestMapping(value = "/limit/stockHistory", method = RequestMethod.GET)
    public ServerResponse getStockHistoryLimit() {
        return new ServerResponse(propertiesService.getStockHistoryLimit());
    }    
    @RequestMapping(value = "/limit/detailedExtract", method = RequestMethod.GET)
    public ServerResponse getDetailedExtractLimit() {
        return new ServerResponse(propertiesService.getDetailedExtractLimit());
    }
    @RequestMapping(value = "/supervisionInterval", method = RequestMethod.GET)
    public ServerResponse getSupervisionInterval() {
        return new ServerResponse(propertiesService.getSupervisionInterval());
    }
    @RequestMapping(value = "/productRenflouementInterval", method = RequestMethod.GET)
    public ServerResponse getProductRenflouementInterval() {
        return new ServerResponse(propertiesService.getProductRenflouementInterval());
    }

}
