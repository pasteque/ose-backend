package com.ose.backend.controller.application;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.application.IStatusService;

@RestController
@RequestMapping(value = StatusController.BASE_URL)
public class StatusController {

    public static final String BASE_URL = "/status";

    @Autowired
    private IStatusService statusService;

    @RequestMapping(value = "/general", method = RequestMethod.GET)
    public ServerResponse getGeneralStatus() {
        return new ServerResponse(statusService.getGeneralStatus());
    }

    @RequestMapping(value = "/cashes", method = RequestMethod.GET)
    public ServerResponse getCashesStatus(@RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(statusService.getCashesStatus(locationId));
    }

}
