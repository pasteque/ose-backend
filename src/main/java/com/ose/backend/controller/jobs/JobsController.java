package com.ose.backend.controller.jobs;

import java.util.Map;

import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.service.job.IJobsService;

@RestController
@RequestMapping(value = JobsController.BASE_URL)
public class JobsController {

    public static final String BASE_URL = "/jobs";

    @Autowired
    private IJobsService jobsService;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ServerResponse get(@RequestParam("id") String id) {
        return new ServerResponse(jobsService.getById(id));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ServerResponse getJob(@PathVariable("id") String id) {
        return new ServerResponse(jobsService.getById(id));
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ServerResponse saveJob(@RequestBody JobDTO dto) {
        return new ServerResponse(jobsService.save(dto));
    }
    
    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public ServerResponse getCriteria(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getByCriteria(parameters));
    }
    
    @RequestMapping(value = "/getTypes", method = RequestMethod.GET)
    public ServerResponse getTypes(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getTypes(parameters));
    }
    
    @RequestMapping(value = "/getSens", method = RequestMethod.GET)
    public ServerResponse getSens(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getSens(parameters));
    }
    
    @RequestMapping(value = "/getEtats", method = RequestMethod.GET)
    public ServerResponse getEtats(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getEtats(parameters));
    }
    
    @RequestMapping(value = "/log/get", method = RequestMethod.GET)
    public ServerResponse getLog(@RequestParam("id") String id) {
        return new ServerResponse(jobsService.getLogById(id));
    }

    @RequestMapping(value = "/log/{id}", method = RequestMethod.GET)
    public ServerResponse getLogbyId(@PathVariable("id") String id) {
        return new ServerResponse(jobsService.getLogById(id));
    }
   
    @RequestMapping(value = "/log/getCriteria", method = RequestMethod.GET)
    public ServerResponse getLogByCriteria(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getLogByCriteria(parameters));
    }
    
    @RequestMapping(value = "/log/getLevels", method = RequestMethod.GET)
    public ServerResponse getLevels(@RequestParam Map<String, Object> parameters) {
        return new ServerResponse(jobsService.getLevels(parameters));
    }
    

}
