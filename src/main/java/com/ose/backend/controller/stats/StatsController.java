package com.ose.backend.controller.stats;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.pasteque.api.response.XlsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockDiaryStatsDTO;
import com.ose.backend.dto.stocks.StockDiaryStatsInfoDTO;
import com.ose.backend.dto.stocks.StockDiaryStatsLocDTO;
import com.ose.backend.dto.utils.DateTimeAggregDTO;
import com.ose.backend.dto.utils.GraphDataDTO;
import com.ose.backend.service.stats.IStatsService;


@RestController
@RequestMapping(value = StatsController.BASE_URL)
public class StatsController {

    public static final String BASE_URL = "/stats";

    @Autowired
    private IStatsService statsService;

    @RequestMapping(value = "/stock/getList", method = RequestMethod.GET)
    public Object getList(@RequestParam(value = "locations", required = false) String locationIds,
            @RequestParam(value = "products", required = false) String products, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "detailed", required = false, defaultValue = "false") Boolean detailed,
            @RequestParam(value = "_groupBy", required = false) String _group, @RequestParam(value = "_format", required = false) String _format)
            throws JsonParseException, JsonMappingException, IOException {
        List<ProductsResultDTO> productsList = new ArrayList<ProductsResultDTO>();
        List<POSCategoriesDTO> categoryList = new ArrayList<POSCategoriesDTO>();
        List<LocationsInfoDTO> locationList = new ArrayList<LocationsInfoDTO>();
        StockDiaryStatsInfoDTO infos = new StockDiaryStatsInfoDTO();

        if (products != null && !"\"".equals(products)) {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(products);
            for (int i = 0; i < node.size(); i++) {
                try {
                    ProductsResultDTO item = mapper.readValue(node.get(i).toString(), ProductsResultDTO.class);
                    productsList.add(item);
                } catch (Exception e) {
                    POSCategoriesDTO item = mapper.readValue(node.get(i).toString(), POSCategoriesDTO.class);
                    categoryList.add(item);
                }
            }
        }
        if (locationIds != null) {
            locationList = new ObjectMapper().readValue(locationIds, new TypeReference<List<LocationsInfoDTO>>() {
            });
        }

        infos.setStartDate(new DateTimeAggregDTO(new Date(Long.parseLong(from)), _group).startDate());
        infos.setEndDate(new DateTimeAggregDTO(new Date(Long.parseLong(to)), _group).endDate());

        infos.set_group(_group);

        infos.setDetails(statsService.getStocksList(locationList, productsList, categoryList, from, to, detailed, _group));

        infos.applyCorrectionsOnDetails();
        // TODO là la valo de fin de mois est faite en fonction de PMPA
        // Et non de valo de début de période suivante
        infos.createPerLocactionAggregateFromDetails();

        infos.setLocationsGraph(new ArrayList<GraphDataDTO>());

        infos.setDetailsGraph(new ArrayList<GraphDataDTO>());

        Map<String, Object> model = new HashMap<>();
        model.put("filename", "stats-stock");
        model.put("content", CsvHelper.getCSVContent(infos.getPerLocations()));

        if (_format != null && _format.equals("csv")) {
            if (detailed) {
                model.put("content", CsvHelper.getCSVContent(infos.getDetails()));
            }
            return new ModelAndView(new CsvResponse(), model);
        }
        if (_format != null && _format.equals("xls")) {

            model.put("content_type", StockDiaryStatsLocDTO.types);

            if (detailed) {
                Map<String, Object> sheetModel = new HashMap<>();
                sheetModel.put("name", "product-stocks");
                sheetModel.put("content", CsvHelper.getCSVContent(infos.getDetails()));
                sheetModel.put("content_type", StockDiaryStatsDTO.types);
                ArrayList<Map<String, Object>> sheetList = new ArrayList<Map<String, Object>>();
                sheetList.add(sheetModel);
                model.put("nextSheets", sheetList);
            }
            return new ModelAndView(new XlsResponse(), model);
        }
        // On gagne un petit quelque chose en taille et en temps en faisant le
        // travail ici
        infos.fillLocationsGraphData();

        // On gagne un gros quelque chose en taille et en temps en faisant
        // sauter les détails par produits si ils ne sont pas nécessaires
        if (detailed) {
            infos.fillDetailsGraphData();
        } else {
            infos.setDetails(new ArrayList<StockDiaryStatsDTO>());
        }

        return new ServerResponse(infos);
    }

    @PreAuthorize("hasAuthority('" + Permissions.Stocks.PERM_STOCK_EVOL_EXTRACT + "')")
    @RequestMapping(value = "/stock/getList/download", method = RequestMethod.GET)
    public Object downloadGetList(@RequestParam(value = "locations", required = false) String locationIds,
            @RequestParam(value = "products", required = false) String products, @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to,
            @RequestParam(value = "detailed", required = false, defaultValue = "false") Boolean detailed,
            @RequestParam(value = "_groupBy", required = false) String _group, @RequestParam(value = "_format", required = false) String _format)
            throws JsonParseException, JsonMappingException, IOException {
        return getList(locationIds, products, from, to, detailed, _group, _format);
    }

    @RequestMapping(value = "/base/getAggregates", method = RequestMethod.GET)
    public Object getAggregates() {
        return new ServerResponse(statsService.getAggregates());
    }

}
