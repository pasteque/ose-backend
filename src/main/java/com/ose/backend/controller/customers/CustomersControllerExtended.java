package com.ose.backend.controller.customers;

import org.pasteque.api.controller.api.customers.CustomersController;
import org.pasteque.api.model.application.ServerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ose.backend.service.customers.ICustomersServiceOSE;

@RestController
@RequestMapping(value = CustomersController.BASE_URL)
public class CustomersControllerExtended {
    @Autowired
    private ICustomersServiceOSE customersService;

    @RequestMapping(value = "/getNextCode", method = RequestMethod.GET)
    public ServerResponse getNextCode() {
        return new ServerResponse(customersService.getNextCode());
    }

}
