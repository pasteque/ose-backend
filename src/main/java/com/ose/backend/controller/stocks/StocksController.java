package com.ose.backend.controller.stocks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.dto.stocks.StockMovementDetailDTO;
import com.ose.backend.dto.stocks.StockProductDetailDTO;
import com.ose.backend.service.stock.IStocksServiceOse;

@RestController
@RequestMapping(value = StocksController.BASE_URL)
public class StocksController {

    public static final String BASE_URL = "/stocks";

    @Autowired
    private IStocksServiceOse stocksService;

    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public Object getCriteria(@RequestParam(value = "locationId", required = true) String locationId,
            @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "from", required = false, defaultValue = "0") Integer from,
            @RequestParam(value = "nbResults", required = false, defaultValue = "-1") Integer nbResults,
            @RequestParam(value = "_format", required = false) String _format) {
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "stock");
            model.put("content", CsvHelper.getCSVContent(AdaptableHelper.getAdapter(stocksService.findListByProductAndLocation(reference , locationId, from, nbResults) , StockProductDetailDTO.class)));
            return new ModelAndView(new CsvResponse(), model);
        }
        return new ServerResponse(AdaptableHelper.getAdapter(stocksService.findListByProductAndLocation(reference , locationId, from, nbResults), StockProductDetailDTO.class));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Stocks.PERM_STOCK_EXTRACT + "')")
    @RequestMapping(value = "/getCriteria/download", method = RequestMethod.GET)
    public Object download(@RequestParam(value = "locationId", required = true) String locationId,
            @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "from", required = false, defaultValue = "0") Integer from,
            @RequestParam(value = "nbResults", required = false, defaultValue = "-1") Integer nbResults,
            @RequestParam(value = "_format", required = false) String _format) {
        return getCriteria(locationId, reference, from, nbResults, _format);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/getMovements", method = RequestMethod.GET)
    public Object getMovements(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "productId", required = false) String productId, @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "type", required = false) String type, @RequestParam(value = "datefrom", required = false) String dateFrom,
            @RequestParam(value = "dateto", required = false) String dateTo, @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "from", required = false) Long from, @RequestParam(value = "nbResults", required = false) Long nbResults,
            @RequestParam(value = "_format", required = false) String _format, @RequestParam(value = "resultType", required = false) String resultType) {
        if (resultType == null) {
            resultType = "StockMovementDetailDTO";
        }
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "stocksMovement");
            model.put("content", CsvHelper.getCSVContent((List<StockMovementDetailDTO>) stocksService.getStocksMovementsByCriteria(locationId, productId,
                    reference, type, dateFrom, dateTo, from, nbResults, sort, resultType)));
            return new ModelAndView(new CsvResponse(), model);

        }

        return new ServerResponse(
                stocksService.getStocksMovementsByCriteria(locationId, productId, reference, type, dateFrom, dateTo, from, nbResults, sort, resultType));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Stocks.PERM_STOCK_MVT_EXTRACT + "')")
    @RequestMapping(value = "/getMovements/download", method = RequestMethod.GET)
    public Object downloadMovements(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "productId", required = false) String productId, @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "type", required = false) String type, @RequestParam(value = "datefrom", required = false) String dateFrom,
            @RequestParam(value = "dateto", required = false) String dateTo, @RequestParam(value = "sort", required = false) String sort,
            @RequestParam(value = "from", required = false) Long from, @RequestParam(value = "nbResults", required = false) Long nbResults,
            @RequestParam(value = "_format", required = false) String _format, @RequestParam(value = "resultType", required = false) String resultType) {
        return getMovements(locationId, productId, reference, type, dateFrom, dateTo, sort, from, nbResults, _format, resultType);
    }

    @RequestMapping(value = "/getReasons", method = RequestMethod.GET)
    public ServerResponse getReasons(@RequestParam(value = "sens", required = false) String sens) {
        return new ServerResponse(stocksService.getReasons(sens));
    }

    @RequestMapping(value = "/initializeMovement", method = RequestMethod.GET)
    public ServerResponse initializeMovement(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "productId", required = false) String productId) {
        return new ServerResponse(stocksService.initializeMovement(locationId, productId));
    }

    @RequestMapping(value = "/saveMovement", method = RequestMethod.POST)
    public ServerResponse saveMovement(@RequestBody StockMovementDetailDTO dto) {
        return new ServerResponse(stocksService.saveMovement(dto));
    }

    @RequestMapping(value = "/getStockLevelByCriteria", method = RequestMethod.GET)
    public ServerResponse getStockLevelByCriteria(@RequestParam(value = "locations", required = false) String locations,
            @RequestParam(value = "products", required = false) String products,
            @RequestParam(value = "dateStart", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date dateStart,
            @RequestParam(value = "dateStop", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") Date dateStop)
            throws JsonParseException, JsonMappingException, IOException {
        List<ProductsResultDTO> lproducts = new ArrayList<ProductsResultDTO>();
        List<LocationsInfoDTO> llocations = new ArrayList<LocationsInfoDTO>();
        if (products != null) {
            lproducts = new ObjectMapper().readValue(products, new TypeReference<List<ProductsResultDTO>>() {
            });
        }
        if (locations != null) {
            llocations = new ObjectMapper().readValue(locations, new TypeReference<List<LocationsInfoDTO>>() {
            });
        }
        return new ServerResponse(stocksService.getStockLevelByCritria(lproducts, llocations, dateStart, dateStop));
    }

    @RequestMapping(value = "/getStockLevelTableByCriteria", method = RequestMethod.GET)
    public ServerResponse getStockLevelTableByCriteria(@RequestParam(value = "locations", required = false) String locations,
            @RequestParam(value = "products", required = false) String products, @RequestParam(value = "tariffs", required = false) String tariffs)
            throws JsonParseException, JsonMappingException, IOException {
        List<ProductsResultDTO> lproducts = new ArrayList<ProductsResultDTO>();
        List<LocationsInfoDTO> llocations = new ArrayList<LocationsInfoDTO>();
        List<TariffAreasIndexDTO> ltariffs = new ArrayList<TariffAreasIndexDTO>();
        if (products != null) {
            lproducts = new ObjectMapper().readValue(products, new TypeReference<List<ProductsResultDTO>>() {
            });
        }
        if (tariffs != null) {
            ltariffs = new ObjectMapper().readValue(tariffs, new TypeReference<List<TariffAreasIndexDTO>>() {
            });
        }
        if (locations != null) {
            llocations = new ObjectMapper().readValue(locations, new TypeReference<List<LocationsInfoDTO>>() {
            });
        }
        List<StockLevelDTO> test = stocksService.getStockLevelTableByCritria(lproducts, llocations, ltariffs);
        return new ServerResponse(test);
    }

    @RequestMapping(value = "/deleteStockLevel", method = RequestMethod.POST)
    public ServerResponse deleteStockLevel(@RequestBody StockLevelDTO dto) {
        return new ServerResponse(stocksService.deleteStockLevel(dto));
    }

    @RequestMapping(value = "/saveStockLevel", method = RequestMethod.POST)
    public ServerResponse saveStockLevel(@RequestBody StockLevelDTO dto) {
        return new ServerResponse(stocksService.saveStockLevel(dto));
    }

    @RequestMapping(value = "/findAllStockLevel", method = RequestMethod.GET)
    public ServerResponse findAllStockLevel() {
        return new ServerResponse(stocksService.findAllStockLevel());
    }
}
