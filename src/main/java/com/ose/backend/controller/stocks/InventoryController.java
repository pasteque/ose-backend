package com.ose.backend.controller.stocks;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.pasteque.api.response.XlsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ose.backend.dto.stocks.InventoryDTO;
import com.ose.backend.dto.stocks.InventoryDetailDTO;
import com.ose.backend.dto.stocks.InventoryDetailGroupedDTO;
import com.ose.backend.service.stock.IInventoryService;

@RestController
@RequestMapping(value = "/inventory")
public class InventoryController {

    @Autowired
    private IInventoryService inventoryService;

    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public ServerResponse getCriteria(@RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "from", required = false) Long from, @RequestParam(value = "to", required = false) Long to,
            @RequestParam(value = "to", required = false) Long page, @RequestParam(value = "nbResults", required = false) Long nbResults) {
        return new ServerResponse(inventoryService.getByCriteria(locationId, from, to, page, nbResults));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ServerResponse get(Principal principal, @PathVariable(value = "id") String id, @RequestParam(value = "_format", required = false) String _format) {
        return new ServerResponse(inventoryService.getById(id));
    }

    @RequestMapping(value = "/canCreateNewInventory", method = RequestMethod.GET)
    public ServerResponse canCreateNewInventory(Principal principal, @RequestParam(value = "locationId", required = true) String locationId) {
        return new ServerResponse(inventoryService.canCreateNewInventory(locationId));
    }

    @RequestMapping(value = "/razCandidates", method = RequestMethod.GET)
    public ServerResponse razCandidates(Principal principal, @RequestParam(value = "inventoryId", required = true) String inventoryId,
            @RequestParam(value = "from", required = false) Long from) {
        return new ServerResponse(inventoryService.razCandidates(principal.getName(), inventoryId, from));
    }

    @RequestMapping(value = "/initialize", method = RequestMethod.GET)
    public ServerResponse initialize(Principal principal, @RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(inventoryService.initialize(principal.getName(), locationId));
    }

    @RequestMapping(value = "/closeInventory", method = RequestMethod.GET)
    public ServerResponse closeInventory(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId) {
        return new ServerResponse(inventoryService.closeInventory(inventoryId));
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/inventoryDetails", method = RequestMethod.GET)
    public Object getInventoryDetails(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId,
            @RequestParam(value = "_format", required = false) String _format) {

        Map<String, Object> model = new HashMap<>();
        model.put("filename", "inventoryDetails");
        model.put("content", CsvHelper.getCSVContent((List<InventoryDetailDTO>) inventoryService.getInventoryDetails(inventoryId, -1)));

        if (_format != null && _format.equals("csv")) {

            return new ModelAndView(new CsvResponse(), model);

        }
        if (_format != null && _format.equals("xls")) {
            model.put("content_type", InventoryDetailDTO.types);

            return new ModelAndView(new XlsResponse(), model);
        }
        return new ServerResponse(inventoryService.getInventoryDetails(inventoryId, 20));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Stocks.PERM_STOCK_INVENTAIRE_EXTRACT + "')")
    @RequestMapping(value = "/inventoryDetails/download", method = RequestMethod.GET)
    public Object downloadInventoryDetails(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId,
            @RequestParam(value = "_format", required = false) String _format) {
        return getInventoryDetails(principal, inventoryId, _format);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/inventoryDetailsGrouped", method = RequestMethod.GET)
    public Object getInventoryDetailsGrouped(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId,
            @RequestParam(value = "_format", required = false) String _format) {
        if (_format != null && _format.equals("xls")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "inventoryDetailsGrouped");
            model.put("content", CsvHelper.getCSVContent((List<InventoryDetailGroupedDTO>) inventoryService.getInventoryDetailsGrouped(inventoryId)));
            model.put("content_type", InventoryDetailGroupedDTO.types);

            return new ModelAndView(new XlsResponse(), model);
        }
        return new ServerResponse(inventoryService.getInventoryDetailsGrouped(inventoryId));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Stocks.PERM_STOCK_INVENTAIRE_EXTRACT + "')")
    @RequestMapping(value = "/inventoryDetailsGrouped/download", method = RequestMethod.GET)
    public Object downloadInventoryDetailsGrouped(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId,
            @RequestParam(value = "_format", required = false) String _format) {
        return getInventoryDetailsGrouped(principal, inventoryId, _format);
    }

    @RequestMapping(value = "/inventoryInfo", method = RequestMethod.GET)
    public Object getinventoryInfo(Principal principal, @RequestParam(value = "inventoryId", required = false) String inventoryId) {
        return new ServerResponse(inventoryService.getInventoryInfo(inventoryId));
    }

    @RequestMapping(value = "/saveInventory", method = RequestMethod.POST)
    public ServerResponse saveInventory(@RequestBody InventoryDTO dto) {
        return new ServerResponse(inventoryService.saveInventory(dto));
    }

    @RequestMapping(value = "/saveInventoryDetails", method = RequestMethod.POST)
    public ServerResponse saveInventoryDetails(Principal principal, @RequestBody InventoryDetailDTO dto) {
        return new ServerResponse(inventoryService.saveInventoryDetails(principal.getName(), dto));
    }
}
