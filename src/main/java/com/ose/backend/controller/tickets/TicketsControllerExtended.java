package com.ose.backend.controller.tickets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.controller.api.tickets.TicketsController;
import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.CsvResponse;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.response.XlsResponse;
import org.pasteque.api.util.ComplexReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ose.backend.dto.customers.CustomerSimpleDTO;
import com.ose.backend.dto.tickets.TicketsIndexDTO;
import com.ose.backend.service.tickets.ITicketsServiceOSE;

@RestController
@RequestMapping(value = TicketsController.BASE_URL)
public class TicketsControllerExtended {

    @Autowired
    private ITicketsServiceOSE ticketsService;
    
    //EDU 2021 04 On crée une possibilité d'obtenir la liste des tickets pour un client donné
    //Il s'agit d'exploiter des données complémentaires et de fournir les informations autrefois présentes 
    //dans l'objet CustomersDetailDTO
    //On a les caractéristiques suivantes :
    //
    // on retourne une liste de TicketsHistoryDTO triés par ordre chronologique décroissant 
    // on considère tous les tickets associés à un client
    // c'est à dire ceux associés directement ( via le champ customer du ticket
    // ceux associés à postériori (via les données de ticket extended)
    // ceux associés à ses enfants

    @RequestMapping(value = "/findCustomerTickets", method = RequestMethod.GET)
    public Object findCustomerTickets(@RequestParam(value = "customerId", required = true) String customerId) {
        List<TicketsHistoryDTO> tickets = ticketsService.findCustomerTickets(customerId);
        return new ServerResponse(tickets);
    }
    
    @RequestMapping(value = "/findTicketExtendedCustomer", method = RequestMethod.GET)
    public Object findTicketExtendedCustomer(@RequestParam(value = "ticketId", required = true) String ticketId) {
        CustomerSimpleDTO cust = ticketsService.findTicketExtendedCustomer(ticketId);
        return new ServerResponse(cust);
    }
    
    @RequestMapping(value = "/updateTicketExtended", method = RequestMethod.GET)
    public Object updateTicketExtended(@RequestParam(value = "ticketId", required = true) long ticketId , 
            @RequestParam(value = "customerId", required = false) String customerId) {
        ticketsService.updateTicketExtended(ticketId , customerId);
        return new ServerResponse(true);
    }
    
    @SuppressWarnings("unchecked")
    @PreAuthorize("hasAuthority('" + Permissions.Ventes.PERM_VTE_TICKET_EXTRACT + "')")
    @RequestMapping(value = "/search/download", method = RequestMethod.GET)
    public Object download(@RequestParam(value = "ticketId", required = false) String ticketId,
            @RequestParam(value = "ticketType", required = false) String ticketType, @RequestParam(value = "cashId", required = false) String cashId,
            @RequestParam(value = "dateStart", required = false) String dateStart, @RequestParam(value = "dateStop", required = false) String dateStop,
            @RequestParam(value = "customerId", required = false) String customerId, @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "resultType", required = false) String resultType, @RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "saleLocationId", required = false) String saleLocationId,
            @RequestParam(value = "locationCategoryName", required = false) String locationCategoryName,
            @RequestParam(value = "tourId", required = false) String tourId, @RequestParam(value = "fromValue", required = false) String fromValue,
            @RequestParam(value = "toValue", required = false) String toValue,
            @RequestParam(value = "orderConstraint", required = false) Boolean orderConstraint,
            @RequestParam(value = "_format", required = false) String _format) {
        
        ComplexReturnType returnInfos = ticketsService.getTicketReturnInfos(resultType);

        List<ICsvResponse> returnListDTO = ticketsService.search(ticketId, ticketType, cashId, dateStart, dateStop, customerId, userId, locationId,
                locationCategoryName, saleLocationId, tourId, fromValue, toValue, orderConstraint, returnInfos.getMainReturnType());
        Map<String, Object> model = new HashMap<>();
        model.put("filename", returnInfos.getMainSheet());

        
        if (_format != null && _format.equals("csv")) {

            model.put("content", CsvHelper.getCSVContent(returnListDTO));

            return new ModelAndView(new CsvResponse(), model);

        }
        if (_format != null && _format.equals("xls")) {
            // List<ICsvResponse> returnList = ticketsService.search(ticketId,
            // ticketType, cashId, dateStart, dateStop, customerId, userId,
            // locationId, locationCategoryName , saleLocationId, tourId,
            // fromValue, toValue, returnInfos.getMainReturnType());

            // On vérifie que ça puisse passer (256o par cellule de manière
            // arbitraire)
            // Tant pis pour le gc qui fait au bon moment pourrait nous sauver
            // car dans ce cas on prendrait aussi le risque de lever un heap sur
            // un autre requête concurrente
            Runtime runtime = Runtime.getRuntime();
            int mb = 1024 * 1024;
            System.out.println("##### Heap utilization statistics [MB] #####");

            // Print free memory
            System.out.println("Free Memory:" + runtime.freeMemory() / mb);

            // Print total available memory
            System.out.println("Total Memory:" + runtime.totalMemory() / mb);

            // Print Maximum available memory
            System.out.println("Max Memory:" + runtime.maxMemory() / mb);

            // Print Estimation of needed memory
            System.out.println("Needed Memory:" + (256 * (returnListDTO == null ? 0 : returnListDTO.size()) * returnInfos.getMainContentType().length / mb));

            if (returnListDTO != null && (256 * returnListDTO.size()
                    * returnInfos.getMainContentType().length > (runtime.maxMemory() - runtime.totalMemory() + runtime.freeMemory()))) {
                returnListDTO = null;
            }

            List<String[]> returnList = CsvHelper.getCSVContent(returnListDTO);
            if (returnInfos.getNumberOfSheets() < 1) {
                returnListDTO = null;
            }

            model.put("content", returnList);
            model.put("content_type", returnInfos.getMainContentType());

            returnList = null;

            ArrayList<Map<String, Object>> sheetList = new ArrayList<Map<String, Object>>();

            for (int index = 0; returnListDTO != null && index < returnInfos.getNumberOfSheets(); index++) {
                ArrayList<ICsvResponse> lines = new ArrayList<ICsvResponse>();
                for (ICsvResponse ticket : returnListDTO) {
                    TicketsIndexDTO ticketDTO = (TicketsIndexDTO) ticket;
                    lines.addAll((Collection<? extends ICsvResponse>) ticketsService.searchElements(ticketDTO, returnInfos.getNextReturnTypes()[index]));
                }
                Map<String, Object> sheetModel = new HashMap<>();
                sheetModel.put("name", returnInfos.getNextSheetNames()[index]);
                sheetModel.put("content", CsvHelper.getCSVContent(lines));
                sheetModel.put("content_type", returnInfos.getNextContentType()[index]);

                sheetList.add(sheetModel);
            }
            if (sheetList.size() > 0) {
                model.put("nextSheets", sheetList);
            }

            return new ModelAndView(new XlsResponse(), model);
        }
        
        return new ServerResponse(
                returnListDTO == null ? "Un problème est survenu - Le serveur est sans doute trop sollicité ou votre requête trop lourde" : returnListDTO,
                        returnListDTO != null);
    }
}
