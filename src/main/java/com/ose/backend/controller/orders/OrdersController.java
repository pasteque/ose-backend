package com.ose.backend.controller.orders;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.pasteque.api.model.application.ServerResponse;
import org.pasteque.api.model.security.Permissions;
import org.pasteque.api.response.CsvHelper;
import org.pasteque.api.response.XlsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ose.backend.dao.helper.CriteriaConstant;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.dto.orders.OrderDetailDTO;
import com.ose.backend.dto.orders.OrderDetailIndexDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.response.TxtResponse;
import com.ose.backend.service.orders.IOrdersService;

@RestController
@RequestMapping(value = "/orders")
public class OrdersController {

    @Autowired
    private IOrdersService ordersService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Object get(Principal principal,
            @PathVariable(value = "id") String id,
            @RequestParam(value = "_format", required = false) String _format) {
        if (_format != null && _format.equals("xls")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "Commandes");
            model.put("content",
                    CsvHelper.getCSVContent(ordersService.getOrderDetails(id)));
            model.put("content_type", OrderDetailIndexDTO.types);
            return new ModelAndView(new XlsResponse(), model);
        }
        if (_format != null && _format.equals("csv")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "Ofbiz");
            model.put("content", CsvHelper
                    .getCSVContent(ordersService.getOrderDetailOfbizDTO(id)));
            return new ModelAndView(new TxtResponse(), model);
        }
        if (_format != null && _format.equals("bl")) {
            Map<String, Object> model = new HashMap<>();
            model.put("filename", "BonDeLivraison");
            model.put("content",
                    CsvHelper.getCSVContent(ordersService.getOrderDetailBLDTO(id)));
            model.put("content_type", OrderDetailBLDTO.types);
            model.put("object", new OrderDetailBLDTO());
            return new ModelAndView(new XlsResponse(), model);
        }
        return new ServerResponse(ordersService.getOrder(id));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Commandes.PERM_COMMANDE_EXTRACT
            + "')")
    @RequestMapping(value = "/{id}/download", method = RequestMethod.GET)
    public Object download(Principal principal,
            @PathVariable(value = "id") String id,
            @RequestParam(value = "_format", required = false) String _format) {
        return get(principal, id, _format);
    }
    
    @PreAuthorize("hasAuthority('" + Permissions.Commandes.PERM_COMMANDE_BON_LIVRAISON
            + "')")
    @RequestMapping(value = "/{id}/bonDeLivraison", method = RequestMethod.GET)
    public Object bonDeLivraison(Principal principal,
            @PathVariable(value = "id") String id) {
        return get(principal, id, "bl");
    }

    @RequestMapping(value = "/getAllByCriteria", method = RequestMethod.GET)
    public Object getAllByCriteria(
            @RequestParam MultiValueMap<String, Object> parameters,
            @RequestParam(value = "nbResults", required = false, defaultValue = "-1") Long nbResults,
            @RequestParam(value = "_format", required = false) String _format) {
        return new ServerResponse(ordersService.getAllByCriteria(parameters));
    }

    @RequestMapping(value = "/getCriteria", method = RequestMethod.GET)
    public Object getCriteria(
            @RequestParam(value = "locationId", required = false) String locationId,
            @RequestParam(value = "reference", required = false) String reference,
            @RequestParam(value = "from", required = false, defaultValue = "0") Long from,
            @RequestParam(value = "to", required = false, defaultValue = "0") Long to,
            @RequestParam(value = "nbResults", required = false, defaultValue = "-1") Integer nbResults,
            @RequestParam(value = "_format", required = false) String _format) {
        
        // TODO Voir si l'absence de traitement du format pose pb
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        if (locationId != null) {
            parameters.add(CriteriaConstant.LOCATION_ID, locationId);
        }
        if (reference != null) {
            parameters.add(CriteriaConstant.REFERENCE, reference);
        }
        if (from != null) {
            parameters.add(CriteriaConstant.FROM, from);
        }
        if (to != null) {
            parameters.add(CriteriaConstant.TO, to);
        }
        if (nbResults != null) {
            parameters.add(CriteriaConstant.NB_RESULTS, nbResults);
        }
        return new ServerResponse(ordersService.getAllByCriteria(parameters));
    }

    @RequestMapping(value = "/getProductsLocations", method = RequestMethod.GET)
    public Object getProductsLocations(
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(ordersService.getProductsLocations(orderId));
    }

    @RequestMapping(value = "/getProductsPaletteInfo", method = RequestMethod.GET)
    public Object getProductsLocations(
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "productId", required = true) String productId) {
        return new ServerResponse(
                ordersService.getProductsPaletteInfo(orderId, productId));
    }

    @RequestMapping(value = "/initialize", method = RequestMethod.GET)
    public ServerResponse initialize(Principal principal,
            @RequestParam(value = "locationId", required = false) String locationId) {
        return new ServerResponse(ordersService
                .initializeOrders(principal.getName(), locationId));
    }

    @RequestMapping(value = "/canValidateOrder", method = RequestMethod.GET)
    public ServerResponse canValidateOrder(
            UsernamePasswordAuthenticationToken principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.canPassOrderToNextStep(principal, orderId));
    }

    @RequestMapping(value = "/canModifyOrder", method = RequestMethod.GET)
    public ServerResponse canModifyOrder(
            UsernamePasswordAuthenticationToken principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.canModifyOrder(principal, orderId));
    }

    @RequestMapping(value = "/canDeleteOrder", method = RequestMethod.GET)
    public ServerResponse canDeleteOrder(
            UsernamePasswordAuthenticationToken principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.canDeleteOrder(principal, orderId));
    }

    @RequestMapping(value = "/canAddToOrder", method = RequestMethod.GET)
    public ServerResponse canAddToOrder(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "quantity", required = true) double quantity,
            @RequestParam(value = "productId", required = true) String productId) {
        return new ServerResponse(
                ordersService.canAddToOrder(orderId, productId, quantity));
    }

    @RequestMapping(value = "/nextOrderStep", method = RequestMethod.GET)
    public ServerResponse nextOrderStep(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.nextOrderStep(principal.getName(), orderId));
    }

    @RequestMapping(value = "/saveOrder", method = RequestMethod.POST)
    public ServerResponse saveOrder(@RequestBody OrderDTO dto) {
        return new ServerResponse(ordersService.saveOrder(dto));
    }

    @RequestMapping(value = "/saveOrderDetail", method = RequestMethod.POST)
    public ServerResponse saveOrderDetail(@RequestBody OrderDetailDTO dto) {
        return new ServerResponse(ordersService.saveOrderDetail(dto));
    }

    @RequestMapping(value = "/addProductToOrder", method = RequestMethod.GET)
    public ServerResponse addProductToOrder(
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "productId", required = true) String productId,
            @RequestParam(value = "quantity", required = true) int quantity) {
        return new ServerResponse(
                ordersService.addProductToOrder(orderId, productId, quantity));
    }

    @RequestMapping(value = "/removeProductToOrder", method = RequestMethod.GET)
    public ServerResponse removeProductToOrder(
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "productId", required = true) String productId) {
        return new ServerResponse(
                ordersService.removeProductToOrder(orderId, productId));
    }

    @PreAuthorize("hasAuthority('" + Permissions.Commandes.PERM_COMMANDE_GENERER
            + "')")
    @RequestMapping(value = "/generateOrder", method = RequestMethod.GET)
    public ServerResponse generateOrder(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(ordersService.generateOrder(orderId));
    }

    @RequestMapping(value = "/deleteOrder", method = RequestMethod.GET)
    public ServerResponse deleteOrder(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(ordersService.deleteOrder(orderId));
    }

    // Permet de générer une palette a partir d'un numéro d'emplacement et d'un
    // nombre d'emplacement
    @RequestMapping(value = "/generatePalette", method = RequestMethod.GET)
    public ServerResponse generatePalette(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "locationFrom", required = true, defaultValue = "") String locationFrom,
            @RequestParam(value = "numberOfLocation", required = true) String numberOfLocation) {
        return new ServerResponse(ordersService.generatePalette(
                principal.getName(), orderId, locationFrom, numberOfLocation));
    }

    // Permet de retourner une palette à partir de son ID
    @RequestMapping(value = "/getPalette", method = RequestMethod.GET)
    public ServerResponse getPalette(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId) {
        return new ServerResponse(ordersService.getPalette(paletteId));
    }

    // Permet de retourner la liste des palettes d'une commande
    @RequestMapping(value = "/getPalettes", method = RequestMethod.GET)
    public ServerResponse getPalettes(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(ordersService.getPalettes(orderId));
    }

    /*
     * Permet de retourner l'ensemble des informations d'avancement de la
     * préparation d'une commande
     */
    @RequestMapping(value = "/getPalettePreparationInfo", method = RequestMethod.GET)
    public ServerResponse getPalettePreparationInfo(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.getPalettePreparationInfo(orderId));
    }

    // Permet de retourner la liste des palettes en fonction de critères ==>
    // TODO:fusionner avec la précédente mais attention aux impacts éventuels
    @RequestMapping(value = "/getPalettesByCriteria", method = RequestMethod.GET)
    public ServerResponse getPalettesByCriteria(Principal principal,
            @RequestParam(value = "statusOrder", required = false) String statusOrder,
            @RequestParam(value = "statusPalette", required = false) String statusPalette,
            @RequestParam(value = "orderId", required = false) String orderId) {
        return new ServerResponse(ordersService.getPalettesByCriteria(
                principal.getName(), statusOrder, statusPalette, orderId));
    }

    // Permet d'enregistrer une palette
    @RequestMapping(value = "/savePalette", method = RequestMethod.POST)
    public ServerResponse generatePalette(Principal principal,
            @RequestBody PaletteDTO dto) {
        return new ServerResponse(ordersService.savePalette(dto));
    }

    // Permet de supprimer une palette
    @RequestMapping(value = "/deletePalette", method = RequestMethod.GET)
    public ServerResponse deletePalette(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId) {
        return new ServerResponse(ordersService.deletePalette(paletteId));
    }

    // Permet d'ajouter le reliquat d'un orderId à une palette
    @RequestMapping(value = "/addToPalette", method = RequestMethod.GET)
    public ServerResponse addToPalette(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "orderDetailId", required = true) String orderDetailId) {
        return new ServerResponse(
                ordersService.addToPalette(paletteId, orderDetailId));
    }

    // Permet d'enlever un paletteDetailId à une palette
    @RequestMapping(value = "/deletePaletteDetail", method = RequestMethod.GET)
    public ServerResponse deletePaletteDetail(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "paletteDetailId", required = true) String paletteDetailId) {
        return new ServerResponse(
                ordersService.deleteFromPalette(paletteId, paletteDetailId));
    }

    // Permet de mettre à jour les quantités d'une paletteDetail
    @RequestMapping(value = "/updatePaletteDetail", method = RequestMethod.GET)
    public ServerResponse updatePaletteDetail(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "statusPalette", required = false) String statusPalette,
            @RequestParam(value = "productId", required = true) String productId,
            @RequestParam(value = "quantity", required = false, defaultValue = "0") Double quantity,
            @RequestParam(value = "close", required = false, defaultValue = "false") boolean closePaletteDetail,
            @RequestParam(value = "inputOrigine", required = true) String inputOrigine) {
        return new ServerResponse(ordersService.updatePaletteDetail(paletteId,
                status == null ? statusPalette : status, productId, quantity,
                closePaletteDetail, principal.getName(), inputOrigine));
    }

    // Permet de mettre à jour les quantités d'une paletteDetail
    @RequestMapping(value = "/canAddToPalette", method = RequestMethod.GET)
    public ServerResponse canAddToPalette(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "statusPalette", required = false) String statusPalette,
            @RequestParam(value = "quantity", required = true) double quantity,
            @RequestParam(value = "productId", required = true) String productId) {
        return new ServerResponse(ordersService.canAddToPalette(paletteId,
                status == null ? statusPalette : status, productId, quantity));
    }

    // Permet de retourner une palette à partir de son ID
    @RequestMapping(value = "/fillPaletteQuantity", method = RequestMethod.GET)
    public ServerResponse fillPaletteQuantity(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId) {
        return new ServerResponse(ordersService.fillPaletteQuantity(paletteId));
    }

    // Permet de mettre à jour les quantités d'une paletteDetail
    @RequestMapping(value = "/nextPaletteStep", method = RequestMethod.GET)
    public ServerResponse nextPaletteStep(Principal principal,
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "status", required = false) String status,
            @RequestParam(value = "statusPalette", required = false) String statusPalette) {
        return new ServerResponse(
                ordersService.nextPaletteStep(principal.getName(), paletteId,
                        status == null ? statusPalette : status));
    }

    @RequestMapping(value = "/receivePalettes", method = RequestMethod.GET)
    public Object receivePalettes(Principal principal,
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.receivePalettes(principal.getName(), orderId));
    }

    @RequestMapping(value = "/getProductsByEmplacement", method = RequestMethod.GET)
    public ServerResponse getProductsByEmplacement(
            @RequestParam(value = "paletteId", required = true) String paletteId,
            @RequestParam(value = "emplacement", required = true) String emplacement) {
        return new ServerResponse(
                ordersService.getProductsByEmplacement(paletteId, emplacement));
    }
    
    @RequestMapping(value = "/resyncJob", method = RequestMethod.GET)
    public ServerResponse resyncJob(
            @RequestParam(value = "orderId", required = true) String orderId) {
        return new ServerResponse(
                ordersService.resyncJob(orderId));
    }
}
