package com.ose.backend.specs.tickets;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;

public class TicketsSpecifications extends GenericSpecification<Tickets>{

    private static final long serialVersionUID = -3972065559523470908L;
    
    public final class SearchCritria {
        public static final String FROM = "from";
        public static final String TO = "to";
        public static final String LOCATIONID = "locationId";
    }
    
    @Override
    protected <R> Expression<R> constructExpression (Root<Tickets> root , SearchCriteria criteria) {
        if(SearchCritria.FROM.equals(criteria.getKey()) 
                || SearchCritria.TO.equals(criteria.getKey())) {
            return root.get(Tickets.Fields.DATE);
        }
        if(SearchCritria.LOCATIONID.equals(criteria.getKey())) {
            return root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION).get(Locations.Fields.ID);
        }
        
        return super.constructExpression(root, criteria);
    }

}
