package com.ose.backend.specs.products;

import org.pasteque.api.model.products.Categories;
import org.pasteque.api.specs.GenericSpecification;
import org.springframework.data.jpa.domain.Specification;

public class CategoriesSpecification extends GenericSpecification<Categories>{

    private static final long serialVersionUID = 2458583004093371782L;

    public static Specification<Categories> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
