package com.ose.backend.specs.products;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.TariffAreasValidity;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;

public class TariffAreasValiditySpecification extends GenericSpecification<TariffAreasValidity>{

    private static final long serialVersionUID = 2160596097555604196L;

    @Override
    protected <R> Expression<R> constructExpression (Root<TariffAreasValidity> root , SearchCriteria criteria) {
        if(TariffAreasValidity.Fields.LOCATION.equals(criteria.getKey())) {
            return root.get(TariffAreasValidity.Fields.LOCATION).get(Locations.Fields.ID);
        }
        if(Locations.Fields.CATEGORY.equals(criteria.getKey())) {
            return root.get(TariffAreasValidity.Fields.LOCATION).get(Locations.Fields.CATEGORY).get(Categories.Fields.NAME);
        }
        if(TariffAreasValidity.Fields.TARIFF_AREAS.equals(criteria.getKey())) {
            return root.get(TariffAreasValidity.Fields.TARIFF_AREAS).get(TariffAreas.Fields.ID);
        }
        if(TariffAreas.Fields.ORDER.equals(criteria.getKey())) {
            return root.get(TariffAreasValidity.Fields.TARIFF_AREAS).get(TariffAreas.Fields.ORDER);
        }
        
        return super.constructExpression(root, criteria);
    }
}
