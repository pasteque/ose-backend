package com.ose.backend.specs.locations;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public class LocationsSpecification extends GenericSpecification<Locations>{

    private static final long serialVersionUID = -7806317049076635690L;

    @Override
    protected <R> Expression<R> constructExpression (Root<Locations> root , SearchCriteria criteria) {

        if(Locations.Fields.CATEGORY.equals(criteria.getKey())) {
            return  root.get(Locations.Fields.CATEGORY).get(Categories.Fields.ID);
        }        
        
        return super.constructExpression(root, criteria);
    }
    
    public static Specification<Locations> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
