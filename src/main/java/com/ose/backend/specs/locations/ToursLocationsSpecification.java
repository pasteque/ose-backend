package com.ose.backend.specs.locations;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.saleslocations.ToursLocations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public class ToursLocationsSpecification extends GenericSpecification<ToursLocations>{

    private static final long serialVersionUID = -7567596156716632705L;

    @Override
    protected <R> Expression<R> constructExpression (Root<ToursLocations> root , SearchCriteria criteria) {


        if(ToursLocations.Fields.LOCATION.equals(criteria.getKey())) {
            return root.get(ToursLocations.Fields.LOCATION).get(Locations.Fields.ID);
        }
        if(ToursLocations.Fields.TOUR.equals(criteria.getKey())) {
            return root.get(ToursLocations.Fields.TOUR).get(Tours.Fields.TOUR_ID);
        }
        
        return super.constructExpression(root, criteria);
    }
    
    public static Specification<Tours> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
