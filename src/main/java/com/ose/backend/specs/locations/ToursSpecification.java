package com.ose.backend.specs.locations;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public class ToursSpecification extends GenericSpecification<Tours>{

    private static final long serialVersionUID = -7806317049076635690L;

    @Override
    protected <R> Expression<R> constructExpression (Root<Tours> root , SearchCriteria criteria) {

        if(Tours.SearchCritria.FROM.equals(criteria.getKey())) {
            return  root.get(Tours.Fields.SALES_LOCATIONS).get(SalesLocations.Fields.START_DATE);
        }
        if(Tours.SearchCritria.TO.equals(criteria.getKey())) {
            return root.get(Tours.Fields.SALES_LOCATIONS).get(SalesLocations.Fields.END_DATE);
        }
        if(Tours.SearchCritria.LOCATION.equals(criteria.getKey())) {
            return root.get(Tours.Fields.STOCK_LOCATION).get(Locations.Fields.ID);
        }
        
        
        return super.constructExpression(root, criteria);
    }
    
    public static Specification<Tours> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
