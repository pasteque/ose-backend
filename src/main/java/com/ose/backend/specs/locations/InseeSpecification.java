package com.ose.backend.specs.locations;

import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.specs.GenericSpecification;
import org.springframework.data.jpa.domain.Specification;

public class InseeSpecification extends GenericSpecification<Insee>{

    private static final long serialVersionUID = 2458583004093371782L;

    
    public static Specification<Insee> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
