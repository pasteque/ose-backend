package com.ose.backend.specs.stocks;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.products.ProductReferences;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import com.ose.backend.model.stocks.StockCurrent;

public class StockCurrentSpecification extends GenericSpecification<StockCurrent> {

    private static final long serialVersionUID = -779500898975196268L;

    @Override
    protected <R> Expression<R> constructExpression (Root<StockCurrent> root , SearchCriteria criteria) {
        if(StockCurrent.Fields.LOCATION.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.LOCATION).get(Locations.Fields.ID);
        }
        if(StockCurrent.Fields.PRODUCT.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.PRODUCT).get(Products.Fields.ID);
        }
        if(Products.Fields.NAME.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.PRODUCT).get(Products.Fields.NAME);
        }
        if(Products.Fields.REFERENCES.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.PRODUCT).get(Products.Fields.REFERENCES).get(ProductReferences.Fields.REFERENCE);
        }
        if(Products.Fields.DELETED.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.PRODUCT).get(Products.Fields.DELETED);
        }
        if(Products.Fields.STOCKABLE.equals(criteria.getKey())) {
            return root.get(StockCurrent.Fields.PRODUCT).get(Products.Fields.STOCKABLE);
        }

        return super.constructExpression(root, criteria);
    }

    public static Specification<StockCurrent> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }

}
