package com.ose.backend.specs.stocks;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;

import com.ose.backend.model.stocks.StockDiary;

public class StockDiarySpecification extends GenericSpecification<StockDiary> {

    private static final long serialVersionUID = -779500898975196268L;
    
    @Override
    protected <R> Expression<R> constructExpression (Root<StockDiary> root , SearchCriteria criteria) {
        if(StockDiary.Fields.LOCATION.equals(criteria.getKey())) {
            return root.get(StockDiary.Fields.LOCATION).get(Locations.Fields.ID);
        }
        if(StockDiary.Fields.DESTINATION.equals(criteria.getKey())) {
            return root.get(StockDiary.Fields.DESTINATION).get(Locations.Fields.ID);
        }
        if(StockDiary.Fields.PRODUCT.equals(criteria.getKey())) {
            return root.get(StockDiary.Fields.PRODUCT).get(Products.Fields.ID);
        }
        
        return super.constructExpression(root, criteria);
    }

}
