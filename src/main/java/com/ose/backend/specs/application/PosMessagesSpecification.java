package com.ose.backend.specs.application;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public class PosMessagesSpecification extends GenericSpecification<PosMessages>{

    private static final long serialVersionUID = 2955145708574520320L;

    @Override
    protected <R> Expression<R> constructExpression (Root<PosMessages> root , SearchCriteria criteria) {

        if(PosMessages.Fields.LOCATIONS.equals(criteria.getKey())) {
            return root.get(PosMessages.Fields.LOCATIONS).get(Locations.Fields.ID);
        }
        if(PosMessages.Fields.PRODUCTS.equals(criteria.getKey())) {
            return root.get(PosMessages.Fields.PRODUCTS).get(Products.Fields.ID);
        }
        if(PosMessages.Fields.PEOPLE.equals(criteria.getKey())) {
            return root.get(PosMessages.Fields.PEOPLE).get(People.Fields.ID);
        }
        if(PosMessages.Fields.TYPE.equals(criteria.getKey())) {
            return root.get(PosMessages.Fields.TYPE).get(PosMessageType.Fields.TYPE);
        }
        
        
        return super.constructExpression(root, criteria);
    }
    
    public static Specification<PosMessages> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
}
