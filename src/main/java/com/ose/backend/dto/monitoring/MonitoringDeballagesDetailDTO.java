package com.ose.backend.dto.monitoring;


public class MonitoringDeballagesDetailDTO extends MonitoringDetailDTO{

    private String pdv;
    private String tournee;
    private String deballage;


    public MonitoringDeballagesDetailDTO(String pdv,String tournee,String type) {
        this.pdv = pdv;
        this.tournee = tournee;
        super.type = type;
    }
    public String getPdv() {
        return pdv;
    }

    public void setPdv(String pdv) {
        this.pdv = pdv;
    }
    
    public String getTournee() {
        return tournee;
    }

    public void setTournee(String tournee) {
        this.tournee = tournee;
    }

    public String getDeballage() {
        return deballage;
    }

    public void setDeballage(String deballage) {
        this.deballage = deballage;
    }

}
