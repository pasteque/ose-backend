package com.ose.backend.dto.monitoring;


public class MonitoringClosedCachesDetailDTO extends MonitoringDetailDTO{

    private String tournee;
    private String pdv;
    private String deballage;


    public MonitoringClosedCachesDetailDTO(String pdv, String tournee,String deballage,String type) {
        this.pdv = pdv;
        this.tournee = tournee;
        this.deballage = deballage;
        super.type = type;
    }

    public String getPdv() {
        return pdv;
    }

    public void setPdv(String pdv) {
        this.pdv = pdv;
    }

    public String getTournee() {
        return tournee;
    }

    public void setTournee(String tournee) {
        this.tournee = tournee;
    }

    public String getDeballage() {
        return deballage;
    }

    public void setDeballage(String deballage) {
        this.deballage = deballage;
    }

}
