package com.ose.backend.dto.monitoring;

public class MonitoringMagasinsDetailDTO extends MonitoringDetailDTO{

    private String magasin;
    private String lastTicket;
    
    public MonitoringMagasinsDetailDTO() {
    }

    public MonitoringMagasinsDetailDTO(String magasin, String lastTicket, String type) {
        this.magasin = magasin;
        this.lastTicket = lastTicket;
        super.type = type;
    }


    public String getMagasin() {
        return magasin;
    }

    public void setMagasin(String magasin) {
        this.magasin = magasin;
    }

    public String getLastTicket() {
        return lastTicket;
    }

    public void setLastTicket(String lastTicket) {
        this.lastTicket = lastTicket;
    }
   
}
