package com.ose.backend.dto.monitoring;


public class MonitoringOrdersWithoutCostumersDTO extends MonitoringDetailDTO{

    private String pdv;
    private String tournee;
    private String ticket;


    public MonitoringOrdersWithoutCostumersDTO(String pdv,String tournee,String ticket,String type) {
        this.pdv = pdv;
        this.tournee = tournee;
        this.ticket = ticket;
        super.type = type;
    }
    public String getPdv() {
        return pdv;
    }

    public void setPdv(String pdv) {
        this.pdv = pdv;
    }
    
    public String getTournee() {
        return tournee;
    }

    public void setTournee(String tournee) {
        this.tournee = tournee;
    }
    public String getTicket() {
        return ticket;
    }
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

}
