package com.ose.backend.dto.monitoring;


public class MonitoringOrderNotReceivedDetailDTO extends MonitoringDetailDTO{

    private String pdv;
    private String origine;
    private String dateLastDelivery;
    
    
    public MonitoringOrderNotReceivedDetailDTO() {
    }

    public MonitoringOrderNotReceivedDetailDTO(String pdv, String origine, String dateLastDelivery,String type) {
        this.pdv = pdv;
        this.origine = origine;
        this.dateLastDelivery = dateLastDelivery;
        super.type = type;
    }
  
	public String getPdv() {
		return pdv;
	}

	public void setPdv(String pdv) {
		this.pdv = pdv;
	}

	public String getOrigine() {
		return origine;
	}

	public void setOrigine(String origine) {
		this.origine = origine;
	}

        public String getDateLastDelivery() {
            return dateLastDelivery;
        }
    
        public void setDateLastDelivery(String dateLastDelivery) {
            this.dateLastDelivery = dateLastDelivery;
        }
	
}
