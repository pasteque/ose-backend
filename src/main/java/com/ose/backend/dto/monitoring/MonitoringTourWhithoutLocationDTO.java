package com.ose.backend.dto.monitoring;

public class MonitoringTourWhithoutLocationDTO extends MonitoringDetailDTO { 
    
    private String tournee;
    
    
    public MonitoringTourWhithoutLocationDTO(String tournee ,String type) {
        this.tournee = tournee;
      super.type = type;
    }

    public String getTournee() {
        return tournee;
    }
    public void setTournee(String tournee) {
        this.tournee = tournee;
    }
    
}
