package com.ose.backend.dto.monitoring;

import java.util.ArrayList;
import java.util.List;

public class MonitoringDetailDTO  implements Comparable<MonitoringDetailDTO> {

    protected String type;
    
    private Long orderBy;
    
    private List<String> erreurList = new ArrayList<String>();
    private List<String> alerteList = new ArrayList<String>();

    public class Type {
        public static final String VENTES_MAGASINS = "Données de ventes Magasins";
        public static final String VENTES_CAMIONS = "Données de ventes Camions";
        public static final String CATALOGUE = "Catalogue en cours";
        public static final String DEBALLAGES = "Liste des déballages";
        public static final String DEBALLAGES_NON_CLOTURES = "Déballages non cloturés";
        public static final String COMMANDES_SANS_CLIENT = "Commandes sans client";
        public static final String COMMANDES_NON_RECEPTIONNEES = "Commandes non réceptionnées";
        public static final String AFFECTATIONS_MULTIPLES_POINT_VENTE = "Locations affectées à plusieurs Tournées";
        public static final String TOURS_WITHOUT_LOCATION = "Tournées affectées à plusieurs locations"; // --> WTF ???

        private Type() {
        }
    }

    public MonitoringDetailDTO() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void addErreur(String erreur) {
        getErreurList().add(erreur);
    }

    public void addAlerte(String alerte) {
        getAlerteList().add(alerte);
    }

    public List<String> getErreurList() {
        return erreurList;
    }

    public void setErreurList(List<String> erreurList) {
        this.erreurList = erreurList;
    }

    public List<String> getAlerteList() {
        return alerteList;
    }

    public void setAlerteList(List<String> alerteList) {
        this.alerteList = alerteList;
    }

    public Long getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Long orderBy) {
        this.orderBy = orderBy;
    }

    @Override
    public int compareTo(MonitoringDetailDTO o) {
        return this.getOrderBy().compareTo(o.getOrderBy());
    }

}
