package com.ose.backend.dto.monitoring;

public class MonitoringCamionsDetailDTO extends MonitoringDetailDTO {

    private String camion;
    private String tournee;
    private String lastTicket;
   
    public MonitoringCamionsDetailDTO() {
    }

    public MonitoringCamionsDetailDTO(String camion, String tournee,String lastTicket, String type) {
        this.camion = camion;
        this.tournee = tournee;
        this.lastTicket = lastTicket;
        this.type = type;
    }

    public String getCamion() {
        return camion;
    }

    public void setCamion(String camion) {
        this.camion = camion;
    }

    public String getTournee() {
        return tournee;
    }

    public void setTournee(String tournee) {
        this.tournee = tournee;
    }

    public String getLastTicket() {
        return lastTicket;
    }

    public void setLastTicket(String lastTicket) {
        this.lastTicket = lastTicket;
    }

    public void addErreur(String erreur) {
        getErreurList().add(erreur);
    }

    public void addAlerte(String alerte) {
        getAlerteList().add(alerte);
    }

}
