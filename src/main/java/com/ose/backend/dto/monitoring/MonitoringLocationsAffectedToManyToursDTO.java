package com.ose.backend.dto.monitoring;

public class MonitoringLocationsAffectedToManyToursDTO extends MonitoringDetailDTO { 
    
    private String location;
    private String tournee;
    private String dateDebut;
    private String dateFin;
    
    public MonitoringLocationsAffectedToManyToursDTO(String type) {
      super.type = type;
    }
    
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTournee() {
        return tournee;
    }
    public void setTournee(String tournee) {
        this.tournee = tournee;
    }
    public String getDateDebut() {
        return dateDebut;
    }
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }
    public String getDateFin() {
        return dateFin;
    }
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }
    
}
