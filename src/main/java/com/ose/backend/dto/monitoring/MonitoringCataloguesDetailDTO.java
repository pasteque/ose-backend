package com.ose.backend.dto.monitoring;


public class MonitoringCataloguesDetailDTO extends MonitoringDetailDTO{

    private String pdv;
    private String origine;
    private String currentCatalogue;
    private String dateFinCurrentCatalogue;
    private String nextCatalogue;
    private String dateDebutNextCatalogue;
    
    public MonitoringCataloguesDetailDTO() {
    }

    public MonitoringCataloguesDetailDTO(String pdv, String origine, String currentCatalogue,String dateFinCurrentCatalogue,String nextCatalogue,String dateDebutNextCatalogue,String type) {
        this.pdv = pdv;
        this.origine = origine;
        this.currentCatalogue = currentCatalogue;
        this.dateFinCurrentCatalogue = dateFinCurrentCatalogue;
        this.nextCatalogue = nextCatalogue;
        this.dateDebutNextCatalogue = dateDebutNextCatalogue;
        super.type = type;
    }
  
	public String getPdv() {
		return pdv;
	}

	public void setPdv(String pdv) {
		this.pdv = pdv;
	}

	public String getOrigine() {
		return origine;
	}

	public void setOrigine(String origine) {
		this.origine = origine;
	}

	public String getCurrentCatalogue() {
		return currentCatalogue;
	}

	public void setCurrentCatalogue(String currentCatalogue) {
		this.currentCatalogue = currentCatalogue;
	}

	public String getDateFinCurrentCatalogue() {
		return dateFinCurrentCatalogue;
	}

	public void setDateFinCurrentCatalogue(String dateFinCurrentCatalogue) {
		this.dateFinCurrentCatalogue = dateFinCurrentCatalogue;
	}

	public String getNextCatalogue() {
		return nextCatalogue;
	}

	public void setNextCatalogue(String nextCatalogue) {
		this.nextCatalogue = nextCatalogue;
	}

        public String getDateDebutNextCatalogue() {
            return dateDebutNextCatalogue;
        }
    
        public void setDateDebutNextCatalogue(String dateDebutNextCatalogue) {
            this.dateDebutNextCatalogue = dateDebutNextCatalogue;
        }
	
}
