package  com.ose.backend.dto.monitoring;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.pasteque.api.util.DateHelper;

public class MonitoringDTO {

    private Date date;
    private boolean hasErreurs = false;
    private boolean hasAlertes = false;
    private List<MonitoringDetailDTO> details = new ArrayList<MonitoringDetailDTO>();
    private String titre;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<MonitoringDetailDTO> getDetails() {
        return details;
    }

    public void setDetails(List<MonitoringDetailDTO> details) {
        this.details = details;
    }

    public void addDetail(MonitoringDetailDTO detail) {
        setHasErreurs(isHasErreurs() || (detail!=null && !detail.getErreurList().isEmpty()));
        setHasAlertes(isHasAlertes() || (detail!=null && !detail.getAlerteList().isEmpty()));
        details.add(detail);
    }

    public boolean isHasErreurs() {
        return hasErreurs;
    }

    public void setHasErreurs(boolean hasErreurs) {
        this.hasErreurs = hasErreurs;
    }

    public boolean isHasAlertes() {
        return hasAlertes;
    }

    public void setHasAlertes(boolean hasAlertes) {
        this.hasAlertes = hasAlertes;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public LinkedHashMap<String, String> getHTMLMessageByType() {
        String content = new String();
        LinkedHashMap<String, String> monitoringTypeContent = new LinkedHashMap<String, String>();
        String colorErreur ;
        String colorAlert ;
        List<MonitoringDetailDTO> lstMonitoringDetailDTO;
        MonitoringMagasinsDetailDTO  monitoringMagasinsDetailDTO = null;
        MonitoringCamionsDetailDTO  monitoringCamionsDetailDTO = null;
        MonitoringCataloguesDetailDTO  monitoringCataloguesDetailDTO = null;
        MonitoringDeballagesDetailDTO  monitoringDeballagesDetailDTO = null;
        MonitoringClosedCachesDetailDTO  monitoringClosedCachesDetailDTO = null;
        MonitoringOrdersWithoutCostumersDTO monitoringOrdersWithoutCostumersDTO =null;
        MonitoringLocationsAffectedToManyToursDTO monitoringLocationsAffectedToManyToursDTO = null;
        MonitoringTourWhithoutLocationDTO monitoringTourWhithoutLocationDTO = null;
        MonitoringOrderNotReceivedDetailDTO monitoringOrderNotReceivedDetailDTO = null;

        setTitre(String.format("<h2>Rapport d'intégration des flux Back Office Central du %s </h2>", DateHelper.format(date)));

        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.VENTES_MAGASINS);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content += "<br/><br/>";
            content += "<h3>Remontée de données des Magasins </h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Magasin</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Dernier ticket</th> ";
            content += "<th style='border: 1px solid black; width: 35%'>Erreurs</th> ";
            content += "<th style='border: 1px solid black; width: 35%'>Alerte préparation</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {

                if  (monitoringDetailDTO instanceof  MonitoringMagasinsDetailDTO) {
                    monitoringMagasinsDetailDTO =(MonitoringMagasinsDetailDTO)monitoringDetailDTO;

                    colorErreur = "";
                    colorAlert ="";
                    if (!monitoringMagasinsDetailDTO.getErreurList().isEmpty()) {
                        colorErreur = ";background-color: red";
                    }
                    if (!monitoringMagasinsDetailDTO.getAlerteList().isEmpty()) {
                        colorAlert = ";background-color: orange";
                    }
                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> <td style='border: 1px solid black'> %s </td>",
                            monitoringMagasinsDetailDTO.getMagasin(), monitoringMagasinsDetailDTO.getLastTicket());
                    content += String.format("<td style='border: 1px solid black"+colorErreur+"'> %s </td>", String.join("<br>", monitoringMagasinsDetailDTO.getErreurList()));
                    content += String.format("<td style='border: 1px solid black"+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringMagasinsDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.VENTES_MAGASINS, content);

        }
        // Camion
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.VENTES_CAMIONS);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Remontée de données des Camions</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 10%'>Tournée</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Camion</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Dernier ticket</th> ";
            content += "<th style='border: 1px solid black; width: 30%'>Erreurs</th> ";
            content += "<th style='border: 1px solid black; width: 30%'>Alerte préparation</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringCamionsDetailDTO) {
                    monitoringCamionsDetailDTO =(MonitoringCamionsDetailDTO)monitoringDetailDTO;

                    colorErreur = "";
                    colorAlert ="";
                    if (!monitoringCamionsDetailDTO.getErreurList().isEmpty()) {
                        colorErreur = ";background-color: red";
                    }
                    if (!monitoringCamionsDetailDTO.getAlerteList().isEmpty()) {
                        colorAlert = ";background-color: orange";
                    }
                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> <td style='border: 1px solid black'> %s </td> <td style='border: 1px solid black'> %s </td>",
                            monitoringCamionsDetailDTO.getTournee(),monitoringCamionsDetailDTO.getCamion(), monitoringCamionsDetailDTO.getLastTicket());
                    content += String.format("<td style='border: 1px solid black"+colorErreur+"'> %s </td>", String.join("<br>", monitoringCamionsDetailDTO.getErreurList()));
                    content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",
                            String.join("<br>", monitoringCamionsDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.VENTES_CAMIONS, content);
        }
        // Catalogue
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.CATALOGUE);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Catalogues en cours</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Point de vente</th> ";
            content += "<th style='border: 1px solid black; width: 10%'>Type</th> ";
            content += "<th style='border: 1px solid black; width: 28%' colspan=2>Catalogue en cours</th> ";
            content += "<th style='border: 1px solid black; width: 28%' colspan=2>Prochain catalogue</th> ";
            content += "<th style='border: 1px solid black; width: 19%' >Alertes</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringCataloguesDetailDTO) {
                    monitoringCataloguesDetailDTO =(MonitoringCataloguesDetailDTO)monitoringDetailDTO;

                    colorErreur = "";
                    colorAlert ="";
                    if (!monitoringCataloguesDetailDTO.getErreurList().isEmpty()) {
                        colorErreur = ";background-color: red";
                    }
                    if (!monitoringCataloguesDetailDTO.getAlerteList().isEmpty()) {
                        colorAlert = ";background-color: orange";
                    }
                    content += String.format("<tr> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black"+colorErreur+" '> %s </td> "
                            + "<td style='border: 1px solid black"+colorErreur+" '> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td>  "
                            + "<td style='border: 1px solid black'> %s </td>",
                            monitoringCataloguesDetailDTO.getPdv(), monitoringCataloguesDetailDTO.getOrigine(),monitoringCataloguesDetailDTO.getCurrentCatalogue(),
                            monitoringCataloguesDetailDTO.getDateFinCurrentCatalogue(),monitoringCataloguesDetailDTO.getNextCatalogue(),monitoringCataloguesDetailDTO.getDateDebutNextCatalogue());
                    // content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringCataloguesDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.CATALOGUE, content);

        }

        // deballages

        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.DEBALLAGES);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des déballages</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Tournée</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Camion</th> ";
            content += "<th style='border: 1px solid black; width: 45%'>Deballage</th> ";
            content += "<th style='border: 1px solid black; width: 25%'>Alerte</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringDeballagesDetailDTO) {
                    monitoringDeballagesDetailDTO =(MonitoringDeballagesDetailDTO)monitoringDetailDTO;

                    colorAlert ="";
                    if (!monitoringDeballagesDetailDTO.getAlerteList().isEmpty()) {
                        colorAlert = ";background-color: orange";
                    }
                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> <td style='border: 1px solid black'> %s </td> <td style='border: 1px solid black'> %s </td>",
                            monitoringDeballagesDetailDTO.getTournee(), monitoringDeballagesDetailDTO.getPdv(),monitoringDeballagesDetailDTO.getDeballage());
                    //content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringDeballagesDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.DEBALLAGES, content);
        }

        // deballages non cloturé
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.DEBALLAGES_NON_CLOTURES);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des déballages non clôturés</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Tournée</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Camion</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Deballage</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO :lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringClosedCachesDetailDTO) {
                    monitoringClosedCachesDetailDTO =(MonitoringClosedCachesDetailDTO)monitoringDetailDTO;


                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black; background-color: red'> %s </td> </tr>",
                            monitoringClosedCachesDetailDTO.getTournee(), monitoringClosedCachesDetailDTO.getPdv(),monitoringClosedCachesDetailDTO.getDeballage());
                    //content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    //content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.DEBALLAGES_NON_CLOTURES, content);
        }

        // Ticket de commande sans client
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.COMMANDES_SANS_CLIENT);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des commandes sans client</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Tournée</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Camion</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Ticket</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringOrdersWithoutCostumersDTO) {
                    monitoringOrdersWithoutCostumersDTO =(MonitoringOrdersWithoutCostumersDTO)monitoringDetailDTO;


                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black; background-color: red'> %s </td> </tr>",
                            monitoringOrdersWithoutCostumersDTO.getTournee(), monitoringOrdersWithoutCostumersDTO.getPdv(),monitoringOrdersWithoutCostumersDTO.getTicket());
                    // content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    // content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.COMMANDES_SANS_CLIENT, content);

        }

        // Locations affectés à plusieurs tournées
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des camions affectés à plusieurs tournées </h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Tournée</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Camion</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Date debut</th> ";
            content += "<th style='border: 1px solid black; width: 15%'>Date Fin</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringLocationsAffectedToManyToursDTO) {
                    monitoringLocationsAffectedToManyToursDTO =(MonitoringLocationsAffectedToManyToursDTO)monitoringDetailDTO;


                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black; background-color: red'> %s </td>"
                            + "<td style='border: 1px solid black; background-color: red'> %s </td> </tr>",
                            monitoringLocationsAffectedToManyToursDTO.getLocation(), monitoringLocationsAffectedToManyToursDTO.getTournee(),
                            monitoringLocationsAffectedToManyToursDTO.getDateDebut(),monitoringLocationsAffectedToManyToursDTO.getDateFin());
                    // content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    // content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE, content);

        }

        // Tournées sans location
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.TOURS_WITHOUT_LOCATION);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des tournées affectées à aucun camions </h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Tournée</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringTourWhithoutLocationDTO) {
                    monitoringTourWhithoutLocationDTO =(MonitoringTourWhithoutLocationDTO)monitoringDetailDTO;


                    content += String.format("<tr> <td style='border: 1px solid black'> %s </td> </tr>",
                            monitoringTourWhithoutLocationDTO.getTournee());
                    // content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    // content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.TOURS_WITHOUT_LOCATION, content);

        }

        // Commandes non receptionnées
        lstMonitoringDetailDTO = getDetailsByType(MonitoringDetailDTO.Type.COMMANDES_NON_RECEPTIONNEES);
        if (lstMonitoringDetailDTO != null && !lstMonitoringDetailDTO.isEmpty()) {
            content = "<br/><br/>";
            content += "<h3>Liste des commandes non réceptionnées</h3>";
            content += "<table style='border: 1px solid black; border-collapse: collapse; width: 100%'>";
            content += "<tr> ";
            content += "<th style='border: 1px solid black; width: 15%'>Point de vente</th> ";
            content += "<th style='border: 1px solid black; width: 10%'>Type</th> ";
            content += "<th style='border: 1px solid black; width: 28%'>Date dernière livraison</th> ";
            content += "<th style='border: 1px solid black; width: 19%' >Alertes</th> ";
            content += "</tr> ";
            for (MonitoringDetailDTO monitoringDetailDTO : lstMonitoringDetailDTO) {
                if  (monitoringDetailDTO instanceof  MonitoringOrderNotReceivedDetailDTO) {
                    monitoringOrderNotReceivedDetailDTO =(MonitoringOrderNotReceivedDetailDTO)monitoringDetailDTO;

                    colorErreur = "";
                    colorAlert ="";
                    if (!monitoringOrderNotReceivedDetailDTO.getErreurList().isEmpty()) {
                        colorErreur = ";background-color: red";
                    }
                    if (!monitoringOrderNotReceivedDetailDTO.getAlerteList().isEmpty()) {
                        colorAlert = ";background-color: orange";
                    }
                    content += String.format("<tr> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td> "
                            + "<td style='border: 1px solid black'> %s </td>",
                            monitoringOrderNotReceivedDetailDTO.getPdv(), monitoringOrderNotReceivedDetailDTO.getOrigine(),monitoringOrderNotReceivedDetailDTO.getDateLastDelivery());
                    // content += String.format("<td style='border: 1px solid black; color: tomato'> %s </td>", String.join("<br>", monitoringDetailDTO.getErreurList()));
                    content += String.format("<td style='border: 1px solid black "+colorAlert+"'> %s </td> </tr> ",String.join("<br>", monitoringOrderNotReceivedDetailDTO.getAlerteList()));
                }
            }
            content += "</table>";
            monitoringTypeContent.put(MonitoringDetailDTO.Type.COMMANDES_NON_RECEPTIONNEES, content);

        }
        return monitoringTypeContent;
    }


    private List<MonitoringDetailDTO> getDetailsByType(String type) {
        return this.details.stream().filter(detail -> type.equals(detail.getType())).collect(Collectors.toList());

    }

}
