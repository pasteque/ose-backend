package com.ose.backend.dto.products;

public class ProductsCompoundDetailDTO {

    private String compoundId;
    private String compoundCode;
    private String compoundLabel;
    private double quantity;

    public String getCompoundId() {
        return compoundId;
    }

    public void setCompoundId(String compoundId) {
        this.compoundId = compoundId;
    }

    public String getCompoundCode() {
        return compoundCode;
    }

    public void setCompoundCode(String compoundCode) {
        this.compoundCode = compoundCode;
    }

    public String getCompoundLabel() {
        return compoundLabel;
    }

    public void setCompoundLabel(String compoundLabel) {
        this.compoundLabel = compoundLabel;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

}
