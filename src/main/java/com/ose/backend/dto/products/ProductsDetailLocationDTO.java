package com.ose.backend.dto.products;

import java.util.List;

import org.pasteque.api.dto.locations.LocationsInfoDTO;

public class ProductsDetailLocationDTO {


    private Double stockUnits;

    private Double priceBuy;
    private Double priceSell;
    private Double priceSellTtc;

    private Double sell7D;
    private Double sell30D;
    private Double sellsPerDay;
    private Double totSell7D;
    private Double totSell30D;

    private Double coverageDays;
    private String stockoutDate;
    
    private Boolean active;

    private List<String> taxes;
    
    private LocationsInfoDTO salesLocation;

    public LocationsInfoDTO getSalesLocation() {
        return salesLocation;
    }

    public void setSalesLocation(LocationsInfoDTO salesLocation) {
        this.salesLocation = salesLocation;
    }

    public Double getStockUnits() {
        return stockUnits;
    }

    public void setStockUnits(Double stockUnits) {
        this.stockUnits = stockUnits;
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy;
    }

    public Double getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(Double priceSell) {
        this.priceSell = priceSell;
    }

    public Double getPriceSellTtc() {
        return priceSellTtc;
    }

    public void setPriceSellTtc(Double priceSellTtc) {
        this.priceSellTtc = priceSellTtc;
    }

    public List<String> getTaxes() {
        return taxes;
    }

    public void setTaxes(List<String> taxes) {
        this.taxes = taxes;
    }

    public Double getSell7D() {
        return sell7D;
    }

    public void setSell7D(Double sell7d) {
        sell7D = sell7d;
    }

    public Double getSell30D() {
        return sell30D;
    }

    public void setSell30D(Double sell30d) {
        sell30D = sell30d;
    }

    public Double getSellsPerDay() {
        return sellsPerDay;
    }

    public void setSellsPerDay(Double sellsPerDay) {
        this.sellsPerDay = sellsPerDay;
    }

    public Double getCoverageDays() {
        return coverageDays;
    }

    public void setCoverageDays(Double coverageDays) {
        this.coverageDays = coverageDays;
    }

    public String getStockoutDate() {
        return stockoutDate;
    }

    public void setStockoutDate(String stockoutDate) {
        this.stockoutDate = stockoutDate;
    }

    public Double getTotSell30D() {
        return totSell30D;
    }

    public void setTotSell30D(Double totSell30D) {
        this.totSell30D = totSell30D;
    }

    public Double getTotSell7D() {
        return totSell7D;
    }

    public void setTotSell7D(Double totSell7D) {
        this.totSell7D = totSell7D;
    }

    public boolean isActive() {
        return active == null ? false : active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}
