package com.ose.backend.dto.products;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.response.IXlsResponse;

public class ProductsResultDTO implements IXlsResponse{
    
    public static enum PricesChange {
        BAISSERECENTE(-2), HAUSSERECENTE(-1), RAS(0), HAUSSEPROCHAINE(1), BAISSEPROCHAINE(2);
        int value;
        private PricesChange(int value) {
            this.value = value;
        }
    }
    
    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING ,  CellType.NUMERIC , CellType.STRING };

    private String id;
    private String barcode;
    private String label;
    private String categoryLabel;
    private String priceSell;
    
    private String stockLocation;
    
    //Quelques données complémentaires 
    //Est-il dans un Tariffaire actif
    private Boolean active=true;
    //Est-ce un produit composé
    private Boolean composed=false;
    //Le PV a-t-il changé récemment ou va-t-il changer prochainement + hausse + baisse
    private PricesChange priceChange=PricesChange.RAS;
    //Prix maximum constaté actuellement - sauf si Loc Id ou tous le même prix
    private String maxPriceSell;
    //PMPA actuel
    private String currentPMPA;

    private String alertePreparation;
     
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getPriceSell() {
        return priceSell;
    }

    public void setPriceSell(String priceSell) {
        this.priceSell = priceSell;
    }
    
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getComposed() {
        return composed;
    }

    public void setComposed(Boolean composed) {
        this.composed = composed;
    }

    public PricesChange getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(PricesChange priceChange) {
        this.priceChange = priceChange;
    }

    public String getMaxPriceSell() {
        return maxPriceSell;
    }

    public void setMaxPriceSell(String maxPriceSell) {
        this.maxPriceSell = maxPriceSell;
    }
    
    public String getCurrentPMPA() {
        return currentPMPA;
    }

    public void setCurrentPMPA(String currentPMPA) {
        this.currentPMPA = currentPMPA;
    }

    //TODO adapter l'export
    @Override
    public String[] header() {
        String[] retour;
        if(maxPriceSell!=null) {
            retour = new String[] { "Code", "Nom", "Cat" , "PV - TTC" , "Actif" };
        } else {
            retour = new String[] { "Code", "Nom", "Cat" , "PV - TTC" , "Actif" };
        }
        return retour;
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] {getId(), getLabel(), getCategoryLabel(), decf.format(Double.parseDouble(getPriceSell())) , getActive() ? "Vrai" : "Faux"};
    }

    public String getStockLocation() {
        return stockLocation;
    }

    public void setStockLocation(String stockLocation) {
        this.stockLocation = stockLocation;
    }
    public String getAlertePreparation() {
        return alertePreparation;
    }
    public void setAlertePreparation(String alertePreparation) {
        this.alertePreparation = alertePreparation;
    }
    
    @Override
    public boolean equals(Object o){
        if(o == null)                return false;
        if(!ProductsResultDTO.class.isInstance(o)) return false;

        ProductsResultDTO other = (ProductsResultDTO) o;
        return this.getId().equals(other.getId());
      }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
