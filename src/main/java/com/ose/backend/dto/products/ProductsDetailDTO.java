package com.ose.backend.dto.products;

import java.util.List;

import com.ose.backend.dto.application.MessagesInfoDTO;


public class ProductsDetailDTO {

    private String code;
    private String label;
    private String categoryId;
    private String categoryLabel;

    private List<String> barcodes;
    private List<MessagesInfoDTO> messages;

    private List<ProductsDetailLocationDTO> details;
    
    private boolean hasComponents;
    private boolean stockable;
    private List<ProductsCompoundDetailDTO> compounds;
    private boolean active;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public List<String> getBarcodes() {
        return barcodes;
    }

    public void setBarcodes(List<String> barcodes) {
        this.barcodes = barcodes;
    }

    public List<MessagesInfoDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessagesInfoDTO> messages) {
        this.messages = messages;
    }

    public List<ProductsDetailLocationDTO> getDetails() {
        return details;
    }

    public void setDetails(List<ProductsDetailLocationDTO> details) {
        this.details = details;
    }

    public List<ProductsCompoundDetailDTO> getCompounds() {
        return compounds;
    }

    public void setCompounds(List<ProductsCompoundDetailDTO> compounds) {
        this.compounds = compounds;
    }

    public boolean isHasComponents() {
        return hasComponents;
    }

    public void setHasComponents(boolean hasComponents) {
        this.hasComponents = hasComponents;
    }

    public boolean isStockable() {
        return stockable;
    }

    public void setStockable(boolean stockable) {
        this.stockable = stockable;
    }

    public void setActive(boolean active) {
       this.active = active;
    }
    
    public boolean isActive() {
        return this.active;
    }
}
