package com.ose.backend.dto.tickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.customers.CustomersIndexDTO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.GeoIPDTO;
import org.pasteque.api.dto.locations.InseeSimpleDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.SalesLocationIndexDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.security.UserIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;

public class TicketsIndexDTO implements IXlsResponse,ILoadable{

    private String id;
    private Long numeroTicket;
    private Date date;
    private double montantTicketTTC;
    private double montantTicketHT;
    private double montantRemise;
    private double nombreArticle;
    private double nombreLigne;

    private InseeSimpleDTO commune;
    private CustomersIndexDTO client;
    private UserIndexDTO vendeur;
    private TariffAreasIndexDTO periodeVente;
    private TariffAreasIndexDTO periodeVenteTheorique;
    private LocationsInfoDTO pointVente;
    private TourIndexDTO tournee;
    private SalesLocationIndexDTO deballage;
    private String note;

    
    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC ,  CellType.STRING , CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.NUMERIC , CellType.STRING};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getNumeroTicket() {
        return numeroTicket;
    }

    public void setNumeroTicket(Long numeroTicket) {
        this.numeroTicket = numeroTicket;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getMontantTicketTTC() {
        return montantTicketTTC;
    }

    public void setMontantTicketTTC(double montantTicketTTC) {
        this.montantTicketTTC = montantTicketTTC;
    }

    public double getMontantTicketHT() {
        return montantTicketHT;
    }

    public void setMontantTicketHT(double montantTicketHT) {
        this.montantTicketHT = montantTicketHT;
    }

    public double getMontantRemise() {
        return montantRemise;
    }

    public void setMontantRemise(double montantRemise) {
        this.montantRemise = montantRemise;
    }

    public double getNombreArticle() {
        return nombreArticle;
    }

    public void setNombreArticle(double nombreArticle) {
        this.nombreArticle = nombreArticle;
    }

    public double getNombreLigne() {
        return nombreLigne;
    }

    public void setNombreLigne(double nombreLigne) {
        this.nombreLigne = nombreLigne;
    }

    public InseeSimpleDTO getCommune() {
        return commune;
    }

    public void setCommune(InseeSimpleDTO commune) {
        this.commune = commune;
    }

    public CustomersIndexDTO getClient() {
        return client;
    }

    public void setClient(CustomersIndexDTO client) {
        this.client = client;
    }

    public UserIndexDTO getVendeur() {
        return vendeur;
    }

    public void setVendeur(UserIndexDTO vendeur) {
        this.vendeur = vendeur;
    }

    public TariffAreasIndexDTO getPeriodeVente() {
        return periodeVente;
    }

    public void setPeriodeVente(TariffAreasIndexDTO periodeVente) {
        this.periodeVente = periodeVente;
    }

    public LocationsInfoDTO getPointVente() {
        return pointVente;
    }

    public void setPointVente(LocationsInfoDTO pointVente) {
        this.pointVente = pointVente;
    }

    public TourIndexDTO getTournee() {
        return tournee;
    }

    public void setTournee(TourIndexDTO tournee) {
        this.tournee = tournee;
    }

    public SalesLocationIndexDTO getDeballage() {
        return deballage;
    }

    public void setDeballage(SalesLocationIndexDTO deballage) {
        this.deballage = deballage;
    }

    public TariffAreasIndexDTO getPeriodeVenteTheorique() {
        return periodeVenteTheorique;
    }

    public void setPeriodeVenteTheorique(TariffAreasIndexDTO periodeVenteTheorique) {
        this.periodeVenteTheorique = periodeVenteTheorique;
    }

    @Override
    public String[] header() {
        
        return new String[] { "Id" , "Num Ticket" , "Date" , "Montant TTC" , "Montant HT" , "Montant Remise HT" , 
                "Nombre Articles" , "Nombre Lignes" ,  "Num Insee Client" , "Code Postal Client" , "Commune Client" , "Latitude Client" , "Longitude CLient" ,
                "Num Client" , "Nom Client" , "Prenom Client" , "Telephone Client" , "Email Client" , "Date Naissance Client" , "Numéro Vendeur" , "Nom Affichage Vendeur" , "Nom Vendeur" , "Prénom Vendeur" , 
                "Num Periode de Vente" , "Nom période de Vente" , "Num Periode de Vente Théorique" , "Nom période de Vente Théorique" , "Numéro point de vente" , "Nom Point de Vente" , "Catégorie Point de Vente" ,
                "Numéro Tournée" , "Nom Tournée" , "Id déballage" , "Insee Déballage" , "Code Postal Déballage" , "Commune Déballage" , "Latitude Déballage" , "Longitude Déballage" , 
                "Ouverture déballage" , "Fermeture Déballage" , "Nombre de Pub" , "Droit de Place" , "Note"};

    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        DecimalFormat entier = new DecimalFormat("0", otherSymbols);
        DecimalFormat moneylong = new DecimalFormat("0.00####", otherSymbols);
        
        
        return new String[] { getId() , getNumeroTicket().toString() , dateHourFormatted , money.format(getMontantTicketTTC()) , moneylong.format(getMontantTicketHT()) , moneylong.format(getMontantRemise()) , 
                entier.format(getNombreArticle()) , entier.format(getNombreLigne()) ,  getCommune() == null ? "" : getCommune().getInsee() , getCommune() == null ? "" : getCommune().getZipCode() , getCommune() == null ? "" : getCommune().getCommune() , getCommune() == null ? "" : getCommune().getLatitude().toString() , getCommune() == null ? "" : getCommune().getLongitude().toString() ,
                getClient() == null ? "" : getClient().getId() , getClient() == null ? "" : getClient().getNom() , getClient() == null ? "" : getClient().getPrenom() , getClient() == null ? "" : getClient().getTelephone() , getClient() == null ? "" : getClient().getEmail() , getClient() == null || getClient().getDateNaissance() == null ? "" : df.format(getClient().getDateNaissance()) , getVendeur().getId() , getVendeur().getNomCaisse() == null ? "" : getVendeur().getNomCaisse() , getVendeur().getNom() == null ? "" : getVendeur().getNom() , getVendeur().getPrenom() == null ? "" : getVendeur().getPrenom() , 
                getPeriodeVente() == null ? "" : getPeriodeVente().getId() , getPeriodeVente() == null ? "" : getPeriodeVente().getNom() ,
                getPeriodeVenteTheorique() == null ? "" : getPeriodeVenteTheorique().getId() , getPeriodeVenteTheorique() == null ? "" : getPeriodeVenteTheorique().getNom() ,
                getPointVente().getId() , getPointVente().getLabel() , getPointVente().getCategory() ,
                getTournee() == null ? "" : getTournee().getId().toString() , getTournee() == null ? "" : getTournee().getNom() , getDeballage() == null ? "" : getDeballage().getId().toString() , getDeballage() == null ? "" : getDeballage().getCommune().getInsee() , getDeballage() == null ? "" : getDeballage().getCommune().getZipCode() , getDeballage() == null ? "" : getDeballage().getCommune().getCommune() , getDeballage() == null ? "" : getDeballage().getCommune().getLatitude().toString() , getDeballage() == null ? "" : getDeballage().getCommune().getLongitude().toString() , 
                getDeballage() == null ? "" : hdf.format(getDeballage().getDateHeureOuverture()) , getDeballage() == null ? "" : hdf.format(getDeballage().getDateHeureFermeture()) , getDeballage() == null ? "" : String.valueOf(getDeballage().getNombrePub()) , getDeballage() == null || getDeballage().getDroitPlace() == 0D ? "" : money.format(getDeballage().getDroitPlace()) , getNote() == null ? "" : getNote()};
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        //        retour.select = "SELECT t0.ID , t1.TICKETID , t0.DATENEW , SUM(t8.UNITS*t8.PRICE*(1-t8.DISCOUNTRATE))*(1-t1.DISCOUNTRATE) ,"
        //                + " SUM(t8.UNITS*t8.PRICE) - SUM(t8.UNITS*t8.PRICE*(1-t8.DISCOUNTRATE))*(1-t1.DISCOUNTRATE)  , COUNT(t8.LINE) , SUM(t8.UNITS) ,"
        //                + " t1.CUSTZIPCODE , t6.SEARCHKEY , t6.CARD , t6.LASTNAME , t6.FIRSTNAME , t6.POSTAL , t6.CITY , t6.PHONE , t6.PHONE2 , t6.EMAIL , t6.DATEOFBIRTH , "
        //                + " t1.PERSON , t7.NAME , t1.TARIFFAREA , t10.NAME , t5.NAME , t4.CASHREGISTER_ID , "
        //                + " t3.TOURNEES_ID , t9.NOM , t4.DEBALLAGE_ID ,  t3.NUM_INSEE , t3.DEBUT , t3.FIN , t3.NB_PUB , t3.DROIT_PLACE , t3.PAYE FROM";
        //        
        //     
        boolean retour = false;
        if((array.length + starting_index) >= 49) {
            try {
                InseeSimpleDTO commune = null;
                GeoIPDTO geoIP = null;
                setId((String) array[0+starting_index]);
                setNumeroTicket((Long) array[1+starting_index]);
                setDate((Date) array[2+starting_index]);
                if(array[47+starting_index] != null) {
                    setMontantTicketTTC((double) array[47+starting_index]);
                }
                setNote((String) array[48+starting_index]);
                
                if(array[3+starting_index] != null) {
                    setMontantTicketHT((double) array[3+starting_index]);
                }
                if(array[4+starting_index] != null) {
                    setMontantRemise((double) array[4+starting_index]);
                }
                setNombreArticle((Long) array[5+starting_index]);
                if(array[6+starting_index] != null) {
                    setNombreLigne((double) array[6+starting_index]);
                }

                //-- commune //
                if(array[7+starting_index] != null) {
                    commune = new InseeSimpleDTO();
                    geoIP = new GeoIPDTO();
                    commune.setCommune((String)array[44+starting_index]);
                    commune.setInsee((String)array[7+starting_index]);
                    commune.setLatitude((Double)array[45+starting_index]);
                    commune.setLongitude((Double)array[46+starting_index]);
                    commune.setZipCode((String)array[43+starting_index]);
                    geoIP.setLat(commune.getLatitude());
                    geoIP.setLon(commune.getLongitude());
                    commune.setLocation(geoIP);

                    setCommune(commune);
                }

                //Client
                if(array[8+starting_index] != null) {
                    setClient(new CustomersIndexDTO());
                    client.setId((String)array[8+starting_index]);
                    client.setNumeroCarte((String)array[9+starting_index]);
                    client.setNom((String)array[10+starting_index]);
                    client.setPrenom((String)array[11+starting_index]);
                    //TODO -insee cf array[7] ça devrait matcher
                    if(commune != null) {                       

                        client.setCommune(commune);
                    }
                    if(array[14+starting_index] != null) {
                        client.setTelephone((String)array[14+starting_index]);
                    } else {
                        if(array[15+starting_index] != null) {
                            client.setTelephone((String)array[15+starting_index]);
                        }
                    }
                    client.setEmail((String)array[16+starting_index]);
                    client.setDateNaissance((Date)array[17+starting_index]);
                }

                //Vendeur
                if(array[18+starting_index] != null) {
                    setVendeur(new UserIndexDTO());
                    vendeur.loadFromObjectArray(array, 18+starting_index);
                }
                //Periode
                if(array[22+starting_index] != null) {
                    setPeriodeVente(new TariffAreasIndexDTO());
                    periodeVente.loadFromObjectArray(array, 22+starting_index);
                }
                
                //Periode Théorique
                if(array[24+starting_index] != null) {
                    setPeriodeVenteTheorique(new TariffAreasIndexDTO());
                    periodeVenteTheorique.loadFromObjectArray(array, 24+starting_index);
                }
                
                //PdV
                if(array[26+starting_index] != null) {
                    setPointVente(new LocationsInfoDTO());
                    pointVente.loadFromObjectArray(array, 26+starting_index);
                }

                //Tournee
                if(array[30+starting_index] != null) {
                    setTournee(new TourIndexDTO());
                    tournee.loadFromObjectArray(array, 30+starting_index);
                }

                //Deballage
                if(array[32+starting_index] != null) {
                    setDeballage(new SalesLocationIndexDTO());
                    deballage.loadFromObjectArray(array, 32+starting_index);

                }

                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }

        }

        return retour;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();

        //Le double agregat sur un jointure n'est pas trivial ( produit cartesien)
        //La solution avec les deux requêtes imbriquées n'est pas efficace (parcours des deux tables dans leur intégralité)

        //On ajoutera à posteriori le montant total et les objets complémentaires qui pourraient servir plusieurs fois
        //InseeSimpleDTO présent au niveau du ticket mais aussi au niveau du client
        retour.select = "SELECT t0.ID , t1.TICKETID , t0.DATENEW , t1.FINALPRICE ,"
                + " SUM(t8.PRICE) - t1.FINALPRICE  , COUNT(t8.LINE) , SUM(t8.UNITS) ,"
                + " t12.NUM_INSEE , t6.SEARCHKEY , t6.CARD , t6.LASTNAME , t6.FIRSTNAME , t6.POSTAL , t6.CITY , t6.PHONE , t6.PHONE2 , t6.EMAIL , t6.DATEOFBIRTH , "
                + " t1.PERSON , t7.NAME , t7.NOM , t7.PRENOM , t1.TARIFFAREA , t10.NAME ,"
                /**
                 * Tarifaire théorique
                 * 
                 */
                + "( SELECT TARIFFAREAS.ID FROM TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_ID, " 
                + "( SELECT TARIFFAREAS.NAME  FROM  TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_NAME, "  
 
                + " t11.NAME , t5.LOCATION_ID , t14.NAME , t11.PARENT_LOCATION_ID , "
                + " t3.TOURNEES_ID , t9.NOM , t4.DEBALLAGE_ID ,  t3.NUM_INSEE , t3.DEBUT , t3.FIN , t3.NB_PUB , t3.DROIT_PLACE , t3.PAYE , "
                + " t912.ZIPCODE , t912.COMMUNE , t912.LATITUDE , t912.LONGITUDE , t12.ZIPCODE , t12.COMMUNE , t12.LATITUDE , t12.LONGITUDE , "
                + " ROUND(t1.FINALTAXEDPRICE,2) , t1.NOTE FROM";

        retour.tableList.add(" TICKETS t1 LEFT JOIN TICKETS_EXTENDED te ON ( te.TICKET_ID = t1.id ) "
                + "LEFT JOIN CUSTOMERS t6 ON ( t6.ID =  COALESCE(t1.CUSTOMER , te.CUSTOMER_ID) ) "
                + "LEFT JOIN TARIFFAREAS t10 ON (t10.ID = t1.TARIFFAREA) "
                + "LEFT JOIN INSEE t12 ON (t12.ID = t1.CUSTZIPCODE) ");
        retour.tableHash.put("t1", "TICKETS");
        retour.tableHash.put("t6", "CUSTOMERS");
        retour.tableHash.put("t10", "TARIFFAREA");
        retour.tableHash.put("t12", "INSEE");

        if(retour.tableHash.put("t0", "RECEIPTS") == null) {
            retour.tableList.add(" RECEIPTS t0 ");
            retour.constraintList.add(" t0.ID = t1.id ");
        }

        if(retour.tableHash.put("t7", "PEOPLE") == null) {
            retour.tableList.add(" PEOPLE t7 ");
            retour.constraintList.add(" t7.ID = t1.PERSON ");
        }

        if(retour.tableHash.put("t8", "TICKETLINES") == null) {
            retour.tableList.add(" TICKETLINES t8 ");
            retour.constraintList.add(" t8.ticket = t1.ID ");
        }

        if(retour.tableHash.put("t4", "CLOSEDCASH") == null) {
            retour.tableHash.put("t3", "DEBALLAGES");
            retour.tableList.add(" CLOSEDCASH t4 "
                    + "LEFT JOIN DEBALLAGES t3 ON (t3.ID = t4.DEBALLAGE_ID ) "
                    + "LEFT JOIN TOURNEES t9 ON (t9.ID = t3.TOURNEES_ID ) "
                    + "LEFT JOIN INSEE t912 ON (t912.ID = t3.NUM_INSEE) ");
            retour.constraintList.add(" t4.MONEY = t0.MONEY ");
        }

        if(retour.tableHash.put("t5", "CASHREGISTERS") == null) {
            retour.tableList.add(" CASHREGISTERS t5 ");
            retour.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
        }

        if(retour.tableHash.put("t11", "LOCATION") == null) {
            retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON t14.ID = t11.CATEG_ID ");
            retour.constraintList.add(" t11.ID = t5.LOCATION_ID ");
            retour.tableHash.put("t14", "CATEGORIES");
        }
        
        if(retour.tableHash.put("t13", "TAXES") == null) {
            retour.tableList.add(" TAXES t13 ");
            retour.constraintList.add(" t13.ID = t8.TAXID ");
        }

        retour.groupBy = "t1.TICKETID ";
        return retour;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
