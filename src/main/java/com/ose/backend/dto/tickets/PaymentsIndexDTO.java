package com.ose.backend.dto.tickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;




public class PaymentsIndexDTO implements IXlsResponse,ILoadable{

    private String ticketId;
    private String payment;
    private Double montant;
    private String reference;
    private String note;
    private Date echeance;
    private Date date;
    private String closedCashId;

    private TicketsIndexDTO ticket;
    
    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING };

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double amount) {
        this.montant = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getEcheance() {
        return echeance;
    }

    public void setEcheance(Date echeance) {
        this.echeance = echeance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date datenew) {
        this.date = datenew;
    }

    public TicketsIndexDTO getTicket() {
        return ticket;
    }

    public void setTicket(TicketsIndexDTO ticket) {
        this.ticket = ticket;
    }

    @Override
    public String[] header() {
        
        return new String[] {"id Ticket" , "Numéro Ticket" , "Mode Reglement" , "Montant" , "Reference" , "Note" , "Date Echéance" , "Date Opération"};
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        String dateFormatted = getEcheance() != null ? df.format(getEcheance()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        
        return new String[] { getTicketId() , getTicket().getNumeroTicket().toString() , getPayment() , money.format(getMontant()) , getReference() , getNote() ,dateFormatted , dateHourFormatted };
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();

        retour.select = "SELECT '-' , t2.PAYMENT , t2.TOTAL , t0.DATENEW , t2.REFERENCE , t2.NOTE , t2.ECHEANCE FROM ";
        //TODO 
        if(retour.tableHash.put("t0", "RECEIPTS") == null) {
            retour.tableList.add(" RECEIPTS t0 ");
        }

        if(retour.tableHash.put("t2", "PAYMENTS") == null) {
            retour.tableList.add(" PAYMENTS t2 ");
            retour.constraintList.add(" t2.receipt = t0.ID ");
        }
        
        
        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        boolean retour = false;
        if((array.length-starting_index ) >= 7) {
            try {
            setTicket((TicketsIndexDTO) array[0+starting_index]);
            setTicketId( new Long (getTicket().getNumeroTicket()).toString());
            setPayment((String) array[1+starting_index]);
            setMontant((Double) array[2+starting_index]);
            setDate((Date) array[3+starting_index]);
            setReference((String) array[4+starting_index]);
            setNote((String) array[5+starting_index]);
            setEcheance((Date) array[6+starting_index]);
            
            retour = true;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                retour = false;
            }
        }
        return retour;
    }

    public String getClosedCashId() {
        return closedCashId;
    }

    public void setClosedCashId(String closedCashId) {
        this.closedCashId = closedCashId;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
