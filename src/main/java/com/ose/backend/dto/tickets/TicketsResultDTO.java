package com.ose.backend.dto.tickets;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;

public class TicketsResultDTO implements IXlsResponse , ILoadable{

    private long ticketId;
    private Date date;
    private String customersLabel;
    private String username;
    private double amount;
    private String insee;
    private String zipcode;
    
    public static CellType[] types = {CellType.STRING , CellType.STRING ,  
        CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.STRING , CellType.STRING};


    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();
        
        retour.select = "SELECT t0.DATENEW , t6.NAME ,  t7.NAME , t1.TICKETID , t12.NUM_INSEE , t1.FINALTAXEDPRICE , t12.ZIPCODE FROM";
        
        retour.tableList.add(" TICKETS t1 LEFT JOIN TICKETS_EXTENDED te ON ( te.TICKET_ID = t1.id ) "
                + "LEFT JOIN CUSTOMERS t6 ON ( t6.ID =  COALESCE(t1.CUSTOMER , te.CUSTOMER_ID) ) "
                + "LEFT JOIN INSEE t12 ON (t12.ID = t1.CUSTZIPCODE) ");
        retour.tableHash.put("t1", "TICKETS");
        retour.tableHash.put("t6", "CUSTOMERS");
        retour.tableHash.put("t12", "INSEE");
        
        if(retour.tableHash.put("t0", "RECEIPTS") == null) {
            retour.tableList.add(" RECEIPTS t0 ");
            retour.constraintList.add(" t0.ID = t1.id ");
        }
        
        if(retour.tableHash.put("t7", "PEOPLE") == null) {
            retour.tableList.add(" PEOPLE t7 ");
            retour.constraintList.add(" t7.ID = t1.PERSON ");
        }
        
        retour.groupBy = "t1.TICKETID ";
        return retour;
    }
    
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCustomersLabel() {
        return customersLabel;
    }

    public void setCustomersLabel(String customersLabel) {
        this.customersLabel = customersLabel;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTicketId() {
        return ticketId;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getInsee() {
        return insee;
    }

    public void setInsee(String insee) {
        this.insee = insee;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String[] header() {
        return new String[] { "Ticket" , "Date" , "Client" , "Utilisateur" , "Montant TTC" , "INSEE" , "CodePostal"};
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        return new String[] {Long.toString(getTicketId()) , dateHourFormatted , getCustomersLabel() , getUsername() , money.format(getAmount()) , getInsee() , getZipcode()};
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        boolean retour = false;
        if((array.length + starting_index) >= 7) {
            try {
            setDate((Date) array[0+starting_index]);
            setCustomersLabel((String) array[1+starting_index]);
            setUsername((String) array[2+starting_index]);
            setTicketId((long) array[3+starting_index]);
            setInsee((String) array[4+starting_index]);
            setAmount((double) array[5+starting_index]);
            setZipcode((String) array[6+starting_index]);
            retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
            
        }
        return retour;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
