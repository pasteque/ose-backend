package com.ose.backend.dto.tickets;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.SalesLocationIndexDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.security.UserIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;

public class TicketLinesIndexDTO implements IXlsResponse,ILoadable{

    private String id;
    private String numTicket;
    private String numLigne;
    
    private String type;
    private Date date;
    private double quantite;
    private double prixUnitaireTheoriqueHT;
    private double prixUnitaireTheoriqueTTC;
    private double prixUnitaireHT;
    private double prixUnitaireTTC;
    private double montantTVA;
    private double autresTaxes;
    private double PMPA;
    private double marge; 
    private double dernierPMPA;
    private double dernierPrixAchat;
    private double montantRemiseTTC;
    private double CAEffectifTTC;
    private double CATheoriqueTTC;
    private double CAEffectifHT;
    private double CATheoriqueHT;
    private TariffAreasIndexDTO catalogue;
    private ProductsIndexDTO produit;
    private ProductsIndexDTO produitUnitaire;
    private boolean kit;
    private double coefOffre;
    private double qteUnitaire;
    private double tauxRemiseLigne;
    private double tauxRemiseTicket;
    private String note;

    private TicketsIndexDTO ticket;

    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.NUMERIC ,
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.BOOLEAN , CellType.STRING , CellType.STRING , CellType.STRING , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.STRING ,  CellType.STRING , CellType.STRING };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public double getPrixUnitaireHT() {
        return prixUnitaireHT;
    }

    public void setPrixUnitaireHT(double prixUnitaireHT) {
        this.prixUnitaireHT = prixUnitaireHT;
    }

    public double getPrixUnitaireTTC() {
        return prixUnitaireTTC;
    }

    public void setPrixUnitaireTTC(double prixUnitaireTTC) {
        this.prixUnitaireTTC = prixUnitaireTTC;
    }

    public double getMontantTVA() {
        return montantTVA;
    }

    public void setMontantTVA(double montantTVA) {
        this.montantTVA = montantTVA;
    }

    public double getAutresTaxes() {
        return autresTaxes;
    }

    public void setAutresTaxes(double autresTaxes) {
        this.autresTaxes = autresTaxes;
    }

    public double getPMPA() {
        return PMPA;
    }

    public void setPMPA(double pMPA) {
        PMPA = pMPA;
    }

    public double getDernierPrixAchat() {
        return dernierPrixAchat;
    }

    public void setDernierPrixAchat(double dernierPrixAchat) {
        this.dernierPrixAchat = dernierPrixAchat;
    }

    public double getMontantRemiseTTC() {
        return montantRemiseTTC;
    }

    public void setMontantRemiseTTC(double montantRemiseTTC) {
        this.montantRemiseTTC = montantRemiseTTC;
    }

    public TariffAreasIndexDTO getCatalogue() {
        return catalogue;
    }

    public void setCatalogue(TariffAreasIndexDTO catalogue) {
        this.catalogue = catalogue;
    }

    public ProductsIndexDTO getProduit() {
        return produit;
    }

    public void setProduit(ProductsIndexDTO produit) {
        this.produit = produit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TicketsIndexDTO getTicket() {
        return ticket;
    }

    public void setTicket(TicketsIndexDTO ticket) {
        this.ticket = ticket;
    }

    public ProductsIndexDTO getProduitUnitaire() {
        return produitUnitaire;
    }

    public void setProduitUnitaire(ProductsIndexDTO produitUnitaire) {
        this.produitUnitaire = produitUnitaire;
    }

    public double getPrixUnitaireTheoriqueHT() {
        return prixUnitaireTheoriqueHT;
    }

    public void setPrixUnitaireTheoriqueHT(double prixUnitaireTheoriqueHT) {
        this.prixUnitaireTheoriqueHT = prixUnitaireTheoriqueHT;
    }

    public double getPrixUnitaireTheoriqueTTC() {
        return prixUnitaireTheoriqueTTC;
    }

    public void setPrixUnitaireTheoriqueTTC(double prixUnitaireTheoriqueTTC) {
        this.prixUnitaireTheoriqueTTC = prixUnitaireTheoriqueTTC;
    }

    public double getCAEffectifTTC() {
        return CAEffectifTTC;
    }

    public void setCAEffectifTTC(double cAEffectifTTC) {
        CAEffectifTTC = cAEffectifTTC;
    }

    public double getCATheoriqueTTC() {
        return CATheoriqueTTC;
    }

    public void setCATheoriqueTTC(double cATheoriqueTTC) {
        CATheoriqueTTC = cATheoriqueTTC;
    }

    public double getCAEffectifHT() {
        return CAEffectifHT;
    }

    public void setCAEffectifHT(double cAEffectifHT) {
        CAEffectifHT = cAEffectifHT;
    }

    public double getCATheoriqueHT() {
        return CATheoriqueHT;
    }

    public void setCATheoriqueHT(double cATheoriqueHT) {
        CATheoriqueHT = cATheoriqueHT;
    }

    public double getMarge() {
        return marge;
    }

    public void setMarge(double marge) {
        this.marge = marge;
    }

    public double getDernierPMPA() {
        return dernierPMPA;
    }

    public void setDernierPMPA(double dernierPMPA) {
        this.dernierPMPA = dernierPMPA;
    }

    public String getNumTicket() {
        return numTicket;
    }

    public void setNumTicket(String numTicket) {
        this.numTicket = numTicket;
    }

    public String getNumLigne() {
        return numLigne;
    }

    public void setNumLigne(String numLigne) {
        this.numLigne = numLigne;
    }

    public boolean isKit() {
        return kit;
    }

    public void setKit(boolean kit) {
        this.kit = kit;
    }

    public double getCoefOffre() {
        return coefOffre;
    }

    public void setCoefOffre(double coefOffre) {
        this.coefOffre = coefOffre;
    }

    public double getQteUnitaire() {
        return qteUnitaire;
    }

    public void setQteUnitaire(double qteUnitaire) {
        this.qteUnitaire = qteUnitaire;
    }

    public double getTauxRemiseLigne() {
        return tauxRemiseLigne;
    }

    public void setTauxRemiseLigne(double tauxRemiseLigne) {
        this.tauxRemiseLigne = tauxRemiseLigne;
    }

    public double getTauxRemiseTicket() {
        return tauxRemiseTicket;
    }

    public void setTauxRemiseTicket(double tauxRemiseTicket) {
        this.tauxRemiseTicket = tauxRemiseTicket;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String[] header() {

        //TODO ajout Montant achat - PMPA et co non arrondis
        //Montant achat = qté * pmpa à date
        return new String[] {"Ticket" , "Ligne", "Type" , "Date" , "Année" , "Mois" , "Jour" ,
                "Id Point de Vente" , "Point de Vente" , "Famille PdV" ,
                "Num Tournée" , "Tournée" ,
                "Insee Deballage" , "Code Postal Déballage" , "Commune Déballage" , "Ouverture Déballage" , "Fermeture Déballage" , "Nb Pub" ,
                "Num Vendeur" , "Nom Vendeur Caisse" , "Nom Vendeur" , "Prénom Vendeur" , "Vendeur" ,
                "Num Période de Vente" , "Nom Période de Vente" ,
                "Quantité" , "PU HT", "PU TTC" , "PU Théorique HT", "PU Théorique TTC" , "Taux Remise Ligne", "Taux Remise Ticket" , "Montant TVA" , "Autres Taxes" , 
                "PMPA" , "Dernier PA" , "Montant Remise TTC" , "CA Effectif" , "CA Théorique" ,
                "CA Effectif HT" , "CA Théorique HT" , "Marge" ,                
                "Num Tarifaire" , "Nom Tarifaire" ,
                "Ref Produit" , "Libellé Produit" , "Univers" ,
                "Kit" ,
                "Ref Produit Unitaire " , "Libellé Produit Unitaire " , "Univers Produt Unitaire" ,
                "Multiple" , "Quantité Unitaire" , "Note",  "Num Période de Vente Théorique" , "Nom Période de Vente Théorique" , "Note Ticket"};
    }

    @Override
    public String[] CSV() {

        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        DecimalFormat moneylong = new DecimalFormat("0.00####", otherSymbols);
        DecimalFormat entier = new DecimalFormat("0", otherSymbols);

        return new String[] {getNumTicket() , getNumLigne() , getType() , dateHourFormatted , getDate() != null ?  new SimpleDateFormat("yyyy").format(getDate()) : "" , getDate() != null ?  new SimpleDateFormat("MM").format(getDate()) : "" , getDate() != null ?  new SimpleDateFormat("dd").format(getDate()) : "" ,
                ticket.getPointVente().getId() , ticket.getPointVente().getLabel() , ticket.getPointVente().getCategory() ,
                ticket.getTournee() == null ? "" : ticket.getTournee().getId().toString() , ticket.getTournee() == null ? "" : ticket.getTournee().getNom() , 
                ticket.getDeballage() == null ? "" : ticket.getDeballage().getCommune().getInsee() , ticket.getDeballage() == null ? "" : ticket.getDeballage().getCommune().getZipCode() , ticket.getDeballage() == null ? "" : ticket.getDeballage().getCommune().getCommune() ,  
                ticket.getDeballage() == null ? "" : hdf.format(ticket.getDeballage().getDateHeureOuverture()) , ticket.getDeballage() == null ? "" : hdf.format(ticket.getDeballage().getDateHeureFermeture()) , ticket.getDeballage() == null ? "" : String.valueOf(ticket.getDeballage().getNombrePub()) ,
                ticket.getVendeur().getId() , ticket.getVendeur().getNomCaisse() == null ? "" : ticket.getVendeur().getNomCaisse() , ticket.getVendeur().getNom() == null ? "" : ticket.getVendeur().getNom() , ticket.getVendeur().getPrenom() == null ? "" : ticket.getVendeur().getPrenom() , ticket.getVendeur().getNom() == null ? "" : ticket.getVendeur().getNom() +  ( ticket.getVendeur().getPrenom() == null ? "" : ( (ticket.getVendeur().getNom() == null ? "" : " " ) + ticket.getVendeur().getPrenom() )) ,
                ticket.getPeriodeVente() == null ? "" : ticket.getPeriodeVente().getId() , ticket.getPeriodeVente() == null ? "" : ticket.getPeriodeVente().getNom() ,              
                String.valueOf(getQuantite()) , moneylong.format(getPrixUnitaireHT()) , money.format(getPrixUnitaireTTC()) , 
                moneylong.format(getPrixUnitaireTheoriqueHT()) , money.format(getPrixUnitaireTheoriqueTTC()) , moneylong.format(100*getTauxRemiseLigne()) , moneylong.format(100*getTauxRemiseTicket()) , 
                moneylong.format(getMontantTVA()) , money.format(getAutresTaxes()) , 
                moneylong.format(getPMPA()) , money.format(getDernierPrixAchat()) , money.format(getMontantRemiseTTC()) , money.format(getCAEffectifTTC()) , money.format(getCATheoriqueTTC()) ,
                moneylong.format(getCAEffectifHT()) , moneylong.format(getCATheoriqueHT()) , moneylong.format(getMarge()),
                getCatalogue() == null ? "" : getCatalogue().getId() , getCatalogue() == null ? "" : getCatalogue().getNom() ,
                getProduit()== null ? "" : getProduit().getId() , getProduit()== null ? "" : getProduit().getLibelle() , getProduit()== null ? "" : getProduit().getCategorie() ,
                String.valueOf(isKit()) , getProduitUnitaire() == null ? "" : getProduitUnitaire().getId() , getProduitUnitaire() == null ? "" : getProduitUnitaire().getLibelle() ,
                getProduitUnitaire() == null ? "" : getProduitUnitaire().getCategorie() , getProduitUnitaire() == null ? "" : entier.format(getCoefOffre()),  getProduitUnitaire() == null ? "" : entier.format(getQteUnitaire()) , getNote() == null ? "" : getNote() , 
                ticket.getPeriodeVenteTheorique() == null ? "" : ticket.getPeriodeVenteTheorique().getId() , ticket.getPeriodeVenteTheorique() == null ? "" : ticket.getPeriodeVenteTheorique().getNom() , ticket.getNote() == null ? "" : ticket.getNote()};
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();

        /** 
         * EDU - 2018-01-19
         * 
         * Trop de temp tables et trop grosses en particulier sur cette requête.
         * On passe la table dérivée dont on n'utilise qu'un champ et dont le calcul ne pourra servir qu'une fois dans le corps de la requête
         * A savoir mvt_price 
         * Grosse progression des perfs sur une requête petite : moins de 10 secondes pour une 200 tickets
         * 45 secondes 2250 tickets
         * On ne peut de toute façon pas se permettre de générer une table temporaire avec tous les mouvements de stocks et tous les tickets
         * plusieurs millions d'entrée par an.
         */
        
        /*
        SELECT  t1.TICKETID , t8.LINE , t0.DATENEW , t8.TYPE , t11.ID , t11.NAME , t14.NAME ,
        t3.TOURNEES_ID , t9.NOM , t4.DEBALLAGE_ID ,  t3.NUM_INSEE , t3.DEBUT , t3.FIN , t3.NB_PUB , t3.DROIT_PLACE , t3.PAYE ,
        t912.ZIPCODE , t912.COMMUNE , t912.LATITUDE , t912.LONGITUDE ,
        t1.PERSON , t7.NAME , t7.NOM , t7.PRENOM , t1.TARIFFAREA , t10.NAME , 
        t8.TARIFFAREA , t910.NAME , t8.PRODUCT , t15.NAME , t8.UNITS , t8.PRICE , t8.PRICE * t8.UNITS  PV_HT , t8.PRICE * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) PU_NET, t8.PRICE * t8.UNITS * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) PV_NET, t13.RATE , 
        ROUND(t8.PRICE * ( 1 + t13.RATE ) , 2) PU_TTC, ROUND(t8.PRICE * ( 1 + t13.RATE ) ,2) * t8.UNITS PV_TTC,
        ROUND(t8.PRICE * ( 1 + t13.RATE ) * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) , 2) PU_NET_TTC, ROUND(t8.PRICE * ( 1 + t13.RATE ) * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) , 2) * t8.UNITS PV_NET_TTC,
        IF(ISNULL(t999.NB) OR t999.NB = 0 , FALSE , TRUE ) AS COMPOS , 
        IF(t999.NB = 1 , t999.PRODUCT2 , NULL ) AS ELEMENT, 
        IF(t999.NB = 1 , t999.NAME_2 , NULL ) AS NOM_COMP ,
        IF(t999.NB = 1 , t999.QUANTITY , NULL ) AS MULTIPLE ,
        IF(t999.NB = 1 , t999.QUANTITY * t8.UNITS , NULL ) AS QTE_UNIT , 
        t8.DISCOUNTRATE AS DISC_LIGN, t1.DISCOUNTRATE AS DISC_TICKET , 
        t8.NOTE ,
        t998.MVT_PRICE , 
        t15.STOCKCOST , 
        t15.PRICEBUY , t1.NOTE
        FROM   CLOSEDCASH t4  LEFT JOIN DEBALLAGES t3 ON (t3.ID = t4.DEBALLAGE_ID ) 
        LEFT JOIN TOURNEES t9 ON (t9.ID = t3.TOURNEES_ID ) 
        LEFT JOIN INSEE t912 ON (t912.NUM_INSEE = t3.NUM_INSEE) ,
        CASHREGISTERS t5 , LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON t14.ID = t11.CATEG_ID, 
        RECEIPTS t0 , 
        TICKETS t1 LEFT JOIN PEOPLE t7 ON (t7.ID = t1.PERSON ) LEFT JOIN TARIFFAREAS t10 ON (t10.ID = t1.TARIFFAREA) ,
        TICKETLINES t8 LEFT JOIN TARIFFAREAS t910 ON (t910.ID = t8.TARIFFAREA) LEFT JOIN PRODUCTS t15 ON t15.ID = t8.PRODUCT 
        LEFT JOIN  ( SELECT COUNT(*) AS NB , PRODUCT , PRODUCT2 , QUANTITY , PRODUCTS.NAME  AS NAME_2 FROM PRODUCTS_COM , PRODUCTS WHERE PRODUCTS.ID = PRODUCTS_COM.PRODUCT2 GROUP BY PRODUCT ) t999 ON t999.PRODUCT = t8.PRODUCT 
        LEFT JOIN  ( SELECT TICKETS.ID TICKET , STOCKDIARY.NOTE LINE , SUM(STOCKDIARY.PRICE) MVT_PRICE FROM  STOCKDIARY , TICKETS WHERE STOCKDIARY.REFERENCE = CAST(TICKETS.TICKETID AS CHAR) GROUP BY STOCKDIARY.REFERENCE , STOCKDIARY.NOTE) t998 ON t998.TICKET = t8.TICKET AND t998.LINE = t8.LINE,
        TAXES t13
        WHERE  t5.ID = t4.CASHREGISTER_ID  AND  t11.ID = t5.LOCATION_ID  AND  t0.MONEY = t4.MONEY  AND t1.ID = t0.ID AND t8.ticket = t1.ID AND t13.ID = t8.TAXID
        AND  (t0.DATENEW >= '2016-06-07') AND  (t0.DATENEW <= '2016-07-07 23:59') ;
         */
        
        retour.select = "SELECT  t1.TICKETID , t8.LINE , t0.DATENEW , t8.TYPE , t11.NAME , t11.ID , t14.NAME , t11.PARENT_LOCATION_ID , "
                + "t3.TOURNEES_ID , t9.NOM , t4.DEBALLAGE_ID ,  t3.NUM_INSEE , t3.DEBUT , t3.FIN , t3.NB_PUB , t3.DROIT_PLACE , t3.PAYE , "
                + "t912.ZIPCODE , t912.COMMUNE , t912.LATITUDE , t912.LONGITUDE , "
                + "t1.PERSON , t7.NAME , t7.NOM , t7.PRENOM , t1.TARIFFAREA , t10.NAME , "
                + "( SELECT TARIFFAREAS.ID FROM TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_ID, " 
                + "( SELECT TARIFFAREAS.NAME  FROM  TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_NAME, "   
                + "t8.TARIFFAREA , t910.NAME , t8.PRODUCT , t15.NAME , t15.CATEGORY , t8.UNITS , t8.UNITPRICE , t8.PRICE  PV_HT , t8.UNITPRICE * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) PU_NET, t8.FINALPRICE * (1 - t1.DISCOUNTRATE ) PV_NET, t13.RATE , " 
                + "ROUND(t8.UNITPRICE * ( 1 + t13.RATE ) ,2) PU_TTC, t8.TAXEDPRICE PV_TTC, "
                + "ROUND(t8.UNITPRICE * ( 1 + t13.RATE ) * ( 1 - t8.DISCOUNTRATE) * (1 - t1.DISCOUNTRATE ) , 2) PU_NET_TTC, ROUND(t8.FINALTAXEDPRICE * (1 - t1.DISCOUNTRATE ) , 2) PV_NET_TTC, "
                + "IF(ISNULL(t999.NB) OR t999.NB = 0 , FALSE , TRUE ) AS COMPOS , "
                + "IF(t999.NB = 1 , t999.PRODUCT2 , NULL ) AS ELEMENT, "
                + "IF(t999.NB = 1 , t999.NAME_2 , NULL ) AS NOM_COMP , "
                + "IF(t999.NB = 1 , t999.CATEGORY , NULL ) AS CAT_COMP , "
                + "IF(t999.NB = 1 , t999.QUANTITY , NULL ) AS MULTIPLE , "
                + "IF(t999.NB = 1 , t999.QUANTITY * t8.UNITS , NULL ) AS QTE_UNIT , " 
                + "t8.DISCOUNTRATE AS DISC_LIGN, t1.DISCOUNTRATE AS DISC_TICKET , "
                + "t8.NOTE , "
                + "( SELECT SUM(STOCKDIARY.PRICE) FROM  STOCKDIARY WHERE STOCKDIARY.REFERENCE = CAST(t1.TICKETID AS CHAR) AND STOCKDIARY.NOTE = t8.LINE GROUP BY STOCKDIARY.REFERENCE , STOCKDIARY.NOTE) AS MVT_PRICE , "
                + "t15.STOCKCOST , "
                + "t15.PRICEBUY , t1.NOTE FROM";


        if(retour.tableHash.put("t0", "RECEIPTS") == null) {
            retour.tableList.add(" RECEIPTS t0 ");
        }

        if(retour.tableHash.put("t1", "TICKETS") == null) {
            retour.tableList.add(" TICKETS t1 "
                    + "LEFT JOIN PEOPLE t7 ON (t7.ID = t1.PERSON ) "
                    + "LEFT JOIN TARIFFAREAS t10 ON (t10.ID = t1.TARIFFAREA) ");
            retour.tableHash.put("t7", "PEOPLE");
            retour.tableHash.put("t10", "TARIFFAREAS");
            retour.constraintList.add(" t1.ID = t0.ID ");
        }


        if(retour.tableHash.put("t8", "TICKETLINES") == null) {
            retour.tableList.add(" TICKETLINES t8 "
                    + "LEFT JOIN TARIFFAREAS t910 ON (t910.ID = t8.TARIFFAREA) "
                    + "LEFT JOIN PRODUCTS t15 ON (t15.ID = t8.PRODUCT) "
                    + "LEFT JOIN  ( SELECT COUNT(*) AS NB , PRODUCT , PRODUCT2 , QUANTITY , PRODUCTS.NAME  AS NAME_2 , PRODUCTS.CATEGORY  AS CATEGORY FROM PRODUCTS_COM , PRODUCTS WHERE PRODUCTS.ID = PRODUCTS_COM.PRODUCT2 GROUP BY PRODUCT ) t999 ON ( t999.PRODUCT = t8.PRODUCT) ");
            retour.constraintList.add(" t8.ticket = t0.ID ");
            retour.tableHash.put("t910", "TARIFFAREAS");
            retour.tableHash.put("t15", "PRODUCTS");
        }


        if(retour.tableHash.put("t4", "CLOSEDCASH") == null) {
            retour.tableHash.put("t3", "DEBALLAGES");
            retour.tableHash.put("t9", "TOURNEES");
            retour.tableList.add(" CLOSEDCASH t4 "
                    + "LEFT JOIN DEBALLAGES t3 ON (t3.ID = t4.DEBALLAGE_ID ) "
                    + "LEFT JOIN TOURNEES t9 ON (t9.ID = t3.TOURNEES_ID ) "
                    + "LEFT JOIN INSEE t912 ON (t912.NUM_INSEE = t3.NUM_INSEE) ");
            retour.constraintList.add(" t4.MONEY = t0.MONEY ");
        }

        if(retour.tableHash.put("t5", "CASHREGISTERS") == null) {
            retour.tableList.add(" CASHREGISTERS t5 ");
            retour.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
        }

        if(retour.tableHash.put("t11", "LOCATION") == null) {
            retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON t14.ID = t11.CATEG_ID ");
            retour.constraintList.add(" t11.ID = t5.LOCATION_ID ");
            retour.tableHash.put("t14", "CATEGORIES");
        }

        //TODO Gestion des taxes taux dates etc etc
        //TODO Gestion  D3E ?
        if(retour.tableHash.put("t13", "TAXES") == null) {
            retour.tableList.add(" TAXES t13 ");
            retour.constraintList.add(" t13.ID = t8.TAXID ");
        }

        //Valeur empirique - basée sur l'occupation mémoire de 160000 résultats en Octobre 2016
        //v max = 2096
        //v moy = 1126
        retour.setRowSize(1280 );

        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        boolean retour = false;
        if((array.length + starting_index) >= 57) {
            try {
                setNumTicket(((Long) array[0+starting_index]).toString());
                setNumLigne(String.format("%02d", (Integer) array[1+starting_index]));
                setId(getNumTicket() + "-" + getNumLigne());
                if(array[2+starting_index] != null) {
                    setDate((Date) array[2+starting_index]);
                }
                if(array[3+starting_index] != null) {
                    setType((String) array[3+starting_index]);
                }
                setTicket(new TicketsIndexDTO());
                
                //Vendeur
                if(array[21+starting_index] != null) {
                    UserIndexDTO vendeur = new UserIndexDTO();
                    vendeur.loadFromObjectArray(array, 21+starting_index);
                    ticket.setVendeur(vendeur);
                }
                //Periode
                if(array[25+starting_index] != null) {
                    TariffAreasIndexDTO periodeVente =  new TariffAreasIndexDTO();
                    periodeVente.loadFromObjectArray(array, 25+starting_index);
                    ticket.setPeriodeVente(periodeVente);
                }
                //Periode Théorique
                if(array[27+starting_index] != null) {
                    TariffAreasIndexDTO periodeVenteTheorique =  new TariffAreasIndexDTO();
                    periodeVenteTheorique.loadFromObjectArray(array, 27+starting_index);
                    ticket.setPeriodeVenteTheorique(periodeVenteTheorique);
                }
                //PdV
                if(array[4+starting_index] != null) {
                    LocationsInfoDTO pointVente = new LocationsInfoDTO();
                    pointVente.loadFromObjectArray(array, 4+starting_index);
                    ticket.setPointVente(pointVente);
                }
                //Tournee
                if(array[8+starting_index] != null) {
                    TourIndexDTO tournee = new TourIndexDTO();
                    tournee.loadFromObjectArray(array, 8+starting_index);
                    ticket.setTournee(tournee);
                }
                //Deballage
                if(array[10+starting_index] != null) {
                    SalesLocationIndexDTO deballage = new SalesLocationIndexDTO();
                    deballage.loadFromObjectArray(array, 10+starting_index);
                    ticket.setDeballage(deballage);

                }
                //Periode
                if(array[29+starting_index] != null) {
                    TariffAreasIndexDTO periodeVente =  new TariffAreasIndexDTO();
                    periodeVente.loadFromObjectArray(array, 29+starting_index);
                    setCatalogue(periodeVente);
                }
                
                //Produit
                if(array[31+starting_index] != null) {
                    
                    ProductsIndexDTO produit =  new ProductsIndexDTO();
                    if(array[31+starting_index] != null) {
                        produit.setId((String) array[31+starting_index]);
                    }
                    if(array[32+starting_index] != null) {
                        produit.setLibelle((String) array[32+starting_index]);
                    }
                    if(array[33+starting_index] != null) {
                        produit.setCategorie((String) array[33+starting_index]);
                    }
                    setProduit(produit);
                }
                
                //Produit-Composant
                if(array[45+starting_index] != null) {
                    
                    ProductsIndexDTO produit =  new ProductsIndexDTO();
                    if(array[45+starting_index] != null) {
                        produit.setId((String) array[45+starting_index]);
                    }
                    if(array[46+starting_index] != null) {
                        produit.setLibelle((String) array[46+starting_index]);
                    }
                    if(array[47+starting_index] != null) {
                        produit.setCategorie((String) array[47+starting_index]);
                    }
                    setProduitUnitaire(produit);
                }
                
                if(array[34+starting_index] != null) {
                    setQuantite((double) array[34+starting_index]);
                }
                
                if(array[35+starting_index] != null) {
                    setPrixUnitaireTheoriqueHT((double) array[35+starting_index]);
                }
                
                if(array[40+starting_index] != null) {
                    setPrixUnitaireTheoriqueTTC((double) array[40+starting_index]);
                }
                
                if(array[37+starting_index] != null) {
                    setPrixUnitaireHT((double) array[37+starting_index]);
                }
                
                if(array[42+starting_index] != null) {
                    setPrixUnitaireTTC((double) array[42+starting_index]);
                }
                
                
                if(array[36+starting_index] != null) {
                    setCATheoriqueHT((double) array[36+starting_index]);
                }
                
                if(array[38+starting_index] != null) {
                    setCAEffectifHT((double) array[38+starting_index]);
                }
                
                if(array[41+starting_index] != null) {
                    setCATheoriqueTTC((double) array[41+starting_index]);
                }
                
                if(array[43+starting_index] != null) {
                    setCAEffectifTTC((double) array[43+starting_index]);
                }
                
                //Taux TVA array[36]
                
                setMontantTVA(getCAEffectifTTC() - getCAEffectifHT());
                setMontantRemiseTTC(getCAEffectifTTC() - getCATheoriqueTTC());
//                private double autresTaxes;

                if(array[53+starting_index] != null) {
                    setPMPA(-1 * ((double) array[53+starting_index]) / getQuantite());
                    setMarge(getCAEffectifHT() + ((double) array[53+starting_index]));
                }
                if(array[54+starting_index] != null) {
                    setDernierPMPA((double) array[54+starting_index]);
                }
                if(array[55+starting_index] != null) {
                    setDernierPrixAchat((double) array[55+starting_index]);
                }
                
                if(array[44+starting_index] instanceof Long) {
                    setKit( 1 == ((long) array[44+starting_index]));
                }
                if(array[44+starting_index] instanceof Integer) {
                    setKit( 1 == ((int) array[44+starting_index]));
                }
                if(array[48+starting_index] != null) {
                    setCoefOffre((double) array[48+starting_index]);
                }
                if(array[49+starting_index] != null) {
                    setQteUnitaire((double) array[49+starting_index]);
                }
                
                setTauxRemiseLigne((double) array[50+starting_index]);
                setTauxRemiseTicket((double) array[51+starting_index]);
                if(array[52+starting_index] != null) {
                    setNote((String) array[52+starting_index]);
                }
                
                if(array[56+starting_index] != null) {
                    ticket.setNote((String) array[56+starting_index]);
                }
                
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }
        return retour;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
