package com.ose.backend.dto.sales;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.response.ICsvResponse;

public class SalesDetailsOverViewDTO implements ICsvResponse {

    private String locationId;
    private String locationLabel;
    private String dimension;
    private double turnoverN0 = 0D;
    private int salesCountN0 = 0;
    private double averageBasketN0 = 0D;
    private double turnoverN1 = 0D;
    private int salesCountN1 = 0;
    private double averageBasketN1 = 0D;
    
    public static CellType[] types = { CellType.STRING , CellType.STRING };

    public SalesDetailsOverViewDTO(String locationId, String locationLabel, String dimension, List<SalesDetailsDTO> listN0, List<SalesDetailsDTO> listN1) {
        
        setDimension(dimension);
        setLocationId(locationId);
        setLocationLabel(locationLabel);
        
        for ( SalesDetailsDTO N0 : listN0) {
            setTurnoverN0(turnoverN0 + N0.getTurnover());
            setSalesCountN0(salesCountN0 + N0.getSalesCount());
        }
        if(salesCountN0 != 0) {
            setAverageBasketN0(turnoverN0 / salesCountN0);
        }
        for ( SalesDetailsDTO N1 : listN1) {
            setTurnoverN1(turnoverN1 + N1.getTurnover());
            setSalesCountN1(salesCountN1 + N1.getSalesCount());
        }
        if(salesCountN1 != 0) {
            setAverageBasketN1(turnoverN1 / salesCountN1);
        }
        
    }

    public double getTurnoverN0() {
        return turnoverN0;
    }

    public void setTurnoverN0(double turnover) {
        this.turnoverN0 = turnover;
    }

    public int getSalesCountN0() {
        return salesCountN0;
    }

    public void setSalesCountN0(int salesCount) {
        this.salesCountN0 = salesCount;
    }

    public double getAverageBasketN0() {
        return averageBasketN0;
    }

    public void setAverageBasketN0(double averageBasket) {
        this.averageBasketN0 = averageBasket;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationLabel() {
        return locationLabel;
    }

    public void setLocationLabel(String locationLabel) {
        this.locationLabel = locationLabel;
    }

    public double getTurnoverN1() {
        return turnoverN1;
    }

    public void setTurnoverN1(double turnoverN1) {
        this.turnoverN1 = turnoverN1;
    }

    public int getSalesCountN1() {
        return salesCountN1;
    }

    public void setSalesCountN1(int salesCountN1) {
        this.salesCountN1 = salesCountN1;
    }

    public double getAverageBasketN1() {
        return averageBasketN1;
    }

    public void setAverageBasketN1(double averageBasketN1) {
        this.averageBasketN1 = averageBasketN1;
    }

    @Override
    public String[] header() {
        return new String[] { "Location" , "Dimension", "Chiffre d'affaires", "Chiffre d'affaires N-1", "Nombre de ventes", "Nombre de ventes N-1", "Panier moyen" , "Panier moyen N-1"};
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] { getLocationLabel() , getDimension(), decf.format(getTurnoverN0()), decf.format(getTurnoverN1()), String.valueOf(getSalesCountN0()),
                String.valueOf(getSalesCountN1()), decf.format(getAverageBasketN0()), decf.format(getAverageBasketN1()) };
    }

}
