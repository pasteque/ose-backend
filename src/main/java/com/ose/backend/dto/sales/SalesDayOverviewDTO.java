package com.ose.backend.dto.sales;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.security.UserIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.ICsvResponse;


public class SalesDayOverviewDTO implements ICsvResponse, ILoadable {
    //Sessions de vente agrégées par jour

    private UserIndexDTO vendeur;
    private TariffAreasIndexDTO periodeVente;
    private LocationsInfoDTO pointVente;
    private Date ouverture;
    private Date fermeture;
    private BigDecimal caisse_ouverture;
    private BigDecimal caisse_fermeture;
    private BigDecimal caisse_theorique;
    private Long nbTickets;
    private BigDecimal CACheques;
    private Long nbCheques;
    private BigDecimal CAEspeces;
    private Long nbEspeces;
    //retour chk retour esp
    private BigDecimal CACartes;
    private Long nbCartes;
    private BigDecimal SortieEspeces;
    private Long nbSortieEspeces;
    private BigDecimal EntreeEspeces;
    private Long nbEntreeEspeces;
    private BigDecimal SortieAchats;
    private Long nbSortieAchats;
    private BigDecimal SortieDroitPlace;
    private Long nbSortieDroitPlace;
    private BigDecimal CAAutres;
    private Long nbAutres;
    private BigDecimal Taxes;
    private BigDecimal CATotal;
    private Date date;

    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING  , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC };

    public UserIndexDTO getVendeur() {
        return vendeur;
    }

    public void setVendeur(UserIndexDTO vendeur) {
        this.vendeur = vendeur;
    }

    public TariffAreasIndexDTO getPeriodeVente() {
        return periodeVente;
    }

    public void setPeriodeVente(TariffAreasIndexDTO periodeVente) {
        this.periodeVente = periodeVente;
    }

    public LocationsInfoDTO getPointVente() {
        return pointVente;
    }

    public void setPointVente(LocationsInfoDTO pointVente) {
        this.pointVente = pointVente;
    }

    public Date getOuverture() {
        return ouverture;
    }

    public void setOuverture(Date ouverture) {
        this.ouverture = ouverture;
    }

    public Date getFermeture() {
        return fermeture;
    }

    public void setFermeture(Date fermeture) {
        this.fermeture = fermeture;
    }

    public BigDecimal getCaisse_ouverture() {
        return caisse_ouverture;
    }

    public void setCaisse_ouverture(BigDecimal caisse_ouverture) {
        this.caisse_ouverture = caisse_ouverture;
    }

    public BigDecimal getCaisse_fermeture() {
        return caisse_fermeture;
    }

    public void setCaisse_fermeture(BigDecimal caisse_fermeture) {
        this.caisse_fermeture = caisse_fermeture;
    }

    public BigDecimal getCaisse_theorique() {
        return caisse_theorique;
    }

    public void setCaisse_theorique(BigDecimal caisse_theorique) {
        this.caisse_theorique = caisse_theorique;
    }

    public Long getNbTickets() {
        return nbTickets;
    }

    public void setNbTickets(Long nbTickets) {
        this.nbTickets = nbTickets;
    }

    public BigDecimal getCACheques() {
        return CACheques  == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : CACheques;
    }

    public void setCACheques(BigDecimal cACheques) {
        CACheques = cACheques;
    }

    public Long getNbCheques() {
        return nbCheques == null ? 0 : nbCheques;
    }

    public void setNbCheques(Long nbCheques) {
        this.nbCheques = nbCheques;
    }

    public BigDecimal getCAEspeces() {
        return CAEspeces == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : CAEspeces;
    }

    public void setCAEspeces(BigDecimal cAEspeces) {
        CAEspeces = cAEspeces;
    }

    public Long getNbEspeces() {
        return nbEspeces == null ? 0 : nbEspeces;
    }

    public void setNbEspeces(Long nbEspeces) {
        this.nbEspeces = nbEspeces;
    }

    public BigDecimal getCACartes() {
        return CACartes == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : CACartes;
    }

    public void setCACartes(BigDecimal cACartes) {
        CACartes = cACartes;
    }

    public Long getNbCartes() {
        return nbCartes == null ? 0 : nbCartes;
    }

    public void setNbCartes(Long nbCartes) {
        this.nbCartes = nbCartes;
    }

    public BigDecimal getSortieEspeces() {
        return SortieEspeces == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : SortieEspeces;
    }

    public void setSortieEspeces(BigDecimal sortieEspeces) {
        SortieEspeces = sortieEspeces;
    }

    public Long getNbSortieEspeces() {
        return nbSortieEspeces == null ? 0 : nbSortieEspeces;
    }

    public void setNbSortieEspeces(Long nbSortieEspeces) {
        this.nbSortieEspeces = nbSortieEspeces;
    }

    public BigDecimal getEntreeEspeces() {
        return EntreeEspeces == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : EntreeEspeces;
    }

    public void setEntreeEspeces(BigDecimal entreeEspeces) {
        EntreeEspeces = entreeEspeces;
    }

    public Long getNbEntreeEspeces() {
        return nbEntreeEspeces == null ? 0 : nbEntreeEspeces;
    }

    public void setNbEntreeEspeces(Long nbEntreeEspeces) {
        this.nbEntreeEspeces = nbEntreeEspeces;
    }

    public BigDecimal getSortieAchats() {
        return SortieAchats == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : SortieAchats;
    }

    public void setSortieAchats(BigDecimal sortieAchats) {
        SortieAchats = sortieAchats;
    }

    public Long getNbSortieAchats() {
        return nbSortieAchats == null ? 0 : nbSortieAchats;
    }

    public void setNbSortieAchats(Long nbSortieAchats) {
        this.nbSortieAchats = nbSortieAchats;
    }

    public BigDecimal getSortieDroitPlace() {
        return SortieDroitPlace == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) :  SortieDroitPlace;
    }

    public void setSortieDroitPlace(BigDecimal sortieDroitPlace) {
        SortieDroitPlace = sortieDroitPlace;
    }

    public Long getNbSortieDroitPlace() {
        return nbSortieDroitPlace == null ? 0 : nbSortieDroitPlace;
    }

    public void setNbSortieDroitPlace(Long nbSortieDroitPlace) {
        this.nbSortieDroitPlace = nbSortieDroitPlace;
    }

    public BigDecimal getCAAutres() {
        return CAAutres == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : CAAutres;
    }

    public void setCAAutres(BigDecimal cAAutres) {
        CAAutres = cAAutres;
    }

    public Long getNbAutres() {
        return nbAutres == null ? 0 : nbAutres;
    }

    public void setNbAutres(Long nbAutres) {
        this.nbAutres = nbAutres;
    }

    public BigDecimal getTaxes() {
        return Taxes == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) :  Taxes;
    }

    public void setTaxes(BigDecimal taxes) {
        Taxes = taxes;
    }

    public BigDecimal getCATotal() {
        return CATotal == null ? new BigDecimal(0).setScale(2,BigDecimal.ROUND_HALF_UP) : CATotal;
    }

    public void setCATotal(BigDecimal cATotal) {
        CATotal = cATotal;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        //Ventes par jour calendaire
        /*
        SELECT  t11.ID , t11.NAME , t1.PERSON , t1.TARIFFAREA , t10.NAME , t7.NAME , MIN(t4.DATESTART) , MAX(t4.DATEEND) , t4.OPENCASH , t4.CLOSECASH , t4.EXPECTEDCASH , COUNT(t1.ID) , COUNT(t0.ID) ,
        SUM(CASE WHEN t2.PAYMENT = 'cheque' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCHECK,
        SUM(CASE WHEN t2.PAYMENT = 'cheque' THEN 1 ELSE 0 END) as NBCHECK,
        SUM(CASE WHEN t2.PAYMENT = 'cash' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASH,
        SUM(CASE WHEN t2.PAYMENT = 'cash' THEN 1 ELSE 0 END) as NBCASH,
        SUM(CASE WHEN t2.PAYMENT = 'chequerefund' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCHECKR,
        SUM(CASE WHEN t2.PAYMENT = 'chequerefund' THEN 1 ELSE 0 END) as NBCHECKR,
        SUM(CASE WHEN t2.PAYMENT = 'cashrefund' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHR,
        SUM(CASE WHEN t2.PAYMENT = 'cashrefund' THEN 1 ELSE 0 END) as NBCASHR,
        SUM(CASE WHEN t2.PAYMENT = 'magcard' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCB,
        SUM(CASE WHEN t2.PAYMENT = 'magcard' THEN 1 ELSE 0 END) as NBCB,  
        SUM(CASE WHEN t2.PAYMENT = 'cashout' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHOUT, 
        SUM(CASE WHEN t2.PAYMENT = 'cashout' THEN 1 ELSE 0 END) as TOTCASHOUT, 
        SUM(CASE WHEN t2.PAYMENT = 'cashin' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHIN, 
        SUM(CASE WHEN t2.PAYMENT = 'cashin' THEN 1 ELSE 0 END) as TOTCASHIN, 
        SUM(CASE WHEN t2.PAYMENT = 'buyout' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTBUYOUT, 
        SUM(CASE WHEN t2.PAYMENT = 'buyout' THEN 1 ELSE 0 END) as NBBUYOUT, 
        SUM(CASE WHEN t2.PAYMENT = 'place' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTPLACE, 
        SUM(CASE WHEN t2.PAYMENT = 'place' THEN 1 ELSE 0 END) as NBPLACE, 
        SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTOTHER ,
        SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) THEN 1 ELSE 0 END) as NBOTHER ,
        ( SELECT  SUM(AMOUNT) FROM TAXLINES T , receipts t904 WHERE t904.MONEY = t4.MONEY AND T.RECEIPT = t904.ID ) as TOTTAXES ,
        SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTTTC 
        FROM   CLOSEDCASH t4  , CASHREGISTERS t5 , LOCATIONS t11 , 
        RECEIPTS t0 LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) LEFT JOIN PEOPLE t7 ON (t7.ID = t1.PERSON ) LEFT JOIN TARIFFAREAS t10 ON (t10.ID = t1.TARIFFAREA) 
        LEFT JOIN PAYMENTS t21 ON t2.RECEIPT = t0.ID 
        WHERE  t5.ID = t4.CASHREGISTER_ID  AND  t11.ID = t5.LOCATION_ID  AND  t0.MONEY = t4.MONEY  
        AND  (t0.DATENEW >= '2016-07-04') AND  (t0.DATENEW <= '2016-07-05 23:59') GROUP BY YEAR(t0.DATENEW) , MONTH(t0.DATENEW) , DAY(t0.DATENEW) , t5.LOCATION_ID  
        ORDER BY YEAR(t0.DATENEW) DESC, MONTH(t0.DATENEW) DESC, DAY(t0.DATENEW) DESC, t5.LOCATION_ID DESC ;
         */
        /*Jour de vente ? */
        DtoSQLParameters retour = new DtoSQLParameters();
        retour.select = "SELECT t11.NAME , t11.ID , t14.NAME , t11.PARENT_LOCATION_ID , "
                + "( SELECT TICKETS.PERSON  FROM  RECEIPTS , TICKETS WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID LIMIT 1 ), "
                + "( SELECT PEOPLE.NAME  FROM  RECEIPTS , TICKETS, PEOPLE WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID AND PEOPLE.ID = TICKETS.PERSON  LIMIT 1 ), "
                + "( SELECT PEOPLE.NOM  FROM  RECEIPTS , TICKETS, PEOPLE WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID AND PEOPLE.ID = TICKETS.PERSON  LIMIT 1 ), "
                + "( SELECT PEOPLE.PRENOM FROM  RECEIPTS , TICKETS, PEOPLE WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID AND PEOPLE.ID = TICKETS.PERSON  LIMIT 1 ), "
                /**
                 * Periode de vente Appliquée
                 * Choix JLA : Période effective uniquement
                 */
                + "( SELECT TICKETS.TARIFFAREA FROM  RECEIPTS , TICKETS WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID LIMIT 1 ), "
                + "( SELECT TARIFFAREAS.NAME  FROM  RECEIPTS , TICKETS, TARIFFAREAS WHERE RECEIPTS.MONEY = t4.MONEY AND TICKETS.ID = RECEIPTS.ID AND TARIFFAREAS.ID = TICKETS.TARIFFAREA LIMIT 1 ), "
                
                /**
                 * Periode de vente Théorique
                + "( SELECT TARIFFAREAS.ID FROM TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_ID, " 
                + "( SELECT TARIFFAREAS.NAME  FROM  TARIFFAREAS_LOCATION , TARIFFAREAS WHERE TARIFFAREAS_LOCATION.LOCATION_ID = t11.ID AND TARIFFAREAS_LOCATION.DATE_FROM <= t0.DATENEW AND ( ISNULL(TARIFFAREAS_LOCATION.DATE_TO) OR TARIFFAREAS_LOCATION.DATE_TO >= t0.DATENEW ) AND TARIFFAREAS.ID = TARIFFAREAS_LOCATION.TARIFFAREAS_ID AND TARIFFAREAS.TARIFFORDER = 100 LIMIT 1 ) AS TH_PERIODE_NAME, "  
                */
                + " MIN(t4.DATESTART) , MAX(t4.DATEEND) , t4.OPENCASH , t4.CLOSECASH , t4.EXPECTEDCASH , COUNT(t1.ID) , "
                + "SUM(CASE WHEN t2.PAYMENT = 'cheque' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCHECK, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cheque' THEN 1 ELSE 0 END) as NBCHECK, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cash' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASH, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cash' THEN 1 ELSE 0 END) as NBCASH, "
                + "SUM(CASE WHEN t2.PAYMENT = 'chequerefund' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCHECKR, "
                + "SUM(CASE WHEN t2.PAYMENT = 'chequerefund' THEN 1 ELSE 0 END) as NBCHECKR, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cashrefund' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHR, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cashrefund' THEN 1 ELSE 0 END) as NBCASHR, "
                + "SUM(CASE WHEN t2.PAYMENT = 'magcard' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCB, "
                + "SUM(CASE WHEN t2.PAYMENT = 'magcard' THEN 1 ELSE 0 END) as NBCB, "  
                + "SUM(CASE WHEN t2.PAYMENT = 'cashout' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHOUT, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cashout' THEN 1 ELSE 0 END) as TOTCASHOUT, " 
                + "SUM(CASE WHEN t2.PAYMENT = 'cashin' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTCASHIN, "
                + "SUM(CASE WHEN t2.PAYMENT = 'cashin' THEN 1 ELSE 0 END) as TOTCASHIN, "
                + "SUM(CASE WHEN t2.PAYMENT = 'buyout' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTBUYOUT, "
                + "SUM(CASE WHEN t2.PAYMENT = 'buyout' THEN 1 ELSE 0 END) as NBBUYOUT, "
                + "SUM(CASE WHEN t2.PAYMENT = 'place' THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTPLACE, "
                + "SUM(CASE WHEN t2.PAYMENT = 'place' THEN 1 ELSE 0 END) as NBPLACE, "
                + "SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTOTHER , "
                + "SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) THEN 1 ELSE 0 END) as NBOTHER , "
                + "TOTTAXES , "
                + "SUM(CASE WHEN t2.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) THEN ROUND(t2.TOTAL,2) ELSE 0 END) as TOTTTC , "
                +" t0.DATENEW "
                + "FROM ";

        //TODO 
        //Le calcul de total HT a changé mais n'est pas non plus totalement satisfaisant 
        //car une restriction supérieur au group by ne sera pas prise en compte pour le calcul
        
        if(retour.tableHash.put("t4", "CLOSEDCASH") == null) {
            retour.tableList.add(" CLOSEDCASH t4 ");
        }

        if(retour.tableHash.put("t5", "CASHREGISTERS") == null) {
            retour.tableList.add(" CASHREGISTERS t5 ");
            retour.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
        }

        if(retour.tableHash.put("t11", "LOCATION") == null) {
            retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON t14.ID = t11.CATEG_ID ");
            retour.constraintList.add(" t11.ID = t5.LOCATION_ID ");
            retour.tableHash.put("t14", "CATEGORIES");
        }

        if(retour.tableHash.put("t0", "RECEIPTS") == null) {
            retour.tableList.add(" RECEIPTS t0 "
                    + "LEFT JOIN TICKETS t1 ON (t1.ID = t0.ID) "
                    + "LEFT JOIN PAYMENTS t2 ON (t2.RECEIPT = t0.ID) "
                    + "LEFT JOIN (SELECT DATE(t00.DATENEW) AS DATENEW, t01.CASHREGISTER_ID, AMOUNT AS TOTTAXES FROM TAXLINES T, receipts t00, CLOSEDCASH t01 %s WHERE t00.MONEY = t01.MONEY AND T.RECEIPT = t00.ID %s GROUP BY DATE(t00.DATENEW) , t01.CASHREGISTER_ID %s) TMPTAXES ON TMPTAXES.DATENEW = date(t0.DATENEW) ");
            retour.tableHash.put("t1", "TICKETS");
            retour.constraintList.add(" t0.MONEY = t4.MONEY ");
            retour.constraintList.add(" TMPTAXES.CASHREGISTER_ID = t5.ID ");
        }
        
//        if(retour.tableHash.put("TMPTAXES", "(SELECT DATE(t904.DATENEW) AS DATENEW, t905.CASHREGISTER_ID, SUM(AMOUNT) AS TOTTAXES FROM TAXLINES T, receipts t904, CLOSEDCASH t905 WHERE t904.DATENEW >= ? AND t904.MONEY = t905.MONEY AND T.RECEIPT = t904.ID GROUP BY DATE(t904.DATENEW) , t905.CASHREGISTER_ID)") == null) {
//            retour.tableList.add(" (SELECT DATE(t904.DATENEW) AS DATENEW, t905.CASHREGISTER_ID, SUM(AMOUNT) AS TOTTAXES FROM TAXLINES T, receipts t904, CLOSEDCASH t905 WHERE t904.DATENEW >= ? AND t904.MONEY = t905.MONEY AND T.RECEIPT = t904.ID GROUP BY DATE(t904.DATENEW) , t905.CASHREGISTER_ID) TMPTAXES ");
//            retour.constraintList.add(" TMPTAXES.DATENEW = DATE(t0.DATENEW) ");
//        }

        //EDU : Pb avec des ouvertures sans tickets / mouvements
        retour.constraintList.add(" ( SELECT COUNT(TICKETS.ID) FROM RECEIPTS , TICKETS  WHERE TICKETS.ID = RECEIPTS.ID AND RECEIPTS.MONEY = t4.MONEY) > 1 ");

        retour.groupBy = "YEAR(t0.DATENEW) , MONTH(t0.DATENEW) , DAY(t0.DATENEW) , t5.LOCATION_ID";

        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        boolean retour = false;
        if((array.length - starting_index) >= 39) {
            try {


                //PdV
                if(array[0+starting_index] != null) {
                    setPointVente(new LocationsInfoDTO());
                    pointVente.loadFromObjectArray(array, 0+starting_index);
                }
                //Vendeur
                if(array[4+starting_index] != null) {
                    setVendeur(new UserIndexDTO());
                    vendeur.loadFromObjectArray(array, 4+starting_index);
                }
                //Periode
                if(array[8+starting_index] != null) {
                    setPeriodeVente(new TariffAreasIndexDTO());
                    periodeVente.loadFromObjectArray(array, 8+starting_index);
                }

                setOuverture((Date) array[10+starting_index]);
                if(array[11+starting_index] != null ) {
                    setFermeture((Date) array[11+starting_index]);
                }

                if(array[12+starting_index] != null ) {
                    setCaisse_ouverture( new BigDecimal((double)array[12+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[13+starting_index] != null ) {
                    setCaisse_fermeture( new BigDecimal((double)array[13+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[14+starting_index] != null ) {
                    setCaisse_theorique( new BigDecimal((double)array[14+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[15+starting_index] != null ) {
                    setNbTickets( (long) array[15+starting_index]);
                }
                if(array[16+starting_index] != null ) {
                    setCACheques( new BigDecimal((double)array[16+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[17+starting_index] != null ) {

                    setNbCheques( array[17+starting_index] instanceof BigDecimal ? ((BigDecimal)array[17+starting_index]).longValue() : (long) array[17+starting_index]);
                }
                if(array[18+starting_index] != null ) {
                    setCAEspeces( new BigDecimal((double)array[18+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[19+starting_index] != null ) {
                    setNbEspeces( array[19+starting_index] instanceof BigDecimal ? ((BigDecimal)array[19+starting_index]).longValue() : (long) array[19+starting_index]);
                }
                if(array[20+starting_index] != null ) {
                    setCACheques(getCACheques().add(new BigDecimal((double)array[20+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP)));
                }
                if(array[21+starting_index] != null ) {
                    setNbCheques(getNbCheques() + (array[21+starting_index] instanceof BigDecimal ? ((BigDecimal)array[21+starting_index]).longValue() : (long) array[21+starting_index]));
                }
                if(array[22+starting_index] != null ) {
                    setCAEspeces( new BigDecimal((double)array[22+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP).add(getCAEspeces()));
                }
                if(array[23+starting_index] != null ) {
                    setNbEspeces(getNbEspeces() + (array[23+starting_index] instanceof BigDecimal ? ((BigDecimal)array[23+starting_index]).longValue() :(long) array[23+starting_index]));
                }
                //TODO
                //              retour chk retour esp
                if(array[24+starting_index] != null ) {
                    setCACartes(new BigDecimal((double)array[24+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[25+starting_index] != null ) {
                    setNbCartes( array[25+starting_index] instanceof BigDecimal ? ((BigDecimal)array[25+starting_index]).longValue() : (long) array[25+starting_index]);
                }
                if(array[26+starting_index] != null ) {
                    setSortieEspeces( new BigDecimal((double)array[26+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[27+starting_index] != null ) {
                    setNbSortieEspeces( array[27+starting_index] instanceof BigDecimal ? ((BigDecimal)array[27+starting_index]).longValue() :(long) array[27+starting_index]);
                }
                if(array[28+starting_index] != null ) {
                    setEntreeEspeces( new BigDecimal((double)array[28+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[29+starting_index] != null ) {
                    setNbEntreeEspeces( array[29+starting_index] instanceof BigDecimal ? ((BigDecimal)array[29+starting_index]).longValue() :(long) array[29+starting_index]);
                }
                if(array[30+starting_index] != null ) {
                    setSortieAchats( new BigDecimal((double)array[30+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[31+starting_index] != null ) {
                    setNbSortieAchats(array[31+starting_index] instanceof BigDecimal ? ((BigDecimal)array[31+starting_index]).longValue() :(long) array[31+starting_index]);
                }
                if(array[32+starting_index] != null ) {
                    setSortieDroitPlace( new BigDecimal((double)array[32+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[33+starting_index] != null ) {
                    setNbSortieDroitPlace( array[33+starting_index] instanceof BigDecimal ? ((BigDecimal)array[33+starting_index]).longValue() :(long) array[33+starting_index]);
                }
                if(array[34+starting_index] != null ) {
                    setCAAutres( new BigDecimal((double)array[34+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[35+starting_index] != null ) {
                    setNbAutres( array[35+starting_index] instanceof BigDecimal ? ((BigDecimal)array[35+starting_index]).longValue() :(long) array[35+starting_index]);
                }
                if(array[36+starting_index] != null ) {
                    setTaxes( new BigDecimal((double)array[36+starting_index]).setScale(5,BigDecimal.ROUND_HALF_UP));
                }
                if(array[37+starting_index] != null ) {
                    setCATotal( new BigDecimal((double)array[37+starting_index]).setScale(2,BigDecimal.ROUND_HALF_UP));
                }
                if(array[38+starting_index] != null ) {
                    setDate( (Date)array[38+starting_index]);
                }
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                retour = false;
            }
        }

        return retour;
    }

    @Override
    public String[] header() {
        return  new String[]{ "Date" , "Numéro point de vente" , "Nom Point de Vente" , "Catégorie Point de Vente" ,
                "Numéro Vendeur" , "Nom Caisse Vendeur" , "Nom Vendeur" , "Prénom Vendeur" ,
                "Num Periode de Vente" , "Nom période de Vente" ,
                "Ouverture" , "Fermeture" , "Fond de caisse Initial" , "Fond de Caisse Final" , "Fond de Caisse Théorique" ,
                "Nb Tickets" , "CA Chèques" , "Nb Chèques" , "CA Espèces" , "Nb Espèces" , "Ca Cartes" , "Nb Cartes" ,
                "Sortie Espèces" , "Nb Sorties Espèces" , "Entrée Espèces" , "Nb Entrées Espèces" , 
                "Sortie Achats" , "Nb Sorties Achats" , "Sortie Droit de Place" , "Nb Sorties Droit de Place" , 
                "CA Autres" , "Nb CA Autres" , "Taxes" , "CA Total TTC" , "CA Total HT" };
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        DecimalFormat entier = new DecimalFormat("0", otherSymbols);
        DecimalFormat moneylong = new DecimalFormat("0.00####", otherSymbols);

        return new String[] { 
                df.format(getDate()) ,
                getPointVente().getId() , getPointVente().getLabel() ,  getPointVente().getCategory() , 
                getVendeur().getId() , getVendeur().getNomCaisse() == null ? "" : getVendeur().getNomCaisse() , getVendeur().getNom() == null ? "" : getVendeur().getNom() , getVendeur().getPrenom() == null ? "" : getVendeur().getPrenom() , 
                        getPeriodeVente() == null ? "" : getPeriodeVente().getId() , getPeriodeVente() == null ? "" : getPeriodeVente().getNom() ,
                                getOuverture() == null ? "" : hdf.format(getOuverture()) , getFermeture() == null ? "" : hdf.format(getFermeture()) ,
                                        getCaisse_ouverture() == null ? "0" : money.format(getCaisse_ouverture())  , getCaisse_fermeture() == null ? "0" : money.format(getCaisse_fermeture()) , getCaisse_theorique() == null ? "0" : money.format(getCaisse_theorique()) , 
                                                getNbTickets() == null ? "0" : entier.format(getNbTickets()) , money.format(getCACheques()) , entier.format(getNbCheques()) , money.format(getCAEspeces()) , entier.format(getNbEspeces()) ,
                                                        money.format(getCACartes()) , entier.format(getNbCartes()) , money.format(getSortieEspeces()) , entier.format(getNbSortieEspeces()) ,
                                                        money.format(getEntreeEspeces()) , entier.format(getNbEntreeEspeces()) , money.format(getSortieAchats()) , entier.format(getNbSortieAchats()) ,
                                                        money.format(getSortieDroitPlace()) , entier.format(getNbSortieDroitPlace()) , money.format(getCAAutres()) , entier.format(getNbAutres()) ,
                                                        moneylong.format(getTaxes()) , money.format(getCATotal()) , moneylong.format(getCATotal().doubleValue() - getTaxes().doubleValue())     
        };
    }

}
