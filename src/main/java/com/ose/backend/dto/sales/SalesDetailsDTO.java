package com.ose.backend.dto.sales;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.pasteque.api.response.ICsvResponse;

public class SalesDetailsDTO implements ICsvResponse {

    private String dimension;
    private double turnover;
    private int salesCount;
    private double averageBasket;

    public double getTurnover() {
        return turnover;
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    public int getSalesCount() {
        return salesCount;
    }

    public void setSalesCount(int salesCount) {
        this.salesCount = salesCount;
    }

    public double getAverageBasket() {
        return averageBasket;
    }

    public void setAverageBasket(double averageBasket) {
        this.averageBasket = averageBasket;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Override
    public String[] header() {
        return new String[] { "Dimension", "Chiffre d'affaires", "Nombre de ventes", "Panier moyen" };
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] { getDimension(), decf.format(getTurnover()), String.valueOf(getSalesCount()),
                decf.format(getAverageBasket()) };
    }

}
