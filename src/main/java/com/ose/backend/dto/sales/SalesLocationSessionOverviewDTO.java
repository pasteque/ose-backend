package com.ose.backend.dto.sales;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.SalesLocationIndexDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.ICsvResponse;



public class SalesLocationSessionOverviewDTO implements ICsvResponse, ILoadable {
    
    private TourIndexDTO tournee;
    private SalesLocationIndexDTO deballage;
    private SalesDayOverviewDTO recapVentes;
    
    public static CellType[] types = { 
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING  , CellType.STRING , CellType.STRING ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.NUMERIC , 
        CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING  , CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING ,  CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC };    
//Sessions de vente agrégées par déballage
    /*
    SELECT t3.* , t2.* , t4.ID , t4.NAME , t5.ID , t5.NOM , COUNT(t7.ID) , t8.ID , t8.NAME , t7.TARIFFAREA , t9.NAME ,
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cheque' AND P.RECEIPT = t904.ID ) as TOTCHK, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cheque' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0 ) as NBCHK, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cash' AND P.RECEIPT = t904.ID ) as TOTCASH, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cash' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASH, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'chequerefund' AND P.RECEIPT = t904.ID ) as TOTCHKR, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'chequerefund' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0 ) as NBCHKR, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashrefund' AND P.RECEIPT = t904.ID ) as TOTCASHR, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashrefund' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHR, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'magcard' AND P.RECEIPT = t904.ID) as TOTCB,
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'magcard' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCB,
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashout' AND P.RECEIPT = t904.ID ) as TOTCASHOUT, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashout' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHOUT, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashin' AND P.RECEIPT = t904.ID ) as TOTCASHIN, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashin' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHIN,
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'buyout' AND P.RECEIPT = t904.ID ) as TOTBUYOUT, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'buyout' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBBUYOUT, 
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'place' AND P.RECEIPT = t904.ID ) as TOTPLACE, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'place' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBPLACE,
    ( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) AND P.RECEIPT = t904.ID ) as TOTOTHER, 
    ( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBOTHER,
    ( SELECT  SUM(AMOUNT) FROM TAXLINES T , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND T.RECEIPT = t904.ID ) as TOTTAXES 
    FROM deballages t1 , closedcash t2 , cashregisters t3 , locations t4 , tournees t5 , receipts t6 LEFT JOIN tickets t7 ON ( t7.ID = t6.ID ) LEFT JOIN people t8 ON (t8.ID = t7.PERSON ) LEFT JOIN tariffareas t9 on t9.ID = t7.TARIFFAREA
    WHERE DEBUT > '2016-07-01' AND FIN < '2016-07-20 23:59' 
    AND t2.DEBALLAGE_ID = t3.ID AND t3.ID = t2.CASHREGISTER_ID AND t4.ID = t3.LOCATION_ID AND t5.ID = t3.TOURNEES_ID AND t6.MONEY = t2.MONEY
    GROUP BY t3.ID;
    
    */

    public TourIndexDTO getTournee() {
        return tournee;
    }

    public void setTournee(TourIndexDTO tournee) {
        this.tournee = tournee;
    }

    public SalesLocationIndexDTO getDeballage() {
        return deballage;
    }

    public void setDeballage(SalesLocationIndexDTO deballage) {
        this.deballage = deballage;
    }

    public SalesDayOverviewDTO getRecapVentes() {
        return recapVentes;
    }

    public void setRecapVentes(SalesDayOverviewDTO recapVentes) {
        this.recapVentes = recapVentes;
    }
    
    @Override
    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();
        /**
         * Attention période de vente réelle  = au ticket
         * Choix JLA plutôt que période de vente théorique
         */
        retour.select = "SELECT t3.ID , t3.NUM_INSEE , t3.DEBUT , t3.FIN , t3.NB_PUB , t3.DROIT_PLACE , t3.PAYE , t12.ZIPCODE , t12.COMMUNE , t12.LATITUDE , t12.LONGITUDE ,"
     + " t9.ID , t9.NOM , t11.NAME , t11.ID , t14.NAME , t11.PARENT_LOCATION_ID , t1.PERSON , t7.NAME ,t7.NOM , t7.PRENOM , t1.TARIFFAREA , t10.NAME , MIN(t4.DATESTART) , MAX(t4.DATEEND) , t4.OPENCASH , t4.CLOSECASH , t4.EXPECTEDCASH , COUNT(t1.ID) , "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cheque' AND P.RECEIPT = t904.ID ) as TOTCHK, " 
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cheque' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0 ) as NBCHK, "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cash' AND P.RECEIPT = t904.ID ) as TOTCASH, " 
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cash' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASH, " 
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'chequerefund' AND P.RECEIPT = t904.ID ) as TOTCHKR, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'chequerefund' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0 ) as NBCHKR, " 
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashrefund' AND P.RECEIPT = t904.ID ) as TOTCASHR, " 
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashrefund' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHR, " 
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'magcard' AND P.RECEIPT = t904.ID) as TOTCB, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'magcard' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCB, "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashout' AND P.RECEIPT = t904.ID ) as TOTCASHOUT, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashout' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHOUT, " 
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashin' AND P.RECEIPT = t904.ID ) as TOTCASHIN, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'cashin' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBCASHIN, "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'buyout' AND P.RECEIPT = t904.ID ) as TOTBUYOUT, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'buyout' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBBUYOUT, " 
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'place' AND P.RECEIPT = t904.ID ) as TOTPLACE, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT = 'place' AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBPLACE, "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) AND P.RECEIPT = t904.ID ) as TOTOTHER, "
     + "( SELECT  COUNT(P.ID) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' , 'magcard' , 'cash' , 'cashrefund' , 'cheque' , 'chequerefund' ) AND P.RECEIPT = t904.ID AND P.TOTAL <> 0) as NBOTHER, "
     + "( SELECT  SUM(AMOUNT) FROM TAXLINES T , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND T.RECEIPT = t904.ID ) as TOTTAXES, "
     + "( SELECT  SUM(ROUND(TOTAL,2)) FROM PAYMENTS P , closedcash t902 , receipts t904 WHERE t902.DEBALLAGE_ID = t3.ID AND t904.MONEY = t902.MONEY AND P.PAYMENT NOT IN ( 'place' , 'buyout' , 'cashin' , 'cashout' ) AND P.RECEIPT = t904.ID ) as TOTTTC , t0.DATENEW "
     + "FROM ";

                if(retour.tableHash.put("t3", "DEBALLAGES") == null) {
                    retour.tableList.add(" DEBALLAGES t3 ");
                }
                
                if(retour.tableHash.put("t4", "CLOSEDCASH") == null) {
                    retour.tableList.add(" CLOSEDCASH t4 ");
                    retour.constraintList.add(" t4.DEBALLAGE_ID = t3.ID ");
                }
        
                if(retour.tableHash.put("t5", "CASHREGISTERS") == null) {
                    retour.tableList.add(" CASHREGISTERS t5 ");
                    retour.constraintList.add(" t5.ID = t4.CASHREGISTER_ID ");
                }

                if(retour.tableHash.put("t11", "LOCATION") == null) {
                    retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON t14.ID = t11.CATEG_ID ");
                    retour.constraintList.add(" t11.ID = t5.LOCATION_ID ");
                    retour.tableHash.put("t14", "CATEGORIES");
                }
        
                if(retour.tableHash.put("t9", "TOURNEES") == null) {
                    retour.tableList.add(" TOURNEES t9 ");
                    retour.constraintList.add(" t9.ID = t3.TOURNEES_ID ");
                }
                
                if(retour.tableHash.put("t12", "INSEE") == null) {
                    retour.tableList.add(" INSEE t12 ");
                    retour.constraintList.add(" t12.NUM_INSEE = t3.NUM_INSEE ");
                }
                
                if(retour.tableHash.put("t0", "RECEIPTS") == null) {
                    retour.tableList.add(" RECEIPTS t0 ");
                    retour.constraintList.add(" t0.MONEY = t4.MONEY ");
                }
                
                if(retour.tableHash.put("t1", "TICKETS") == null) {
                    retour.tableList.add(" TICKETS t1 ");
                    retour.constraintList.add(" t1.ID = t0.ID ");
                }
                
                if(retour.tableHash.put("t7", "PEOPLE") == null) {
                    retour.tableList.add(" PEOPLE t7 ");
                    retour.constraintList.add(" t7.ID = t1.PERSON ");
                }
                
                if(retour.tableHash.put("t10", "TARIFFAREAS") == null) {
                    retour.tableList.add(" TARIFFAREAS t10 ");
                    retour.constraintList.add(" t10.ID = t1.TARIFFAREA ");
                }
               
                retour.groupBy = "t3.ID ";
        
        return retour;
    }



    @Override
    public boolean loadFromObjectArray(Object[] array , int starting_index) {
        // TODO Auto-generated method stub
        boolean retour = false;
        if((array.length - starting_index) >= 52) {
            try {
                retour = true;
                //Deballage
                if(array[0+starting_index] != null) {
                    setDeballage(new SalesLocationIndexDTO());
                    retour = retour && deballage.loadFromObjectArray(array, 0+starting_index);
                }
                //Tournee
                if(array[11+starting_index] != null) {
                    setTournee(new TourIndexDTO());
                    retour = retour && tournee.loadFromObjectArray(array, 11+starting_index);
                }
                //Recap
                if(array[13+starting_index] != null) {
                    setRecapVentes(new SalesDayOverviewDTO());
                    retour = retour && recapVentes.loadFromObjectArray(array, 13+starting_index);
                }
                
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                retour = false;
            }
        }
        return retour;
    }

    @Override
    public String[] header() {
        String[] tmp =   new String[]{ "Id déballage" , "Insee Déballage" , "Code Postal Déballage" , "Commune Déballage" ,
                "Latitude Déballage" , "Longitude Déballage" , "Ouverture déballage" , "Fermeture Déballage" , 
                "Nombre de Pub" , "Droit de Place" , "Numéro Tournée" , "Nom Tournée" ,  
                "Ratio" , "Fréquentation" 
                };
        
        ArrayList<String> tmpList = new ArrayList<String>(Arrays.asList(tmp));
        tmpList.addAll(Arrays.asList(recapVentes.header()));
        
        return tmpList.toArray(new String[tmpList.size()]) ;
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        DecimalFormat moneylong = new DecimalFormat("0.00######", otherSymbols);
        
        String[] tmp = new String[] {getDeballage() == null ? "" : getDeballage().getId().toString() , getDeballage() == null ? "" : getDeballage().getCommune().getInsee() , getDeballage() == null ? "" : getDeballage().getCommune().getZipCode() , getDeballage() == null ? "" : getDeballage().getCommune().getCommune() , getDeballage() == null ? "" : getDeballage().getCommune().getLatitude().toString() , getDeballage() == null ? "" : getDeballage().getCommune().getLongitude().toString() , 
                getDeballage() == null ? "" : hdf.format(getDeballage().getDateHeureOuverture()) , getDeballage() == null ? "" : hdf.format(getDeballage().getDateHeureFermeture()) , getDeballage() == null ? "" : String.valueOf(getDeballage().getNombrePub()) , getDeballage() == null || getDeballage().getDroitPlace() == 0D ? "" : money.format(getDeballage().getDroitPlace()) ,
                getTournee() == null ? "" : getTournee().getId().toString() , getTournee() == null ? "" : getTournee().getNom() ,                   
                getDeballage().getNombrePub() > 0 ? moneylong.format( recapVentes.getCATotal().doubleValue() / getDeballage().getNombrePub()) : "" , getDeballage().getNombrePub() > 0 ? moneylong.format((double) (recapVentes.getNbTickets() * 1000) / getDeballage().getNombrePub()) : ""};
        ArrayList<String> tmpList = new ArrayList<String>(Arrays.asList(tmp));
        tmpList.addAll(Arrays.asList(recapVentes.CSV()));
        return tmpList.toArray(new String[tmpList.size()]) ;
    }

}
