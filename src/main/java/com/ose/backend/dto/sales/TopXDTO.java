package com.ose.backend.dto.sales;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.response.IXlsResponse;

public class TopXDTO implements IXlsResponse {

    private String productCode;
    private String productLabel;
    private double units;
    
    public static CellType[] types = { CellType.STRING , CellType.STRING ,  CellType.NUMERIC  };

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    @Override
    public String[] header() {
        return new String[] { "Code", "Nom", "Valeur" };
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.##", otherSymbols);
        return new String[] { getProductCode(), getProductLabel(), decf.format(getUnits()) };
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
