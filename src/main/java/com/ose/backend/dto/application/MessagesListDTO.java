package com.ose.backend.dto.application;

import java.util.List;

public class MessagesListDTO {

        List<MessagesDTO> messages;
        String locationId;
        String productId;
        String userId;
        String typeId;
        
        public List<MessagesDTO> getMessages() {
            return messages;
        }
        public void setMessages(List<MessagesDTO> messages) {
            this.messages = messages;
        }
        public String getLocationId() {
            return locationId;
        }
        public void setLocationId(String locationId) {
            this.locationId = locationId;
        }
        public String getProductId() {
            return productId;
        }
        public void setProductId(String productId) {
            this.productId = productId;
        }
        public String getUserId() {
            return userId;
        }
        public void setUserId(String userId) {
            this.userId = userId;
        }
        public String getTypeId() {
            return typeId;
        }
        public void setTypeId(String typeId) {
            this.typeId = typeId;
        }
}
