package com.ose.backend.dto.application;

public class SupervisionIntervalDTO {

    private long ordersInterval;
    private long detailsInterval;

    public SupervisionIntervalDTO(long ordersInterval, long detailsInterval) {
        this.ordersInterval = ordersInterval;
        this.detailsInterval = detailsInterval;
    }

    public long getOrdersInterval() {
        return ordersInterval;
    }

    public void setOrdersInterval(long ordersInterval) {
        this.ordersInterval = ordersInterval;
    }

    public long getDetailsInterval() {
        return detailsInterval;
    }

    public void setDetailsInterval(long detailsInterval) {
        this.detailsInterval = detailsInterval;
    }

}
