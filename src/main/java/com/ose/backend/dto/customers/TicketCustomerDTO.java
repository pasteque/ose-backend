package com.ose.backend.dto.customers;

public class TicketCustomerDTO {

    private String ticketNumber;
    private String customerSearchKey;

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketId) {
        this.ticketNumber = ticketId;
    }

    public String getCustomerSearchKey() {
        return customerSearchKey;
    }

    public void setCustomerSearchKey(String customerId) {
        this.customerSearchKey = customerId;
    }

}
