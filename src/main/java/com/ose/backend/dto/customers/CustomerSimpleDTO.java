package com.ose.backend.dto.customers;

public class CustomerSimpleDTO {

    private String customersLabel;
    private String customersId;
    
    
    public String getCustomersLabel() {
        return customersLabel;
    }
    public void setCustomersLabel(String customersLabel) {
        this.customersLabel = customersLabel;
    }
    public String getCustomersId() {
        return customersId;
    }
    public void setCustomersId(String customersId) {
        this.customersId = customersId;
    }
    
    
}
