package com.ose.backend.dto.stocks;

import java.util.Date;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;

public class StockDiaryIndexDTO {
    
    private String id;
    private Date date;
    private LocationsInfoDTO lieuStockage;
    private LocationsInfoDTO destination;
    private ProductsIndexDTO produit;
    private int codeMouvement;
    private String libelleMouvement;
    private Double quantiteAvant;
    private double quantiteMouvement;
    private double prixRevient;
    private String note;
    private String numPiece;
    private String typePiece;
    private String etat;

    public LocationsInfoDTO getDestination() {
        return destination;
    }
    public void setDestination(LocationsInfoDTO destination) {
        this.destination = destination;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public int getCodeMouvement() {
        return codeMouvement;
    }
    public void setCodeMouvement(int codeMouvement) {
        this.codeMouvement = codeMouvement;
    }

    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public LocationsInfoDTO getLieuStockage() {
        return lieuStockage;
    }
    public void setLieuStockage(LocationsInfoDTO lieuStockage) {
        this.lieuStockage = lieuStockage;
    }
    public ProductsIndexDTO getProduit() {
        return produit;
    }
    public void setProduit(ProductsIndexDTO produit) {
        this.produit = produit;
    }
    public String getLibelleMouvement() {
        return libelleMouvement;
    }
    public void setLibelleMouvement(String libelleMouvement) {
        this.libelleMouvement = libelleMouvement;
    }
    public Double getQuantiteAvant() {
        return quantiteAvant;
    }
    public void setQuantiteAvant(Double quantiteAvant) {
        this.quantiteAvant = quantiteAvant;
    }
    public double getQuantiteMouvement() {
        return quantiteMouvement;
    }
    public void setQuantiteMouvement(double quantiteMouvement) {
        this.quantiteMouvement = quantiteMouvement;
    }
    public double getPrixRevient() {
        return prixRevient;
    }
    public void setPrixRevient(double prixRevient) {
        this.prixRevient = prixRevient;
    }
    public String getNumPiece() {
        return numPiece;
    }
    public void setNumPiece(String numPiece) {
        this.numPiece = numPiece;
    }
    public String getEtat() {
        return etat;
    }
    public void setEtat(String etat) {
        this.etat = etat;
    }
    public String getTypePiece() {
        return typePiece;
    }
    public void setTypePiece(String typePiece) {
        this.typePiece = typePiece;
    }
    
}
