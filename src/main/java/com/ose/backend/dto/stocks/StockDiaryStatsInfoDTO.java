package com.ose.backend.dto.stocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.ose.backend.dto.utils.DateTimeAggregDTO;
import com.ose.backend.dto.utils.GraphDataDTO;

public class StockDiaryStatsInfoDTO {

    private ArrayList<StockDiaryStatsDTO> details;
    private ArrayList<StockDiaryStatsLocDTO> perLocations;
    private ArrayList<GraphDataDTO> locationsGraph;
    private ArrayList<GraphDataDTO> detailsGraph;
    
    private String _group;
    private Date startDate;
    private Date endDate;
    
    public ArrayList<StockDiaryStatsDTO> getDetails() {
        return details;
    }
    public void setDetails(ArrayList<StockDiaryStatsDTO> details) {
        this.details = details;
    }
    public ArrayList<StockDiaryStatsLocDTO> getPerLocations() {
        return perLocations;
    }
    public void setPerLocations(ArrayList<StockDiaryStatsLocDTO> perLocations) {
        this.perLocations = perLocations;
    }
    public String get_group() {
        return _group;
    }
    public void set_group(String _group) {
        this._group = _group;
    }
    
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public ArrayList<GraphDataDTO> getLocationsGraph() {
        return locationsGraph;
    }
    public void setLocationsGraph(ArrayList<GraphDataDTO> locationsGraph) {
        this.locationsGraph = locationsGraph;
    }
    public ArrayList<GraphDataDTO> getDetailsGraph() {
        return detailsGraph;
    }
    public void setDetailsGraph(ArrayList<GraphDataDTO> detailsGraph) {
        this.detailsGraph = detailsGraph;
    }
    /**
     *  
     * De base on a pour chaque article le stock courant or dans bien des cas il est préférable de disposer du stock à la fin de la période
     * exemple :  
     * Article A - Lieu 1 Décembre - Stock courant 12 - mouvements sur la période +1
     * Article A - Lieu 1 Novembre - Stock courant 12 - mouvements sur la période +1
     * 
     * devient
     * Article A - Lieu 1 Décembre - Stock fin 12 - mouvements sur la période +1
     * Article A - Lieu 1 Novembre - Stock fin 11 - mouvements sur la période +1
     */
    public void applyCorrectionsOnDetails() {
        //EDU On se permet de retoucher les données ? 
        //Par exemple pour agréger
        //Rectifier les stock finaux des périodes - OK
        //Supprimer les périodes hors 'champ'
        //Rectifier les infos temporelles sur les lignes sans mouvement - 
        // ajouter dans ce cas une ligne identique pour avoir au minimum deux points OK
        //Préparer les séries pour les graphs ?
        if(details!= null && !details.isEmpty()) {
            DateTimeAggregDTO current = null;
            DateTimeAggregDTO next = null;
            StockDiaryStatsDTO baseLineProd = null;
            for(int index = 0 ; index < details.size() ; index++) {
                StockDiaryStatsDTO ligne = details.get(index);
                //TODO Partie produits - On verra lus tard
                if(baseLineProd == null || !baseLineProd.isSameObject(ligne)) {                   
                    
                    baseLineProd = ligne ;
                    if(ligne.getYear() != 0) {
                        current = new DateTimeAggregDTO(endDate, _group);
                        next = ligne.getDateTimeAggregDTO( _group);
                        if(next.compareTo(current) > 0) {
                            next = current;
                        }
                        while(next != null && current != null && current.compareTo(next) > 0) {
                            //TODO on doit créer une ligne sans mouvement pour boucher le trou 
                            //Attention à la 'première ligne' pb de next et pb si elle manque
                            baseLineProd = createStockDiaryStatsDTO(baseLineProd , current);
                            details.add(index, baseLineProd);
                            current = current.getNext();
                            index++;
                        }
                        baseLineProd = ligne ;
                    } else {
                        current = null;
                        //TODO Cas pas de mouvements sur intervalle de date
                    }
                } else {
                  //même produit -> on remonte le temps sans trou donc on ajuste la quantité finale
                    //DONE attention au 'sans trou'
                    if(ligne.getYear() != 0) {
                        next = ligne.getDateTimeAggregDTO( _group);
                    } else {
                        next = null;
                    }
                    while(next != null && current != null && current.compareTo(next) > 0 && current.getNext().compareTo(next) != 0) {
                        //TODO on doit créer une ligne sans mouvement pour boucher le trou 
                        //Attention à la 'première ligne' pb de next et pb si elle manque
                        baseLineProd = createStockDiaryStatsDTO(baseLineProd , current.getNext());
                        details.add(index, baseLineProd);
                        current = current.getNext();
                        index++;
                    }
                    ligne.setCurrentStock(baseLineProd.getCurrentStock() - baseLineProd.getStockChangeUnits());
                    //On recalcule un PMPA adapté à la valorisation du mouvement de stock sur la période précédente
                    ligne.setCurrentPMAP(ligne.getCurrentStock() != 0D ? ( baseLineProd.getFinalStockValue() / ligne.getCurrentStock()) : ligne.getCorrectedPMAP());
                    baseLineProd = ligne ;
                    current = next;
                }
                ligne.setTimeStamp(ligne.getDateTimeAggregDTO(_group).getTimeStamp().getTime());
            }
            //Mouvements sur la période 2 et la période 4
            //Si je m'intéresse à la période 1 ou la période 3 il me faut une version light au moins pour faire remonter la valo du stock
            //De plus j'ai l'info que ça n'a pas bougé sur cette période donc si je ne fais rien je la perds
            
        }
    }
    
    private StockDiaryStatsDTO createStockDiaryStatsDTO(StockDiaryStatsDTO baseLineProd, DateTimeAggregDTO next) {
        StockDiaryStatsDTO retour = new StockDiaryStatsDTO();
        
        retour.setCurrentPurchasePrice(baseLineProd.getCurrentPurchasePrice());
        retour.setProducts(baseLineProd.getProducts());
        retour.setLocations(baseLineProd.getLocations());
        if(baseLineProd.getDateTimeAggregDTO(_group).compareTo(next)>0) {
            retour.setCurrentStock(baseLineProd.getCurrentStock() - baseLineProd.getStockChangeUnits());
            retour.setCurrentPMAP(retour.getCurrentStock() != 0D ? ( baseLineProd.getFinalStockValue() / retour.getCurrentStock()) : baseLineProd.getCorrectedPMAP());
        } else {
            retour.setCurrentStock(baseLineProd.getCurrentStock());
            retour.setCurrentPMAP(baseLineProd.getCurrentPMAP());
        }
        retour.setYear(next.getYear());
        retour.setMonth(next.getMonth());
        retour.setWeek(next.getWeek());
        retour.setDay(next.getDay());
        retour.setHour(next.getHour());
        retour.setTimeStamp(next.getTimeStamp().getTime());

        return retour;
    }
    
    
    public void createPerLocactionAggregateFromDetails() {
        perLocations = new ArrayList<StockDiaryStatsLocDTO>();
        
        if(details!= null && !details.isEmpty()) {
            StockDiaryStatsLocDTO baseLineLoc = null;
            for(StockDiaryStatsDTO ligne : details) {
                     
                if(baseLineLoc != null && !baseLineLoc.isSame(ligne , _group)) {
                    //rechercher dans la liste si on a quelque chose pour cette période et ce pdv 
                    Iterator<StockDiaryStatsLocDTO> itr = perLocations.iterator();
                    baseLineLoc = null ;
                    while (baseLineLoc ==null && itr.hasNext()) {
                      StockDiaryStatsLocDTO b = itr.next();
                      if(b.isSame(ligne, _group)) {
                          baseLineLoc = b;
                      }
                    }
                }
                
                if (baseLineLoc == null) {
                    //rien trouvé nouvelle entrée
                    baseLineLoc = new StockDiaryStatsLocDTO(ligne) ;
                    perLocations.add(baseLineLoc);
                } else {
                    //Trouvé, on ajoute
                    baseLineLoc.add(new StockDiaryStatsLocDTO(ligne));
                }
                
            }
            
            /**
             * Second passage on fait disparaitre une ligne dont le timestamp serait à 0 en l'ajoutant à toutes les autres lignes
             * De la même location
             */
            int debut = 0 ;
            int courant = 0 ;
            Integer position = null;
            Integer fin = null;
            String locId = null;
            while(courant < perLocations.size()) {
                if(locId == null) {
                    locId = perLocations.get(courant).getLocationId();
                }
                if(!locId.equals(perLocations.get(courant).getLocationId())) {
                    fin = courant;
                } else {
                    if(perLocations.get(courant).getYear() == 0 && perLocations.get(courant).getTimeStamp().getTime() == 0L) {
                        position = courant;
                    }
                }
                if(fin != null) {
                    for(int start = debut ; position != null && start< fin ; start++) {
                        if(position != start) {
                            perLocations.get(start).setStartingValue( perLocations.get(start).getStartingValue() + perLocations.get(position).getStartingValue());
                            perLocations.get(start).setEndingValue(perLocations.get(start).getEndingValue() + perLocations.get(position).getStartingValue());
                        }
                    }
                    if(position != null && debut +1 < fin) {
                        perLocations.remove(position.intValue());
                        fin--;
                        courant--;
                    }
                    position = null;
                    debut = fin;
                    fin = null;
                    locId = null;
                } else {
                    courant++;
                }
            }
            
            //Un dernier passage pour le dernier pdv
            for(int start = debut ; position != null && start< courant ; start++) {
                if(position != start) {
                    perLocations.get(start).setStartingValue( perLocations.get(start).getStartingValue() + perLocations.get(position).getStartingValue());
                    perLocations.get(start).setEndingValue(perLocations.get(start).getEndingValue() + perLocations.get(position).getStartingValue());
                }
            }
            if(position != null && debut +1 < courant) {
                perLocations.remove(position.intValue());
                courant--;
            }
            
        }
        
    }
    
    public void fillDetailsGraphData() {
        detailsGraph = new ArrayList<GraphDataDTO>() ;
        StockDiaryStatsDTO baseLine = null , oldLine = null ;
        GraphDataDTO current = null;
        for(int  index = details.size() ; index-- > 0 ; oldLine = baseLine) {
            baseLine = details.get(index);
            if(oldLine == null || !oldLine.getLocationId().equals(baseLine.getLocationId())
                    || !oldLine.getProductId().equals(baseLine.getProductId())) {
                //Il faut ajouter un graph
                current = new GraphDataDTO(String.format("%s-%s<br>%s-%s", baseLine.getLocationId() , baseLine.getLocationName() , 
                        baseLine.getProductId() , baseLine.getProductName()) ,
                        baseLine.getProductId() , baseLine.getTimeStamp().getClass().toString());
                //current.setDisabled(!detailsGraph.isEmpty());
                detailsGraph.add(current);
            }
            current.addValue(baseLine.getDateTimeAggregDTO(_group).startDate(), baseLine.getCurrentStock()-baseLine.getStockChangeUnits());
            current.addValue(baseLine.getDateTimeAggregDTO(_group).endDate(), baseLine.getCurrentStock());
        }
    }
    
    public void fillLocationsGraphData() {
        locationsGraph = new ArrayList<GraphDataDTO>() ;
        StockDiaryStatsLocDTO baseLine = null , oldLine = null ;
        GraphDataDTO current = null;
        for(int  index = perLocations.size() ; index-- > 0 ; oldLine = baseLine) {
            baseLine = perLocations.get(index);
            if(oldLine == null || !oldLine.getLocationId().equals(baseLine.getLocationId())) {
                //Il faut ajouter un graph
                current = new GraphDataDTO(String.format("%s-%s", baseLine.getLocationId() , baseLine.getLocationName()) ,
                        baseLine.getLocationId() , baseLine.getTimeStamp().getClass().toString());
                locationsGraph.add(current);
            }
            current.addValue(baseLine.getDateTimeAggregDTO(_group).startDate(), baseLine.getStartingValue());
            current.addValue(baseLine.getDateTimeAggregDTO(_group).endDate(), baseLine.getEndingValue());
        }
        
    }
    
}
