package com.ose.backend.dto.stocks;

import java.util.Date;

import org.pasteque.api.dto.locations.LocationsInfoDTO;

public class StockMovementHeaderDTO {

    private Date date;
    private LocationsInfoDTO location;
    private String note;
    private Integer reason;
    private String reasonLabel;
    private String reference;
    private String referenceType;
    private LocationsInfoDTO destination;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getReason() {
        return reason;
    }

    public void setReason(Integer reason) {
        this.reason = reason;
    }

    public String getReasonLabel() {
        return reasonLabel;
    }

    public void setReasonLabel(String reasonLabel) {
        this.reasonLabel = reasonLabel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public LocationsInfoDTO getDestination() {
        return destination;
    }

    public void setDestination(LocationsInfoDTO destination) {
        this.destination = destination;
    }

    public LocationsInfoDTO getLocation() {
        return location;
    }

    public void setLocation(LocationsInfoDTO location) {
        this.location = location;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }
    
    public String getLocationName() {
        return location == null ? "" : location.getLabel() ;
    }

}
