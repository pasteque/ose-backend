package com.ose.backend.dto.stocks;

import java.util.ArrayList;
import java.util.List;

public class StockLevelDTO {

    private String id;
    private String locationsId;
    private String productsId;
    private double stockSecurity;
    private double stockMaximum;
	private String productName;
	private String locationName;
	private List<StockLevelLocationDTO> stockLevelLocationsDTO = new ArrayList<StockLevelLocationDTO>();
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getLocationsId() {
        return locationsId;
    }
    public void setLocationsId(String locationsId) {
        this.locationsId = locationsId;
    }
    public String getProductsId() {
        return productsId;
    }
    public void setProductsId(String productsId) {
        this.productsId = productsId;
    }
    public double getStockSecurity() {
        return stockSecurity;
    }
    public void setStockSecurity(double stockSecurity) {
        this.stockSecurity = stockSecurity;
    }
    public double getStockMaximum() {
        return stockMaximum;
    }
    public void setStockMaximum(double stockMaximum) {
        this.stockMaximum = stockMaximum;
    }
    
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public List<StockLevelLocationDTO> getStockLevelLocationsDTO() {
		return stockLevelLocationsDTO;
	}
	public void setStockLevelLocationsDTO(List<StockLevelLocationDTO> stockLevelLocationsDTO) {
		this.stockLevelLocationsDTO = stockLevelLocationsDTO;
	}
    
}
