package com.ose.backend.dto.stocks;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.response.IXlsResponse;

import com.ose.backend.dto.utils.DateTimeAggregDTO;
import com.ose.backend.service.stats.impl.StatsService.GroupType;

public class StockDiaryStatsLocDTO implements IXlsResponse {

    private String locationId;
    private String locationName;
    private double startingValue;
    private double endingValue;
    private double valueChange;
    private int reason;
    private int year;
    private int month;
    private int week;
    private int day;
    private int hour;
    private Date timeStamp;
    
    public StockDiaryStatsLocDTO(StockDiaryStatsDTO base) {
        if(base != null)  {
            this.day = base.getDay();
            this.hour = base.getHour();
            this.locationId = base.getLocationId();
            this.locationName = base.getLocationName();
            this.month = base.getMonth();
            this.reason = base.getReason();
            this.timeStamp = base.getTimeStamp();
            this.week = base.getWeek();
            this.year = base.getYear();
            this.valueChange = base.getStockChangePrice() == 0D ? base.getStockChangeUnits() * base.getCurrentPurchasePrice() : base.getStockChangePrice() ;
            
            this.endingValue = base.getCurrentStock() * ( base.getCurrentPMAP() == 0D ? base.getCurrentPurchasePrice() : base.getCurrentPMAP());
            this.startingValue = this.endingValue - this.valueChange ;
        }
    }
    
    public StockDiaryStatsLocDTO() {
        
    }
    public String getLocationId() {
        return locationId;
    }
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
    public String getLocationName() {
        return locationName;
    }
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getWeek() {
        return week;
    }
    public void setWeek(int week) {
        this.week = week;
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getHour() {
        return hour;
    }
    public void setHour(int hour) {
        this.hour = hour;
    }
    public Date getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
    

    public double getStartingValue() {
        return startingValue;
    }

    public void setStartingValue(double startingValue) {
        this.startingValue = startingValue;
    }

    public double getEndingValue() {
        return endingValue;
    }

    public void setEndingValue(double endingValue) {
        this.endingValue = endingValue;
    }

    public double getValueChange() {
        return valueChange;
    }

    public void setValueChange(double valueChange) {
        this.valueChange = valueChange;
    }


    public static CellType[] types = {CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC, CellType.STRING ,
        CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING , CellType.STRING};
    
    @Override
    public String[] header() {
        return new String[] { "locationId" , "locationName" , "startingValue" , "endingValue" , "valueChange" ,"reason" , "year" , "month" ,"week" , "day" , "hour" , "timeStamp" };
    }
    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        DecimalFormat units = new DecimalFormat("0.##", otherSymbols);
        DecimalFormat twopositions = new DecimalFormat("00.##", otherSymbols);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return new String[] { getLocationId() , getLocationName() , money.format(getStartingValue()) , money.format(getEndingValue()) , money.format(getValueChange()),
                units.format(getReason()) , units.format(getYear()) , twopositions.format(getMonth()) , twopositions.format(getWeek()) ,
                twopositions.format(getDay()) , twopositions.format(getHour()) , df.format(getTimeStamp())};
        
    }

    public void add(StockDiaryStatsLocDTO stockDiaryStatsDTO) {
        if(this.isSameObject(stockDiaryStatsDTO)) {
            setStartingValue(getStartingValue() + stockDiaryStatsDTO.getStartingValue());
            setEndingValue(getEndingValue() + stockDiaryStatsDTO.getEndingValue());
            setValueChange(getValueChange() + stockDiaryStatsDTO.getValueChange());            
        }
    }

    private boolean isSameObject(StockDiaryStatsLocDTO stockDiaryStatsDTO) {
        return stockDiaryStatsDTO != null && stockDiaryStatsDTO instanceof StockDiaryStatsLocDTO &&
                getLocationId().equals(stockDiaryStatsDTO.getLocationId());

    }

    public void setLocation(Locations locations) {
        setLocationId(locations.getId());
        setLocationName(locations.getName());   
    }

    public boolean isSame(StockDiaryStatsDTO ligne, String _group) {
        boolean retour = ligne!=null && getLocationId().equals(ligne.getLocationId());
        if(retour) {
            GroupType type = Enum.valueOf(GroupType.class ,_group);
            retour = ligne.getYear() == getYear();
            if(GroupType.WEEK == type) {
                   retour &= ligne.getWeek() == getWeek();     
            } else {
                if(GroupType.MONTH.getValue() <= type.getValue()) {
                    retour &= ligne.getMonth() == getMonth();
                }
                if(GroupType.DAY.getValue() <= type.getValue()) {
                    retour &= ligne.getDay() == getDay();
                }
    
                if(GroupType.HOUR.getValue() <= type.getValue()) {
                    retour &= ligne.getHour() == getHour();
                }
            }
        }
        return retour;
    }

    public DateTimeAggregDTO getDateTimeAggregDTO(String _group) {
        return new DateTimeAggregDTO(getYear(),getMonth() , getWeek() , getDay() , getHour(), _group);
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
