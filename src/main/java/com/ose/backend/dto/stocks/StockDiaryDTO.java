package com.ose.backend.dto.stocks;

import java.util.Date;

public class StockDiaryDTO {
    
    private String id;
    private Date dateNew;
    private int reason;
    private String locationsId;
    private String productsId;
    private String attributeSetInstanceId;
    private double units;
    private double price;
    private String note;
    private String destination;
    private String reference;
    private String state;
    private Double stockBefore;
    
    public String getDestination() {
        return destination;
    }
    public void setDestination(String destination) {
        this.destination = destination;
    }
    public String getReference() {
        return reference;
    }
    public void setReference(String reference) {
        this.reference = reference;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public Date getDateNew() {
        return dateNew;
    }
    public void setDateNew(Date dateNew) {
        this.dateNew = dateNew;
    }
    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }
    public String getLocationsId() {
        return locationsId;
    }
    public void setLocationsId(String locationsId) {
        this.locationsId = locationsId;
    }
    public String getProductsId() {
        return productsId;
    }
    public void setProductsId(String productsId) {
        this.productsId = productsId;
    }
    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }
    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }
    public double getUnits() {
        return units;
    }
    public void setUnits(double units) {
        this.units = units;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public Double getStockBefore() {
        return stockBefore;
    }
    public void setStockBefore(Double stockBefore) {
        this.stockBefore = stockBefore;
    }
    
    
}
