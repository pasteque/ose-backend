package com.ose.backend.dto.stocks;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.pasteque.api.response.ICsvResponse;

public class StockProductDetailDTO implements ICsvResponse {

    private String productId;
    private String productCode;
    private String productLabel;
    private String categoryId;
    private String categoryLabel;
    private String productPlace;
    private double units;
    private double priceBuy;
    private double priceStock;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public String getProductPlace() {
        return productPlace;
    }

    public void setProductPlace(String productPlace) {
        this.productPlace = productPlace;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy == null ? 0D : priceBuy;
    }

    public double getPriceStock() {
        return priceStock;
    }

    public void setPriceStock(Double priceStock) {
        this.priceStock = priceStock == null ? 0D : priceStock;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    @Override
    public String[] header() {
        return new String[] { "Code", "Nom", "Univers" , "Quantité"};
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.##", otherSymbols);
        return new String[] { getProductCode(), getProductLabel(), getCategoryLabel() == null ? "" : getCategoryLabel() , decf.format(getUnits()) };
    }

}
