package com.ose.backend.dto.stocks;

import com.ose.backend.dto.products.ProductsResultDTO;

public class ProductMovementDTO {

    private ProductsResultDTO product;
    private double units;
    private double price;
    private String note;

    public ProductsResultDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsResultDTO product) {
        this.product = product;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
