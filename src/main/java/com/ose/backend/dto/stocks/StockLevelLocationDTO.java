package com.ose.backend.dto.stocks;

public class StockLevelLocationDTO {
    private String locationsId;
    private String productsId;
    private String locationName;
    //private String enteteColonne;
    private double stockSecurity;
    private double stockMaximum;
    
	public String getLocationsId() {
		return locationsId;
	}
	public void setLocationsId(String locationsId) {
		this.locationsId = locationsId;
	}
	public String getProductsId() {
		return productsId;
	}
	public void setProductsId(String productsId) {
		this.productsId = productsId;
	}
	public double getStockSecurity() {
		return stockSecurity;
	}
	public void setStockSecurity(double stockSecurity) {
		this.stockSecurity = stockSecurity;
	}
	public double getStockMaximum() {
		return stockMaximum;
	}
	public void setStockMaximum(double stockMaximum) {
		this.stockMaximum = stockMaximum;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	//EDU le champ est il bien utile ? puisqu'il n'est pas fourni avec le getter
	public String getEnteteColonne() {
		if (locationsId == null){
			return "";
		}
		return locationsId + " - "+ locationName;
	}
	/*
	public void setEnteteColonne(String enteteColonne) {
		if (locationsId == null){
			this.enteteColonne = "";
		}else {
			this.enteteColonne = locationsId + " - "+ locationName;;
		}
	}
	*/
	
}
