package com.ose.backend.dto.stocks;

import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;

public class StockCurrentInfoDTO implements ILoadable{

    private LocationsInfoDTO locations;
    private ProductsIndexDTO products;
    private String attributeSetInstanceId;
    private double units;
    private Double stockCost;
    private Double priceBuy;
    private double stockValue;



    public LocationsInfoDTO getLocations() {
        return locations;
    }

    public void setLocations(LocationsInfoDTO locations) {
        this.locations = locations;
    }

    public ProductsIndexDTO getProducts() {
        return products;
    }

    public void setProducts(ProductsIndexDTO products) {
        this.products = products;
    }

    public String getProductsId() {
        return products.getId();
    }


    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public Double getStockCost() {
        return stockCost;
    }

    public void setStockCost(Double stockCost) {
        this.stockCost = stockCost;
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        DtoSQLParameters retour = new DtoSQLParameters();
        
        retour.select = "SELECT t21.PRODUCT , t15.NAME , t15.CATEGORY , t15.PRICEBUY , t15.STOCKCOST , "
                + "t21.UNITS , t21.ATTRIBUTESETINSTANCE_ID , "
                + "t11.NAME , t21.LOCATION , t14.NAME , t11.PARENT_LOCATION_ID , "
                + "IF(t21.UNITS<0 , 0 , t21.UNITS) * IF(ISNULL(t15.STOCKCOST) OR t15.STOCKCOST <= 0  , IFNULL(t15.PRICEBUY , 0) , t15.STOCKCOST ) AS STOCK_VALUE  FROM ";
        
        retour.tableList.add(" STOCKCURRENT t21 ");
        retour.tableHash.put("t21", "STOCKCURRENT");
        
        if(retour.tableHash.put("t15", "PRODUCTS") == null) {
            retour.tableList.add(" PRODUCTS t15 ");
            retour.constraintList.add(" t15.ID = t21.PRODUCT ");
        }
        
        if(retour.tableHash.put("t11", "LOCATIONS") == null) {
            retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON ( t14.ID = t11.CATEG_ID) ");
            retour.tableHash.put("t14", "CATEGORIES");
            retour.constraintList.add(" t11.ID = t21.LOCATION ");
        }
        
        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        int index = 0;
        LocationsInfoDTO newLocations;
        ProductsIndexDTO newProducts;
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 12) {

            try {
                newProducts = new ProductsIndexDTO();
                newProducts.setId((String)array[index++ +starting_index]);
                newProducts.setLibelle((String)array[index++ +starting_index]);
                newProducts.setCategorie((String)array[index++ +starting_index]);
                this.setProducts(newProducts);
                this.setPriceBuy((Double)array[index++ +starting_index]);
                this.setStockCost((Double)array[index++ +starting_index]);
                this.setUnits((double)array[index++ +starting_index]);
                this.setAttributeSetInstanceId((String)array[index++ +starting_index]);
                
                newLocations = new LocationsInfoDTO();
                newLocations.loadFromObjectArray(array, index +starting_index);
                this.setLocations(newLocations);
                index += 4;
                this.setStockValue((double)array[index++ +starting_index]);
                retour = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }

        return retour;
    }

    public double getStockValue() {
        return stockValue;
    }

    public void setStockValue(double stockValue) {
        this.stockValue = stockValue;
    }

}
