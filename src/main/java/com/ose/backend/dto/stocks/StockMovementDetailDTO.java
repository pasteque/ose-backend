package com.ose.backend.dto.stocks;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.response.ICsvResponse;

public class StockMovementDetailDTO implements ICsvResponse{

    private Date date;
    private LocationsInfoDTO location;
    private String note;
    private Integer reason;
    private String reasonLabel;
    private String reference;
    private String referenceType;
    private LocationsInfoDTO destination;

    // Pour le cas de la visu d'un stock diary
    private String productId;
    private String productCode;
    private String productLabel;
    private double units;
    private double price;
    private Double previousStock;

    // Pour le cas de la saisie multiple ou visu des mouvements via reference
    private List<ProductMovementDTO> products = new ArrayList<ProductMovementDTO>();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getReason() {
        return reason;
    }

    public void setReason(Integer reason) {
        this.reason = reason;
    }

    public String getReasonLabel() {
        return reasonLabel;
    }

    public void setReasonLabel(String reasonLabel) {
        this.reasonLabel = reasonLabel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<ProductMovementDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductMovementDTO> products) {
        this.products = products;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocationsInfoDTO getDestination() {
        return destination;
    }

    public void setDestination(LocationsInfoDTO destination) {
        this.destination = destination;
    }

    public LocationsInfoDTO getLocation() {
        return location;
    }

    public void setLocation(LocationsInfoDTO location) {
        this.location = location;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }
    
    public String getLocationName() {
        return location == null ? "" : location.getLabel() ;
    }

    public Double getPreviousStock() {
        return previousStock;
    }

    public void setPreviousStock(Double previousStock) {
        this.previousStock = previousStock;
    }
    
    public String getDestinationName() {
        return destination != null ? destination.getLabel() : "";
    }

    @Override
    public String[] header() {
        return new String[] { "Date", "Origine", "Destination", "Code", "Nom", "Quantite", "Ref", "Type", "Stock Initial" };
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.##", otherSymbols);
        return new String[] { dateHourFormatted, getLocationName(),  getDestinationName(),getProductCode(), getProductLabel(),
                decf.format(getUnits()), getReference(), getReasonLabel(),
                (getPreviousStock() == null ? "" : decf.format(getPreviousStock())) };
    }

}
