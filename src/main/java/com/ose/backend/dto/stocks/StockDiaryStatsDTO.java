package com.ose.backend.dto.stocks;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.response.IXlsResponse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ose.backend.dto.utils.DateTimeAggregDTO;


public class StockDiaryStatsDTO implements IXlsResponse, ILoadable{

    private LocationsInfoDTO locations;
    private ProductsIndexDTO products;
    private double currentStock;
    private double currentPurchasePrice;
    private double currentPMAP;
    private double stockChangeUnits;
    private double soldUnits;
    private double stockChangePriceStored;
    private double stockChangePrice;
    private int reason;
    private int year;
    private int month;
    private int week;
    private int day;
    private int hour;
    private Date timeStamp;
    
    public StockDiaryStatsDTO(Object[] base) {
        loadFromObjectArray(base , 0);
    }
    
    public StockDiaryStatsDTO() {
        
    }


    public double getCurrentStock() {
        return currentStock;
    }
    public void setCurrentStock(double currentStock) {
        this.currentStock = currentStock;
    }
    public double getCurrentPurchasePrice() {
        return currentPurchasePrice;
    }
    public void setCurrentPurchasePrice(double currentPurchasePrice) {
        this.currentPurchasePrice = currentPurchasePrice;
    }
    public double getCurrentPMAP() {
        return currentPMAP;
    }
    public void setCurrentPMAP(double currentPMAP) {
        this.currentPMAP = currentPMAP;
    }
    public double getStockChangeUnits() {
        return stockChangeUnits;
    }
    public void setStockChangeUnits(double stockChangeUnits) {
        this.stockChangeUnits = stockChangeUnits;
    }
    public double getStockChangePrice() {
        return stockChangePrice;
    }
    public void setStockChangePrice(double stockChangePrice) {
        this.stockChangePrice = stockChangePrice;
    }
    public int getReason() {
        return reason;
    }
    public void setReason(int reason) {
        this.reason = reason;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public int getMonth() {
        return month;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public int getWeek() {
        return week;
    }
    public void setWeek(int week) {
        this.week = week;
    }
    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public int getHour() {
        return hour;
    }
    public void setHour(int hour) {
        this.hour = hour;
    }
    public Date getTimeStamp() {
        return timeStamp;
    }
    public void setTimeStamp(Date timeStamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timeStamp);
        calendar.set(Calendar.MILLISECOND, 0);
        this.timeStamp = calendar.getTime();
    }
    
    public DateTimeAggregDTO getDateTimeAggregDTO(String _group) {
        return new DateTimeAggregDTO(getYear(),getMonth() , getWeek() , getDay() , getHour(), _group);
    }
    

    public static CellType[] types = {CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC,CellType.NUMERIC, CellType.NUMERIC,
        CellType.NUMERIC, CellType.NUMERIC,
        CellType.STRING , CellType.STRING , 
        CellType.STRING , CellType.STRING , CellType.STRING};
    
    @Override
    public String[] header() {
        //TODO gérer les catégories
        return new String[] { "locationId" , "locationName" , "productId" , "productName" , "currentStock" , "soldUnits" , "currentPurchasePrice" , "currentPMAP" , "stockChangeUnits" , "stockChangePriceStored" , "stockChangePrice" , "reason" , "year" , "month" ,"week" , "day" , "hour" , "timeStamp" };
    }
    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat money = new DecimalFormat("0.00###", otherSymbols);
        DecimalFormat units = new DecimalFormat("0.##", otherSymbols);
        DecimalFormat twopositions = new DecimalFormat("00.##", otherSymbols);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return new String[] { getLocationId() , getLocations().getLabel() , getProductId() , getProducts().getLibelle() , 
                units.format(getCurrentStock()) , units.format(getSoldUnits()) ,  money.format(getCurrentPurchasePrice()) ,
                money.format(getCurrentPMAP()) , units.format(getStockChangeUnits()) , money.format(getStockChangePriceStored()) , money.format(getStockChangePrice()) ,
                units.format(getReason()) , units.format(getYear()) , twopositions.format(getMonth()) , twopositions.format(getWeek()) ,
                twopositions.format(getDay()) , twopositions.format(getHour()) , df.format(getTimeStamp())};
        
    }

    public void add(StockDiaryStatsDTO stockDiaryStatsDTO) {
        if(this.isSameObject(stockDiaryStatsDTO)) {
            setStockChangeUnits(getStockChangeUnits() + stockDiaryStatsDTO.getStockChangeUnits());
            setStockChangePrice(getStockChangePrice() + stockDiaryStatsDTO.getStockChangePrice());
            
        }
    }

    public boolean isSameObject(StockDiaryStatsDTO stockDiaryStatsDTO) {
        return stockDiaryStatsDTO != null && stockDiaryStatsDTO instanceof StockDiaryStatsDTO &&
                getLocationId().equals(stockDiaryStatsDTO.getLocationId()) && getProductId().equals(stockDiaryStatsDTO.getProductId());

    }

    @JsonIgnore
    public String getProductId() {
        return getProducts() == null ? null : getProducts().getId();
    }

    @JsonIgnore
    public String getLocationId() {
        return getLocations() == null ? null : getLocations().getId();
    }
    
    @JsonIgnore
    public String getProductName() {
        return getProducts() == null ? null : getProducts().getLibelle();
    }

    @JsonIgnore
    public String getLocationName() {
        return getLocations() == null ? null : getLocations().getLabel();
    }

    public void setCurrentStock(StockCurrentInfoDTO element) {
  
        if(element.getPriceBuy() != null) {
            setCurrentPurchasePrice(element.getPriceBuy());
        }
        if(element.getStockCost() != null) {
            setCurrentPMAP(element.getStockCost());
        }
        if(element.getStockValue() != 0D && element.getUnits() != 0D) {
            setCurrentPMAP(element.getStockValue() / element.getUnits());
        }
        setProducts(element.getProducts());
        setLocations(element.getLocations());
        setCurrentStock(element.getUnits());
        
    }

    public double getFinalStockValue() {
        Double value = getCorrectedPMAP();

        value = value * getCurrentStock();
        
        return value.doubleValue();
    }
    
    public double getInitialStockValue() {
        Double value = getCorrectedPMAP();

        value = value * getCurrentStock() - getStockChangePrice();
        
        return value.doubleValue();
    }

    public Double getCorrectedPMAP() {
        Double value = getStockChangeUnits() == 0D ? getCurrentPMAP() : getStockChangePrice() / getStockChangeUnits();
        
        if(value.equals(new Double(0D))) {
            value = getCurrentPurchasePrice();
        }
        return value;
    }

    public LocationsInfoDTO getLocations() {
        return locations;
    }

    public void setLocations(LocationsInfoDTO locations) {
        this.locations = locations;
    }

    public ProductsIndexDTO getProducts() {
        return products;
    }

    public void setProducts(ProductsIndexDTO products) {
        this.products = products;
    }

    public double getStockChangePriceStored() {
        return stockChangePriceStored;
    }

    public void setStockChangePriceStored(double stockChangePriceStored) {
        this.stockChangePriceStored = stockChangePriceStored;
    }

    public double getSoldUnits() {
        return soldUnits;
    }

    public void setSoldUnits(double sellsUnits) {
        this.soldUnits = sellsUnits;
    }

    @Override
    public DtoSQLParameters getSQLParameters() {
        /**
         * SELECT t11.ID, t11.NAME, t15.ID, t15.NAME, t21.UNITS, t15.pricebuy, t15.stockcost, 
         * SUM(t22.UNITS), SUM(t22.PRICE), 
         * SUM( IF(t22.PRICE * t22.UNITS > 0 , t22.PRICE , 
         * IFNULL( ( SELECT SD.PRICE/ SD.UNITS FROM STOCKDIARY SD WHERE SD.DATENEW < t22.DATENEW AND SD.DATENEW > '2016-01-01'AND PRICE * UNITS > 0 AND SD.PRODUCT = t22.PRODUCT LIMIT 1) *  t22.UNITS, 
         * IFNULL( ( SELECT SD.PRICE/ SD.UNITS FROM STOCKDIARY SD WHERE SD.DATENEW > t22.DATENEW AND PRICE * UNITS > 0 AND SD.PRODUCT = t22.PRODUCT LIMIT 1) *  t22.UNITS, 
         * IF(ISNULL(t15.STOCKCOST) OR t15.STOCKCOST <= 0  , IFNULL(t15.PRICEBUY , 0) , t15.STOCKCOST ) * t22.UNITS)) )) AS MOVMT_VALUE,
         * t22.REASON, 
         * YEAR(t22.DATENEW), MONTH(t22.DATENEW), WEEK(t22.DATENEW, 3), DAY(t22.DATENEW), HOUR(t22.DATENEW), 
         * t22.DATENEW 
         * FROM PRODUCTS t15 , 
         * STOCKDIARY t22 LEFT OUTER JOIN STOCKCURRENT t21 ON (t21.product = t22.product AND t21.location = t22.location), 
         * LOCATIONS t11 
         * WHERE ((
         * (t22.location = '1001') 
         * AND (t22.product = '14106')
         * AND  (t22.DATENEW >= '2016-07-20')) AND (((t15.ID = t22.product) 
         * AND (t11.ID = t22.location)))) 
         * GROUP BY t11.ID,  t15.ID, YEAR(t22.DATENEW), MONTH(t22.DATENEW), WEEK(t22.DATENEW, 3), DAY(t22.DATENEW) 
         * ORDER BY t11.ID ASC, t15.ID ASC, YEAR(t22.DATENEW) DESC, MONTH(t22.DATENEW) DESC, WEEK(t22.DATENEW, 3) DESC, DAY(t22.DATENEW) DESC ;
         */
        /*
         * Valorisation d'un mouvement de stock -> valeur stockée
         * si nulle ou incohérente : on recalcule sur la base du PMPA précédement connu/stocké pour la ref
         * si toujours KO on recalcule sur la base du PMPA suivant pour la ref
         * si toujours KO on recalcule sur la base du PMPA actuel
         * si toujours KO on recalcule sur la base du prix d'achat
         * si toujours KO on met une valeur de zéro
         */
        
        DtoSQLParameters retour = new DtoSQLParameters();
        
        retour.select = "SELECT t11.NAME, t11.ID, t14.NAME, t11.PARENT_LOCATION_ID , t15.ID, t15.NAME, t15.CATEGORY , t21.UNITS, t15.pricebuy, t15.stockcost, "
                +" SUM(t22.UNITS), SUM(t22.PRICE), "
                +" SUM( IF(t22.PRICE * t22.UNITS > 0 , t22.PRICE , "
                +" IFNULL( ( SELECT SD.PRICE/ SD.UNITS FROM STOCKDIARY SD WHERE SD.DATENEW < t22.DATENEW AND SD.DATENEW > '2016-01-01'AND PRICE * UNITS > 0 AND SD.PRODUCT = t22.PRODUCT LIMIT 1) *  t22.UNITS, "
                +" IFNULL( ( SELECT SD.PRICE/ SD.UNITS FROM STOCKDIARY SD WHERE SD.DATENEW > t22.DATENEW AND PRICE * UNITS > 0 AND SD.PRODUCT = t22.PRODUCT LIMIT 1) *  t22.UNITS, "
                +" IF(ISNULL(t15.STOCKCOST) OR t15.STOCKCOST <= 0  , IFNULL(t15.PRICEBUY , 0) , t15.STOCKCOST ) * t22.UNITS)) )) AS MOVMT_VALUE, "
                +" t22.REASON, "
                +" YEAR(t22.DATENEW), MONTH(t22.DATENEW), WEEK(t22.DATENEW, 3), DAY(t22.DATENEW), HOUR(t22.DATENEW), "
                +" t22.DATENEW , SUM(IF(t22.REASON = -1 AND ( t22.NOTE = '0' or CAST(t22.NOTE as SIGNED) != 0 ), t22.UNITS * -1 , 0)) FROM ";
        

        retour.tableList.add(" STOCKDIARY t22 LEFT OUTER JOIN STOCKCURRENT t21 ON (t21.product = t22.product AND t21.location = t22.location)");
        retour.tableHash.put("t22", "STOCKDIARY");
        retour.tableHash.put("t21", "STOCKCURRENT");
        
        if(retour.tableHash.put("t15", "PRODUCTS") == null) {
            retour.tableList.add(" PRODUCTS t15 ");
            retour.constraintList.add(" t15.ID = t22.PRODUCT ");
        }
        
        if(retour.tableHash.put("t11", "LOCATIONS") == null) {
            retour.tableList.add(" LOCATIONS t11 LEFT JOIN CATEGORIES t14 ON ( t14.ID = t11.CATEG_ID) ");
            retour.tableHash.put("t14", "CATEGORIES");
            retour.constraintList.add(" t11.ID = t22.LOCATION ");
        }
        
        retour.groupBy = "t11.ID ,  t15.ID";
         //, YEAR(t22.DATENEW), MONTH(t22.DATENEW), WEEK(t22.DATENEW, 3), DAY(t22.DATENEW)
        
        return retour;
    }

    @Override
    public boolean loadFromObjectArray(Object[] array, int starting_index) {
        boolean retour = false;
        int index = 0;
        ProductsIndexDTO newProducts = new ProductsIndexDTO();
        LocationsInfoDTO newLocations = new LocationsInfoDTO();
        
        if(array[0+starting_index] != null && (array.length - starting_index) >= 21)  {

            newLocations.loadFromObjectArray(array, index +starting_index);
            this.setLocations(newLocations);
            index += 4;
            newProducts.setId((String)array[index++ +starting_index]);
            newProducts.setLibelle((String)array[index++ +starting_index]);
            newProducts.setCategorie((String)array[index++ +starting_index]);
            this.setProducts(newProducts);
            currentStock = (double) array[index++ +starting_index];
            if(array[index++ +starting_index] != null) {
                currentPurchasePrice = (double) array[index -1 +starting_index];
            }
            if(array[index++ +starting_index] != null) {
                currentPMAP = (double) array[index - 1 +starting_index];
            }
            stockChangeUnits = (double) array[index++ +starting_index];
            stockChangePriceStored = (double) array[index++ +starting_index];
            stockChangePrice = (double) array[index++ +starting_index];
            reason = (int) array[index++ +starting_index];
            year = (int) array[index++ +starting_index];
            month = (int) array[index++ +starting_index];
            week = (int) array[index++ +starting_index];
            day = (int) array[index++ +starting_index];
            hour = (int) array[index++ +starting_index];
            timeStamp = (Date) array[index++ +starting_index];
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(timeStamp);
            calendar.set(Calendar.MILLISECOND, 0);
            timeStamp = calendar.getTime();
            soldUnits = (double) array[index++ +starting_index];
      }
        return retour;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }
    

}
