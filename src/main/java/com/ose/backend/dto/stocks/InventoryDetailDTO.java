package com.ose.backend.dto.stocks;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.response.ICsvResponse;

import com.ose.backend.dto.products.ProductsResultDTO;

public class InventoryDetailDTO implements ICsvResponse{


    
    public static CellType[] types = {CellType.STRING , CellType.STRING , 
            CellType.STRING , CellType.STRING , CellType.NUMERIC };
    
    private String id;
    private String inventoryId;
    private String usernameId;
    private String username;
    private Date date;
    private ProductsResultDTO product;
    private long quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getUsernameId() {
        return usernameId;
    }

    public void setUsernameId(String usernameId) {
        this.usernameId = usernameId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ProductsResultDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsResultDTO product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String[] header() {
        return new String[] { "Date", "Auteur", "Ref" , "Nom" , "Quantite" };
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat hdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateHourFormatted = getDate() != null ? hdf.format(getDate()) : "";
        return new String[] {dateHourFormatted , getUsername(), getProduct().getId(), getProduct().getLabel(), Long.toString(getQuantity()) };
    }

    public void setProductLocation(Products products, String locationId) {
        product.setStockLocation(products.getProductLocation(locationId));
        
    }

}
