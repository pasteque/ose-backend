package com.ose.backend.dto.stocks;

import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.response.ICsvResponse;

public class InventoryDetailGroupedDTO implements ICsvResponse{

    private String productReference;
    private String productLabel;
    private String productLocation;
    private Long quantity;
    private Double stock;
    private Double PMPA;
    private Double valeur;
    
    public static CellType[] types = {CellType.STRING , CellType.STRING , CellType.STRING ,  
            CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC };

    public String getProductReference() {
        return productReference;
    }

    public void setProductReference(String productReference) {
        this.productReference = productReference;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getStock() {
        return stock;
    }

    public void setStock(Double objects) {
        this.stock = objects;
    }

    public Double getPMPA() {
        return PMPA;
    }

    public void setPMPA(Double pMPA) {
        PMPA = pMPA;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }

    public String getProductLocation() {
        return productLocation;
    }

    public void setProductLocation(String productLocation) {
        this.productLocation = productLocation;
    }

    @Override
    public String[] header() {
        return new String[] { "Ref" , "Nom" , "Emplacement" , "Quantite Inv" , "Quantite Th" , "Valeur" , "Valeur Tot"};
    }

    @Override
    public String[] CSV() {
        return new String[] {getProductReference(), getProductLabel(), getProductLocation(), Long.toString(getQuantity()) , Double.toString(getStock()) , Double.toString(getPMPA()) , Double.toString(getValeur())  };
    }

}
