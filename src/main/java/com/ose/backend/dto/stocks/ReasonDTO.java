package com.ose.backend.dto.stocks;

public class ReasonDTO {

    private int reason;
    private String label;

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
