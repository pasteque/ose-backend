package com.ose.backend.dto.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.ose.backend.service.stats.impl.StatsService.GroupType;

public class DateTimeAggregDTO implements Comparable<DateTimeAggregDTO>{

    private int year;
    private int month;
    private int week;
    private int day;
    private int hour;
    private Calendar timeStamp;
    
    private String _group;
    
    public DateTimeAggregDTO(Date timestamp , String _group) {
        timeStamp = Calendar.getInstance();
        timeStamp.setTime(timestamp);
        this._group = _group ;
        this.year = timeStamp.get(Calendar.YEAR);
        this.day = timeStamp.get(Calendar.DAY_OF_MONTH);
        this.week = timeStamp.get(Calendar.WEEK_OF_YEAR);
        this.month = timeStamp.get(Calendar.MONTH)+1;
        this.hour = timeStamp.get(Calendar.HOUR_OF_DAY);
        setTimeStamp();
    }
    
    public DateTimeAggregDTO(int year , int month , int week , int day , int hour , String _group) {
        timeStamp = Calendar.getInstance();
        
        this._group = _group ;
        this.year = year;
        this.day = day;
        this.week = week;
        this.month = month;
        this.hour = hour;

        setTimeStamp();
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public Calendar getTimeStamp() {
        return ((Calendar) timeStamp.clone());
    }

    public void setTimeStamp(Calendar timeStamp) {
        this.timeStamp = (Calendar) timeStamp.clone();
        this.timeStamp.set(Calendar.MILLISECOND, 0);
    }
    
    public void setTimeStamp() {
        if(timeStamp == null) {
            timeStamp = Calendar.getInstance();
        } else {
            timeStamp.clear();
        }
        this.timeStamp.setTimeInMillis(( this.endDate().getTime() + this.startDate().getTime() ) / 2);
        this.timeStamp.set(Calendar.MILLISECOND, 0);
    }

    public String get_group() {
        return _group;
    }

    public void set_group(String _group) {
        this._group = _group;
    }
    
    public String toString() {
        String retour = "";
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat twopositions = new DecimalFormat("00.##", otherSymbols);
        GroupType type = Enum.valueOf(GroupType.class, _group);
        retour = String.format("%d", getYear());
        if(GroupType.WEEK == type) {
               retour = retour.concat("-S").concat(twopositions.format(getWeek()));     
        } else {
            if(GroupType.MONTH.getValue() <= type.getValue()) {
                retour = retour.concat("-").concat(twopositions.format(getMonth())); 
            }
            if(GroupType.DAY.getValue() <= type.getValue()) {
                retour = retour.concat("-").concat(twopositions.format(getDay())); 
            }

            if(GroupType.HOUR.getValue() <= type.getValue()) {
                retour = retour.concat(" ").concat(twopositions.format(getHour())).concat("h"); 
            }
        }
        
        return retour;
    }
    
    public Date startDate() {
        GroupType type = Enum.valueOf(GroupType.class ,_group);
        Calendar calendar = Calendar.getInstance();
        calendar.set(getYear(), getMonth()-1, getDay(), getHour(), 0, 0);
        switch(type) {
        case YEAR :
            calendar.set(Calendar.DAY_OF_YEAR, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            break;
        case WEEK :
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.add(Calendar.DAY_OF_YEAR, -( 5  + calendar.get(Calendar.DAY_OF_WEEK)) % 7);
            //calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            //calendar.set(Calendar.WEEK_OF_YEAR, getWeek());
            break;
        case MONTH :
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            break;
        case DAY :
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            break;
        default:
            break;
        }
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
    
    public Date endDate() {
        GroupType type = Enum.valueOf(GroupType.class , _group);
        Calendar calendar = Calendar.getInstance();
        calendar.set(getYear(), getMonth()-1, getDay(), getHour(), 59, 59);
        switch(type) {
        case YEAR :
            calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            break;
        case WEEK :
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.add(Calendar.DAY_OF_YEAR, ( Calendar.SATURDAY + 1  - calendar.get(Calendar.DAY_OF_WEEK)) % 7);
            //calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
            //calendar.set(Calendar.WEEK_OF_YEAR, getWeek());
            break;
        case MONTH :
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            break;
        case DAY :
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            break;
        default:
            break;
        }
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    @Override
    public int compareTo(DateTimeAggregDTO arg0) {
            return toString().compareTo(arg0.toString());        
    }
    
    public DateTimeAggregDTO getNext(){
        GroupType type = Enum.valueOf(GroupType.class ,_group);
        Calendar calendar = getTimeStamp();
        switch(type) {
        case YEAR :
            calendar.add(Calendar.YEAR, -1);
            break;
        case WEEK :
            calendar.add(Calendar.DATE, -7);
            break;
        case MONTH :
            calendar.add(Calendar.MONTH, -1);
            break;
        case DAY :
            calendar.add(Calendar.DATE, -1);
            break;
        case HOUR :
            calendar.add(Calendar.HOUR_OF_DAY, -1);
            break;
        default:
            break;
        }
        return new DateTimeAggregDTO(calendar.getTime() , get_group());
        
    }
}
