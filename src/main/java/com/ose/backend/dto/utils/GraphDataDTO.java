package com.ose.backend.dto.utils;

import java.util.ArrayList;

public class GraphDataDTO {
    private String key;
    private ArrayList<Object[]> values;
    private String hiddenKey;
    private String xType;
    private boolean disabled;
    
    public GraphDataDTO(String key , String hiddenKey ,String xType) {
        values = new ArrayList<Object[]>();
        this.key = key;
        this.hiddenKey = hiddenKey;
        this.xType = xType;
        this.disabled = false;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public ArrayList<Object[]> getValues() {
        return values;
    }
    
    public void setValues(ArrayList<Object[]> values) {
        this.values = values;
    }
    public String getHiddenKey() {
        return hiddenKey;
    }
    public void setHiddenKey(String hiddenKey) {
        this.hiddenKey = hiddenKey;
    }
    public String getxType() {
        return xType;
    }
    public void setxType(String xType) {
        this.xType = xType;
    }
    
    public boolean isDisabled() {
        return disabled;
    }
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
    public void addValue(Object key , Object value) {
        Object[] newValue = new Object[2];
        newValue[0] = key;
        newValue[1] = value;
        if(key.getClass().toString().equals(xType) ) {
            values.add(newValue);
        }
    }
    
    
}
