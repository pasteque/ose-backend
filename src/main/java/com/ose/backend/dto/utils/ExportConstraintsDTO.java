package com.ose.backend.dto.utils;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;

import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.stocks.StockCurrentDTO;
import com.ose.backend.dto.stocks.StockDiaryDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.dto.tickets.PaymentsIndexDTO;

/**
 * ExportConstraintsDTO
 * 
 * @author edouard.ducray Classe majeure dans l'export de données En effet c'est elle qui va déterminer ce qui peut être exportable Mais
 *         aussi ce qui sera réellement exporté Dans un premier temps nous allons exporter - les ventes - les clients
 *
 */
public class ExportConstraintsDTO {

    private Boolean exportSales;
    private Boolean exportCust;
    private Boolean exportStocks;
    private Boolean exportSalesLocations;
    private Boolean exportCashRegisters;
    private Boolean exportCashMovements;
    private Boolean exportMessages;
    private Boolean exportAll;
    private Boolean jsonFormat;
    private Boolean csvFormat;
    private Boolean withoutHeaders;
    private Boolean emailsOnly;
    private Boolean exportOrders;
    private String locationId;
    private String custCodeRegex;
    private Date salesFrom;
    private Date salesTo;
    private List<POSTicketsDTO> tickets;
    private List<PaymentsIndexDTO> cashMovements;
    private List<CustomersDetailDTO> customers;
    private List<CashRegistersSimpleDTO> cashes;
    private List<POSSalesLocationsDTO> salesLocations;
    private List<StockCurrentDTO> stockCurrent;
    private List<StockLevelDTO> stockLevel;
    private List<StockDiaryDTO> stockDiary;
    private List<POSClosedCashDTO> closedCash;
    private List<OrderDTO> orders;
    private List<PaletteDTO> palettes;
    private List<POSMessagesDTO> messages;

    public List<POSTicketsDTO> getTickets() {
        return tickets;
    }

    public void setTickets(List<POSTicketsDTO> tickets) {
        this.tickets = tickets;
    }

    public List<CustomersDetailDTO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomersDetailDTO> customers) {
        this.customers = customers;
    }

    public List<CashRegistersSimpleDTO> getCashes() {
        return cashes;
    }

    public void setCashes(List<CashRegistersSimpleDTO> cashes) {
        this.cashes = cashes;
    }

    public List<POSSalesLocationsDTO> getSalesLocations() {
        return salesLocations;
    }

    public void setSalesLocations(List<POSSalesLocationsDTO> salesLocations) {
        this.salesLocations = salesLocations;
    }

    public boolean isExportSales() {
        return (exportSales != null ? exportSales : false);
    }

    public void setExportSales(Boolean exportSales) {
        this.exportSales = exportSales;
    }

    public boolean isExportCust() {
        return (exportCust != null ? exportCust : false);
    }

    public void setExportCust(Boolean exportCust) {
        this.exportCust = exportCust;
    }

    public String getCustCodeRegex() {
        return custCodeRegex;
    }

    public void setCustCodeRegex(String custCodeRegex) {
        this.custCodeRegex = custCodeRegex;
    }

    public Date getSalesFrom() {
        return salesFrom;
    }

    public void setSalesFrom(Date salesFrom) {
        this.salesFrom = salesFrom;
    }

    public Date getSalesTo() {
        return salesTo;
    }

    public void setSalesTo(Date salesTo) {
        this.salesTo = salesTo;
    }

    public boolean isExportStocks() {
        return (exportStocks != null ? exportStocks : false);
    }

    public void setExportStocks(Boolean exportStocks) {
        this.exportStocks = exportStocks;
    }

    public boolean isExportSalesLocations() {
        return (exportSalesLocations != null ? exportSalesLocations : false);
    }

    public void setExportSalesLocations(Boolean exportSalesLocations) {
        this.exportSalesLocations = exportSalesLocations;
    }

    public boolean isExportMessages() {
        return (exportMessages != null ? exportMessages : false);
    }

    public void setExportMessages(Boolean exportMessages) {
        this.exportMessages = exportMessages;
    }

    public boolean isExportCashRegisters() {
        return (exportCashRegisters != null ? exportCashRegisters : false);
    }

    public void setExportCashRegisters(Boolean exportCashRegisters) {
        this.exportCashRegisters = exportCashRegisters;
    }

    public boolean isJsonFormat() {
        return (jsonFormat != null ? jsonFormat : false);
    }

    public void setJsonFormat(Boolean jsonFormat) {
        this.jsonFormat = jsonFormat;
    }

    public List<StockCurrentDTO> getStockCurrent() {
        return stockCurrent;
    }

    public void setStockCurrent(List<StockCurrentDTO> stockCurrent) {
        this.stockCurrent = stockCurrent;
    }

    public List<StockLevelDTO> getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(List<StockLevelDTO> stockLevel) {
        this.stockLevel = stockLevel;
    }

    public boolean isExportAll() {
        return (exportAll != null ? exportAll : false);
    }

    public void setExportAll(Boolean exportAll) {
        this.exportAll = exportAll;
    }

    public List<StockDiaryDTO> getStockDiary() {
        return stockDiary;
    }

    public void setStockDiary(List<StockDiaryDTO> stockDiary) {
        this.stockDiary = stockDiary;
    }

    public List<POSClosedCashDTO> getClosedCash() {
        return closedCash;
    }

    public void setClosedCash(List<POSClosedCashDTO> closedCash) {
        this.closedCash = closedCash;
    }

    public boolean isCsvFormat() {
        return csvFormat == null ? false : csvFormat;
    }

    public void setCsvFormat(Boolean csvFormat) {
        this.csvFormat = csvFormat;
    }

    public boolean isWithoutHeaders() {
        return withoutHeaders == null ? false : withoutHeaders;
    }

    public void setWithoutHeaders(Boolean withoutHeaders) {
        this.withoutHeaders = withoutHeaders;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public boolean isEmailsOnly() {
        return emailsOnly==null ? false : emailsOnly;
    }

    public void setEmailsOnly(Boolean emailsOnly) {
        this.emailsOnly = emailsOnly;
    }
    
    public boolean isExportOrders() {
        return exportOrders==null ? false : exportOrders;
    }

    public void setExportOrders(Boolean exportOrders) {
        this.exportOrders = exportOrders;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public List<PaletteDTO> getPalettes() {
        return palettes;
    }

    public void setPalettes(List<PaletteDTO> palettes) {
        this.palettes = palettes;
    }

    public List<POSMessagesDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<POSMessagesDTO> messages) {
        this.messages = messages;
    }

    public List<PaymentsIndexDTO> getCashMovements() {
        return cashMovements;
    }

    public void setCashMovements(List<PaymentsIndexDTO> cashMovements) {
        this.cashMovements = cashMovements;
    }

    public boolean isExportCashMovements() {
        return exportCashMovements ==null ? false : exportCashMovements;
    }

    public void setExportCashMovements(Boolean exportCashMovements) {
        this.exportCashMovements = exportCashMovements;
    }

    // TODO en mode productif faire en sorte que sans instruction on soit en JSON avec ExportAll

}
