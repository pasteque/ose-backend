package com.ose.backend.dto.job;

import java.util.Date;


public class JobDTO {
    
    private String id;
    private String sens;
    private String type;
    private String destinataire;
    private String etat;
    private String message;
    private Date dateCreation;
    private Date dateEnvoi;
    private Date dateReception;
    private Date dateIntegration;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getSens() {
        return sens;
    }
    public void setSens(String sens) {
        this.sens = sens;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDestinataire() {
        return destinataire;
    }
    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }
    public String getEtat() {
        return etat;
    }
    public void setEtat(String etat) {
        this.etat = etat;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Date getDateCreation() {
        return dateCreation;
    }
    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
    public Date getDateEnvoi() {
        return dateEnvoi;
    }
    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }
    public Date getDateReception() {
        return dateReception;
    }
    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }
    public Date getDateIntegration() {
        return dateIntegration;
    }
    public void setDateIntegration(Date dateIntegration) {
        this.dateIntegration = dateIntegration;
    }
    
    

}
