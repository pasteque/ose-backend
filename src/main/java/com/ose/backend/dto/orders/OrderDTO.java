package com.ose.backend.dto.orders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.response.ICsvResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDTO implements ICsvResponse {

    private String id;
    private String usernameId;
    private String username;
    private Date creationDate;
    private LocationsInfoDTO locationTo;
    private LocationsInfoDTO locationFrom;
    private String reference;
    private String note;
    private String status;
    private int statusOrder;
    private String planifUserId;
    private Date planifDate;
    private String prepUserId;
    private Date prepDate;
    private String controlUserId;
    private Date controlDate;
    private String delivUserId;
    private Date delivDate;
    private String receptUserId;
    private Date receptDate;
    private String planifUserName;
    private String prepUserName;
    private String controlUserName;
    private String delivUserName;
    private String receptUserName;

    private List<OrderDetailDTO> orderDetails = new ArrayList<>();

    private List<PaletteStatusInfoDTO> palettes = new ArrayList<>();

    @Override
    public String[] header() {
        return null;
    }

    @Override
    public String[] CSV() {
        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsernameId() {
        return usernameId;
    }

    public void setUsernameId(String usernameId) {
        this.usernameId = usernameId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public LocationsInfoDTO getLocationTo() {
        return locationTo;
    }

    public void setLocationTo(LocationsInfoDTO locationTo) {
        this.locationTo = locationTo;
    }

    public LocationsInfoDTO getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(LocationsInfoDTO locationFrom) {
        this.locationFrom = locationFrom;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<OrderDetailDTO> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetailDTO> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(int statusOrder) {
        this.statusOrder = statusOrder;
    }

    public List<PaletteStatusInfoDTO> getPalettes() {
        return palettes;
    }

    public void setPalettes(List<PaletteStatusInfoDTO> palettes) {
        this.palettes = palettes;
    }

    public String getLocationFromId() {
        return getLocationFrom() == null ? "" : getLocationFrom().getId();
    }

    public String getLocationToId() {
        return getLocationTo() == null ? "" : getLocationTo().getId();
    }

    public String getPlanifUserId() {
        return planifUserId;
    }

    public void setPlanifUserId(String planifUserId) {
        this.planifUserId = planifUserId;
    }

    public Date getPlanifDate() {
        return planifDate;
    }

    public void setPlanifDate(Date planifDate) {
        this.planifDate = planifDate;
    }

    public String getPrepUserId() {
        return prepUserId;
    }

    public void setPrepUserId(String prepUserId) {
        this.prepUserId = prepUserId;
    }

    public Date getPrepDate() {
        return prepDate;
    }

    public void setPrepDate(Date prepDate) {
        this.prepDate = prepDate;
    }

    public String getControlUserId() {
        return controlUserId;
    }

    public void setControlUserId(String controlUserId) {
        this.controlUserId = controlUserId;
    }

    public Date getControlDate() {
        return controlDate;
    }

    public void setControlDate(Date controlDate) {
        this.controlDate = controlDate;
    }

    public String getDelivUserId() {
        return delivUserId;
    }

    public void setDelivUserId(String delivUserId) {
        this.delivUserId = delivUserId;
    }

    public Date getDelivDate() {
        return delivDate;
    }

    public void setDelivDate(Date delivDate) {
        this.delivDate = delivDate;
    }

    public String getReceptUserId() {
        return receptUserId;
    }

    public void setReceptUserId(String receptUserId) {
        this.receptUserId = receptUserId;
    }

    public Date getReceptDate() {
        return receptDate;
    }

    public void setReceptDate(Date receptDate) {
        this.receptDate = receptDate;
    }

    public String getPlanifUserName() {
        return planifUserName;
    }

    public void setPlanifUserName(String planifUserName) {
        this.planifUserName = planifUserName;
    }

    public String getPrepUserName() {
        return prepUserName;
    }

    public void setPrepUserName(String prepUserName) {
        this.prepUserName = prepUserName;
    }

    public String getControlUserName() {
        return controlUserName;
    }

    public void setControlUserName(String controlUserName) {
        this.controlUserName = controlUserName;
    }

    public String getDelivUserName() {
        return delivUserName;
    }

    public void setDelivUserName(String delivUserName) {
        this.delivUserName = delivUserName;
    }

    public String getReceptUserName() {
        return receptUserName;
    }

    public void setReceptUserName(String receptUserName) {
        this.receptUserName = receptUserName;
    }

}
