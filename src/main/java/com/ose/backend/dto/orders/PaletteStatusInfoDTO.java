package com.ose.backend.dto.orders;

public class PaletteStatusInfoDTO {

    private String id;
    private String name;
    private String statusOrder;
    private String preparationUsername;
    private String lastProductLocation;
    private int nbLines;
    private int nbLinesLeft;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreparationUsername() {
        return preparationUsername;
    }

    public void setPreparationUsername(String preparationUsername) {
        this.preparationUsername = preparationUsername;
    }

    public String getLastProductLocation() {
        return lastProductLocation;
    }

    public void setLastProductLocation(String lastProductLocation) {
        this.lastProductLocation = lastProductLocation;
    }

    public int getNbLines() {
        return nbLines;
    }

    public void setNbLines(int nbLines) {
        this.nbLines = nbLines;
    }

    public int getNbLinesLeft() {
        return nbLinesLeft;
    }

    public void setNbLinesLeft(int nbLinesLeft) {
        this.nbLinesLeft = nbLinesLeft;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(String statusOrder) {
        this.statusOrder = statusOrder;
    }

}
