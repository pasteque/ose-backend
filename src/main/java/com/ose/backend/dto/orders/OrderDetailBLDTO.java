package com.ose.backend.dto.orders;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.response.IXlsResponse;

public class OrderDetailBLDTO implements IXlsResponse {

    /* Pour des exports-imports dans de bonnes conditions
     * */

    private String orderId;
    private String paletteId;
    private String productId;
    private String productLabel;
    private String productEAN;
    
    private Double quantityReceived;
    private Double quantityPrepared;
    
    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.NUMERIC , CellType.NUMERIC };

    public int[] colWidths = { 450*24 , 450*24 , 220*24 , 600*24 , 200*24 , 200*24 , 200*24 };

    public static Boolean shrink = true;
    
    public static Boolean border = true;
    
    public static Boolean boldHeader = true;
    
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaletteId() {
        return paletteId;
    }

    public void setPaletteId(String paletteId) {
        this.paletteId = paletteId;
    }

    public Double getQuantityPrepared() {
        return quantityPrepared == null ? 0D : quantityPrepared;
    }

    public void setQuantityPrepared(Double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public Double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(Double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public String getProductEAN() {
        return productEAN;
    }

    public void setProductEAN(String productEAN) {
        this.productEAN = productEAN;
    }

    @Override
    public String[] header() {
        return new String[] {
                "Référence commande" , "Numéro de palette" , "Référence article" , 
                "Libellé article" , "Code EAN" , "Qté expédiée" , "Qté reçue"};
        }

    @Override
    public String[] CSV() {
        return new String[] {
                getOrderId() , getPaletteId() , getProductId() , getProductLabel() , getProductEAN() , 
                Double.toString(getQuantityPrepared()) ,  getQuantityReceived() == null ? " " : Double.toString(getQuantityReceived())
                };
    }
    
    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        
        Font defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short)10);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(false);
        defaultFont.setItalic(false);
        
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        
        
        cellStyle.setFont(defaultFont);
        return cellStyle;
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        
        Font defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short)12);
        defaultFont.setFontName("Arial");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        defaultFont.setBold(true);
        defaultFont.setItalic(false);
        
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        
        
        cellStyle.setFont(defaultFont);
        return cellStyle;
    }
    
    @Override
    public int getColWidth(int index) {
        return colWidths == null || colWidths.length <= index ? 0 :  colWidths[index] ;
    }

}
