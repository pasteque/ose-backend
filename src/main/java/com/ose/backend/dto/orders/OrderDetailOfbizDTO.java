package com.ose.backend.dto.orders;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.pasteque.api.response.ICsvResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ose.backend.dto.products.ProductsResultDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailOfbizDTO implements ICsvResponse {

    private String id;
    private String orderId;
    private ProductsResultDTO product;
    private double quantitySent;
    private String store;
    private String clientId;
    
    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    private String productPaletteInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductsResultDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsResultDTO product) {
        this.product = product;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }
    
    public String getProductId() {
        return getProduct() == null ? "" : getProduct().getId();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductPaletteInfo() {
        return productPaletteInfo;
    }

    public void setProductPaletteInfo(String productPaletteInfo) {
        this.productPaletteInfo = productPaletteInfo;
    }

    /**
     * Le but est de sortir ceci
     * 
     * ProductStore;ProClientId;OrderExtrenalId;ProductId;Quantity;Price;Location
       CAMION1;OSE;CAMION1-20-S38;6129;101;5,52;B041
       CAMION1;OSE;CAMION1-20-S38;6752;8;24,56;D027
       CAMION1;OSE;CAMION1-20-S38;2522;162;1,38;F034
     */
    @Override
    public String[] header() {
        // TODO Auto-generated method stub
        return new String[] {"ProductStore","ProClientId","OrderExtrenalId","ProductId","Quantity","Price","Location"};
    }

    @Override
    public String[] CSV() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator(',');
        DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
        return new String[] {getStore() , getClientId() , getOrderId() , getProductId() , String.format("%.0f", getQuantitySent()) ,  decf.format(Double.parseDouble(getProduct().getCurrentPMPA())) , getProduct().getStockLocation() };
    }

}
