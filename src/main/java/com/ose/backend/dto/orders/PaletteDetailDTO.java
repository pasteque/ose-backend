package com.ose.backend.dto.orders;

import java.util.Date;

import org.pasteque.api.dto.security.POSUsersDTO;

public class PaletteDetailDTO {

    private String id;
    private String orderDetailId;
    private String productCode;
    private String productName;
    private String stockLocationName;
    private String note;
    private String status;
    private String paletteId;
    private double quantityFromOrderDetail;
    private double quantityToPrepare;
    private double quantityPrepared;
    private double quantitySent;
    private double quantityReceived;
    private boolean close;
    
    private String preparationUserId;
    private Date preparationDate;
    private POSUsersDTO preparationUser;
    private String inputOrigine;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getQuantityToPrepare() {
        return quantityToPrepare;
    }

    public void setQuantityToPrepare(Double quantityToPrepare) {
        this.quantityToPrepare = quantityToPrepare;
    }

    public double getQuantityPrepared() {
        return quantityPrepared;
    }

    public void setQuantityPrepared(double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public String getStockLocationName() {
        return stockLocationName;
    }

    public void setStockLocationName(String stockLocationName) {
        this.stockLocationName = stockLocationName;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getQuantityFromOrderDetail() {
        return quantityFromOrderDetail;
    }

    public void setQuantityFromOrderDetail(double quantityFromOrderDetail) {
        this.quantityFromOrderDetail = quantityFromOrderDetail;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }

    public double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaletteId() {
        return paletteId;
    }

    public void setPaletteId(String paletteId) {
        this.paletteId = paletteId;
    }

    public String getPreparationUserId() {
        return preparationUser == null ? ( preparationUserId == null ? "" : preparationUserId ): preparationUser.getId();
    }

    public void setPreparationUserId(String preparationUserId) {
        this.preparationUserId = preparationUserId;
    }

    public Date getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(Date preparationDate) {
        this.preparationDate = preparationDate;
    }

    public POSUsersDTO getPreparationUser() {
        return preparationUser;
    }

    public void setPreparationUser(POSUsersDTO preparationUser) {
        this.preparationUser = preparationUser;
    }
    
    public String getInputOrigine() {
        return inputOrigine;
    }
    public void setInputOrigine(String inputOrigine) {
        this.inputOrigine = inputOrigine;
    }

}
