package com.ose.backend.dto.orders;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.ose.backend.model.orders.Orders;

public class PalettePreparationInfoDTO {

    private String preparateurId;
    private String preparateur;
    private String dernierEmplacement;
    private Date dDernierEmplacement;
    private String dateDernierEmplacement;
    private String heureDernierEmplacement;
    private Long nombreEmplacement;
    private Long nombreEmplacementRestant;
    private Integer indicateurProgression;
    private Integer nombrePalette;

    private boolean alert = false;

    public String getPreparateurId() {
        return preparateurId;
    }

    public void setPreparateurId(String preparateurId) {
        this.preparateurId = preparateurId;
    }

    public String getPreparateur() {
        return preparateur;
    }

    public void setPreparateur(String preparateur) {
        this.preparateur = preparateur;
    }

    public String getDernierEmplacement() {
        return dernierEmplacement;
    }

    public void setDernierEmplacement(String dernierEmplacement) {
        this.dernierEmplacement = dernierEmplacement;
    }

    public Date getdDernierEmplacement() {
        return dDernierEmplacement;
    }

    public void setdDernierEmplacement(Date dDernierEmplacement) {
        this.dDernierEmplacement = dDernierEmplacement;
        if (this.dDernierEmplacement != null) {
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfHeure = new SimpleDateFormat("HH:mm:ss");
            setDateDernierEmplacement(sdfDate.format(dDernierEmplacement));
            setHeureDernierEmplacement(sdfHeure.format(dDernierEmplacement));
            updateAlert();
        }
    }

    public Long getNombreEmplacement() {
        return nombreEmplacement;
    }

    public void setNombreEmplacement(Long nombreEmplacement) {
        this.nombreEmplacement = nombreEmplacement;
    }

    public Long getNombreEmplacementRestant() {
        return nombreEmplacementRestant;
    }

    public void setNombreEmplacementRestant(Long nombreEmplacementRestant) {
        this.nombreEmplacementRestant = nombreEmplacementRestant;
    }

    public String getDateDernierEmplacement() {
        return dateDernierEmplacement;
    }

    public void setDateDernierEmplacement(String dateDernierEmplacement) {
        this.dateDernierEmplacement = dateDernierEmplacement;
    }

    public String getHeureDernierEmplacement() {
        return heureDernierEmplacement;
    }

    public void setHeureDernierEmplacement(String heureDernierEmplacement) {
        this.heureDernierEmplacement = heureDernierEmplacement;
    }

    public Integer getIndicateurProgression() {
        return this.indicateurProgression;
    }

    public void setIndicateurProgression(Integer indicateurProgression) {
        this.indicateurProgression = indicateurProgression;
    }

    public Integer getNombrePalette() {
        return nombrePalette;
    }

    public void setNombrePalette(Integer nombrePalette) {
        this.nombrePalette = nombrePalette;
    }

    public boolean isAlert() {
        return alert;
    }

    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    private void updateIndicateurProgression() {
        if (nombreEmplacement != null && nombreEmplacementRestant != null) {
            if (nombreEmplacement != 0) {
                if (nombreEmplacement == nombreEmplacementRestant) {
                    indicateurProgression = -1;
                }
                if (nombreEmplacementRestant == 0) {
                    indicateurProgression = 1;
                } else {
                    indicateurProgression = 0;
                }
            }
        }
    }

    public void addPalette() {
        if (this.nombrePalette == null) {
            this.nombrePalette = 1;
        } else {
            this.nombrePalette++;
        }
    }

    public void addNombreEmplacement(Long nbEmpl) {
        if (nbEmpl != null) {
            this.nombreEmplacement = this.nombreEmplacement != null ? this.nombreEmplacement + nbEmpl : nbEmpl;
        }
        updateIndicateurProgression();
    }

    public void addNombreEmplacementRestant(Long nbEmplRestant) {
        if (nbEmplRestant != null) {
            this.nombreEmplacementRestant = this.nombreEmplacementRestant != null ? this.nombreEmplacementRestant + nbEmplRestant : nbEmplRestant;
        }
        updateIndicateurProgression();
    }

    public void updateAlert() {
        LocalDateTime ldt = dDernierEmplacement.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime alertTime = now.minusMinutes(Orders.TEMPS_MAXI_PREPARATION_MN);
        this.alert = ldt.isBefore(alertTime);
    }

}
