package com.ose.backend.dto.orders;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ose.backend.dto.products.ProductsResultDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailDTO {

    private String id;
    private String orderId;
    private ProductsResultDTO product;
    private double quantityAsked;
    private double quantityToPrepare;
    private double quantityPrepared;
    private double quantitySent;
    private double quantityReceived;
    private double qtAPrepSansPalette;
    
    private double stockTo;
    private double stockFrom;

    private boolean canBePaletized;
    
    private String productPaletteInfo;
    
    private double valoLignePMPA;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductsResultDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsResultDTO product) {
        this.product = product;
    }

    public double getQuantityAsked() {
        return quantityAsked;
    }

    public void setQuantityAsked(double quantityAsked) {
        this.quantityAsked = quantityAsked;
    }

    public double getStockTo() {
        return stockTo;
    }

    public void setStockTo(double stockTo) {
        this.stockTo = stockTo;
    }

    public double getStockFrom() {
        return stockFrom;
    }

    public void setStockFrom(double stockFrom) {
        this.stockFrom = stockFrom;
    }

    public double getQuantityToPrepare() {
        return quantityToPrepare;
    }

    public void setQuantityToPrepare(double quantityToPrepare) {
        this.quantityToPrepare = quantityToPrepare;
    }

    public double getQuantityPrepared() {
        return quantityPrepared;
    }

    public void setQuantityPrepared(double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public boolean isCanBePaletized() {
        return canBePaletized;
    }

    public void setCanBePaletized(boolean canBePaletized) {
        this.canBePaletized = canBePaletized;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }
    
    public String getProductId() {
        return getProduct() == null ? "" : getProduct().getId();
    }

    public double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public double getQtAPrepSansPalette() {
        return qtAPrepSansPalette;
    }

    public void setQtAPrepSansPalette(double qtAPrepSansPalette) {
        this.qtAPrepSansPalette = qtAPrepSansPalette;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductPaletteInfo() {
        return productPaletteInfo;
    }

    public void setProductPaletteInfo(String productPaletteInfo) {
        this.productPaletteInfo = productPaletteInfo;
    }

    public double getValoLignePMPA() {
        return valoLignePMPA;
    }

    public void setValoLignePMPA(double valoLignePMPA) {
        this.valoLignePMPA = valoLignePMPA;
    }
    
    

}
