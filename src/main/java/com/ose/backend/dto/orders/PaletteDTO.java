package com.ose.backend.dto.orders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.security.POSUsersDTO;

public class PaletteDTO {

    private String id;
    private String name;
    private String note;
    private String orderId;
    private String destinationName;

    private POSUsersDTO creationUser;
    private Date creationDate;

    private Date preparationDate;
    private POSUsersDTO preparationUser;
    private String preparationUserId;

    private Date planificationDate;
    private POSUsersDTO planificationUser;
    private String planificationUserId;

    private String status;
    private int statusOrder;

    private List<PaletteDetailDTO> paletteDetails = new ArrayList<>();


    private String controlUserId;
    private Date controlDate;
    private String delivUserId;
    private Date delivDate;
    private String receptUserId;
    private Date receptDate;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PaletteDetailDTO> getPaletteDetails() {
        return paletteDetails;
    }

    public void setPaletteDetails(List<PaletteDetailDTO> paletteDetails) {
        this.paletteDetails = paletteDetails;
    }

    public POSUsersDTO getPreparationUser() {
        return preparationUser;
    }

    public void setPreparationUser(POSUsersDTO preparationUser) {
        this.preparationUser = preparationUser;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPreparationUserId() {
        return preparationUser == null ? ( preparationUserId == null ? "" : preparationUserId ): preparationUser.getId();
    }
    
    public void setPreparationUserId(String preparationUserId) {
        this.preparationUserId = preparationUserId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusOrder() {
        return statusOrder;
    }

    public void setStatusOrder(int statusOrder) {
        this.statusOrder = statusOrder;
    }

    public Date getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(Date preparationDate) {
        this.preparationDate = preparationDate;
    }

    public POSUsersDTO getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(POSUsersDTO creationUser) {
        this.creationUser = creationUser;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getPlanificationDate() {
        return planificationDate;
    }

    public void setPlanificationDate(Date planificationDate) {
        this.planificationDate = planificationDate;
    }

    public POSUsersDTO getPlanificationUser() {
        return planificationUser;
    }

    public void setPlanificationUser(POSUsersDTO planificationUser) {
        this.planificationUser = planificationUser;
    }
    
    public String getPlanificationUserId() {
        return planificationUser == null ? ( planificationUserId == null ? "" : planificationUserId ): planificationUser.getId();
    }

    public void setPlanificationUserId(String planifUserId) {
        this.planificationUserId = planifUserId;
    }

    public String getControlUserId() {
        return controlUserId;
    }

    public void setControlUserId(String controlUserId) {
        this.controlUserId = controlUserId;
    }

    public Date getControlDate() {
        return controlDate;
    }

    public void setControlDate(Date controlDate) {
        this.controlDate = controlDate;
    }

    public String getDelivUserId() {
        return delivUserId;
    }

    public void setDelivUserId(String delivUserId) {
        this.delivUserId = delivUserId;
    }

    public Date getDelivDate() {
        return delivDate;
    }

    public void setDelivDate(Date delivDate) {
        this.delivDate = delivDate;
    }

    public String getReceptUserId() {
        return receptUserId;
    }

    public void setReceptUserId(String receptUserId) {
        this.receptUserId = receptUserId;
    }

    public Date getReceptDate() {
        return receptDate;
    }

    public void setReceptDate(Date receptDate) {
        this.receptDate = receptDate;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public void upgPlanificationDate(Date date) {
        if(date != null) {
            if( this.planificationDate == null || date.before(planificationDate)) {
                    this.planificationDate = date;
                }
            }  
    }

    public void upgPreparationDate(Date date) {
        if(date != null) {
            if( this.preparationDate == null || date.before(preparationDate)) {
                    this.preparationDate = date;
                }
            }
    }

    public void upgControlDate(Date date) {
        if(date != null) {
            if( this.controlDate == null || date.before(controlDate)) {
                    this.controlDate = date;
                }
            }
    }

    public void upgDelivDate(Date date) {
        if(date != null) {
            if( this.delivDate == null || date.before(delivDate)) {
                    this.delivDate = date;
                }
            }
        
    }

    public void upgReceptDate(Date date) {
        if(date != null) {
            if( this.receptDate == null || date.before(receptDate)) {
                    this.receptDate = date;
                }
            }
        
    }

}
