package com.ose.backend.dto.orders;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.response.IXlsResponse;

import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.service.edi.impl.OrdersEdiService;

public class OrderDetailIndexDTO implements IXlsResponse {

    /* Pour des exports-imports dans de bonnes conditions
     * */
    private String internalOrderId;
    private String internalOrderDetailId;
    private String internalPaletteId;
    private String internalPaletteDetailId;
    
    private String orderId;
    private String paletteId;
    private Date date;
    private String status;
    private LocationsInfoDTO origine;
    private LocationsInfoDTO destination;
    
    private ProductsResultDTO product;
    private String inputOrigine;
    private double quantityAsked;
    private double quantityToPrepare;
    private double quantityPrepared;
    private double quantitySent;
    private double quantityReceived;
    
    private String userId;
    private String userName;
    
    private Date orderDate;
    private String orderUserId;
    private String orderUserName;
    
    private Date validationDate;
    private String validationUserId;
    private String validationUserName;
    
    private Date prepareDate;
    private String prepareUserId;
    private String prepareUserName;
    
    private Date sendDate;
    private String sendUserId;
    private String sendUserName;
    
    private Date receptDate;
    private String receptUserId;
    private String receptUserName;
    
    private String note;
    
    public static CellType[] types = { CellType.STRING , CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING , 
        CellType.NUMERIC , CellType.NUMERIC ,
        CellType.NUMERIC , CellType.NUMERIC , CellType.NUMERIC ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING ,
        CellType.STRING , CellType.STRING , CellType.STRING};


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaletteId() {
        return paletteId;
    }

    public void setPaletteId(String paletteId) {
        this.paletteId = paletteId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocationsInfoDTO getOrigine() {
        return origine;
    }

    public void setOrigine(LocationsInfoDTO origine) {
        this.origine = origine;
    }

    public LocationsInfoDTO getDestination() {
        return destination;
    }

    public void setDestination(LocationsInfoDTO destination) {
        this.destination = destination;
    }

    public ProductsResultDTO getProduct() {
        return product;
    }

    public void setProduct(ProductsResultDTO product) {
        this.product = product;
    }

    public String getInputOrigine() {
        return inputOrigine;
    }

    public void setInputOrigine(String inputOrigine) {
        this.inputOrigine = inputOrigine;
    }

    public double getQuantityAsked() {
        return quantityAsked;
    }

    public void setQuantityAsked(double quantityAsked) {
        this.quantityAsked = quantityAsked;
    }

    public double getQuantityToPrepare() {
        return quantityToPrepare;
    }

    public void setQuantityToPrepare(double quantityToPrepare) {
        this.quantityToPrepare = quantityToPrepare;
    }

    public double getQuantityPrepared() {
        return quantityPrepared;
    }

    public void setQuantityPrepared(double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }

    public double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getInternalOrderId() {
        return internalOrderId;
    }

    public void setInternalOrderId(String internalOrderId) {
        this.internalOrderId = internalOrderId;
    }

    public String getInternalOrderDetailId() {
        return internalOrderDetailId;
    }

    public void setInternalOrderDetailId(String internalOrderDetailId) {
        this.internalOrderDetailId = internalOrderDetailId;
    }

    public String getInternalPaletteId() {
        return internalPaletteId;
    }

    public void setInternalPaletteId(String internalPaletteId) {
        this.internalPaletteId = internalPaletteId;
    }

    public String getInternalPaletteDetailId() {
        return internalPaletteDetailId;
    }

    public void setInternalPaletteDetailId(String internalPaletteDetailId) {
        this.internalPaletteDetailId = internalPaletteDetailId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderUserId() {
        return orderUserId;
    }

    public void setOrderUserId(String orderUserId) {
        this.orderUserId = orderUserId == null ? "" : orderUserId;
    }

    public String getOrderUserName() {
        return orderUserName;
    }

    public void setOrderUserName(String orderUserName) {
        this.orderUserName = orderUserName == null ? "" : orderUserName;
    }

    public Date getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(Date validationDate) {
        this.validationDate = validationDate;
    }

    public String getValidationUserId() {
        return validationUserId;
    }

    public void setValidationUserId(String validationUserId) {
        this.validationUserId = validationUserId == null ? "" : validationUserId;
    }

    public String getValidationUserName() {
        return validationUserName;
    }

    public void setValidationUserName(String validationUserName) {
        this.validationUserName = validationUserName == null ? "" : validationUserName;
    }

    public Date getPrepareDate() {
        return prepareDate;
    }

    public void setPrepareDate(Date prepareDate) {
        this.prepareDate = prepareDate;
    }

    public String getPrepareUserId() {
        return prepareUserId;
    }

    public void setPrepareUserId(String prepareUserId) {
        this.prepareUserId = prepareUserId == null ? "" : prepareUserId;
    }

    public String getPrepareUserName() {
        return prepareUserName;
    }

    public void setPrepareUserName(String prepareUserName) {
        this.prepareUserName = prepareUserName == null ? "" : prepareUserName;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(String sendUserId) {
        this.sendUserId = sendUserId == null ? "" : sendUserId;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName == null ? "" : sendUserName;
    }

    public Date getReceptDate() {
        return receptDate;
    }

    public void setReceptDate(Date receptDate) {
        this.receptDate = receptDate;
    }

    public String getReceptUserId() {
        return receptUserId;
    }

    public void setReceptUserId(String receptUserId) {
        this.receptUserId = receptUserId == null ? "" : receptUserId;
    }

    public String getReceptUserName() {
        return receptUserName;
    }

    public void setReceptUserName(String receptUserName) {
        this.receptUserName = receptUserName == null ? "" : receptUserName;
    }

    @Override
    public String[] header() {
        return new String[] {OrdersEdiService.OrderColumns.INT_ORDER_ID , OrdersEdiService.OrderColumns.INT_ODETAIL_ID , 
                OrdersEdiService.OrderColumns.INT_PAL_ID , OrdersEdiService.OrderColumns.INT_PDETAIL_ID , OrdersEdiService.OrderColumns.REF , OrdersEdiService.OrderColumns.DATE , 
                OrdersEdiService.OrderColumns.STATUS , OrdersEdiService.OrderColumns.NOTE , 
                OrdersEdiService.OrderColumns.ORIG_ID , "Origine" , OrdersEdiService.OrderColumns.DEST_ID , 
                "Destination" , OrdersEdiService.OrderColumns.PRODUCT_ID , 
                "Produit" , "Code EAN (scan)", "Code (scan)", OrdersEdiService.OrderColumns.ORDERED_QTY , 
                OrdersEdiService.OrderColumns.VALID_QTY , OrdersEdiService.OrderColumns.PREP_QTY , 
                OrdersEdiService.OrderColumns.DELIV_QTY , OrdersEdiService.OrderColumns.RECEPT_QTY , 
                OrdersEdiService.OrderColumns.PALET_NUM , OrdersEdiService.OrderColumns.USER_ID , "Nom_Opérateur" , 
                OrdersEdiService.OrderColumns.ORDERED_DATE , OrdersEdiService.OrderColumns.ORDERED_USER , "Nom_Opérateur_Commande" ,
                OrdersEdiService.OrderColumns.VALID_DATE , OrdersEdiService.OrderColumns.VALID_USER , "Nom_Opérateur_Validation" ,
                OrdersEdiService.OrderColumns.PREP_DATE , OrdersEdiService.OrderColumns.PREP_USER , "Nom_Opérateur_Preparation" ,
                OrdersEdiService.OrderColumns.DELIV_DATE , OrdersEdiService.OrderColumns.DELIV_USER , "Nom_Opérateur_Reception" ,
                OrdersEdiService.OrderColumns.RECEPT_DATE , OrdersEdiService.OrderColumns.RECEPT_USER , "Nom_Opérateur_Reception"};
    }

    @Override
    public String[] CSV() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dateFormatted = getDate() != null ? df.format(getDate()) : "";
        return new String[] {
                getInternalOrderId() , getInternalOrderDetailId() , getInternalPaletteId() , getInternalPaletteDetailId() ,
                getOrderId() , dateFormatted , getStatus() , getNote() , 
                getOrigine() ==null ? "" : getOrigine().getId() , getOrigine() ==null ? "" : getOrigine().getLabel() ,
                getDestination() ==null ? "" : getDestination().getId() , getDestination() ==null ? "" : getDestination().getLabel() , 
                getProduct() == null ? "" : getProduct().getId() , getProduct() == null ? "" : getProduct().getLabel() ,
                getInputOrigine() != null && getInputOrigine().length() == 13 ? getInputOrigine() : "",
                getInputOrigine() != null && getInputOrigine().length() != 13 ? getInputOrigine() : "",
                Double.toString(getQuantityAsked()) , Double.toString(getQuantityToPrepare()) , Double.toString(getQuantityPrepared()) ,
                Double.toString(getQuantitySent()) , Double.toString(getQuantityReceived()) ,
                getPaletteId()==null ? "" : getPaletteId() , getUserId() , getUserName() ,
                getOrderDate() != null ? df.format(getOrderDate()) : "" , getOrderUserId() , getOrderUserName() ,
                getValidationDate() != null ? df.format(getValidationDate()) : "" , getValidationUserId() , getValidationUserName() ,
                getPrepareDate() != null ? df.format(getPrepareDate()) : "" , getPrepareUserId() , getPrepareUserName() ,
                getSendDate() != null ? df.format(getSendDate()) : "" , getSendUserId() , getSendUserName() ,
                getReceptDate() != null ? df.format(getReceptDate()) : "" , getReceptUserId() , getReceptUserName() };
    }

    @Override
    public CellStyle getHeaderStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CellStyle getDefaultStyle(Workbook workbook) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getColWidth(int index) {
        return 0 ;
    }

}
