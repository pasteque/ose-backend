package com.ose.backend.dto.locations;

public class PaysDTO {

    private String nom;
    private String codeDAE;
    private Long id;
    private String abreviation;
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getCodeDAE() {
        return codeDAE;
    }
    public void setCodeDAE(String codeDAE) {
        this.codeDAE = codeDAE;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getAbreviation() {
        return abreviation;
    }
    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }
        
}
