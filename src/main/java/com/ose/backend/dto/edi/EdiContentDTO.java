package com.ose.backend.dto.edi;

import java.util.ArrayList;
import java.util.List;

public class EdiContentDTO {

    public List<String> articles;
    public List<String> references;
    public List<String> del_references;
    public List<String> deballages;
    public List<String> compositions;
    public List<String> blocages;
    public List<String> emplacements;
    public List<String> univers;
    public List<String> prod_categ;
    public List<String> infos_stocks;
    public List<String> infos_pa;
    public List<String> infos_pmpa;
    public List<String> infos_cat_price;
    public List<String> infos_del_cat_price;
    public List<String> infos_prix_defaut;
    public List<String> stocktypes;
    public List<String> catalog;
    public List<String> user;
    
    public EdiContentDTO() {
        this.articles = new ArrayList<String>();
        this.references = new ArrayList<String>();
        this.deballages = new ArrayList<String>();
        this.compositions = new ArrayList<String>();
        this.blocages = new ArrayList<String>();
        this.emplacements = new ArrayList<String>();
        this.univers = new ArrayList<String>();
        this.prod_categ = new ArrayList<String>();
        this.infos_stocks = new ArrayList<String>();
        this.infos_pa = new ArrayList<String>();
        this.infos_cat_price = new ArrayList<String>();
        this.infos_pmpa = new ArrayList<String>();
        this.infos_prix_defaut = new ArrayList<String>();
        this.stocktypes = new ArrayList<String>();
        this.catalog = new ArrayList<String>();
        this.user = new ArrayList<String>();
        this.del_references = new ArrayList<String>();
        this.infos_del_cat_price = new ArrayList<String>();
        
    }

    public List<String> getArticles() {
        return articles;
    }

    public void setArticles(List<String> articles) {
        this.articles = articles;
    }

    public List<String> getReferences() {
        return references;
    }

    public void setReferences(List<String> references) {
        this.references = references;
    }

    public List<String> getDeballages() {
        return deballages;
    }

    public void setDeballages(List<String> deballages) {
        this.deballages = deballages;
    }

    public List<String> getCompositions() {
        return compositions;
    }

    public void setCompositions(List<String> compositions) {
        this.compositions = compositions;
    }

    public List<String> getBlocages() {
        return blocages;
    }

    public void setBlocages(List<String> blocages) {
        this.blocages = blocages;
    }

    public List<String> getEmplacements() {
        return emplacements;
    }

    public void setEmplacements(List<String> emplacements) {
        this.emplacements = emplacements;
    }

    public List<String> getUnivers() {
        return univers;
    }

    public void setUnivers(List<String> univers) {
        this.univers = univers;
    }

    public List<String> getProd_categ() {
        return prod_categ;
    }

    public void setProd_categ(List<String> prod_categ) {
        this.prod_categ = prod_categ;
    }

    public List<String> getInfos_stocks() {
        return infos_stocks;
    }

    public void setInfos_stocks(List<String> infos_stocks) {
        this.infos_stocks = infos_stocks;
    }

    public List<String> getInfos_pa() {
        return infos_pa;
    }

    public void setInfos_pa(List<String> infos_pa) {
        this.infos_pa = infos_pa;
    }

    public List<String> getInfos_pmpa() {
        return infos_pmpa;
    }

    public void setInfos_pmpa(List<String> infos_pmpa) {
        this.infos_pmpa = infos_pmpa;
    }

    public List<String> getInfos_cat_price() {
        return infos_cat_price;
    }

    public void setInfos_cat_price(List<String> infos_cat_price) {
        this.infos_cat_price = infos_cat_price;
    }

    public List<String> getInfos_prix_defaut() {
        return infos_prix_defaut;
    }

    public void setInfos_prix_defaut(List<String> infos_prix_defaut) {
        this.infos_prix_defaut = infos_prix_defaut;
    }

    public List<String> getStocktypes() {
        return stocktypes;
    }

    public void setStocktypes(List<String> stocktypes) {
        this.stocktypes = stocktypes;
    }

    public List<String> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<String> catalog) {
        this.catalog = catalog;
    }

    public List<String> getDel_references() {
        return del_references;
    }

    public void setDel_references(List<String> del_references) {
        this.del_references = del_references;
    }

    public List<String> getInfos_del_cat_price() {
        return infos_del_cat_price;
    }

    public void setInfos_del_cat_price(List<String> infos_del_cat_price) {
        this.infos_del_cat_price = infos_del_cat_price;
    }

    public List<String> getUser() {
        return user;
    }

    public void setUser(List<String> user) {
        this.user = user;
    }
    
    
}
