package com.ose.backend;

import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * EDU - Suite tutoriels pour utiliser EclipseLink
 */
@Configuration
public class JpaConfiguration extends JpaBaseConfiguration {

    protected JpaConfiguration(DataSource dataSource, JpaProperties properties, ObjectProvider<JtaTransactionManager> jtaTransactionManager) {
        super(dataSource, properties, jtaTransactionManager);
    }

    @Override
    protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
        return new EclipseLinkJpaVendorAdapter();
    }

    @Override
    protected Map<String, Object> getVendorProperties() {
        HashMap<String, Object> map = new HashMap<>();
        map.put(PersistenceUnitProperties.WEAVING, detectWeavingMode());
        //EDU par défaut on ne fait rien mais pourrait peut être être intéressant de créer en auto les tables ?
        //map.put(PersistenceUnitProperties.DDL_GENERATION, "create-tables");
        return map;
    }

    private String detectWeavingMode() {
        return InstrumentationLoadTimeWeaver.isInstrumentationAvailable() ? "true" : "static";
    }
    
    //EDU TODO - Check si c'est nécessaire
//    @Bean("entityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactory(
//      EntityManagerFactoryBuilder builder, DataSource dataSource) {
//
//      return builder
//        .dataSource(dataSource)
//        .packages("com.ose.dao")
//        .persistenceUnit("pasteque").build();
//    }
    
    //TODO gérer les ecritures en batch par ici ?
}
