package com.ose.backend.dao.helper;

public final class CriteriaConstant {

    public static final String FROM = "from";
    public static final String NB_RESULTS = "nbResults";
    public static final String LIST_STATUS_IN = "listStatusIn";
    public static final String LIST_STATUS_NOT_IN = "listStatusNotIn";
    public static final String LIST_CATEGORY_IN = "listCategoryIn";
    public static final String LIST_CATEGORY_NOT_IN = "listCategoryNotIn";
    public static final String LOCATION_ID = "locationId";
    public static final String REFERENCE = "reference";
    public static final String LIKE_REFERENCE = "likeReference";
    public static final String TO = "to";
}
