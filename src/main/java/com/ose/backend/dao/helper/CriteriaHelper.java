package com.ose.backend.dao.helper;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class CriteriaHelper {

    private MultiValueMap<String, Object> criteres = new LinkedMultiValueMap<>();
    private Map<String, Object> parameters = new HashMap<>();

    public CriteriaHelper() {
    }

    public CriteriaHelper(MultiValueMap<String, Object> criteres) {
        this.criteres = criteres;
    }

    public boolean containsKey(String key) {
        return criteres.containsKey(key);
    }

    public Object getFirstFromKey(String key) {
        if (containsKey(key)) {
            return criteres.get(key).get(0);
        }
        return null;
    }

    public String getFirstFromKeyAsString(String key) {
        Object object = getFirstFromKey(key);
        if (object != null) {
            return getFirstFromKey(key).toString();
        }
        return null;
    }

    public Long getFirstFromKeyAsLong(String key) {
        String objectString = getFirstFromKeyAsString(key);
        if (objectString != null) {
            return Long.parseLong(objectString);
        }
        return null;
    }

    public boolean getFirstFromKeyAsBoolean(String key) {
        String objectString = getFirstFromKeyAsString(key);
        if (objectString != null) {
            return Boolean.parseBoolean(objectString);
        }
        return false;
    }

    public Timestamp getFirstFromKeyAsTimestamp(String key) {
        String date = getFirstFromKeyAsString(key);
        return getStringAsTimestamp(date);
    }

    public Date getFirstFromKeyAsDate(String key) {
        String object = getFirstFromKeyAsString(key);
        if (object != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                return sdf.parse(object);
            } catch (Exception exception) {
            }
        }
        return null;
    }

    public static Timestamp getStringAsTimestamp(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'Z");
            Date d = null;
            try {
                d = sdf.parse(date);
            } catch (ParseException e) {
            }
            if (d == null) {
                try {
                    d = sdfISO.parse(date.concat("-0000"));
                } catch (ParseException e) {
                }
            }
            Timestamp timestamp = new Timestamp(d.getTime());
            return timestamp;
        } catch (Exception e) {
        }
        return null;
    }

    public Object getFromKeyAsList(String key) {
        if (containsKey(key)) {
            return criteres.get(key);
        }
        return null;
    }

    public List<String> getFromKeyAsStringList(String key) {
        List<String> list = new ArrayList<>();
        if (containsKey(key)) {
            for (Object obj : criteres.get(key)) {
                list.add(obj.toString());
            }
        }
        return list;
    }

    public Object getFromKeyAsIntergerList(String key) {
        List<Integer> list = new ArrayList<>();
        if (containsKey(key)) {
            for (Object obj : criteres.get(key)) {
                list.add(Integer.parseInt(obj.toString().trim()));
            }
        }
        return list;
    }

    public void addCriteria(String key, Object value) {
        criteres.add(key, value);
    }
    
    public void putCriteria(String key, Object value) {
        List<Object> list = new ArrayList<>();
        list.add(value);
        criteres.put(key, list);
    }

    public void putParameters(String key, Object value) {
        parameters.put(key, value);
    }

    public void fillAllParameters() {
        for (String key : criteres.keySet()) {
            putParameters(key, criteres.get(key).get(0));
        }
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public Timestamp getDateDebut(String paramName) {
        return getFirstFromKeyAsTimestamp(paramName);
    }

    public static Timestamp getDateDebut(Map<String, Object> object, String paramName) {
        return getStringAsTimestamp((String) object.get(paramName));
    }

    public static Timestamp getDateFin(Map<String, Object> object, String paramName) {
        Timestamp date = getStringAsTimestamp((String) object.get(paramName));
        return getEndOfDay(date);
    }

    public static Timestamp getEndOfDay(Timestamp date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return new Timestamp(calendar.getTime().getTime());
    }

    public Timestamp getDateDebut() {
        return getDateDebut("dDebut");
    }

    public Timestamp getDateFin(String paramName) {
        Timestamp date = getFirstFromKeyAsTimestamp(paramName);
        return getEndOfDay(date);
    }

    public Timestamp getDateFin() {
        return getDateFin("dFin");
    }

    public void putCriterias(String key, List<Map<String, Object>> criterias) {
        for (Map<String, Object> map : criterias) {
            for (Entry<String, Object> entry : map.entrySet()) {
                addCriteria(key, entry.getValue());
            }
        }
    }

    public void removeCriteres(String key) {
        if (containsKey(key)) {
            criteres.remove(key);
        }
    }

    public void mutate(String key, String newKey) {
        if (criteres.containsKey(key)) {
            criteres.put(newKey, criteres.get(key));
        }
    }
}
