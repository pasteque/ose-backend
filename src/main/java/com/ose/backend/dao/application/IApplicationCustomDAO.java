package com.ose.backend.dao.application;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.application.Applications;

public interface IApplicationCustomDAO extends IGenericDAO<Applications,String>{

    int getDetailedExtractLimit();

    int getStockHistoryLimit();

    long getSupervisionDetailsInterval();

    long getSupervisionOrdersInterval();

    double getDefaultSecurityLevel();

    long getProductRenflouementInterval();

    List<Object> getCashesStatus(String locationId);
    
    Object getGeneralStatus();

    String getOfbizClientId(String locationId);

    String getOfbizStoreId(String locationId);

    String getFichierRejetImportClient();

}
