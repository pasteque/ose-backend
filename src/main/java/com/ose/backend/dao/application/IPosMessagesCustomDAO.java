package com.ose.backend.dao.application;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.application.PosMessages;

public interface IPosMessagesCustomDAO extends IGenericDAO<PosMessages,String>{

    public String getNextId(String locationId);

    List<PosMessages> findChangeByCriteria(String locationId, String productId,
            String userId, String typeId, Date dfrom, Date dto);
}
