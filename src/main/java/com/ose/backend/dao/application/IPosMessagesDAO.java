package com.ose.backend.dao.application;

import org.pasteque.api.model.application.PosMessages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IPosMessagesDAO extends JpaRepository<PosMessages, String> , JpaSpecificationExecutor<PosMessages> , IPosMessagesCustomDAO {

}
