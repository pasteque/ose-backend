package com.ose.backend.dao.application;

import org.pasteque.api.model.application.Applications;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IApplicationDAO extends JpaRepository<Applications, String> , IApplicationCustomDAO {

}
