package com.ose.backend.dao.application.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

import com.ose.backend.dao.application.IPosMessagesCustomDAO;

public class IPosMessagesCustomDAOImpl extends GenericDAOImpl<PosMessages,String> implements IPosMessagesCustomDAO {

    @SuppressWarnings("unchecked")
    @Override
    public String getNextId(String locationId) {
        String retour = null;
        String requete = "SELECT RIGHT(ID , 5)*1 AS ID_NUM FROM messages ORDER BY ID_NUM desc" ;
        
        Long count = 0L;
        if(locationId == null || locationId.isEmpty()) {
            requete = "SELECT ID*1 AS ID_NUM FROM messages ORDER BY ID_NUM desc" ; 
        }
               
        Query query = entityManager.createNativeQuery(requete);
        List<Double> result = query.getResultList();
        if (result.size() != 0) {
            count = result.get(0).longValue() + 1;
        } 
        if(locationId == null || locationId.isEmpty()) {
            retour =  String.format("%07d",count);
        } else {
            retour =  String.format("%s-%07d", locationId,count);
        }
        return retour;
    }
    
    //TODO reporté tel quel car ça demande réflexion plus poussée pour réécrire avec Spécifications
    @Override
    public List<PosMessages> findChangeByCriteria(String locationId, String productId, String userId, String typeId, Date dfrom,
            Date dto) {
        CriteriaQuery<PosMessages> query = getQuery();
        Root<PosMessages> root = query.from(PosMessages.class);
        List<Predicate> predicates = new ArrayList<>();
        
        if (typeId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(PosMessages.Fields.TYPE).get(PosMessageType.Fields.TYPE), typeId));
        }       
        
        
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(PosMessages.Fields.LOCATIONS).get(Locations.Fields.ID), locationId));
        }
        
        if (productId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(PosMessages.Fields.PRODUCTS).get(Products.Fields.ID), productId));
        }
        
        if (userId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(PosMessages.Fields.PEOPLE).get(People.Fields.ID), userId));
        }
        
        if (dfrom != null && dto == null) {
            predicates.add(getCriteriaBuilder().or(
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(PosMessages.Fields.START_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(PosMessages.Fields.END_DATE), dfrom)));
        }
        if (dto != null && dfrom == null) {
            predicates.add(getCriteriaBuilder().or(
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(PosMessages.Fields.START_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(PosMessages.Fields.END_DATE), dto)));
        }
        if (dto != null && dfrom != null) {
            predicates.add(getCriteriaBuilder().or(
                    getCriteriaBuilder().between(root.<Date> get(PosMessages.Fields.START_DATE), dfrom ,dto),
                    getCriteriaBuilder().between(root.<Date> get(PosMessages.Fields.END_DATE), dfrom ,dto)));
        }
        
        return getResultList(query.where(predicates.toArray(new Predicate[] {})));
    }
}
