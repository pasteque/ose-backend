package com.ose.backend.dao.application.impl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.application.Applications;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.ose.backend.dao.application.IApplicationCustomDAO;


@Repository
public class IApplicationCustomDAOImpl extends GenericDAOImpl<Applications,String> implements IApplicationCustomDAO {
    
    @Value("${spring.data.product.renflouement.interval:60000}")
    private long productRenflouementInterval;

    @Value("${spring.data.supervision.orders.interval:60000}")
    private long supervisionOrdersInterval;
    
    @Value("${spring.data.supervision.details.interval:30000}")
    private long supervisionDetailsInterval;
    
    @Value("${spring.data.stocktype.default.securitylevel:80}")
    private long defaultSecurityLevel;
    
    @Value("${spring.data.extractlimit.stockhistory.maxdays:1}")
    private int stockHistoryMaxday;
    
    @Value("${spring.data.extractlimit.details.maxdays:3}")
    private int detailsMaxDay;
    
    @Value("#{${spring.data.ofbiz.mappingCommandes.clientId: {:}}}")
    private Map<String,String> mappingOfbizClientId;
    
    @Value("#{${spring.data.ofbiz.mappingCommandes.store: {:}}}")
    private Map<String,String> mappingOfbizStore;
    
    @Value("${spring.data.log.fichierRejet.importClient:C:/tmp/rejet_import_ticket_client.rej}")
    private String fichierRejetImportClient;
    
    private static final Map<String, String> defaultMappingOfbizClientId;

    static {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("","OSE");
        map.put("1001","MAG_ANDREZIEUX");
        map.put("1002","APPOIGNY");
        map.put("1003","CHALLANS");
        map.put("1004","COURNON");
        map.put("1005","HYERES");
        map.put("1006","LESGONDS");
        map.put("1007","SAINT-CLAIR");
        map.put("1008","TORCY");
        map.put("1009","MACON");
        map.put("1010","LE_COTEAU");
        map.put("1011","LE_PUY");
        map.put("1012","ANGERS");
        map.put("1013","CAEN");
        map.put("1014","LE_HAVRE");
        map.put("1015","LE_MANS");
        map.put("1016","NANTES");
        map.put("1017","ORLEANS");
        map.put("1018","POITIERS");
        map.put("1019","RENNES");
        map.put("1020","VALENCIENNES");
        
        map.put("16 G","DIAMANT");
        map.put("16 M","DIAMANT");
        map.put("16 R","DIAMANT");
        map.put("16 MY","DIAMANT");
        
        map.put("GC","GRANDCOMPTEI");
        
        defaultMappingOfbizClientId = Collections.unmodifiableMap(map);
    }
    
    private static final Map<String, String> defaultMappingOfbizStore;

    static {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("","INCONNU");
        
        map.put("1001","MAG_ANDREZIEUX");
        map.put("1002","APPOIGNY");
        map.put("1003","CHALLANS");
        map.put("1004","COURNON");
        map.put("1005","HYERES");
        map.put("1006","LESGONDS");
        map.put("1007","SAINT-CLAIR");
        map.put("1008","TORCY");
        map.put("1009","MACON");
        map.put("1010","LE_COTEAU");
        map.put("1011","LE_PUY");
        map.put("1012","ANGERS");
        map.put("1013","CAEN");
        map.put("1014","LE_HAVRE");
        map.put("1015","LE_MANS");
        map.put("1016","NANTES");
        map.put("1017","ORLEANS");
        map.put("1018","POITIERS");
        map.put("1019","RENNES");
        map.put("1020","VALENCIENNES");
        
        map.put("2001","CAMION1");
        map.put("2002","CAMION2");
        map.put("2003","CAMION3");
        map.put("2004","CAMION4");
        map.put("2005","CAMION5");
        map.put("2006","CAMION6");
        map.put("2007","CAMION7");
        map.put("2008","CAMION8");
        map.put("2009","CAMION9");
        map.put("2010","CAMION10");
        map.put("2011","CAMION11");
        map.put("2012","CAMION12");
        map.put("2013","CAMION13");
        map.put("2014","CAMION14");
        map.put("2015","CAMION15");
        map.put("2016","CAMION16");
        map.put("2017","CAMION17");
        map.put("2018","CAMION18");
        map.put("2019","CAMION19");
        map.put("2020","CAMION20");
        map.put("2021","CAMION21");
        map.put("2022","CAMION22");
        map.put("2023","CAMION23");
        map.put("2024","CAMION24");
        map.put("2025","CAMION25");
        map.put("2026","CAMION26");
        
        map.put("16 G","proGuadeloupe");
        map.put("16 M","proMartinique");
        map.put("16 R","proReunion");
        map.put("16 MY","proMayotte");
        
        map.put("GC","ABRATOOLS");
        defaultMappingOfbizStore = Collections.unmodifiableMap(map);
    }
    
    @Override
    public int getDetailedExtractLimit() {
        return detailsMaxDay;
    }

    @Override
    public int getStockHistoryLimit() {
        return stockHistoryMaxday;
    }

    @Override
    public long getSupervisionDetailsInterval() {
        return supervisionDetailsInterval;
    }

    @Override
    public long getSupervisionOrdersInterval() {
        return supervisionOrdersInterval;
    }

    @Override
    public double getDefaultSecurityLevel() {
        return ((double)defaultSecurityLevel)/100;
    }
    @Override
    public long getProductRenflouementInterval() {
        return productRenflouementInterval;
    }

    
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getCashesStatus(String locationId) {
        //v1 : String nativeSQL = " SELECT RESTE.* , DATELASTTICKET FROM ( SELECT MAX(DATENEW) DATELASTTICKET,  closedcash.CASHREGISTER_ID FROM receipts , closedcash WHERE closedcash.MONEY = receipts.MONEY GROUP BY CASHREGISTER_ID ) AS DATETICKET JOIN ( SELECT  LOCATION_ID , locations.NAME POINT_DE_VENTE , CASHREGISTER_ID , cashregisters.NAME CAISSE, MAX(DATESTART) LASTOPEN , MAX(DATEEND) LASTCLOSE , cashregisters.NEXTTICKETID-1 LASTTICKET  FROM closedcash , cashregisters , locations  WHERE cashregisters.ID = CASHREGISTER_ID AND locations.ID = cashregisters.LOCATION_ID  GROUP BY CASHREGISTER_ID ) AS RESTE ON DATETICKET.CASHREGISTER_ID = RESTE.CASHREGISTER_ID ";
        //v2 : String nativeSQL = "SELECT  LOCATION_ID , locations.NAME POINT_DE_VENTE , CASHREGISTER_ID , cashregisters.NAME CAISSE, MAX(DATESTART) LASTOPEN , MAX(DATEEND) LASTCLOSE , MAX(TICKETS.TICKETID) LASTTICKET  , MAX(RECEIPTS.DATENEW) AS DATELASTTICKET FROM closedcash , cashregisters , locations , TICKETS , RECEIPTS WHERE cashregisters.ID = CASHREGISTER_ID AND locations.ID = cashregisters.LOCATION_ID AND RECEIPTS.MONEY = closedcash.MONEY AND TICKETS.ID = RECEIPTS.ID GROUP BY CASHREGISTER_ID ";
        //v3 : On refonde la recherche des dernieres ouvertures / fermetures x25 en vitesse
        String nativeSQL = "SELECT  LOCATION_ID , L.NAME POINT_DE_VENTE , CR.ID , CR.NAME CAISSE, "
                + " CC.DATESTART LASTOPEN , CC2.DATEEND LASTCLOSE , "
                + " MAX(TICKETS.TICKETID) LASTTICKET  , MAX(RECEIPTS.DATENEW) AS DATELASTTICKET , "
                + " L.URL_TEXT , L.SERVER_DEFAULT , false "
                + " FROM locations AS L , cashregisters AS CR JOIN closedcash AS CC ON CC.MONEY = ( "
                + " SELECT p1.MONEY FROM closedcash AS p1 "
                + "   WHERE CR.ID = p1.CASHREGISTER_ID "
                + "   ORDER BY p1.DATESTART DESC LIMIT 1 "
                + " )  JOIN closedcash AS CC2 ON CC2.MONEY = ( "
                + " SELECT p1.MONEY FROM closedcash AS p1 "
                + " WHERE CR.ID = p1.CASHREGISTER_ID AND NOT ISNULL(p1.DATEEND) "
                + " ORDER BY p1.DATEEND DESC LIMIT 1 ) "
                + " , TICKETS , RECEIPTS "  
                + " WHERE L.ID = CR.LOCATION_ID "   
                + " AND RECEIPTS.MONEY = CC.MONEY "
                + " AND TICKETS.ID = RECEIPTS.ID "
                + " AND L.ID = '"+locationId+"'"
                + " GROUP BY CR.ID ";
              
        //Pb sur lastTicket dans version précédente
        Query query = null;
        
        query = entityManager.createNativeQuery(nativeSQL);
        //List<Object> retour = query.getResultList();
        List<Object> retour =  query.getResultList();
        
        for (Object elem : retour) {
            Object[] castedElem = (Object[]) elem;
            
                try {
                    castedElem[10]= castedElem[8] == null ? false : doesURLExist(new URL((String) castedElem[8]));
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            
        }
        return retour;
    }
    
    @Override
    public Object getGeneralStatus() {
        String nativeSQL = "SELECT (SELECT MAX(LASTUPDATE) FROM products ) UPDPRODUCT , (SELECT MAX(DATENEW) FROM stockdiary WHERE LOCATION = '0' AND REASON = 0) UPDSTOCKSIEGE , ( SELECT MAX(FIN) FROM deballages) LASTDEBALLAGE , (SELECT MAX(DATENEW) LASTTICKET  FROM receipts) LASTTICKET";
        Query query = null;
        
        query = entityManager.createNativeQuery(nativeSQL);
        
        @SuppressWarnings("unchecked")
        List<Object> retour = query.getResultList();
        if(retour.size() > 0) {
            return retour.get(0);
        } else {
            return null;
        }
    }
    
    public static boolean doesURLExist(URL url) throws IOException
    {
        // We want to check the current URL
        HttpURLConnection.setFollowRedirects(false);

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        // We don't need to get data
        httpURLConnection.setRequestMethod("HEAD");

        // Some websites don't like programmatic access so pretend to be a browser
        httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
        int responseCode = httpURLConnection.getResponseCode();

        // We accept response code 2xx and 3xx
        return responseCode < 400;
    }
    
    @Override
    public String getOfbizClientId(String locationId) {
        String defaut = defaultMappingOfbizClientId.get(""); 
        String retour = null;
        if(locationId != null) {
            retour = defaultMappingOfbizClientId.get(locationId);
            defaut = retour != null ? retour : defaut ;
            retour = mappingOfbizClientId == null ? null : mappingOfbizClientId.get(locationId);
        }
        
        return retour == null ? defaut : retour;
        
    }
    
    @Override
    public String getOfbizStoreId(String locationId) {
        String defaut = defaultMappingOfbizStore.get(""); 
        String retour = null;
        if(locationId != null) {
            retour = defaultMappingOfbizStore.get(locationId);
            defaut = retour != null ? retour : defaut ;
            retour = mappingOfbizStore == null ? null : mappingOfbizStore.get(locationId);;
        }
        
        return retour == null ? defaut : retour;
    }
    
    @Override
    public String getFichierRejetImportClient() {
        return fichierRejetImportClient;
    }


}
