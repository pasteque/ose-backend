package com.ose.backend.dao.orders;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.orders.Orders;

public interface IOrdersDAO extends JpaRepository<Orders, String> , JpaSpecificationExecutor<Orders> , IOrdersCustomDAO{
    
    Optional<Orders> findFirstByName(String name);

}