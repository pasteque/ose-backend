package com.ose.backend.dao.orders.impl;


import java.util.List;

import javax.persistence.Query;

import org.pasteque.api.dao.generic.GenericDAOImpl;

import com.ose.backend.dao.orders.IOrdersDetailsCustomDAO;
import com.ose.backend.model.orders.OrdersDetails;

public class IOrdersDetailsCustomDAOImpl extends GenericDAOImpl<OrdersDetails , String>implements IOrdersDetailsCustomDAO {


    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> getOrderDetailDTO(String orderId) {
        /**
         * TODO : LE MIEUX SERAIT DE PASSER AVEC L'API CRITERIA POUR LES PB LIES AU REFACTORING, MAIS JE PASSE PAR UNE REQUETE SQL NATIVE,
         * CAR JE NE VOIS PAS COMMENT FAIRE AVEC L'API CRITERIA ...
         * */
       /* String nativeSQL = "SELECT DISTINCT od.ID, pdt.ID, pdt.REFERENCE, pdt.NAME, msg.CONTENU, scTo.UNITS, scFrom.UNITS, "
                + "od.QUANTITY_ASKED, od.QUANTITY_TO_PREPARE, od.QUANTITY_PREPARED, od.QUANTITY_SENT, od.QUANTITY_RECEIVED,"
                + "(SELECT SUM(pd.QUANTITY_TO_PREPARE) FROM PALETTES p " + "LEFT JOIN PALETTES_DETAIL pd ON pd.PALETTE_ID = p.ID "
                + "INNER JOIN ORDERS_DETAIL od ON od.ID=pd.ORDER_DETAIL_ID " + "INNER JOIN ORDERS o ON o.ID=od.ORDERS_ID "
                + "WHERE o.ID=?)" + "FROM ORDERS_DETAIL od " + "INNER JOIN ORDERS o ON o.ID = od.ORDERS_ID "
                + "INNER JOIN LOCATIONS lTo ON lTo.ID=o.LOCATION_TO_ID " + "INNER JOIN LOCATIONS lFrom ON lFrom.ID=o.LOCATION_FROM_ID "
                + "LEFT JOIN PALETTES p ON p.ORDER_ID = o.ID " + "LEFT JOIN PALETTES_DETAIL pd ON pd.PALETTE_ID = p.ID "
                + "INNER JOIN PRODUCTS pdt ON pdt.ID = od.PRODUCT_ID "
                + "LEFT JOIN MESSAGES msg ON (msg.product = pdt.ID AND msg.type = 'EMPL') "
                + "LEFT JOIN STOCKCURRENT scTo ON (scTo.product = pdt.ID AND scTo.LOCATION = lTo.ID) "
                + "LEFT JOIN STOCKCURRENT scFrom ON (scFrom.product = pdt.ID AND scFrom.LOCATION = lFrom.ID) " + "WHERE o.ID = ?";*/
        
        /*String nativeSQL = "SELECT DISTINCT od.ID, pdt.ID, pdt.REFERENCE, pdt.NAME, msg.CONTENU, scTo.UNITS, scFrom.UNITS, "
                + "od.QUANTITY_ASKED, od.QUANTITY_TO_PREPARE, od.QUANTITY_PREPARED, od.QUANTITY_SENT, od.QUANTITY_RECEIVED,"
                + "(SELECT SUM(pd.QUANTITY_TO_PREPARE) "
                + " FROM PALETTES p , PALETTES_DETAIL pd ,ORDERS_DETAIL od,ORDERS o"
                +  " where pd.PALETTE_ID = p.ID "
                +  " and od.ID=pd.ORDER_DETAIL_ID"
                +  " and  o.ID=od.ORDERS_ID and o.ID=?"
                +  " and  pdt.id = od.product_id)" + "FROM ORDERS_DETAIL od " + "INNER JOIN ORDERS o ON o.ID = od.ORDERS_ID "
                + "INNER JOIN LOCATIONS lTo ON lTo.ID=o.LOCATION_TO_ID " + "INNER JOIN LOCATIONS lFrom ON lFrom.ID=o.LOCATION_FROM_ID "
                + "LEFT JOIN PALETTES p ON p.ORDER_ID = o.ID " + "LEFT JOIN PALETTES_DETAIL pd ON pd.PALETTE_ID = p.ID "
                + "INNER JOIN PRODUCTS pdt ON pdt.ID = od.PRODUCT_ID "
                + "LEFT JOIN MESSAGES msg ON (msg.product = pdt.ID AND msg.type = 'EMPL') "
                + "LEFT JOIN STOCKCURRENT scTo ON (scTo.product = pdt.ID AND scTo.LOCATION = lTo.ID) "
                + "LEFT JOIN STOCKCURRENT scFrom ON (scFrom.product = pdt.ID AND scFrom.LOCATION = lFrom.ID) " + "WHERE o.ID = ?";*/
        
        String nativeSQL = " SELECT DISTINCT od.ID, pdt.ID , pdt.REFERENCE, pdt.NAME, msg.CONTENU, scTo.UNITS, scFrom.UNITS, od.QUANTITY_ASKED, od.QUANTITY_TO_PREPARE, "
                +" od.QUANTITY_PREPARED, od.QUANTITY_SENT, od.QUANTITY_RECEIVED,"
                +" (SELECT SUM(pd.QUANTITY_TO_PREPARE) "
                 +" FROM PALETTES p , PALETTES_DETAIL pd ,ORDERS_DETAIL od,ORDERS o"
                 +" where pd.PALETTE_ID = p.ID "
                 +" and od.ID=pd.ORDER_DETAIL_ID"
                 +" and  o.ID=od.ORDERS_ID and o.ID=? "
                 +" and  pdt.id = od.product_id) as qteAprepsurPal, "
                 +" (SELECT SUM(pd.QUANTITY_TO_PREPARE) - SUM(pd.QUANTITY_PREPARED)"
                 +" FROM PALETTES p , PALETTES_DETAIL pd ,ORDERS_DETAIL od,ORDERS o"
                 +" where pd.PALETTE_ID = p.ID "
                 +" and od.ID=pd.ORDER_DETAIL_ID"
                 +" and  o.ID=od.ORDERS_ID and o.ID=? "
                 +" and  pdt.id = od.product_id  and pd.STATUS < 4) as encoursDePrep,"
                 +" od.QUANTITY_TO_PREPARE - od.QUANTITY_PREPARED as qteAPrepSansPal,"
                 + "pdt.stockcost as pmpa"
                 +" FROM ORDERS_DETAIL od INNER JOIN ORDERS o ON o.ID = od.ORDERS_ID "
                 +" INNER JOIN LOCATIONS lTo ON lTo.ID=o.LOCATION_TO_ID "
                 +" INNER JOIN LOCATIONS lFrom ON lFrom.ID=o.LOCATION_FROM_ID "
                 +" LEFT JOIN PALETTES p ON p.ORDER_ID = o.ID LEFT JOIN PALETTES_DETAIL pd ON pd.PALETTE_ID = p.ID "
                 +" INNER JOIN PRODUCTS pdt ON pdt.ID = od.PRODUCT_ID "
                 +" LEFT JOIN MESSAGES msg ON msg.id = ( SELECT id FROM MESSAGES fmsg WHERE fmsg.product = pdt.ID AND fmsg.type = 'EMPL' AND fmsg.LOCATION = lFrom.ID LIMIT 1) "
                 +" LEFT JOIN STOCKCURRENT scTo ON (scTo.product = pdt.ID AND scTo.LOCATION = lTo.ID) "
                 +" LEFT JOIN STOCKCURRENT scFrom ON (scFrom.product = pdt.ID AND scFrom.LOCATION = lFrom.ID) WHERE o.ID = ? ;";
        
        //EDU 2021 02 : On ajoute la notion de valorisation produit PMPA
        Query query = entityManager.createNativeQuery(nativeSQL);
        query.setParameter(1, orderId);
        query.setParameter(2, orderId);
        query.setParameter(3, orderId);

        return query.getResultList();
    }
}
