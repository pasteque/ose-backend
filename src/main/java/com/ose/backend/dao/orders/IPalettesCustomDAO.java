package com.ose.backend.dao.orders;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.products.Products;

import com.ose.backend.model.orders.Palettes;


public interface IPalettesCustomDAO  extends IGenericDAO<Palettes, String>{

    List<Object[]> getPaletteStatusInfoDTO(String orderId);

    List<Map<String, Object>> findAllPalettePreparationInfo(String orderId);

    List<Products> getProductsByEmplacement(String paletteId,
            String emplacement);

    List<Palettes> findByCriteria(String orderId, String paletteName);

    List<Palettes> findByCriteria(String name, String status,
            String statusPalette, String orderId);

    List<Palettes> findByCriteria(String locationId, Date dfrom, Date dto);

}
