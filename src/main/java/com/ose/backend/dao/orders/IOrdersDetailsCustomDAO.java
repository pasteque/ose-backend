package com.ose.backend.dao.orders;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;

import com.ose.backend.model.orders.OrdersDetails;


public interface IOrdersDetailsCustomDAO extends IGenericDAO<OrdersDetails, String>{

    List<Object[]> getOrderDetailDTO(String orderId);

}
