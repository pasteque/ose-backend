package com.ose.backend.dao.orders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.orders.Palettes;

public interface IPalettesDAO extends JpaRepository<Palettes, String>  , JpaSpecificationExecutor<Palettes> , IPalettesCustomDAO {

}