package com.ose.backend.dao.orders.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

import com.ose.backend.dao.orders.IPalettesCustomDAO;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.model.orders.Palettes;
import com.ose.backend.model.orders.PalettesDetails;

public class IPalettesCustomDAOImpl extends GenericDAOImpl<Palettes , String> implements IPalettesCustomDAO{

    @Override
    public List<Palettes> findByCriteria(String orderId, String paletteName) {
        CriteriaQuery<Palettes> query = getQuery();
        Root<Palettes> root = query.from(Palettes.class);
        List<Predicate> predicates = new ArrayList<>();
        if (orderId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.ID), orderId));
        }
        if (paletteName != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.NAME), paletteName));
        }
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
    }

    @Override
    public List<Palettes> findByCriteria(String name, String status, String statusPalette, String orderId) {
        CriteriaQuery<Palettes> query = getQuery();
        Root<Palettes> root = query.from(Palettes.class);
        List<Predicate> predicates = new ArrayList<>();
        if (name != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.PREPARATION_USER).get(People.Fields.USERNAME), name));
        }
        if (orderId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.ID), orderId));
        }
        if (statusPalette != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.STATUS), statusPalette));
        }
        if (status != null) {
            if (statusPalette != null) {
                predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.STATUS), status)));
            } else {
                predicates.add(getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.STATUS), status));
            }
        }
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
    }

    @Override
    public List<Palettes> findByCriteria(String locationId, Date dfrom, Date dto) {
        CriteriaQuery<Palettes> query = getQuery();
        Root<Palettes> root = query.from(Palettes.class);
        List<Predicate> predicates = new ArrayList<>();

        if (locationId != null) {
            predicates.add(getCriteriaBuilder().or(
                    getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.LOCATION_FROM).get(Locations.Fields.ID), locationId),
                    getCriteriaBuilder().equal(root.get(Palettes.Fields.ORDER).get(Orders.Fields.LOCATION_TO).get(Locations.Fields.ID), locationId)));
        }
        if (dfrom != null && dto == null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.CREATION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.PREPARATION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.RECEPTION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.EXPEDITION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.CONTROL_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Palettes.Fields.VALIDATION_DATE), dfrom)));
        }
        if (dto != null && dfrom == null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.CREATION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.PREPARATION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.RECEPTION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.EXPEDITION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.CONTROL_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Palettes.Fields.VALIDATION_DATE), dto)));
        }
        if (dto != null && dfrom != null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.CREATION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.PREPARATION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.RECEPTION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.EXPEDITION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.CONTROL_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Palettes.Fields.VALIDATION_DATE), dfrom, dto)));
        }

        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> getPaletteStatusInfoDTO(String orderId) {
        /**
         * TODO : LE MIEUX SERAIT DE PASSER AVEC L'API CRITERIA POUR LES PB LIES
         * AU REFACTORING, MAIS JE PASSE PAR UNE REQUETE SQL NATIVE, CAR JE NE
         * VOIS PAS COMMENT FAIRE AVEC L'API CRITERIA ...
         */
        /*
         * String nativeSQL =
         * "SELECT DISTINCT t0.ID, t0.STATUS, t0.NAME, t1.NAME, COUNT(DISTINCT(t2.STOCK_LOCATION_NAME)), "
         * +
         * "(SELECT COUNT(DISTINCT(t3.STOCK_LOCATION_NAME)) FROM PALETTES t4 LEFT OUTER JOIN PALETTES_DETAIL t3 ON (t3.PALETTE_ID = t4.ID)"
         * + "WHERE t4.ORDER_ID = ? AND t3.STATUS = 4)" +
         * "FROM PALETTES t0 LEFT OUTER JOIN PEOPLE t1 ON (t1.ID = t0.USER_PREPARATION) "
         * + "LEFT OUTER JOIN PALETTES_DETAIL t2 ON (t2.PALETTE_ID = t0.ID) " +
         * "WHERE (t0.ORDER_ID = ?)";
         */

        String nativeSQL = "SELECT  t0.ID, t0.STATUS, t0.NAME, t1.NAME, " + " COUNT(DISTINCT(t2.STOCK_LOCATION_NAME)), TMP_REQ.REST"

                + " FROM PALETTES t0 LEFT OUTER JOIN PEOPLE t1 ON (t1.ID = t0.USER_PREPARATION) "
                + " LEFT OUTER JOIN PALETTES_DETAIL t2 ON (t2.PALETTE_ID = t0.ID) "
                + " LEFT OUTER JOIN ( SELECT T4.Id , t4.ORDER_ID , COUNT(DISTINCT(pt.STOCK_LOCATION_NAME)) AS REST" + "  FROM PALETTES t4 "
                + " LEFT OUTER JOIN PALETTES_DETAIL pt ON (pt.PALETTE_ID = t4.ID)"
                + " WHERE pt.STATUS < 4 GROUP  BY T4.Id) AS TMP_REQ ON  TMP_REQ.ID = t0.ID AND TMP_REQ.ORDER_ID = ?"
                + " WHERE (t0.ORDER_ID = ?) GROUP BY t0.ID";

        Query query = entityManager.createNativeQuery(nativeSQL);
        query.setParameter(1, orderId);
        query.setParameter(2, orderId);

        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Map<String, Object>> findAllPalettePreparationInfo(String orderId) {

        String nativeSQL = "SELECT ";
        nativeSQL += "pal.ID AS PALETTE_ID, pal.NAME AS PALETTE_NOM, pal.STATUS AS PALETTE_STATUS, ";
        nativeSQL += "pal.USER_PREPARATION AS PREPARATEUR_ID, ";
        nativeSQL += "CONCAT(prep.PRENOM, \" \", prep.NOM) AS PREPARATEUR, ";
        nativeSQL += "COUNT(pDet.ID) AS NB_EMPL_TOTAL, ";
        nativeSQL += "(SELECT COUNT(pd.ID) FROM palettes_detail pd WHERE pd.PALETTE_ID=pal.ID AND pd.QUANTITY_PREPARED=0 AND pd.STATUS<4 AND pd.STATUS<=pal.STATUS) AS NB_EMPL_RESTANT, ";
        nativeSQL += "(SELECT palDetMaxEmp.STOCK_LOCATION_NAME FROM palettes_detail palDetMaxEmp WHERE palDetMaxEmp.PALETTE_ID=pal.ID AND ";
        nativeSQL += "palDetMaxEmp.DPREPARATION=(SELECT MAX(palDetMaxDate.DPREPARATION) FROM palettes_detail palDetMaxDate WHERE palDetMaxDate.PALETTE_ID=pal.ID) LIMIT 1) AS DERNIER_EMPL, ";
        nativeSQL += "(SELECT MAX(palDetMaxDate.DPREPARATION) FROM palettes_detail palDetMaxDate WHERE palDetMaxDate.PALETTE_ID=pal.ID) AS DATE_DERNIER_EMPL ";
        nativeSQL += "FROM palettes pal ";
        nativeSQL += "INNER JOIN people prep ON prep.ID=pal.USER_PREPARATION ";
        nativeSQL += "INNER JOIN palettes_detail pDet ON pDet.PALETTE_ID=pal.ID ";
        nativeSQL += "WHERE 1=1 ";
        nativeSQL += "AND pal.ORDER_ID = ? ";
        nativeSQL += "GROUP BY PALETTE_ID, PREPARATEUR_ID ";
        nativeSQL += "ORDER BY PREPARATEUR, DATE_DERNIER_EMPL ASC ";

        Query query = entityManager.createNativeQuery(nativeSQL);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        query.setParameter(1, orderId);
        return query.getResultList();
    }

        @Override
        public List<Products> getProductsByEmplacement(String paletteId,String emplacement) {
                Palettes palette = this.findByPrimaryKey(paletteId);
                List<PalettesDetails> PaletteDetails = palette.getPalettesDetails();
                List<Products> listProducts = new ArrayList<Products> ();
                for (PalettesDetails paletteDetail:PaletteDetails) {
                        if (paletteDetail.getStockLocationName().equals(emplacement)) {
                                listProducts.add(paletteDetail.getOrderDetail().getProduct());
                        }
                }
                return listProducts;
        }
}
