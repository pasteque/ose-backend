package com.ose.backend.dao.orders.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.util.MultiValueMap;

import com.ose.backend.dao.helper.CriteriaConstant;
import com.ose.backend.dao.helper.CriteriaHelper;
import com.ose.backend.dao.orders.IOrdersCustomDAO;
import com.ose.backend.dao.sqlresult.SQLResultNativeQuery;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.model.orders.Orders;

public class IOrdersCustomDAOImpl extends GenericDAOImpl<Orders , String> implements IOrdersCustomDAO {

    @Override
    public List<Orders> findAllByCriteria(MultiValueMap<String, Object> parameters) {
        CriteriaQuery<Orders> query = getQuery();
        Root<Orders> root = query.from(Orders.class);
        CriteriaHelper criteriaHelper = new CriteriaHelper(parameters);
        List<Predicate> predicates = new ArrayList<>();

        if (criteriaHelper.containsKey(CriteriaConstant.LOCATION_ID)) {
            predicates.add(getCriteriaBuilder().equal(root.get(Orders.Fields.LOCATION_TO).get(Locations.Fields.ID),
                    criteriaHelper.getFirstFromKeyAsString(CriteriaConstant.LOCATION_ID)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.LIST_CATEGORY_IN)) {
            predicates.add(root.get(Orders.Fields.LOCATION_TO).get(Locations.Fields.CATEGORY).get(Categories.Fields.ID)
                    .in(criteriaHelper.getFromKeyAsStringList(CriteriaConstant.LIST_CATEGORY_IN)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.LIST_CATEGORY_NOT_IN)) {
            predicates.add(getCriteriaBuilder().not(root.get(Orders.Fields.LOCATION_TO).get(Locations.Fields.CATEGORY).get(Categories.Fields.ID)
                    .in(criteriaHelper.getFromKeyAsStringList(CriteriaConstant.LIST_CATEGORY_NOT_IN))));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.LIKE_REFERENCE)) {
            String reference = String.format("%%%s%%", criteriaHelper.getFirstFromKeyAsString(CriteriaConstant.LIKE_REFERENCE));
            predicates.add(getCriteriaBuilder().like(root.<String> get(Orders.Fields.NAME), reference));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.REFERENCE)) {
            predicates.add(getCriteriaBuilder().like(root.<String> get(Orders.Fields.NAME),
                    criteriaHelper.getFirstFromKeyAsString(CriteriaConstant.REFERENCE)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.FROM)) {
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.CREATION_DATE),
                    criteriaHelper.getFirstFromKeyAsTimestamp(CriteriaConstant.FROM)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.TO)) {
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.CREATION_DATE),
                    criteriaHelper.getFirstFromKeyAsTimestamp(CriteriaConstant.TO)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.LIST_STATUS_IN)) {
            predicates.add(root.get(Orders.Fields.STATUS).in(criteriaHelper.getFromKeyAsStringList(CriteriaConstant.LIST_STATUS_IN)));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.LIST_STATUS_NOT_IN)) {
            predicates.add(getCriteriaBuilder().not(root.get(Orders.Fields.STATUS).in(criteriaHelper.getFromKeyAsStringList(CriteriaConstant.LIST_STATUS_NOT_IN))));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.NB_RESULTS)) {
            return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
        }
        Order order1 = getCriteriaBuilder().asc(root.get(Orders.Fields.STATUS));
        Order order2 = getCriteriaBuilder().asc(root.get(Orders.Fields.PLANIFICATION_DATE));
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(order1, order2));
    }


    @Override
    public List<Orders> findChangesByCriteria(String locationId, String reference, Date dfrom, Date dto, Integer nbResults) {
        CriteriaQuery<Orders> query = getQuery();
        Root<Orders> root = query.from(Orders.class);
        List<Predicate> predicates = new ArrayList<>();
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().equal(root.get(Orders.Fields.LOCATION_TO).get(Locations.Fields.ID), locationId),
                    getCriteriaBuilder().equal(root.get(Orders.Fields.LOCATION_FROM).get(Locations.Fields.ID), locationId)));
        }
        if (reference != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Orders.Fields.NAME), reference));
        }
        if (dfrom != null && dto == null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.CREATION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.PREPARATION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.PLANIFICATION_DATE), dfrom),
                    getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.RECEPTION_DATE), dfrom)));
        }
        if (dto != null && dfrom == null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.CREATION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.PREPARATION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.RECEPTION_DATE), dto),
                    getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.PLANIFICATION_DATE), dto)));
        }
        if (dto != null && dfrom != null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().between(root.<Date> get(Orders.Fields.CREATION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Orders.Fields.PREPARATION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Orders.Fields.PLANIFICATION_DATE), dfrom, dto),
                    getCriteriaBuilder().between(root.<Date> get(Orders.Fields.RECEPTION_DATE), dfrom, dto)));
        }
        if (nbResults != null && nbResults > 0) {
            return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})), nbResults);
        }
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Object findProductsPaletteInfo(String orderId, String productId) {
        String retour = "--";
        String baseQuery = "SELECT palettes.NAME  , people.NAME , palettes_detail.QUANTITY_PREPARED ,  palettes_detail.QUANTITY_TO_PREPARE "
                + " FROM orders_detail,palettes,palettes_detail,people WHERE orders_detail.ORDERS_ID = '" + orderId + "' AND orders_detail.PRODUCT_ID = '"
                + productId + "' " + " AND palettes_detail.ORDER_DETAIL_ID = orders_detail.ID AND palettes.ID = palettes_detail.PALETTE_ID "
                + " AND people.ID = IFNULL(palettes_detail.USER_PREPARATION , palettes.USER_PREPARATION)";
        Query query = entityManager.createNativeQuery(baseQuery);
        try {
            List<Object[]> retourL = query.getResultList();
            if (retourL.size() > 0) {
                Boolean first = true;
                retour = "";
                for (Object[] element : retourL) {
                    if (!first) {
                        retour += "<BR>";
                    }
                    retour += element[0] + " - " + element[1] + " - " + element[2] + "/" + element[3];
                    first = false;
                }
            }
        } catch (Exception exception) {
            retour = "--";
        }
        return retour;
    }
    
    
    //TODO EDU à reprendre 
    @Override
    public List<Orders> findOrdersRecievedByCriteria(String locationId, String reference, Date dfrom, Date dto) {
        CriteriaQuery<Orders> query = getQuery();
        Root<Orders> root = query.from(Orders.class);
        List<Predicate> predicates = new ArrayList<>();
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().equal(root.get(Orders.Fields.LOCATION_TO).get(Locations.Fields.ID), locationId),
                    getCriteriaBuilder().equal(root.get(Orders.Fields.LOCATION_FROM).get(Locations.Fields.ID), locationId)));
        }
        if (reference != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Orders.Fields.NAME), reference));
        }
        if (dfrom!=null) {
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Orders.Fields.RECEPTION_DATE), dfrom));
        }
        if (dto != null) {
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Orders.Fields.RECEPTION_DATE),dto));
        }
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public OrderDTO getOrderDTONativeQuery(String id) {
        OrderDTO retour = null;
        String baseQuery = "SELECT orders.id , orders.name , dcreation , dplanification , dpreparation , dcontrole , dexpedition , dreception , note , status ,"
                + " location_to_id , lto.name as ltoname , lto.parent_location_id as ltoparent_location_id, lto.categ_id as ltocategory , lto.etat as ltoetat  ,"
                + " location_from_id , lfrom.name as lfromname , lfrom.parent_location_id as ltoparent_location_id , lfrom.categ_id as lfromcategory , lfrom.etat as lfrometat ,"
                + " user_creation , IF(creation_user.nom IS NULL , creation_user.name , CONCAT(creation_user.nom , IF(creation_user.prenom IS NULL , '' , CONCAT(' ', creation_user.prenom ) )) ) as creation_user_longname ,"
                + " user_planification , IF(planification_user.nom IS NULL , planification_user.name , CONCAT(planification_user.nom , IF(planification_user.prenom IS NULL , '' , CONCAT(' ', planification_user.prenom ) )) ) as planification_user_longname , "
                + " user_preparation , IF(preparation_user.nom IS NULL , preparation_user.name , CONCAT(preparation_user.nom , IF(preparation_user.prenom IS NULL , '' , CONCAT(' ', preparation_user.prenom ) )) ) as preparation_user_longname , "
                + " user_controle , IF(controle_user.nom IS NULL , controle_user.name , CONCAT(controle_user.nom , IF(controle_user.prenom IS NULL , '' , CONCAT(' ', controle_user.prenom ) )) ) as user_controle_longname , "
                + " user_expedition , IF(expedition_user.nom IS NULL , expedition_user.name , CONCAT(expedition_user.nom , IF(expedition_user.prenom IS NULL , '' , CONCAT(' ', expedition_user.prenom ) )) ) as expedition_user_longname ,"
                + " user_reception , IF(reception_user.nom IS NULL , reception_user.name , CONCAT(reception_user.nom , IF(reception_user.prenom IS NULL , '' , CONCAT(' ', reception_user.prenom ) )) ) as reception_user_longname "
                + " FROM orders LEFT JOIN locations lfrom on lfrom.id = orders.location_from_id LEFT JOIN locations lto on lto.id = orders.location_to_id "
                + " LEFT JOIN people creation_user ON creation_user.id = user_creation "
                + " LEFT JOIN people planification_user ON planification_user.id = user_planification "
                + " LEFT JOIN people preparation_user ON preparation_user.id = user_preparation "
                + " LEFT JOIN people controle_user ON controle_user.id = user_controle "
                + " LEFT JOIN people expedition_user ON expedition_user.id = user_expedition "
                + " LEFT JOIN people reception_user ON reception_user.id = user_reception "
                + " where orders.id = ?1";
        
        Query query = entityManager.createNativeQuery(baseQuery);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        query.setParameter(1 , id);
        SQLResultNativeQuery order = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
        
        retour = order.getOrderDTOHeader();
        
        

        return retour;
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<OrderDetailBLDTO> getOrderDetailBLDTONativeQuery(String id) {
        List<OrderDetailBLDTO> retour = null;
        String baseQuery = "SELECT o.name as orderId , p.name as paletteId, pr.id as productId, pr.NAME as productLabel , IF( d.INPUT_ORIGINE is NULL OR LENGTH(d.INPUT_ORIGINE) <> 13 , ref.REFERENCE , d.INPUT_ORIGINE) as productEAN , d.QUANTITY_SENT as qtySent, IF( o.status = 6 , d.QUANTITY_RECEIVED , NULL)  as qtyReceived FROM orders o join palettes p on p.order_id = o.id  join palettes_detail d on d.palette_id = p.id "
                + "join orders_detail od on od.id = d.order_detail_id "
                + "join products pr on pr.id = od.product_id "
                + "join products_ref ref on  ref.product_id = pr.id "
                + "where o.id = ?1 "
                + "group by d.id,pr.id "
                + "order by p.name , pr.name";
        Query query = entityManager.createNativeQuery(baseQuery);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        query.setParameter(1 , id);
        SQLResultNativeQuery order = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
        retour = order.getOrderDetailBLDTOList();
        return retour;
    }

}
