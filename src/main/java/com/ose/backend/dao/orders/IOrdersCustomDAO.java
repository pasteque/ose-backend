package com.ose.backend.dao.orders;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.springframework.util.MultiValueMap;

import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.model.orders.Orders;

public interface IOrdersCustomDAO extends IGenericDAO<Orders, String>{

    Object findProductsPaletteInfo(String orderId, String productId);

    List<Orders> findChangesByCriteria(String locationId, String reference,
            Date dfrom, Date dto, Integer nbResults);

    List<Orders> findAllByCriteria(MultiValueMap<String, Object> parameters);

    List<Orders> findOrdersRecievedByCriteria(String locationId,
            String reference, Date dfrom, Date dto);
    
    OrderDTO getOrderDTONativeQuery(String id);
    
    List<OrderDetailBLDTO> getOrderDetailBLDTONativeQuery(String id);

}
