package com.ose.backend.dao.orders;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ose.backend.model.orders.OrdersDetails;

public interface IOrdersDetailsDAO extends JpaRepository<OrdersDetails, String> , IOrdersDetailsCustomDAO{

}