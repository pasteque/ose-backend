package com.ose.backend.dao.orders;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ose.backend.model.orders.PalettesDetails;

public interface IPalettesDetailsDAO extends JpaRepository<PalettesDetails, String> {

}