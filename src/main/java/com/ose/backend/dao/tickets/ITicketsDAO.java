package com.ose.backend.dao.tickets;

import org.pasteque.api.model.tickets.Tickets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ITicketsDAO extends JpaRepository<Tickets, String> , JpaSpecificationExecutor<Tickets> , ITicketsCustomDAO{

}
