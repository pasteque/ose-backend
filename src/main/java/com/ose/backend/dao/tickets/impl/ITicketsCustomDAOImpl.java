package com.ose.backend.dao.tickets.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.springframework.util.StringUtils;

import com.ose.backend.constants.RequestMappingNames;
import com.ose.backend.dao.sqlresult.SQLResultNativeQuery;
import com.ose.backend.dao.tickets.ITicketsCustomDAO;
import com.ose.backend.dto.tickets.TicketsIndexDTO;

public class ITicketsCustomDAOImpl extends GenericDAOImpl<Tickets, String> implements ITicketsCustomDAO {

    @Override
    public List<Object[]> getTopX(String locationId, String tourId,  String pricelistId, int x, List<String> prodRestrictions, List<String> catRestrictions, String dimension, String from, String to) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Tickets> root = query.from(Tickets.class);
        List<Predicate> predicates = new ArrayList<>();
        List<Selection<?>> selections = new ArrayList<>();
        Predicate prodPredicate = null;
        Predicate catPredicate = null;

        Join<Tickets, Receipts> receipt = root.join(Tickets.Fields.RECEIPTS);
        Join<Tickets, TicketLines> lines = root.join(Tickets.Fields.LINES);
        Join<TicketLines, Products> product = lines.join(TicketLines.Fields.PRODUCT);

        if (locationId != null) {
            predicates.add(getCriteriaBuilder().or(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                    .get(Locations.Fields.ID), locationId) ,
                    getCriteriaBuilder().equal(
                            receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                            .get(Locations.Fields.CATEGORY).get(Categories.Fields.NAME), locationId)));
        }

        if (tourId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.TOUR)
                    .get(Tours.Fields.TOUR_ID), tourId) );
        }

        if (pricelistId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    root.get(Tickets.Fields.TARIFF_AREA).get(TariffAreas.Fields.ID), pricelistId) );
        }

        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(
                    root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW), dFrom));
        }
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW),
                    dTo));
        }
        if (prodRestrictions != null && !prodRestrictions.isEmpty()) {
            prodPredicate = product.get(Products.Fields.ID).in(prodRestrictions);
        }
        if (catRestrictions != null && !catRestrictions.isEmpty()) {
            catPredicate = product.get(Products.Fields.CATEGORY).get(Categories.Fields.ID).in(catRestrictions);
        }

        if(prodPredicate != null) {
            if(catPredicate != null) {
                predicates.add(getCriteriaBuilder().or(prodPredicate , catPredicate));
            } else {
                predicates.add(prodPredicate);
            }
        }else {
            if(catPredicate != null) {
                predicates.add(catPredicate);
            }
        }

        selections.add(product.get(Products.Fields.CODE));
        selections.add(product.get(Products.Fields.NAME));
        if (dimension != null && dimension.equals("units")) {
            selections.add(getCriteriaBuilder().sum(lines.<Double> get(TicketLines.Fields.UNITS)));
        }
        if (dimension != null && dimension.equals("value")) {
            selections.add(getCriteriaBuilder().sum(
                    getCriteriaBuilder().prod(lines.<Double> get(TicketLines.Fields.FINAL_PRICE),
                                    getCriteriaBuilder().diff(1D,
                                            lines.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE))
                                    )));
        }

        Order order = getCriteriaBuilder().desc(getCriteriaBuilder().sum(lines.<Double> get(TicketLines.Fields.UNITS)));
        if (dimension != null && dimension.equals("value")) {
            order = getCriteriaBuilder().desc(
                    getCriteriaBuilder().sum(
                            getCriteriaBuilder().prod(lines.<Double> get(TicketLines.Fields.FINAL_PRICE),
                                    getCriteriaBuilder().diff(1D,
                                            lines.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE))
                                    )));
        }

        query.multiselect(selections).where(predicates.toArray(new Predicate[] {}))
        .groupBy(product.get(Products.Fields.CODE), product.get(Products.Fields.NAME)).orderBy(order);

        TypedQuery<Object[]> tq;
        if (x > 0) {
            tq = entityManager.createQuery(query).setMaxResults(x);
        } else {
            tq = entityManager.createQuery(query);
        }

        return tq.getResultList();

    }

    @Override
    public Object[] findBySalesSessions(List<ClosedCash> closedCashs) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Tickets> root = query.from(Tickets.class);
        List<Selection<?>> selections = new ArrayList<>();
        List<Predicate> predicates = new ArrayList<>();

        if (!closedCashs.isEmpty()) {
            predicates.add(root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.CLOSED_CASH).in(closedCashs));
        }

        Join<Tickets, TicketLines> join1 = root.join(Tickets.Fields.LINES);
        selections.add(getCriteriaBuilder().countDistinct(root.get(Tickets.Fields.RECEIPTS).get(Receipts.Fields.ID)));

        selections.add(getCriteriaBuilder().sum(
                getCriteriaBuilder().prod(join1.<Double> get(TicketLines.Fields.FINAL_PRICE),
                        getCriteriaBuilder().diff(1D,
                                join1.get(TicketLines.Fields.TICKET).<Double> get(Tickets.Fields.DISCOUNT_RATE))
                        )));


        query.multiselect(selections).where(predicates.toArray(new Predicate[] {}));
        TypedQuery<Object[]> tq = entityManager.createQuery(query);

        Object[] retour = tq.getSingleResult();

        if(retour != null && retour.length == 2) {
            retour  = Arrays.copyOf(retour , 3);
            //EDU Ajouter les taxes afin de pouvoir avoir le CA TTC
            selections = new ArrayList<>();
            predicates = new ArrayList<>();
            Root<Receipts> receipts = query.from(Receipts.class);
            query = getCriteriaBuilder().createQuery(Object[].class);

            if (!closedCashs.isEmpty()) {
                predicates.add(receipts.get(Receipts.Fields.CLOSED_CASH).in(closedCashs));
            }
            Join<Receipts, Taxes> joinRecTax = receipts.join(Receipts.Fields.TAXES);   
            selections.add(getCriteriaBuilder().sum(joinRecTax.<Double> get(Taxes.Fields.AMOUNT)));
            query.multiselect(selections).where(predicates.toArray(new Predicate[] {}));
            tq = entityManager.createQuery(query);

            retour[2] = tq.getSingleResult();
        }
        return retour;
    }

    /**
     * 
     * @return une Map avec les données suivants :
     * Heure
     * Numéro Ticket
     * Id Point de vente
     * Nom point de vente
     * Id tournée
     * Nom tournée
     */
    @SuppressWarnings("unchecked")
    @Override
    public
    Map<String,Object> getLastTicketInformations(String cashRegisterId , String tourneeId){
        String base = "Select t.ticketId as "+ RequestMappingNames.Tickets.TICKETID +", r.datenew as "+ RequestMappingNames.Tickets.TICKETDATE +" , "
                + " tour.id as "+ RequestMappingNames.Tournees.TOURID +", tour.nom as "+ RequestMappingNames.Tournees.TOURNAME +" , "
                + " l.id as "+ RequestMappingNames.Locations.LOCATIONID +", l.name as "+ RequestMappingNames.Locations.LOCATIONNAME +" "
                + " FROM cashregisters cr "
                + "left join closedcash cc on cc.cashregister_id = cr.id "
                + "left join deballages d on d.id = cc.deballage_id "
                + "left join tournees tour on tour.id = d.tournees_id "
                + "left join receipts r on cc.money = r.money "
                + "left join locations l on l.id = cr.location_id "
                + "join tickets t on  r.id = t.id "
                + "WHERE ";
        String condCr = cashRegisterId == null ? "" : " cr.id = '"+ cashRegisterId +"' ";
        String condDeb = tourneeId == null ? "" : " d.tournees_id = '"+ tourneeId +"' " ;
        String order= " order by datenew desc LIMIT 1 ";
        Query query = entityManager.createNativeQuery(base + condCr +  condDeb + order);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map); 
        try {
        return (Map<String, Object>) query.getSingleResult();
        } catch ( NoResultException ex ) {
            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public SQLResultNativeQuery findTicketOrderWhithoutCustomers(String dateStart) {
        //EDU 2021-04 
        //Refont nativequery + prise en charge extended
        //Retour demandé
        //location.getLabel(), tournee, String.valueOf(ticket.getTicketId()) closedCash.getSalesLocation().getTour().getId()
        String base = "SELECT ticketid as ticket, locations.name as location , tournees.id as numTournee, tournees.nom as nomTournee FROM ticketlines , tickets left join tickets_extended ON tickets.id = tickets_extended.TICKET_ID  "
                + ", receipts , closedcash  left join deballages on deballages.ID = closedcash.DEBALLAGE_ID LEFT JOIN tournees on tournees.ID = deballages.TOURNEES_ID , cashregisters , locations "
                + "WHERE tickets.date > '" + dateStart + "' AND tickets.id = ticketlines.ticket and  ticketlines.type = 'O' "
                + "and tickets.CUSTOMER is null "
                + "and tickets_extended.CUSTOMER_ID is null "
                + "and receipts.id = tickets.id "
                + "and closedcash.money = receipts.money "
                + "and cashregisters.ID = closedcash.CASHREGISTER_ID "
                + "and locations.id = cashregisters.LOCATION_ID "
                + "group by ticketid" ;

        Query query = entityManager.createNativeQuery(base);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        
        
        SQLResultNativeQuery custQuery = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
        return custQuery;
        
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public
    List<TicketsHistoryDTO> findCustomerTickets(String customerId){
//        private Date date;
//        private long ticketId;
//        private int productsUnit;
//        private double ticketsAmount;
//        private int ticketNumber;
//        private String locationsName;
        String cust = customerId == null ? "-" : customerId ;
        String base = "SELECT * FROM (select date as date , ticketid as ticketId, count(*) as productsUnit , "
                + " tickets.finalTaxedPrice as ticketsAmount ,  locations.NAME as locationsName "
                + "from tickets , receipts , closedcash , cashregisters , locations , ticketlines "
                + "where tickets.id = receipts.id "
                + "AND ticketlines.TICKET = tickets.id "
                + "AND closedcash.money = receipts.money "
                + "AND cashregisters.id = closedcash.CASHREGISTER_ID "
                + "AND locations.id = cashregisters.LOCATION_ID "
                + "AND customer in ( "
                + "select id from customers where id = '"+ cust +"' or id_parent = '"+ cust +"') "
                + "group by ticketId "
                + "UNION select date as date , ticketid as ticketId, count(*) as productsUnit , "
                + " tickets.finalTaxedPrice as ticketsAmount ,  locations.NAME as locationsName "
                + "from tickets , receipts , closedcash , cashregisters , locations , ticketlines "
                + "where tickets.id = receipts.id "
                + "AND ticketlines.TICKET = tickets.id "
                + "AND closedcash.money = receipts.money "
                + "AND cashregisters.id = closedcash.CASHREGISTER_ID "
                + "AND locations.id = cashregisters.LOCATION_ID "
                + "AND tickets.id in ( select TICKET_ID from tickets_extended where tickets_extended.customer_id in ( "
                + "select id from customers where id = '"+ cust +"' or id_parent = '"+ cust +"') ) "
                + "group by ticketId ) t "
                + "order by date desc";

        Query query = entityManager.createNativeQuery(base);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        
        SQLResultNativeQuery order = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
            
        return order.getTicketsHistoryDTO();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ILoadable> DtoSQLParameters findByTicketIndexDTO(TicketsIndexDTO ticketDTO, Class<T> selector) {
        T instance;
        DtoSQLParameters model;
        List<Object[]> result;
        int paramIndex = 0;
        
        try {
            instance = selector.newInstance();
            model = instance.getSQLParameters();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return (new POSTicketsDTO()).getSQLParameters();
        }
        
        String nativeSQL_0 = model.select;

        //construction tableau param et chaine
        if (ticketDTO.getId() != null && ! ticketDTO.getId().isEmpty()) {
            model.constraintList.add(" (t0.ID = ?) ");
            model.parameterList.put(++paramIndex, ticketDTO.getId());
        }  
        
        nativeSQL_0 = nativeSQL_0.concat(StringUtils.collectionToCommaDelimitedString(model.tableList));
        if (model.constraintList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("WHERE ").concat(StringUtils.collectionToDelimitedString(model.constraintList, " AND "));
        }
        if(model.groupBy != null) {
            nativeSQL_0 = nativeSQL_0.concat("GROUP BY ").concat(model.groupBy) ;
        }
        if (model.havingList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("HAVING ").concat(StringUtils.collectionToDelimitedString(model.havingList, " AND "));
        }
        
        Query query = entityManager.createNativeQuery(nativeSQL_0);
        //injection param
        for(Map.Entry<Integer, Object> entry : model.parameterList.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        result = query.getResultList();
        for(Object[] line : result) {
            line[0] = ticketDTO ;
        }
        model.setResult( result );
        
        return model;
    }

}
