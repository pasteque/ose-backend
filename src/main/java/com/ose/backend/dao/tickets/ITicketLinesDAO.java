package com.ose.backend.dao.tickets;

import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.TicketLinesId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ITicketLinesDAO extends JpaRepository<TicketLines, TicketLinesId> , JpaSpecificationExecutor<TicketLines> , ITicketLinesCustomDAO{

}
