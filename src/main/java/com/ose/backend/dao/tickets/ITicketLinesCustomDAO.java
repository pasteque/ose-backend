package com.ose.backend.dao.tickets;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.TicketLinesId;

public interface ITicketLinesCustomDAO extends IGenericDAO<TicketLines, TicketLinesId>{

    Double findNbSell(String productsId, String locationId, int nbDays);

    List<Object[]> findNbSellByLocation(String productsId, int nbDays,
            int totDays);

}
