package com.ose.backend.dao.tickets;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.tickets.TicketsExtended;

public interface ITicketsExtendedDAO extends JpaRepository<TicketsExtended, String> , JpaSpecificationExecutor<TicketsExtended>{


}
