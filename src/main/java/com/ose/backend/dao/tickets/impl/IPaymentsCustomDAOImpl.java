package com.ose.backend.dao.tickets.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.Tickets;

import com.ose.backend.dao.tickets.IPaymentsCustomDAO;

public class IPaymentsCustomDAOImpl extends GenericDAOImpl<Payments, String> implements IPaymentsCustomDAO {

    @Override
    public List<Payments> getDetailsPayment(String locationId, String payment, String from, String to, String saleLocationId, 
            String tourId) {
        CriteriaQuery<Payments> query = getQuery();
        Root<Tickets> root = query.from(Tickets.class);
        List<Predicate> predicates = new ArrayList<>();

        Join<Tickets, Receipts> receipt = root.join(Tickets.Fields.RECEIPTS);
        Join<Receipts, Payments> payments = receipt.join(Receipts.Fields.PAYMENTS);

        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                            .get(Locations.Fields.ID), locationId));
        }
        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(
                    root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW), dFrom));
        }
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW),
                    dTo));
        }
        if (tourId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.TOUR).get(Tours.Fields.TOUR_ID), tourId));
        }
        if (saleLocationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.SALES_LOCATION_ID), saleLocationId));
        }
        if (payment != null) {
            predicates.add(getCriteriaBuilder().equal(payments.get(Payments.Fields.PAYMENT), payment));
        }
        
        Order order = getCriteriaBuilder().desc(receipt.get(Receipts.Fields.DATE_NEW));

        return getResultList(query.select(payments).where(predicates.toArray(new Predicate[] {})).orderBy(order));
    }
    
    @Override
    public List<Object[]> getPaymentsInfo(String locationId, String from, String to, String saleLocationId, String tourId) {
        CriteriaQuery<Object[]> query = getCriteriaBuilder().createQuery(Object[].class);
        Root<Tickets> root = query.from(Tickets.class);
        List<Predicate> predicates = new ArrayList<>();
        List<Selection<?>> selections = new ArrayList<>();

        Join<Tickets, Receipts> receipt = root.join(Tickets.Fields.RECEIPTS);
        Join<Receipts, Payments> payments = receipt.join(Receipts.Fields.PAYMENTS);

        predicates.add(getCriteriaBuilder().notEqual(
                payments.<Double> get(Payments.Fields.TOTAL), 0D));
        
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                            .get(Locations.Fields.ID), locationId));
        }
        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(
                    root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW), dFrom));
        }
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.get(Tickets.Fields.RECEIPTS).<Date> get(Receipts.Fields.DATE_NEW),
                    dTo));
        }
        if (tourId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.TOUR).get(Tours.Fields.TOUR_ID), tourId));
        }
        if (saleLocationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.SALES_LOCATION_ID), saleLocationId));
        }
        selections.add(payments.get(Payments.Fields.PAYMENT));
        selections.add(getCriteriaBuilder().sum(payments.<Double> get(Payments.Fields.TOTAL)));
        
        //EDU - 2017 03 Ajout du nombre
        selections.add(getCriteriaBuilder().count(payments.get(Payments.Fields.ID)));

        Order order = getCriteriaBuilder().desc(payments.get(Payments.Fields.PAYMENT));

        query.multiselect(selections).where(predicates.toArray(new Predicate[] {})).groupBy(payments.get(Payments.Fields.PAYMENT))
                .orderBy(order);

        TypedQuery<Object[]> tq = entityManager.createQuery(query);
        return tq.getResultList();
    }
    
    @Override
    public List<Payments> getPayments(String locationId, String payment, String from, String to, String saleLocationId, 
            String tourId, String movementTypeConstraint , String closedCashId) {
        CriteriaQuery<Payments> query = getQuery();
        Root<Receipts> receipt = query.from(Receipts.class);
        List<Predicate> predicates = new ArrayList<>();

        Join<Receipts, Payments> payments = receipt.join(Receipts.Fields.PAYMENTS);

        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                            .get(Locations.Fields.ID), locationId));
        }
        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(
                    receipt.<Date> get(Receipts.Fields.DATE_NEW), dFrom));
        }
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(receipt.<Date> get(Receipts.Fields.DATE_NEW),
                    dTo));
        }
        if (tourId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.TOUR).get(Tours.Fields.TOUR_ID), tourId));
        }
        if (saleLocationId != null) {
            predicates.add(getCriteriaBuilder().equal(
                    receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.SALES_LOCATION).get(SalesLocations.Fields.SALES_LOCATION_ID), saleLocationId));
        }
        if (payment != null) {
            predicates.add(getCriteriaBuilder().equal(payments.get(Payments.Fields.PAYMENT), payment));
        }
        
        if(movementTypeConstraint != null) {
            ArrayList<String> plist= Payments.getCategPayments(movementTypeConstraint);
            List<Predicate> orpredicates = new ArrayList<>();
            for(String element : plist){
                orpredicates.add(getCriteriaBuilder().equal(payments.get(Payments.Fields.PAYMENT),element));
            }
            predicates.add(getCriteriaBuilder().or(orpredicates.toArray(new Predicate[] {})));
        }
        
        if(closedCashId != null) {
            predicates.add(getCriteriaBuilder().equal(receipt.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.MONEY), closedCashId));
        }
        
        Order order = getCriteriaBuilder().desc(receipt.get(Receipts.Fields.DATE_NEW));

        return getResultList(query.select(payments).where(predicates.toArray(new Predicate[] {})).orderBy(order));
    }
}
