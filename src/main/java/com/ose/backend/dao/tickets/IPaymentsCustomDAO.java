package com.ose.backend.dao.tickets;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.tickets.Payments;

public interface IPaymentsCustomDAO extends IGenericDAO<Payments, String>{

    List<Payments> getDetailsPayment(String locationId, String payment,
            String from, String to, String saleLocationId, String tourId);

    List<Object[]> getPaymentsInfo(String locationId, String from, String to,
            String saleLocationId, String tourId);

    List<Payments> getPayments(String locationId, String payment, String from,
            String to, String saleLocationId, String tourId,
            String movementTypeConstraint, String closedCashId);


}
