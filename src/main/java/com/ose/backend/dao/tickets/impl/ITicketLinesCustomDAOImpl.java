package com.ose.backend.dao.tickets.impl;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Query;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.TicketLinesId;
import org.springframework.stereotype.Repository;

import com.ose.backend.dao.tickets.ITicketLinesCustomDAO;

@Repository
@Cacheable(value = false)
public class ITicketLinesCustomDAOImpl extends GenericDAOImpl<TicketLines , TicketLinesId> implements ITicketLinesCustomDAO {
    
    @Override
    public Double findNbSell(String productsId, String locationId, int nbDays) {
        
        /*
         * 
         * SELECT SUM(UNITS) FROM PRODUCTS,TICKETLINES,RECEIPTS,closedcash,cashregisters,locations WHERE 
CASHREGISTERS.LOCATION_ID = LOCATIONS.ID
AND closedcash.CASHREGISTER_ID = CASHREGISTERS.ID
AND RECEIPTS.MONEY = closedcash.MONEY
AND TICKETLINES.TICKET = RECEIPTS.ID
AND TICKETLINES.PRODUCT = PRODUCTS.ID
AND LOCATIONS.ID = '1001' AND PRODUCTS.ID = '1234'
AND RECEIPTS.DATENEW > DATE_ADD(current_date() , INTERVAL -20 DAY);
         */
        
        String baseQuery = "SELECT SUM(UNITS) FROM PRODUCTS,TICKETLINES,RECEIPTS,closedcash,cashregisters,locations WHERE " 
         + " CASHREGISTERS.LOCATION_ID = LOCATIONS.ID "
         + " AND closedcash.CASHREGISTER_ID = CASHREGISTERS.ID "
         + " AND RECEIPTS.MONEY = closedcash.MONEY "
         + " AND TICKETLINES.TICKET = RECEIPTS.ID "
         + " AND TICKETLINES.PRODUCT = PRODUCTS.ID ";
        
        String params = String.format( "AND LOCATIONS.ID = '%s' AND PRODUCTS.ID = '%s' AND RECEIPTS.DATENEW > DATE_ADD(current_date() , INTERVAL -%d DAY)", locationId ,productsId , nbDays);
        
        /*CriteriaQuery<Double> query = entityManager.getCriteriaBuilder().createQuery(Double.class);
        Root<TicketLines> root = query.from(TicketLines.class);

        Join<TicketLines, Products> join = root.join(TicketLines.Fields.PRODUCT);
        Join<TicketLines, Receipts> join2 = root.join(TicketLines.Fields.TICKET).join(Tickets.Fields.RECEIPTS);

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -nbDays);
        Predicate predicate1 = getCriteriaBuilder().equal(join.get(Products.Fields.ID), productsId);
        Predicate predicate2 = getCriteriaBuilder().greaterThan(join2.<Date> get(Receipts.Fields.DATE_NEW), calendar.getTime());
        Predicate predicate3 = getCriteriaBuilder().equal(
                join2.get(Receipts.Fields.CLOSED_CASH).get(ClosedCash.Fields.CASH_REGISTER).get(CashRegisters.Fields.LOCATION)
                        .get(Locations.Fields.ID), locationId);
    
        try {
            return entityManager.createQuery(
                    query.select(getCriteriaBuilder().sum(root.<Double> get(TicketLines.Fields.UNITS)))
                            .where(predicate1, predicate2, predicate3).groupBy(join.get(Products.Fields.ID))).getSingleResult();
        } catch (Exception exception) {
            return 0D;
        } */
        Query query = entityManager.createNativeQuery(baseQuery+params);

        try {
            Object retour = query.getSingleResult();
            return ((retour == null) ? 0D : (Double) retour );
        } catch (Exception exception) {
            return 0D;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findNbSellByLocation(String productsId, int nbDays, int totDays ) {
        
        /*
         * 
         * SELECT LOCATIONS.ID , SUM(UNITS) FROM PRODUCTS,TICKETLINES,RECEIPTS,closedcash,cashregisters,locations WHERE 
CASHREGISTERS.LOCATION_ID = LOCATIONS.ID
AND closedcash.CASHREGISTER_ID = CASHREGISTERS.ID
AND RECEIPTS.MONEY = closedcash.MONEY
AND TICKETLINES.TICKET = RECEIPTS.ID
AND TICKETLINES.PRODUCT = PRODUCTS.ID 
AND PRODUCTS.ID = '1234'
AND RECEIPTS.DATENEW > DATE_ADD(current_date() , INTERVAL -20 DAY) GROUP BYLOCATIONS.ID;
         */
        
     // TODO MODIF Pref EDU
     // 1 jours tous les jours regroupé ou pas le temps est constant
     // En effet ça dépend du nombre de mouvements pour ce produit
     // Du coup autant le faire une seule fois et redécouper / répartir après en cas de besoin
     // Exemple ventes tous emplacements à 7 et 30 jours -> un passage au lieu de deux gain 50%
     // 
//        SELECT CASHREGISTERS.LOCATION_ID , SUM(UNITS) , IF( DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL 8 DAY) , 7 , 30 ) AS TYPE_GROUP
//        FROM TICKETLINES,RECEIPTS,closedcash,cashregisters 
//        WHERE closedcash.CASHREGISTER_ID = CASHREGISTERS.ID  
//        AND RECEIPTS.MONEY = closedcash.MONEY  
//        AND TICKETLINES.TICKET = RECEIPTS.ID  
//        AND TICKETLINES.PRODUCT = '1234' 
//        AND DATE(RECEIPTS.DATENEW) > DATE_ADD(current_date() , INTERVAL -31 DAY) 
//        GROUP BY CASHREGISTERS.LOCATION_ID , TYPE_GROUP 
//        ORDER BY CASHREGISTERS.LOCATION_ID ASC , TYPE_GROUP ASC;
        
        String baseQuery = String.format( "SELECT CASHREGISTERS.LOCATION_ID , SUM(UNITS) , IF( DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL %d DAY) , %d , %d ) AS TYPE_GROUP " 
         + " FROM TICKETLINES,RECEIPTS,closedcash,cashregisters "
         + " WHERE closedcash.CASHREGISTER_ID = CASHREGISTERS.ID "
         + " AND RECEIPTS.MONEY = closedcash.MONEY "
         + " AND TICKETLINES.TICKET = RECEIPTS.ID ", 1+nbDays , nbDays , totDays);
        
        String params = String.format( "AND TICKETLINES.PRODUCT = '%s' AND DATE(RECEIPTS.DATENEW) > DATE_ADD(current_date() , INTERVAL %d DAY) ", productsId , 1+totDays);
        String group = " GROUP BY CASHREGISTERS.LOCATION_ID , TYPE_GROUP " 
                        + " ORDER BY CASHREGISTERS.LOCATION_ID ASC , TYPE_GROUP ASC";

        Query query = entityManager.createNativeQuery(baseQuery+params+group);
        

        try {
            return query.getResultList();
        } catch (Exception exception) {
            return null;
        }
        
        
    }
    
}
