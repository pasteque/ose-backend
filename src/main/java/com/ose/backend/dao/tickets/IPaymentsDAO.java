package com.ose.backend.dao.tickets;

import org.pasteque.api.model.tickets.Payments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IPaymentsDAO extends JpaRepository<Payments, String> , JpaSpecificationExecutor<Payments> , IPaymentsCustomDAO {

}
