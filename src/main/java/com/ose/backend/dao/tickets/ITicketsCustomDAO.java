package com.ose.backend.dao.tickets;

import java.util.List;
import java.util.Map;
import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.tickets.Tickets;

import com.ose.backend.dao.sqlresult.SQLResultNativeQuery;
import com.ose.backend.dto.tickets.TicketsIndexDTO;

public interface ITicketsCustomDAO extends IGenericDAO<Tickets, String>{

    List<Object[]> getTopX(String locationId, String tourId, String pricelistId,
            int x, List<String> prodRestrictions, List<String> catRestrictions,
            String dimension, String from, String to);

    Object[] findBySalesSessions(List<ClosedCash> closedCashs);

    /**
     * 
     * @return une Map avec les données suivants :
     * Heure
     * Numéro Ticket
     * Id Point de vente
     * Nom point de vente
     * Id tournée
     * Nom tournée
     */
    Map<String, Object> getLastTicketInformations(String cashRegisterId,
            String tourneeId);

    SQLResultNativeQuery findTicketOrderWhithoutCustomers(String dateStart);

    List<TicketsHistoryDTO> findCustomerTickets(String customerId);

    <T extends ILoadable> DtoSQLParameters findByTicketIndexDTO(
            TicketsIndexDTO ticketDTO, Class<T> selector);

}
