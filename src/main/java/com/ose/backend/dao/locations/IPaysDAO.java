package com.ose.backend.dao.locations;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ose.backend.model.locations.Pays;


public interface IPaysDAO extends JpaRepository<Pays, Long> {
    
    Pays findByNom(String nom);

}