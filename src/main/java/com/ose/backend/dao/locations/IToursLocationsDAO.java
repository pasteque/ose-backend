package com.ose.backend.dao.locations;

import org.pasteque.api.model.saleslocations.ToursLocations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IToursLocationsDAO extends JpaRepository<ToursLocations, Long> , JpaSpecificationExecutor<ToursLocations>{

}
