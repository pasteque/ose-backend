package com.ose.backend.dao.sqlresult;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.util.DateHelper;

import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.model.orders.Orders;

public class SQLResultNativeQuery {

    public SQLResultNativeQuery(List<Map<String, Object>> mymap){
        this.mapList = (mymap == null ) ? new ArrayList<Map<String, Object>>() : mymap;
    }

    List<Map<String, Object>> mapList;

    public String getValueAsString(int index, String key) {
        return getValueAsString(getElement(index), key);
    }

    public Date getValueAsDate(int index, String key) {
        return getValueAsDate(getElement(index), key);
    }

    public Long getValueAsLong(int index, String key) {
        return getValueAsLong(getElement(index), key);
    }

    public Double getValueAsDouble(int index, String key) {
        return getValueAsDouble(getElement(index), key);
    }
    
    public Integer getValueAsInteger(int index, String key) {
        return getValueAsInteger(getElement(index), key);
    }


    public String getValueAsString(Map<String, Object> map, String key) {
        if (map != null && map.containsKey(key)) {
            if (map.get(key) != null) {
                return (String) map.get(key);
            }
        }
        return null;
    }

    public Date getValueAsDate(Map<String, Object> map, String key) {
        if (map != null && map.containsKey(key)) {
            if (map.get(key) != null) {
                return DateHelper.parse(map.get(key).toString(), DateHelper.SQLTIMESTAMP) ;
            }
        }
        return null;
    }

    public Long getValueAsLong(Map<String, Object> map, String key) {
        if (map != null && map.containsKey(key)) {
            if (map.get(key) != null) {
                return Long.parseLong(map.get(key).toString());
            }
        }
        return null;
    }
    
    public Double getValueAsDouble(Map<String, Object> map, String key) {
        if (map != null && map.containsKey(key)) {
            if (map.get(key) != null) {
                return Double.parseDouble(map.get(key).toString());
            }
        }
        return null;
    }

    public Integer getValueAsInteger(Map<String, Object> map, String key) {
        if (map != null && map.containsKey(key)) {
            if (map.get(key) != null) {
                return Integer.parseInt(map.get(key).toString());
            }
        }
        return null;
    }

    public int getListSize() {
        return mapList == null ? 0 : mapList.size();
    }

    public Map<String,Object> getElement(int index) {
        return mapList == null || index < 0 || index >= getListSize() ? new HashMap<String,Object>() : mapList.get(index);
    }

    public OrderDTO getOrderDTOHeader() {

        OrderDTO retour = null; 
        if(getListSize() > 0) {
            retour = new OrderDTO();

            retour.setId(getValueAsString(0 , Orders.Columns.ID));
            retour.setStatusOrder(getValueAsInteger(0 , Orders.Columns.STATUS));
            retour.setStatus(Orders.Status.getStatusLabel(String.valueOf(retour.getStatusOrder())));
            retour.setCreationDate(getValueAsDate(0 , Orders.Columns.CREATION_DATE));
            retour.setPlanifDate(getValueAsDate(0 , Orders.Columns.PLANIFICATION_DATE));
            retour.setPrepDate(getValueAsDate(0 , Orders.Columns.PREPARATION_DATE));
            retour.setControlDate(getValueAsDate(0 , Orders.Columns.CONTROLE_DATE));
            retour.setDelivDate(getValueAsDate(0 , Orders.Columns.EXPEDITION_DATE));
            retour.setReceptDate(getValueAsDate(0 ,Orders.Columns.RECEPTION_DATE));
            retour.setReference(getValueAsString(0 , Orders.Columns.NAME));
            retour.setNote(getValueAsString(0 , Orders.Columns.NOTE));

            String tmpString = getValueAsString(0 , Orders.Columns.LOCATION_TO);
            if(tmpString != null) {
                LocationsInfoDTO to = new LocationsInfoDTO();
                to.setId(tmpString);
                to.setLabel(getValueAsString(0, "ltoname" ));
                to.setParentId(getValueAsString(0, "ltoparent_location_id"));
                to.setCategory(getValueAsString(0, "ltocategory"));
                to.setEtat(getValueAsString(0, "ltoetat"));
                retour.setLocationTo(to);
            }

            tmpString = getValueAsString(0 , Orders.Columns.LOCATION_FROM);
            if(tmpString != null) {
                LocationsInfoDTO from = new LocationsInfoDTO();
                from.setId(tmpString);
                from.setLabel(getValueAsString(0, "lfromname" ));
                from.setParentId(getValueAsString(0, "lfromparent_location_id"));
                from.setCategory(getValueAsString(0, "lfromcategory"));
                from.setEtat(getValueAsString(0, "lfrometat"));
                retour.setLocationFrom(from);
            }

            tmpString = getValueAsString(0 , Orders.Columns.CREATION_USER);
            if (tmpString != null) {
                retour.setUsernameId(tmpString);
                retour.setUsername(getValueAsString(0, "creation_user_longname"));
            }

            tmpString = getValueAsString(0 , Orders.Columns.PLANIFICATION_USER);
            if (tmpString != null) {
                retour.setPlanifUserId(tmpString);
                retour.setPlanifUserName(getValueAsString(0, "planification_user_longname"));
            }

            tmpString = getValueAsString(0 , Orders.Columns.PREPARATION_USER);
            if (tmpString != null) {
                retour.setPrepUserId(tmpString);
                retour.setPrepUserName(getValueAsString(0, "preparation_user_longname"));
            }

            tmpString = getValueAsString(0 , Orders.Columns.CONTROLE_USER);
            if (tmpString != null) {
                retour.setControlUserId(tmpString);
                retour.setControlUserName(getValueAsString(0, "controle_user_longname"));
            }

            tmpString = getValueAsString(0 , Orders.Columns.EXPEDITION_USER);
            if (tmpString != null) {
                retour.setDelivUserId(tmpString);
                retour.setDelivUserName(getValueAsString(0, "expedition_user_longname"));
            }

            tmpString = getValueAsString(0 , Orders.Columns.RECEPTION_USER);
            if (tmpString != null) {
                retour.setReceptUserId(tmpString);
                retour.setReceptUserName(getValueAsString(0, "reception_user_longname"));
            }

        }
        return retour;

    }

    public List<TicketsHistoryDTO> getTicketsHistoryDTO() {
        List<TicketsHistoryDTO> retour = new ArrayList<TicketsHistoryDTO>();
        for(int index = 0 ; index < getListSize() ; index++) {
            TicketsHistoryDTO e = new TicketsHistoryDTO();
            e.setDate(getValueAsDate(index, "date"));
            e.setLocationsName(getValueAsString(index, "locationsName"));
            e.setProductsUnit(getValueAsInteger(index, "productsUnit"));
            e.setTicketId(getValueAsLong(index,"ticketId"));
            e.setTicketsAmount(getValueAsDouble(index,"ticketsAmount"));
            e.setTicketNumber( getListSize() - index);
            
            retour.add(e );
        }
        return retour;
    }
    
    public List<OrderDetailBLDTO> getOrderDetailBLDTOList() {
        List<OrderDetailBLDTO> retour = new ArrayList<OrderDetailBLDTO>();
        for(int index = 0 ; index < getListSize() ; index++) {
            OrderDetailBLDTO e = new OrderDetailBLDTO();
            e.setOrderId(getValueAsString(index,"orderId"));
            e.setPaletteId(getValueAsString(index,"paletteId"));
            e.setProductId(getValueAsString(index,"productId"));
            e.setProductLabel(getValueAsString(index,"productLabel"));
            e.setProductEAN(getValueAsString(index,"productEAN"));
            e.setQuantityPrepared(getValueAsDouble(index,"qtySent"));
            
            e.setQuantityReceived(getValueAsDouble(index,"qtyReceived"));
            
            retour.add(e);
        }
        return retour;
    }



}
