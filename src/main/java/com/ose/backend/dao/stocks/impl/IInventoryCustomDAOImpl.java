package com.ose.backend.dao.stocks.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.stereotype.Repository;

import com.ose.backend.dao.stocks.IInventoryCustomDAO;
import com.ose.backend.model.stocks.Inventory;
import com.ose.backend.model.stocks.InventoryDetails;

@Repository
public class IInventoryCustomDAOImpl extends GenericDAOImpl<Inventory, String> implements IInventoryCustomDAO {

    @Override
    public List<Inventory> findByCriteria(String locationId, Date from, Date to, Long page, Long nbResults, String type, String status) {
        CriteriaQuery<Inventory> query = getQuery();
        Root<Inventory> root = query.from(Inventory.class);
        List<Predicate> predicates = new ArrayList<>();
        Order order = getCriteriaBuilder().desc(root.get(Inventory.Fields.START_DATE));
        if (locationId != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Inventory.Fields.LOCATION).get(Locations.Fields.ID), locationId));
        }
        if (from != null) {
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(Inventory.Fields.START_DATE), from));
        }
        if (to != null) {
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Inventory.Fields.START_DATE), to));
        }
        if (type != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Inventory.Fields.TYPE), type));
        }
        if (status != null) {
            predicates.add(getCriteriaBuilder().equal(root.get(Inventory.Fields.STATUS), status));
        }
        if (nbResults != null) {
            //TODO Prendre en compte les infos max result et page
            //Sans doute intervenir sur le genericDAO en ce sens pour eviter de repartir par entitymanager etc etc
            return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})));
        }
        return getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(order));
    }

    @Override
    public long sumQuantity(String inventoryId, String productId) {
        CriteriaQuery<Long> query = entityManager.getCriteriaBuilder().createQuery(Long.class);
        Root<InventoryDetails> root = query.from(InventoryDetails.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(getCriteriaBuilder().equal(root.get(InventoryDetails.Fields.INVENTORY).get(Inventory.Fields.ID), inventoryId));
        predicates.add(getCriteriaBuilder().equal(root.get(InventoryDetails.Fields.PRODUCT).get(Products.Fields.ID), productId));

        Long result = entityManager.createQuery(
                query.select(getCriteriaBuilder().sum(root.<Long> get(InventoryDetails.Fields.QUANTITY))).where(
                        predicates.toArray(new Predicate[] {}))).getSingleResult();
        return result != null ? result.longValue() : 0;
    }
}
