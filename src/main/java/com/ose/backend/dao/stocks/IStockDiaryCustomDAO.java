package com.ose.backend.dao.stocks;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;

import com.ose.backend.model.stocks.StockDiary;

public interface IStockDiaryCustomDAO extends IGenericDAO<StockDiary, String> {

    public <T extends ILoadable> DtoSQLParameters  findByCriteriaAggr(String locationId, String productsId, 
            String  productsCatList, String locationsCatList, 
            String reference, String type , String dateFrom , String group , Class<T> selector);

    Double findNbChange(String productsId, String locationId, Date dateStart,
            Date dateEnd, Integer reasonConstraint);

    List<StockDiary> findByCriteria(String locationId, String productsId,
            String reference, String type, String dateFrom, String dateTo,
            Long from, Long nbResults, String sort);

    Double findNbSell(String productsId, String locationId, int nbDays);

    List<Object[]> findNbSellByLocation(String productsId, int nbDays,
            int totDays);
}
