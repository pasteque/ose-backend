package com.ose.backend.dao.stocks.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.springframework.stereotype.Repository;

import com.ose.backend.dao.stocks.IInventoryDetailsCustomDAO;
import com.ose.backend.model.stocks.Inventory;
import com.ose.backend.model.stocks.InventoryDetails;

@Repository
public class IInventoryDetailsCustomDAOImpl extends GenericDAOImpl<InventoryDetails , String> implements IInventoryDetailsCustomDAO {

    @Override
    public List<InventoryDetails> findByInventory(String inventoryId , int limit) {
        CriteriaQuery<InventoryDetails> query = getQuery();
        Root<InventoryDetails> root = query.from(InventoryDetails.class);
        Predicate predicate = getCriteriaBuilder().equal(root.get(InventoryDetails.Fields.INVENTORY).get(Inventory.Fields.ID), inventoryId);
        Order order = getCriteriaBuilder().desc(root.get(InventoryDetails.Fields.DATE));
        if(limit == 0) {
            limit = 20;
        }
        if(limit > 0) {
            return getResultList(query.select(root).where(predicate).orderBy(order), 20);
        } else {
            return getResultList(query.select(root).where(predicate).orderBy(order));
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findByInventoryGrouped(String inventoryId, String reference) {
        
        //On avait un problème lorsqu'on trouve des produits n'ayant jamais eu leur stock initialisé dans le magasin en question
        //
        //La requête suivante est OK
        /*SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , TMP_INVENT.QTY_INV , IFNULL(STOCK.UNITS , 0) AS STOCK  FROM 
        ( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME, SUM(t1.QUANTITY) AS QTY_INV
                FROM  INVENTORY_DETAILS t1 , PRODUCTS t0, INVENTORY t4 
                WHERE t1.inventory = '1467281379167-2014' AND t4.ID = t1.inventory AND t0.ID = t1.product  
                GROUP BY t0.REFERENCE, t0.NAME ) AS TMP_INVENT 
                LEFT OUTER JOIN STOCKCURRENT STOCK  ON ( STOCK.product = TMP_INVENT.product 
                AND STOCK.location = TMP_INVENT.LOCATION )
                ORDER BY TMP_INVENT.REFERENCE ASC ;
                */
        /*
                         * SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , TMP_INVENT.QTY_INV , IFNULL(STOCK.UNITS , 0) AS STOCK  FROM 
                ( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME, SUM(t1.QUANTITY) AS QTY_INV
                FROM  INVENTORY_DETAILS t1 , PRODUCTS t0 LEFT OUTER JOIN PRODUCTS_REF t5 ON T5.PRODUCT_ID = T0.ID , INVENTORY t4 
                WHERE t1.inventory = '1467281379167-2014' AND t4.ID = t1.inventory AND t0.ID = t1.product AND ( T5.REFERENCE LIKE '%76948%' OR T0.CODE LIKE '%76948%' OR T0.NAME LIKE '%76948%' OR T0.ID LIKE '%76948%' OR T0.REFERENCE LIKE '%76948%')  
                GROUP BY t0.REFERENCE, t0.NAME ) AS TMP_INVENT 
                LEFT OUTER JOIN STOCKCURRENT STOCK  ON ( STOCK.product = TMP_INVENT.product 
                AND STOCK.location = TMP_INVENT.LOCATION )
                ORDER BY TMP_INVENT.REFERENCE ASC ;
                
                SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , TMP_INVENT.QTY_INV , IFNULL(STOCK.UNITS , 0) AS STOCK , IF(TMP_INVENT.BUY = 0 , TMP_INVENT.COST , TMP_INVENT.BUY) AS PMPA FROM 
                ( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME, SUM(t1.QUANTITY) AS QTY_INV , IFNULL(t0.PRICEBUY , 0 ) AS BUY, IFNULL(t0.STOCKCOST , 0 ) AS COST
                FROM  INVENTORY_DETAILS t1 , PRODUCTS t0 LEFT OUTER JOIN PRODUCTS_REF t5 ON T5.PRODUCT_ID = T0.ID , INVENTORY t4 
                WHERE t1.inventory = '1471350348347-1001' AND t4.ID = t1.inventory AND t0.ID = t1.product AND ( T5.REFERENCE LIKE '%4142%' OR T0.CODE LIKE '%4142%' OR T0.NAME LIKE '%4142%' OR T0.ID LIKE '%4142%' OR T0.REFERENCE LIKE '%4142%')  
                GROUP BY t0.REFERENCE, t0.NAME ) AS TMP_INVENT 
                LEFT OUTER JOIN STOCKCURRENT STOCK  ON ( STOCK.product = TMP_INVENT.product 
                AND STOCK.location = TMP_INVENT.LOCATION )
                ORDER BY TMP_INVENT.REFERENCE ASC ;

SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , 0 , STOCK.UNITS AS STOCK  , IF(TMP_INVENT.BUY = 0 , TMP_INVENT.COST , TMP_INVENT.BUY) AS PMPA FROM 
                ( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME , IFNULL(t0.PRICEBUY , 0 ) AS BUY, IFNULL(t0.STOCKCOST , 0 ) AS COST
                FROM   INVENTORY t4 ,  PRODUCTS t0 LEFT OUTER JOIN INVENTORY_DETAILS t1 ON t1.inventory= '1471350348347-1001' AND t0.ID = t1.product 
                WHERE t4.id = '1471350348347-1001' AND ISNULL(t1.product)
                GROUP BY t0.REFERENCE, t0.NAME) AS TMP_INVENT
                JOIN STOCKCURRENT STOCK ON ( STOCK.UNITS <>0 AND STOCK.product = TMP_INVENT.reference AND STOCK.location = TMP_INVENT.LOCATION )
                ORDER BY TMP_INVENT.REFERENCE ASC 
                 ;

         */
        
        //On a un problème sur les références non comptées
        //On fait la fonction soeur avec les ref non inventoriées
        
        String nativeSQL_0 = " SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , TMP_INVENT.QTY_INV , IFNULL(STOCK.UNITS , 0) AS STOCK , IF(TMP_INVENT.BUY = 0 , TMP_INVENT.COST , TMP_INVENT.BUY) AS PMPA , TMP_INVENT.LOCATION FROM "
                          +"( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME, SUM(t1.QUANTITY) AS QTY_INV , IFNULL(t0.PRICEBUY , 0 ) AS BUY, IFNULL(t0.STOCKCOST , 0 ) AS COST"
                          +" FROM  INVENTORY_DETAILS t1 , PRODUCTS t0 ";
        String nativeSQL_1 =" , INVENTORY t4 WHERE t1.inventory =? AND t4.ID = t1.inventory AND t0.ID = t1.product ";
        String nativeSQL_ref_0 = " LEFT OUTER JOIN PRODUCTS_REF t5 ON T5.PRODUCT_ID = T0.ID ";
        String nativeSQL_ref_1 = "AND ( T5.REFERENCE LIKE ? OR T0.CODE LIKE ? OR T0.NAME LIKE ? OR T0.ID LIKE ? OR T0.REFERENCE LIKE ?) ";
        String nativeSQL_end =" GROUP BY t0.REFERENCE, t0.NAME ) AS TMP_INVENT " 
                          +" LEFT OUTER JOIN STOCKCURRENT STOCK  ON ( STOCK.product = TMP_INVENT.product AND STOCK.location = TMP_INVENT.LOCATION ) "
                          +" ORDER BY TMP_INVENT.REFERENCE ASC ";
        
        if (reference != null) {
            reference = "%" + reference + "%";
            nativeSQL_0 = nativeSQL_0.concat(nativeSQL_ref_0).concat(nativeSQL_1).concat(nativeSQL_ref_1);
        }
        else {
            nativeSQL_0 = nativeSQL_0.concat(nativeSQL_1);
        }
        nativeSQL_0 = nativeSQL_0.concat(nativeSQL_end);
        Query query = entityManager.createNativeQuery(nativeSQL_0);
        query.setParameter(1, inventoryId);
        
        if (reference != null) {
            query.setParameter(2, reference);
            query.setParameter(3, reference);
            query.setParameter(4, reference);
            query.setParameter(5, reference);
            query.setParameter(6, reference);
        }

        return query.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findMissingCountsByInventoryGrouped(String inventoryId) {
        
        /*

SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , 0 , STOCK.UNITS AS STOCK  , IF(TMP_INVENT.BUY = 0 , TMP_INVENT.COST , TMP_INVENT.BUY) AS PMPA FROM 
                ( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME , IFNULL(t0.PRICEBUY , 0 ) AS BUY, IFNULL(t0.STOCKCOST , 0 ) AS COST
                FROM   INVENTORY t4 ,  PRODUCTS t0 LEFT OUTER JOIN INVENTORY_DETAILS t1 ON t1.inventory= '1471350348347-1001' AND t0.ID = t1.product 
                WHERE t4.id = '1471350348347-1001' AND ISNULL(t1.product)
                GROUP BY t0.REFERENCE, t0.NAME) AS TMP_INVENT
                JOIN STOCKCURRENT STOCK ON ( STOCK.UNITS <>0 AND STOCK.product = TMP_INVENT.reference AND STOCK.location = TMP_INVENT.LOCATION )
                ORDER BY TMP_INVENT.REFERENCE ASC 
                 ;

         */
        
        //On a un problème sur les références non comptées
        //On fait la fonction soeur avec les ref non inventoriées
        //Uniquement valide pour des inventaires complets
        
        String nativeSQL_0 = " SELECT TMP_INVENT.REFERENCE , TMP_INVENT.NAME , 0 AS QTY_INV ,STOCK.UNITS AS STOCK , IF(TMP_INVENT.BUY = 0 , TMP_INVENT.COST , TMP_INVENT.BUY) AS PMPA , TMP_INVENT.LOCATION FROM "
                          +"( SELECT DISTINCT t1.product , t4.LOCATION, t0.REFERENCE, t0.NAME, SUM(t1.QUANTITY) AS QTY_INV , IFNULL(t0.PRICEBUY , 0 ) AS BUY, IFNULL(t0.STOCKCOST , 0 ) AS COST "
                          +"FROM   INVENTORY t4 ,  PRODUCTS t0 LEFT OUTER JOIN INVENTORY_DETAILS t1 ON t1.inventory= ? AND t0.ID = t1.product "
                          +"WHERE t4.id = ? AND t4.CTYPE = 'COMPLET' AND ISNULL(t1.product) GROUP BY t0.REFERENCE, t0.NAME) AS TMP_INVENT "
                          +"JOIN STOCKCURRENT STOCK ON ( STOCK.UNITS <>0 AND STOCK.product = TMP_INVENT.reference AND STOCK.location = TMP_INVENT.LOCATION ) "
                          +"ORDER BY TMP_INVENT.REFERENCE ASC ";

        

        Query query = entityManager.createNativeQuery(nativeSQL_0);
        query.setParameter(1, inventoryId);
        query.setParameter(2, inventoryId);

        return query.getResultList();
    }

}
