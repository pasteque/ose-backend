package com.ose.backend.dao.stocks;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import com.ose.backend.model.stocks.Inventory;

public interface IInventoryCustomDAO extends IGenericDAO<Inventory, String>{

    long sumQuantity(String inventoryId, String productId);

    List<Inventory> findByCriteria(String locationId, Date from, Date to,
            Long page, Long nbResults, String type, String status);
    
}
