package com.ose.backend.dao.stocks.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.stereotype.Repository;

import com.ose.backend.dao.stocks.IStockLevelCustomDAO;
import com.ose.backend.model.stocks.StockLevel;

@Repository
public class IStockLevelCustomDAOImpl extends GenericDAOImpl<StockLevel , String> implements IStockLevelCustomDAO {
    
    //TODO on peut faire sauter avec des Specifications 
    @Override
    public List<StockLevel> findListByCriteria(List<String> productsId, List<String> locationsId,
            Date dateFrom, Date dateTo) {
        CriteriaQuery<StockLevel> query = getQuery();
        Root<StockLevel> root = query.from(StockLevel.class);
        List<Predicate> predicates = new ArrayList<>();
        List<Order> orderList = new ArrayList<>();

        if (productsId != null && !productsId.isEmpty()) {
            predicates.add(root.get(StockLevel.Fields.PRODUCT).<String> get(Products.Fields.ID).in(productsId));
        }
        if (locationsId != null && !locationsId.isEmpty()) {
            predicates.add(root.get(StockLevel.Fields.LOCATION).<String> get(Locations.Fields.ID).in(locationsId));
        }
        if (dateFrom != null) {
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(StockLevel.Fields.LAST_UPDATE), dateFrom));
        }
        if (dateTo != null) {
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date>get(StockLevel.Fields.LAST_UPDATE), dateTo));
        }
        
        orderList.add(getCriteriaBuilder().asc(root.get(StockLevel.Fields.PRODUCT).get(Products.Fields.ID)));
        orderList.add(getCriteriaBuilder().asc(root.get(StockLevel.Fields.LOCATION).get(Locations.Fields.ID)));
   
        List<StockLevel> stocks = getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(orderList));
        return stocks;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Map<String, Object>> findStockLevelDTOByCritria(List<String> productsRestrictionIds,
                    List<String> locationsRestrictionIds, List<String> tariffsRestrictionIds) {

                    String ids="";
                    String nativeSQL = "select p.id as productId ,null as locationId,p.name as productName ,null as locationName,stocksecurity,stockMaximum from products p ";
            String join = " left join stocklevel sl on sl.PRODUCT=p.ID ";
            String where = " where sl.location is null ";
           
            if(!productsRestrictionIds.isEmpty()) {
                    ids = String.join("','", productsRestrictionIds);
                    where = where.concat(" and  p.id in ('"+ ids +"')");
            }
            if(!tariffsRestrictionIds.isEmpty()) {
                    ids = String.join("','", tariffsRestrictionIds);
                    join = join.concat(" left join tariffareas_prod on  p.id = tariffareas_prod.productid ");
                    where = where.concat(" and tariffareas_prod.tariffid in ('"+ ids +"')");
            }
            nativeSQL += join + where + " union ";
    
            nativeSQL += "select p.id as productId ,l.id locationId ,p.name as productName , l.name as locationName,stocksecurity,stockMaximum from products p ";
            join  = " join locations l on l.PARENT_LOCATION_ID is NULL and l.ETAT is NULL "
                                      + " left join stocklevel sl on sl.PRODUCT=p.ID and l.ID=sl.LOCATION ";
            where = " where 1=1 ";
            if(!locationsRestrictionIds.isEmpty()) {
                    ids = String.join("','", locationsRestrictionIds);
                    where = where.concat(" and l.id in ('"+ ids +"')");
            }
            if(!productsRestrictionIds.isEmpty()) {
                    ids = String.join("','", productsRestrictionIds);
                    where = where.concat(" and  p.id in ('"+ ids +"')");
            }
            if(!tariffsRestrictionIds.isEmpty()) {
                    ids = String.join("','", tariffsRestrictionIds);
                    join = join.concat(" left join tariffareas_prod on  p.id = tariffareas_prod.productid ");
                    where = where.concat(" and tariffareas_prod.tariffid in ('"+ ids +"')");
            }
            nativeSQL += join + where;
           
            nativeSQL = nativeSQL.concat(" order by productId,locationId ");
            
            Query query = entityManager.createNativeQuery(nativeSQL);
            
             query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
             return query.getResultList();
    }
    
}
