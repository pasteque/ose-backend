package com.ose.backend.dao.stocks;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.generic.IGenericDAO;

import com.ose.backend.model.stocks.StockLevel;

public interface IStockLevelCustomDAO extends IGenericDAO<StockLevel , String>{

    List<Map<String, Object>> findStockLevelDTOByCritria(
            List<String> productsRestrictionIds,
            List<String> locationsRestrictionIds,
            List<String> tariffsRestrictionIds);

    List<StockLevel> findListByCriteria(List<String> productsId,
            List<String> locationsId, Date dateFrom, Date dateTo);

}
