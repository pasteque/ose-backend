package com.ose.backend.dao.stocks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.stocks.StockLevel;

public interface IStockLevelDAO extends JpaRepository<StockLevel, String> , JpaSpecificationExecutor<StockLevel> , IStockLevelCustomDAO{

}
