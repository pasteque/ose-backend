package com.ose.backend.dao.stocks;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.products.Products;

import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockCurrentId;

public interface IStockCurrentCustomDAO extends IGenericDAO<StockCurrent, StockCurrentId>{
    
    <T extends ILoadable> DtoSQLParameters findDTOListByCriteria(String productId, String locationId , String  productsCatList, String locationsCatList, Class<T> selector);

    List<Products> findProductsToOrder(String locationId);
}
