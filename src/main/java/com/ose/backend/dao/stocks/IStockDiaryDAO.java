package com.ose.backend.dao.stocks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.stocks.StockDiary;

public interface IStockDiaryDAO extends JpaRepository<StockDiary, String> , JpaSpecificationExecutor<StockDiary> , IStockDiaryCustomDAO{

}
