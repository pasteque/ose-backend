package com.ose.backend.dao.stocks;

import java.util.List;

import org.pasteque.api.model.products.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockCurrentId;

public interface IStockCurrentDAO extends JpaRepository<StockCurrent, StockCurrentId> , JpaSpecificationExecutor<StockCurrent> , IStockCurrentCustomDAO {

List<StockCurrent> findByProducts(Products product);

}
