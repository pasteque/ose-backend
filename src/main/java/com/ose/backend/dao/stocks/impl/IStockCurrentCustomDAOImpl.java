package com.ose.backend.dao.stocks.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.ose.backend.dao.stocks.IStockCurrentCustomDAO;
import com.ose.backend.dto.stocks.StockCurrentInfoDTO;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockCurrentId;
import com.ose.backend.model.stocks.StockLevel;

@Repository
public class IStockCurrentCustomDAOImpl extends GenericDAOImpl<StockCurrent, StockCurrentId> implements IStockCurrentCustomDAO{

    
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ILoadable> DtoSQLParameters findDTOListByCriteria(String productId, String locationId , String  productsCatList, String locationsCatList, Class<T> selector) {
        
        T instance;
        DtoSQLParameters model;
        
        try {
            instance = selector.newInstance();
            model = instance.getSQLParameters();
        
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            model = (new StockCurrentInfoDTO()).getSQLParameters();
        }
        String nativeSQL_0 = model.select; 
        
        
        if (locationId != null) {
            model.constraintList.add(" (t21.LOCATION IN ("+locationId+"))");
        }
        
        if (productId != null) {
            model.constraintList.add(" (t21.PRODUCT IN ("+productId+"))");
        }
        
        if (locationsCatList != null) {
            model.constraintList.add(" (t14.NAME IN ("+locationsCatList+"))");
        }
        
        if (productsCatList != null) {
            model.constraintList.add(" (t15.CATEGORY IN ("+productsCatList+"))");
        }
        
        nativeSQL_0 = nativeSQL_0.concat(StringUtils.collectionToCommaDelimitedString(model.tableList));
        if (model.constraintList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("WHERE ").concat(StringUtils.collectionToDelimitedString(model.constraintList, " AND "));
        }
        if(model.groupBy != null) {
            nativeSQL_0 = nativeSQL_0.concat("GROUP BY ").concat(model.groupBy) ;
        }
        if (model.havingList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("HAVING ").concat(StringUtils.collectionToDelimitedString(model.havingList, " AND "));
        }
        
        Query query = entityManager.createNativeQuery(nativeSQL_0);
        //System.out.println(nativeSQL_0);
        //injection param
        for(Map.Entry<Integer, Object> entry : model.parameterList.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        model.setResult(  query.getResultList());
        
        return model;
        
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Products> findProductsToOrder(String locationId) {
        
        String nativeSQL = "SELECT p.*  from products p "
                + "left join stockcurrent sc on p.id = sc.product and  sc.location = '"+locationId +"' "
                + "join stocklevel sl on p.id = sl.product and sl.location = '"+locationId+"'  "
                + "where coalesce(sc.units , 0) <= sl.stocksecurity" ;

        Query query = null;
        
        query = entityManager.createNativeQuery(nativeSQL , Products.class);
        
        return query.getResultList();
    }



}
