package com.ose.backend.dao.stocks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.stocks.InventoryDetails;

public interface IInventoryDetailsDAO extends JpaRepository<InventoryDetails, String> , JpaSpecificationExecutor<InventoryDetails> , IInventoryDetailsCustomDAO{

}
