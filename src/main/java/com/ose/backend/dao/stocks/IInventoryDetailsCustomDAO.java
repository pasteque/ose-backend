package com.ose.backend.dao.stocks;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;

import com.ose.backend.model.stocks.InventoryDetails;

public interface IInventoryDetailsCustomDAO extends IGenericDAO< InventoryDetails , String>{

    List<InventoryDetails> findByInventory(String inventoryId, int limit);

    List<Object[]> findByInventoryGrouped(String inventoryId, String reference);

    List<Object[]> findMissingCountsByInventoryGrouped(String inventoryId);

}
