package com.ose.backend.dao.stocks;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.stocks.Inventory;

public interface IInventoryDAO extends JpaRepository<Inventory, String> , JpaSpecificationExecutor<Inventory> , IInventoryCustomDAO {

}
