package com.ose.backend.dao.stocks.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.persistence.Cacheable;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.ose.backend.dao.stocks.IStockDiaryCustomDAO;
import com.ose.backend.dto.stocks.StockDiaryStatsDTO;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.service.stats.impl.StatsService.GroupType;

@Repository
@Cacheable(value = false)
public class IStockDiaryCustomDAOImpl extends GenericDAOImpl<StockDiary , String> implements IStockDiaryCustomDAO {
    
    @Override
    public Double findNbChange(String productsId, String locationId, Date dateStart , Date dateEnd , Integer reasonConstraint) {
        
        /**
         * 
         * 
         * SELECT * FROM stockdiary WHERE 
stockdiary.REASON = -1
AND stockdiary.LOCATION = '1001' AND stockdiary.product = '1234'
AND stockdiary.DATENEW > DATE_ADD(current_date() , INTERVAL -20 DAY)
AND stockdiary.DATENEW < DATE_ADD(current_date() , INTERVAL -2 DAY);
         */
        String baseQuery = "SELECT SUM(UNITS) FROM stockdiary WHERE ";
        String params = String.format(" stockdiary.LOCATION = '%s' AND stockdiary.product = '%s'", locationId , productsId);
        SimpleDateFormat hdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        if(reasonConstraint != null ) {
            params += String.format(" AND stockdiary.REASON = %d ", reasonConstraint);
        }
        if(dateEnd != null) {
            params += " AND stockdiary.DATENEW < '" + hdf.format(dateEnd) + "' ";
        }
        if(dateStart != null) {
            params += " AND stockdiary.DATENEW > '" + hdf.format(dateStart) + "' ";
        }
        
        Query query = entityManager.createNativeQuery(baseQuery+params);
        try {
            Object retour = query.getSingleResult();
            return ((retour == null) ? 0D : (Double) retour );
        } catch (Exception exception) {
            return 0D;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ILoadable> DtoSQLParameters  findByCriteriaAggr(String locationId, String productsId, 
            String  productsCatList, String locationsCatList, 
            String reference, String type , String dateFrom , String group , Class<T> selector) {
        T instance;
        DtoSQLParameters model;
        String order = "t11.ID ASC, t15.ID ASC, t22.DATENEW DESC , YEAR(t22.DATENEW) DESC, MONTH(t22.DATENEW) DESC, WEEK(t22.DATENEW, 3) DESC, DAY(t22.DATENEW) DESC , HOUR(t22.DATENEW) DESC ";
        GroupType groupType = GroupType.valueOf(group);
        
        
        try {
            instance = selector.newInstance();
            model = instance.getSQLParameters();
        
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            model = (new StockDiaryStatsDTO()).getSQLParameters();
        }
        String nativeSQL_0 = model.select; 
        String groupBy = model.groupBy;
        
        int paramIndex = 0;
        
        if (locationId != null) {
            model.constraintList.add(" (t22.LOCATION IN ("+locationId+"))");
            //model.parameterList.put(++paramIndex , locationId);
        }
        
        if (productsId != null) {
            model.constraintList.add(" (t22.PRODUCT IN ("+productsId+"))");
            //model.parameterList.put(++paramIndex, productId);
        }
        
        if (locationsCatList != null) {
            model.constraintList.add(" (t14.NAME IN ("+locationsCatList+"))");
        }
        
        if (productsCatList != null) {
            if(model.tableHash.put("t914", "CATEGORIES") == null) {
                model.tableList.add(" CATEGORIES t914 ");
                model.constraintList.add(" t914.ID = t15.CATEGORY ");
            }
            model.constraintList.add(" (t914.NAME IN ("+productsCatList+"))");
        }
        
        if (reference != null && !reference.trim().isEmpty()) {
            model.constraintList.add(" (t22.REFERENCE = ? )");
            model.parameterList.put(++paramIndex, reference);
        }
        
        if (type != null && !type.trim().isEmpty()) {
            model.constraintList.add(" (t22.TYPE = ? )");
            model.parameterList.put(++paramIndex, type);
        }
        
        if (dateFrom != null && !dateFrom.trim().isEmpty()) {
            Date dStop = new Date(Long.parseLong(dateFrom));
            model.constraintList.add(" (t22.DATENEW >= ? )");
            model.parameterList.put(++paramIndex, dStop);
        }
        
        nativeSQL_0 = nativeSQL_0.concat(StringUtils.collectionToCommaDelimitedString(model.tableList));
        if (model.constraintList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("WHERE ").concat(StringUtils.collectionToDelimitedString(model.constraintList, " AND "));
        }

        if(groupType != null ) {
            for( GroupType gt : GroupType.values()) {
                if( gt.getValue() <= groupType.getValue()) {
                    if(gt == GroupType.WEEK) {
                        //Cas particulier car la fonction WEEK fait des choses étranges semaines de 0 à 53, avec un delta par rapport à l'ISO 8601
                        //Qui est utilisée par ailleurs.
                        groupBy = groupBy == null ? gt.name().concat("(t22.DATENEW, 3)"): groupBy.concat(" , ").concat(gt.name()).concat("(t22.DATENEW, 3)");
                    } else {
                        if(groupType != GroupType.WEEK || ( gt != GroupType.MONTH && gt != GroupType.YEAR)) {
                            //Pas de group by month ni de group by year si on est sur un regroupement à la semaine
                            groupBy = groupBy == null ? gt.name().concat("(t22.DATENEW)") : groupBy.concat(" , ").concat(gt.name()).concat("(t22.DATENEW)");
                        }
                    }
                }
            }
        }
        
        if(groupBy != null) {
            nativeSQL_0 = nativeSQL_0.concat("GROUP BY ").concat(groupBy) ;
        }
        
        if (model.havingList.size() > 0) {
            nativeSQL_0 = nativeSQL_0.concat("HAVING ").concat(StringUtils.collectionToDelimitedString(model.havingList, " AND "));
        }
        
        nativeSQL_0 = nativeSQL_0.concat("ORDER BY ").concat(order);
        
        Query query = entityManager.createNativeQuery(nativeSQL_0);
        //System.out.println(nativeSQL_0);
        //injection param
        for(Map.Entry<Integer, Object> entry : model.parameterList.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        model.setResult(  query.getResultList());
        
        return model;
        
    }
    
    @Override
    public List<StockDiary> findByCriteria(String locationId, String productsId, String reference, String type , String dateFrom , String dateTo , Long from, Long nbResults, String sort) {
        CriteriaQuery<StockDiary> query = getQuery();
        Root<StockDiary> root = query.from(StockDiary.class);
        List<Predicate> predicates = new ArrayList<>();
        List<Order> orderList = new ArrayList<>();

        if (locationId != null && !locationId.isEmpty()) {
            predicates.add(getCriteriaBuilder().equal(root.get(StockDiary.Fields.LOCATION).get(Locations.Fields.ID), locationId));
        }
        if (productsId != null && !productsId.isEmpty()) {
            predicates.add(getCriteriaBuilder().equal(root.get(StockDiary.Fields.PRODUCT).get(Products.Fields.ID), productsId));
        }
        if (reference != null && !reference.isEmpty()) {
            predicates.add(getCriteriaBuilder().like(root.get(StockDiary.Fields.REFERENCE), "%"+reference+"%"));
        }
        if (type != null && !type.isEmpty()) {
            predicates.add(getCriteriaBuilder().or(
                    getCriteriaBuilder().equal(root.get(StockDiary.Fields.REASON), Integer.parseInt(type)) ,
                    getCriteriaBuilder().equal(root.get(StockDiary.Fields.REASON), -1*Integer.parseInt(type))));
        }
        if (dateFrom != null && !dateFrom.isEmpty()) {
            Date dStart = new Date(Long.parseLong(dateFrom));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(StockDiary.Fields.DATE_NEW), dStart));
        }
        if (dateTo != null && !dateTo.isEmpty()) {
            Date dEnd = new Date(Long.parseLong(dateTo));
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(StockDiary.Fields.DATE_NEW), dEnd));
        }
        
        orderList.add(getCriteriaBuilder().asc(root.get(StockDiary.Fields.LOCATION).get(Locations.Fields.ID)));
        orderList.add(getCriteriaBuilder().asc(root.get(StockDiary.Fields.PRODUCT).get(Products.Fields.ID)));
        
        if("desc".equalsIgnoreCase(sort)) {
            orderList.add(getCriteriaBuilder().desc(root.<Date> get(StockDiary.Fields.DATE_NEW)));
        } else {
            orderList.add(getCriteriaBuilder().asc(root.<Date> get(StockDiary.Fields.DATE_NEW)));
        }
    
        if(from == null) {
            from = 0L;
        }

        TypedQuery<StockDiary> tQuery = entityManager.createQuery(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(orderList))
        .setFirstResult(from.intValue());
        
        if(nbResults != null) {
            tQuery = tQuery.setMaxResults(nbResults.intValue());
        }

        return tQuery.getResultList();
    }
    
    @Override
    public Double findNbSell(String productsId, String locationId, int nbDays) {
        
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -nbDays);
        
        return findNbChange(productsId, locationId, calendar.getTime(), null, StockDiary.Type.REASON_OUT_SELL) * -1;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findNbSellByLocation(String productsId, int nbDays, int totDays) {
        
        /**
         * 
         * 
         * SELECT * FROM stockdiary WHERE 
stockdiary.REASON = -1
AND stockdiary.product = '1234'
AND stockdiary.DATENEW > DATE_ADD(current_date() , INTERVAL -20 DAY)
AND stockdiary.DATENEW < DATE_ADD(current_date() , INTERVAL -2 DAY) GROUP BY LOCATION;
         */
   
// MODIF Perf EDU
// 1 jours tous les jours regroupé ou pas le temps est constant
// En effet ça dépend du nombre de mouvements pour ce produit
// Du coup autant le faire une seule fois et redécouper / répartir après en cas de besoin
// Exemple mouvements tous emplacements à 7 et 30 jours -> un passage au lieu de deux gain 50%
// 
//        SELECT LOCATION , -1*SUM(UNITS) , 
//        IF( DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL 8 DAY) , 7 , 30 ) AS TYPE_GROUP
//        FROM stockdiary WHERE  stockdiary.product = '1234' 
//        AND DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL 31 DAY) 
//        GROUP BY LOCATION , TYPE_GROUP
//        ORDER BY LOCATION ASC , TYPE_GROUP ASC
        
        
        String baseQuery = String.format("SELECT LOCATION , -1*SUM(UNITS), IF( DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL %d DAY) , %d , %d ) AS TYPE_GROUP FROM stockdiary WHERE ", 1+nbDays , nbDays , totDays );;
        String params = String.format(" stockdiary.REASON = -1 AND stockdiary.product = '%s' AND DATE(DATENEW) > DATE_SUB( curdate() , INTERVAL %d DAY)  ", productsId , 1+ totDays);
        String group = " GROUP BY LOCATION , TYPE_GROUP ORDER BY LOCATION ASC , TYPE_GROUP ASC";

        
        Query query = entityManager.createNativeQuery(baseQuery+params+group);
        try {
            return query.getResultList();
        } catch (Exception exception) {
            return null;
        }
    }
    
}
