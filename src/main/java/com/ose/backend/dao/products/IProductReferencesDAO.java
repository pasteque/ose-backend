package com.ose.backend.dao.products;

import java.util.List;

import org.pasteque.api.model.products.ProductReferences;
import org.pasteque.api.model.products.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IProductReferencesDAO extends JpaRepository<ProductReferences, Long> , JpaSpecificationExecutor<ProductReferences> {

    List<ProductReferences> findByProductAndReference(Products product , String reference);
}
