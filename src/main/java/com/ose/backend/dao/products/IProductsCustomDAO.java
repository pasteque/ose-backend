package com.ose.backend.dao.products;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.products.Products;

import com.ose.backend.dto.products.ProductsResultDTO;

public interface IProductsCustomDAO extends IGenericDAO<Products,String>{

    List<ProductsResultDTO> findByCriteriaDTO(String reference, String category,
            Integer limit, Boolean composition);

    List<Products> findByNameSplitted(String name, Integer limit,
            Boolean composition);

    List<Products> findLinked(String reference);

    List<Products> findProductsToCloseInventory(String locationId,
            String inventoryId);
    
    List<Products> findProductsToCloseRAZInventory(String locationId,
            String inventoryId, Long from);

}
