package com.ose.backend.dao.products;

import org.pasteque.api.model.products.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IProductsDAO extends JpaRepository<Products, String> , JpaSpecificationExecutor<Products> , IProductsCustomDAO {

}
