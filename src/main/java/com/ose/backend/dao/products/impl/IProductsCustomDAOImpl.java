package com.ose.backend.dao.products.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.saleslocations.Locations;

import com.ose.backend.dao.products.IProductsCustomDAO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.model.stocks.Inventory;
import com.ose.backend.model.stocks.InventoryDetails;
import com.ose.backend.model.stocks.StockCurrent;


public class IProductsCustomDAOImpl extends GenericDAOImpl<Products,String> implements IProductsCustomDAO{

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductsResultDTO> findByCriteriaDTO(String reference, String category, Integer limit, Boolean composition) {
        
        //EDU : Question activité du produit. Compter le prix par défaut dans les prix dispos si il y a des PdV où le produit n'est dans aucun tariffaire.
        /* La requête pour un produit donné (en subquery c'est douloureux):
            SELECT tariffareas_prod.PRODUCTID , TARIFFAREAS_ID , COUNT(DISTINCT LOCATION_ID) , (SELECT COUNT(DISTINCT LOCATION_ID) FROM CASHREGISTERS) , ( SELECT COUNT(ID) FROM locations WHERE SERVER_DEFAULT = TRUE )
            FROM products,tariffareas_prod,tariffareas_location 
            WHERE 
            tariffareas_prod.PRODUCTID = PRODUCTS.ID
            AND tariffareas_prod.TARIFFID = tariffareas_location.TARIFFAREAS_ID
            AND tariffareas_location.DATE_FROM < CURRENT_DATE() AND tariffareas_location.DATE_TO > CURRENT_DATE() 
            AND ( tariffareas_location.LOCATION_ID = ( SELECT ID FROM locations WHERE SERVER_DEFAULT = TRUE ) OR ( SELECT COUNT(ID) FROM locations WHERE SERVER_DEFAULT = TRUE ) = 0 ) 
            AND PRODUCTS.ID=  ? GROUP BY PRODUCTID
        */   
        String nb_locations_querystring = "SELECT tariffareas_prod.PRODUCTID , TARIFFAREAS_ID , COUNT(DISTINCT LOCATION_ID)" 
            + " FROM products,tariffareas_prod,tariffareas_location " 
            + "WHERE tariffareas_prod.PRODUCTID = PRODUCTS.ID AND tariffareas_prod.TARIFFID = tariffareas_location.TARIFFAREAS_ID "
            + "AND tariffareas_location.DATE_FROM < CURRENT_DATE() AND tariffareas_location.DATE_TO > CURRENT_DATE() " 
            + "AND ( tariffareas_location.LOCATION_ID = ( SELECT ID FROM locations WHERE SERVER_DEFAULT = TRUE ) OR ( SELECT COUNT(ID) FROM locations WHERE SERVER_DEFAULT = TRUE ) = 0 ) " 
            + "AND PRODUCTS.REFERENCE= '" ;
        String nb_locations_querystring_end = "' GROUP BY PRODUCTID " ;
        Query nb_locations_query;
        
        List<ProductsResultDTO> retour = new ArrayList<ProductsResultDTO>();
        
        String native_base = "SELECT PRODUCTS.REFERENCE , CODE , PRODUCTS.NAME , PRODUCTS.CATEGORY , PRICEBUY , PRODUCTS.PRICESELL , STOCKCOST , MAX(tariffareas_prod.PRICESELL) , NOT(ISNULL(PRODUCTS_COM.PRODUCT)) as compo, null , TAXES.RATE "
                + " , (SELECT COUNT(DISTINCT LOCATION_ID) FROM CASHREGISTERS) , ( SELECT COUNT(ID) FROM locations WHERE SERVER_DEFAULT = TRUE ) , MESSAGES.CONTENU "
                + "FROM PRODUCTS "
                + "LEFT JOIN TAXES ON TAXES.CATEGORY = PRODUCTS.TAXCAT AND TAXES.VALIDFROM < CURRENT_DATE() AND ( TAXES.VALIDTO > CURRENT_DATE() OR ISNULL(TAXES.VALIDTO)) "
                + "LEFT JOIN PRODUCTS_COM ON PRODUCTS_COM.PRODUCT = PRODUCTS.ID "
                + "LEFT JOIN tariffareas_prod ON tariffareas_prod.PRODUCTID = PRODUCTS.ID AND " 
                + "tariffareas_prod.TARIFFID IN (SELECT TARIFFAREAS_ID FROM tariffareas_location WHERE tariffareas_location.DATE_FROM < CURRENT_DATE() " 
                + "AND tariffareas_location.DATE_TO > CURRENT_DATE() " 
                + "AND ( tariffareas_location.LOCATION_ID = ( SELECT ID FROM locations WHERE SERVER_DEFAULT = TRUE ) OR ( SELECT COUNT(ID) FROM locations WHERE SERVER_DEFAULT = TRUE ) = 0 ) GROUP BY TARIFFAREAS_ID ) " ;
        //MHA j'ai desactivé cette ligne parceque la jointure est n'est pas correcte, et j'ai remplacé msg.contenu  par null dans la select
               // + "LEFT JOIN MESSAGES msg ON (msg.product = ( SELECT ID FROM locations WHERE SERVER_DEFAULT = TRUE ) AND msg.type = 'EMPL') ";
       String group = " GROUP BY PRODUCTS.ID ";
       String ean_search = "LEFT JOIN products_ref ON products_ref.PRODUCT_ID = PRODUCTS.ID ";
       String prep_alert = "LEFT JOIN MESSAGES ON MESSAGES.PRODUCT = PRODUCTS.ID AND MESSAGES.TYPE = 'PREPARATION' AND MESSAGES.FIN IS NULL "; 
       String test_compo = "ISNULL(PRODUCTS_COM.PRODUCT)" ;
       
       String native_request =  native_base + prep_alert;
       
       String condition = "";
       String condition_ref = "";
       String retour_limit = "";
       
       List<Object[]> tmpRetour;
       List<Object[]> retourNbLoc;
       
       DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
       otherSymbols.setDecimalSeparator('.');
       DecimalFormat decf = new DecimalFormat("#.00", otherSymbols);
       
       Double pSell,pCat , pPMPA;
       
       //On gère les paramètres (category et composition)
       if(category != null){
           if(condition.length() == 0 ) {
               condition = "WHERE " ;
           } else {
               condition += "AND ";
           }
           condition += "PRODUCTS.CATEGORY = '" + category + "' ";
       }
       
       if (composition != null) {
           if(condition.length() == 0 ) {
               condition = "WHERE " ;
           } else {
               condition += "AND ";
           }
           if(composition) {
               test_compo = "NOT("+ test_compo +")" ;
           }
           condition += test_compo;
       }
       
       
       
       //On gère le cas de reference ( jointure plus recherche sur les ean)
       if( reference != null && !"%".equals(reference) && !"".equals(reference)) {
           if(condition.length() == 0 ) {
               condition = "WHERE " ;
           } else {
               condition += "AND ";
           }
           native_request += ean_search ;
           //condition_ref = "(PRODUCTS.REFERENCE = '" + reference+ "' OR PRODUCTS.CODE = '" + reference+ "' OR PRODUCTS.NAME = '" + reference+ "' OR PRODUCTS_REF.REFERENCE = '" + reference+ "') ";
           condition_ref = "(PRODUCTS.REFERENCE = '" + reference+ "' OR PRODUCTS.CODE = '" + reference+ "' OR PRODUCTS.NAME = '" + reference+ "') ";
       }
       
       //On gère la limite
       if ( limit != null) {
           retour_limit = String.format(" LIMIT %d", limit); 
       }
       
       Query query = entityManager.createNativeQuery(native_request+condition+condition_ref+group+retour_limit);

       tmpRetour = query.getResultList();
       
       for (Object[] objects : tmpRetour) {

           ProductsResultDTO pDto = new ProductsResultDTO();
           pDto.setId(String.valueOf(objects[0]));
           pDto.setBarcode(String.valueOf(objects[1]));
           pDto.setLabel(String.valueOf(objects[2]));
           pDto.setCategoryLabel(String.valueOf(objects[3]));
           pDto.setComposed("1".equals(String.valueOf(objects[8])));
           if (objects[9] != null) {
               pDto.setStockLocation(String.valueOf(objects[9]));
           } else {
               pDto.setStockLocation(Products.EMPLACEMENT_INCONNU);
           }
           
           //active ?
           pDto.setActive(objects[7] != null);
           pSell = Double.valueOf(String.valueOf(objects[5]))*(1+Double.valueOf(String.valueOf(objects[10])));
           pDto.setPriceSell(decf.format(pSell));
           
           if(objects[6] != null){
               pPMPA = Double.valueOf(String.valueOf(objects[6]));
               pDto.setCurrentPMPA(decf.format(pPMPA));
           }
           
           if(objects[7] != null){
               pDto.setMaxPriceSell(decf.format(pSell));
               pCat = Double.valueOf(String.valueOf(objects[7]))*(1+Double.valueOf(String.valueOf(objects[10])));
               pDto.setPriceSell(decf.format(pCat));
               nb_locations_query = entityManager.createNativeQuery( nb_locations_querystring + pDto.getId() + nb_locations_querystring_end);
               retourNbLoc = nb_locations_query.getResultList();
               if(retourNbLoc.size() > 0) {
                   if(pSell > pCat && ((Long)(objects[12]) == 0 ? (Long)(retourNbLoc.get(0)[2]) !=  (Long)(objects[11]) : (Long)(retourNbLoc.get(0)[2]) != 1 )) {
                       //Cas 1 tous les Points de ventes ne sont pas au catalogue et nous ne sommes pas sur un PdV précis
                       //Cas 2  nous sommes sur un PdV précis et le produit n'est pas dans une catalogue actif sur ce PdV 
                       pDto.setPriceSell(pDto.getMaxPriceSell());
                   }
               }
           }
           if(objects[13] != null) {
               pDto.setAlertePreparation(String.valueOf(objects[13]));
           }
           retour.add(pDto);
       }
       
       if(reference != null && !"%".equals(reference) && !"".equals(reference) && ( limit == null || retour.size() < limit)) {
           
           Integer newlimit = limit == null ? null : limit - retour.size();
           
           condition_ref = " PRODUCTS_REF.REFERENCE = '" + reference+ "' ";
           //On gère la limite
           if ( newlimit != null) {
               retour_limit = String.format(" LIMIT %d", newlimit); 
           }

           query = entityManager.createNativeQuery(native_request+condition+condition_ref+group+retour_limit);


           tmpRetour = query.getResultList();

           for (Object[] objects : tmpRetour) {

               ProductsResultDTO pDto = new ProductsResultDTO();
               pDto.setId(String.valueOf(objects[0]));
               pDto.setBarcode(String.valueOf(objects[1]));
               pDto.setLabel(String.valueOf(objects[2]));
               pDto.setCategoryLabel(String.valueOf(objects[3]));
               pDto.setComposed("1".equals(String.valueOf(objects[8])));
               if (objects[9] != null) {
                   pDto.setStockLocation(String.valueOf(objects[9]));
               } else {
                   pDto.setStockLocation(Products.EMPLACEMENT_INCONNU);
               }

               //active ?
               pDto.setActive(objects[7] != null);
               pSell = Double.valueOf(String.valueOf(objects[5]))*(1+Double.valueOf(String.valueOf(objects[10])));
               pDto.setPriceSell(decf.format(pSell));

               if(objects[7] != null){
                   pDto.setMaxPriceSell(decf.format(pSell));
                   pCat = Double.valueOf(String.valueOf(objects[7]))*(1+Double.valueOf(String.valueOf(objects[10])));
                   pDto.setPriceSell(decf.format(pCat));
                   nb_locations_query = entityManager.createNativeQuery( nb_locations_querystring + pDto.getId() + nb_locations_querystring_end);
                   retourNbLoc = nb_locations_query.getResultList();
                   if(retourNbLoc.size() > 0) {
                       if(pSell > pCat && ((Long)(objects[12]) == 0 ? (Long)(retourNbLoc.get(0)[2]) !=  (Long)(objects[11]) : (Long)(retourNbLoc.get(0)[2]) != 1 )) {
                           //Cas 1 tous les Points de ventes ne sont pas au catalogue et nous ne sommes pas sur un PdV précis
                           //Cas 2  nous sommes sur un PdV précis et le produit n'est pas dans une catalogue actif sur ce PdV 
                           pDto.setPriceSell(pDto.getMaxPriceSell());
                       }
                   }
               }
               if(objects[13] != null) {
                   pDto.setAlertePreparation(String.valueOf(objects[13]));
               }
               retour.add(pDto);
           }
           
           if ( limit == null || retour.size() < limit) {

               newlimit = limit == null ? null : limit - retour.size();

               condition_ref = "(PRODUCTS.REFERENCE LIKE '" + reference+ "_%' OR PRODUCTS.CODE LIKE '" + reference+ "_%' OR PRODUCTS.NAME LIKE '" + reference+ "_%') ";
               //On gère la limite
               if ( newlimit != null) {
                   retour_limit = String.format(" LIMIT %d", newlimit); 
               }

               query = entityManager.createNativeQuery(native_request+condition+condition_ref+group+retour_limit);


               tmpRetour = query.getResultList();

               for (Object[] objects : tmpRetour) {

                   ProductsResultDTO pDto = new ProductsResultDTO();
                   pDto.setId(String.valueOf(objects[0]));
                   pDto.setBarcode(String.valueOf(objects[1]));
                   pDto.setLabel(String.valueOf(objects[2]));
                   pDto.setCategoryLabel(String.valueOf(objects[3]));
                   pDto.setComposed("1".equals(String.valueOf(objects[8])));
                   if (objects[9] != null) {
                       pDto.setStockLocation(String.valueOf(objects[9]));
                   } else {
                       pDto.setStockLocation(Products.EMPLACEMENT_INCONNU);
                   }

                   //active ?
                   pDto.setActive(objects[7] != null);
                   pSell = Double.valueOf(String.valueOf(objects[5]))*(1+Double.valueOf(String.valueOf(objects[10])));
                   pDto.setPriceSell(decf.format(pSell));

                   if(objects[7] != null){
                       pDto.setMaxPriceSell(decf.format(pSell));
                       pCat = Double.valueOf(String.valueOf(objects[7]))*(1+Double.valueOf(String.valueOf(objects[10])));
                       pDto.setPriceSell(decf.format(pCat));
                       nb_locations_query = entityManager.createNativeQuery( nb_locations_querystring + pDto.getId() + nb_locations_querystring_end);
                       retourNbLoc = nb_locations_query.getResultList();
                       if(retourNbLoc.size() > 0) {
                           if(pSell > pCat && ((Long)(objects[12]) == 0 ? (Long)(retourNbLoc.get(0)[2]) !=  (Long)(objects[11]) : (Long)(retourNbLoc.get(0)[2]) != 1 )) {
                               //Cas 1 tous les Points de ventes ne sont pas au catalogue et nous ne sommes pas sur un PdV précis
                               //Cas 2  nous sommes sur un PdV précis et le produit n'est pas dans une catalogue actif sur ce PdV 
                               pDto.setPriceSell(pDto.getMaxPriceSell());
                           }
                       }
                   }
                   if(objects[13] != null) {
                       pDto.setAlertePreparation(String.valueOf(objects[13]));
                   }
                   retour.add(pDto);
               }
           }
           
           if ( limit == null || retour.size() < limit) {
               
               newlimit = limit == null ? null : limit - retour.size();
               
               condition_ref = "(PRODUCTS.REFERENCE LIKE '_%" + reference+ "%' OR PRODUCTS.CODE LIKE '_%" + reference+ "%' OR PRODUCTS.NAME LIKE '_%" + reference+ "%') ";
               //On gère la limite
               if ( newlimit != null) {
                   retour_limit = String.format(" LIMIT %d", newlimit); 
               }
               
               query = entityManager.createNativeQuery(native_request+condition+condition_ref+group+retour_limit);
    
    
               tmpRetour = query.getResultList();
               
               for (Object[] objects : tmpRetour) {
    
                   ProductsResultDTO pDto = new ProductsResultDTO();
                   pDto.setId(String.valueOf(objects[0]));
                   pDto.setBarcode(String.valueOf(objects[1]));
                   pDto.setLabel(String.valueOf(objects[2]));
                   pDto.setCategoryLabel(String.valueOf(objects[3]));
                   pDto.setComposed("1".equals(String.valueOf(objects[8])));
                   if (objects[9] != null) {
                       pDto.setStockLocation(String.valueOf(objects[9]));
                   } else {
                       pDto.setStockLocation(Products.EMPLACEMENT_INCONNU);
                   }
                   
                   //active ?
                   pDto.setActive(objects[7] != null);
                   pSell = Double.valueOf(String.valueOf(objects[5]))*(1+Double.valueOf(String.valueOf(objects[10])));
                   pDto.setPriceSell(decf.format(pSell));
                   
                   if(objects[7] != null){
                       pDto.setMaxPriceSell(decf.format(pSell));
                       pCat = Double.valueOf(String.valueOf(objects[7]))*(1+Double.valueOf(String.valueOf(objects[10])));
                       pDto.setPriceSell(decf.format(pCat));
                       nb_locations_query = entityManager.createNativeQuery( nb_locations_querystring + pDto.getId() + nb_locations_querystring_end);
                       retourNbLoc = nb_locations_query.getResultList();
                       if(retourNbLoc.size() > 0) {
                           if(pSell > pCat && ((Long)(objects[12]) == 0 ? (Long)(retourNbLoc.get(0)[2]) !=  (Long)(objects[11]) : (Long)(retourNbLoc.get(0)[2]) != 1 )) {
                               //Cas 1 tous les Points de ventes ne sont pas au catalogue et nous ne sommes pas sur un PdV précis
                               //Cas 2  nous sommes sur un PdV précis et le produit n'est pas dans une catalogue actif sur ce PdV 
                               pDto.setPriceSell(pDto.getMaxPriceSell());
                           }
                       }
                   }
                   if(objects[13] != null) {
                       pDto.setAlertePreparation(String.valueOf(objects[13]));
                   }
                   retour.add(pDto);
               }
           }
           
       }
       
        return retour;
    }

    @Override
    public List<Products> findByNameSplitted(String name, Integer limit, Boolean composition) {
                CriteriaQuery<Products> query = getQuery();
                Root<Products> root = query.from(Products.class);
                List<Predicate> andpredicates = new ArrayList<>();
                if (name != null) {
                    
                    String[] candidates = name.split("\\s+");
                    for(String reference : candidates) {
                    String referenceJok = "%" + reference + "%";
                    andpredicates.add(getCriteriaBuilder().like(root.<String> get(Products.Fields.NAME), referenceJok));
                    }
                    andpredicates.add(getCriteriaBuilder().not(getCriteriaBuilder().like(root.<String> get(Products.Fields.NAME),  "%" + name + "%")));
                }

                if (composition != null) {
                    Expression<Collection<ProductsComponents>> composants = root.get(Products.Fields.COMPONENTS_PRODUCTS);
                    if (composition) {
                        andpredicates.add(getCriteriaBuilder().isNotEmpty(composants));
                    } else {
                        andpredicates.add(getCriteriaBuilder().isEmpty(composants));
                    }
                }
                Order order = getCriteriaBuilder().asc(root.get(Products.Fields.NAME));

                return getResultList(
                        query.select(root).distinct(true).where(entityManager.getCriteriaBuilder().and(andpredicates.toArray(new Predicate[] {})))
                        .orderBy(order), limit);

    }
    
    @Override
    public List<Products> findLinked(String reference){
        List<Products> result = new ArrayList<Products>();
        Products root = findByPrimaryKey(reference);
        if(root != null) {
            List<ProductsComponents> masters = root.getMastersProducts();
            for(ProductsComponents master : masters){
                result.add(master.getMaster());
            }
            List<ProductsComponents> components = root.getComponentsProducts();
            for(ProductsComponents component : components){
                result.add(component.getComponent());
            }
        }
        return result;
        
    }
    
    @Override
    public List<Products> findProductsToCloseInventory(String locationId, String inventoryId) {
        CriteriaQuery<Products> query = getQuery();
        Root<Products> root = query.from(Products.class);

        List<Predicate> predicates = new ArrayList<>();

        Subquery<String> inventoryProduct = query.subquery(String.class);
        Root<InventoryDetails> inventoryProductRoot = inventoryProduct.from(InventoryDetails.class);
        Predicate inventoryProductPredicate = getCriteriaBuilder()
                .equal(inventoryProductRoot.get(InventoryDetails.Fields.INVENTORY)
                        .get(Inventory.Fields.ID), inventoryId);
        inventoryProduct.select(inventoryProductRoot.get(InventoryDetails.Fields.PRODUCT).<String>get(Products.Fields.ID)).where(inventoryProductPredicate);
        predicates.add(root.get(Products.Fields.ID).in(inventoryProduct));

        if (locationId != null) {
            Subquery<String> stocksProduct = query.subquery(String.class);
            Root<StockCurrent> stocksProductRoot = inventoryProduct.from(StockCurrent.class);
            Predicate stocksProductPredicate = getCriteriaBuilder().equal(
                    stocksProductRoot.get(StockCurrent.Fields.LOCATION).get(Locations.Fields.ID),
                    locationId);
            stocksProduct.select(stocksProductRoot.get(StockCurrent.Fields.PRODUCT).<String> get(Products.Fields.ID)).where(stocksProductPredicate);
            predicates.add(root.get(Products.Fields.ID).in(stocksProduct));
        }

        return getResultList(query.select(root).distinct(true).where(getCriteriaBuilder().or(predicates.toArray(new Predicate[] {}))));
    }
    
    @Override
    public List<Products> findProductsToCloseRAZInventory(String locationId, String inventoryId, Long from) {

        Date dateFrom = new Date(from == null ? 0 : from);
        CriteriaQuery<Products> query = getQuery();
        Root<Products> root = query.from(Products.class);
        List<Predicate> predicates = new ArrayList<>();
        // On exclu les produits non stockables et les produits composés
        // TODO Il devrait en être de même d'ailleurs pour tous les détails d'inventaires
        predicates.add(getCriteriaBuilder().equal(root.get(Products.Fields.STOCKABLE), true));
        predicates.add(getCriteriaBuilder().isEmpty(root.<List<Products>> get(Products.Fields.COMPONENTS_PRODUCTS)));

        Subquery<String> inventoryProduct = query.subquery(String.class);
        Root<InventoryDetails> inventoryProductRoot = inventoryProduct.from(InventoryDetails.class);
        List<Predicate> inventoryPredicates = new ArrayList<>();
        inventoryPredicates.add(getCriteriaBuilder().greaterThan(
                inventoryProductRoot.<Date> get( InventoryDetails.Fields.DATE), dateFrom));
        inventoryPredicates.add(getCriteriaBuilder().equal(
                inventoryProductRoot.get(InventoryDetails.Fields.INVENTORY).get(Inventory.Fields.LOCATION)
                        .get(Locations.Fields.ID), locationId));
        inventoryProduct.select(inventoryProductRoot.<String> get(InventoryDetails.Fields.PRODUCT)).where(
                inventoryPredicates.toArray(new Predicate[] {}));

        predicates.add(getCriteriaBuilder().not(root.get(Products.Fields.ID).in(inventoryProduct)));

        // Est-ce suffisant ? Quid d'un article passé à zéro sur un précédent inventaire complet ?
        // Dans ce cas il n'apparait pas les détails de cet inventaire
        return getResultList(query.select(root).distinct(true).where(predicates.toArray(new Predicate[] {})));
    }
}
