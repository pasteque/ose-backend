package com.ose.backend.dao.products;

import org.pasteque.api.model.products.TariffAreasValidity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ITariffAreasValidityDAO extends JpaRepository<TariffAreasValidity, Long> , JpaSpecificationExecutor<TariffAreasValidity>{

}
