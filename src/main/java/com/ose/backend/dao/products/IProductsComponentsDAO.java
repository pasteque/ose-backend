package com.ose.backend.dao.products;

import org.pasteque.api.model.products.ProductsComponents;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IProductsComponentsDAO extends JpaRepository<ProductsComponents, Long> , JpaSpecificationExecutor<ProductsComponents>{

}
