package com.ose.backend.dao.cash;

import org.pasteque.api.model.cash.ClosedCash;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IClosedCashDAO extends JpaRepository<ClosedCash, String> , JpaSpecificationExecutor<ClosedCash> , IClosedCashCustomDAO {

}
