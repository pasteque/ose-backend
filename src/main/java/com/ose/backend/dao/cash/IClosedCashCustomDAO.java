package com.ose.backend.dao.cash;


import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.cash.ClosedCash;

public interface IClosedCashCustomDAO extends IGenericDAO<ClosedCash , String>{

    List<ClosedCash> findNotClosedCash(String dateFrom);

}
