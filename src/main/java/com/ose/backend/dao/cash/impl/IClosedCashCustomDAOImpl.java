package com.ose.backend.dao.cash.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.cash.ClosedCash;

import com.ose.backend.dao.cash.IClosedCashCustomDAO;

public class IClosedCashCustomDAOImpl extends GenericDAOImpl<ClosedCash,String> implements IClosedCashCustomDAO{

    @Override
    public List<ClosedCash> findNotClosedCash(String dateFrom) {
        CriteriaQuery<ClosedCash> query = getQuery();
        Root<ClosedCash> root = query.from(ClosedCash.class);
        List<Predicate> predicates = new ArrayList<>();
        java.sql.Timestamp from = new java.sql.Timestamp(Long.parseLong("0"));
        if (dateFrom != null) {
                from = new java.sql.Timestamp(Long.parseLong(dateFrom));
            predicates.add(getCriteriaBuilder().greaterThanOrEqualTo(root.<Date> get(ClosedCash.Fields.DATE_START), from));
        }
        predicates.add(getCriteriaBuilder().isNotNull(root.get(ClosedCash.Fields.SALES_LOCATION)));
        predicates.add(getCriteriaBuilder().isNull(root.get(ClosedCash.Fields.DATE_END)));
        Order order = getCriteriaBuilder().desc(root.get(ClosedCash.Fields.HOST_SEQUENCE));
        return  getResultList(query.select(root).where(predicates.toArray(new Predicate[] {})).orderBy(order));
       
    }
}
