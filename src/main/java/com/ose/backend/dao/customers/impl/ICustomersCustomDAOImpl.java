package com.ose.backend.dao.customers.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.config.ResultType;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.pasteque.api.model.customers.Customers;
import com.ose.backend.dao.customers.ICustomersCustomDAO;
import com.ose.backend.dao.sqlresult.SQLResultNativeQuery;
import com.ose.backend.dto.customers.CustomerSimpleDTO;

public class ICustomersCustomDAOImpl extends GenericDAOImpl<Customers, String> implements ICustomersCustomDAO {

    @Override
    public String findNextId(String locationId) {
        CriteriaQuery<String> query = getCriteriaBuilder().createQuery(String.class);
        Root<Customers> root = query.from(Customers.class);
        List<Selection<?>> selections = new ArrayList<>();
        selections.add(root.get(Customers.Fields.SEARCH_KEY));

        locationId = locationId + "%";
        Predicate predicate = getCriteriaBuilder().like(root.<String> get(Customers.Fields.SEARCH_KEY), locationId);
        Order order = getCriteriaBuilder().desc(root.get(Customers.Fields.SEARCH_KEY));

        query.multiselect(selections).where(predicate).orderBy(order);
        TypedQuery<String> tq = entityManager.createQuery(query).setMaxResults(1);
        List<String> retour = tq.getResultList() ;

        String resultat = null ;
        if (!retour.isEmpty()) {
            resultat = retour.get(0);
            Long tmp = Long.parseLong(resultat.replaceAll("[^0-9]", ""));
            if(tmp != null) {
                tmp++;
                resultat = tmp.toString();
            }
        }

        return  resultat;
    }
    
    @Override
    public List<Customers> findByRegexSearchKey(String custCodeRegex, String dateStartString , String dateStopString) {
        CriteriaQuery<Customers> query = getQuery();
        Root<Customers> root = query.from(Customers.class);
        List<Predicate> predicates = new ArrayList<>();
        Date dStart = new Date(0);
        Date dStop = new Date(0);

        if(custCodeRegex != null && !custCodeRegex.isEmpty()) {
            predicates.add(getCriteriaBuilder().like(root.<String> get(Customers.Fields.SEARCH_KEY), custCodeRegex));
        }
        if(dateStartString != null && !dateStartString.isEmpty()) {
            try {
                dStart = new Date(Long.parseLong(dateStartString));
            } catch (Exception e) {

            }
            predicates.add(getCriteriaBuilder().greaterThan(root.<Date> get(Customers.Fields.LAST_UPDATE), dStart));

        }
        if(dateStopString != null && !dateStopString.isEmpty()) {
            try {
                dStop = new Date(Long.parseLong(dateStopString));
            } catch (Exception e) {

            }
            predicates.add(getCriteriaBuilder().lessThanOrEqualTo(root.<Date> get(Customers.Fields.LAST_UPDATE), dStop));

        }
        Predicate predicate = getCriteriaBuilder().and(predicates.toArray(new Predicate[] {}));
        return getResultList(query.select(root).distinct(true).where(predicate));
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public CustomerSimpleDTO findSimple(String customerId) {
        CustomerSimpleDTO retour = null;
        
        String cust = customerId == null ? "-" : customerId ;
        
        String base = "SELECT id , name FROM customers where id ='"+ cust +"'";

        Query query = entityManager.createNativeQuery(base);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        
        SQLResultNativeQuery custQuery = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
        
        if(custQuery.getListSize() > 0) {
            retour = new CustomerSimpleDTO();
            retour.setCustomersId(custQuery.getValueAsString(0, "id"));
            retour.setCustomersLabel(custQuery.getValueAsString(0, "name"));
        }
        return retour;
    }
    
    
    @SuppressWarnings("unchecked")
    @Override
    public CustomerSimpleDTO findSimpleByTicketId(String ticketId) {
        CustomerSimpleDTO retour = null;
        
        String ticket = ticketId == null ? "-" : ticketId ;
        
        String base = "SELECT customers.id as id , customers.name as name FROM tickets , tickets_extended , customers where  tickets.ticketid = '"+ ticket +"' AND tickets_extended.ticket_id = tickets.id and tickets_extended.customer_id = customers.id";

        Query query = entityManager.createNativeQuery(base);
        query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
        
        SQLResultNativeQuery custQuery = new SQLResultNativeQuery((List<Map<String, Object>>) query.getResultList());
        
        if(custQuery.getListSize() > 0) {
            retour = new CustomerSimpleDTO();
            retour.setCustomersId(custQuery.getValueAsString(0, "id"));
            retour.setCustomersLabel(custQuery.getValueAsString(0, "name"));
        }
        return retour;
    }

}
