package com.ose.backend.dao.customers;

import java.util.List;

import org.pasteque.api.dao.generic.IGenericDAO;
import org.pasteque.api.model.customers.Customers;

import com.ose.backend.dto.customers.CustomerSimpleDTO;

public interface ICustomersCustomDAO extends IGenericDAO<Customers, String>{

    String findNextId(String locationId);

    List<Customers> findByRegexSearchKey(String custCodeRegex,
            String dateStartString, String dateStopString);

    CustomerSimpleDTO findSimple(String customerId);

    CustomerSimpleDTO findSimpleByTicketId(String ticketId);

}
