package com.ose.backend.dao.customers;

import org.pasteque.api.model.customers.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ICustomersDAO extends JpaRepository<Customers, String> , JpaSpecificationExecutor<Customers> , ICustomersCustomDAO {

    
}
