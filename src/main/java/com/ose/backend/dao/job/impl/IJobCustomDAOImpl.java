package com.ose.backend.dao.job.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.pasteque.api.constants.CriteriaParameters;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ose.backend.config.JobsDefinitionConfig;
import com.ose.backend.constants.CriteriaParametersExtended;
import com.ose.backend.dao.job.IJobCustomDAO;
import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.model.job.Job;

@Repository
public class IJobCustomDAOImpl extends GenericDAOImpl<Job,String> implements IJobCustomDAO {

    @Value("#{'${job.locations.includelist:}'.split(',')}")
    private List<String> includeLocationList;
    
    @Autowired
    private JobsDefinitionConfig jobsDefinitionConfig;

    @SuppressWarnings("unchecked")
    @Override
    public List<Job> findAll() {
        return (List<Job>) entityManager.createNamedQuery("Job.findAll").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Job> findTodoList() {
        return (List<Job>) entityManager.createNamedQuery("Job.findTodo").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Job> findTodoListByType(String type) {
        Query query = null;
        String sens = Job.Sens.INTERNE ;
        List<Job> retour = null;
        // Déterminer les critères acceptés

        if(Job.Type.UPLOAD.equals(type)){
            sens = Job.Sens.UPLOAD;
        }

        String queryString = "SELECT * FROM "+ Job.Table.NAME +" WHERE sens = ?"+ CriteriaParametersExtended.SENS +" AND dcreation <= NOW() AND etat = '"+Job.Etat.PRET+"'"
                + " AND ( ?"+ CriteriaParametersExtended.TYPE +" is null OR type = ?"+ CriteriaParametersExtended.TYPE +" ) ORDER BY dcreation ASC";

        query = entityManager.createNativeQuery(queryString,Job.class);
        query.setParameter(CriteriaParametersExtended.TYPE, type);
        query.setParameter(CriteriaParametersExtended.SENS, sens);

        retour = query.getResultList();
        return retour;
    }


    @SuppressWarnings("unchecked")
    @Override
    public List<Job> findByCriteria(Map<String, Object> parameters) {
        Query query = null;
        List<Job> retour = null;
        // Déterminer les critères acceptés

        //Générer la requête associée
        String queryString = "SELECT * FROM "+ Job.Table.NAME +" WHERE ( ?"+ CriteriaParameters.DATE_DEBUT +" is null OR dcreation > ?"+ CriteriaParameters.DATE_DEBUT +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_FIN +" is null OR dcreation < ?"+ CriteriaParameters.DATE_FIN +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_ENVOI +" is null OR denvoi > ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_ENVOI +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_ENVOI +" is null OR denvoi < ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_ENVOI +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_RECEPTION +" is null OR dreception > ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_RECEPTION +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_RECEPTION +" is null OR dreception < ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_RECEPTION +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_INTEGRATION +" is null OR dintegration > ?"+ CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_INTEGRATION +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_INTEGRATION +" is null OR dintegration < ?"+ CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_INTEGRATION +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.SENS +" is null OR sens = ?"+ CriteriaParametersExtended.SENS +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.TYPE +" is null OR type = ?"+ CriteriaParametersExtended.TYPE +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.DESTINATION +" is null OR destination = ?"+ CriteriaParametersExtended.DESTINATION +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.ETAT +" is null OR etat = ?"+ CriteriaParametersExtended.ETAT +" ) ";

        query = entityManager.createNativeQuery(queryString,Job.class);
        query.setParameter( CriteriaParameters.DATE_DEBUT , parameters.get(CriteriaParameters.DATE_DEBUT));
        query.setParameter(CriteriaParameters.DATE_FIN, parameters.get(CriteriaParameters.DATE_FIN));
        query.setParameter(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_ENVOI, parameters.get(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_ENVOI));
        query.setParameter(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_ENVOI, parameters.get(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_ENVOI));
        query.setParameter(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_RECEPTION, parameters.get(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_RECEPTION));
        query.setParameter(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_RECEPTION, parameters.get(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_RECEPTION));
        query.setParameter(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_INTEGRATION, parameters.get(CriteriaParameters.DATE_DEBUT + CriteriaParametersExtended.SUFFIXE_INTEGRATION));
        query.setParameter(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_INTEGRATION, parameters.get(CriteriaParameters.DATE_FIN + CriteriaParametersExtended.SUFFIXE_INTEGRATION));
        query.setParameter(CriteriaParametersExtended.SENS, parameters.get(CriteriaParametersExtended.SENS));
        query.setParameter(CriteriaParametersExtended.TYPE, parameters.get(CriteriaParametersExtended.TYPE));
        query.setParameter(CriteriaParametersExtended.DESTINATION, parameters.get(CriteriaParametersExtended.DESTINATION));
        query.setParameter(CriteriaParametersExtended.ETAT, parameters.get(CriteriaParametersExtended.ETAT));

        retour = query.getResultList();
        return retour;
    }


    @Override
    @Transactional
    public JobDTO save(JobDTO dto) {
        Job edi = null ;
        boolean update = false ;
        boolean nouveau = false ;
            if(dto.getId() != null) {
                edi = findByPrimaryKey(dto.getId());
            }
            if(edi == null) {
                edi = new Job(); 
                nouveau = true;
            }

            update = edi.loadFromDTO(dto);

            if(nouveau) {
                update = false;
                if(edi.getId() == null) {
                    edi.createId("import");
                }
                return create(edi).getAdapter(JobDTO.class);
            }
            if(update) {
                if(edi.getId() == null) {
                    edi.createId("import");
                }
                return update(edi).getAdapter(JobDTO.class);
            }

        return dto;
    }

    @Override
    public Map<String, String> getJobsDefinition() {
        return jobsDefinitionConfig.getDefinition();
    }
    
    @Override
    public List<String> getIncludeLocationList() {
        return includeLocationList;
    }

}
