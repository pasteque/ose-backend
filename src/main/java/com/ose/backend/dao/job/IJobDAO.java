package com.ose.backend.dao.job;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.job.Job;

public interface IJobDAO extends JpaRepository<Job, String> , JpaSpecificationExecutor<Job> , IJobCustomDAO{

}