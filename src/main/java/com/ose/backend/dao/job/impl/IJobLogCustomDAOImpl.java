package com.ose.backend.dao.job.impl;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.pasteque.api.constants.CriteriaParameters;
import org.pasteque.api.dao.generic.GenericDAOImpl;
import org.springframework.stereotype.Repository;

import com.ose.backend.constants.CriteriaParametersExtended;
import com.ose.backend.dao.job.IJobLogCustomDAO;
import com.ose.backend.model.job.JobLog;

@Repository
public class IJobLogCustomDAOImpl extends GenericDAOImpl<JobLog,String> implements IJobLogCustomDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<JobLog> findAll() {
        return (List<JobLog>) entityManager.createNamedQuery("JobLog.findAll").getResultList();
    }

    
    @SuppressWarnings("unchecked")
    @Override
    public List<JobLog> findByCriteria(Map<String, Object> parameters) {
        Query query = null;
        List<JobLog> retour = null;
        // Déterminer les critères acceptés

        //Générer la requête associée
        String queryString = "SELECT * FROM "+ JobLog.Table.NAME +" WHERE ( ?"+ CriteriaParameters.DATE_DEBUT +" is null OR date > ?"+ CriteriaParameters.DATE_DEBUT +" ) "
                + " AND ( ?"+ CriteriaParameters.DATE_FIN +" is null OR date < ?"+ CriteriaParameters.DATE_FIN +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.JOB +" is null OR job_id = ?"+ CriteriaParametersExtended.JOB +" ) "
                + " AND ( ?"+ CriteriaParametersExtended.LEVEL +" is null OR level = ?"+ CriteriaParametersExtended.LEVEL +" ) ";
        
        query = entityManager.createNativeQuery(queryString,JobLog.class);
        query.setParameter(CriteriaParameters.DATE_DEBUT, parameters.get(CriteriaParameters.DATE_DEBUT));
        query.setParameter(CriteriaParameters.DATE_FIN, parameters.get(CriteriaParameters.DATE_FIN));
        query.setParameter(CriteriaParametersExtended.JOB, parameters.get(CriteriaParametersExtended.JOB));
        query.setParameter(CriteriaParametersExtended.LEVEL, parameters.get(CriteriaParametersExtended.LEVEL));
        
        retour = query.getResultList();
        return retour;
    }

}
