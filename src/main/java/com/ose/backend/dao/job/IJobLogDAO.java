package com.ose.backend.dao.job;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ose.backend.model.job.JobLog;

public interface IJobLogDAO extends JpaRepository<JobLog, String> , JpaSpecificationExecutor<JobLog> , IJobLogCustomDAO{

}