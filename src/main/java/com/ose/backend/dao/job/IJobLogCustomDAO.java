package com.ose.backend.dao.job;

import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.generic.IGenericDAO;

import com.ose.backend.model.job.JobLog;

public interface IJobLogCustomDAO  extends IGenericDAO<JobLog , String>{

    public List<JobLog> findByCriteria(Map<String, Object> parameters);

    List<JobLog> findAll();
    
}
