package com.ose.backend.dao.job;

import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.generic.IGenericDAO;

import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.model.job.Job;

public interface IJobCustomDAO extends IGenericDAO<Job , String>{

    List<Job> findAll();

    List<Job> findTodoList();

    List<Job> findTodoListByType(String type);

    Map<String, String> getJobsDefinition();

    List<String> getIncludeLocationList();

    JobDTO save(JobDTO dto);

    List<Job> findByCriteria(Map<String, Object> parameters);

}
