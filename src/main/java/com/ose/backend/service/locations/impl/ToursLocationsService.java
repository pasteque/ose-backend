package com.ose.backend.service.locations.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.locations.IToursApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.saleslocations.ToursLocations;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ose.backend.dao.locations.IToursLocationsDAO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.service.locations.IToursLocationsService;
import com.ose.backend.specs.locations.ToursLocationsSpecification;

@Service
public class ToursLocationsService implements IToursLocationsService {

    @Autowired
    private IToursApiDAO toursDAO;
    @Autowired
    private ILocationsApiDAO locationDAO;
    @Autowired
    private IToursLocationsDAO toursLocationDAO;
    @Autowired
    private IJobsService jobService;
    @Autowired
    private ILocationsServiceOSE locationsServiceOSE;

    @Override 
    public void createJobs(POSToursLocationsDTO dto , String action) {
        Locations localServer = locationDAO.findTopByServerDefaultTrue();
        LocationsInfoDTO localServerdto = localServer == null ? null : localServer.getAdapter(LocationsInfoDTO.class);
        Job job = JobHelper.createJobAffectationCamionsTournees(dto, localServerdto);
        if( job != null) {
            try {
                JobHelper.fillJobAffectationCamionsTournees(job, action, dto);
                jobService.save(job);
                if (localServerdto == null ) {
                    //Dans ce cas nous sommes sur le back central donc propager 'plus'
                    //TODO voir si c'est pour la log ou pour tout le monde
                    List<LocationsInfoDTO> dtos = AdaptableHelper.getAdapter( locationsServiceOSE.findAllParentLocations(false, job.getDestination(),false,false) , LocationsInfoDTO.class);
                    List<Job> jobList = JobHelper.createJobForEveryBack(job, dtos);
                    for(Job element : jobList) {
                        jobService.save(element);
                    }
                }
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    @Override
    public POSToursLocationsDTO save(POSToursLocationsDTO dto) {
        boolean existingEntry = true;

        List<ToursLocations> found = getModelListByCriteria(null, dto.getLocation().getId(), dto.getTour().getId(), dto.getStartDate());
        ToursLocations toursLocations = found.size() == 0 ? null : found.get(0);
        Locations location = toursLocations != null ? toursLocations.getLocation() : locationDAO.findById(dto.getLocation().getId()).orElse(null);
        Tours tour = toursLocations != null ? toursLocations.getTour() : toursDAO.findById(dto.getTour().getId()).orElse(null);

        if(toursLocations != null && dto.getEndDate() == toursLocations.getEndDate()) {
            //EDU 2020 05 si pas de modif de date de fin, alors pas de modif tout court
            location = null;
        }

        if (location != null && tour != null) {
            if (toursLocations == null) {
                existingEntry = false;
                toursLocations = new ToursLocations();
                // Charger les données
                // locations.setId(element.getId());
                toursLocations.setEndDate(dto.getEndDate());
                toursLocations.setStartDate(dto.getStartDate());
                toursLocations.setLocation(location);
                toursLocations.setTour(tour);
            } else {
                // On a 'juste' ajusté la date de fin
                toursLocations.setEndDate(dto.getEndDate());
            }

            if (existingEntry) {
                toursLocations = toursLocationDAO.save(toursLocations);
                // update des locations et tours ?
            } else {
                toursLocations = toursLocationDAO.save(toursLocations);
                location.getTours().add(toursLocations);
                tour.getStockLocations().add(toursLocations);

                toursDAO.save(tour);
                locationDAO.save(location);
            }

            //EDU : Pour les modifs via l'IHM il faut pouvoir générer le Job d'ici...
            //Si nous sommes ici alors c'est que nous avons une modification donc on pousse le job
            createJobs(dto, Job.Actions.INSERT);


        }
        return toursLocations == null ? null : toursLocations.getAdapter(POSToursLocationsDTO.class);
    }

    @Override
    public boolean delete(POSToursLocationsDTO toursDTO) {
        // EDU - 2020 05 possibilité génération job ici
        ToursLocations toursLocations = null;
        boolean retour = false;

        if (toursDTO != null) {
            //EDU 2020 05 Changement car les Id ne sont pas statiques à travers les back
            //toursLocations = toursLocationDAO.findByPrimaryKey(toursDTO.getId());
            List<ToursLocations> found = getModelListByCriteria(null, toursDTO.getLocation().getId(), toursDTO.getTour().getId(), toursDTO.getStartDate());
            toursLocations = found.size() == 0 ? null : found.get(0);
            Locations location = toursLocations != null ? toursLocations.getLocation() : locationDAO.findById(toursDTO.getLocation().getId()).orElse(null);
            Tours tour = toursLocations != null ? toursLocations.getTour() : toursDAO.findById(toursDTO.getTour().getId()).orElse(null);

            if (toursDTO.getId() != 0 && toursLocations != null) {
                toursLocationDAO.delete(toursLocations);

                location.getTours().remove(toursLocations);
                tour.getStockLocations().remove(toursLocations);
                toursDAO.save(tour);
                locationDAO.save(location);

                //EDU 2020 05 si il y a eu supression alors on crée le(s) job(s).
                createJobs(toursDTO, Job.Actions.DELETE);

                // Update des tours et Locations ?
            }
        }

        return retour;
    }

    @Override
    public List<POSToursLocationsDTO> saveAll(List<POSToursLocationsDTO> list) {
        List<POSToursLocationsDTO> results = new ArrayList<POSToursLocationsDTO>();
        if (list != null) {
            for (POSToursLocationsDTO element : list) {
                POSToursLocationsDTO result = save(element);
                if (result != null) {
                    results.add(result);
                }
            }
        }
        return results;
    }

    @Override
    public List<POSToursLocationsDTO> getByCriteria(String reference, String locationId , long tourId , Date dateStart) {
        return AdaptableHelper.getAdapter(getModelListByCriteria(reference, locationId, tourId, dateStart),POSToursLocationsDTO.class);
    }

    public List<ToursLocations> getModelListByCriteria(String reference, String locationId , long tourId , Date dateStart) {
        ToursLocationsSpecification specs = new ToursLocationsSpecification();
        specs.add(new SearchCriteria(ToursLocations.Fields.LOCATION , locationId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(ToursLocations.Fields.ID , reference , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(ToursLocations.Fields.TOUR , tourId , SearchOperation.EQUAL));
        if(dateStart != null) {
            specs.add(new SearchCriteria(ToursLocations.Fields.START_DATE , DateHelper.formatSQLTimeStamp(dateStart) , SearchOperation.EQUAL));
        }
        Sort sort = Sort.by(ToursLocations.Fields.START_DATE).ascending();

        return toursLocationDAO.findAll(specs , sort);
    }


    @Override
    public List<ToursLocations> findCurrent(String locationId, String tourId,
            Date dStart, Date dEnd) {
        ToursLocationsSpecification specs = new ToursLocationsSpecification();
        specs.add(new SearchCriteria(ToursLocations.Fields.LOCATION , locationId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(ToursLocations.Fields.TOUR , tourId , SearchOperation.EQUAL));
        if(dStart != null) {
            specs.add(new SearchCriteria(ToursLocations.Fields.END_DATE , DateHelper.formatSQLTimeStamp(dStart) , SearchOperation.GREATER_THAN_EQUAL));
        }
        if(dEnd != null) {
            specs.add(new SearchCriteria(ToursLocations.Fields.START_DATE , DateHelper.formatSQLTimeStamp(dEnd) , SearchOperation.LESS_THAN_EQUAL));
        }        
        Sort sort = Sort.by(ToursLocations.Fields.TOUR).ascending().and(Sort.by(ToursLocations.Fields.LOCATION).descending());
        return toursLocationDAO.findAll(specs , sort );
    }
}
