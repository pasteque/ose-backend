package com.ose.backend.service.locations.impl;

import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dto.locations.InseeSimpleDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ose.backend.service.locations.IInseeServiceOSE;
import com.ose.backend.specs.locations.InseeSpecification;

@Service
public class InseeServiceOSE implements IInseeServiceOSE {

    @Autowired
    private IInseeApiDAO inseeDAO;

    @Override
    public List<InseeSimpleDTO> getDTOByCriteria(String reference, Integer limit){
        
        return AdaptableHelper.getAdapter(getByCriteria(reference, limit), InseeSimpleDTO.class);
    }
    
    @Override
    public List<Insee> getByCriteria(String reference, Integer limit){
        InseeSpecification  zip = new InseeSpecification();
        zip.add(new SearchCriteria(Insee.Fields.ZIPCODE ,  reference , SearchOperation.MATCH_START));
        InseeSpecification  ville = new InseeSpecification();
        ville.add(new SearchCriteria(Insee.Fields.COMMUNE ,  reference , SearchOperation.MATCH));
        Specification<Insee> distinct = InseeSpecification.distinct();
        Sort sort = Sort.by(Insee.Fields.ZIPCODE).ascending() ;


        List<Insee> l = new ArrayList<Insee>();
        if(limit != null && limit > 1) { 

            Page<Insee> p = inseeDAO.findAll(distinct.and(zip.or(ville)) , PageRequest.of(0, limit , sort) );
            l = p.getContent();
        } else {
            l = inseeDAO.findAll(distinct.and(zip.or(ville)) , sort );
        }

        return l;
    }


}
