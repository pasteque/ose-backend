package com.ose.backend.service.locations.impl;

import java.util.List;

import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dto.locations.SalesLocationsDetailDTO;
import org.pasteque.api.dto.locations.SalesLocationsStatsDTO;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.tickets.ITicketsDAO;
import com.ose.backend.service.locations.ISalesLocationsServiceOse;

@Service
public class SalesLocationsServiceOse implements ISalesLocationsServiceOse {

    @Autowired
    private ISalesLocationsApiDAO salesLocationsDAO;
    @Autowired
    private ITicketsDAO ticketsDAO;

    @Override
    public <T> T getById(Long salesLocationId, Class<T> selector) {
        return getAdapter( salesLocationsDAO.findById(salesLocationId).get() , selector );
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(SalesLocations salesLocations , Class<T> selector) {
        if (selector.equals(SalesLocationsDetailDTO.class)) {
            SalesLocationsDetailDTO salesLocationsDTO = new SalesLocationsDetailDTO();
            salesLocationsDTO.setCity(salesLocations.getCity());
            salesLocationsDTO.setEndDate(salesLocations.getEndDate());
            salesLocationsDTO.setFee(salesLocations.getFee());
            salesLocationsDTO.setId(salesLocations.getId());
            salesLocationsDTO.setInseeNb(salesLocations.getInseeNum());
            salesLocationsDTO.setPaid(salesLocations.isPaid());
            salesLocationsDTO.setPubNb(salesLocations.getPubNb());
            salesLocationsDTO.setStartDate(salesLocations.getStartDate());
            salesLocationsDTO.setTourName(salesLocations.getTour().getName());

            if (!salesLocations.getSalesSessions().isEmpty()) {
                Object[] results = ticketsDAO.findBySalesSessions(salesLocations.getSalesSessions());
                SalesLocationsStatsDTO statsDTO = new SalesLocationsStatsDTO();
                if (results != null && results.length == 3) {
                    statsDTO.setDate(salesLocations.getStartDate());
                    statsDTO.setSellNb((long) results[0]);
                    statsDTO.setSellAmount((double) results[1]);
                    statsDTO.setSellAmountTTC(statsDTO.getSellAmount() + (double) results[2]);
                    statsDTO.setAverageBasket(statsDTO.getSellAmountTTC() / statsDTO.getSellNb());
                    statsDTO.setRatio(statsDTO.getSellAmountTTC() / salesLocations.getPubNb());
                    for(ClosedCash session : salesLocations.getSalesSessions()){
                        statsDTO.getSessions().add(session.getMoney());
                    }
                    salesLocationsDTO.setCurrentStats(statsDTO);
                }
                salesLocationsDTO.setLocationName(salesLocations.getSalesSessions().get(0).getCashRegister().getLocation().getName());
            }

            SalesLocations nextSalesLocations = salesLocationsDAO.findNextSalesLocation(salesLocations.getInseeNum(),
                    salesLocations.getStartDate());
            if (nextSalesLocations != null) {
                salesLocationsDTO.setNextStartDate(nextSalesLocations.getStartDate());
                salesLocationsDTO.setNextEndDate(nextSalesLocations.getEndDate());
            }

            List<SalesLocations> pastSalesLocations = salesLocationsDAO.findPastSalesLocation(salesLocations.getInseeNum(),
                    salesLocations.getStartDate());
            for (SalesLocations locations : pastSalesLocations) {
                if (!locations.getSalesSessions().isEmpty()) {
                    Object[] results = ticketsDAO.findBySalesSessions(locations.getSalesSessions());
                    SalesLocationsStatsDTO statsDTO = new SalesLocationsStatsDTO();
                    if (results != null && results.length == 3) {
                        statsDTO.setDate(locations.getStartDate());
                        statsDTO.setSellNb((long) results[0]);
                        statsDTO.setSellAmount((double) results[1]);
                        statsDTO.setSellAmountTTC(statsDTO.getSellAmount() + (double) results[2]);
                        statsDTO.setAverageBasket(statsDTO.getSellAmountTTC() / statsDTO.getSellNb());
                        statsDTO.setRatio(statsDTO.getSellAmountTTC() / locations.getPubNb());
                        statsDTO.setSalesLocationId(locations.getId());
                        salesLocationsDTO.getSalesHistory().add(statsDTO);
                    }
                }
            }

            return (T) salesLocationsDTO;
        }
        return null;
    }
    
   
}
