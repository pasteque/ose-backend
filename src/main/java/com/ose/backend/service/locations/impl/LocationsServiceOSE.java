package com.ose.backend.service.locations.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.specs.locations.LocationsSpecification;

@Service
public class LocationsServiceOSE implements ILocationsServiceOSE {


    @Autowired
    private ILocationsApiDAO locationsDAO;
    @Autowired
    private IInseeApiDAO inseeDAO;
    @Autowired
    private ICategoriesApiDAO categoriesDAO;
    

    @Override
    public List<Locations> findAllParentLocations(boolean excludeBase,
            String locationId, boolean avecFerme, boolean avecVirtuels) {
        
        GenericSpecification<Locations> spec = new GenericSpecification<Locations>();
        spec.add(new SearchCriteria( Locations.Fields.PARENT ,null, SearchOperation.IS_NULL));
        
        if(excludeBase) {
            spec.add(new SearchCriteria( Locations.Fields.ID ,Locations.SIEGE_ID, SearchOperation.NOT_EQUAL));
        }
        if(locationId != null) {
            spec.add(new SearchCriteria( Locations.Fields.ID ,locationId, SearchOperation.NOT_EQUAL));
        }
        if(!avecFerme) {
            spec.add(new SearchCriteria( Locations.Fields.ETAT, Locations.Etats.FERME , SearchOperation.NOT_EQUAL));
        }
        if(!avecVirtuels) {
            spec.add(new SearchCriteria( Locations.Fields.ETAT , Locations.Etats.VIRTUEL, SearchOperation.NOT_EQUAL));
        }
        
        return locationsDAO.findAll(spec);
    }
    
    @Override
    public <T> List<T> getAll(Class<T> selector) {
        return AdaptableHelper.getAdapter(locationsDAO.findAll(), selector);
    }

    
    @Override
    public Object getSubLocations(String locationId) {
        Locations locations = locationsDAO.findById(locationId).orElse(null);
        return AdaptableHelper.getAdapter(locations.getChildren(), LocationsInfoDTO.class);
    }
    
    @Override
    public Object getSistersLocations(String locationId) {
        Locations locations = locationsDAO.findById(locationId).orElse(null);
        List<LocationsInfoDTO> retour = AdaptableHelper.getAdapter(locations.getParent().getChildren(), LocationsInfoDTO.class);
        for (int i = 0 ; i < retour.size() ; i++) {
            LocationsInfoDTO loc = retour.get(i);
            if(loc.getId().equals(locationId)) {
                retour.remove(i);
                break;
            }
        }
        retour.add(locations.getParent().getAdapter(LocationsInfoDTO.class));
        return retour;
    }

    @Override
    public Object getRemoteLocations(String locationId) {
        Locations locations = locationsDAO.findById(locationId).orElse(null);
        List<LocationsInfoDTO> retour = AdaptableHelper.getAdapter(locationsDAO.findAll(), LocationsInfoDTO.class);
        retour.removeAll(AdaptableHelper.getAdapter(locations.getChildren(), LocationsInfoDTO.class));
        retour.remove(locations.getAdapter(LocationsInfoDTO.class));
        return retour;
    }

    @Override
    /**
     * Renvoie une liste d'informations sur les lieux en DTO
     * <p>
     * @param reference : une chaine, ne seront retournés que les lieux contenant cette chaine (utilisée dans un Like en mode SQL) que c soit dans la référence ou dans la description
     * @param limit : Nombre maxi de Lieux retournés
     * @param parentOnly : True on ne retourne que des lieux 'parents' , False que des enfants , null aucune distinction
     */
    public List<LocationsInfoDTO> getCriteria(String reference, String category , Integer limit , Boolean parentOnly) {
            
        return AdaptableHelper.getAdapter(findByCriteria(reference, category, limit, parentOnly) ,  LocationsInfoDTO.class);
    }
    
    /**
     * Renvoie une liste d'informations sur les lieux
     * <p>
     * @param reference : une chaine, ne seront retournés que les lieux contenant cette chaine (utilisée dans un Like en mode SQL) que c soit dans la référence ou dans la description
     * @param limit : Nombre maxi de Lieux retournés
     * @param parentOnly : True on ne retourne que des lieux 'parents' , False que des enfants , null aucune distinction
     */
    @Override
    public List<Locations> findByCriteria(String reference, String category , Integer limit , Boolean parentOnly) {
        
        LocationsSpecification spec = new LocationsSpecification();
        if(parentOnly != null ) {

            spec.add(new SearchCriteria( Locations.Fields.PARENT ,null, parentOnly ? SearchOperation.IS_NULL : SearchOperation.IS_NOT_NULL));   
        }
        
        Specification<Locations> ref = new LocationsSpecification();
        if(reference != null ) {
            GenericSpecification<Locations> id = new GenericSpecification<Locations>();
            GenericSpecification<Locations> re = new GenericSpecification<Locations>();
            id.add(new SearchCriteria(Locations.Fields.ID, reference , SearchOperation.MATCH));
            re.add(new SearchCriteria(Locations.Fields.NAME, reference , SearchOperation.MATCH));
            ref = id.or(re);
        }
        
        if(category != null ) {
            spec.add(new SearchCriteria( Locations.Fields.CATEGORY ,category, SearchOperation.EQUAL));
        } 
        
        Sort sort = Sort.by(Locations.Fields.ID).ascending();
        
        List<Locations> liste = null ;
        
        if(limit != null && limit > 0) {
            Page<Locations> p = locationsDAO.findAll(ref.and(spec) , PageRequest.of(0, limit , sort) );
            liste = p.getContent();
        } else {
            liste = locationsDAO.findAll(ref.and(spec) , sort);
        }
        
        return liste;
    }
    
    
    @Override
    public
    List<String> getEtats(){
        Locations jobject = new Locations();
        Locations.Etats object = jobject.new Etats();
        List<String> retour = new ArrayList<String>();
        Field[] fields = Locations.Etats.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }
    
    @Override
    public boolean loadFromDTO(Object source , Locations locations) {
        boolean retour = false ;
        if (POSLocationsDTO.class.isInstance(source)) {
            POSLocationsDTO dto = (POSLocationsDTO) source;
            if((dto.getInseeNum()!= null && !dto.getInseeNum().equals(locations.getInseeNum())) || (dto.getInseeNum()==null && locations.getInseeNum() != null)) {
                retour = true ;
                Insee insee = dto.getInseeNum()== null ? null : inseeDAO.findById(dto.getInseeNum()).orElse(null);
                locations.setInsee(insee);
            }
            if((dto.getLabel()!= null && !dto.getLabel().equals(locations.getName())) || (dto.getLabel()==null && locations.getName() != null)) {
                retour = true ;
                locations.setName(dto.getLabel());
            }
            if((dto.getAddress()!= null && !dto.getAddress().equals(locations.getAddress())) || (dto.getAddress()==null && locations.getAddress() != null)) {
                retour = true ;
                locations.setAddress(dto.getAddress());
            }
            if((dto.getParentId()!= null && !dto.getParentId().equals(locations.getParentId())) || (dto.getParentId()==null && locations.getParentId() != null)) {
                retour = true ;
                Locations parent = dto.getParentId()== null ? null : locationsDAO.findById(dto.getParentId()).orElse(null);
                locations.setParent(parent);
            }
            if((dto.getCategory()!= null && !dto.getCategory().getId().equals(locations.getCategoryId())) || (dto.getCategory()==null && locations.getCategoryId() != null)) {
                retour = true ;
                Categories categ = dto.getCategory() != null ? categoriesDAO.findById(dto.getCategory().getId()).orElse(null) : null;
                locations.setCategory(categ);
            }
        }
        return retour;
    }


}
