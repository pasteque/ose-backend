package com.ose.backend.service.locations;

import java.util.List;

import org.pasteque.api.dto.locations.InseeSimpleDTO;
import org.pasteque.api.model.saleslocations.Insee;

public interface IInseeServiceOSE {

  List<Insee> getByCriteria(String reference, Integer limit);

  List<InseeSimpleDTO> getDTOByCriteria(String reference, Integer limit);
 
}
