package com.ose.backend.service.locations;

import java.util.List;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.model.saleslocations.Locations;

public interface ILocationsServiceOSE {

    List<Locations> findAllParentLocations(boolean excludeBase, String locationId, boolean avecFerme, boolean avecVirtuels);

    List<String> getEtats();

    <T> List<T> getAll(Class<T> selector);

    Object getRemoteLocations(String locationId);

    List<LocationsInfoDTO> getCriteria(String reference, String category,
            Integer limit, Boolean parentOnly);

    boolean loadFromDTO(Object source, Locations locations);

    Object getSubLocations(String locationId);

    /**
     * Renvoie une liste d'informations sur les lieux
     * <p>
     * @param reference : une chaine, ne seront retournés que les lieux contenant cette chaine (utilisée dans un Like en mode SQL) que c soit dans la référence ou dans la description
     * @param limit : Nombre maxi de Lieux retournés
     * @param parentOnly : True on ne retourne que des lieux 'parents' , False que des enfants , null aucune distinction
     */
    List<Locations> findByCriteria(String reference, String category,
            Integer limit, Boolean parentOnly);

    Object getSistersLocations(String locationId);

}
