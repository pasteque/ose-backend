package com.ose.backend.service.locations;

import java.util.List;
public interface IPaysService {

    <T>  List<T> getAll(Class<T> selector);
    <T> T getById(Long paysId, Class<T> selector);
}
