package com.ose.backend.service.locations;

import java.util.List;

import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.locations.ToursDTO;

public interface IToursService {

    ToursDTO getTourById(int id);
    ToursDTO getTourByName(String name);
    Object save(List<ToursDTO> l);
    <T> List<T> getAllTours(Class<T> selector);
    <T> List<T> getCurrentTours(String locationId, Class<T> selector);
    <T> List<T> getCurrentToursLinks(String locationId, String tourId , Boolean futur , String from, String to , Class<T> selector);
    Object saveLink(POSToursLocationsDTO l);
    Object deleteLink(POSToursLocationsDTO l);
    
}
