package com.ose.backend.service.locations;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.model.saleslocations.ToursLocations;

public interface IToursLocationsService {

    POSToursLocationsDTO save(POSToursLocationsDTO dto);
    List<POSToursLocationsDTO> saveAll(List<POSToursLocationsDTO> dto);
    boolean delete(POSToursLocationsDTO dto);
    void createJobs(POSToursLocationsDTO dto, String action);
    List<POSToursLocationsDTO> getByCriteria(String reference, String locationId, long tourId, Date dateStart);
    List<ToursLocations> findCurrent(String locationId, String tourId, Date dStart, Date dEnd);

}
