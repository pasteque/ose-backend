package com.ose.backend.service.locations.impl;

import java.util.List;

import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.locations.IPaysDAO;
import com.ose.backend.model.locations.Pays;
import com.ose.backend.service.locations.IPaysService;

@Service
public class PaysService implements IPaysService {

    @Autowired
    private IPaysDAO paysDAO;

    @Override
    public <T> List<T> getAll(Class<T> selector) {
        return AdaptableHelper.getAdapter(paysDAO.findAll(), selector);
    }

    @Override
    public <T> T getById(Long paysId, Class<T> selector) {
        Pays p = paysDAO.findById(paysId).orElse(null);
        return p == null ? null : p.getAdapter(selector);
    }

}
