package com.ose.backend.service.locations.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.pasteque.api.dao.locations.IToursApiDAO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.locations.ToursDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.service.locations.IToursLocationsService;
import com.ose.backend.service.locations.IToursService;
import com.ose.backend.specs.locations.ToursSpecification;

@Service
public class ToursService implements IToursService {

    @Autowired
    private IToursApiDAO toursDAO;
    @Autowired
    private IToursLocationsService toursLocationsService;

    @Override
    public <T> List<T> getAllTours(Class<T> selector) {
        return AdaptableHelper.getAdapter(toursDAO.findAll(), selector);
    }

    @Override
    public ToursDTO getTourById(int id) {
        Tours tour = toursDAO.findById((long) id).orElse(null) ;
        return tour ==null ? null : tour.getAdapter(ToursDTO.class);
    }

    @Override
    public ToursDTO getTourByName(String name) {
        ToursSpecification specs = new ToursSpecification();
        specs.add(new SearchCriteria(Tours.Fields.TOUR_NAME,  name , SearchOperation.EQUAL));
        Optional<Tours> tour =  toursDAO.findOne(specs);
        return tour.isPresent() ? tour.get().getAdapter(ToursDTO.class) : null;
    }

    @Override
    public Object save(List<ToursDTO> l) {
        // TODO voir pour gérer convenablement les liens
        Tours tour;
        for (ToursDTO toursDTO : l) {
            tour = toursDAO.findById(toursDTO.getId()).orElse(null);
            if (toursDTO.getId() != 0 && tour != null) {
                tour.setName(toursDTO.getName());
                toursDAO.save(tour);
            } else {
                tour = new Tours();
                if (toursDTO.getId() != 0)
                    tour.setId(toursDTO.getId());
                tour.setName(toursDTO.getName());
                toursDAO.save(tour);
            }
        }
        return null;
    }

    @Override
    public <T> List<T> getCurrentTours(String locationId, Class<T> selector) {
        ToursSpecification specs = new ToursSpecification();
        String now = DateHelper.formatSQLTimeStamp(new Date()) ;
        specs.add(new SearchCriteria(Tours.SearchCritria.LOCATION ,  locationId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Tours.SearchCritria.TO ,  now , SearchOperation.LESS_THAN_EQUAL));
        specs.add(new SearchCriteria(Tours.SearchCritria.FROM ,  now , SearchOperation.GREATER_THAN_EQUAL));
        
        return AdaptableHelper.getAdapter(toursDAO.findAll(specs), selector);
    }

    @Override
    public <T> List<T> getCurrentToursLinks(String locationId, String tourId, Boolean futur, String from, String to, Class<T> selector) {
        Date dStart = from == null ? null : new Date(Long.parseLong(from));
        Date dEnd = to == null ? null : new Date(Long.parseLong(to));
        if(futur!=null) {
            dStart = new Date();
            dEnd = futur ? null : dStart;
        }
        return AdaptableHelper.getAdapter(toursLocationsService.findCurrent(locationId, tourId, dStart, dEnd), selector);
    }

    @Override
    public Object saveLink(POSToursLocationsDTO toursDTO) {
        // 09/03/2020 - AME -> Refactoring création du service dédié
        // EDU - 2020 05 possibilité génération job ici
        return toursLocationsService.save(toursDTO);
    }

    @Override
    public Object deleteLink(POSToursLocationsDTO toursDTO) {
        return toursLocationsService.delete(toursDTO);
    }

}
