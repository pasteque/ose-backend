package com.ose.backend.service.locations;

public interface ISalesLocationsServiceOse {

    <T> T getById(Long salesLocationId, Class<T> selector);


}
