package com.ose.backend.service.cash.impl;

import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.model.cash.ClosedCash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.service.cash.IClosedCashService;

@Service
public class ClosedCashService implements IClosedCashService {

    @Autowired
    private ISalesLocationsApiDAO salesLocationsDAO;
    @Autowired
    private ICashRegistersApiDAO cashRegistersDAO;
    
    @Override
    public boolean loadFromDTO(Object source , ClosedCash closedCash) {
        boolean retour = false;
        if (POSClosedCashDTO.class.isInstance(source)) {
            POSClosedCashDTO dto = (POSClosedCashDTO) source;
            
            if((dto.getId()!= null && !dto.getId().equals(closedCash.getMoney())) || (dto.getId()==null && closedCash.getMoney() != null)) {
                retour = true ;
                closedCash.setMoney(dto.getId());
            }
            if((dto.getCashRegisterId()!= null && !dto.getCashRegisterId().equals(closedCash.getCashRegisterId())) || (dto.getCashRegisterId()==null && closedCash.getCashRegisterId() != null)) {
                retour = true ;
                closedCash.setCashRegister(cashRegistersDAO.findById(dto.getCashRegisterId()).orElse(null));
            }
            if((dto.getSalesLocation()!= null && !dto.getSalesLocation().getId().equals(closedCash.getSalesLocation().getId())) || (dto.getSalesLocation()==null && closedCash.getSalesLocation() != null)) {
                retour = true ;
                closedCash.setSalesLocation(salesLocationsDAO.findById(dto.getSalesLocation().getId()).orElse(null));
            }
            if((dto.getExpectedCash()!= null && !dto.getExpectedCash().equals(String.valueOf(closedCash.getExpectedCash()))) || (dto.getExpectedCash()==null && closedCash.getExpectedCash() != null)) {
                retour = true ;
                closedCash.setExpectedCash(dto.getExpectedCash()==null ? null : Double.valueOf(dto.getExpectedCash()));
            }
            if((dto.getCloseCash()!= null && !dto.getCloseCash().equals(String.valueOf(closedCash.getCloseCash()))) || (dto.getCloseCash()==null && closedCash.getCloseCash() != null)) {
                retour = true ;
                closedCash.setCloseCash(dto.getCloseCash()==null ? null :Double.valueOf(dto.getCloseCash()));
            }
            if((dto.getOpenCash()!= null && !dto.getOpenCash().equals(String.valueOf(closedCash.getOpenCash()))) || (dto.getOpenCash()==null && closedCash.getOpenCash() != null)) {
                retour = true ;
                closedCash.setOpenCash(dto.getOpenCash()==null ? null : Double.valueOf(dto.getOpenCash()));
            }
            if((dto.getCloseDate()!= null && !dto.getCloseDate().equals(closedCash.getDateEnd())) || (dto.getCloseDate()==null && closedCash.getDateEnd() != null)) {
                retour = true ;
                closedCash.setDateEnd(dto.getCloseDate()==null ? null : dto.getCloseDate());
            }
            if((dto.getOpenDate()!= null && !dto.getOpenDate().equals(closedCash.getDateStart())) || (dto.getOpenDate()==null && closedCash.getDateStart() != null)) {
                retour = true ;
                closedCash.setDateStart(dto.getOpenDate());
            }
            if((dto.getSequence() != closedCash.getHostSequence())) {
                retour = true ;
                closedCash.setHostSequence(dto.getSequence());
            }
            /*
            if (closedCashDTO.getId() != null) {
                closedCash = closedCashDAO.findByPrimaryKey(closedCashDTO.getId());
            } else {
                int sequence = closedCashDAO.findLastSequenceByCashRegisterId(closedCashDTO.getCashRegisterId());
                closedCash.setHostSequence(sequence + 1);
                closedCash.setCashRegister(cashRegistersDAO.findByPrimaryKey(closedCashDTO.getCashRegisterId()));
                String id = Long.toString(new Date().getTime());
                closedCash.setMoney(id);
            }

            */
            
        }
        
        return retour;
    }
}
