package com.ose.backend.service.cash.impl;


import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.service.cash.ICashRegistersServiceOSE;

@Service
public class CashRegistersServiceOSE implements ICashRegistersServiceOSE {
    
    @Autowired 
    ILocationsApiDAO locationsDAO;
    
    @Override
    public boolean loadFromDTO(Object source , CashRegisters cashRegisters) {
        boolean retour = false ;
        if (CashRegistersSimpleDTO.class.isInstance(source)) {
            CashRegistersSimpleDTO dto = (CashRegistersSimpleDTO) source;
            if((dto.getId()!= null && !dto.getId().equals(cashRegisters.getId())) || (dto.getId()==null && cashRegisters.getId() != null)) {
                retour = true ;
                cashRegisters.setId(dto.getId());
            }
            if((dto.getLabel()!= null && !dto.getLabel().equals(cashRegisters.getName())) || (dto.getLabel()==null && cashRegisters.getName() != null)) {
                retour = true ;
                cashRegisters.setName(dto.getLabel());
            }
            if(dto.getNextTicketId() > cashRegisters.getNextTicketId()) {
                retour = true ;
                cashRegisters.setNextTicketId(dto.getNextTicketId());
            }
            if(! dto.getLocationId().equals(cashRegisters.getLocation() == null ? "" : cashRegisters.getLocation().getId())) {
                retour = true ;
                Locations location = locationsDAO.findById(dto.getLocationId()).orElse(null);
                cashRegisters.setLocation(location);
            }
            
        }
        return retour;
    }

}
