package com.ose.backend.service.cash;

import org.pasteque.api.model.cash.ClosedCash;

public interface IClosedCashService {

    boolean loadFromDTO(Object source, ClosedCash closedCash);

}
