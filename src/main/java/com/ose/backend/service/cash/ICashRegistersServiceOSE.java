package com.ose.backend.service.cash;

import org.pasteque.api.model.cash.CashRegisters;

public interface ICashRegistersServiceOSE {

    boolean loadFromDTO(Object source, CashRegisters cashRegisters);

}
