package com.ose.backend.service.stock.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.security.IPeopleApiDAO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.products.IProductsDAO;
import com.ose.backend.dao.stocks.IInventoryDAO;
import com.ose.backend.dao.stocks.IInventoryDetailsDAO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.InventoryDTO;
import com.ose.backend.dto.stocks.InventoryDetailDTO;
import com.ose.backend.dto.stocks.InventoryDetailGroupedDTO;
import com.ose.backend.model.stocks.Inventory;
import com.ose.backend.model.stocks.InventoryDetails;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.service.products.IProductsServiceOSE;
import com.ose.backend.service.stock.IInventoryService;
import com.ose.backend.service.stock.IStocksServiceOse;

@Service
public class InventoryService implements IInventoryService {

    @Autowired
    private IInventoryDAO inventoryDAO;
    @Autowired
    private IInventoryDetailsDAO inventoryDetailsDAO;
    @Autowired
    private IPeopleApiDAO peopleDAO;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    @Autowired
    private IProductsDAO productsDAO;
    @Autowired
    private IStocksServiceOse stocksService;
    @Autowired
    private IProductsServiceOSE productsService;

    @Override
    public Object getByCriteria(String locationId, Long from, Long to, Long page, Long nbResults) {
        Date dfrom = from != null ? new Date(from) : null;
        Date dto = to != null ? new Date(to) : null;
        return AdaptableHelper.getAdapter(inventoryDAO.findByCriteria(locationId, dfrom, dto, page, nbResults, null, null),
                InventoryDTO.class);
    }

    @Override
    public Object initialize(String name, String locationId) {
        Inventory inventory = new Inventory();
        People people = peopleDAO.findByUsername(name);
        Locations location = locationsDAO.findById(locationId).orElse(null);
        inventory.setUser(people);
        inventory.setLocation(location);
        inventory.setType(Inventory.Type.COMPLETE);
        return inventory.getAdapter(InventoryDTO.class);
    }

    @Override
    public Object getById(String id) {
        Inventory inv = inventoryDAO.findById(id).orElse(null);
        return inv == null ? null : inv.getAdapter(InventoryDTO.class);
    }

    @Override
    public Object getInventoryDetails(String inventoryId , int limit) {
        return getInventoryDetailAdapter(inventoryDetailsDAO.findByInventory(inventoryId , limit));
    }
    
    @Override
    public Object getInventoryInfo(String inventoryId) {
        List<Integer> retour = new ArrayList<Integer>(2);
        List<Object[]> result = inventoryDetailsDAO.findByInventoryGrouped(inventoryId, null);
        retour.add(result.size());
        result = inventoryDetailsDAO.findMissingCountsByInventoryGrouped(inventoryId);
        retour.add(result.size());
        return retour;
    }

    @Override
    public Object getInventoryDetailsGrouped(String inventoryId) {
        List<Object[]> result = inventoryDetailsDAO.findByInventoryGrouped(inventoryId, null);
        List<InventoryDetailGroupedDTO> dtos = new ArrayList<InventoryDetailGroupedDTO>();
        for (Object[] objects : result) {
            InventoryDetailGroupedDTO dto = new InventoryDetailGroupedDTO();
            dto.setProductReference((String) objects[0]);
            dto.setProductLabel((String) objects[1]);
            dto.setQuantity(((BigDecimal) objects[2]).setScale( 0, BigDecimal.ROUND_HALF_UP ).longValue());
            dto.setStock((Double) objects[3]);
            dto.setPMPA(BigDecimal.valueOf((double)objects[4]).setScale( 2, BigDecimal.ROUND_HALF_UP ).doubleValue());
            dto.setValeur(BigDecimal.valueOf(( dto.getQuantity() - dto.getStock() ) * dto.getPMPA()).setScale( 2, BigDecimal.ROUND_HALF_UP ).doubleValue());
            Products produit = productsDAO.findById(dto.getProductReference()).orElse(null);
            dto.setProductLocation(produit == null ? null : produit.getProductLocation((String) objects[5]));
            dtos.add(dto);
        }
        
        result = inventoryDetailsDAO.findMissingCountsByInventoryGrouped(inventoryId);
        for (Object[] objects : result) {
            InventoryDetailGroupedDTO dto = new InventoryDetailGroupedDTO();
            dto.setProductReference((String) objects[0]);
            dto.setProductLabel((String) objects[1]);
            dto.setQuantity(BigDecimal.valueOf( (long) objects[2]).setScale( 0, BigDecimal.ROUND_HALF_UP ).longValue());
            dto.setStock((Double) objects[3]);
            dto.setPMPA(BigDecimal.valueOf((double)objects[4]).setScale( 2, BigDecimal.ROUND_HALF_UP ).doubleValue());
            dto.setValeur(BigDecimal.valueOf(( dto.getQuantity() - dto.getStock()) * dto.getPMPA()).setScale( 2, BigDecimal.ROUND_HALF_UP ).doubleValue());
            Products produit = productsDAO.findById(dto.getProductReference()).orElse(null);
            dto.setProductLocation(produit == null ? null : produit.getProductLocation((String) objects[5]));
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public Object saveInventory(InventoryDTO dto) {
        Inventory inventory = new Inventory();
        if (dto.getId() != null) {
            inventory = inventoryDAO.findById(dto.getId()).orElse(null);
        }
        inventory.setStartDate(dto.getStartDate());
        Locations location = locationsDAO.findById(dto.getLocationId()).orElse(null);
        inventory.setLocation(location);
        inventory.setName(dto.getName());
        inventory.setNote(dto.getNote());
        inventory.setStatus(dto.getStatusCode());
        inventory.setType(dto.getType());
        People people = peopleDAO.findById(dto.getUsernameId()).orElse(null);
        inventory.setUser(people);
        if (dto.getId() != null) {
            inventory = inventoryDAO.save(inventory);
        } else {
            //EDU - On ne devrait logiquement pas pouvoir avoir deux inventaires créés en simultanés à la ms près
            //Sinon passer en String incorporer le location ID dans la clef de l'inventaire
            inventory.createId();
            inventory = inventoryDAO.save(inventory);
        }
        if(Inventory.Type.RAZ.equals(inventory.getType()) &&  Inventory.Status.IN_PROGRESS.equals(inventory.getStatus())) {
            razCandidates(inventory.getUser().getUsername(), inventory.getId(), inventory.getStartDate().getTime());
        }
        return inventory.getAdapter(InventoryDTO.class);
    }

    @Override
    public Object saveInventoryDetails(String name, InventoryDetailDTO dto) {
        InventoryDetails inventoryDetails = new InventoryDetails();
        Inventory inventory = inventoryDAO.findById(dto.getInventoryId()).orElse(null);
        inventoryDetails.setDate(new Date());
        inventoryDetails.setInventory(inventory);
        if (dto.getProduct() != null && dto.getProduct().getId() != null && dto.getQuantity() != 0) {
            inventoryDetails.setProduct(productsDAO.findById(dto.getProduct().getId()).orElse(null));
            inventoryDetails.setQuantity(dto.getQuantity());
            inventoryDetails.setUser(peopleDAO.findByUsername(name));
            inventoryDetails.createId();
            return getInventoryDetailAdapter(inventoryDetailsDAO.save(inventoryDetails));
        }
        return null;
    }

    @Override
    public Object closeInventory(String inventoryId) {
        Inventory inventory = inventoryDAO.findById(inventoryId).orElse(null);

        List<Products> stocksProducts = new ArrayList<>();
        if (inventory.getType().equals(Inventory.Type.COMPLETE)) {
            stocksProducts = productsDAO.findProductsToCloseInventory(inventory.getLocation().getId(), inventoryId);
        } else {
            stocksProducts = productsDAO.findProductsToCloseInventory(null, inventoryId);
        }

        for (Products products : stocksProducts) {
            StockCurrent stockCurrent = stocksService.findByProductAndLocation(products.getId(), inventory.getLocation().getId());
            double stockUnits = 0;
            if (stockCurrent != null) {
                stockUnits = stockCurrent.getUnits();
            }
            long counted = inventoryDAO.sumQuantity(inventoryId, products.getId());

            double quantity = counted - stockUnits;
            stocksService.generateMovement(inventory.getLocation().getId(), products.getId(),  quantity < 0 ? -quantity : quantity, 0,
                    quantity < 0 ? StockDiary.Type.REASON_OUT_INVENTORY : StockDiary.Type.REASON_IN_INVENTORY, inventory.getName(),
                    inventory.getId(), null, null);
        }

        inventory.setEndDate(new Date());
        inventory.setStatus(Inventory.Status.FINISHED);
        inventoryDAO.save(inventory);

        return null;
    }

    @Override
    public Object canCreateNewInventory(String locationId) {
        List<Inventory> list = inventoryDAO.findByCriteria(locationId, null, null, 0L, 10L, Inventory.Type.COMPLETE,
                Inventory.Status.CREATED);
        list.addAll(inventoryDAO.findByCriteria(locationId, null, null, 0L, 10L, Inventory.Type.COMPLETE, Inventory.Status.IN_PROGRESS));
        return list.isEmpty();
    }

    @Override
    public List<InventoryDetailDTO> razCandidates(String name, String inventoryId , Long from) {
        // TODO Implémenter : Lister tout produit n'ayant pas bénéficié d'un inventaire depuis une date donnée
        List<InventoryDetailDTO> inventoryDetails = new ArrayList<>();
        Inventory inventory = inventoryDAO.findById(inventoryId).orElse(null);
        List<Products> stocksProducts = new ArrayList<>();
        stocksProducts = productsDAO.findProductsToCloseRAZInventory(inventory.getLocation().getId(), inventoryId, from);
        for (Products products : stocksProducts) {
            InventoryDetails inventoryDetail = new InventoryDetails();
            inventoryDetail.setDate(new Date());
            inventoryDetail.setInventory(inventory);
            inventoryDetail.setProduct(products);
            inventoryDetail.setQuantity(0);
            inventoryDetail.setUser(peopleDAO.findByUsername(name));
            inventoryDetail.createId();
            inventoryDetails.add(getInventoryDetailAdapter(inventoryDetailsDAO.save(inventoryDetail)));          
        }
        return inventoryDetails;
    }
    
    @Override
    public InventoryDetailDTO getInventoryDetailAdapter(InventoryDetails detail) {
            InventoryDetailDTO dto = new InventoryDetailDTO();
            dto.setDate(detail.getDate());
            dto.setId(detail.getId());
            dto.setInventoryId(detail.getInventory().getId());
            dto.setProduct(productsService.getAdapter(detail.getProduct() , ProductsResultDTO.class , false));
            dto.setProductLocation(detail.getProduct(),detail.getInventory().getLocation().getId());
            dto.setQuantity(detail.getQuantity());
            if (detail.getUser() != null) {
                dto.setUsername(detail.getUser().getUsername());
                dto.setUsernameId(detail.getUser().getId());
            }
            return dto;
    }
    
    @Override
    public List<InventoryDetailDTO> getInventoryDetailAdapter( List<InventoryDetails> liste ) {
        List<InventoryDetailDTO> listDTO = new ArrayList<>();
        for (InventoryDetails adaptable : liste) {
            listDTO.add(getInventoryDetailAdapter(adaptable));
        }
        return listDTO;
    }
}
