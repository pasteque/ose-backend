package com.ose.backend.service.stock;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.stock.IStocksService;

import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.dto.stocks.StockMovementDetailDTO;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.model.stocks.StockLevel;

public interface IStocksServiceOse extends IStocksService{

    StockCurrent findByProductAndLocation(String productId, String locationId);

    List<StockDiary> findByLocationAndProductAndDate(String locationId,
            String productId, String dateFrom);
    
    void saveStocksMouvements();

    Object generateMovement(String locationId, String productId, double units,
            double price, int reason, String note, String reference,
            String destinationId, Date date);

    Object generateMovement(String locationId, String productId, double units,
            double price, int reason, String note, String reference,
            String destinationId, Date date, Boolean cacheOnly);

    List<StockCurrent> findListByProductAndLocation(String productId,
            String locationId , Integer from , Integer nbResult);

    double getQuantityToOrder(String locationId, String productId);

    StockCurrent getStockProducts(String locationId, String productId);

    StockLevel getStockLevel(String locationId, String productId);
    
    Object saveMovement(StockMovementDetailDTO dto);

    Object initializeMovement(String locationId, String productId);

    Object getReasons(String sens);  

    void createStockLevelJobs(List<StockLevelDTO> dtos, String action);

    Object findAllStockLevel();

    Object saveStockLevel(StockLevelDTO dto);

    Object deleteStockLevel(StockLevelDTO dto);

    List<StockLevelDTO> getStockLevelTableByCritria(
            List<ProductsResultDTO> lproducts,
            List<LocationsInfoDTO> llocations,
            List<TariffAreasIndexDTO> ltariffs);

    List<StockLevelDTO> getStockLevelByCritria(List<ProductsResultDTO> lproducts,
            List<LocationsInfoDTO> llocations, Date dateStart, Date dateStop);


    Object getStocksMovementsByCriteria(String locationId, String productId,
            String reference, String type, String dateFrom, String dateTo,
            Long from, Long nbResults, String sort, String resultType);

    Object createStockLevels(List<StockLevel> stockLevelList);

    Object updateStockLevels(List<StockLevel> stockLevelList);

    /**
     * Forcer le stock à atteindre une valeur donnée
     * On utilise le type de mouvement RESET, il s'agit d'un mouvement distant dont nous n'avons pas le détail 
     * Typiquement utilisé pour faire remonter le niveau de stock d'un entrepot distant sans avoir à se préocuper
     * des mouvements intermédiaires
     * On ne valorise d'ailleurs pas ce mouvement et lui attribue une référence en dur
     */
    boolean resetStock(String locationId, String productId, Double newStock);

    double checkIntervalMovements(Date date, String locationId,
            String productId);

    <T> T getStockLevelAdapter(StockLevel stockLevel, Class<T> selector);

    <T> T getStockLevelAdapter(StockLevel stockLevel, Class<T> selector,
            boolean filtered);

    <T> T getStockDiaryAdapter(StockDiary stockDiary, Class<T> selector);

    <T> T getStockDiaryAdapter(StockDiary stockDiary, Class<T> selector,
            boolean filtered);

    <T> T getStockCurrentAdapter(StockCurrent stockCurrent, Class<T> selector);

    <T> T getStockCurrentAdapter(StockCurrent stockCurrent, Class<T> selector,
            boolean filtered);

    boolean loadStockDiaryFromDTO(StockDiary stockDiary, Object source);

    boolean loadStockCurrentFromDTO(StockCurrent stockCurrent, Object source);

    <T> List<T> getStockDiaryListAdapter(List<StockDiary> stockDiaries,
            Class<T> selector);

    boolean createOrderStockMoves(List<PaletteDetailDTO> paletteDetails,
            OrderDTO currentOrder , PaletteDTO currentPalette);

    void createStockMove(Locations orig, Locations dest, String productId,
            String ref, Date dateMvt ,  double units, double price, int reason, String note);

    void adjustMovements();


}
