package com.ose.backend.service.stock;

import java.util.List;

import com.ose.backend.dto.stocks.InventoryDTO;
import com.ose.backend.dto.stocks.InventoryDetailDTO;
import com.ose.backend.model.stocks.InventoryDetails;

public interface IInventoryService {

    Object getByCriteria(String locationId, Long from, Long to, Long page, Long nbResults);

    Object initialize(String name, String locationId);

    Object getById(String id);

    Object getInventoryDetails(String inventoryId , int limit);

    Object saveInventory(InventoryDTO dto);

    Object saveInventoryDetails(String name, InventoryDetailDTO dto);

    Object getInventoryDetailsGrouped(String inventoryId);

    Object closeInventory(String inventoryId);

    Object canCreateNewInventory(String locationId);

    List<InventoryDetailDTO> razCandidates(String name, String inventoryId, Long from);

    Object getInventoryInfo(String inventoryId);

    List<InventoryDetailDTO> getInventoryDetailAdapter(
            List<InventoryDetails> liste);

    InventoryDetailDTO getInventoryDetailAdapter(InventoryDetails detail);

}
