package com.ose.backend.service.stock.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.ProductsIndexDTO;
import org.pasteque.api.dto.products.TariffAreasIndexDTO;
import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ose.backend.dao.stocks.IStockCurrentDAO;
import com.ose.backend.dao.stocks.IStockDiaryDAO;
import com.ose.backend.dao.stocks.IStockLevelDAO;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.ProductMovementDTO;
import com.ose.backend.dto.stocks.ReasonDTO;
import com.ose.backend.dto.stocks.StockCurrentDTO;
import com.ose.backend.dto.stocks.StockDiaryDTO;
import com.ose.backend.dto.stocks.StockDiaryIndexDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.dto.stocks.StockLevelLocationDTO;
import com.ose.backend.dto.stocks.StockMovementDetailDTO;
import com.ose.backend.dto.stocks.StockMovementHeaderDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockCurrentId;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.model.stocks.StockLevel;
import com.ose.backend.model.stocks.StockMovement;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.service.orders.IOrdersService;
import com.ose.backend.service.products.IProductsServiceOSE;
import com.ose.backend.service.stock.IStocksServiceOse;
import com.ose.backend.specs.stocks.StockCurrentSpecification;
import com.ose.backend.specs.stocks.StockDiarySpecification;

@Service
@Primary
public class StockServiceOSE implements IStocksServiceOse{

    @Autowired
    private IStockCurrentDAO stockCurrentDAO;
    @Autowired
    private IStockDiaryDAO stockDiaryDAO;
    @Autowired
    private IStockLevelDAO stockLevelDAO;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    @Autowired
    private ILocationsServiceOSE locationsService;
    @Autowired
    private IProductsApiDAO productsDAO;
    @Autowired
    private IJobsService jobService;
    @Autowired
    private ITicketsApiDAO ticketsDAO;
    @Autowired
    private IStocksServiceOse stockService;
    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private IProductsServiceOSE productsService;

    private HashMap<String,StockCurrent> newCurrent = new HashMap<String,StockCurrent>();
    private HashMap<String,StockCurrent> updCurrent = new HashMap<String,StockCurrent>();
    private HashMap<String,StockDiary> newMove = new HashMap<String,StockDiary>();
    private HashMap<String,StockDiary> updMove = new HashMap<String,StockDiary>();
    private HashMap<String,Double> updMoveQty = new HashMap<String,Double>();
    private HashMap<String,Date> origCurrent = new HashMap<String,Date>();

    @Override
    public StockCurrent findByProductAndLocation(String productId , String locationId) {
        StockCurrentSpecification specs = new StockCurrentSpecification();
        specs.add(new SearchCriteria(StockCurrent.Fields.PRODUCT ,  productId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockCurrent.Fields.LOCATION ,  locationId , SearchOperation.EQUAL));

        return stockCurrentDAO.findOne(specs).orElse(null);
    }

    public StockCurrent findByProductAndLocationWithCache(String productId , String locationId,boolean useCache) {
        String currentStockMapKey = getStockMapKey(productId , locationId); 
        StockCurrent stockCurrent = updCurrent.get(currentStockMapKey);

        if (stockCurrent == null) {
            stockCurrent =  newCurrent.get(currentStockMapKey);
        }

        if (stockCurrent == null) {
            stockCurrent = findByProductAndLocation(productId, locationId);
            if(useCache) {
                origCurrent.put(currentStockMapKey , new Date());
            }
        }

        return stockCurrent;
    }

    private String getStockMapKey(String productId , String locationId) {
        return locationId.concat("-").concat(productId);
    }

    @Override
    public List<StockCurrent> findListByProductAndLocation(String productId , String locationId, Integer from  , Integer nbResults) {
        StockCurrentSpecification specs = new StockCurrentSpecification();
        StockCurrentSpecification specsIdProduct = new StockCurrentSpecification();
        StockCurrentSpecification specsNameProduct = new StockCurrentSpecification();
        StockCurrentSpecification specsEANProduct = new StockCurrentSpecification();
        specsIdProduct.add(new SearchCriteria(StockCurrent.Fields.PRODUCT ,  productId , SearchOperation.MATCH));
        specsNameProduct.add(new SearchCriteria(Products.Fields.NAME ,  productId , SearchOperation.MATCH));
        specsEANProduct.add(new SearchCriteria(Products.Fields.REFERENCES ,  productId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockCurrent.Fields.LOCATION ,  locationId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Products.Fields.DELETED ,  false , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Products.Fields.STOCKABLE ,  true , SearchOperation.EQUAL));

        Sort sort = Sort.by(StockCurrent.Fields.LOCATION).ascending().and(Sort.by(StockCurrent.Fields.PRODUCT).ascending()) ;

        Specification<StockCurrent> distinct = StockCurrentSpecification.distinct();
        //TODO traiter pagination
        return stockCurrentDAO.findAll(distinct.and(specs.and(specsIdProduct.or(specsNameProduct).or(specsEANProduct))), sort);

    }

    @Override
    public List<StockDiary> findByLocationAndProductAndDate( String locationId , String productId ,String dateFrom) {
        StockDiarySpecification specs = new StockDiarySpecification();
        specs.add(new SearchCriteria(StockDiary.Fields.PRODUCT ,  productId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockDiary.Fields.LOCATION ,  locationId , SearchOperation.EQUAL));
        if(dateFrom != null) {
            Date dFrom = new Date(Long.parseLong(dateFrom));
            specs.add(new SearchCriteria(StockDiary.Fields.DATE_NEW ,  DateHelper.formatSQLTimeStamp(dFrom) , SearchOperation.GREATER_THAN_EQUAL));
        }

        Sort sort = Sort.by(StockDiary.Fields.LOCATION).ascending().and(Sort.by(StockDiary.Fields.PRODUCT).ascending()).and(Sort.by(StockDiary.Fields.DATE_NEW).ascending()) ; 
        return stockDiaryDAO.findAll(specs, sort);

    }




    @Override
    public void generateMovement(Tickets tickets, Products products,
            POSTicketLinesDTO ticketLinesDTO) {
        //      on avait ceci côté ticket ça pourrait être dans l'implémentation

        if (!ticketLinesDTO.isOrder() && products != null) {
            StockMovement movement = new StockMovement();
            movement.setDate(tickets.getReceipts().getDateNew());
            movement.setTypeMovement(StockDiary.Type.REASON_OUT_SELL);
            movement.setLocations(tickets.getReceipts().getClosedCash().getCashRegister().getLocation());
            movement.setProducts(products);
            // movement.setAttributeSetInstanceId(attributeSetInstanceId);
            movement.setUnits(ticketLinesDTO.getQuantity());
            movement.setPrice(ticketLinesDTO.getPrice());
            movement.setReference(String.valueOf(tickets.getTicketId()));
            //EDU On stocke dans la note le numéro de ligne de ticket concerné
            movement.setNote(Integer.toString(ticketLinesDTO.getDispOrder()));
            generateMovement(movement);
        }

    }


    public boolean generateMovement(StockMovement movement) {

        List<ProductsComponents> productsComponents = movement.getProducts().getComponentsProducts();
        Double stock = 0D;
        Double mostRecent = 0D;

        if (productsComponents.size() == 0) {
            productsComponents = new ArrayList<ProductsComponents>(1);
            productsComponents.add(new ProductsComponents());
            productsComponents.get(0).setQuantity(1);
            productsComponents.get(0).setComponent(movement.getProducts());
        }

        for (ProductsComponents singleComponent : productsComponents) {
            mostRecent = 0D;
            if (singleComponent.getComponent().isStockable()) {

                try {
                    StockCurrent stockCurrent = findByProductAndLocation(singleComponent.getComponent().getId() ,   movement.getLocations().getId());

                    if (stockCurrent == null) {
                        stockCurrent = new StockCurrent();
                        stockCurrent.setLocations(movement.getLocations());
                        stockCurrent.setProducts(singleComponent.getComponent());
                        if (movement.isIn()) {
                            stockCurrent.setUnits(movement.getUnits() * singleComponent.getQuantity());
                        } else {
                            stockCurrent.setUnits(-movement.getUnits() * singleComponent.getQuantity());
                        }
                        stockCurrentDAO.create(stockCurrent);
                    } else {
                        if (movement.isIn()) {
                            stockCurrent.setUnits(stockCurrent.getUnits() + movement.getUnits() * singleComponent.getQuantity());
                        } else {
                            stockCurrent.setUnits(stockCurrent.getUnits() - movement.getUnits() * singleComponent.getQuantity());
                        }
                        stockCurrentDAO.save(stockCurrent);
                    }
                    stock = stockCurrent.getUnits();
                } catch (Exception exception) {
                    return false;
                }

                try {
                    StockDiary stockDiary = new StockDiary();
                    // stockDiary.setId(Long.toString(new Date().getTime()));

                    stockDiary.setLocations(movement.getLocations());
                    stockDiary.setProducts(singleComponent.getComponent());
                    //stockDiary.setPrice(movement.getPrice());
                    //Modification 13-11 On passe au PMPA du moment
                    stockDiary.setNote(movement.getNote());
                    if (movement.isIn()) {
                        stockDiary.setPrice(movement.getUnits() * singleComponent.getQuantity() *( singleComponent.getComponent().getStockCost() == null ? 0D : singleComponent.getComponent().getStockCost()));
                        stockDiary.setUnits(movement.getUnits() * singleComponent.getQuantity());
                    } else {
                        stockDiary.setPrice(-movement.getUnits() * singleComponent.getQuantity() * ( singleComponent.getComponent().getStockCost() == null ? 0D : singleComponent.getComponent().getStockCost()));
                        stockDiary.setUnits(-movement.getUnits() * singleComponent.getQuantity());
                    }
                    stockDiary.setReason(movement.getTypeMovement());
                    stockDiary.setDateNew(movement.getDate());
                    stockDiary.setReference(movement.getReference());
                    mostRecent = stockDiaryDAO.findNbChange(singleComponent.getComponent().getId(), movement.getLocations().getId(), movement.getDate(), null, null);
                    if(mostRecent != 0D) {
                        //On update tout ce qui s'est fait depuis avec un delta de stockDiary.getUnits()
                        //on est dans le cas où on enregistre un mouvement sur un produit  de manière antidaté et il y a eu des mouvements plus récents
                        //du coup ceux ci n'ont plus un startingStock ok
                        List<StockDiary> stockDiariesToUpd = findByLocationAndProductAndDate(movement.getLocations().getId(), singleComponent.getComponent().getId(), String.valueOf(movement.getDate().getTime()));
                        if(stockDiariesToUpd != null && stockDiariesToUpd.size() > 0) {
                            for(StockDiary stockDiarieToUpd : stockDiariesToUpd) {
                                if(stockDiarieToUpd.getStartingStock() != null){
                                    stockDiarieToUpd.setStartingStock(stockDiarieToUpd.getStartingStock()+mostRecent);
                                    stockDiaryDAO.save(stockDiarieToUpd);
                                }
                            }
                        }
                    }
                    stockDiary.setEndingStock(stock - mostRecent);
                    stockDiary.createId(stock);
                    //TODO set le user
                    //stockDiary.setUser(user);
                    stockDiaryDAO.create(stockDiary);

                } catch (Exception exception) {
                    return false;
                }
            }
        }

        return true;
    }


    @Override
    public Object generateMovement(String locationId, String productId, double units, double price, int reason, String note,
            String reference, String destinationId, Date date) {
        return generateMovement( locationId,  productId,  units,  price,  reason,  note,
                reference,  destinationId,  date, false);
    }


    @Override
    public Object generateMovement(String locationId, String productId, double units, double price, int reason, String note,
            String reference, String destinationId, Date date, Boolean cacheOnly) {

        Double stock;
        double mostRecent = 0D ;
        Locations location = locationsDAO.findById(locationId).orElse(null);
        Products product = productsDAO.findById(productId).orElse(null);

        StockCurrent stockCurrent = findByProductAndLocationWithCache(productId, locationId , cacheOnly);

        //EDU quid du cas où on arrive avec un locationId erroné ? ou un produit inconnu ?
        if(product == null || location == null) {
            return false;
        }

        String currentStockMapKey = getStockMapKey(productId , locationId); 

        if (stockCurrent == null) {
            stockCurrent = new StockCurrent();
            stockCurrent.setLocations(location);
            stockCurrent.setProducts(product);
            if (reason >= 0) {
                stockCurrent.setUnits(units);
            } else {
                stockCurrent.setUnits(-units);
            }
            if(cacheOnly) {
                //stockCurrentDAO.persist(stockCurrent);
                newCurrent.put(currentStockMapKey , stockCurrent);
            } else {
                stockCurrentDAO.create(stockCurrent);
            }
        } else {
            if (reason >= 0) {
                stockCurrent.setUnits(stockCurrent.getUnits() + units);
            } else {
                stockCurrent.setUnits(stockCurrent.getUnits() - units);
            }
            if(cacheOnly) {
                //stockCurrentDAO.merge(stockCurrent);
                updCurrent.put(currentStockMapKey , stockCurrent);
            } else {
                stockCurrentDAO.save(stockCurrent);
            }
        }

        stock = stockCurrent.getUnits();

        try {
            StockDiary stockDiary = new StockDiary();

            stockDiary.setLocations(location);
            stockDiary.setProducts(product);
            // Que veut dire cette valeur ?
            //stockDiary.setPrice(0D);
            stockDiary.setNote(note);
            stockDiary.setReference(reference);
            if (reason >= 0) {
                stockDiary.setPrice(units * ( product.getStockCost() == null ? 0D : product.getStockCost()));
                stockDiary.setUnits(units);
            } else {
                stockDiary.setPrice(-units * ( product.getStockCost() == null ? 0D : product.getStockCost()));
                stockDiary.setUnits(-units);
            }
            stockDiary.setReason(reason);
            mostRecent = checkIntervalMovements(date, locationId, productId);
            stockDiary.setEndingStock(stock - mostRecent);
            stockDiary.setDateNew(date == null ? new Date() : date);
            stockDiary.createId(stock);
            if (destinationId != null) {
                stockDiary.setDestination(locationsDAO.findById(destinationId).orElse(null));
            }
            if(cacheOnly) {
                newMove.put(stockDiary.getId(), stockDiary);
            } else {

                stockDiary = stockDiaryDAO.create(stockDiary);
            }
            return stockService.getStockDiaryAdapter(stockDiary , StockMovementDetailDTO.class);
        } catch (Exception exception) {
            return false;
        }

    }

    /**
     * EDU 2020 11 Modification pour uniquement stocker les modifications à effectuer pendant le parcours mais ne pas en réaliser
     */
    @Override
    public double checkIntervalMovements(Date date , String locationId, String productId) {
        double mostRecent = 0D;
        if(date != null) {
            mostRecent = stockDiaryDAO.findNbChange(productId, locationId, date, null, null);

            if(mostRecent != 0D) {
                //On update tout ce qui s'est fait depuis avec un delta de stockDiary.getUnits()
                //on est dans le cas où on enregistre un mouvement sur un produit  de manière antidaté et il y a eu des mouvements plus récents
                //du coup ceux ci n'ont plus un startingStock ok
                List<StockDiary> stockDiariesToUpd = findByLocationAndProductAndDate(locationId, productId, String.valueOf(date.getTime()));
                if(stockDiariesToUpd != null && stockDiariesToUpd.size() > 0) {
                    for(StockDiary stockDiarieToUpd : stockDiariesToUpd) {
                        if(stockDiarieToUpd.getStartingStock() != null){
                            Double delta = mostRecent;
                            if(updMoveQty.containsKey(stockDiarieToUpd.getId())) {
                                //On a délà mis à jour ce mouvement sur la transaction en cours
                                //On repart d'où on était
                                delta += updMoveQty.get(stockDiarieToUpd.getId());
                            }
                            updMove.put(stockDiarieToUpd.getId(), stockDiarieToUpd);
                            updMoveQty.put(stockDiarieToUpd.getId(), delta);


                        }
                    }
                }
            }
        }
        return mostRecent;
    }

    @Override
    public void saveStocksMouvements() {
        List<StockDiary> objectList = null;
        List<StockCurrent> curentStockList = null;

        /*
        ICI l'idée serait d'obtenir les mouvements de stock qui ont eu lieu dans l'intervalle
        Pour chaque couple produit - location
        On sauve ceux qui peuvent l'être = non concernés par les mouvements
        On corrige les autres
        On bis tant que les listes ne sont pas vides
         */

        objectList = new ArrayList<StockDiary>(newMove.values()) ;
        if(objectList != null && objectList.size()>0) {
            stockDiaryDAO.create(objectList);
            newMove = new HashMap<String,StockDiary>();
        }
        objectList = new ArrayList<StockDiary>(updMove.values()) ;
        if(objectList != null && objectList.size()>0) {
            stockDiaryDAO.saveAll(objectList);
            updMove = new HashMap<String,StockDiary>();
        }
        curentStockList = new ArrayList<StockCurrent>(newCurrent.values()) ;
        if(curentStockList != null && curentStockList.size()>0) {
            stockCurrentDAO.create(curentStockList);
            newCurrent = new HashMap<String,StockCurrent>();
        }
        curentStockList = new ArrayList<StockCurrent>(updCurrent.values()) ;
        if(curentStockList != null && curentStockList.size()>0) {
            stockCurrentDAO.saveAll(curentStockList);
            updCurrent = new HashMap<String,StockCurrent>();
        }
        origCurrent = new HashMap<String,Date>();
    }


    @Override
    public double getQuantityToOrder(String locationId , String productId) {
        StockCurrent current = getStockProducts(locationId , productId);
        StockLevel level = getStockLevel(locationId , productId);
        if (level != null) {
            // j'imagine que le stockSecurity est le stock mini, seuil de réappro
            // et stockMaximum le stock à atteindre, stock type
            if (current != null) {
                double qty = level.getStockSecurity() - current.getUnits();
                return qty >= 0 ? level.getStockMaximum() - Math.max(0D, current.getUnits()) : 0;
            } else {
                return level.getStockMaximum();
            }
        }
        return 0;
    }

    @Override
    public StockLevel getStockLevel(String locationId, String productId) {
        return stockLevelDAO.findById(StockLevel.createId( locationId, productId)).orElse(null);
    }

    @Override
    public StockCurrent getStockProducts(String locationId, String productId) {
        return stockCurrentDAO.findById(new StockCurrentId( locationId, productId)).orElse(null);
    }

    @Override
    public Object saveMovement(StockMovementDetailDTO dto) {
        Object o = null;
        if (dto.getReference() == null) {
            StockDiarySpecification specs = new StockDiarySpecification();
            specs.add(new SearchCriteria(StockDiary.Fields.LOCATION , dto.getLocation().getId() , SearchOperation.EQUAL ));
            long count = stockDiaryDAO.count(specs);
            dto.setReference(dto.getLocation().getId() + "/" + count);
        }
        if (!dto.getProducts().isEmpty()) {
            for (ProductMovementDTO productMovementDTO : dto.getProducts()) {
                o = generateMovement(dto.getLocation().getId(), productMovementDTO.getProduct().getId(), productMovementDTO.getUnits(),
                        productMovementDTO.getPrice(), dto.getReason(), dto.getNote() == null ? productMovementDTO.getNote() : dto.getNote() , dto.getReference(),
                                dto.getDestination() != null ? dto.getDestination().getId() : null , null);
                if (dto.getReason() == StockDiary.Type.REASON_OUT_TRANSFERT || dto.getReason() == StockDiary.Type.REASON_IN_TRANSFERT) {
                    o = generateMovement(dto.getDestination().getId(), productMovementDTO.getProduct().getId(),
                            productMovementDTO.getUnits(), productMovementDTO.getPrice(), -dto.getReason(), dto.getNote() == null ? productMovementDTO.getNote() : dto.getNote(),
                                    dto.getReference(), dto.getLocation().getId(),null);
                }
            }
        }
        adjustMovements();
        return o;
    }

    @Override
    public Object initializeMovement(String locationId, String productId) {
        StockMovementDetailDTO dto = new StockMovementDetailDTO();
        dto.setDate(new Date());
        Locations location = locationsDAO.findById(locationId).orElse(null);
        dto.setLocation(location.getAdapter(LocationsInfoDTO.class));
        if (productId != null) {
            Products products = productsDAO.findById(productId).orElse(null);
            ProductMovementDTO productMovementDTO = new ProductMovementDTO();
            productMovementDTO.setProduct(productsService.getAdapter(products , ProductsResultDTO.class , false));
            productMovementDTO.setUnits(1);
            dto.getProducts().add(productMovementDTO);
        }
        return dto;
    }

    @Override
    public Object getStocksMovementsByCriteria(String locationId, String productsId, String reference, String type , String dateFrom , String dateTo , Long from, Long nbResults, String sort, String resultType) {

        Class<?> selector = StockMovementDetailDTO.class ;

        if("StockMovementHeaderDTO".equals(resultType)) {
            selector = StockMovementHeaderDTO.class ;
        }

        if (from == null) {
            from = 0L;
        }
        if (nbResults == null) {
            nbResults = 100L;
        }

        if (nbResults <= 0) {
            nbResults = null;
        }

        List<StockDiary> stockDiaries = stockDiaryDAO.findByCriteria(locationId, productsId, reference, type, dateFrom , dateTo ,from, nbResults, sort);
        if (sort == null && reference != null && !stockDiaries.isEmpty()) {
            StockMovementDetailDTO dto = stockService.getStockDiaryAdapter(stockDiaries.get(0) , StockMovementDetailDTO.class);
            for (StockDiary stockDiary : stockDiaries) {
                ProductMovementDTO product = new ProductMovementDTO();
                product.setProduct(productsService.getAdapter( stockDiary.getProducts() , ProductsResultDTO.class , false));
                product.setPrice(stockDiary.getPrice());
                product.setUnits(stockDiary.getUnits());
                if(stockDiary.getNote() != null ) {
                    product.setNote(stockDiary.getNote());
                    if (!stockDiary.getNote().equals(dto.getNote())) {
                        dto.setNote(null);
                    }
                }
                dto.getProducts().add(product);

            }

            return dto;
        }
        return stockService.getStockDiaryListAdapter(stockDiaries, selector);
    }

    @Override
    public Object getReasons(String sens) {
        List<ReasonDTO> reasons = new ArrayList<ReasonDTO>();
        ReasonDTO dto = new ReasonDTO();
        if (sens.equals("E")) {
            dto.setReason(StockDiary.Type.REASON_IN_BUY);
            dto.setLabel("Entree - Stock");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_IN_REFUND);
            dto.setLabel("Entree - Retour");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_IN_MOVEMENT);
            dto.setLabel("Entree - Transfert groupe");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_IN_TRANSFERT);
            dto.setLabel("Entree - Transfert interne");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_IN_INVENTORY);
            dto.setLabel("Entree - Correction");
            reasons.add(dto);
        } else {
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_SELL);
            dto.setLabel("Sortie - Vente");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_REFUND);
            dto.setLabel("Sortie - Retour");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_BACK);
            dto.setLabel("Sortie - Stock");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_MOVEMENT);
            dto.setLabel("Sortie - Transfert groupe");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_TRANSFERT);
            dto.setLabel("Sortie - Transfert interne");
            reasons.add(dto);
            dto = new ReasonDTO();
            dto.setReason(StockDiary.Type.REASON_OUT_INVENTORY);
            dto.setLabel("Sortie - Correction");
            reasons.add(dto);
        }

        return reasons;
    }

    @Override
    public Object saveStockLevel(StockLevelDTO dto) {
        StockLevel stockLevel = dto.getId() == null ? null : stockLevelDAO.findById(dto.getId()).orElse(null);
        Locations location = dto.getLocationsId() == null ? null : locationsDAO.findById(dto.getLocationsId()).orElse(null);
        Products products = dto.getProductsId() == null ? null : productsDAO.findById(dto.getProductsId()).orElse(null);
        if(location != null && products != null) {
            if (stockLevel == null) {
                stockLevel = new StockLevel();
                stockLevel.setId(dto.getLocationsId().concat("-").concat(dto.getProductsId()));
                stockLevel.setLocations(location);
                stockLevel.setProducts(products);
                stockLevel.setStockSecurity(dto.getStockSecurity());
                stockLevel.setStockMaximum(dto.getStockMaximum());
                stockLevel.setLastUpdate(new Date());
                stockLevelDAO.save(stockLevel);
                createStockLevelJobs(Arrays.asList(dto), Job.Actions.INSERT);
            } else {                
                if(loadStockLevelFromDTO(stockLevel , dto)) {
                    stockLevel.setLastUpdate(new Date());
                    stockLevelDAO.save(stockLevel);
                    createStockLevelJobs(Arrays.asList(dto), Job.Actions.UPDATE);
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object deleteStockLevel(StockLevelDTO dto) {
        Optional<StockLevel> stockLevel = stockLevelDAO.findById(dto.getId());

        if (stockLevel.isPresent()) {
            stockLevelDAO.delete(stockLevel.get());
            createStockLevelJobs(Arrays.asList(dto), Job.Actions.DELETE);
        }
        return true;
    }

    @Override
    public Object createStockLevels(List<StockLevel> stockLevelList) {
        stockLevelDAO.create(stockLevelList);
        createStockLevelJobs(AdaptableHelper.getAdapter(stockLevelList , StockLevelDTO.class), Job.Actions.INSERT);
        return true;
    }

    @Override
    public Object updateStockLevels(List<StockLevel> stockLevelList) {
        stockLevelDAO.update(stockLevelList);
        createStockLevelJobs(AdaptableHelper.getAdapter(stockLevelList , StockLevelDTO.class), Job.Actions.UPDATE);
        return true;

    }

    @Override 
    public void createStockLevelJobs(List<StockLevelDTO> dtos , String action) {
        Locations localServer = locationsDAO.findTopByServerDefaultTrue();
        LocationsInfoDTO localServerdto = localServer == null ? null : localServer.getAdapter(LocationsInfoDTO.class);
        Job job = JobHelper.createJobStockType( localServerdto);
        if( job != null) {
            //Dans ce cas nous sommes sur le back central donc remplir et propager 'plus'
            try {
                JobHelper.fillJobStockType(job, action, dtos);

                List<LocationsInfoDTO> Locationsdtos = AdaptableHelper.getAdapter( locationsService.findAllParentLocations(false, null ,false ,false ) , LocationsInfoDTO.class);
                List<Job> jobList = JobHelper.createJobForEveryBack(job, Locationsdtos);
                for(Job element : jobList) {
                    jobService.save(element);
                }
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object findAllStockLevel() {
        return AdaptableHelper.getAdapter(stockLevelDAO.findAll() , StockLevelDTO.class);
    }

    @Override
    public List<StockLevelDTO>  getStockLevelTableByCritria(List<ProductsResultDTO> productsRestriction ,
            List<LocationsInfoDTO> locationsRestriction ,
            List<TariffAreasIndexDTO> tariffsRestriction) {

        List<String> restrictionsProductId = new ArrayList<String>();
        List<String> restrictionsLocationId= new ArrayList<String>();
        List<String> restrictionsTarifId= new ArrayList<String>();
        for (ProductsResultDTO dto : productsRestriction) {

            restrictionsProductId.add(dto.getId());
        }
        for (LocationsInfoDTO dto : locationsRestriction) {

            restrictionsLocationId.add(dto.getId());
        }
        for (TariffAreasIndexDTO dto : tariffsRestriction) {

            restrictionsTarifId.add(String.valueOf(dto.getId()));
        }
        List<Map<String, Object>> results = stockLevelDAO.findStockLevelDTOByCritria(restrictionsProductId,restrictionsLocationId,restrictionsTarifId);
        StockLevelDTO stockLevelDto = null;
        String oldProdutId = null;
        List<StockLevelDTO> ls = new ArrayList<StockLevelDTO>();
        StockLevelLocationDTO stockLevelLocationDTO=null;
        for (Map<String, Object> mapValue : results) {

            String productId = (String) mapValue.get("productId");
            String locationId = (String) mapValue.get("locationId");
            String productName = (String) mapValue.get("productName");
            String locationName = (String) mapValue.get("locationName");
            double stocksecurity =  mapValue.get("stocksecurity") == null ? 0:(double) mapValue.get("stocksecurity");
            double stockMaximum = mapValue.get("stockMaximum") == null ? 0:(double) mapValue.get("stockMaximum");


            if (oldProdutId == null || oldProdutId.compareTo(productId) != 0)
            {
                if (stockLevelDto != null) {
                    ls.add(stockLevelDto);
                }
                stockLevelDto = new StockLevelDTO();
                stockLevelDto.setProductsId(productId);
                stockLevelDto.setProductName(productName);
                if ((oldProdutId == null || oldProdutId.compareTo(productId) != 0) && locationId != null) {//lors du premier passage ou au chagement de produit, on construit le stock type global, dans le cas ou existe pas
                    stockLevelLocationDTO = new StockLevelLocationDTO();
                    stockLevelLocationDTO.setLocationsId(null);
                    stockLevelLocationDTO.setLocationName(null);
                    stockLevelLocationDTO.setStockSecurity(0);
                    stockLevelLocationDTO.setStockMaximum(0);
                    stockLevelDto.getStockLevelLocationsDTO().add(stockLevelLocationDTO);
                }
                oldProdutId = stockLevelDto.getProductsId();
            }
            stockLevelLocationDTO = new StockLevelLocationDTO();
            stockLevelLocationDTO.setLocationsId(locationId);
            stockLevelLocationDTO.setLocationName(locationName);
            stockLevelLocationDTO.setStockSecurity((stocksecurity));
            stockLevelLocationDTO.setStockMaximum(stockMaximum);
            stockLevelDto.getStockLevelLocationsDTO().add(stockLevelLocationDTO);
        }
        if (stockLevelDto!=null) {
            ls.add(stockLevelDto);
        }
        return ls;
    }

    /**
     * Une recherche multicritères produits/locations des stocks levels
     * 
     * On consome findListByCriterias
     */
    @Override
    public List<StockLevelDTO> getStockLevelByCritria(List<ProductsResultDTO> productsRestriction, 
            List<LocationsInfoDTO> locationsRestriction,
            Date from,Date to) {
        List<String> restrictionsProductId = new ArrayList<>();
        List<String> restrictionsLocationId = new ArrayList<>();
        for (ProductsResultDTO dto : productsRestriction) {
            restrictionsProductId.add(dto.getId());
        }
        for (LocationsInfoDTO dto : locationsRestriction) {
            restrictionsLocationId.add(dto.getId());
        }
        return AdaptableHelper.getAdapter(stockLevelDAO.findListByCriteria(restrictionsProductId , restrictionsLocationId  ,  from , to ) , StockLevelDTO.class);
    }

    /**
     * Forcer le stock à atteindre une valeur donnée
     * On utilise le type de mouvement RESET, il s'agit d'un mouvement distant dont nous n'avons pas le détail 
     * Typiquement utilisé pour faire remonter le niveau de stock d'un entrepot distant sans avoir à se préocuper
     * des mouvements intermédiaires
     * On ne valorise d'ailleurs pas ce mouvement et lui attribue une référence en dur
     */
    @Override
    public boolean resetStock(String locationId, String productId, Double newStock) {
        StockCurrent stockCurrent =  findByProductAndLocationWithCache(productId, locationId,true);

        if (stockCurrent != null) {
            newStock -= stockCurrent.getUnits();
        }
        if(!newStock.isNaN() && newStock != 0D) {
            Object resp = generateMovement(locationId, productId, newStock , 0D /*price ?*/ , StockDiary.Type.REASON_RESET , null , "RESET-SIEGE", null , null, true);
            if(resp.equals(false)) {
                newStock = 0D;
            }
        }
        return newStock == 0D;
    }

    @Override 
    public <T> T getStockLevelAdapter(StockLevel stockLevel , Class<T> selector) {
        return getStockLevelAdapter(stockLevel , selector, false);
    }

    @Override 
    public <T> T getStockLevelAdapter(StockLevel stockLevel , Class<T> selector , boolean filtered) {
        T retour = stockLevel == null ? null : stockLevel.getAdapter(selector , filtered);
        if(stockLevel!= null && retour == null) {

        }

        return retour;
    }

    @Override 
    public <T> List<T> getStockDiaryListAdapter(List<StockDiary> stockDiaries, Class<T> selector ) {
        List<T> listDTO = new ArrayList<>();
        for (StockDiary stockDiary : stockDiaries) {
            listDTO.add(getStockDiaryAdapter(stockDiary , selector, false));
        }
        return listDTO;
    }

    @Override 
    public <T> T getStockDiaryAdapter(StockDiary stockDiary , Class<T> selector) {
        return getStockDiaryAdapter(stockDiary , selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override 
    public <T> T getStockDiaryAdapter(StockDiary stockDiary , Class<T> selector , boolean filtered) {
        T retour = stockDiary == null ? null : stockDiary.getAdapter(selector , filtered);
        if(stockDiary != null && retour == null) {
            if (selector.equals(StockMovementDetailDTO.class)) {
                retour = (T) getStockMovementDetailDTO(stockDiary);
            } else if (selector.equals(StockDiaryIndexDTO.class)) {
                retour = (T) getStockDiaryIndexDTO(stockDiary);
            } else if (selector.equals(StockMovementHeaderDTO.class)) {
                retour = (T) getStockMovementHeaderDTO(stockDiary);
            } 
        }

        return retour;
    }

    @Override 
    public <T> T getStockCurrentAdapter(StockCurrent stockCurrent , Class<T> selector) {
        return getStockCurrentAdapter(stockCurrent , selector, false);
    }

    @Override 
    public <T> T getStockCurrentAdapter(StockCurrent stockCurrent , Class<T> selector , boolean filtered) {
        T retour = stockCurrent == null ? null :  stockCurrent.getAdapter(selector , filtered);
        if(stockCurrent != null &&retour == null) {

        }

        return retour;

    }

    public StockMovementDetailDTO getStockMovementDetailDTO(StockDiary stockDiary) {
        StockMovementDetailDTO dto = new StockMovementDetailDTO();
        dto.setDate(stockDiary.getDateNew());
        dto.setLocation(stockDiary.getLocations().getAdapter(LocationsInfoDTO.class));
        dto.setProductId(stockDiary.getProductsId());
        dto.setProductCode(stockDiary.getProducts().getCode());
        dto.setProductLabel(stockDiary.getProducts().getName());
        dto.setReason(stockDiary.getReason());
        dto.setReasonLabel(stockDiary.getReasonLabel());
        dto.setReference(stockDiary.getReference());
        dto.setUnits(stockDiary.getUnits());
        dto.setPrice(stockDiary.getPrice());
        dto.setNote(stockDiary.getNote());
        dto.setPreviousStock(stockDiary.getStartingStock());
        if(stockDiary.getDestination() != null) {
            dto.setDestination(stockDiary.getDestination().getAdapter(LocationsInfoDTO.class));
        }

        if (stockDiary.getReference() != null) {
            Tickets tickets = ticketsDAO.findById(stockDiary.getReference()).orElse(null);
            if (tickets != null) {
                dto.setReferenceType(StockDiary.ReferenceType.TICKET);
            } else {
                dto.setReferenceType(StockDiary.ReferenceType.MOVEMENT);
            }
        } else {
            dto.setReferenceType(StockDiary.ReferenceType.UNKNOW);
        }
        return dto;
    }

    public StockMovementHeaderDTO getStockMovementHeaderDTO(StockDiary stockDiary) {
        StockMovementHeaderDTO dto = new StockMovementHeaderDTO();
        dto.setDate(stockDiary.getDateNew());
        dto.setLocation(stockDiary.getLocations().getAdapter(LocationsInfoDTO.class));
        dto.setReason(stockDiary.getReason());
        dto.setReasonLabel(stockDiary.getReasonLabel());
        dto.setReference(stockDiary.getReference());
        dto.setNote(stockDiary.getNote());
        if(stockDiary.getDestination() != null) {
            dto.setDestination(stockDiary.getDestination().getAdapter(LocationsInfoDTO.class));
        }

        if (stockDiary.getReference() != null) {
            Tickets tickets = ticketsDAO.findById(stockDiary.getReference()).orElse(null);
            if (tickets != null) {
                dto.setReferenceType(StockDiary.ReferenceType.TICKET);
            } else {
                dto.setReferenceType(StockDiary.ReferenceType.MOVEMENT);
            }
        } else {
            dto.setReferenceType(StockDiary.ReferenceType.UNKNOW);
        }
        return dto;
    }

    public StockDiaryIndexDTO getStockDiaryIndexDTO(StockDiary stockDiary) {
        StockDiaryIndexDTO dto = new StockDiaryIndexDTO();
        dto.setQuantiteAvant(stockDiary.getStartingStock());
        dto.setId(stockDiary.getId());
        dto.setDate(stockDiary.getDateNew());
        dto.setLieuStockage(stockDiary.getLocations().getAdapter(LocationsInfoDTO.class));
        dto.setProduit(stockDiary.getProducts().getAdapter(ProductsIndexDTO.class));
        dto.setCodeMouvement(stockDiary.getReason());
        dto.setLibelleMouvement(stockDiary.getReasonLabel());
        dto.setNumPiece(stockDiary.getReference());
        dto.setQuantiteMouvement(stockDiary.getUnits());
        dto.setPrixRevient(stockDiary.getPrice());
        dto.setNote(stockDiary.getNote());
        if(stockDiary.getDestination() != null) {
            dto.setDestination(stockDiary.getDestination().getAdapter(LocationsInfoDTO.class));
        }

        if (stockDiary.getReference() != null) {
            Tickets tickets = ticketsDAO.findById(stockDiary.getReference()).orElse(null);
            if (tickets != null) {
                dto.setTypePiece(StockDiary.ReferenceType.TICKET);
            } else {
                dto.setTypePiece(StockDiary.ReferenceType.MOVEMENT);
            }
        } else {
            dto.setTypePiece(StockDiary.ReferenceType.UNKNOW);
        }
        return dto;
    }

    @Override
    public boolean loadStockCurrentFromDTO(StockCurrent stockCurrent , Object source ) {
        boolean retour = false;
        if (StockCurrentDTO.class.isInstance(source)) {
            StockCurrentDTO dto = (StockCurrentDTO) source;
            if ((dto.getLocationsId() != null && !dto.getLocationsId().equals(stockCurrent.getLocationsId()))
                    || (dto.getLocationsId() == null && stockCurrent.getLocationsId() != null)) {
                retour = true;
                stockCurrent.setLocations(locationsDAO.findById(dto.getLocationsId()).orElse(null));
            }
            if ((dto.getProductsId() != null && !dto.getProductsId().equals(stockCurrent.getProductsId()))
                    || (dto.getProductsId() == null && stockCurrent.getProductsId() != null)) {
                retour = true;
                stockCurrent.setProducts(productsDAO.findById(dto.getProductsId()).orElse(null));
            }
            if ((dto.getUnits() != stockCurrent.getUnits())) {
                retour = true;
                stockCurrent.setUnits(dto.getUnits());
            }
            if ((dto.getAttributeSetInstanceId() != null && !dto.getAttributeSetInstanceId().equals(
                    stockCurrent.getAttributeSetInstanceId()))
                    || (dto.getAttributeSetInstanceId() == null && stockCurrent.getAttributeSetInstanceId() != null)) {
                retour = true;
                stockCurrent.setAttributeSetInstanceId(dto.getAttributeSetInstanceId());
            }
        }

        return retour;
    }

    @Override
    public boolean loadStockDiaryFromDTO(StockDiary stockDiary ,Object source) {
        boolean retour = false;

        if (StockDiaryDTO.class.isInstance(source)) {
            StockDiaryDTO dto = (StockDiaryDTO) source;
            if ((dto.getAttributeSetInstanceId() != null && !dto.getAttributeSetInstanceId().equals(stockDiary.getAttributeSetInstanceId()))
                    || (dto.getAttributeSetInstanceId() == null && stockDiary.getAttributeSetInstanceId() != null)) {
                retour = true;
                stockDiary.setAttributeSetInstanceId(dto.getAttributeSetInstanceId());
            }
            if ((dto.getDateNew() != null && !dto.getDateNew().equals(stockDiary.getDateNew()))
                    || (dto.getDateNew() == null && stockDiary.getDateNew() != null)) {
                retour = true;
                stockDiary.setDateNew(dto.getDateNew());
            }
            if ((dto.getId() != null && !dto.getId().equals(stockDiary.getId())) || (dto.getId() == null && stockDiary.getId() != null)) {
                retour = true;
                stockDiary.setId(dto.getId());
            }
            if ((dto.getLocationsId() != null && !dto.getLocationsId().equals(stockDiary.getLocationsId()))
                    || (dto.getLocationsId() == null && stockDiary.getLocationsId() != null)) {
                retour = true;
                stockDiary.setLocations(locationsDAO.findById(dto.getLocationsId()).orElse(null));
            }
            if ((dto.getNote() != null && !dto.getNote().equals(stockDiary.getNote()))
                    || (dto.getNote() == null && stockDiary.getNote() != null)) {
                retour = true;
                stockDiary.setNote(dto.getNote());
            }
            if ((dto.getStockBefore() != null && !dto.getStockBefore().equals(stockDiary.getStartingStock()))) {
                retour = true;
                stockDiary.setStartingStock(dto.getStockBefore());
            }
            if ((dto.getDestination() != null && !dto.getDestination().equals(stockDiary.getDestination() != null ? stockDiary.getDestination() : null ))
                    || (dto.getDestination() == null && stockDiary.getDestination() != null)) {
                retour = true;
                stockDiary.setDestination(dto.getDestination() != null ? locationsDAO.findById(dto.getDestination()).orElse(null) : null );
            }
            if ((dto.getReference() != null && !dto.getReference().equals(stockDiary.getReference()))
                    || (dto.getReference() == null && stockDiary.getReference() != null)) {
                retour = true;
                stockDiary.setReference(dto.getReference());
            }
            if ((dto.getState() != null && !dto.getState().equals(stockDiary.getState()))
                    || (dto.getState() == null && stockDiary.getState() != null)) {
                retour = true;
                stockDiary.setState(dto.getState());
            }
            if ((dto.getProductsId() != null && !dto.getProductsId().equals(stockDiary.getProductsId()))
                    || (dto.getProductsId() == null && stockDiary.getProductsId() != null)) {
                retour = true;
                stockDiary.setProducts(productsDAO.findById(dto.getProductsId()).orElse(null));
            }
            if (dto.getPrice() != stockDiary.getPrice()) {
                retour = true;
                stockDiary.setPrice(dto.getPrice());
            }
            if (dto.getReason() != stockDiary.getReason()) {
                retour = true;
                stockDiary.setReason(dto.getReason());
            }
            if (dto.getUnits() != stockDiary.getUnits()) {
                retour = true;
                stockDiary.setUnits(dto.getUnits());
            }
        }
        return retour;
    }

    public boolean loadStockLevelFromDTO(StockLevel stockLevel , Object source) {
        boolean retour = false;

        if (StockLevelDTO.class.isInstance(source)) {
            StockLevelDTO dto = (StockLevelDTO) source;
            if ((dto.getId() != null && !dto.getId().equals(stockLevel.getId())) || (dto.getId() == null && stockLevel.getId() != null)) {
                retour = true;
                stockLevel.setId(dto.getId());
            }
            if ((dto.getLocationsId() != null && !dto.getLocationsId().equals(stockLevel.getLocationsId()))
                    || (dto.getLocationsId() == null && stockLevel.getLocationsId() != null)) {
                retour = true;
                stockLevel.setLocations(locationsDAO.findById(dto.getLocationsId()).orElse(null));
            }
            if ((dto.getProductsId() != null && !dto.getProductsId().equals(stockLevel.getProductsId()))
                    || (dto.getProductsId() == null && stockLevel.getProductsId() != null)) {
                retour = true;
                stockLevel.setProducts(productsDAO.findById(dto.getProductsId()).orElse(null));
            }
            if (dto.getStockMaximum() != stockLevel.getStockMaximum()) {
                retour = true;
                stockLevel.setStockMaximum(dto.getStockMaximum());
            }
            if (dto.getStockSecurity() != stockLevel.getStockSecurity()) {
                retour = true;
                stockLevel.setStockSecurity(dto.getStockSecurity());
            }
        }
        return retour;
    }

    @Override
    public boolean createOrderStockMoves(List<PaletteDetailDTO> paletteDetails , OrderDTO currentOrder , PaletteDTO currentPalette) {
        OrderDTO order = ordersService.getOrder(currentOrder.getId()) ;
        Locations currentTransit = null;
        Locations from = null;
        Locations to = null;
        boolean retour = false;
        if( order == null || ! Orders.Status.SRECEPTED.equals(order.getStatus())) {
            retour = true;
            for(PaletteDetailDTO detail : paletteDetails) {

                if(Integer.parseInt(detail.getStatus()) > Integer.parseInt(Orders.Status.CONTROLE) ) {
                    //Mouvement Départ -> TRANSIT
                    if(currentTransit == null) {
                        currentTransit = locationsDAO.findById(currentOrder.getLocationToId() + "-TRANSIT").orElse(null);
                    }
                    if(from == null) {
                        from = locationsDAO.findById(currentOrder.getLocationFromId()).orElse(null);
                    }
                    createStockMove(from , currentTransit , detail.getProductCode() , currentPalette.getName() , currentPalette.getDelivDate() , detail.getQuantitySent() , 0D , StockDiary.Type.REASON_OUT_MOVEMENT , null);
                    createStockMove(currentTransit , from , detail.getProductCode() , currentPalette.getName() , currentPalette.getDelivDate() , detail.getQuantitySent() , 0D , StockDiary.Type.REASON_IN_MOVEMENT , null);
                }
                if(Orders.Status.RECEPTED.equals(detail.getStatus())) {
                    //Mouvement TRANSIT -> destination 
                    if(to == null) {
                        to = locationsDAO.findById(currentOrder.getLocationToId()).orElse(null);
                    }
                    createStockMove(currentTransit , to , detail.getProductCode() , currentPalette.getName() , currentPalette.getReceptDate() , detail.getQuantityReceived() , 0D , StockDiary.Type.REASON_OUT_TRANSFERT , null);
                    createStockMove(to , currentTransit , detail.getProductCode() , currentPalette.getName() , currentPalette.getReceptDate() , detail.getQuantityReceived() , 0D , StockDiary.Type.REASON_IN_TRANSFERT , null);
                }
            }
            adjustMovements();
        }

        return retour;
    }

    @Override 
    public void createStockMove(Locations orig , Locations dest , String productId , String ref , Date dateMvt ,  double units, double price, int reason, String note) {
        //on teste si il y a un mouvement source -> dest pour ce produit avec la ref donnée
        //Si la réponse est non 
        //On crée le mouvement 
        StockDiarySpecification specs = new StockDiarySpecification();
        specs.add(new SearchCriteria(StockDiary.Fields.PRODUCT ,  productId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockDiary.Fields.LOCATION ,  orig.getId() , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockDiary.Fields.REFERENCE ,  ref , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(StockDiary.Fields.DESTINATION ,  dest.getId() , SearchOperation.EQUAL));
        if(dateMvt != null) {
            specs.add(new SearchCriteria(StockDiary.Fields.DATE_NEW ,  DateHelper.formatSQLTimeStamp(dateMvt) , SearchOperation.EQUAL));
        }
        List<StockDiary> list = stockDiaryDAO.findAll(specs );
        if(list.size() == 0) {
            generateMovement(orig.getId(), productId,  units,  price,  reason,  note,
                    ref, dest.getId(), dateMvt , false);
        }
    }

    @Override
    public void adjustMovements() {
        for (Iterator<Map.Entry<String,StockDiary>> entries = updMove.entrySet().iterator(); entries.hasNext(); ) {
            Map.Entry<String,StockDiary> entry = entries.next();
            StockDiary mvt = entry.getValue();
            entries.remove();
            mvt.setStartingStock(mvt.getStartingStock() + updMoveQty.get(mvt.getId()));
            updMoveQty.remove(mvt.getId());
            stockDiaryDAO.save(mvt);
        }

    }


}

