package com.ose.backend.service.sales.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.dto.tickets.DetailsPaymentsDTO;
import org.pasteque.api.dto.tickets.PaymentsInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.tickets.IPaymentsDAO;
import com.ose.backend.dao.tickets.ITicketsDAO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.sales.SalesDetailsDTO;
import com.ose.backend.dto.sales.SalesDetailsOverViewDTO;
import com.ose.backend.dto.sales.TopXDTO;
import com.ose.backend.service.sales.ISalesService;
import com.ose.backend.specs.tickets.TicketsSpecifications;

@Service
public class SalesService implements ISalesService {

    @Autowired
    private ITicketsDAO ticketDAO;
    @Autowired
    private IPaymentsDAO paymentsDAO;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    
    public static final String DATE_DIMENSION = "date";
    public static final String ZIPCODE_DIMENSION = "zipcode";
    public static final String DEBALLAGE_DIMENSION = "deballage";
    //TODO implémenter - dans le get SalesDetails (pour l'accumulation) et Dans l'overview
    public static final String WEEK_DIMENSION = "W";
    public static final String MONTH_DIMENSION = "M";

    @Override
    public List<SalesDetailsDTO> getSalesDetails(String locationId, String dimension, String from, String to) {

        List<Tickets> tickets = getSalesDetailsTickets(locationId, dimension, from, to);
        List<SalesDetailsDTO> dtos = new ArrayList<>();

        if (!tickets.isEmpty()) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            String dimensionValue = null;
            String dimensionComparison = null;
            boolean first = true;
            SalesDetailsDTO dto = new SalesDetailsDTO();
            for (Tickets ticket : tickets) {
                if (first) {
                    if (dimension.equals(SalesService.DATE_DIMENSION)) {
                        dimensionValue = format.format(ticket.getReceipts().getDateNew());
                    }
                    if (dimension.equals(SalesService.ZIPCODE_DIMENSION)) {
                        Insee insee = ticket.getCustomerZipCode();
                        dimensionValue = insee == null ? "UNKNOW" : insee.toString();
                    }
                    if (dimension.equals(SalesService.DEBALLAGE_DIMENSION)) {
                        dimensionValue = format.format(ticket.getReceipts().getDateNew());
                        if(ticket.getReceipts().getClosedCash().getSalesLocation() != null) {
                            dimensionValue = dimensionValue.concat(" ").concat(ticket.getReceipts().getClosedCash().getSalesLocation().getCity());
                        }
                    }
                    dto.setDimension(dimensionValue);
                    dto.setSalesCount(1);
                    dto.setTurnover(ticket.getFinalTaxedPrice());
                    first = false;
                } else {
                    if (dimension.equals(SalesService.DATE_DIMENSION)) {
                        dimensionComparison = format.format(ticket.getReceipts().getDateNew());
                    } 
                    if (dimension.equals(SalesService.ZIPCODE_DIMENSION)) {
                        Insee insee = ticket.getCustomerZipCode();
                        dimensionComparison = insee == null ? "UNKNOW" : insee.toString();
                    }
                    if (dimension.equals(SalesService.DEBALLAGE_DIMENSION)) {
                        dimensionComparison = format.format(ticket.getReceipts().getDateNew());
                        if(ticket.getReceipts().getClosedCash().getSalesLocation() != null) {
                            dimensionComparison = dimensionComparison.concat(" ").concat(ticket.getReceipts().getClosedCash().getSalesLocation().getCity());
                        }
                        
                    }

                    if (dimensionValue.equals(dimensionComparison)) {
                        dto.setSalesCount(dto.getSalesCount() + 1);
                        dto.setTurnover(dto.getTurnover() + ticket.getFinalTaxedPrice());
                    } else {
                        dto.setAverageBasket(dto.getTurnover() / dto.getSalesCount());
                        dtos.add(dto);

                        dimensionValue = dimensionComparison;
                        dto = new SalesDetailsDTO();
                        dto.setDimension(dimensionComparison);
                        dto.setSalesCount(1);
                        dto.setTurnover(ticket.getFinalTaxedPrice());
                    }
                }
            }
            dto.setAverageBasket(dto.getTurnover() / dto.getSalesCount());
            dtos.add(dto);

            if (dimension.equals(SalesService.ZIPCODE_DIMENSION)) {
                Comparator<SalesDetailsDTO> comparator = new Comparator<SalesDetailsDTO>() {
                    @Override
                    public int compare(SalesDetailsDTO o1, SalesDetailsDTO o2) {
                        if (o1.getTurnover() == o2.getTurnover()) {
                            return 0;
                        }
                        if (o1.getTurnover() > o2.getTurnover()) {
                            return -1;
                        }
                        return 1;
                    }
                };

                dtos = dtos.stream().sorted(comparator).collect(Collectors.toList());
                
                if (dtos.size() >= 5) {
                    //On ajoute un accumulateur pour représenter toutes les autres localisations
                    SalesDetailsDTO accu = new SalesDetailsDTO();
                    accu.setTurnover(0);
                    accu.setSalesCount(0);
                    accu.setDimension("OTHERS");
                    accu.setAverageBasket(0);
                    for(int i = 5 ; i<dtos.size() ; i++) {
                        accu.setTurnover(accu.getTurnover()+dtos.get(i).getTurnover());
                        accu.setSalesCount(accu.getSalesCount()+dtos.get(i).getSalesCount());
                        if(accu.getSalesCount() != 0) {
                            accu.setAverageBasket(accu.getTurnover()/accu.getSalesCount());
                        }
                    }
                    dtos.add(5, accu);
                    return dtos.subList(0, 6);
                }
            }
        }
        return dtos;
    }

    @Override
    public List<TopXDTO> getTopX(String locationId, String tourId, String pricelistId, int x, List<ProductsResultDTO> productsRestriction, List<POSCategoriesDTO> categoriesRestriction, String dimension, String from,
            String to) {

        List<String> prodRestrictions = new ArrayList<>();
        for (ProductsResultDTO dto : productsRestriction) {
            prodRestrictions.add(dto.getId());
        }
        
        List<String> catRestrictions = new ArrayList<>();
        for (POSCategoriesDTO dto : categoriesRestriction) {
            catRestrictions.add(dto.getId());
        }
        
        List<Object[]> results = ticketDAO.getTopX(locationId, tourId , pricelistId , x, prodRestrictions, catRestrictions ,dimension, from, to);

        List<TopXDTO> dtos = new ArrayList<>();
        for (Object[] objects : results) {
            TopXDTO dto = new TopXDTO();
            dto.setProductCode((String) objects[0]);
            dto.setProductLabel((String) objects[1]);
            dto.setUnits((double) objects[2]);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<? extends ICsvResponse> getPaymentsInfo(String locationId, String from, String to, String saleLocationId, String tourId) {
        List<Object[]> results = paymentsDAO.getPaymentsInfo(locationId, from, to, saleLocationId, tourId);

        List<PaymentsInfoDTO> dtos = new ArrayList<>();
        for (Object[] objects : results) {
            PaymentsInfoDTO dto = new PaymentsInfoDTO();
            dto.setPayment((String) objects[0]);
            dto.setPaymentLabel(Payments.getPaymentsLabel((String) objects[0]));
            dto.setPaymentCateg(Payments.getPaymentsCateg((String) objects[0]));
            dto.setAmount((double) objects[1]);
            dto.setNumber((long) objects[2]);
            dtos.add(dto);
        }

        return dtos;
    }

    @Override
    public List<? extends ICsvResponse> getDetailsPayment(String locationId, String payment, String from, String to, String saleLocationId, String tourId) {
        return AdaptableHelper.getAdapter(paymentsDAO.getDetailsPayment(locationId, payment, from, to, saleLocationId, tourId), DetailsPaymentsDTO.class);
    }

    @Override
    public Object getSalesDetailsOverView(String locationId, String dimension, String from, String to) {
        Calendar calendarFrom = new GregorianCalendar() ;
        Calendar calendarTo = new GregorianCalendar() ;
        Locations locations = locationsDAO.findById(locationId).orElse(null) ;
        if(from != null) {
            calendarFrom.setTimeInMillis(Long.parseLong(from));
        } else {
            calendarFrom.set(Calendar.HOUR_OF_DAY , 0);
            calendarFrom.set(Calendar.MINUTE , 0);
            calendarFrom.set(Calendar.SECOND , 0);
            calendarFrom.set(Calendar.MILLISECOND , 0);
        }
        if(to != null) {
            calendarTo.setTimeInMillis(Long.parseLong(to));
        } else {
            calendarTo.set(Calendar.HOUR_OF_DAY , 23);
            calendarTo.set(Calendar.MINUTE , 59);
            calendarTo.set(Calendar.SECOND , 59);
            calendarTo.set(Calendar.MILLISECOND , 999);
        }
        List<SalesDetailsDTO> listN0 = getSalesDetails(locationId , dimension, String.valueOf(calendarFrom.getTimeInMillis()) , String.valueOf(calendarTo.getTimeInMillis())) ;
        switch(dimension) {
        case SalesService.MONTH_DIMENSION :
            calendarFrom.add(Calendar.MONTH, -1);
            calendarTo.add(Calendar.MONTH, -1);
            break;
        case SalesService.WEEK_DIMENSION :
            calendarFrom.add(Calendar.WEEK_OF_YEAR, -1);
            calendarTo.add(Calendar.WEEK_OF_YEAR, -1);
            break;
        default :
            calendarFrom.add(Calendar.DAY_OF_MONTH, -1);
            calendarTo.add(Calendar.DAY_OF_MONTH, -1);
        }
        List<SalesDetailsDTO> listN1 = getSalesDetails(locationId, dimension, String.valueOf(calendarFrom.getTimeInMillis()), String.valueOf(calendarTo.getTimeInMillis())) ;
        
         
        return new SalesDetailsOverViewDTO(locationId, locations==null ? "" : locations.getName() , dimension , listN0 , listN1);
    }
    
    @Override
    public List<Tickets> getSalesDetailsTickets(String locationId, String dimension, String from, String to) {
        TicketsSpecifications spec = new TicketsSpecifications();

        if (locationId != null) {
            spec.add(new SearchCriteria(TicketsSpecifications.SearchCritria.LOCATIONID , locationId , SearchOperation.EQUAL));
        }
        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            spec.add(new SearchCriteria(TicketsSpecifications.SearchCritria.FROM , DateHelper.formatSQLTimeStamp(dFrom) , SearchOperation.GREATER_THAN_EQUAL));
        }
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            spec.add(new SearchCriteria(TicketsSpecifications.SearchCritria.FROM , DateHelper.formatSQLTimeStamp(dTo) , SearchOperation.LESS_THAN_EQUAL));
        }

        Sort sort = null;
        if (dimension.equals(SalesService.DATE_DIMENSION)) {
            sort = Sort.by(Tickets.Fields.DATE).ascending();
        }
        if (dimension.equals(SalesService.ZIPCODE_DIMENSION)) {
            sort = Sort.by(Tickets.Fields.CUSTOMER_ZIPCODE).ascending();
        }
        if (dimension.equals(SalesService.DEBALLAGE_DIMENSION)) {
            sort = Sort.by(Tickets.Fields.DATE).descending();
        }

        return ticketDAO.findAll(spec, sort);
    }

    
}
