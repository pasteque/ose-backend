package com.ose.backend.service.sales;

import java.util.List;

import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.response.ICsvResponse;

import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.sales.SalesDetailsDTO;
import com.ose.backend.dto.sales.TopXDTO;


public interface ISalesService {

    List<SalesDetailsDTO> getSalesDetails(String locationId, String dimension, String from, String to);

    List<TopXDTO> getTopX(String locationId, String tourId, String pricelistId, int x, List<ProductsResultDTO> productsRestriction, List<POSCategoriesDTO> categoriesRestriction, String dimension, String from, String to);

    List<? extends ICsvResponse> getPaymentsInfo(String locationId, String from, String to, String saleLocationId, String tourId);

    List<? extends ICsvResponse> getDetailsPayment(String locationId, String payment, String from, String to, String saleLocationId, String tourId);

    Object getSalesDetailsOverView(String locationId, String dimension, String from, String to);

    List<Tickets> getSalesDetailsTickets(String locationId, String dimension,
            String from, String to);

}
