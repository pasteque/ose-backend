package com.ose.backend.service.customers.impl;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.customers.ICustomersDAO;
import com.ose.backend.service.customers.ICustomersServiceOSE;

@Service
public class CustomersServiceOSE implements ICustomersServiceOSE {

    @Autowired
    private ICustomersDAO customersDAO;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    
    @Override
    public Object getNextCode() {
        Locations locations = locationsDAO.findTopByServerDefaultTrue();
        long count = customersDAO.count();
        String retour = String.format("%012d", count);
        if(locations != null ) {
            String code = locations.getId();
            retour = customersDAO.findNextId(code);
            if(retour == null) {
                retour = code + String.format("%08d", count);
            }
        } 

        return retour;
    }


}
