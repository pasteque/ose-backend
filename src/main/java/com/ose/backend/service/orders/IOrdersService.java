package com.ose.backend.service.orders;

import java.util.List;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.MultiValueMap;

import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.dto.orders.OrderDetailDTO;
import com.ose.backend.dto.orders.OrderDetailIndexDTO;
import com.ose.backend.dto.orders.OrderDetailOfbizDTO;
import com.ose.backend.dto.orders.OrderListDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.orders.PaletteInfoDTO;
import com.ose.backend.dto.orders.PalettePreparationInfoDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.model.orders.Palettes;

public interface IOrdersService {

    List<OrderListDTO> getAllByCriteria(MultiValueMap<String, Object> parameters);

    List<PalettePreparationInfoDTO> getPalettePreparationInfo(String orderId);
    
    OrderDTO initializeOrders(String name, String locationId);

    OrderDTO generateOrder(String orderId);

    OrderDTO saveOrder(OrderDTO dto);

    OrderDetailDTO saveOrderDetail(OrderDetailDTO dto);

    void saveOrderDetails(List<OrderDetailDTO> dtos);

    OrderDTO getOrder(String orderId);

    List<OrderDetailIndexDTO> getOrderDetails(String orderId);

    Object canModifyOrder(UsernamePasswordAuthenticationToken token, String orderId);

    Object canPassOrderToNextStep(UsernamePasswordAuthenticationToken token, String orderId);

    Object nextOrderStep(String name, String orderId);

    Object getProductsLocations(String orderId);

    PaletteDTO generatePalette(String name, String orderId, String locationFrom, String numberOfLocation);

    Object savePalette(PaletteDTO dto);

    Object getPalette(String paletteId);

    List<PaletteInfoDTO> getPalettes(String orderId);

    Object addToPalette(String paletteId, String orderDetailId);

    Object getPalettesByCriteria(String name, String status, String statusPalette, String orderId);

    Object updatePaletteDetail(String paletteId, String status, String productId, Double quantity, boolean closePaletteDetail, String userName,String inputOrigine);

    Object nextPaletteStep(String name, String paletteId, String status);

    Object deleteOrder(String orderId);

    Object canAddToPalette(String paletteId, String status, String productId, double quantity);

    Object addProductToOrder(String orderId, String productId, int quantity);

    Object removeProductToOrder(String orderId, String productId);

    Object deletePalette(String paletteId);

    Object canAddToOrder(String orderId, String productId, double quantity);

    Object fillPaletteQuantity(String paletteId);

    Object savePaletteDetail(PaletteDetailDTO paletteDetailDto);

    Object deleteFromPalette(String paletteId, String orderDetailId);

    Object getProductsPaletteInfo(String orderId, String productId);

    void savePaletteDetails(List<PaletteDetailDTO> dtos);

    Object receivePalettes(String user, String orderId);

    List<ProductsResultDTO> getProductsByEmplacement(String paletteId, String emplacement);

    boolean loadFromDTO(Object source, Orders orders);

    List<OrderDetailOfbizDTO> getOrderDetailOfbizDTO(String id);
    
    List<OrderDetailBLDTO> getOrderDetailBLDTO(String id);

    OrderDTO getOrderDTO(Orders orders);
    
    public boolean loadPaletteFromDTO(Palettes palettes , Object source);

    Object canDeleteOrder(UsernamePasswordAuthenticationToken principal,
            String orderId);

    Object resyncJob(String orderId);

}
