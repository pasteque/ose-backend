package com.ose.backend.service.orders.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.security.IPeopleApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;
import org.pasteque.api.model.security.Permissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ose.backend.dao.application.IApplicationDAO;
import com.ose.backend.dao.helper.CriteriaConstant;
import com.ose.backend.dao.helper.CriteriaHelper;
import com.ose.backend.dao.orders.IOrdersDAO;
import com.ose.backend.dao.orders.IOrdersDetailsDAO;
import com.ose.backend.dao.orders.IPalettesDAO;
import com.ose.backend.dao.orders.IPalettesDetailsDAO;
import com.ose.backend.dao.stocks.IStockCurrentDAO;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailBLDTO;
import com.ose.backend.dto.orders.OrderDetailDTO;
import com.ose.backend.dto.orders.OrderDetailIndexDTO;
import com.ose.backend.dto.orders.OrderDetailOfbizDTO;
import com.ose.backend.dto.orders.OrderListDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.orders.PaletteInfoDTO;
import com.ose.backend.dto.orders.PalettePreparationInfoDTO;
import com.ose.backend.dto.orders.PaletteStatusInfoDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockProductDetailDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.model.orders.OrdersDetails;
import com.ose.backend.model.orders.Palettes;
import com.ose.backend.model.orders.PalettesDetails;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.orders.IOrdersService;
import com.ose.backend.service.products.IProductsServiceOSE;
import com.ose.backend.service.stock.IStocksServiceOse;

@Service
public class OrdersService implements IOrdersService {

    @Autowired
    private IOrdersDAO ordersDAO;
    @Autowired
    private IOrdersDetailsDAO ordersDetailsDAO;
    @Autowired
    private IPeopleApiDAO peopleDAO;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    @Autowired
    private IProductsApiDAO productsDAO;
    @Autowired
    private IPalettesDAO palettesDAO;
    @Autowired
    private IPalettesDetailsDAO palettesDetailsDAO;
    @Autowired
    private IStocksServiceOse stocksService;
    @Autowired
    IStockCurrentDAO stockCurrentDAO;
    @Autowired
    private IJobsService jobService;
    @Autowired
    private IProductsServiceOSE productsService;
    @Autowired
    private IApplicationDAO applicationDAO;


    @Override
    public OrderDTO getOrder(String id) {
//        Optional<Orders> order = ordersDAO.findById(id);
//        return order.isPresent() ?   getOrderDTO(order.get()) : null;
          OrderDTO dto = ordersDAO.getOrderDTONativeQuery(id);
          fillOrderDTO(dto);
          return dto;
    };

    @Override
    public List<OrderListDTO> getAllByCriteria(MultiValueMap<String, Object> parameters) {
        CriteriaHelper criteriaHelper = new CriteriaHelper(parameters);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'Z");
        if (criteriaHelper.containsKey(CriteriaConstant.FROM)) {
            Long from = criteriaHelper.getFirstFromKeyAsLong(CriteriaConstant.FROM);
            Date dfrom = from != null ? new Date(from) : null;
            criteriaHelper.putCriteria(CriteriaConstant.FROM, sdf.format(dfrom));
        }
        if (criteriaHelper.containsKey(CriteriaConstant.TO)) {
            Long from = criteriaHelper.getFirstFromKeyAsLong(CriteriaConstant.TO);
            Date dfrom = from != null ? new Date(from) : null;
            criteriaHelper.putCriteria(CriteriaConstant.TO, sdf.format(dfrom));
        }
        return AdaptableHelper.getAdapter(ordersDAO.findAllByCriteria(parameters), OrderListDTO.class);
    }

    @Override
    public OrderDTO initializeOrders(String name, String locationId) {
        Orders orders = new Orders();
        initializeOrders(orders , name, locationId);
        return getOrderDTO(orders);
    }

    @Override
    public OrderDTO saveOrder(OrderDTO dto) {
        Orders order = new Orders();
        if (dto.getId() != null && !dto.getId().isEmpty()) {
            order = ordersDAO.findById(dto.getId()).orElse(ordersDAO.findFirstByName(dto.getReference()).orElse(new Orders()));
            if(Orders.Status.RECEPTED.equals(order.getStatus())) {
                return null;
            }
        }
        
        Locations locationFrom = locationsDAO.findById(dto.getLocationFrom().getId()).orElse(null);
        order.setLocationFrom(locationFrom);
        Locations locationTo = locationsDAO.findById(dto.getLocationTo().getId()).orElse(null);
        order.setLocationTo(locationTo);
        order.setCreationDate(dto.getCreationDate());
        order.setCreationUser(peopleDAO.findById(dto.getUsernameId()).orElse(null));

        if (dto.getPrepUserId() != null && !dto.getPrepUserId().isEmpty()) {
            order.setPreparationDate(dto.getPrepDate());
            order.setPreparationUser(peopleDAO.findById(dto.getPrepUserId()).orElse(null));
        }

        if (dto.getPlanifUserId() != null && !dto.getPlanifUserId().isEmpty()) {
            order.setPlanificationDate(dto.getPlanifDate());
            order.setPlanificationUser(peopleDAO.findById(dto.getPlanifUserId()).orElse(null));
        }

        if (dto.getControlUserId() != null && !dto.getControlUserId().isEmpty()) {
            order.setControleDate(dto.getControlDate());
            order.setControleUser(peopleDAO.findById(dto.getControlUserId()).orElse(null));
        }

        if (dto.getDelivUserId() != null && !dto.getDelivUserId().isEmpty()) {
            order.setExpeditionDate(dto.getDelivDate());
            order.setExpeditionUser(peopleDAO.findById(dto.getDelivUserId()).orElse(null));
        }

        if (dto.getReceptUserId() != null && !dto.getReceptUserId().isEmpty()) {
            order.setReceptionDate(dto.getReceptDate());
            order.setReceptionUser(peopleDAO.findById(dto.getReceptUserId()).orElse(null));
        }

        if (dto.getStatusOrder() > Integer.parseInt(order.getStatus())) {
            order.setStatus(String.valueOf(dto.getStatusOrder()));
        }

        order.setNote(dto.getNote());
        order.setName(dto.getReference());

        // Attention il s'agit d'un ajout pas d'une mise à jour
        for (OrderDetailDTO detailDTO : dto.getOrderDetails()) {
            addProductsToOrder(order , detailDTO.getId(), detailDTO.getProduct().getId(), detailDTO.getQuantityAsked(),
                    detailDTO.getQuantityToPrepare(), 0, 0);
        }

        if (order.getId() != null) {
            return getOrderDTO(ordersDAO.save(order));
        } else {
            if (dto.getId() != null && !dto.getId().isEmpty()) {
                order.setId(dto.getId());
            } else {
                order.createId();
            }
            return getOrderDTO(ordersDAO.save(order));
        }
    }

    @Override
    public OrderDTO generateOrder(String orderId) {
        Orders order = ordersDAO.findById(orderId).orElse(null);

        List<Products> products = stockCurrentDAO.findProductsToOrder(order.getLocationToId());

        order.setOrdersDetails(new ArrayList<OrdersDetails>());
        for (Products p : products) {
            boolean canAdd = true;
            List<PosMessages> msgList = p.getMessages();

            for (PosMessages msg : msgList) {
                if (msg.getType().getType().equals(PosMessageType.Values.ALERT) && msg.getContent().contains("ARRIVAGE") && (msg.getEndDate() == null)) {
                    canAdd = false;
                }
            }
            if (canAdd ) {
                StockCurrent qtySiege = stocksService.getStockProducts( Locations.SIEGE_ID, p.getId());
                if ( qtySiege!=null &&  qtySiege.getUnits()>0) {
                    OrdersDetails ordersDetails = new OrdersDetails();
                    ordersDetails.setOrder(order);
                    ordersDetails.setProduct(p);
                    double qty = stocksService.getQuantityToOrder(order.getLocationToId(),p.getId());
                    if (qty > 0) {
                        ordersDetails.createId();
                        ordersDetails.setQuantityAsked(qty);
                        order.getOrdersDetails().add(ordersDetails);
                    }
                }
            }
        }

        order = ordersDAO.save(order);
        return getOrderDTO(order);
    };

    @Override
    public void saveOrderDetails(List<OrderDetailDTO> dtos) {
        ArrayList<OrdersDetails> newOrdersDetails = new ArrayList<OrdersDetails>();
        ArrayList<OrdersDetails> updOrdersDetails = new ArrayList<OrdersDetails>();
        for (OrderDetailDTO dto : dtos) {
            OrdersDetails ordersDetails = ordersDetailsDAO.findById(dto.getId()).orElse(null);
            Boolean newOne = ordersDetails == null;
            if(newOne || ! Orders.Status.RECEPTED.equals(ordersDetails.getOrder().getStatus())) {
            ordersDetails = preparesaveOrderDetail(dto, ordersDetails);
                if (ordersDetails.getProduct() != null) {
                    if (newOne) {
                        newOrdersDetails.add(ordersDetails);
                    } else {
                        updOrdersDetails.add(ordersDetails);
                    }
                }
            }
        }
        ordersDetailsDAO.saveAll(newOrdersDetails);
        ordersDetailsDAO.saveAll(updOrdersDetails);
    }

    @Override
    public OrderDetailDTO saveOrderDetail(OrderDetailDTO dto) {

        OrderDetailDTO retour = null;
        OrdersDetails ordersDetails = ordersDetailsDAO.findById(dto.getId()).orElse(null);

        ordersDetails = preparesaveOrderDetail(dto, ordersDetails);

        if (ordersDetails.getProduct() != null) {
                retour = getOrderDetailDTO(ordersDetailsDAO.save(ordersDetails));
        }

        return retour;
    }

    public OrdersDetails preparesaveOrderDetail(OrderDetailDTO dto, OrdersDetails ordersDetails) {
        // OrderDetailDTO retour = null;
        // OrdersDetails ordersDetails =
        // ordersDetailsDAO.findById(dto.getId());

        Boolean newOne = ordersDetails == null;

        if (newOne) {
            ordersDetails = new OrdersDetails();
            ordersDetails.setProduct(productsDAO.findById(dto.getProductId()).orElse(null));
            ordersDetails.setOrder(ordersDAO.findById(dto.getOrderId()).orElse(null));
            // pas d'id null
            ordersDetails.setId(dto.getId());
        }

        // Modif EDU 01/2017 : Ajout des infos sur les quantités non initiales
        // Ajout de la possibilité de créer depuis ce point une nouvelle ligne

        ordersDetails.setQuantityAsked(dto.getQuantityAsked());
        ordersDetails.setQuantityToPrepare(dto.getQuantityToPrepare());
        ordersDetails.setQuantityPrepared(dto.getQuantityPrepared());
        ordersDetails.setQuantitySent(dto.getQuantitySent());
        ordersDetails.setQuantityReceived(dto.getQuantityReceived());

        return ordersDetails;
    }

    @Override
    public Object addProductToOrder(String orderId, String productId, int quantity) {
        Orders orders = ordersDAO.findById(orderId).orElse(null);
        int quantityAsked = 0, quantityToPrepare = 0, quantitySent = 0, quantityReceived = 0;
        if (orders.getStatus().equals(Orders.Status.CREATION)) {
            quantityAsked = quantity;
        } else if (orders.getStatus().equals(Orders.Status.PLANIFICATION) || orders.getStatus().equals(Orders.Status.PREPARATION)
                || orders.getStatus().equals(Orders.Status.CONTROLE)) {
            quantityToPrepare = quantity;
            // } else if (orders.getStatus().equals(Orders.Status.CONTROLE)) {
            // quantitySent = quantity;
        } else if (orders.getStatus().equals(Orders.Status.EXPEDITION)) {
            quantityReceived = quantity;
        }
        addProductsToOrder(orders , null, productId, quantityAsked, quantityToPrepare, quantitySent, quantityReceived);

        return getOrderDTO(ordersDAO.save(orders));
    }

    @Override
    public Object removeProductToOrder(String orderId, String productId) {
        Orders orders = ordersDAO.findById(orderId).orElse(null);
        OrdersDetails ordersDetails = orders.getOrdersManager().getOrdersDetails(productId);
        orders.getOrdersDetails().remove(ordersDetails);
        ordersDetails.setOrder(null);
        return getOrderDTO(ordersDAO.save(orders));
    };

    @Override
    public Object canModifyOrder(UsernamePasswordAuthenticationToken token, String orderId) {
        //EDU 2020 03 - On ajoute des critères pour la possibilité de modifier ou changer d'état
        //En particulier suivant le lieu où on est

        //Si on est dans le central on ne peut pas
        Locations locationServer = locationsDAO.findTopByServerDefaultTrue();
        Orders order = ordersDAO.findById(orderId).orElse(null);
        //        Boolean isServeurDestinataire = false ;
        Boolean isServeurFournisseur = false ;
        if (locationServer != null ) {
            //            isServeurDestinataire = locationServer.getId().equals(order.getLocationToId());
            isServeurFournisseur = locationServer.getId().equals(order.getLocationFromId());
            if (order.getStatus().equals(Orders.Status.CREATION)) {
                // AME : 26/11/2019 -> Semble plus cohérent avec la mise en place de la gestion des droits
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE));
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.PLANIFICATION)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_PLANIFICATION));
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.PREPARATION)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_PREPARATION));
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.CONTROLE)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_CONTROLE));
            }
        }
        return false;
    }

    /**
     * METHODE POUR PASSER LA COMMANDE A L'ETAT SUIVANT DEPUIS L'IHM DE LA
     * COMMANDE EN GROS CA CONDITIONNE LE BTN ETAT SUIVANT
     */
    @Override
    public Object canPassOrderToNextStep(UsernamePasswordAuthenticationToken token, String orderId) {
        //EDU 2020 03 - On ajoute des critères pour la possibilité de modifier ou changer d'état
        //En particulier suivant le lieu où on est

        //Si on est dans le central on ne peut pas
        Locations locationServer = locationsDAO.findTopByServerDefaultTrue();
        Orders order = ordersDAO.findById(orderId).orElse(null);
        Boolean isServeurDestinataire = false ;
        Boolean isServeurFournisseur = false ;
        if (locationServer != null ) {
            isServeurDestinataire = locationServer.getId().equals(order.getLocationToId());
            isServeurFournisseur = locationServer.getId().equals(order.getLocationFromId());

            if (order.getStatus().equals(Orders.Status.CREATION)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE));
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.PLANIFICATION)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_PLANIFICATION));
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.PREPARATION)) {
                for (Palettes palettes : order.getPalettes()) {
                    if (Integer.parseInt(palettes.getStatus()) < Integer.parseInt(Orders.Status.CONTROLE)) {
                        return false;
                    }
                }
                return true;
            } else if (isServeurFournisseur && order.getStatus().equals(Orders.Status.CONTROLE)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_CONTROLE));
            } else if (isServeurDestinataire && order.getStatus().equals(Orders.Status.EXPEDITION)) {
                return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_RECEPTION));
            }
        }
        return false;
    }

    /**
     * METHODE POUR PASSER LA COMMANDE A L'ETAT SUIVANT DEPUIS L'IHM DE LA
     * COMMANDE
     */
    @Override
    public Object nextOrderStep(String name, String orderId) {
        OrderDTO retour = null;
        Job job = null;
        Orders order = ordersDAO.findById(orderId).orElse(null);
        People user = peopleDAO.findByUsername(name);
        if (order.getStatus().equals(Orders.Status.CREATION)) {
            // order.setValidationDate(new Date());
            // order.setValidationUser(user);
            order.setPlanificationDate(new Date());
            order.setPlanificationUser(user);
            order.setStatus(Orders.Status.PLANIFICATION);
            for (OrdersDetails ordersDetails : order.getOrdersDetails()) {
                // Par défaut on initialise la qté a préparer avec la qté
                // demandée
                ordersDetails.setQuantityToPrepare(ordersDetails.getQuantityAsked());
            }

            /** EDU - 2020 02
             *  Une commande passant en Validée doit remonter au back central
             *  
             */
            //Création du JOB (flux poussé)
            //C'est un JOB d'Upload
            //Il sera du type Commande
            //Il contiendra le DTO de notre commande (disponible une fois l'update OK )
            //Il sera à destination du central (Location 0) ?
            job = JobHelper.initJobCommandePoussee();

        } else if (order.getStatus().equals(Orders.Status.PLANIFICATION)) {
            // order.setPlanificationDate(new Date());
            // order.setPlanificationUser(user);
            order.setPreparationDate(new Date());
            order.setPreparationUser(user);
            order.setStatus(Orders.Status.PREPARATION);

            // maj du status des palettes détails...
            for (Palettes palettes : order.getPalettes()) {
                // palettes.setPlanificationDate(new Date());
                // palettes.setPlanificationUser(user);
                // palettes.setPreparationDate(new Date());
                palettes.setStatus(Orders.Status.PREPARATION);
                for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                    palettesDetails.setStatus(Orders.Status.PREPARATION);
                }
            }
        } else if (order.getStatus().equals(Orders.Status.PREPARATION)) {
            // order.setPreparationDate(new Date());
            // order.setPreparationUser(user);
            order.setControleDate(new Date());
            order.setControleUser(user);
            order.setStatus(Orders.Status.CONTROLE);
        } else if (order.getStatus().equals(Orders.Status.CONTROLE)) {
            // order.setControleDate(new Date());
            // order.setControleUser(user);
            order.setExpeditionDate(new Date());
            order.setExpeditionUser(user);
            order.setStatus(Orders.Status.EXPEDITION);
            for (Palettes palettes : order.getPalettes()) {
                if (Integer.parseInt(palettes.getStatus()) < Integer.parseInt(Orders.Status.EXPEDITION)) {
                    // on positionne la date mais pas l'utilisateur pour repérer
                    // que c'est un passage automatique
                    // palettes.setControleDate(new Date());
                    // palettes.setControleUser(null);
                    palettes.setExpeditionDate(new Date());
                    palettes.setExpeditionUser(user);
                    palettes.setStatus(Orders.Status.EXPEDITION);
                    for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                        // lors du forçage du controle depuis la commande on
                        // considère qu'il y a tout
                        palettesDetails.setQuantitySent(palettesDetails.getQuantityPrepared());
                        palettesDetails.setStatus(Orders.Status.EXPEDITION);
                        palettesDetails.getOrderDetail()
                        .setQuantitySent(palettesDetails.getOrderDetail().getQuantitySent() + palettesDetails.getQuantityPrepared());
                    }
                }
            }
            // Generer les mouvements de stock
            generateSendingMovements(order);

            /** EDU - 2020 02
             *  Une commande passant en Expédiée doit remonter au back central
             *  
             */
            //Création du JOB (flux poussé)
            //C'est un JOB d'Upload
            //Il sera du type Commande
            //Il contiendra le DTO de notre commande (disponible une fois l'update OK )
            //Il sera à destination du central (Location 0) ?
            job = JobHelper.initJobCommandePoussee();
        }

        retour = getOrderDTO(ordersDAO.save(order));

        if(job != null) {

            try {
                List<PaletteDTO> paletteDtos = AdaptableHelper.getAdapter(order.getPalettes() , PaletteDTO.class);
                JobHelper.fillJobCommandes(job, retour, paletteDtos);
                jobService.save(job);
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return retour;
    }


    @Override
    public Object getProductsLocations(String orderId) {
        Orders order = ordersDAO.findById(orderId).orElse(null);
        List<String> productsLocations = new ArrayList<>();
        for (OrdersDetails ordersDetails : order.getSortedOrdersDetails()) {
            double qtyPalettized = order.getQuantityPalettized(ordersDetails.getId());
            if (qtyPalettized < ordersDetails.getQuantityToPrepare()) {
                String productLocation = ordersDetails.getProduct().getProductLocation(order.getLocationFromId());
                if (!productsLocations.contains(productLocation)) {
                    productsLocations.add(productLocation);
                }
            }
        }
        return productsLocations;
    }

    @Override
    public PaletteDTO generatePalette(String name, String orderId, String locationFrom, String numberOfLocation) {

        Palettes palettes = new Palettes();
        Orders order = ordersDAO.findById(orderId).orElse(null);
        People user = peopleDAO.findByUsername(name);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        palettes.setName("P/" + String.format("%03d", order.getPalettes().size() + 1) + " - " + order.getLocationTo().getId() + "/"
                + simpleDateFormat.format(order.getCreationDate()));
        palettes.setOrder(order);

        // A la génération l'état est initialisé à validation -> l'opérateur ne
        // peut pas commencer à la préparer
        palettes.setCreationDate(new Date());
        palettes.setCreationUser(user);

        palettes.setPlanificationDate(new Date());
        palettes.setPlanificationUser(user);

        String paletteStatus = Orders.Status.PLANIFICATION;
        if (order.getStatus().equals(Orders.Status.PREPARATION) || order.getStatus().equals(Orders.Status.CONTROLE)) {
            // Si la commande est en préparation ou contrôle , alors la palette
            // doit aussi passer en préparation
            palettes.setPlanificationDate(new Date());
            palettes.setPlanificationUser(user);
            paletteStatus = Orders.Status.PREPARATION;
        }
        palettes.setStatus(paletteStatus);

        List<PalettesDetails> details = new ArrayList<>();
        List<String> locationsInPalette = new ArrayList<>();
        int nb = Integer.parseInt(numberOfLocation);
        boolean ok = false;
        for (OrdersDetails ordersDetails : order.getSortedOrdersDetails()) {
            // permet de définir le premier emplacement a partir duquel on prend
            // les produits
            if (ordersDetails.getProduct().getProductLocation(order.getLocationFromId()).equals(locationFrom)) {
                ok = true;
            }
            double qtyPalettized = order.getQuantityPalettized(ordersDetails.getId());

            if (ok && qtyPalettized < ordersDetails.getQuantityToPrepare()) {
                String location = ordersDetails.getProduct().getProductLocation(order.getLocationFromId());
                if (!locationsInPalette.contains(location)) {
                    locationsInPalette.add(location);
                }
                if (locationsInPalette.size() <= nb) {
                    PalettesDetails palettesDetails = new PalettesDetails();
                    palettesDetails.setOrderDetail(ordersDetails);
                    palettesDetails.setPalette(palettes);
                    palettesDetails.setQuantityToPrepare(ordersDetails.getQuantityToPrepare() - qtyPalettized);
                    palettesDetails.setStockLocationName(location);
                    palettesDetails.setStatus(paletteStatus);
                    details.add(palettesDetails);
                }
            }
        }
        palettes.setPalettesDetails(details);
        return palettes.getAdapter(PaletteDTO.class);
    }

    @Override
    public Object savePalette(PaletteDTO dto) {
        Palettes palettes = new Palettes();
        if (dto.getId() != null && !dto.getId().isEmpty()) {
            palettes = palettesDAO.findById(dto.getId()).orElse(null);
            if (palettes == null) {
                palettes = new Palettes();
            } else {
                if(Orders.Status.RECEPTED.equals(palettes.getStatus())) {
                    return null;
                }
            }
        }
        palettes.setName(dto.getName());
        palettes.setNote(dto.getNote());
        Orders order = ordersDAO.findById(dto.getOrderId()).orElse(null);
        if(order == null) {
            return null;
        }
        palettes.setOrder(order);

        palettes.setCreationDate(dto.getCreationDate());
        // Ne doit jamais être null ...
        if (dto.getCreationUser() != null && dto.getCreationUser().getId() != null) {
            palettes.setCreationUser(peopleDAO.findById(dto.getCreationUser().getId()).orElse(null));
        }

        palettes.setPlanificationDate(dto.getPlanificationDate());
        if (dto.getPlanificationUserId() != null) {
            palettes.setPlanificationUser(peopleDAO.findById(dto.getPlanificationUserId()).orElse(null));
        }

        palettes.setPreparationDate(dto.getPreparationDate());
        // Ne doit jamais être null ...
        if (dto.getPreparationUserId() != null) {
            palettes.setPreparationUser(peopleDAO.findById(dto.getPreparationUserId()).orElse(null));
        }

        palettes.setControleDate(dto.getControlDate());
        // Ne doit jamais être null ...
        if (dto.getControlUserId() != null) {
            palettes.setControleUser(peopleDAO.findById(dto.getControlUserId()).orElse(null));
        }

        palettes.setExpeditionDate(dto.getDelivDate());
        // Ne doit jamais être null ...
        if (dto.getDelivUserId() != null) {
            palettes.setExpeditionUser(peopleDAO.findById(dto.getDelivUserId()).orElse(null));
        }

        palettes.setReceptionDate(dto.getReceptDate());
        // Ne doit jamais être null ...
        if (dto.getReceptUserId() != null) {
            palettes.setReceptionUser(peopleDAO.findById(dto.getReceptUserId()).orElse(null));
        }

        palettes.setStatus(Integer.toString(dto.getStatusOrder()));

        List<PalettesDetails> details = new ArrayList<>();
        for (PaletteDetailDTO detailDTO : dto.getPaletteDetails()) {
            PalettesDetails palettesDetails = new PalettesDetails();

            OrdersDetails ordersDetails = ordersDetailsDAO.findById(detailDTO.getOrderDetailId()).orElse(null);

            palettesDetails.setPreparationDate(detailDTO.getPreparationDate());
            if (detailDTO.getPreparationUserId() != null) {
                palettesDetails.setPreparationUser(peopleDAO.findById(detailDTO.getPreparationUserId()).orElse(null));
            }
            palettesDetails.setOrderDetail(ordersDetails);
            palettesDetails.setPalette(palettes);
            palettesDetails.setQuantityToPrepare(detailDTO.getQuantityToPrepare());
            palettesDetails.setQuantityPrepared(detailDTO.getQuantityPrepared());
            palettesDetails.setQuantitySent(detailDTO.getQuantitySent());
            palettesDetails.setQuantityReceived(detailDTO.getQuantityReceived());
            palettesDetails.setStatus(detailDTO.getStatus());
            palettesDetails.setStockLocationName(detailDTO.getStockLocationName());
            palettesDetails.setNote(detailDTO.getNote());
            if (detailDTO.getId() != null && !detailDTO.getId().isEmpty()) {
                palettesDetails.setId(detailDTO.getId());
            } else {
                palettesDetails.createId();
            }
            details.add(palettesDetails);
        }

        palettes.setPalettesDetails(details);

        if (palettes.getId() != null) {
            palettes = palettesDAO.save(palettes);
        } else {
            if (dto.getId() != null && !dto.getId().isEmpty()) {
                palettes.setId(dto.getId());
            } else {
                palettes.createId();
            }
            palettes = palettesDAO.save(palettes);
            order.getPalettes().add(palettes);
        }

        return palettes.getAdapter(PaletteDTO.class);
    }

    @Override
    public Object getPalette(String paletteId) {
        Optional<Palettes> palette = palettesDAO.findById(paletteId);
        return palette.isPresent() ? palette.get().getAdapter(PaletteDTO.class) : null;
    }

    @Override
    public List<PaletteInfoDTO> getPalettes(String orderId) {
        List<Palettes> candidates = palettesDAO.findByCriteria(orderId, null);
        List<Palettes> results = new ArrayList<>();
        for (Palettes palettes : candidates) {
            if (Integer.parseInt(palettes.getStatus()) < Integer.parseInt(Orders.Status.EXPEDITION)) {
                results.add(palettes);
            }
        }
        return AdaptableHelper.getAdapter(results, PaletteInfoDTO.class);
    }

    @Override
    public Object addToPalette(String paletteId, String orderDetailId) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);
        addOrdersDetails(palettes , orderDetailId, true);
        palettesDAO.save(palettes);
        return "OK";
    }

    @Override
    public Object deleteFromPalette(String paletteId, String paletteDetailId) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);
        PalettesDetails retour = palettes.getPalettesManager().deletePaletteDetails(paletteDetailId);

        if (retour != null) {
            palettesDAO.save(palettes);
            palettesDetailsDAO.delete(retour);
        }

        return palettes == null ? null : palettes.getAdapter(PaletteDTO.class);
    }

    @Override
    public Object getPalettesByCriteria(String name, String status, String statusPalette, String orderId) {
        if (Orders.Status.PREPARATION.equals(status) || Orders.Status.PREPARATION.equals(statusPalette)) {
            return AdaptableHelper.getAdapter(palettesDAO.findByCriteria(name, status, statusPalette, orderId), PaletteInfoDTO.class);
        }
        return AdaptableHelper.getAdapter(palettesDAO.findByCriteria(null, status, statusPalette, orderId), PaletteInfoDTO.class);
    }

    @Override
    public Object updatePaletteDetail(String paletteId, String status, String productId, Double quantity, boolean closePaletteDetail, String userName,String inputOrigine) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);

        PalettesDetails palettesDetails = palettes.getPaletteDetail(productId);
        if (Orders.Status.PREPARATION.equals(status) && palettesDetails != null) {
            People preparationUser = peopleDAO.findByUsername(userName);
            ;
            if (palettes.getPreparationDate() == null) {
                palettes.setPreparationDate(new Date());
            }
            palettesDetails.setPreparationDate(new Date());

            palettesDetails.setPreparationUser(preparationUser);
            palettesDetails.setQuantityPrepared(palettesDetails.getQuantityPrepared() + quantity);
            // si on a atteint la qté a préparer on peut passer la palette à
            // l'état controle
            if (palettesDetails.getQuantityPrepared() >= palettesDetails.getQuantityToPrepare() || closePaletteDetail) {
                palettesDetails.setStatus(Orders.Status.CONTROLE);
            }
            palettesDetails.setInputOrigine(inputOrigine);


        }
        if (Orders.Status.CONTROLE.equals(status)) {
            if (palettesDetails != null) {
                palettesDetails.setQuantitySent(palettesDetails.getQuantitySent() + quantity);
            } else {
                // création d'un OD
                Orders orders = palettes.getOrder();
                addProductsToOrder(orders , null, productId, 0, 0, quantity, 0);
                ordersDAO.save(orders);
                // affectation à la palette (création d'un nouveau
                // PaletteDetail)
                OrdersDetails ordersDetails = palettes.getOrder().getOrdersManager().getOrdersDetails(productId);
                addOrdersDetails(palettes ,ordersDetails.getId(), false);
                palettesDetails = palettes.getPalettesDetails().get(0);
            }
            palettesDetails.setInputOrigine(inputOrigine);
        }
        if (Orders.Status.EXPEDITION.equals(status)) {
            if (palettesDetails != null) {
                palettesDetails.setQuantityReceived(palettesDetails.getQuantityReceived() + quantity);
            } else {
                // création d'un OD
                Orders orders = palettes.getOrder();
                addProductsToOrder(orders , null, productId, 0, 0, 0, quantity);
                ordersDAO.save(orders);
                // affectation à la palette (création d'un nouveau
                // PaletteDetail)
                OrdersDetails ordersDetails = palettes.getOrder().getOrdersManager().getOrdersDetails(productId);
                addOrdersDetails(palettes, ordersDetails.getId(), false);
            }
        }

        Orders orders = palettes.getOrder();
        for (OrdersDetails ordersDetails : orders.getOrdersDetails()) {
            if (Orders.Status.PREPARATION.equals(status)) {
                double qty = orders.getQuantityPrepared(ordersDetails.getId());
                ordersDetails.setQuantityPrepared(qty);
            }
            if (Orders.Status.CONTROLE.equals(status)) {
                double qty = orders.getQuantitySent(ordersDetails.getId());
                ordersDetails.setQuantitySent(qty);
            }
            if (Orders.Status.EXPEDITION.equals(status)) {
                double qty = orders.getQuantityReceived(ordersDetails.getId());
                ordersDetails.setQuantityReceived(qty);
            }
        }
        ordersDAO.save(orders);

        return palettesDAO.save(palettes).getAdapter(PaletteDTO.class);
    }

    private Object preparationEnd(People user, Palettes palettes) {

        Palettes palettes2 = new Palettes();
        // Orders order =
        // ordersDAO.findById(palettes.getOrder().getId());
        Orders order = palettes.getOrder();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        palettes2.setName("P/" + String.format("%03d", order.getPalettes().size() + 1) + " - " + order.getLocationTo().getId() + "/"
                + simpleDateFormat.format(order.getCreationDate()));
        palettes2.setOrder(order);

        // c'est le préparateur qui créé la palette contenant le reliquat
        palettes2.setCreationDate(new Date());
        palettes2.setCreationUser(user);
        // c'est le préparateur qui planifie la palette contenant le reliquat
        palettes2.setPlanificationDate(new Date());
        palettes2.setPlanificationUser(user);
        // c'est le préparateur qui prépare la palette contenant le reliquat
        palettes2.setPreparationDate(new Date());
        palettes2.setPreparationUser(user);
        palettes2.setStatus(Orders.Status.PREPARATION);

        List<PalettesDetails> details = new ArrayList<>();
        List<PalettesDetails> details2 = new ArrayList<>();

        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            // Si le pd est a préparation c'est que la ligne n'est pas terminée
            // création d'un reliquat à préparer
            if (palettesDetails.getStatus().equals(Orders.Status.PREPARATION)) {
                double qty = 0D;
                palettesDetails.setStatus(Orders.Status.CONTROLE);
                qty = palettesDetails.getQuantityToPrepare() - palettesDetails.getQuantityPrepared();

                if (qty > 0) {
                    PalettesDetails palettesDetails2 = new PalettesDetails();
                    palettesDetails2.setNote(palettesDetails.getNote());
                    palettesDetails2.setOrderDetail(palettesDetails.getOrderDetail());
                    palettesDetails2.setPalette(palettes2);
                    palettesDetails2.setQuantityToPrepare(qty);
                    palettesDetails2.setStatus(Orders.Status.PREPARATION);
                    palettesDetails2.setStockLocationName(palettesDetails.getStockLocationName());
                    palettesDetails2.createId();
                    details2.add(palettesDetails2);
                }
                if (palettesDetails.getQuantityPrepared() > 0) {
                    details.add(palettesDetails);
                }
            } else {
                details.add(palettesDetails);
            }
        }

        palettes.setPalettesDetails(details);
        // palettes.setPreparationDate(new Date());

        palettes.setControleDate(new Date());
        palettes.setControleUser(user);
        palettes.setStatus(Orders.Status.CONTROLE);

        palettes = palettesDAO.save(palettes);

        if (!details2.isEmpty()) {
            palettes2.setPalettesDetails(details2);
            palettes2.createId();
            palettes2 = palettesDAO.save(palettes2);
            return palettes2.getAdapter(PaletteDTO.class);
        }

        // On ne passe pas automatiquement à l'état CONTROLE
        // order.getOrdersManager().tryPassNextStep(Orders.Status.CONTROLE);
        return palettes.getAdapter(PaletteDTO.class);
    }

    private Object controleEnd(People user, Palettes palettes) {
        palettes.setStatus(Orders.Status.EXPEDITION);
        // palettes.setControleDate(new Date());
        // palettes.setControleUser(user);
        palettes.setExpeditionDate(new Date());
        palettes.setExpeditionUser(user);

        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            palettesDetails.setStatus(Orders.Status.EXPEDITION);
        }
        palettes = palettesDAO.save(palettes);
        // On ne passe pas automatiquement à l'état CONTROLE
        tryPassNextStep(palettes.getOrder() , Orders.Status.EXPEDITION);
        return palettes.getAdapter(PaletteDTO.class);
    }

    private Object receptionEnd(People user, Palettes palettes) {
        palettes.setStatus(Orders.Status.RECEPTED);
        // palettes.setExpeditionDate(new Date());
        // palettes.setExpeditionUser(user);
        palettes.setReceptionDate(new Date());
        palettes.setReceptionUser(user);
        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            palettesDetails.setStatus(Orders.Status.RECEPTED);
        }

        palettes = palettesDAO.save(palettes);
        tryPassNextStep(palettes.getOrder() , Orders.Status.RECEPTED);
        //générer les mouvements de stocks !!!
        generateStockMovements(palettes);
        return palettes.getAdapter(PaletteDTO.class);
    }

    private void generateStockMovements(Palettes palettes) {
        Orders orders = palettes.getOrder();
        Locations to = orders.getLocationTo();

        Locations transit = locationsDAO.findById(to.getId() + "-TRANSIT").orElse(null);
        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            OrdersDetails ordersDetails = palettesDetails.getOrderDetail();
            
            //EDU - 2020 10 On passe sur la nouvelle fonction qui checke que ce mouvmeent n'a pas déjà été passé
            //+ Modificationn de la ref du mvt
           
            stocksService.createStockMove(transit , to , ordersDetails.getProductId() , palettes.getName() , null , palettesDetails.getQuantityReceived() 
                    , 0D , StockDiary.Type.REASON_OUT_TRANSFERT , null);
            stocksService.createStockMove(to , transit , ordersDetails.getProductId() , palettes.getName() , null , palettesDetails.getQuantityReceived() 
                    , 0D , StockDiary.Type.REASON_IN_TRANSFERT , null);
        }
        stocksService.adjustMovements();
    }

    @Override
    public Object nextPaletteStep(String name, String paletteId, String status) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);
        People user = peopleDAO.findByUsername(name);
        if (Orders.Status.PREPARATION.equals(status)) {
            return preparationEnd(user, palettes);
        }
        if (Orders.Status.CONTROLE.equals(status)) {
            return controleEnd(user, palettes);
        }
        if (Orders.Status.EXPEDITION.equals(status)) {
            return receptionEnd(user, palettes);
        }
        return palettes.getAdapter(PaletteDTO.class);
    }

    @Override
    public Object deleteOrder(String orderId) {
        Orders order = ordersDAO.findById(orderId).orElse(null);  
        ordersDAO.delete(order);
        //TODO voir l'usage de ce qu'on fait du retour
        return true;
    }

    @Override
    public Object canAddToPalette(String paletteId, String status, String productId, double quantity) {
        if (Orders.Status.CONTROLE.equals(status) || Orders.Status.EXPEDITION.equals(status)) {
            return 0;
        }
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);
        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            if (palettesDetails.getOrderDetail().getProductId().equals(productId)) {
                if (palettesDetails.getQuantityPrepared() + quantity > palettesDetails.getQuantityToPrepare()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
        return -1;
    }

    @Override
    public Object canAddToOrder(String orderId, String productId, double quantity) {
        Orders orders = ordersDAO.findById(orderId).orElse(null);

        for (OrdersDetails ordersDetails : orders.getOrdersDetails()) {
            if (ordersDetails.getProductId().equals(productId)) {
                double quantitySent = orders.getQuantitySent(ordersDetails.getId());
                // Si on a commencé à ramassé et qu'on veut en enlever
                if (quantitySent > 0 && quantity < 0) {
                    return false;
                }
            }
        }

        return true;
    };

    @Override
    public Object deletePalette(String paletteId) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);
        Orders orders = palettes.getOrder();
        orders.getPalettes().remove(palettes);
        palettesDAO.delete(palettes);
        return getOrderDTO(orders);
    }

    @Override
    public Object fillPaletteQuantity(String paletteId) {
        Palettes palettes = palettesDAO.findById(paletteId).orElse(null);

        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            if (Orders.Status.EXPEDITION.equals(palettes.getStatus())) {
                palettesDetails.setQuantityReceived(palettesDetails.getQuantitySent());
            }
            if (Orders.Status.CONTROLE.equals(palettes.getStatus())) {
                palettesDetails.setQuantitySent(palettesDetails.getQuantityPrepared());
            }
        }
        return palettesDAO.save(palettes).getAdapter(PaletteDTO.class);
    }

    @Override
    public void savePaletteDetails(List<PaletteDetailDTO> dtos) {
        ArrayList<PalettesDetails> newOrdersDetails = new ArrayList<PalettesDetails>();
        ArrayList<PalettesDetails> updOrdersDetails = new ArrayList<PalettesDetails>();
        for (PaletteDetailDTO dto : dtos) {
            PalettesDetails palettesDetails = palettesDetailsDAO.findById(dto.getId()).orElse(null);
            Boolean newOne = palettesDetails == null;
            
            if(newOne || ! Orders.Status.RECEPTED.equals(palettesDetails.getPalette().getOrder().getStatus()) ) {
                palettesDetails = prepareSavePaletteDetail(dto, palettesDetails);
                if (palettesDetails.getOrderDetail() != null) {
                    if (newOne) {
                        newOrdersDetails.add(palettesDetails);
                    } else {
                        updOrdersDetails.add(palettesDetails);
                    }
                }
            }
        }
        palettesDetailsDAO.saveAll(newOrdersDetails);
        palettesDetailsDAO.saveAll(updOrdersDetails);
    }

    @Override
    public Object savePaletteDetail(PaletteDetailDTO dto) {

        PaletteDetailDTO retour = null;

        PalettesDetails palettesDetails = palettesDetailsDAO.findById(dto.getId()).orElse(null);

        Boolean newOne = palettesDetails == null;

        palettesDetails = prepareSavePaletteDetail(dto, palettesDetails);

        if (palettesDetails.getOrderDetail() != null) {
            if (newOne) {
                retour = palettesDetailsDAO.save(palettesDetails).getAdapter(PaletteDetailDTO.class);
            } else {
                retour = palettesDetailsDAO.save(palettesDetails).getAdapter(PaletteDetailDTO.class);
            }
        }

        return retour;
    }

    public PalettesDetails prepareSavePaletteDetail(PaletteDetailDTO dto, PalettesDetails palettesDetails) {
        String id = dto.getId();
        Boolean newOne = palettesDetails == null;

        if (id == null) {
            System.out.print("Id est null c'est vraiement étrange !");
        }
        if (newOne) {
            palettesDetails = new PalettesDetails();
            // On rempli
            // pas d'id null
            palettesDetails.setOrderDetail(ordersDetailsDAO.findById(dto.getOrderDetailId()).orElse(null));
            palettesDetails.setId(id);
            id = dto.getPaletteId();
            if (id == null) {
                System.out.print("Id est null pour la palette c'est vraiment étrange !");
            }
            palettesDetails.setPalette(palettesDAO.findById(id).orElse(null));

        }

        palettesDetails.setPreparationDate(dto.getPreparationDate());
        if (dto.getPreparationUserId() != null) {
            palettesDetails.setPreparationUser(peopleDAO.findById(dto.getPreparationUserId()).orElse(null));
        }
        palettesDetails.setStatus(dto.getStatus());
        palettesDetails.setStockLocationName(dto.getStockLocationName());
        palettesDetails.setNote(dto.getNote());
        palettesDetails.setQuantityToPrepare(dto.getQuantityToPrepare());
        palettesDetails.setQuantityPrepared(dto.getQuantityPrepared());
        palettesDetails.setQuantitySent(dto.getQuantitySent());
        palettesDetails.setQuantityReceived(dto.getQuantityReceived());
        palettesDetails.setInputOrigine(dto.getInputOrigine());

        return palettesDetails;
    }

    @Override
    public List<OrderDetailIndexDTO> getOrderDetails(String orderId) {
        Orders order = ordersDAO.findById(orderId).orElse(null);

        return order == null ? new ArrayList<OrderDetailIndexDTO>() : getOrderDetailIndexDTOList(order);
    }

    @Override
    public Object getProductsPaletteInfo(String orderId, String productId) {
        return ordersDAO.findProductsPaletteInfo(orderId, productId);
    }

    @Override
    public List<PalettePreparationInfoDTO> getPalettePreparationInfo(String orderId) {

        List<Map<String, Object>> sqlResults = palettesDAO.findAllPalettePreparationInfo(orderId);
        //        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        //        SimpleDateFormat sdfHeure = new SimpleDateFormat("HH:mm:ss");
        Map<String, PalettePreparationInfoDTO> results = new HashMap<String, PalettePreparationInfoDTO>();

        for (Map<String, Object> mapSql : sqlResults) {
            //            String status = getValueAsString(mapSql, "PALETTE_STATUS");
            String id = getValueAsString(mapSql, "PREPARATEUR_ID");
            String preparateur = getValueAsString(mapSql, "PREPARATEUR");
            Long nbEmpl = getValueAsLong(mapSql, "NB_EMPL_TOTAL");
            Long nbEmplRestant = getValueAsLong(mapSql, "NB_EMPL_RESTANT");
            String emp = getValueAsString(mapSql, "DERNIER_EMPL");
            Date date = getValueAsDate(mapSql, "DATE_DERNIER_EMPL");

            if (!results.containsKey(id)) {
                PalettePreparationInfoDTO dto = new PalettePreparationInfoDTO();
                results.put(id, dto);
            }

            PalettePreparationInfoDTO dto = results.get(id);
            dto.addPalette();
            dto.setPreparateurId(id);
            dto.setPreparateur(preparateur);
            dto.addNombreEmplacement(nbEmpl);
            dto.addNombreEmplacementRestant(nbEmplRestant);
            if (date != null) {
                dto.setDernierEmplacement(emp);
                dto.setdDernierEmplacement(date);
            }
        }

        return new ArrayList<PalettePreparationInfoDTO>(results.values());
    }

    private String getValueAsString(Map<String, Object> map, String key) {
        if (map.containsKey(key)) {
            if (map.get(key) != null) {
                return map.get(key).toString();
            }
        }
        return null;
    }

    private Date getValueAsDate(Map<String, Object> map, String key) {
        if (map.containsKey(key)) {
            if (map.get(key) != null) {
                return (Date) map.get(key);
            }
        }
        return null;
    }

    private Long getValueAsLong(Map<String, Object> map, String key) {
        if (map.containsKey(key)) {
            if (map.get(key) != null) {
                return (Long) map.get(key);
            }
        }
        return null;
    }

    @Override
    public Object receivePalettes(String user,String orderId) {
        Orders order = ordersDAO.findById(orderId).orElse(null);
        List<Palettes> palettes = order.getPalettes();
        for (Palettes palette : palettes) {
            this.fillPaletteQuantity(palette.getId());
            this.nextPaletteStep(user, palette.getId(),palette.getStatus());
        }
        return true;
    }

    @Override
    public List<ProductsResultDTO> getProductsByEmplacement(String paletteId,String emplacement) {
        return productsService.getAdapter(palettesDAO.getProductsByEmplacement(paletteId,emplacement) , ProductsResultDTO.class ,true);
    }

    public void initializeOrders(Orders orders , String name, String locationId) {
        Locations locations = locationsDAO.findById(locationId).orElse(null);
        orders.setLocationTo(locations);
        orders.setCreationUser(peopleDAO.findByUsername(name));
        generateOrderName(orders);
    }

    public void generateOrderName(Orders orders) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");        
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
        parameters.add(CriteriaConstant.LOCATION_ID, orders.getLocationTo().getId());
        List<Orders> list = ordersDAO.findAllByCriteria(parameters);
        
        orders.setName(orders.getLocationTo().getId() + "/" + String.format("%03d", list.size() + 1) + "-"
                + simpleDateFormat.format(new Date()));
    }

    public void addProductsToOrder(Orders orders , String orderDetailId, String productId, double quantityAsked, double quantityToPrepare, double quantitySent,
            double quantityReceived) {

        if (orders.getStatus().equals(Orders.Status.CREATION) && quantityAsked == 0) {
            return;
        }

        OrdersDetails ordersDetails = null;
        // si orderDetailId != null alors MAJ de OD avec les nouvelles qté
        if (orderDetailId != null) {
            ordersDetails = ordersDetailsDAO.findById(orderDetailId).orElse(null);
        } else {
            // sinon on cherche s'il existe déjà un OD avec ce produit
            ordersDetails = orders.getOrdersManager().getOrdersDetails(productId);
        }

        if (ordersDetails != null) {
            if (orders.getStatus().equals(Orders.Status.CREATION)) {
                if (orderDetailId != null) {
                    // il faut ajouter écraser les qté si MAJ OD
                    ordersDetails.setQuantityAsked(quantityAsked);
                } else {
                    // ajouter sinon
                    ordersDetails.setQuantityAsked(ordersDetails.getQuantityAsked() + quantityAsked);
                }
            }
            if (orders.getStatus().equals(Orders.Status.PLANIFICATION) || orders.getStatus().equals(Orders.Status.PREPARATION)) {
                if (orderDetailId != null) {
                    // il faut ajouter écraser les qté si MAJ OD
                    ordersDetails.setQuantityToPrepare(quantityToPrepare);
                } else {
                    // ajouter sinon
                    ordersDetails.setQuantityToPrepare( ordersDetails.getQuantityToPrepare() + quantityToPrepare);
                }
            }
        } else {
            // Si aucun OD avec le produit, il faut créer un nouvel OD
            ordersDetails = new OrdersDetails();
            ordersDetails.setOrder(orders);
            Products products = productsDAO.findById(productId).orElse(null);
            ordersDetails.setProduct(products);
            ordersDetails.setQuantityAsked(quantityAsked);
            ordersDetails.setQuantityToPrepare(quantityToPrepare);
            ordersDetails.setQuantitySent(quantitySent);
            ordersDetails.setQuantityReceived(quantityReceived);
            ordersDetails.createId();
            orders.getOrdersDetails().add(ordersDetails);
        }
    }

    public void generateSendingMovements(Orders orders) {
        Locations from = orders.getLocationFrom();
        Locations to = orders.getLocationTo();
        Locations transit = locationsDAO.findById(to.getId() + "-TRANSIT").orElse(null);
        //EDU 2020 10 On change le fonctionnement désormais les mouvements sont tagués par palette
        for( Palettes palettes : orders.getPalettes()) {
            for(PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
             
                stocksService.createStockMove(from , transit , palettesDetails.getOrderDetail().getProductId() , palettes.getName() , null , palettesDetails.getQuantityReceived() 
                        , 0D , StockDiary.Type.REASON_OUT_MOVEMENT , null);
                stocksService.createStockMove(transit , from , palettesDetails.getOrderDetail().getProductId() , palettes.getName() , null , palettesDetails.getQuantityReceived() 
                        , 0D , StockDiary.Type.REASON_IN_MOVEMENT , null);
            }
        }
        stocksService.adjustMovements();
    }

    // SEULEMENT POUR MAJ AUTO LORSQUE L'ON CHANGE L'ETAT D'UNE PALETTE
    public void tryPassNextStep(Orders orders , String status) {
        boolean canPass = true;
        boolean createJob = false;
        for (Palettes palettes : orders.getPalettes()) {
            if (Integer.parseInt(palettes.getStatus()) < Integer.parseInt(status)) {
                canPass = false;
            }
        }
        if (canPass) {
            orders.setStatus(status);
            // recalcul des quantités
            for (OrdersDetails ordersDetails : orders.getOrdersDetails()) {
                ordersDetails.setQuantityPrepared(orders.getQuantityPrepared(ordersDetails.getId()));
                ordersDetails.setQuantitySent(orders.getQuantitySent(ordersDetails.getId()));
                ordersDetails.setQuantityReceived(orders.getQuantityReceived(ordersDetails.getId()));
            }

            if (status.equals(Orders.Status.EXPEDITION)) {
                // MAJ de la date de fin de controle, on laisse le user a null car c'est le systeme qui fait passer à l'état suivant
                // orders.setControleDate(new Date());
                // orders.setControleUser(null);
                orders.setExpeditionDate(new Date());
                orders.setExpeditionUser(null);

                generateSendingMovements(orders);
                createJob  = true;
            } else if (status.equals(Orders.Status.RECEPTED)) {
                // MAJ de la date de fin de controle, on laisse le user a null car c'est le systeme qui fait passer à l'état suivant
                // orders.setExpeditionDate(new Date());
                // orders.setExpeditionUser(null);
                orders.setReceptionDate(new Date());
                orders.setReceptionUser(null);
                createJob = true;

            }
            ordersDAO.save(orders);
            if(createJob) {
                Job job = null;
                try {
                    job = JobHelper.initJobCommandePoussee();
                    List<PaletteDTO> paletteDtos = AdaptableHelper.getAdapter(orders.getPalettes() , PaletteDTO.class);
                    JobHelper.fillJobCommandes(job, getOrderDTO(orders), paletteDtos);
                    jobService.save(job);
                } catch (JsonProcessingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean loadFromDTO(Orders orders , Object source) {
        boolean retour = false;
        if (OrderDTO.class.isInstance(source)) {
            OrderDTO dto = (OrderDTO) source;

            if (!dto.getId().equals(orders.getId())) {
                retour = true;
                orders.setId(dto.getId());
            }
            if (!dto.getCreationDate().equals(orders.getCreationDate())) {
                retour = true;
                orders.setCreationDate(dto.getCreationDate());
            }
            if (!dto.getLocationFromId().equals(orders.getLocationFromId())) {
                retour = true;
                orders.setLocationFrom(locationsDAO.findById(dto.getLocationFromId()).orElse(null));
            }
            if (!dto.getLocationToId().equals(orders.getLocationToId())) {
                retour = true;
                orders.setLocationTo(locationsDAO.findById(dto.getLocationToId()).orElse(null));
            }
            if (!dto.getNote().equals(orders.getNote())) {
                retour = true;
                orders.setNote(dto.getNote());
            }
            if (!dto.getReference().equals(orders.getName())) {
                retour = true;
                orders.setName(dto.getReference());
            }
            if (dto.getStatusOrder() != Integer.parseInt(orders.getStatus())) {
                retour = true;
                orders.setStatus(String.valueOf(dto.getStatusOrder()));
            }
            if (!dto.getUsernameId().equals(orders.getCreationUserId())) {
                retour = true;
                orders.setCreationUser(peopleDAO.findById(dto.getUsernameId()).orElse(null));
            }
            if (!dto.getPlanifDate().equals(orders.getPlanificationDate())) {
                retour = true;
                orders.setPlanificationDate(dto.getPlanifDate());
            }
            if (!dto.getPlanifUserId().equals(orders.getPlanificationUserId())) {
                retour = true;
                orders.setPlanificationUser(peopleDAO.findById(dto.getPlanifUserId()).orElse(null));
            }
            if (!dto.getPrepDate().equals(orders.getPlanificationDate())) {
                retour = true;
                orders.setPlanificationDate(dto.getPrepDate());
            }
            if (!dto.getPrepUserId().equals(orders.getPreparationUserId())) {
                retour = true;
                orders.setPreparationUser(peopleDAO.findById(dto.getPrepUserId()).orElse(null));
            }
            if (!dto.getControlDate().equals(orders.getControleDate())) {
                retour = true;
                orders.setControleDate(dto.getControlDate());
            }
            if (!dto.getControlUserId().equals(orders.getControleUserId())) {
                retour = true;
                orders.setControleUser(peopleDAO.findById(dto.getControlUserId()).orElse(null));
            }
            if (!dto.getDelivDate().equals(orders.getExpeditionDate())) {
                retour = true;
                orders.setExpeditionDate(dto.getDelivDate());
            }
            if (!dto.getDelivUserId().equals(orders.getExpeditionUserId())) {
                retour = true;
                orders.setExpeditionUser(peopleDAO.findById(dto.getDelivUserId()).orElse(null));
            }
            if (!dto.getReceptDate().equals(orders.getReceptionDate())) {
                retour = true;
                orders.setReceptionDate(dto.getReceptDate());
            }
            if (!dto.getReceptUserId().equals(orders.getReceptionUserId())) {
                retour = true;
                orders.setReceptionUser(peopleDAO.findById(dto.getReceptUserId()).orElse(null));
            }

            ArrayList<OrdersDetails> lines = new ArrayList<OrdersDetails>();

            for (OrderDetailDTO orderDetailDTO : dto.getOrderDetails()) {
                OrdersDetails ordersDetails = new OrdersDetails();
                ordersDetails.setOrder(orders);
                loadOrderDetailsFromDTO(ordersDetails , orderDetailDTO);
                lines.add(ordersDetails);
            }
            List<OrdersDetails> linesDiff;
            int lineSize = 0;
            if (orders.getOrdersDetails() != null) {
                linesDiff = new ArrayList<OrdersDetails>(orders.getOrdersDetails());
                lineSize = orders.getOrdersDetails().size();
            } else {
                linesDiff = new ArrayList<OrdersDetails>();
            }

            // TODO le cas échéant ajouter le comparateur entre deux palette_detail
            linesDiff.removeAll(lines);
            if (lines.size() > lineSize || linesDiff.size() != 0) {
                retour = true;
                orders.setOrdersDetails(lines);
            }

            // Palettes si nécessaire
            // Par exemple si jamais le lien Palette/Commande ne remonte pas seul dans le cache
            // Dans la mesure où on est en cacheable alse pas de raison pour le moment

        }

        return retour;
    }

    @Override
    public OrderDTO getOrderDTO(Orders orders) {
        OrderDTO dto = new OrderDTO();
        dto.setId(orders.getId());
        dto.setStatusOrder(Integer.parseInt(orders.getStatus()));
        dto.setCreationDate(orders.getCreationDate());
        dto.setPlanifDate(orders.getPlanificationDate());
        dto.setPrepDate(orders.getPreparationDate());
        dto.setControlDate(orders.getControleDate());
        dto.setDelivDate(orders.getExpeditionDate());
        dto.setReceptDate(orders.getReceptionDate());
        dto.setReference(orders.getName());
        dto.setNote(orders.getNote());
        if (orders.getLocationTo() != null) {
            dto.setLocationTo(orders.getLocationTo().getAdapter(LocationsInfoDTO.class));
        }
        if (orders.getLocationFrom() != null) {
            dto.setLocationFrom(orders.getLocationFrom().getAdapter(LocationsInfoDTO.class));
        }
        if (orders.getCreationUser() != null) {
            dto.setUsernameId(orders.getCreationUserId());
            dto.setUsername(orders.getCreationUser().getLongName());
        }
        if (orders.getPlanificationUser() != null) {
            dto.setPlanifUserId(orders.getPlanificationUserId());
            dto.setPlanifUserName(orders.getPlanificationUser().getLongName());
        }
        if (orders.getPreparationUser() != null) {
            dto.setPrepUserId(orders.getPreparationUserId());
            dto.setPrepUserName(orders.getPreparationUser().getLongName());
        }
        if (orders.getControleUser() != null) {
            dto.setControlUserId(orders.getControleUserId());
            dto.setControlUserName(orders.getControleUser().getLongName());
        }
        if (orders.getExpeditionUser() != null) {
            dto.setDelivUserId(orders.getExpeditionUserId());
            dto.setDelivUserName(orders.getExpeditionUser().getLongName());
        }
        if (orders.getReceptionUser() != null) {
            dto.setReceptUserId(orders.getReceptionUserId());
            dto.setReceptUserName(orders.getReceptionUser().getLongName());
        }
        dto.setStatus(Orders.Status.getStatusLabel(orders.getStatus()));

        fillOrderDTO(dto);

        return dto;
    }
    
    void fillOrderDTO(OrderDTO dto){
        if(dto != null) {
            List<Object[]> results = ordersDetailsDAO.getOrderDetailDTO(dto.getId());
            for (Object[] objects : results) {
                OrderDetailDTO oDto = new OrderDetailDTO();
                oDto.setId(String.valueOf(objects[0]));
                oDto.setOrderId(dto.getId());
                ProductsResultDTO pDto = new ProductsResultDTO();
                pDto.setId(String.valueOf(objects[1]));
                pDto.setBarcode(String.valueOf(objects[2]));
                pDto.setLabel(String.valueOf(objects[3]));
                pDto.setCurrentPMPA(String.valueOf(objects[15]));
                
                if (objects[4] != null) {
                    pDto.setStockLocation(String.valueOf(objects[4]));
                } else {
                    pDto.setStockLocation(Products.EMPLACEMENT_INCONNU);
    
                }
                oDto.setProduct(pDto);
    
                oDto.setStockTo(Double.valueOf(String.valueOf(objects[5]==null ? "0": objects[5])));
                oDto.setStockFrom(Double.valueOf(String.valueOf(objects[6]==null ? "0":objects[6])));
                oDto.setQuantityAsked(Double.valueOf(String.valueOf(objects[7])));
                oDto.setQuantityToPrepare(Double.valueOf(String.valueOf(objects[8])));
                oDto.setQuantityPrepared(Double.valueOf(String.valueOf(objects[9])));
                oDto.setQuantitySent(Double.valueOf(String.valueOf(objects[10])));
                oDto.setQuantityReceived(Double.valueOf(String.valueOf(objects[11])));
    
                //Double quantityToPrepare = objects[8] !=null ?Double.valueOf(String.valueOf(objects[8])) :  null ;
                //Double qtAprepSurPalette = objects[12] !=null ?Double.valueOf(String.valueOf(objects[12])) :  null ;
    
                Double qtEnCoursDePrep = objects[13] !=null ?Double.valueOf(String.valueOf(objects[13])) :  0 ;
                Double qtAPrep = objects[14] !=null ?Double.valueOf(String.valueOf(objects[14])) :  null ;
                Double qtAPrepSansPalette = qtAPrep - qtEnCoursDePrep ;
                oDto.setQtAPrepSansPalette(qtAPrepSansPalette);
                // if (qtAprepSurPalette == null || (qtAprepSurPalette != null && quantityToPrepare < qtAprepSurPalette)) {
                if (qtAPrepSansPalette > 0)
                {
                    oDto.setCanBePaletized(true);
                } else {
                    oDto.setCanBePaletized(false);
                }
    
                oDto.setProductPaletteInfo((String)ordersDAO.findProductsPaletteInfo(dto.getId() , pDto.getId()));
    
                dto.getOrderDetails().add(oDto);
            }
    
            results = palettesDAO.getPaletteStatusInfoDTO(dto.getId());
            for (Object[] objects : results) {
                if (objects[0] != null) {
                    PaletteStatusInfoDTO pDto = new PaletteStatusInfoDTO();
                    pDto.setId(String.valueOf(objects[0]));
                    pDto.setStatusOrder(String.valueOf(objects[1]));
                    pDto.setName(String.valueOf(objects[2]));
                    pDto.setPreparationUsername(String.valueOf(objects[3]));
                    int nbLines = objects[4] != null ? Integer.valueOf(String.valueOf(objects[4])) : 0;
                    int nbLinesControle = objects[5] != null ? Integer.valueOf(String.valueOf(objects[5])) : nbLines;
                    pDto.setNbLines(nbLines);
                    if (String.valueOf(objects[1]) .equals("4")){ //palette terminée
                        pDto.setNbLinesLeft(nbLines - nbLinesControle);
                    }else{
                        pDto.setNbLinesLeft(nbLinesControle);
                    }
                    pDto.setStatusOrder(String.valueOf(objects[1]));
                    dto.getPalettes().add(pDto);
                }
            }
        }
    }    
    

    @Override
    public boolean loadFromDTO(Object source , Orders orders) {
        boolean retour = false;
        if (OrderDTO.class.isInstance(source)) {
            OrderDTO dto = (OrderDTO) source;

            if (!dto.getId().equals(orders.getId())) {
                retour = true;
                orders.setId(dto.getId());
            }
            if (!dto.getCreationDate().equals(orders.getCreationDate())) {
                retour = true;
                orders.setCreationDate(dto.getCreationDate());
            }
            if (!dto.getLocationFromId().equals(orders.getLocationFromId())) {
                retour = true;
                orders.setLocationFrom(locationsDAO.findById(dto.getLocationFromId()).orElse(null));
            }
            if (!dto.getLocationToId().equals(orders.getLocationToId())) {
                retour = true;
                orders.setLocationTo(locationsDAO.findById(dto.getLocationToId()).orElse(null));
            }
            if (!dto.getNote().equals(orders.getNote())) {
                retour = true;
                orders.setNote(dto.getNote());
            }
            if (!dto.getReference().equals(orders.getName())) {
                retour = true;
                orders.setName(dto.getReference());
            }
            if (dto.getStatusOrder() != Integer.parseInt(orders.getStatus())) {
                retour = true;
                orders.setStatus(String.valueOf(dto.getStatusOrder()));
            }
            if (!dto.getUsernameId().equals(orders.getCreationUserId())) {
                retour = true;
                orders.setCreationUser(peopleDAO.findById(dto.getUsernameId()).orElse(null));
            }
            if (!dto.getPlanifDate().equals(orders.getPlanificationDate())) {
                retour = true;
                orders.setPlanificationDate(dto.getPlanifDate());
            }
            if (!dto.getPlanifUserId().equals(orders.getPlanificationUserId())) {
                retour = true;
                orders.setPlanificationUser(peopleDAO.findById(dto.getPlanifUserId()).orElse(null));
            }
            if (!dto.getPrepDate().equals(orders.getPlanificationDate())) {
                retour = true;
                orders.setPlanificationDate(dto.getPrepDate());
            }
            if (!dto.getPrepUserId().equals(orders.getPreparationUserId())) {
                retour = true;
                orders.setPreparationUser(peopleDAO.findById(dto.getPrepUserId()).orElse(null));
            }
            if (!dto.getControlDate().equals(orders.getControleDate())) {
                retour = true;
                orders.setControleDate(dto.getControlDate());
            }
            if (!dto.getControlUserId().equals(orders.getControleUserId())) {
                retour = true;
                orders.setControleUser(peopleDAO.findById(dto.getControlUserId()).orElse(null));
            }
            if (!dto.getDelivDate().equals(orders.getExpeditionDate())) {
                retour = true;
                orders.setExpeditionDate(dto.getDelivDate());
            }
            if (!dto.getDelivUserId().equals(orders.getExpeditionUserId())) {
                retour = true;
                orders.setExpeditionUser(peopleDAO.findById(dto.getDelivUserId()).orElse(null));
            }
            if (!dto.getReceptDate().equals(orders.getReceptionDate())) {
                retour = true;
                orders.setReceptionDate(dto.getReceptDate());
            }
            if (!dto.getReceptUserId().equals(orders.getReceptionUserId())) {
                retour = true;
                orders.setReceptionUser(peopleDAO.findById(dto.getReceptUserId()).orElse(null));
            }

            ArrayList<OrdersDetails> lines = new ArrayList<OrdersDetails>();

            for (OrderDetailDTO orderDetailDTO : dto.getOrderDetails()) {
                OrdersDetails ordersDetails = new OrdersDetails();
                ordersDetails.setOrder(orders);
                loadOrderDetailsFromDTO(ordersDetails , orderDetailDTO);
                lines.add(ordersDetails);
            }
            List<OrdersDetails> linesDiff;
            int lineSize = 0;
            if (orders.getOrdersDetails() != null) {
                linesDiff = new ArrayList<OrdersDetails>(orders.getOrdersDetails());
                lineSize = orders.getOrdersDetails().size();
            } else {
                linesDiff = new ArrayList<OrdersDetails>();
            }

            // TODO le cas échéant ajouter le comparateur entre deux palette_detail
            linesDiff.removeAll(lines);
            if (lines.size() > lineSize || linesDiff.size() != 0) {
                retour = true;
                orders.setOrdersDetails(lines);
            }

            // Palettes si nécessaire
            // Par exemple si jamais le lien Palette/Commande ne remonte pas seul dans le cache
            // Dans la mesure où on est en cacheable alse pas de raison pour le moment

        }

        return retour;
    }

    //TODO pourquoi pas un update product emplacement ?
    //On met à jour les infos d'emplacement de la commande quand on l'ouvre si elle est en prépa
    //Une commande pouvant tout à fait êtré générée à un moment et l'emplacement de ramasse bouger avant que le produit ne soit ramassé

    @Override
    public List<OrderDetailOfbizDTO> getOrderDetailOfbizDTO(String id) {

        Orders order = ordersDAO.findById(id).orElse(null);
        List<OrderDetailOfbizDTO> retour = null ;
        if(order != null) {
            retour = new ArrayList<OrderDetailOfbizDTO>();

            List<OrdersDetails> ordersDetailsList = order.getOrdersDetails();
            String locationFrom = order.getLocationFromId();
            for(OrdersDetails ordersDetails : ordersDetailsList) {
                OrderDetailOfbizDTO dto = new OrderDetailOfbizDTO();
                dto.setId(ordersDetails.getId());
                dto.setOrderId(ordersDetails.getOrder().getName());
                ProductsResultDTO productDTO =  productsService.getAdapter(ordersDetails.getProduct() , ProductsResultDTO.class , true);
                productDTO.setStockLocation(ordersDetails.getProduct().getProductLocation(locationFrom));
                dto.setProduct(productDTO);        

                if (!ordersDetails.getOrder().getStatus().equals(Orders.Status.RECEPTED)) {
                    dto.setQuantitySent(ordersDetails.getQuantitySent());
                } else {
                    dto.setQuantitySent(ordersDetails.getQuantityReceived()-ordersDetails.getQuantitySent());
                }

                dto.setStore(applicationDAO.getOfbizStoreId(ordersDetails.getOrder().getLocationTo().getId()));
                dto.setClientId(applicationDAO.getOfbizClientId(ordersDetails.getOrder().getLocationTo().getId()));

                retour .add(dto);
            }
        }
        return retour;
    } 
    
    @Override
    public List<OrderDetailBLDTO> getOrderDetailBLDTO(String id) {
        List<OrderDetailBLDTO> retour = ordersDAO.getOrderDetailBLDTONativeQuery(id) ;
        
        return retour;
        
    }
    
    public static String createOrderId(String locationTo) {
        return String.format("%d-%s", new Date().getTime(), locationTo == null ? "" : locationTo);
    }
    
    public static String createPaletteId(String orderId) {
        return String.format("%d-%s", new Date().getTime(), orderId == null ? "" : orderId);
    }
    
    public static String createOrderDetailId(String locationTo , String productId) {
        return String.format("%d-%s-%s", new Date().getTime(), locationTo == null ? "" : locationTo , productId == null ? "" : productId);
    }
    
    public static String createPaletteDetailId(String orderDetailId) {
        return String.format("%d-%s", new Date().getTime(), orderDetailId == null ? "" : orderDetailId);
    }
    
    public OrderDetailDTO getOrderDetailDTO(OrdersDetails ordersDetails) {
        OrderDetailDTO dto = new OrderDetailDTO();
        dto.setId(ordersDetails.getId());
        dto.setOrderId(ordersDetails.getOrder().getId());
        ProductsResultDTO productDTO =  productsService.getAdapter( ordersDetails.getProduct() , ProductsResultDTO.class , true);
        productDTO.setStockLocation(ordersDetails.getProduct().getProductLocation(ordersDetails.getOrder().getLocationFrom().getId()));
        dto.setProduct(productDTO);        
        dto.setQuantityAsked(ordersDetails.getQuantityAsked());
        dto.setQuantityToPrepare(ordersDetails.getQuantityToPrepare());
        dto.setQuantityPrepared(ordersDetails.getQuantityPrepared());
        dto.setQuantitySent(ordersDetails.getQuantitySent());
        dto.setQuantityReceived(ordersDetails.getQuantityReceived());
        List<StockProductDetailDTO> froms = AdaptableHelper.getAdapter(stocksService.findListByProductAndLocation(ordersDetails.getProduct().getId() , ordersDetails.getOrder().getLocationFrom().getId() , null , null) , StockProductDetailDTO.class );
        dto.setStockFrom(froms.isEmpty() ? 0 : froms.get(0).getUnits());
        List<StockProductDetailDTO> tos = AdaptableHelper.getAdapter(stocksService.findListByProductAndLocation(ordersDetails.getProduct().getId() , ordersDetails.getOrder().getLocationTo().getId() , null , null) , StockProductDetailDTO.class );
        dto.setStockTo(tos.isEmpty() ? 0 : tos.get(0).getUnits());
        dto.setProductPaletteInfo((String) getProductsPaletteInfo(ordersDetails.getOrder().getId(), productDTO.getId()));
        
        dto.setCanBePaletized(false);
        if (!ordersDetails.getOrder().getStatus().equals(Orders.Status.CREATION)) {
            double qtyPalettized = ordersDetails.getOrder().getQuantityPalettized(ordersDetails.getId());
            double qtyLeft = ordersDetails.getQuantityToPrepare() - qtyPalettized;
            boolean hasPalettes = !getPalettes(ordersDetails.getOrder().getId()).isEmpty();
            
            if (hasPalettes && qtyLeft > 0) {
                dto.setCanBePaletized(true);
            }
        }

        return dto;

    }
    
    
    public boolean loadOrderDetailsFromDTO(OrdersDetails ordersDetails , Object source) {
        boolean retour = false;
        if (OrderDetailDTO.class.isInstance(source)) {
            OrderDetailDTO dto = (OrderDetailDTO) source;
            if (!dto.getId().equals(ordersDetails.getId())) {
                retour = true;
                ordersDetails.setId(dto.getId());
            }
            if (dto.getQuantityAsked() != ordersDetails.getQuantityAsked()) {
                retour = true;
                ordersDetails.setQuantityAsked(dto.getQuantityAsked());
            }
            if (dto.getQuantitySent() != ordersDetails.getQuantitySent()) {
                retour = true;
                ordersDetails.setQuantitySent(dto.getQuantitySent());
            }
            if (dto.getQuantityToPrepare() != ordersDetails.getQuantityToPrepare()) {
                retour = true;
                ordersDetails.setQuantityToPrepare(dto.getQuantityToPrepare());
            }
            if (!dto.getProductId().equals(ordersDetails.getProductId())) {
                retour = true;
                ordersDetails.setProduct(productsDAO.findById(dto.getProductId()).orElse(null));
            }

            // TODO repasser ici il manque quelques paramètres
            // Note
            // REceived

        }

        return retour;
    }
    
    public void addOrdersDetails(Palettes palettes , String orderDetailId, boolean fromOrder) {
        OrdersDetails ordersDetails = ordersDetailsDAO.findById(orderDetailId).orElse(null);

        // qté a ajouter sur la palette
        double qtyPalettized = ordersDetails.getOrder().getQuantityPalettized(orderDetailId);
        double qtySent = ordersDetails.getOrder().getQuantitySent(orderDetailId);
        double qtyReceived = ordersDetails.getOrder().getQuantityReceived(orderDetailId);

        boolean inPalette = false;
        for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
            if (palettesDetails.getOrderDetail().getId().equals(orderDetailId)) {
                // Si l'ajout provient de la commande, on repasse à la palette à l'état PREPARATION car l'opérateur devra ajouter un article
                // même s'il pense avoir déjà terminé le pdt
                if (fromOrder) {
                        //si status = fini -> Palettized = palettized - Prepared + toPrepare 
                        if (Double.valueOf(palettesDetails.getStatus()) > Double.valueOf(Orders.Status.PREPARATION)) {
                                qtyPalettized = qtyPalettized - palettesDetails.getQuantityPrepared() + palettesDetails.getQuantityToPrepare();
                        }
                    // Si le palette détail est en planification, le status du palette détail est obligatoirement en planification aussi,
                    // sinon il doit etre en préparation
                        
                        if (palettesDetails.getStatus().equals(Orders.Status.PLANIFICATION)) {
                        palettesDetails.setStatus(Orders.Status.PLANIFICATION);
                    } else {
                        palettesDetails.setStatus(Orders.Status.PREPARATION);
                    }
                    
                    palettesDetails.setQuantityToPrepare(ordersDetails.getQuantityToPrepare() - qtyPalettized + palettesDetails.getQuantityToPrepare());
                } else {
                    // On a ajouté directement un article sur la palette --> c'est a dire pendant l'expédition ou la reception
                    // On ne touche pas au status de la palette
                    if (palettes.getStatus().equals(Orders.Status.CONTROLE)) {
                        palettesDetails.setQuantitySent(ordersDetails.getQuantitySent() - qtySent + palettesDetails.getQuantitySent());
                    }
                    if (palettes.getStatus().equals(Orders.Status.EXPEDITION)) {
                        palettesDetails.setQuantityReceived(ordersDetails.getQuantityReceived() - qtyReceived
                                + palettesDetails.getQuantityReceived());
                    }
                }
                inPalette = true;
            }
        }

        if (!inPalette) {
            PalettesDetails palettesDetails = new PalettesDetails();
            palettesDetails.setOrderDetail(ordersDetails);
            palettesDetails.setPalette(palettes);
            palettesDetails.setStockLocationName(ordersDetails.getProduct()
                    .getProductLocation(ordersDetails.getOrder().getLocationFromId()));
            if (fromOrder) {
                // Si la palette est en planification, le status du palette détail est obligatoirement en planification aussi, sinon il doit
                // etre en préparation
                if (palettes.getStatus().equals(Orders.Status.PLANIFICATION)) {
                    palettesDetails.setStatus(Orders.Status.PLANIFICATION);
                } else {
                    palettesDetails.setStatus(Orders.Status.PREPARATION);
                }
                palettesDetails.setQuantityToPrepare(ordersDetails.getQuantityToPrepare() - qtyPalettized);
            } else {
                if (palettes.getStatus().equals(Orders.Status.CONTROLE)) {
                    palettesDetails.setStatus(Orders.Status.CONTROLE);
                    palettesDetails.setQuantitySent(ordersDetails.getQuantitySent() - qtySent);
                }
                if (palettes.getStatus().equals(Orders.Status.EXPEDITION)) {
                    palettesDetails.setStatus(Orders.Status.EXPEDITION);
                    palettesDetails.setQuantityReceived(ordersDetails.getQuantityReceived() - qtyReceived);
                }
            }
            palettesDetails.createId();
            palettes.getPalettesDetails().add(palettesDetails);
        }
        if (fromOrder) {
            // si la palette est en planification, il ne faut pas la passer a préparer
            if (!palettes.getStatus().equals(Orders.Status.PLANIFICATION)) {
                palettes.setStatus(Orders.Status.PREPARATION);
            }
        }
    }

    @Override
    public boolean loadPaletteFromDTO(Palettes palettes, Object source) {
      boolean retour = false;
      if (PaletteDTO.class.isInstance(source)) {
          PaletteDTO dto = (PaletteDTO) source;
          if (!dto.getOrderId().equals(palettes.getOrderId())) {
              retour = true;
              palettes.setOrder(ordersDAO.findById(dto.getOrderId()).orElse(null));
          }
          if (!dto.getName().equals(palettes.getName())) {
              retour = true;
              palettes.setName(dto.getName());
          }
          if (!dto.getNote().equals(palettes.getNote())) {
              retour = true;
              palettes.setNote(dto.getNote());
          }
          if (!dto.getPreparationUserId().equals(palettes.getPreparationUserId())) {
              retour = true;
              palettes.setPreparationUser(peopleDAO.findById(dto.getPreparationUserId()).orElse(null));
          }
          // Palette Details
          ArrayList<PalettesDetails> lines = new ArrayList<PalettesDetails>();

          /**
           * on génère la liste associée au dto
           * 
           */
          for (PaletteDetailDTO paletteDetailDTO : dto.getPaletteDetails()) {
              PalettesDetails paletteDetail = new PalettesDetails();
              paletteDetail.setPalette(palettes);
              paletteDetail.setQuantityReceived(paletteDetailDTO.getQuantityReceived());
              paletteDetail.setQuantitySent(paletteDetailDTO.getQuantitySent());
              paletteDetail.setQuantityToPrepare(paletteDetailDTO.getQuantityToPrepare());
              // paletteDetail.setStatus(paletteDetailDTO.getStatus());
              paletteDetail.setStockLocationName(paletteDetailDTO.getStockLocationName());
              paletteDetail.setOrderDetail(ordersDetailsDAO.findById(paletteDetailDTO.getOrderDetailId()).orElse(null));
              paletteDetail.setPreparationDate(paletteDetailDTO.getPreparationDate());
              if (paletteDetailDTO.getPreparationUserId() != null) {
                  paletteDetail.setPreparationUser(peopleDAO.findById(paletteDetailDTO.getPreparationUserId()).orElse(null));
              }
              
              if (paletteDetailDTO.getId() == null) {
                  paletteDetail.createId();
              } else {
                  paletteDetail.setId(paletteDetailDTO.getId());
              }
              paletteDetail.setNote(paletteDetailDTO.getNote());

              lines.add(paletteDetail);

          }

          /*
           * Vérifier si il y a du mouvement
           */
          /**
           * On récupère la liste actuelle
           */
          List<PalettesDetails> linesDiff;
          int lineSize = 0;
          if (palettes.getPalettesDetails() != null) {
              linesDiff = new ArrayList<PalettesDetails>(palettes.getPalettesDetails());
              lineSize = palettes.getPalettesDetails().size();
          } else {
              linesDiff = new ArrayList<PalettesDetails>();
          }

          // le cas échéant ajouter le comparateur entre deux palette_detail
          linesDiff.removeAll(lines);
          if (lines.size() > lineSize || linesDiff.size() != 0) {
              retour = true;
              palettes.setPalettesDetails(lines);
          }
          // check et ne pas exclure de devoir repasser ligne à ligne
          // Attention aux update des lignes !

      }

      return retour;
    }
    
    @Override
    public Object canDeleteOrder(UsernamePasswordAuthenticationToken token, String orderId) {
        // EDU 2020 09 - Déterminer la possibilité de supprimer une commande
        // Seule contrainte : pas plus avancé que préparation
        Orders order = ordersDAO.findByPrimaryKey(orderId);
        if (order.getStatus().equals(Orders.Status.CREATION)) {
            return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE));
        } else if (order.getStatus().equals(Orders.Status.PLANIFICATION)) {
            return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_PLANIFICATION));
        } else if (order.getStatus().equals(Orders.Status.PREPARATION)) {
            return token.getAuthorities().contains(new SimpleGrantedAuthority(Permissions.Commandes.PERM_COMMANDE_PREPARATION));
        }
        return false;
    }

    @Override
    public Object resyncJob(String orderId) {
        Job job = null;
        Orders order = ordersDAO.findById(orderId).orElse(null);
        job = JobHelper.initJobCommandePoussee();
        Boolean retour = false;
        if(job != null && order != null) {

            OrderDTO orderDto = getOrderDTO(order);
                try {
                    List<PaletteDTO> paletteDtos = AdaptableHelper.getAdapter(order.getPalettes() , PaletteDTO.class);
                    JobHelper.fillJobCommandes(job, orderDto, paletteDtos);
                    jobService.save(job);
                    retour = true;
                } catch (JsonProcessingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
        return retour;
    }
    
    /**
     * Fonction peu crade pour faciliter le filtrage des qté > 0
     * Héritage transport refact OrdersDAO
     */
    //TODO aviser car deux fonctiones similaires
    /*
    @Override
    public List<OrderDetailOfbizDTO> getOrderDetailOfbizDTO(String orderId) {
        Orders order = ordersDAO.findByPrimaryKey(orderId);
        List<OrderDetailOfbizDTO> results = new ArrayList<OrderDetailOfbizDTO>();
        for (OrdersDetails ordersDetails : order.getOrdersDetails()) {
            OrderDetailOfbizDTO dto = ordersDetails.getAdapter(OrderDetailOfbizDTO.class);
            if (dto != null && dto.getQuantitySent() != 0) {
                results.add(dto);
            }
        }
        return results;
    }
    */
    
    public ArrayList<OrderDetailIndexDTO> getOrderDetailIndexDTOList(Orders orders) {
        ArrayList<OrderDetailIndexDTO> retour = new ArrayList<OrderDetailIndexDTO>();
        String userId , userName ;
        if(Integer.parseInt(orders.getStatus()) > 2  && orders.getPalettes() != null) {
            for(Palettes palette : orders.getPalettes()) {
                for(PalettesDetails detail : palette.getPalettesDetails()) {
                    OrderDetailIndexDTO element = new OrderDetailIndexDTO();
                    element.setInternalPaletteDetailId(detail.getId());
                    element.setInternalOrderDetailId(detail.getOrderDetail().getId());
                    element.setInternalOrderId(orders.getId());
                    element.setInternalPaletteId(palette.getId());
                    element.setOrderId(orders.getName());
                    element.setPaletteId(palette.getName());
                    element.setStatus(Orders.Status.getStatusLabel(detail.getStatus()));
                    element.setOrigine(orders.getLocationFrom().getAdapter(LocationsInfoDTO.class));
                    element.setDestination(orders.getLocationTo().getAdapter(LocationsInfoDTO.class));
                    //orig
                    //dest

                    //produit
                    element.setProduct(productsService.getAdapter(detail.getOrderDetail().getProduct() , ProductsResultDTO.class , true));
                    element.setInputOrigine(detail.getInputOrigine());

                    //operateur
                    //date
                    userId = "";
                    userName = "";
                    switch(detail.getStatus()) {
                    case Orders.Status.CREATION :
                        userId = palette.getCreationUser().getId();
                        userName = palette.getCreationUser().getLongName();
                        element.setDate(palette.getCreationDate());
                        break;
                    case Orders.Status.PLANIFICATION :
                        userId = palette.getPlanificationUser().getId();
                        userName = palette.getPlanificationUser().getLongName();
                        element.setDate(palette.getControleDate());
                        break;
                    case Orders.Status.PREPARATION :
                        userId = palette.getPreparationUser() == null ?  "": palette.getPreparationUser().getId();
                        userName = palette.getPreparationUser() == null ?  "": palette.getPreparationUser().getLongName();
                        element.setDate(palette.getPreparationDate());
                        break;
                    case Orders.Status.CONTROLE :
                        userId = palette.getControleUser() == null ?  "": palette.getControleUser().getId();
                        userName = palette.getControleUser() == null ?  "": palette.getControleUser().getLongName();
                        element.setDate(palette.getPlanificationDate());
                        break;
                    case Orders.Status.EXPEDITION :
                        userId = palette.getExpeditionUser() == null ? palette.getExpeditionUser().getId() : palette.getExpeditionUser().getId();
                        userName = palette.getExpeditionUser() == null ? palette.getExpeditionUser().getLongName() : palette.getExpeditionUser().getLongName();
                        element.setDate(palette.getExpeditionDate() == null ? orders.getExpeditionDate() : palette.getExpeditionDate());
                        break;
                    case Orders.Status.RECEPTED :
                        userId = palette.getReceptionUser().getId();
                        userName = palette.getReceptionUser().getLongName();
                        element.setDate(palette.getReceptionDate());
                        break;
                    }

                    if(palette.getCreationUser()!= null){
                        element.setOrderUserId(palette.getCreationUser().getId());
                        element.setOrderUserName(palette.getCreationUser().getLongName());
                    }
                    element.setOrderDate(palette.getCreationDate());

                    if(palette.getPlanificationUser()!= null){
                        element.setValidationUserId(palette.getPlanificationUser().getId());
                        element.setValidationUserName(palette.getPlanificationUser().getLongName());
                    }
                    element.setValidationDate(palette.getControleDate());

                    if(palette.getPreparationUser()!= null || detail.getPreparationUser()!= null){
                        element.setPrepareUserId(detail.getPreparationUser() == null ? palette.getPreparationUser().getId() : detail.getPreparationUser().getId());
                        element.setPrepareUserName(detail.getPreparationUser() == null ? palette.getPreparationUser().getLongName() : detail.getPreparationUser().getLongName());
                    }
                    element.setPrepareDate(detail.getPreparationDate() == null ? palette.getPreparationDate() : detail.getPreparationDate());

                    if(palette.getExpeditionUser()!= null || palette.getControleUser()!= null){
                        element.setSendUserId(palette.getExpeditionUser() == null ? palette.getControleUser().getId() : palette.getExpeditionUser().getId());
                        element.setSendUserName(palette.getExpeditionUser() == null ? palette.getControleUser().getLongName() : palette.getExpeditionUser().getLongName());
                    }
                    element.setSendDate(palette.getExpeditionDate() == null ? palette.getPlanificationDate() : palette.getExpeditionDate());

                    if(palette.getReceptionUser()!= null){
                        element.setReceptUserId(palette.getReceptionUser().getId());
                        element.setReceptUserName(palette.getReceptionUser().getLongName());
                    }

                    element.setReceptDate(palette.getReceptionDate());

                    element.setUserId(userId);
                    element.setUserName(userName);
                    element.setQuantityAsked(detail.getOrderDetail().getQuantityAsked());
                    element.setQuantityToPrepare(detail.getOrderDetail().getQuantityToPrepare());
                    element.setQuantityPrepared(detail.getQuantityPrepared());
                    element.setQuantitySent(detail.getQuantitySent());
                    element.setQuantityReceived(detail.getQuantityReceived());
                    element.setNote(detail.getNote());
                    retour.add(element);
                }
            }
        } else {
            for(OrdersDetails detail : orders.getOrdersDetails()) {
                OrderDetailIndexDTO element = new OrderDetailIndexDTO();
                element.setInternalOrderDetailId(detail.getId());
                element.setInternalOrderId(orders.getId());
                element.setInternalPaletteId("");
                element.setInternalPaletteDetailId("");
                element.setOrderId(orders.getName());
                element.setStatus(Orders.Status.getStatusLabel(orders.getStatus()));

                //orig
                //dest
                element.setOrigine(orders.getLocationFrom().getAdapter(LocationsInfoDTO.class));
                element.setDestination(orders.getLocationTo().getAdapter(LocationsInfoDTO.class));
                //produit
                element.setProduct(productsService.getAdapter(detail.getProduct() , ProductsResultDTO.class , false));
                //operateur
                userId = "";
                userName = "";
                switch(orders.getStatus()) {
                case Orders.Status.CREATION :
                    userId = orders.getCreationUser().getId();
                    userName = orders.getCreationUser().getLongName();
                    element.setDate(orders.getCreationDate());
                    break;
                case Orders.Status.PLANIFICATION :
                    userId = orders.getPlanificationUser().getId();
                    userName = orders.getPlanificationUser().getLongName();
                    element.setDate(orders.getControleDate());
                    break;
                case Orders.Status.PREPARATION :
                    userId = orders.getPreparationUser().getId();
                    userName = orders.getPreparationUser().getLongName();
                    element.setDate(orders.getPreparationDate());
                    break;
                case Orders.Status.CONTROLE :
                    userId = orders.getControleUser().getId();
                    userName = orders.getControleUser().getLongName();
                    element.setDate(orders.getPlanificationDate());
                    break;
                case Orders.Status.EXPEDITION :
                    userId = orders.getExpeditionUser().getId();
                    userName = orders.getExpeditionUser().getLongName();
                    element.setDate(orders.getExpeditionDate());
                    break;
                case Orders.Status.RECEPTED :
                    userId = orders.getReceptionUser().getId();
                    userName = orders.getReceptionUser().getLongName();
                    element.setDate(orders.getReceptionDate());
                    break;
                }

                if(orders.getCreationUser() != null){
                    element.setOrderUserId(orders.getCreationUser().getId());
                    element.setOrderUserName(orders.getCreationUser().getLongName());
                }
                element.setOrderDate(orders.getCreationDate());

                if(orders.getPlanificationUser() != null){
                    element.setValidationUserId(orders.getPlanificationUser().getId());
                    element.setValidationUserName(orders.getPlanificationUser().getLongName());
                }
                element.setValidationDate(orders.getControleDate());

                if(orders.getPreparationUser() != null){
                    element.setPrepareUserId(orders.getPreparationUser().getId() );
                    element.setPrepareUserName(orders.getPreparationUser().getLongName() );
                }
                element.setPrepareDate(orders.getPreparationDate() );

                if(orders.getControleUser() != null || orders.getExpeditionUser() != null){
                    element.setSendUserId(orders.getExpeditionUser() == null ? orders.getControleUser().getId() : orders.getExpeditionUser().getId());
                    element.setSendUserName(orders.getExpeditionUser() == null ? orders.getControleUser().getLongName() : orders.getExpeditionUser().getLongName());
                }
                element.setSendDate(orders.getExpeditionDate() == null ? orders.getPlanificationDate() : orders.getExpeditionDate());

                if(orders.getReceptionUser() != null){
                    element.setReceptUserId(orders.getReceptionUser().getId());
                    element.setReceptUserName(orders.getReceptionUser().getLongName());
                }
                element.setReceptDate(orders.getReceptionDate());

                element.setUserId(userId);
                element.setUserName(userName);
                element.setQuantityAsked(detail.getQuantityAsked());
                element.setQuantityToPrepare(detail.getQuantityToPrepare());
                element.setQuantityPrepared(detail.getQuantityPrepared());
                element.setQuantitySent(detail.getQuantitySent());
                element.setQuantityReceived(detail.getQuantityReceived());
                element.setNote(detail.getNote());
                retour.add(element);
            }
        }
        return retour;
    }

}
