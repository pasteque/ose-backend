package com.ose.backend.service.tickets;

import java.util.List;

import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.service.tickets.ITicketsService;

import com.ose.backend.dto.customers.CustomerSimpleDTO;
import com.ose.backend.dto.tickets.TicketsIndexDTO;


public interface ITicketsServiceOSE extends ITicketsService{

    boolean loadFromDTO(Object source, Tickets tickets);

    List<TicketsHistoryDTO> findCustomerTickets(String customerId);

    <T extends ILoadable, ICsvResponse> List<T> searchElements(
            TicketsIndexDTO ticketDTO, Class<T> selector);

    CustomerSimpleDTO findTicketExtendedCustomer(String ticketId);

    void updateTicketExtended(long ticketId, String customerId);

    void updateTicketExtended(String ticketId, String customerId);

}
