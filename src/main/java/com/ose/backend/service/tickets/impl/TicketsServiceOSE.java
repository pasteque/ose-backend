package com.ose.backend.service.tickets.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.CellType;
import org.pasteque.api.dto.customers.TicketsHistoryDTO;
import org.pasteque.api.dto.generator.DtoSQLParameters;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.dto.tickets.POSTicketLinesDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.model.adaptable.ILoadable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.Receipts;
import org.pasteque.api.model.tickets.TaxLines;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.model.tickets.TicketsManager;
import org.pasteque.api.response.ICsvResponse;
import org.pasteque.api.service.tickets.impl.TicketsService;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.ComplexReturnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.customers.ICustomersDAO;
import com.ose.backend.dao.tickets.ITicketsDAO;
import com.ose.backend.dao.tickets.ITicketsExtendedDAO;
import com.ose.backend.dto.customers.CustomerSimpleDTO;
import com.ose.backend.dto.sales.SalesDayOverviewDTO;
import com.ose.backend.dto.sales.SalesLocationSessionOverviewDTO;
import com.ose.backend.dto.tickets.PaymentsIndexDTO;
import com.ose.backend.dto.tickets.TicketLinesIndexDTO;
import com.ose.backend.dto.tickets.TicketsIndexDTO;
import com.ose.backend.dto.tickets.TicketsResultDTO;
import com.ose.backend.model.tickets.TicketsExtended;
import com.ose.backend.service.tickets.ITicketsServiceOSE;
import com.ose.backend.specs.tickets.TicketsSpecifications;

@SuppressWarnings("unchecked")
@Service
@Primary
public class TicketsServiceOSE extends TicketsService implements ITicketsServiceOSE {

    protected static final Map<String, ComplexReturnType> returnTypesRulesOse;
    @Autowired
    private ITicketsDAO ticketsDAO;
    @Autowired
    private ICustomersDAO customersDAO;
    @Autowired
    private ITicketsExtendedDAO ticketsExtendedDAO;

    // Gestion des cas 'composés disponibles pour les recherches
    static {
        HashMap<String, ComplexReturnType> map = new HashMap<String, ComplexReturnType>();
        map.put("TicketsResultDTO", new ComplexReturnType("TicketsResultDTO", "TicketsResultDTO", "tickets",
                TicketsResultDTO.types, null, null, null));
        map.put("TicketLinesIndexDTO", new ComplexReturnType("TicketLinesIndexDTO", "TicketLinesIndexDTO",
                "ticketsLinesDetails", TicketLinesIndexDTO.types, null, null, null));
        map.put("TicketsIndexDTO", new ComplexReturnType("TicketsIndexDTO", "TicketsIndexDTO", "ticketsDetails",
                TicketsIndexDTO.types, null, null, null));
        map.put("PaymentsIndexDTO",
                new ComplexReturnType("TicketsIndexDTO", "TicketsIndexDTO", "ticketsDetailsAndPayments",
                        TicketsIndexDTO.types, new Class[] { PaymentsIndexDTO.class }, new String[] { "incomeDetails" },
                        new CellType[][] { PaymentsIndexDTO.types }));
        map.put("SalesLocationSessionOverviewDTO",
                new ComplexReturnType("SalesLocationSessionOverviewDTO", "SalesLocationSessionOverviewDTO",
                        "DetailsDeballages", SalesLocationSessionOverviewDTO.types, null, null, null));
        map.put("SalesDayOverviewDTO", new ComplexReturnType("SalesDayOverviewDTO", "SalesDayOverviewDTO",
                "DetailsJoursVente", SalesDayOverviewDTO.types, null, null, null));
        returnTypesRulesOse = Collections.unmodifiableMap(map);
    }

    @Override
    public boolean loadFromDTO(Object source, Tickets tickets) {
        boolean retour = false;
        if (POSTicketsDTO.class.isInstance(source)) {
            POSTicketsDTO dto = (POSTicketsDTO) source;

            if (dto.getNote() != tickets.getNote()) {
                retour = true;
                tickets.setNote(dto.getNote());
            }
            if (dto.getType() != tickets.getTicketType()) {
                retour = true;
                tickets.setTicketType(dto.getType());
            }
            if (dto.getCustCount() != tickets.getCustCount()) {
                retour = true;
                tickets.setCustCount(dto.getCustCount());
            }
            if (dto.getDiscountRate() != tickets.getDiscountRate()) {
                retour = true;
                tickets.setDiscountRate(dto.getDiscountRate());
            }
            if (dto.getTicketId() != tickets.getTicketId()) {
                retour = true;
                tickets.setTicketId(dto.getTicketId());
            }

            if ((dto.getUserId() != null && !dto.getUserId().equals(tickets.getUserId()))
                    || (dto.getUserId() == null && tickets.getUserId() != null)) {
                retour = true;
                tickets.setPerson(peopleApiDAO.findById(dto.getUserId()).orElse(null));
            }
            if ((dto.getCustomerId() != null && !dto.getCustomerId().equals(tickets.getCustomerId()))
                    || (dto.getCustomerId() == null && tickets.getCustomerId() != null)) {
                retour = true;
                tickets.setCustomer(customersApiDAO.findById(dto.getCustomerId()).orElse(null));
            }
            if ((dto.getDiscountProfileId() != null
                    && !dto.getDiscountProfileId().equals(tickets.getDiscountProfileId()))
                    || (dto.getDiscountProfileId() == null && tickets.getDiscountProfileId() != null)) {
                retour = true;
                tickets.setDiscountProfile(discountProfileDAO.findById(dto.getDiscountProfileId()).orElse(null));
            }
            if ((dto.getTariffAreaId() != null && !dto.getTariffAreaId().equals(tickets.getTariffAreaId()))
                    || (dto.getTariffAreaId() == null && tickets.getTariffAreaId() != null)) {
                retour = true;
                tickets.setTariffArea(tariffAreasApiDAO.findById(dto.getTariffAreaId()).orElse(null));
            }
            if ((dto.getInseeNum() != null && !dto.getInseeNum().equals(tickets.getInseeId()))
                    || (dto.getInseeNum() == null && tickets.getInseeId() != null)) {
                retour = true;
                tickets.setCustomerZipCode(inseeApiDAO.findById(dto.getInseeNum()).orElse(null));
            }

            if (dto.getParentTicketId() != null && dto.getParentTicketId().equals(tickets.getParentId())) {
                List<Tickets> parent = null;
                parent = ticketsApiDAO.findByCriteria(dto.getParentTicketId().toString(), null, null, null, null, null,
                        null, null, null, null, null, null, null);
                tickets.setParent(parent == null || parent.size() == 0 ? null : parent.get(0));
            }

            if (dto.getParentTicketId() == null && tickets.getParentId() != null) {
                retour = true;
                tickets.setParent(null);
            }

            // Lien avec les receipts
            /*
             * Receipts : id = id
             * 
             * money = cashId date_new = max (date) Attributes ??
             */
            if (tickets.getReceipts() == null && dto.getCashId() != null) {
                retour = true;
                tickets.setReceipts(new Receipts());
            }
            if ((dto.getCashId() != null && !dto.getCashId().equals(tickets.getReceipts().getClosedCashId()))
                    || (dto.getCashId() == null && tickets.getReceipts().getClosedCashId() != null)) {
                retour = true;
                tickets.getReceipts().setClosedCash(closedCashApiDAO.findById(dto.getCashId()).orElse(null));
            }
            if ((dto.getDate() != 0) && ((tickets.getReceipts().getDateNew() == null)
                    || dto.getDatetime().after(tickets.getReceipts().getDateNew()))) {
                retour = true;
                tickets.getReceipts().setDateNew(dto.getDatetime());
            }
            if (dto.getDate() != 0L && dto.getDate() != tickets.getDate().getTime()){
                retour = true;
                tickets.setDate(new Date(dto.getDate()));
            }
            if ((dto.getId() != null && !dto.getId().equals(tickets.getReceipts().getId()))
                    || (dto.getId() == null && tickets.getReceipts().getId() != null)) {
                retour = true;
                tickets.getReceipts().setId(dto.getId());
            }

            /**
             * Gérer les lignes de vente
             */

            /**
             * Gérer les lignes de taxes -> receipt
             */

            List<TicketLines> lines = new ArrayList<TicketLines>();
            List<TaxLines> taxesLines = new ArrayList<TaxLines>();
            for (POSTicketLinesDTO ticketLinesDTO : dto.getLines()) {
                Products products = productsApiDAO.findById(ticketLinesDTO.getProductId()).orElse(null);
                List<Tickets> parent = null;
                TicketLines parentLine = null;
                TicketLines ticketLines = new TicketLines();

                ticketLines.setLine(ticketLinesDTO.getDispOrder());
                ticketLines.setProduct(products);
                ticketLines.setAttributes(ticketLinesDTO.getAttributes());
                ticketLines.setDiscountRate(ticketLinesDTO.getDiscountRate());
                ticketLines.setPrice(ticketLinesDTO.getPrice());
                if (ticketLinesDTO.getVatId() == null) {
                    // TODO EDU intercepter ce cas
                    // throw new APIException("Unknown tax");
                }
                Taxes tax = taxesApiDAO.findById(ticketLinesDTO.getVatId()).orElse(null);
                if (tax == null) {
                    // TODO EDU intercepter ce cas
                    // throw new APIException("Unknown tax");
                }
                ticketLines.setTax(tax);
                ticketLines.setUnits(ticketLinesDTO.getQuantity());
                ticketLines.setTicket(tickets);
                ticketLines.setType(
                        (!ticketLinesDTO.isOrder() && ticketLinesDTO.getType() == 0 ? 'S' : ticketLinesDTO.getType()));
                if (ticketLinesDTO.getTariffAreaId() != null) {
                    ticketLines
                    .setTariffArea(tariffAreasApiDAO.findById(ticketLinesDTO.getTariffAreaId()).orElse(null));
                }
                ticketLines.setNote(ticketLinesDTO.getNote());

                if (ticketLinesDTO.getParentLine() != null && ticketLinesDTO.getParentId() != null) {
                    parent = ticketsApiDAO.findByCriteria(ticketLinesDTO.getParentId().toString(), null, null, null,
                            null, null, null, null, null, null, null, null, null);
                    if (parent != null && parent.size() > 0) {
                        for (int i = 0; i < parent.get(0).getLines().size(); i++) {
                            if (parent.get(0).getLines().get(i).getLine() == ticketLinesDTO.getParentLine()) {
                                parentLine = parent.get(0).getLines().get(i);
                                i = parent.get(0).getLines().size();
                            }
                        }
                    }
                }
                ticketLines.setParentLine(parentLine);
                if (parentLine == null && ticketLinesDTO.getParentLine() != null
                        && ticketLinesDTO.getParentId() != null) {
                    ticketLines.setParentLineNumber(ticketLinesDTO.getParentLine());
                    ticketLines.setParentTicketId(ticketLinesDTO.getParentId());

                }

                lines.add(ticketLines);

                // TRAITEMENT DE LA TVA
                Taxes vat = taxesApiDAO.findById(ticketLinesDTO.getVatId()).orElse(null);
                double discount = ticketLinesDTO.getDiscountRate() + dto.getDiscountRate();
                double vatAmount = ticketLinesDTO.getPrice() * (1D - discount) * ticketLinesDTO.getQuantity();

                TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), vat, vatAmount);

                // TRAITEMENT DES AUTRES TAXES : DEEE, ECOMOB, ...
                if (ticketLinesDTO.getTaxes() != null) {
                    for (String taxId : ticketLinesDTO.getTaxes()) {
                        Taxes eco = taxesApiDAO.findById(taxId).orElse(null);
                        double ecoAmount = (eco.getAmount() / (1D + vat.getRate())) * ticketLinesDTO.getQuantity();
                        tickets.getTicketsManager();
                        // Ajout de la taxe DEEE, ECOMOB, ...
                        TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), eco, ecoAmount);
                        tickets.getTicketsManager();
                        // Ajout de la TVA sur la taxe DEEE, ECOMOB, ...
                        TicketsManager.addTaxesLines(taxesLines, tickets.getReceipts(), vat, ecoAmount);
                    }
                }
            }

            /*
             * Vérifier si il y a du mouvement
             */
            List<TicketLines> linesDiff;
            int lineSize = 0, taxesSize = 0;
            if (tickets.getLines() != null) {
                linesDiff = new ArrayList<TicketLines>(tickets.getLines());
                lineSize = tickets.getLines().size();
            } else {
                linesDiff = new ArrayList<TicketLines>();
            }
            List<TaxLines> taxesLinesFiff;
            if (tickets.getReceipts() != null && tickets.getReceipts().getTaxes() != null) {
                taxesLinesFiff = new ArrayList<TaxLines>(tickets.getReceipts().getTaxes());
                taxesSize = tickets.getReceipts().getTaxes().size();
            } else {
                taxesLinesFiff = new ArrayList<TaxLines>();
            }
            linesDiff.removeAll(lines);
            taxesLinesFiff.removeAll(taxesLines);
            if (lines.size() > lineSize || linesDiff.size() != 0) {
                retour = true;
                tickets.setLines(lines);
            }
            if (taxesLines.size() > taxesSize || taxesLinesFiff.size() != 0) {
                retour = true;
                tickets.getReceipts().setTaxes(taxesLines);
            }

            /**
             * Gérer les lignes de payment -> receipt
             */

            List<Payments> payments = new ArrayList<Payments>();
            for (POSPaymentsDTO paymentsDTO : dto.getPayments()) {
                Payments payment = new Payments();
                // Danger !
                if (paymentsDTO.getId() == null) {
                    paymentsDTO.setId(Long.toString(new Date().getTime()));
                }
                payment.setId(paymentsDTO.getId());
                payment.setTickets(tickets);
                payment.setReceipts(tickets.getReceipts());
                payment.setPayment(paymentsDTO.getType());
                payment.setTotal(paymentsDTO.getAmount());
                if (paymentsDTO.getCurrencyId() != null) {
                    payment.setCurrency(currenciesApiDAO.findById(paymentsDTO.getCurrencyId()).orElse(null));
                }
                payment.setTotalCurrency(paymentsDTO.getCurrencyAmount());
                payment.setNote(paymentsDTO.getNote());
                payment.setReturnMessage(paymentsDTO.getReturnMessage());
                payment.setTransId(paymentsDTO.getTransId());
                if (paymentsDTO.getEcheance() != null) {
                    payment.setEcheance(new Date(paymentsDTO.getEcheance()));
                }
                payments.add(payment);

            }
            /**
             * vérifier si il y a du mouvement
             */
            List<Payments> paymentsDiff;
            int paymentSize = 0;
            if (tickets.getReceipts().getPayments() != null) {
                paymentsDiff = new ArrayList<Payments>(tickets.getReceipts().getPayments());
                paymentSize = tickets.getReceipts().getPayments().size();
            } else {
                paymentsDiff = new ArrayList<Payments>();
            }
            paymentsDiff.removeAll(payments);
            if (payments.size() > paymentSize || paymentsDiff.size() != 0) {
                retour = true;
                tickets.getReceipts().setPayments(payments);
            }

        }

        return retour;
    }

    @Override
    public ComplexReturnType getTicketReturnInfos(String resultType) {
        ComplexReturnType returnInfos = returnTypesRules.get(resultType);
        if (returnInfos == null) {
            returnInfos = returnTypesRulesOse.get(resultType);
        }
        if (returnInfos == null) {
            returnInfos = returnTypesRules.get("TicketsDTO");
        }
        return returnInfos;
    }

    @Override
    public <T extends ICsvResponse> List<T> search(String ticketId, String ticketType, String cashId, String dateStart,
            String dateStop, String customerId, String userId, String locationId, String locationCategoryName,
            String saleLocationId, String tourId, String fromValue, String toValue, Boolean orderConstraint,
            String resultType) {
        // resultType = "com.ose.dto.tickets." + resultType;
        boolean validated = false;
        Class<? extends ILoadable> selector = POSTicketsDTO.class;
        if (resultType != null
                && ("com.ose.backend.dto.tickets." + resultType).equals(TicketsResultDTO.class.getName())) {
            selector = TicketsResultDTO.class;
            validated = true;
        }
        if (resultType != null
                && ("com.ose.backend.dto.tickets." + resultType).equals(TicketsIndexDTO.class.getName())) {
            selector = TicketsIndexDTO.class;
            validated = true;
        }
        if (resultType != null && ("com.ose.backend.dto.sales." + resultType)
                .equals(SalesLocationSessionOverviewDTO.class.getName())) {
            selector = SalesLocationSessionOverviewDTO.class;
            validated = true;
        }
        if (resultType != null
                && ("com.ose.backend.dto.sales." + resultType).equals(SalesDayOverviewDTO.class.getName())) {
            selector = SalesDayOverviewDTO.class;
            validated = true;
        }
        if (resultType != null
                && ("com.ose.backend.dto.tickets." + resultType).equals(TicketLinesIndexDTO.class.getName())) {
            selector = TicketLinesIndexDTO.class;
            validated = true;
        }
        if (validated) {
            DtoSQLParameters model = ticketsApiDAO.findByCriteriaDTO(ticketId, ticketType, cashId, dateStart, dateStop,
                    customerId, userId, locationId, locationCategoryName, saleLocationId, tourId, fromValue, toValue,
                    orderConstraint, selector);
            return model == null ? null : (List<T>) model.getResult(selector);
        } else {
            return super.search(ticketId, ticketType, cashId, dateStart, dateStop, customerId, userId, locationId,
                    locationCategoryName, saleLocationId, tourId, fromValue, toValue, orderConstraint, resultType);
        }
    }

    @Override
    public List<TicketsHistoryDTO> findCustomerTickets(String customerId) {
        List<TicketsHistoryDTO> retour = ticketsDAO.findCustomerTickets(customerId);
        return retour;
    }

    @Override
    public CustomerSimpleDTO findTicketExtendedCustomer(String ticketId) {
        CustomerSimpleDTO retour = customersDAO.findSimpleByTicketId(ticketId);
        return retour;
    }

    @Override
    public void updateTicketExtended(long ticketId, String customerId) {
        TicketsSpecifications spec = new TicketsSpecifications();
        spec.add(new SearchCriteria(Tickets.Fields.TICKET_ID, ticketId, SearchOperation.EQUAL));
        Tickets ticket = ticketsDAO.findOne(spec).orElse(null);
        if (ticket != null) {
            updateTicketExtended(ticket.getId(), customerId);
        }
    }

    @Override
    public void updateTicketExtended(String ticketId, String customerId) {
        // Enrichissement des données
        TicketsExtended ticketsExtended = ticketsExtendedDAO.findById(ticketId).orElse(null);
        if (customerId == null) {
            if (ticketsExtended != null) {
                ticketsExtendedDAO.delete(ticketsExtended);
            }
        } else {
            if (ticketsExtended == null) {
                ticketsExtended = new TicketsExtended();
                ticketsExtended.setTicketId(ticketId);
            }
            ticketsExtended.setCustomerId(customerId);
            ticketsExtendedDAO.save(ticketsExtended);
        }

    }

    @SuppressWarnings("hiding")
    @Override
    public <T extends ILoadable, ICsvResponse> List<T> searchElements(TicketsIndexDTO ticketDTO, Class<T> selector) {

        // TODO Danger il faut appliquer à chaque element un setticketindexdto
        return ticketsDAO.findByTicketIndexDTO(ticketDTO, selector).getResult(selector);

    }

}
