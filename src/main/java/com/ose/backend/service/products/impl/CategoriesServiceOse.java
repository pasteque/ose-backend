package com.ose.backend.service.products.impl;

import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.ose.backend.service.products.ICategoriesServiceOse;
import com.ose.backend.specs.products.CategoriesSpecification;

@Service
public class CategoriesServiceOse implements ICategoriesServiceOse {

    @Autowired
    private ICategoriesApiDAO categoriesDAO;

    @Override
    public List<POSCategoriesDTO> getByCriteria(String reference, Integer limit) {
            
            
            GenericSpecification<Categories> specsName  = new GenericSpecification<Categories>();
            specsName.add(new SearchCriteria(Categories.Fields.NAME ,  reference , SearchOperation.MATCH));
            GenericSpecification<Categories> specsId  = new GenericSpecification<Categories>();
            specsId.add(new SearchCriteria(Categories.Fields.ID ,  reference , SearchOperation.MATCH));
            
            Sort sortByOrder = Sort.by(Categories.Fields.DISPLAY_ORDER).ascending() ;
            Sort sortByName = Sort.by(Categories.Fields.NAME).ascending() ;
            
            Specification<Categories> distinct = CategoriesSpecification.distinct();
            
            List<Categories> l = new ArrayList<Categories>();
            if(limit != null && limit > 1) { 

                Page<Categories> p = categoriesDAO.findAll(distinct.and(specsName.or(specsId)) , PageRequest.of(0, limit , sortByOrder.and(sortByName)) );
                l = p.getContent();
            } else {
                l = categoriesDAO.findAll(distinct.and(specsName.or(specsId)) , sortByOrder.and(sortByName) );
            }

        return AdaptableHelper.getAdapter(l, POSCategoriesDTO.class);
    }

}
