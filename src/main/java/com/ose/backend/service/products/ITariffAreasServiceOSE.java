package com.ose.backend.service.products;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dto.products.TariffAreasLocationsDTO;

public interface ITariffAreasServiceOSE {
    
//    List<TariffAreasIndexDTO> getByCriteria(String reference);
//
//    <T> List<T> getCurrentTariffAreas(String locationId, Class<T> selector);
//
//    TariffAreasDTO getTariffAreaById(int id);
//
//    TariffAreasDTO getTariffAreaByName(String name);
//
//    Object save(List<TariffAreasDTO> l);
//

    Object saveLink(TariffAreasLocationsDTO TariffAreasDTO);
    Object deleteLink(TariffAreasLocationsDTO TariffAreasDTO);

    void createJobs(TariffAreasLocationsDTO dto, String action);

    <T> List<T> getCurrentTariffAreasLinks(String locationId,
            String tariffAreaId, String categoryName ,String tarifOrder, Date from, Date to,
            Class<T> selector);

}
