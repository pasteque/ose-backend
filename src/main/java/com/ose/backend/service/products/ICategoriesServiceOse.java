package com.ose.backend.service.products;

import java.util.List;

import org.pasteque.api.dto.products.POSCategoriesDTO;


public interface ICategoriesServiceOse {

    List<POSCategoriesDTO> getByCriteria(String reference, Integer limit);

}
