package com.ose.backend.service.products.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.POSProductsDTO;
import org.pasteque.api.model.products.ProductReferences;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.service.products.IProductsService;
import org.pasteque.api.specs.GenericSpecification;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.products.IProductsDAO;
import com.ose.backend.dao.stocks.IStockDiaryDAO;
import com.ose.backend.dao.tickets.ITicketLinesDAO;
import com.ose.backend.dto.application.MessagesInfoDTO;
import com.ose.backend.dto.products.ProductsCompoundDetailDTO;
import com.ose.backend.dto.products.ProductsDetailDTO;
import com.ose.backend.dto.products.ProductsDetailLocationDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.products.ProductsResultDTO.PricesChange;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.service.application.IMessagesService;
import com.ose.backend.service.products.IProductsServiceOSE;
import com.ose.backend.service.stock.IStocksServiceOse;

@Service
public class ProductsServiceOSE implements IProductsServiceOSE {

    @Autowired
    private IProductsDAO productsDAO;
    @Autowired
    private IProductsService productsService;
    @Autowired
    private IStocksServiceOse stockService;
    @Autowired
    private IMessagesService messageService;
    @Autowired
    private ILocationsApiDAO locationsDAO;
    @Autowired
    private IStockDiaryDAO stockDiaryDAO;
    @Autowired
    private ITicketLinesDAO ticketLinesDAO;
    
    // private ArrayList<HashMap<String, Double>> allInfosNbTotSell = null;
    //private ArrayList<HashMap<String, Double>> allInfosNbSell = null;
    //private boolean batched = false;

    @Override
    public Object getByCriteria(String id, String code, String reference, String _type) {
        if(id == null && code == null && reference == null && _type == null) {
            return null;
        }
        GenericSpecification<Products> specs = new GenericSpecification<Products>();
        specs.add(new SearchCriteria(Products.Fields.ID ,  id , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Products.Fields.CODE ,  code , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Products.Fields.REFERENCE ,  reference , SearchOperation.EQUAL));
        
        Products product = productsDAO.findOne(specs).orElse(null);
        if( product == null ) {
            return null;
        }
        if (_type != null) {
                return getAdapter(product, ProductsDetailDTO.class , !"ProductsDetailDTO".equals(_type));
        }
        return product.getAdapter(POSProductsDTO.class);
    }

    public void setPriceRange(ProductsResultDTO dto , Products products) {
        Double minPrice = 9999D;
        Double maxPrice = -9999D;
        Double current;

        // TODO on fait le tour des locations
        for (Locations locations : locationsDAO.findAll()) {
            current = productsService.getFullPrice(locations , products);
            if (current < minPrice) {
                minPrice = current;
            }
            if (current > maxPrice) {
                maxPrice = current;
            }
        }

        if (maxPrice > minPrice) {
            dto.setMaxPriceSell(Double.toString(maxPrice));
        }

        dto.setPriceSell(Double.toString(minPrice));
    }
    
    public PricesChange recentPriceChange() {
        // TODO connaitre le plus récent changement de prix et son sens
        // si default location on regarde
        // sinon on fait le tour de toutes
        // On pose un hierarchie dans ce deuxième cas RAS < baisse à venir < hausse recente < hausse à venir < baisse recente
        return PricesChange.RAS;
    }

    @Override
    public List<ProductsResultDTO> getByCriteria(String reference, String category, Integer limit, Boolean composition) {
        
        List<ProductsResultDTO> retour = productsDAO.findByCriteriaDTO(reference, category, limit, composition);
        
        if ( limit == null || retour.size() < limit) {
            
            Integer newlimit = ( limit == null ) ? null : limit - retour.size();
            retour.addAll(getAdapter(productsDAO.findByNameSplitted( reference,  newlimit,  composition), ProductsResultDTO.class , true));
        }
        
        return retour;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter( Products item , Class<T> selector , Boolean filtered) {
        if(selector.equals(ProductsResultDTO.class)) {
            ProductsResultDTO dto = new ProductsResultDTO();
            dto.setId(item.getId());
            dto.setBarcode(item.getCode());
            dto.setLabel(item.getName());
            dto.setStockLocation(item.getProductLocation(""));
            dto.setComposed(!item.getComponentsProducts().isEmpty());
            
            if(!filtered) {
                dto.setPriceSell(Double.toString(productsService.getDefaultLocationFullPrice(item)));
                if (dto.getPriceSell() == null || Double.parseDouble(dto.getPriceSell()) == -1D) {
                    setPriceRange(dto,item);
                }
            
                // TODO : Complement d'infos
                dto.setActive(isActive(item));
                dto.setPriceChange(recentPriceChange());
            }
            if (item.getCategory() != null) {
                dto.setCategoryLabel(item.getCategory().getName());
            }
            dto.setCurrentPMPA(Double.toString(item.getStockCost() == null ? 0.01D : item.getStockCost()));

        return (T) dto ;
        } else if (selector.equals(ProductsDetailDTO.class)) {

            ProductsDetailDTO dto = new ProductsDetailDTO();
            dto.setCode(item.getCode());
            if (item.getCategory() != null) {
                dto.setCategoryId(item.getCategory().getId());
                dto.setCategoryLabel(item.getCategory().getName());
            }
            // if (getTaxCategory() != null) {
            // dto.setTaxCatId(getTaxCategory().getId());
            // dto.setTaxCatLabel(getTaxCategory().getName());
            // }
            dto.setLabel(item.getName());
            List<String> taxCategLabel = new ArrayList<String>();
            for (TaxCategories taxCategories : item.getTaxCategories()) {
                taxCategLabel.add(taxCategories.getId() + " - " + taxCategories.getName());
            }
            // dto.setTaxCategoriesLabel(taxCategLabel);
            List<String> barcodes = new ArrayList<String>();
            for (ProductReferences elements : item.getReferences()) {
                barcodes.add(elements.getReference());
            }
            dto.setBarcodes(barcodes);
            dto.setMessages(messageService.getAdapter(item.getAvailableMessages(), MessagesInfoDTO.class , false));
            dto.setActive(true);
            if (!filtered) {
                dto.setDetails(getProductsDetails(item));
                // ici pour l'inactivité en cas de besoin
                dto.setActive(false);
                for (ProductsDetailLocationDTO detail : dto.getDetails()) {
                    dto.setActive(dto.isActive() || detail.isActive());
                }
            }

            if (item.getComponentsProducts().isEmpty()) {
                dto.setHasComponents(false);
                dto.setCompounds(getCompoundProducts(item));
            } else {
                dto.setHasComponents(true);
                dto.setCompounds(getComponentsProducts(item));
            }
            dto.setStockable(item.isStockable());
            return (T) dto;
        } 
        
        return null;
    }
    
    @Override
    public <T> List<T> getAdapter( List<Products> liste , Class<T> selector , Boolean filtered) {
        List<T> listDTO = new ArrayList<>();
        for (Products adaptable : liste) {
            listDTO.add((T) getAdapter(adaptable , selector , filtered));
        }
        return listDTO;
    }
    
    public Boolean isActive(Products product) {
        // TODO produit actif ?
        // si default location faut un tariffaire
        // sinon il faut un tariffaire pour chaque location 'parente'
        Boolean retour = true;
        return retour;
    }
    
    public List<ProductsCompoundDetailDTO> getComponentsProducts(Products products) {
        List<ProductsCompoundDetailDTO> list = new ArrayList<>();
        for (ProductsComponents productsComponents : products.getComponentsProducts()) {
            Products compound = productsComponents.getComponent();
            ProductsCompoundDetailDTO compoundDetailDTO = new ProductsCompoundDetailDTO();
            compoundDetailDTO.setCompoundId(compound.getId());
            compoundDetailDTO.setCompoundCode(compound.getCode());
            compoundDetailDTO.setCompoundLabel(compound.getName());
            compoundDetailDTO.setQuantity(productsComponents.getQuantity());
            list.add(compoundDetailDTO);
        }
        return list;
    }
    
    public List<ProductsCompoundDetailDTO> getCompoundProducts(Products products) {
        List<ProductsCompoundDetailDTO> list = new ArrayList<>();
        for (ProductsComponents productsComponents : products.getMastersProducts()) {
            Products compound = productsComponents.getMaster();
            ProductsCompoundDetailDTO compoundDetailDTO = new ProductsCompoundDetailDTO();
            compoundDetailDTO.setCompoundId(compound.getId());
            compoundDetailDTO.setCompoundCode(compound.getCode());
            compoundDetailDTO.setCompoundLabel(compound.getName());
            compoundDetailDTO.setQuantity(productsComponents.getQuantity());
            list.add(compoundDetailDTO);
        }
        return list;
    }
    
    public List<ProductsDetailLocationDTO> getProductsDetails(Products products) {
        List<ProductsDetailLocationDTO> details = new ArrayList<ProductsDetailLocationDTO>();
        Locations location = locationsDAO.findTopByServerDefaultTrue();

        // TODO EDU Cas de pas de location par defaut
        // On devrait alors avoir une liste de chaque location
        if (location != null && !location.getId().equals(Locations.SIEGE_ID)) {
            fillDetailList(details, location, products, null, null);
        } else {
            List<Locations> list = locationsDAO.findAll();
            // getAllLocationsData( 30);
            // getAllLocationsData( 7);
            //batched = true;
            // EDU ce boolean va nous éviter de faire à nouveau le tour magasin par magasin si on ne dispose d'aucune donnée
            ArrayList<ArrayList<HashMap<String, Double>>> allInfos =  getAllLocationsData(7, 30, products);
            // TODO Voir si on peut accélérer le bazard en calculant en bloc pour tous les produits enfants
            // mais pas ainsi ( besoin de stocker l'objet manager) ou de mettre en cache pour chaque produit
            // if (!products.getComponentsProducts().isEmpty()) {
            // for (ProductsComponents components : products.getComponentsProducts()) {
            // components.getComponent().getProductsManager().getAllLocationsData( 30);
            // }
            // }
            for (Locations location_item : list) {
                if (location_item.getParentId() == null) {
                    fillDetailList(details, location_item , products , allInfos.get(1) , allInfos.get(0));
                }
            }
        }

        return details;
    }
    
    private void fillDetailList(List<ProductsDetailLocationDTO> details, Locations location , Products products ,  ArrayList<HashMap<String, Double>> allInfosNbTotSell , ArrayList<HashMap<String, Double>> allInfosNbSell) {
        if (location != null) {
            ProductsDetailLocationDTO dto = new ProductsDetailLocationDTO();

            dto.setSalesLocation(location.getAdapter(LocationsInfoDTO.class));
            dto.setStockUnits(getStock(location , products));

            if (!"0".equals(location.getId())) {
                dto.setPriceSellTtc(productsService.getFullPrice(location , products));
                dto.setTaxes(productsService.getEcotaxes(location , products));
                dto.setTotSell7D(getNbTotSell(location, 7 , products, allInfosNbTotSell));
                dto.setSell7D(dto.getTotSell7D() - getNbSell(location, 7 , products , allInfosNbSell));
                dto.setTotSell30D(getNbTotSell(location, 30 , products , allInfosNbTotSell));
                dto.setSell30D(dto.getTotSell30D() - getNbSell(location, 30 , products , allInfosNbSell));
                dto.setSellsPerDay(dto.getTotSell30D() / 30D);
                dto.setPriceBuy(products.getPriceBuy());
                dto.setPriceSell(productsService.getLocationPriceSell(location , products));
                dto.setActive(productsService.getLocationActiveTariffAreas(location , products) != null);

                if (dto.getStockUnits() > 0 || !products.getComponentsProducts().isEmpty()) {
                    Date stockoutDate = null;
                    // si c'est une offre ou un produit composé, alors la date de rupture est le min des composants
                    if (!products.getComponentsProducts().isEmpty()) {
                        for (ProductsComponents components : products.getComponentsProducts()) {
                            Date componentStockoutDate = getStockoutDate(location , components.getComponent() , allInfosNbTotSell);
                            if ((componentStockoutDate != null) && (stockoutDate == null || componentStockoutDate.before(stockoutDate))) {
                                stockoutDate = componentStockoutDate;
                            }
                        }
                        dto.setTotSell7D(dto.getSell7D() * -1);
                        dto.setSell7D(null);
                        dto.setTotSell30D(dto.getSell30D() * -1);
                        dto.setSell30D(null);
                        dto.setSellsPerDay(dto.getTotSell30D() / 30D);
                    } else {
                        stockoutDate = getStockoutDate(location , products , allInfosNbTotSell);
                    }
                    // TODO confirmer le concept ventes ou sorties sur ventes ( ie promos incluses ou pas ?)
                    if (stockoutDate != null) {
                        dto.setCoverageDays(dto.getStockUnits() / dto.getSellsPerDay());
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        dto.setStockoutDate(dateFormat.format(stockoutDate.getTime()));
                    }
                } else {
                    dto.setStockoutDate("En rupture de stock");
                }
            }
            details.add(dto);
            for (Locations subLocation : location.getChildren()) {
                ProductsDetailLocationDTO dto2 = new ProductsDetailLocationDTO();
                dto2.setSalesLocation(subLocation.getAdapter(LocationsInfoDTO.class));
                dto2.setStockUnits(getStock(subLocation , products));
                details.add(dto2);
            }

        }
    }
    
    public double getStock(Locations locations , Products products) {
        StockCurrent stockCurrent = locations == null ? null : stockService.findByProductAndLocation(products.getId(), locations.getId());
        return stockCurrent != null ? stockCurrent.getUnits() : 0D;
    }
    
    private ArrayList<ArrayList<HashMap<String, Double>>> getAllLocationsData(int nbDays, int totDays,  Products products) {
        HashMap<String, Double> data = null;
        List<Object[]> tmpList = null;
        ArrayList<ArrayList<HashMap<String, Double>>> retour = new ArrayList<ArrayList<HashMap<String, Double>>>();

        ArrayList<HashMap<String, Double>> allInfosNbSell = new ArrayList<HashMap<String, Double>>();
        
        if (allInfosNbSell.size() <= totDays) {
            while (allInfosNbSell.size() <= totDays) {
                allInfosNbSell.add(null);
            }
        }
        ArrayList<HashMap<String, Double>> allInfosNbTotSell = new ArrayList<HashMap<String, Double>>();
        
        if (allInfosNbTotSell.size() <= totDays) {
            while (allInfosNbTotSell.size() <= totDays) {
                allInfosNbTotSell.add(null);
            }
        }
        
        retour.add(allInfosNbSell);
        retour.add(allInfosNbTotSell);
        // TODO : init à zero si pas de valeur ( SQL ? )
        tmpList = ticketLinesDAO.findNbSellByLocation(products.getId(), nbDays, totDays);
        if (tmpList != null && tmpList.size() > 0) {
            data = new HashMap<String, Double>();
            allInfosNbSell.set(nbDays, data);
            data = new HashMap<String, Double>();
            allInfosNbSell.set(totDays, data);

            for (Object[] entree : tmpList) {
                // On ajoute entree[0] , entree[1] à la liste entree3
                data = allInfosNbSell.get((int) entree[2]);
                Double base = 0D;
                if ((int) (entree[2]) != nbDays && allInfosNbSell.get(nbDays) != null && !allInfosNbSell.get(nbDays).isEmpty()) {
                    base = allInfosNbSell.get(nbDays).get((String) entree[0]);
                }
                data.put((String) entree[0], (base == null ? 0D : base) + (Double) entree[1]);
            }

        }
        // TODO : init à zero si pas de valeur ( SQL ? )
        tmpList = stockDiaryDAO.findNbSellByLocation(products.getId(), nbDays, totDays);
        ;
        if (tmpList != null && tmpList.size() > 0) {
            data = new HashMap<String, Double>();
            allInfosNbTotSell.set(nbDays, data);
            data = new HashMap<String, Double>();
            allInfosNbTotSell.set(totDays, data);

            for (Object[] entree : tmpList) {
                // On ajoute entree[0] , entree[1] à la liste entree3
                data = allInfosNbTotSell.get((int) entree[2]);
                Double base = 0D;
                if ((int) (entree[2]) != nbDays && allInfosNbTotSell.get(nbDays) != null && !allInfosNbTotSell.get(nbDays).isEmpty()) {
                    base = allInfosNbTotSell.get(nbDays).get((String) entree[0]);
                }
                data.put((String) entree[0], (base == null ? 0D : base) + (Double) entree[1]);
            }
        }
        
        return retour;

    }
    
    private Double getNbSell(Locations location, int nbDays, Products products , ArrayList<HashMap<String, Double>> allInfosNbSell) {
        Double retour = 0D;
        if (allInfosNbSell != null && allInfosNbSell.get(nbDays) == null) {
            retour = 0D;
        } else {
            if (allInfosNbSell != null && allInfosNbSell.get(nbDays) != null) {
                retour = allInfosNbSell.get(nbDays).get(location.getId());
            } else {
                retour = ticketLinesDAO.findNbSell(products.getId(), location.getId(), nbDays);
            }
        }
        return (retour == null ? 0D : retour);
    }

    private Double getNbTotSell(Locations location, int nbDays , Products products, ArrayList<HashMap<String, Double>> allInfosNbTotSell ) {
        Double retour = 0D;
        if (allInfosNbTotSell != null && allInfosNbTotSell.get(nbDays) == null) {
            retour = 0D;
        } else {
            if (allInfosNbTotSell != null && allInfosNbTotSell.get(nbDays) != null) {
                retour = allInfosNbTotSell.get(nbDays).get(location.getId());
            } else {
                retour = stockDiaryDAO.findNbSell(products.getId(), location.getId(), nbDays);
            }
        }
        return (retour == null ? 0D : retour);
    }
    
//    private Date getStockoutDate(Products products) {
//        Locations location = locationsDAO.findTopByServerDefaultTrue();
//        return getStockoutDate(location,products);
//    }

    private Date getStockoutDate(Locations location, Products products, ArrayList<HashMap<String, Double>> allInfosNbTotSell) {
        double stock = getStock(location , products);
        double sellsPerDay = getNbTotSell(location, 30 , products , allInfosNbTotSell) / 30;
        Double coverageDay = sellsPerDay > (1.0 / 60) ? stock / sellsPerDay : 0;
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, coverageDay.intValue());
        return sellsPerDay < (1.0 / 60) ? null : calendar.getTime();
    }
    

    @Override
    public List<ProductsResultDTO> getLinks(String id) {
        return getAdapter(productsDAO.findLinked(id), ProductsResultDTO.class , false);
    }

}
