package com.ose.backend.service.products;

import java.util.List;

import org.pasteque.api.model.products.Products;

import com.ose.backend.dto.products.ProductsResultDTO;

public interface IProductsServiceOSE {

//    List<ProductsDTO> getByCategory(String id);

    Object getByCriteria(String id, String code, String reference, String dtoType);

    List<ProductsResultDTO> getByCriteria(String reference, String category, Integer limit , Boolean composition);

    List<ProductsResultDTO> getLinks(String id);

    <T> T getAdapter(Products item, Class<T> selector, Boolean filtered);

    <T> List<T>  getAdapter(List<Products> liste, Class<T> selector , Boolean filtered);

}
