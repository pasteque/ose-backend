package com.ose.backend.service.products.impl;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.ITariffAreasApiDAO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.POSTariffAreasValidityDTO;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.TariffAreasValidity;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ose.backend.dao.products.ITariffAreasValidityDAO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.service.products.ITariffAreasServiceOSE;
import com.ose.backend.specs.products.TariffAreasValiditySpecification;

@Service
public class TariffAreasServiceOSE implements ITariffAreasServiceOSE {

    @Autowired
    private ITariffAreasApiDAO tariffAreasDAO;
    @Autowired
    private IJobsService jobService;
    @Autowired
    private ITariffAreasValidityDAO tariffAreasValidityDAO;
    @Autowired
    private ILocationsApiDAO locationDAO;
    @Autowired
    private ILocationsServiceOSE locationsService;


    
//
//    @Override
//    public List<TariffAreasIndexDTO> getByCriteria(String reference) {
//        return AdaptableHelper.getAdapter(tariffAreasDAO.findByCriteria(reference), TariffAreasIndexDTO.class);
//    }
//    
//    @Override
//    public <T> List<T> getCurrentTariffAreas(String locationId, Class<T> selector) {
//        return AdaptableHelper.getAdapter(tariffAreasDAO.findCurrent(locationId), selector);
//    }
//    
//    @Override
//    public TariffAreasDTO getTariffAreaById(int id) {
//        return tariffAreasDAO.findByPrimaryKey(id).getAdapter(TariffAreasDTO.class);
//    }
//
//    @Override
//    public TariffAreasDTO getTariffAreaByName(String name) {
//        return tariffAreasDAO.findByName(name).getAdapter(TariffAreasDTO.class);
//    }
//    
//    @Override
//    public Object save(List<TariffAreasDTO> l) {
//        // TODO voir pour gérer convenablement les liens
//        TariffAreas tariffArea;
//        for (TariffAreasDTO tariffAreasDTO : l) {
//            tariffArea = tariffAreasDAO.findByPrimaryKey(tariffAreasDTO.getId());
//            if(tariffAreasDTO.getId() != 0 && tariffArea != null) {
//                tariffArea.setName(tariffAreasDTO.getLabel());
//                tariffAreasDAO.update(tariffArea);
//            } else {
//                tariffArea = new TariffAreas();
//                if(tariffAreasDTO.getId() != 0) tariffArea.setId(tariffAreasDTO.getId());
//                tariffArea.setName(tariffAreasDTO.getLabel());
//                tariffAreasDAO.create(tariffArea);
//            }
//        }
//        return null;
//    }
    
    @Override
    public Object saveLink(TariffAreasLocationsDTO tariffAreasDTO) {
        // TODO voir pour gérer convenablement les liens
        TariffAreasValidity tariffAreasValidity = null;
        boolean modified = false ;
        boolean created = false ;
        String action = null;
        if(tariffAreasDTO != null && tariffAreasDTO.getLocation() != null  && tariffAreasDTO.getTariffArea() != null 
                && tariffAreasDTO.getLocation().getId() != null && tariffAreasDTO.getTariffArea().getId() != null
                && tariffAreasDTO.getEndDate() != null && tariffAreasDTO.getStartDate() != null) {
            //EDU 2020 05 On change de methode l'Id n'étant pas stable à travers les installations
            //Et le dto pouvant désormais arriver d'une installation distante
            //tariffAreasValidity = tariffAreasValidityDAO.findByPrimaryKey(tariffAreasDTO.getId());
            tariffAreasValidity = findTariffAreasValidity(tariffAreasDTO.getTariffArea().getId(), tariffAreasDTO.getLocation().getId(), tariffAreasDTO.getStartDate());
            Locations location =  tariffAreasValidity != null ? tariffAreasValidity.getLocations() : locationDAO.findById(tariffAreasDTO.getLocation().getId()).orElse(null);
            TariffAreas tariffArea = tariffAreasValidity != null ? tariffAreasValidity.getTariffAreas() : tariffAreasDAO.findById(Long.parseLong(tariffAreasDTO.getTariffArea().getId())).orElse(null);

            if(tariffArea != null && location != null) {
                if(tariffAreasValidity == null) {
                    modified = true ;
                    created = true ;
                    tariffAreasValidity = new TariffAreasValidity();
                    tariffAreasValidity.setEndDate(tariffAreasDTO.getEndDate());
                    tariffAreasValidity.setStartDate(tariffAreasDTO.getStartDate());
                    tariffAreasValidity.setLocations(location);
                    tariffAreasValidity.setTariffAreas(tariffArea);
                } else {
                    //on ne peut modifier *que* la date de fin
                    if(! tariffAreasDTO.getEndDate().equals(tariffAreasValidity.getEndDate())){
                        tariffAreasValidity.setEndDate(tariffAreasDTO.getEndDate());
                        modified = true ;
                    }
                }
                if(modified) {
                    if(tariffAreasValidity.getEndDate().after(tariffAreasValidity.getStartDate())) {
                        if(created) {
                            tariffAreasValidityDAO.save(tariffAreasValidity);
                            action = Job.Actions.INSERT;
                        } else {
                            tariffAreasValidityDAO.save(tariffAreasValidity);
                            action = Job.Actions.UPDATE;
                        }
                    } else {
                        if(!created) {
                            tariffAreasValidityDAO.delete(tariffAreasValidity);
                            action = Job.Actions.DELETE;
                        }
                    }
                }
                
            }
        }
        if(action != null) {
            createJobs(tariffAreasDTO, action);
        }
        return tariffAreasValidity == null ? null : tariffAreasValidity.getAdapter(POSTariffAreasValidityDTO.class);
    }
    
    private TariffAreasValidity findTariffAreasValidity(String tariffAreaId, String locationId, Date startDate) {
        TariffAreasValiditySpecification specs = new TariffAreasValiditySpecification();
        specs.add(new SearchCriteria(TariffAreasValidity.Fields.TARIFF_AREAS ,  tariffAreaId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(TariffAreasValidity.Fields.LOCATION ,  locationId , SearchOperation.EQUAL));
        if(startDate != null) {
            specs.add(new SearchCriteria(TariffAreasValidity.Fields.START_DATE ,  DateHelper.formatSQLTimeStamp(startDate) , SearchOperation.EQUAL));
        }
        return tariffAreasValidityDAO.findOne(specs).orElse(null);
    }
    
    private List<TariffAreasValidity> findTariffAreasValiditylist(String tariffAreaId, String locationId, String categoryName , String tariffOrder , Date startDate , Date endDate) {
        TariffAreasValiditySpecification specs = new TariffAreasValiditySpecification();
        specs.add(new SearchCriteria(TariffAreasValidity.Fields.TARIFF_AREAS ,  tariffAreaId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(TariffAreasValidity.Fields.LOCATION ,  locationId , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(Locations.Fields.CATEGORY ,  categoryName , SearchOperation.EQUAL));
        specs.add(new SearchCriteria(TariffAreas.Fields.ORDER ,  tariffOrder , SearchOperation.EQUAL));
        if(endDate != null) {
            specs.add(new SearchCriteria(TariffAreasValidity.Fields.START_DATE ,  DateHelper.formatSQLTimeStamp(endDate) , SearchOperation.LESS_THAN));
        }
        if(startDate != null) {
            specs.add(new SearchCriteria(TariffAreasValidity.Fields.END_DATE ,  DateHelper.formatSQLTimeStamp(startDate) , SearchOperation.GREATER_THAN));
        }
        return tariffAreasValidityDAO.findAll(specs);
    }

    @Override
    public Object deleteLink(TariffAreasLocationsDTO tariffAreasDTO) {
        TariffAreasValidity tariffAreasValidity = null;
        boolean retour = false;

        if(tariffAreasDTO != null && tariffAreasDTO.getLocation() != null  && tariffAreasDTO.getTariffArea() != null 
                && tariffAreasDTO.getLocation().getId() != null && tariffAreasDTO.getTariffArea().getId() != null
                && tariffAreasDTO.getEndDate() != null && tariffAreasDTO.getStartDate() != null) {
            //EDU 2020 05 On change de methode l'Id n'étant pas stable à travers les installations
            //Et le dto pouvant désormais arriver d'une installation distante
            //tariffAreasValidity = tariffAreasValidityDAO.findByPrimaryKey(tariffAreasDTO.getId());
            tariffAreasValidity = findTariffAreasValidity(tariffAreasDTO.getTariffArea().getId(), tariffAreasDTO.getLocation().getId(), tariffAreasDTO.getStartDate());
        
            if(tariffAreasValidity != null) {
                tariffAreasValidityDAO.delete(tariffAreasValidity);
                retour = true;
                //Create Job
                createJobs(tariffAreasDTO, Job.Actions.DELETE);
            }
        }

        return retour;
    }
    
    @Override
    public <T> List<T> getCurrentTariffAreasLinks(String locationId, String tariffAreaId , String categoryName , String tarifOrder , Date from , Date to, Class<T> selector) {
        return AdaptableHelper.getAdapter(findTariffAreasValiditylist(tariffAreaId , locationId , categoryName , tarifOrder , from , to), selector);
    }
    
    @Override 
    public void createJobs(TariffAreasLocationsDTO dto , String action) {
        Locations localServer = locationDAO.findTopByServerDefaultTrue();
        LocationsInfoDTO localServerdto = localServer == null ? null : localServer.getAdapter(LocationsInfoDTO.class);
        Job job = JobHelper.createJobAffectationPDVCatalogues(dto, localServerdto);
        if( job != null) {
            try {
                JobHelper.fillJobAffectationPDVCatalogues(job, action, dto);
                jobService.save(job);
                if (localServerdto == null ) {
                    //Dans ce cas nous sommes sur le back central donc propager 'plus'
                    //TODO voir si c'est pour la log ou pour tout le monde
                    List<LocationsInfoDTO> dtos = AdaptableHelper.getAdapter( locationsService.findAllParentLocations(false, job.getDestination(),false,false) , LocationsInfoDTO.class);
                    List<Job> jobList = JobHelper.createJobForEveryBack(job, dtos);
                    for(Job element : jobList) {
                        jobService.save(element);
                    }
                }
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }
    }
}
