package com.ose.backend.service.edi.impl;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceException;
import javax.persistence.PersistenceUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.pasteque.api.dao.application.IPosMessageTypeApiDAO;
import org.pasteque.api.dao.application.IResourcesApiDAO;
import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.dao.cash.ITaxesApiDAO;
import org.pasteque.api.dao.customers.ICustomersApiDAO;
import org.pasteque.api.dao.customers.IDiscountProfilApiDAO;
import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dao.locations.IToursApiDAO;
import org.pasteque.api.dao.products.ICategoriesApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.products.ITariffAreasApiDAO;
import org.pasteque.api.dao.products.ITariffAreasProductsApiDAO;
import org.pasteque.api.dao.products.ITaxCategoriesApiDAO;
import org.pasteque.api.dao.security.IRolesApiDAO;
import org.pasteque.api.dao.tickets.IPaymentsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.dto.cash.CashRegistersSimpleDTO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSLocationsDTO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.locations.TourIndexDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.pasteque.api.dto.products.POSTariffAreaPriceDTO;
import org.pasteque.api.dto.products.POSTariffAreasDTO;
import org.pasteque.api.dto.products.POSTariffAreasValidityDTO;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.dto.tickets.POSPaymentsDTO;
import org.pasteque.api.dto.tickets.POSTicketsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;
import org.pasteque.api.model.application.Resources;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.products.Categories;
import org.pasteque.api.model.products.ProductReferences;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.products.ProductsCategory;
import org.pasteque.api.model.products.ProductsComponents;
import org.pasteque.api.model.products.TariffAreas;
import org.pasteque.api.model.products.TariffAreasProducts;
import org.pasteque.api.model.products.TariffAreasProductsId;
import org.pasteque.api.model.products.TaxCategories;
import org.pasteque.api.model.products.Taxes;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.security.Roles;
import org.pasteque.api.model.tickets.Payments;
import org.pasteque.api.model.tickets.TicketLines;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.service.cash.ICashRegistersService;
import org.pasteque.api.service.customers.ICustomersService;
import org.pasteque.api.service.fiscal.ITaxesService;
import org.pasteque.api.service.products.IProductsService;
import org.pasteque.api.service.saleslocations.ISalesLocationsService;
import org.pasteque.api.service.security.IUsersService;
import org.pasteque.api.service.tickets.ITicketsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.constants.DefaultValues;
import com.ose.backend.dao.application.IApplicationDAO;
import com.ose.backend.dao.application.IPosMessagesDAO;
import com.ose.backend.dao.customers.ICustomersDAO;
import com.ose.backend.dao.orders.IOrdersDAO;
import com.ose.backend.dao.orders.IPalettesDAO;
import com.ose.backend.dao.products.IProductReferencesDAO;
import com.ose.backend.dao.products.IProductsComponentsDAO;
import com.ose.backend.dao.stocks.IStockCurrentDAO;
import com.ose.backend.dao.stocks.IStockDiaryDAO;
import com.ose.backend.dao.stocks.IStockLevelDAO;
import com.ose.backend.dao.tickets.IPaymentsDAO;
import com.ose.backend.dao.tickets.ITicketsExtendedDAO;
import com.ose.backend.dto.edi.EdiContentDTO;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.stocks.StockCurrentDTO;
import com.ose.backend.dto.stocks.StockDiaryDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.dto.tickets.PaymentsIndexDTO;
import com.ose.backend.dto.utils.ExportConstraintsDTO;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.model.orders.Palettes;
import com.ose.backend.model.stocks.StockCurrent;
import com.ose.backend.model.stocks.StockDiary;
import com.ose.backend.model.stocks.StockLevel;
import com.ose.backend.model.tickets.TicketsExtended;
import com.ose.backend.service.application.IMessagesService;
import com.ose.backend.service.cash.ICashRegistersServiceOSE;
import com.ose.backend.service.cash.IClosedCashService;
import com.ose.backend.service.edi.ICustomerEdiService;
import com.ose.backend.service.edi.IEdiService;
import com.ose.backend.service.edi.IOrdersEdiService;
import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.service.locations.IToursLocationsService;
import com.ose.backend.service.orders.IOrdersService;
import com.ose.backend.service.products.ITariffAreasServiceOSE;
import com.ose.backend.service.stock.IStocksServiceOse;
import com.ose.backend.service.tickets.ITicketsServiceOSE;

@Service
public class EdiService implements IEdiService {

    @Autowired
    private IApplicationDAO applicationDAO;
    @Autowired
    private ISalesLocationsApiDAO salesLocationsDAO;
    @Autowired
    private IProductsApiDAO productsDAO;
    @Autowired
    private IProductReferencesDAO productReferencesDAO;
    @Autowired
    private ITaxCategoriesApiDAO taxCategoriesDAO;
    @Autowired
    private ICategoriesApiDAO categoriesDAO;
    @Autowired
    private IToursApiDAO toursDAO;
    @Autowired
    private ILocationsApiDAO locationDAO;
    @Autowired
    private ITicketsApiDAO ticketsDAO;
    @Autowired
    private ICustomersApiDAO customersApiDAO;
    @Autowired
    private ICustomersDAO customersDAO;
    @Autowired
    private ICashRegistersApiDAO cashRegistersDAO;
    @Autowired
    private IStockCurrentDAO stockCurrentDAO;
    @Autowired
    private IStockLevelDAO stockLevelDAO;
    @Autowired
    private IStockDiaryDAO stockDiaryDAO;
    @Autowired
    private IPosMessageTypeApiDAO posMessageTypeDAO;
    @Autowired
    private IPosMessagesDAO posMessagesDAO;
    @Autowired
    private IClosedCashApiDAO closedCashDAO;
    @Autowired
    private ITaxesApiDAO taxesDAO;
    @Autowired
    private IInseeApiDAO inseeDAO;
    @Autowired
    private ITariffAreasProductsApiDAO tariffAreasProductsDAO;
    @Autowired
    private ITariffAreasApiDAO tariffAreasDAO;
    @Autowired
    private IStocksServiceOse stocksService;
    @Autowired
    private IOrdersDAO ordersDAO;
    @Autowired
    private IPalettesDAO palettesDAO;
    @Autowired
    private IPaymentsApiDAO paymentsApiDAO;
    @Autowired
    private IPaymentsDAO paymentsDAO;
    @Autowired
    private IProductsComponentsDAO productsComponentsDAO;
    @Autowired
    private IResourcesApiDAO resourcesDAO;
    @Autowired
    private IRolesApiDAO rolesDAO;
    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private ICustomersService customersService;
    @Autowired
    private IToursLocationsService toursLocationsService;
    @Autowired
    private ISalesLocationsService salesLocationsService;
    @Autowired
    private ILocationsServiceOSE locationsService;
    @Autowired
    private IDiscountProfilApiDAO discountProfilDAO;
    @Autowired
    private ITicketsExtendedDAO ticketsExtendedDAO;
    @Autowired
    private IUsersService usersService;
    @Autowired
    private ITariffAreasServiceOSE tariffAreasService ; 
    @Autowired
    private IProductsService productsService ; 
    @Autowired
    private ICashRegistersServiceOSE cashRegistersServiceOse;
    @Autowired
    private ICashRegistersService cashRegistersService;
    @Autowired
    private IClosedCashService closedCashService;
    @Autowired
    private ITicketsServiceOSE ticketsService;
    @Autowired
    private ITicketsService ticketsServiceApi;
    @Autowired
    private ITaxesService taxesService;
    @Autowired
    private IStocksServiceOse stockService;
    @Autowired
    private IOrdersEdiService ordersEdiService;
    @Autowired
    private IMessagesService messagesService;
    @Autowired
    private ICustomerEdiService customerEdiService;
    @PersistenceUnit
    EntityManagerFactory emf;

    
    /***
     * EDU : je pose un hash en dur pour faire correspondre les données telles qu'elles parviennent jusque là 
     * avec celles qui seront dans le système 
     */
    private static final HashMap<String, String> oldIdMap = new HashMap<String, String>() {

        private static final long serialVersionUID = -6952318816474803385L;

        {
            put("1", "Taux plein");
            put("2", "Taux réduit");
        }
    };
    // Les noms de feuilles reconnues / écrites dans un excel
    public final class Sheets {
        public static final String CUST = "Clients";
        public static final String TARRIFAREAS = "Catalogues";
        public static final String TARRIFAREAS_LOC = "Dates-Cata";
        public static final String TARRIFAREAS_PROD = "Prix-Cata";
        public static final String LOCATIONS = "Lieux-Stockage";
        public static final String LOCATIONS_TOURS = "Tournées-Camions";
        public static final String ORDERS = "Commandes";
        public static final String DOUBLON = "Dedup";
        public static final String STOCK_TYPE = "STOCK TYPE";
        public static final String TICKET_CLIENT = "Ticket client";

    }

    // Le nom des préfixes connus pour les colonnes des tarriffaires
    public class TarrifColumns {
        public static final String CAT_ID = "PROD_CATALOG_ID";
        public static final String CAT_NOM = "CATEGORY_NAME";
        public static final String CAT_RANK = "RANG";
        public static final String LOC_ID = "LOCATION_ID";
        public static final String DATE_FROM = "DATE_FROM";
        public static final String DATE_TO = "DATE_TO";
        public static final String PROD_ID = "PRODUCT_ID";
        public static final String PRICE_WITH_TAX = "PRICE_WITH_TAX";
        public static final String TAX_PERCENTAGE = "TAX_PERCENTAGE";

        public Map<Integer, String> columns;
    }

    // Le nom des préfixes connus pour les colonnes des lieus de stock
    public class LocationsColumns {
        public static final String LOC_ID = "ID";
        public static final String LOC_NOM = "NOM";
        public static final String LOC_ADDRESS = "ADRESSE";
        public static final String LOC_TAXCUST_ID = "TAX_ID";
        public static final String LOC_INSEENNUM = "NUM_INSEE";
        public static final String LOC_PARENT_ID = "PARENT_ID";
        public static final String LOC_CATEG_ID = "CATEG_ID";
        public static final String LOC_ETAT = "ETAT";

        public Map<Integer, String> columns;
    }

    // Le nom des préfixes connus pour les colonnes des associations Tournées - point de vente
    public class LocationsToursColumns {
        public static final String LOC_ID = "LOCATION_ID";
        public static final String TOUR_ID = "TOUR_ID";
        public static final String START = "DEBUT";
        public static final String END = "FIN";

        public Map<Integer, String> columns;
    }

    // Le nom des préfixes connus pour les csv
    public final class Prefixes {
        public static final String ARTICLE = "A";
        public static final String BLOCKED = "B";
        public static final String REFERENCE = "C";
        public static final String REFERENCE_DELETE = "DEL_C";
        public static final String COMPOSED = "D";
        public static final String SALE_LOCATION = "G";
        public static final String UNIVERSE = "U";
        public static final String FAMILY = "V";
        public static final String STOCKINFO = "S";
        public static final String TARRIFPRICE = "TAR_PRICE";
        public static final String TARRIFPRICE_DELETE = "DEL_TAR_PRICE";
        public static final String PMPA = "PMPA";
        public static final String LASTBUYPRICE = "LAST_PRICE";
        public static final String DEFAULTPRICE = "DEF_PRICE";
        public static final String CATALOG = "CATALOG";
        public static final String USER = "USER";
    }

    // Stock Type
    public class StockLevelInfo {
        public static final int NUM_LIGNE_PDV = 1;
        public static final int NUM_LIGNE_DATA = 3;
        public static final String CHAR_NOT_PDV = "-";
        // UPDATE_STCKTYPE_NOTEXIST sert a mettre à 0 les valeur de stocktype exsitante à 0
        public static final boolean INITIALISE_STYPE_TYPE = true;

    }

    @Override
    public String importFile(String filecontent, String locationId) {

        EdiContentDTO content = new EdiContentDTO();
        String retour;
        // On explose le fichier en lignes
        // On trie les lignes
        // On gère chaque bloc
        String[] lignes = filecontent.split("[\r\n]+");

        try {
            ExportConstraintsDTO dto = new ObjectMapper().readValue(filecontent, ExportConstraintsDTO.class);
            retour = dto.getTickets() == null ? "0" : String.valueOf(dto.getTickets().size());

            // Sauvegarde des Clients
            saveCustomers(dto.getCustomers());

            // Sauvegarde des Caisses - état des tickets en particulier
            List<CashRegistersSimpleDTO> listCashes = dto.getCashes();
            if (listCashes != null) {
                for (CashRegistersSimpleDTO cashesDto : listCashes) {
                    boolean existingEntry = true;
                    CashRegisters cashes = cashRegistersDAO.findById(cashesDto.getId()).orElse(null);

                    if (cashes == null) {
                        existingEntry = false;
                        cashes = new CashRegisters();
                        cashes.setId(cashesDto.getId());
                    }

                    if (cashRegistersServiceOse.loadFromDTO(cashesDto , cashes)) {
                        if (existingEntry) {
                            cashes = cashRegistersDAO.save(cashes);
                        } else {
                            cashes = cashRegistersDAO.save(cashes);
                        }
                    }

                }
            }
            // Sauvegarde des Sessions de vente - surtout les nouvelles- Attention aux Id !
            List<POSClosedCashDTO> listClosedCash = dto.getClosedCash();
            if (listClosedCash != null) {
                for (POSClosedCashDTO POSClosedCashDTO : listClosedCash) {
                    boolean existingEntry = true;
                    ClosedCash closedCash = closedCashDAO.findById(POSClosedCashDTO.getId()).orElse(null);

                    if (closedCash == null) {
                        existingEntry = false;
                        closedCash = new ClosedCash();
                        closedCash.setMoney(POSClosedCashDTO.getId());
                    }
                    if (closedCashService.loadFromDTO(POSClosedCashDTO,closedCash)) {
                        if (existingEntry) {
                            closedCash = closedCashDAO.save(closedCash);
                        } else {
                            closedCash = closedCashDAO.save(closedCash);
                        }
                    }

                }
            }
            // Sauvegarde des Déballages - surtout les nouveaux - Attention aux Id !
            List<POSSalesLocationsDTO> listSalesLocations = dto.getSalesLocations();
            if (listSalesLocations != null) {
                for (POSSalesLocationsDTO salesLocationDto : listSalesLocations) {
                    boolean existingEntry = true;
                    SalesLocations salesLocation = salesLocationsDAO.findById(salesLocationDto.getId()).orElse(null);

                    if (salesLocation == null) {
                        existingEntry = false;
                        salesLocation = new SalesLocations();
                        salesLocation.setId(salesLocationDto.getId());
                    }
                    if (salesLocationsService.loadFromDTO(salesLocationDto , salesLocation)) {
                        if (existingEntry) {
                            salesLocation = salesLocationsDAO.save(salesLocation);
                        } else {
                            salesLocation.createId();
                            salesLocation = salesLocationsDAO.save(salesLocation);
                        }
                    }

                }
            }
            // Sauvegarde des détails des mouvements de stocks - Attention aux Id !
            List<StockDiaryDTO> listStockDiary = dto.getStockDiary();
            if (listStockDiary != null) {
                for (StockDiaryDTO stockDiaryDto : listStockDiary) {
                    boolean existingEntry = true;
                    StockDiary stockDiary = stockDiaryDAO.findById(stockDiaryDto.getId()).orElse(null);

                    if (stockDiary == null) {
                        existingEntry = false;
                        stockDiary = new StockDiary();
                        stockDiary.setId(stockDiaryDto.getId());
                    }
                    if (stockService.loadStockDiaryFromDTO(stockDiary , stockDiaryDto)) {
                        if (existingEntry) {
                            stockDiary = stockDiaryDAO.save(stockDiary);
                        } else {
                            stockDiary = stockDiaryDAO.save(stockDiary);
                        }
                    }

                }
            }
            // Sauvegarde des réglages de stocks - Attention aux Id !
            List<StockLevelDTO> listStockLevel = dto.getStockLevel();
            //TODO : EDU ici génération job +  sans doute modif service et appel 
            if (listStockLevel != null) {
                for (StockLevelDTO stockLevelDto : listStockLevel) {
                    stocksService.saveStockLevel(stockLevelDto);
                    /* On utilise désormais le service
                    boolean existingEntry = true;
                    
                    StockLevel stockLevel = stockLevelDAO.findById(stockLevelDto.getId());

                    if (stockLevel == null) {
                        existingEntry = false;
                        stockLevel = new StockLevel();
                        stockLevel.setId(stockLevelDto.getId());
                    }
                    if (stockLevel.getStocksManager().loadFromDTO(stockLevelDto)) {
                        if (existingEntry) {
                            stockLevel = stockLevelDAO.save(stockLevel);
                        } else {
                            stockLevel = stockLevelDAO.save(stockLevel);
                        }
                    }
                    */

                }
            }
            // Sauvegarde des niveaux de stocks - Attention aux Id !
            List<StockCurrentDTO> listStockCurrent = dto.getStockCurrent();
            if (listStockCurrent != null) {
                for (StockCurrentDTO stockCurrentDto : listStockCurrent) {
                    boolean existingEntry = true;
                    StockCurrent stockCurrent = stocksService.findByProductAndLocation(stockCurrentDto.getProductsId(), stockCurrentDto.getLocationsId());

                    if (stockCurrent == null) {
                        existingEntry = false;
                        stockCurrent = new StockCurrent();
                    }
                    if (stockService.loadStockCurrentFromDTO(stockCurrent , stockCurrentDto)) {
                        if (existingEntry) {
                            stockCurrent = stockCurrentDAO.save(stockCurrent);
                        } else {
                            stockCurrent = stockCurrentDAO.save(stockCurrent);
                        }
                    }

                }
            }
            // Sauvegarde des Tickets - Attention aux Id !
            List<POSTicketsDTO> listTickets = dto.getTickets();
            if (listTickets != null) {
                for (POSTicketsDTO ticketsDto : listTickets) {
                    boolean existingEntry = true;
                    Tickets tickets = ticketsDAO.findById(ticketsDto.getId()).orElse(null);

                    if (tickets == null) {
                        existingEntry = false;
                        tickets = new Tickets();
                    }
                    if (ticketsService.loadFromDTO(ticketsDto,tickets)) {
                        if (existingEntry) {
                            tickets = ticketsDAO.save(tickets);
                        } else {
                            tickets = ticketsDAO.save(tickets);
                        }
                    }

                }
            }

            List<OrderDTO> listOrders = dto.getOrders();
            if (listOrders != null) {
                for (OrderDTO orderDto : listOrders) {
                    boolean existingEntry = true;
                    Orders orders = ordersDAO.findById(orderDto.getId()).orElse(null);

                    if (orders == null) {
                        existingEntry = false;
                        orders = new Orders();
                    }
                    if (ordersService.loadFromDTO(orderDto,orders)) {
                        if (existingEntry) {
                            orders = ordersDAO.save(orders);
                        } else {
                            orders = ordersDAO.save(orders);
                        }
                    }

                }
            }

            List<PaletteDTO> listPalette = dto.getPalettes();
            if (listPalette != null) {
                for (PaletteDTO paletteDto : listPalette) {
                    boolean existingEntry = true;
                    Palettes palettes = palettesDAO.findById(paletteDto.getId()).orElse(null);
                    if (palettes == null) {
                        existingEntry = false;
                        palettes = new Palettes();
                    }
                    if (ordersService.loadPaletteFromDTO(palettes , paletteDto)) {
                        if (existingEntry) {
                            palettes = palettesDAO.save(palettes);
                        } else {
                            palettes = palettesDAO.save(palettes);
                        }
                    }

                }
            }

            // TODO question des ID ?
            List<POSMessagesDTO> listMessages = dto.getMessages();
            if (listMessages != null) {
                for (POSMessagesDTO messagesDto : listMessages) {
                    boolean existingEntry = true;
                    // List<PosMessages> messages_list = posMessagesDAO.findByCriteria(messagesDto.getLocationId(), messagesDto.getProductId() ,
                    // messagesDto.getPeopleId() , messagesDto.getType() , false);

                    PosMessages messages = posMessagesDAO.findById(messagesDto.getId()).orElse(null);
                    if (messages == null) {
                        existingEntry = false;
                        messages = new PosMessages();
                    }
                    if (messagesService.loadFromDTO(messages ,messagesDto)) {
                        if (existingEntry) {
                            messages = posMessagesDAO.save(messages);
                        } else {
                            messages = posMessagesDAO.save(messages);
                        }
                    }
                }
            }

            return filecontent;
        } catch (IOException e) {

            // Arrays.sort(lignes); pas besoin je fais le tour
            for (String ligne : lignes) {
                String[] elements = ligne.split(";");
                if (elements.length > 0) {
                    switch (elements[0]) {
                    case Prefixes.ARTICLE:
                        content.articles.add(ligne);
                        break;
                    case Prefixes.BLOCKED:
                        content.blocages.add(ligne);
                        break;
                    case Prefixes.REFERENCE:
                        content.references.add(ligne);
                        break;
                    case Prefixes.REFERENCE_DELETE:
                        content.del_references.add(ligne);
                        break;
                    case Prefixes.COMPOSED:
                        content.compositions.add(ligne);
                        break;
                    case Prefixes.SALE_LOCATION:
                        content.deballages.add(ligne);
                        break;
                    case Prefixes.UNIVERSE:
                        content.univers.add(ligne);
                        break;
                    case Prefixes.FAMILY:
                        content.prod_categ.add(ligne);
                        break;
                    case Prefixes.STOCKINFO:
                        content.infos_stocks.add(ligne);
                        break;
                    case Prefixes.PMPA:
                        content.infos_pmpa.add(ligne);
                        break;
                    case Prefixes.LASTBUYPRICE:
                        content.infos_pa.add(ligne);
                        break;
                    case Prefixes.DEFAULTPRICE:
                        content.infos_prix_defaut.add(ligne);
                        break;
                    case Prefixes.TARRIFPRICE:
                        content.infos_cat_price.add(ligne);
                        break;
                    case Prefixes.TARRIFPRICE_DELETE:
                        content.infos_del_cat_price.add(ligne);
                        break;
                    case Prefixes.CATALOG:
                        content.catalog.add(ligne);
                        break;
                    case Prefixes.USER:
                        content.user.add(ligne);
                        break;
                    default:
                        if (locationId != null) {
                            content.emplacements.add(ligne);
                        } else {
                            content.stocktypes.add(ligne);
                        }
                    }
                }
            }
            retour = importUniversCsv(content);
            retour += " - " + importProductsCsv(content);
            retour += " - " + importProductUniversCsv(content);
            retour += " - " + importCompositionsCsv(content);
            retour += " - " + importReferencesCsv(content);
            retour += " - " + importSalesConstraintsCsv(content);
            retour += " - " + importSalesSessionsCsv(content);
            retour += " - " + importStocksPlaces(locationId, content);
            retour += " - " + importTarrifPricecsv(content);
            retour += " - " + importLastPriceBuycsv(content);
            retour += " - " + importLastPMPAcsv(content);
            retour += " - " + importDefaultPricecsv(content);
            //retour += " - " + importStockTypecsv(content);
            retour += " - " + importCatalogcsv(content);
            // EDU - on force à 0 le locationId si on a un import c'est pour le siège
            retour += " - " + importDeleteReferencesCsv(content);
            retour += " - " + importDeleteTarrifPricecsv(content);
            retour += " - " + importUserscsv(content);
            retour += " - " + importLocationsStocks("0", content);
            return filecontent + "\n" + retour;
        }

    }

    private void saveCustomers(List<CustomersDetailDTO> listCustomers) {
        HashMap<String, Customers> newClient = new HashMap<String, Customers>();
        HashMap<String, Customers> updClient = new HashMap<String, Customers>();
        List<Customers> clientList;
        if (listCustomers != null) {
            for (CustomersDetailDTO customersDto : listCustomers) {
                boolean existingEntry = true;
                Customers customer = null;

                if (updClient.size() > 0) {
                    customer = updClient.get(customersDto.getSearchKey());
                }

                if (customer == null && newClient.size() > 0) {
                    customer = newClient.get(customersDto.getSearchKey());
                    if (customer != null) {
                        existingEntry = false;
                    }

                }

                if (customer == null) {

                    List<Customers> list = customersApiDAO.findBySearchKey(customersDto.getSearchKey());
                    if (list.size() > 0) {
                        customer = list.get(0);
                    }

                    if (customer == null) {
                        customer = customersDto.getId() != null ? customersDAO.findById(customersDto.getId()).orElse(null) : null;
                    }
                }

                if (customer == null) {
                    existingEntry = false;
                    customer = new Customers();
                    customer.setId(customersDto.getId());
                    if (customer.getId() == null) {
                        customer.setId(String.valueOf(new Date().getTime()) + "-" + customersDto.getSearchKey());
                    }
                }

                if (customersService.loadFromDTO(customersDto , customer)) {
                    //EDU - peut etre vérifier ici les objets Location et DiscountProfil
                    if(customer.getDiscountProfil() != null && customer.getDiscountProfil().getName() == null) {
                        customer.setDiscountProfil(discountProfilDAO.findById(Long.valueOf(customersDto.getDiscountProfile().getId())).orElse(null));
                    }
                    if(customer.getLocation() != null && customer.getLocation().getName() == null) {
                        customer.setLocation(locationDAO.findById(customersDto.getLocation().getId()).orElse(null));
                    }
                    customer.setDateUpdate(new Date());
                    if (existingEntry) {
                        updClient.put(customersDto.getSearchKey(), customer);

                    } else {
                        if (customer.getDateCreation() == null) {
                            customer.setDateCreation(new Date());
                        }
                        newClient.put(customersDto.getSearchKey(), customer);
                    }
                }

            }

            clientList = new ArrayList<Customers>(newClient.values());
            if (clientList.size() > 0) {
                customersDAO.saveAll(clientList);
            }
            clientList = new ArrayList<Customers>(updClient.values());
            if (clientList.size() > 0) {
                customersDAO.saveAll(clientList);
            }
        }

    }

    private int saveTarifPrice(List<POSTariffAreaPriceDTO> list) {
        int retour = 0;
        if (list != null && list.size() > 0) {
            // int i = 0;
            for (POSTariffAreaPriceDTO element : list) {
                boolean existingEntry = true;
                // i++;
                TariffAreasProducts tariffAreasProducts = element.getPrice() != 0D ? tariffAreasProductsDAO.findById(new TariffAreasProductsId(element.getAreaId(), element.getProductId())).orElse(null)
                        : null;

                if (tariffAreasProducts == null) {
                    existingEntry = false;
                    tariffAreasProducts = new TariffAreasProducts();

                }

                if (tariffAreasProducts.getTarriffAreasManager().loadFromDTO(element)) {
                    retour++;
                    try {
                        if (existingEntry) {
                            tariffAreasProducts = tariffAreasProductsDAO.save(tariffAreasProducts);
                        } else {

                            tariffAreasProducts = tariffAreasProductsDAO.save(tariffAreasProducts);
                        }
                    } catch (java.lang.IllegalStateException ex) {
                        // EDU On pourrait essayer de lier avec des catalogues ou de produits qui n'existent pas.
                        // Dans la mesure où on ne fait plus une extraction depuis la base pour retrouver le cata
                        // On va donc intercepter l'erreur levée dans un tel cas en silence.
                        // Attention toutefois à ne pas mettre la relation prix - catalogue en create
                        retour--;
                    }
                }
            }
        }

        return retour;

    }

    private int delTarifPrice(List<POSTariffAreaPriceDTO> list) {
        int retour = 0;
        if (list != null && list.size() > 0) {
            for (POSTariffAreaPriceDTO element : list) {
                // i++;
                TariffAreasProducts tariffAreasProducts = tariffAreasProductsDAO.findById(new TariffAreasProductsId(element.getAreaId(), element.getProductId())).orElse(null);

                if (tariffAreasProducts != null) {
                    tariffAreasProductsDAO.delete(tariffAreasProducts);
                    retour++;
                }
            }
        }

        return retour;

    }

    private int saveUsers(List<POSUsersDTO> list) {
        int retour = 0;
        if (list != null && list.size() > 0) {
            for (POSUsersDTO element : list) {
                usersService.save(element);
                retour++;
            }
            
        }

        return retour;
    }

    private void saveTarrifValidity(List<POSTariffAreasValidityDTO> list) {
        if (list != null) {
            for (POSTariffAreasValidityDTO element : list) {
                /**
                boolean existingEntry = true;
                TariffAreasValidity tariffAreasValidity = element.getAreaId() != 0 ? tariffAreasValidityDAO.find(element.getAreaId(), element.getLocationId() , element.getStartDate())
                        : null;

                if (tariffAreasValidity == null) {
                    existingEntry = false;
                    tariffAreasValidity = new TariffAreasValidity();

                }

                if (tariffAreasValidity.getTarriffAreasManager().loadFromDTO(element)) {

                    try {
                        if (existingEntry) {
                            tariffAreasValidity = tariffAreasValidityDAO.save(tariffAreasValidity);
                        } else {

                            tariffAreasValidity = tariffAreasValidityDAO.save(tariffAreasValidity);
                        }
                    } catch (java.lang.IllegalStateException ex) {
                        // EDU On pourrait essayer de lier avec des catalogues qui n'existent pas.
                        // Dans la mesure où on ne fait plus une extraction depuis la base pour retrouver le cata
                        // On va donc intercepter l'erreur levée dans un tel cas en silence.
                        // Attention toutefois à ne pas mettre la relation validity - catalogue en create
                    }
                }
                */
                //EDU 2020 05 Utilisation du service
                tariffAreasService.saveLink(new TariffAreasLocationsDTO(element));
            }
        }

    }

    private void saveLocationsTours(List<POSToursLocationsDTO> list) {
        toursLocationsService.saveAll(list);
    }

    private void saveLocations(List<POSLocationsDTO> list) {
        if (list != null) {
            for (POSLocationsDTO element : list) {
                boolean existingEntry = true;
                String oldParentId = null;
                Locations oldParent = null;
                Locations found = null;
                List<Locations> children = null;
                Locations locations = element.getId() != null ? locationDAO.findById(element.getId()).orElse(null) : null;

                if (locations == null) {
                    existingEntry = false;
                    locations = new Locations();
                    locations.setId(element.getId());
                } else {
                    oldParentId = locations.getParentId();
                    oldParent = locations.getParent();
                }

                if (locationsService.loadFromDTO(element,locations)) {

                    if (oldParentId == null) {
                        if (locations.getParentId() != null) {
                            // On a un parent hors avant il n'y en avait pas
                            // On ajoute cet enfant au parent.
                            children = locations.getParent().getChildren();
                            if (children == null) {
                                children = new ArrayList<Locations>();
                            }
                            children.add(locations);
                            locations.getParent().setChildren(children);

                            locationDAO.save(locations.getParent());
                            existingEntry = true;
                            // la création se fait ici par cascade si l'objet actuel n'est pas déjà existant
                        }
                    } else {
                        // ici on a forcement déjà existing entry à true puisqu'on avait historiquement un parent.
                        if (!oldParentId.equals(locations.getParentId())) {
                            // Le parent a changé
                            // On enlève celui ci du père précédent
                            children = oldParent.getChildren();
                            for (Locations child : children) {
                                if (child.getId().equals(locations.getId())) {
                                    found = child;
                                    break;
                                }
                            }
                            if (found != null) {
                                children.remove(found);
                                oldParent.setChildren(children);
                                locationDAO.save(oldParent);
                            }

                            if (locations.getParentId() != null) {
                                // Avant il y avait un parent, il n'y en a plus
                                // Enlever celui-ci du père;
                                children = locations.getParent().getChildren();
                                if (children == null) {
                                    children = new ArrayList<Locations>();
                                }
                                children.add(locations);
                                locations.getParent().setChildren(children);
                                locationDAO.save(locations.getParent());
                            }
                        }
                    }

                    if (existingEntry) {
                        locations = locationDAO.save(locations);
                    } else {

                        locations = locationDAO.save(locations);
                    }

                }
            }
        }
    }

    private void saveTarrif(List<POSTariffAreasDTO> list) {
        if (list != null) {
            for (POSTariffAreasDTO element : list) {
                boolean existingEntry = true;
                TariffAreas tariffAreas = element.getId() != null ? tariffAreasDAO.findById(element.getId()).orElse(null) : null;

                if (tariffAreas == null) {
                    existingEntry = false;
                    tariffAreas = new TariffAreas();

                }

                if (tariffAreas.getTarriffAreasManager().loadFromDTO(element)) {

                    if (existingEntry) {
                        tariffAreas = tariffAreasDAO.save(tariffAreas);
                    } else {

                        tariffAreas = tariffAreasDAO.save(tariffAreas);
                    }
                }
            }
        }

    }

    /**
     * EDU 2020 07 - Historique avant 2020 il  n'y avait qu'un imort de stock types
     * Sur la base des fichiers plats d'échanges avec l'ancienne caisse
     * Il n'y avait pas de notion de stock de sécurité
     * Le modèle n'a pas été mis à jour et est abandonné
     * On inhibe 
     */
    /*
    private String importStockTypecsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        Products pR;
        List<Products> pRList;
        StockLevel stockLevel;
        int ok = 0, stockType;
        // Obtenir la liste des camions
        // TODO très spécifique OSE le rgex des locations concerné pourrait passer en paramètre
        // d'un autre côté on est surtout sur de la reprise de données
        List<Locations> listpdv = locationDAO.findAllParentLocations(true, null, false , false);
        List<Locations> listCamions = new ArrayList<Locations>();
        for (Locations pdv : listpdv) {
            if (pdv.getId().matches("2...")) {
                listCamions.add(pdv);
            }
        }
        Collections.sort(listCamions, new Comparator<Locations>() {
            public int compare(Locations loc1, Locations loc2) {
                return loc1.getId().compareTo(loc2.getId());
            }
        });
        
        for (String ligne : content.stocktypes) {
            elements = ligne.split(";");
            if (elements.length > listCamions.size() && elements[0].length() != 0) {
                pRList = productsDAO.findByReference(elements[0]);
                if (!pRList.isEmpty()) {
                    pR = pRList.get(0);
                    for (int i = 0; i < listCamions.size(); i++) {
                        try {
                            stockType = Integer.parseInt(elements[i + 1]);
                            // on pousse la mise à jour pour cet article

                            stockLevel = stockLevelDAO.findByCriteria(pR.getId(), listCamions.get(i).getId());
                            if (stockLevel == null) {
                                stockLevel = new StockLevel();
                                stockLevel.setId(listCamions.get(i).getId().concat("-").concat(pR.getId()));
                                stockLevel.setLocations(listCamions.get(i));
                                stockLevel.setProducts(pR);
                                stockLevelDAO.save(stockLevel);
                            }
                            if (stockLevel.getStockSecurity() != (double) stockType) {
                                stockLevel.setStockSecurity(stockType);
                                stockLevel.setLastUpdate(new Date());
                                ok++;
                                stockLevelDAO.save(stockLevel);
                            }

                        } catch (NumberFormatException ex) {

                        }
                    }
                }
            }
        }

        if (ok != 0) {
            retour = String.format("%d stock-types produits insérés ", ok);
        }
        return retour;
    }
    */

    private String importStocksPlaces(String locationId, EdiContentDTO content) {
        String retour = "";
        String[] elements;
        Products pR;
        List<Products> pRList;
        List<PosMessages> msgList;
        PosMessages tmpMsg;
        Locations lieu;
        int ok = 0;
        Locations serverLocation = locationDAO.findTopByServerDefaultTrue();

        if (locationId != null) {
            lieu = locationDAO.findById(locationId).orElse(null);
            for (String ligne : content.emplacements) {
                elements = ligne.split(";");
                if (elements.length == 2 && elements[1].length() != 0) {
                    pRList = productsDAO.findByReference(elements[0]);
                    if (!pRList.isEmpty()) {
                        pR = pRList.get(0);
                        // On recup les messages.
                        msgList = pR.getMessages();
                        // On recup le message en cours
                        tmpMsg = null;
                        if (msgList != null && msgList.size() > 0) {
                            for (PosMessages msg : msgList) {
                                if (msg.getType().getType().equals(PosMessageType.Values.EMPL) && msg.getLocation() != null
                                        && msg.getLocation().getId().equals(locationId)) {
                                    tmpMsg = msg;
                                    break;
                                }
                            }
                        }

                        // Cas où il y avait un message d'emplacement -> on le remplace
                        if ((tmpMsg != null) && (!tmpMsg.getContent().equals(elements[1]))) {
                            msgList.remove(tmpMsg);
                            posMessagesDAO.delete(tmpMsg);
                            tmpMsg = null;
                        }

                        if (tmpMsg == null) {
                            tmpMsg = new PosMessages();
                            tmpMsg.setProduct(pR);
                            tmpMsg.setLocation(lieu);
                            tmpMsg.setStartDate(new Date());
                            pR.setLastUpdate(new Date());
                            tmpMsg.setContent(elements[1]);
                            tmpMsg.setType(posMessageTypeDAO.findById(PosMessageType.Values.EMPL).orElse(null));
                            // tmpMsg.createId();
                            tmpMsg.setId(posMessagesDAO.getNextId(serverLocation == null ? null : serverLocation.getId()));
                            msgList.add(tmpMsg);
                            pR.setMessages(msgList);

                            productsDAO.save(pR);
                            ok++;
                        }
                    }
                }

            }
            if (ok != 0) {
                retour = String.format("%d emplacement produits insérés pour le lieu %s", ok, lieu.getName());
            }
        }
        return retour;
    }

    /**
     * Dans un premier temps il s'agit de permettre de mettre à jour 'brutalement' le niveau de stock du siège Afin de rendre l'information disponible dans les
     * points de vente En pratique on exploitera le type de mouvement de stock 'RESET' pour conserver la cohérence et l'information entre stockDiary et
     * StockLevel
     * 
     * @param locationId
     * @return une chaine décrivant l'import
     */
    private String importLocationsStocks(String locationId, EdiContentDTO content) {
        String retour = "";
        String[] elements;
        // Long pRList;
        List<Products> pRList;
        Locations lieu;
        int ok = 0;
        int row = 0;

        if (locationId != null && "0".equals(locationId)) {
            lieu = locationDAO.findById(locationId).orElse(null);
            for (String ligne : content.infos_stocks) {
                elements = ligne.split(";");

                if (elements.length == 3) {
                    // pRList = productsDAO.checkExist(elements[1], null);
                    // if (pRList != 0L)
                    pRList = productsDAO.findByReference(elements[1]);
                    if (!pRList.isEmpty()) {
                        if (stocksService.resetStock(locationId, elements[1], Double.parseDouble(elements[2])) != false) {
                            ok++;
                        }
                    }
                }
                if (elements.length == 4) {
                    if (row++ == 0) {
                        lieu = locationDAO.findById(elements[3]).orElse(null);
                    }
                    // pRList = productsDAO.checkExist(elements[1], null);
                    // if (pRList != 0L)
                    pRList = productsDAO.findByReference(elements[1]);
                    if (!pRList.isEmpty()) {
                        if (stocksService.resetStock(elements[3], elements[1], Double.parseDouble(elements[2])) != false) {
                            ok++;
                        }
                    }
                }
            }
            
            stocksService.saveStocksMouvements();
            stocksService.adjustMovements();
            
            if (ok != 0) {
                retour = String.format("%d niveaux de stock produit insérées pour le lieu %s", ok, lieu.getName());
            }
        }
        return retour;
    }

    private String importUniversCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        int ok = 0;
        Categories categorie;
        boolean alreadyExist = true;

        for (String ligne : content.univers) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                categorie = categoriesDAO.findById(elements[1]).orElse(null);
                if (categorie == null) {
                    alreadyExist = false;
                    categorie = new Categories();
                    categorie.setId(elements[1]);
                }
                categorie.setName(elements[2]);

                if (alreadyExist) {
                    categoriesDAO.save(categorie);
                } else {
                    categoriesDAO.save(categorie);
                }
                ok++;
            }
        }
        if (ok != 0) {
            retour = String.format("%d univers produits insérés ", ok);
        }
        return retour;
    }

    private String importProductUniversCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        Categories categorie;
        Products pR;
        List<Products> pRList;

        for (String ligne : content.prod_categ) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                categorie = categoriesDAO.findById(elements[2]).orElse(null);
                pRList = productsDAO.findByReference(elements[1]);
                if (!pRList.isEmpty() && categorie != null) {
                    pR = pRList.get(0);
                    if (pR.setCategory(categorie)) {
                        pR.setLastUpdate(new Date());
                        productsDAO.save(pR);
                    }
                }

            }
        }
        return retour;
    }

    private String importProductsCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        Products pR;
        List<Products> pRList;
        HashMap<String, Products> newPr, updPr;
        List<PosMessages> msgList;
        PosMessages tmpMsg;
        int ok = 0;
        boolean modification = false;
        Locations serverLocation = null;

        String nextPosMessage = null;
        Categories defaultCateg = null;
        PosMessageType alertMessageType = null;
        HashMap<String, TaxCategories> taxesCateg = new HashMap<String, TaxCategories>();
        HashMap<String, Taxes> taxes = new HashMap<String, Taxes>();
        Long nextPosMessageNb = 0L;

        if (!content.articles.isEmpty()) {
            serverLocation = locationDAO.findTopByServerDefaultTrue();
            nextPosMessage = posMessagesDAO.getNextId(serverLocation == null ? null : serverLocation.getId());
            defaultCateg = categoriesDAO.findById(Categories.DEFAULT_CATEGORY).orElse(null);
            alertMessageType = posMessageTypeDAO.findById(PosMessageType.Values.ALERT).orElse(null);
            taxes.put("", taxesService.findCurrentByCategoryAndCustCategory(null, null));

            nextPosMessageNb = (serverLocation == null ? Long.parseLong(nextPosMessage)
                    : Long.parseLong(nextPosMessage.substring(1 + serverLocation.getId().length())));
        }

        newPr = new HashMap<String, Products>();
        updPr = new HashMap<String, Products>();
        for (String ligne : content.articles) {
            elements = ligne.split(";");
            modification = false;
            if (elements.length == 9) {
                pRList = productsDAO.findByReference(elements[1]);
                if (pRList.isEmpty()) {
                    pR = new Products();
                    pR.setId(elements[1]);
                    // On passe le code d'identification par défaut au code produit
                    pR.setCode(elements[1]);
                    modification = true;

                } else {
                    pR = pRList.get(0);
                }
                // string code, string libelle, string cdeTVA, string prix1, string prix2, string prix3, string comment, string ecoTaxe
                modification = pR.setReference(elements[1]) || modification;

                modification = pR.setName(elements[2]) || modification;
                // Trouver un moyen propre de passer de la cat de TVA à un autre
                if (!taxesCateg.containsKey(elements[3])) {
                    if (elements[3].length() == 1) {
                        modification = pR.setTaxCategory(taxCategoriesDAO.findByName(oldIdMap.get(elements[3]))) || modification;
                    } else {
                        modification = pR.setTaxCategory(taxCategoriesDAO.findByName(elements[3])) || modification;
                    }
                    taxesCateg.put(elements[3], pR.getTaxCategory());
                    if (pR.getTaxCategory() != null && !taxes.containsKey(pR.getTaxCategory().getId())) {
                        taxes.put(pR.getTaxCategory().getId(), taxesService.findCurrentByCategoryAndCustCategory(pR.getTaxCategory(), null));
                    }

                } else {
                    modification = pR.setTaxCategory(taxesCateg.get(elements[3])) || modification;
                }

                // faire la même chose avec element8 pour l'ecotaxe
                // mais là on pousse dans la table producttaxcateg
                // TODO implementer tous ce qui est producttaxcateg

                // Attention à prendre en compte la DEEE qui est incluse dans le prix de vente final du fichier

                try {
                modification = pR.setPriceSell(productsService.getSellPrice(Double.parseDouble(elements[4]),
                        taxes.get(pR.getTaxCategory() != null ? pR.getTaxCategory().getId() : "") , pR) ) || modification;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                // EDU elements 5 PMPA - StockCost
                // EDU elements 6 Dernier PA - PriceBuy
                if (elements[5].length() > 0) {
                    modification = pR.setStockCost(Double.parseDouble(elements[5])) || modification;
                }
                if (elements[6].length() > 0) {
                    modification = pR.setPriceBuy(Double.parseDouble(elements[6])) || modification;
                }
                if (pR.getCategory() == null) {
                    modification = pR.setCategory(defaultCateg) || modification;
                }

                if (pR.getProductsCategories() == null || pR.getProductsCategories().isEmpty()) {
                    List<ProductsCategory> pcList = new ArrayList<>();
                    pcList.add(new ProductsCategory());
                    pcList.get(0).setProducts(pR);
                    pR.setProductsCategories(pcList);
                }
                // On recup les messages.
                msgList = pR.getMessages();
                // On recup le message en cours
                tmpMsg = null;
                if (msgList != null) {
                    for (PosMessages msg : msgList) {
                        if (msg.getType().getType().equals(PosMessageType.Values.ALERT) && (msg.getEndDate() == null)) {
                            tmpMsg = msg;
                            break;
                        }
                    }
                } else {
                    msgList = new ArrayList<PosMessages>();
                }

                if (tmpMsg == null || !(tmpMsg.getContent().equals(elements[7]))) {
                    // Si le nouveau est différent -> on ferme en cours
                    if (tmpMsg != null) {
                        tmpMsg.setEndDate(new Date());
                        posMessagesDAO.save(tmpMsg);
                    }
                    // Si le nouveau a du contenu, on l'ajoute à la liste.
                    if (!elements[7].isEmpty()) {
                        tmpMsg = new PosMessages();
                        tmpMsg.setProduct(pR);
                        tmpMsg.setContent(elements[7]);
                        tmpMsg.setStartDate(new Date());

                        tmpMsg.setType(alertMessageType);
                        // tmpMsg.createId();
                        tmpMsg.setId(serverLocation == null ? String.format("%07d", nextPosMessageNb)
                                : String.format("%s-%07d", serverLocation.getId(), nextPosMessageNb));
                        nextPosMessageNb++;
                        msgList.add(tmpMsg);
                    }
                    pR.setMessages(msgList);
                    modification = true;
                }

                if (modification) {
                    pR.setLastUpdate(new Date());
                }

                if (pRList.isEmpty()) {
                    // productsDAO.save(pR);
                    newPr.put(pR.getId(), pR);
                    // retour = retour.concat("insertion de mon produit ").concat(pR.getId());
                } else {
                    // productsDAO.save(pR);
                    updPr.put(pR.getId(), pR);
                    // retour = retour.concat("update de mon produit ").concat(pR.getId());
                }

                ok++;
            }
        }
        pRList = new ArrayList<Products>(newPr.values());
        if (pRList.size() > 0) {
            productsDAO.saveAll(pRList);
        }
        pRList = new ArrayList<Products>(updPr.values());
        if (pRList.size() > 0) {
            productsDAO.saveAll(pRList);
        }
        if (ok > 0) {
            retour += String.format("%d produits insérés ", ok);
        }
        return retour;

    }

    private String importCompositionsCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<Products> product;
        List<Products> composants;
        Products composant;
        Products pR;
        double quantity;
        boolean remove = false;
        List<ProductsComponents> components;
        ProductsComponents found;
        int ok = 0;

        for (String ligne : content.compositions) {
            elements = ligne.split(";");
            if (elements.length == 6 || elements.length == 4) {
                product = productsDAO.findByReference(elements[1]);
                composants = productsDAO.findByReference(elements[2]);
                quantity = Double.parseDouble(elements[3]);
                if (!product.isEmpty() && !composants.isEmpty()) {
                    // vérifier si il dispose d'un composant adéquat avec la bonne quantité
                    pR = product.get(0);
                    components = pR.getComponentsProducts();
                    found = null;
                    remove = false;
                    for (ProductsComponents pC : components) {
                        if (pC.getComponent().getReference().equals(elements[2])) {
                            found = pC;
                            if (pC.getQuantity() != quantity) {
                                remove = true;
                            }

                            break;
                        }
                    }
                    if (remove) {
                        components.remove(found);
                        productsComponentsDAO.delete(found);
                        found = null;
                    }

                    if (found == null) {
                        try {
                            composant = composants.get(0);
                            found = new ProductsComponents();
                            found.setMaster(pR);
                            found.setQuantity(quantity);
                            found.setComponent(composants.get(0));
                            productsComponentsDAO.save(found);
                            composant.setLastUpdate(new Date());
                            composant.getMastersProducts().add(found);
                            productsDAO.saveAndFlush(composant);// !Attention , il faut pas changer l'ordre si non probleme de persistence
                            components.add(found);
                            pR.setLastUpdate(new Date());
                            pR.setComponentsProducts(components);
                            productsDAO.saveAndFlush(pR);
                            ok++;
                        } catch (PersistenceException ex) {
                            // l'association a été créé dans l'intervalle
                            // TODO faire ça mieux - Testing purpose
                            System.err.print("Cas d'une insertion déjà réalisée");
                            remove = false;
                            for (ProductsComponents pC : components) {
                                if (pC.getComponent().getReference().equals(elements[2])) {
                                    found = pC;
                                    if (pC.getQuantity() != quantity) {
                                        remove = true;
                                    }

                                    break;
                                }
                            }
                            if (remove) {
                                components.remove(found);
                                productsComponentsDAO.delete(found);
                                found = null;
                            }
                            composant = composants.get(0);
                            found = new ProductsComponents();
                            found.setMaster(pR);
                            found.setQuantity(quantity);
                            found.setComponent(composants.get(0));
                            productsComponentsDAO.save(found);
                            composant.setLastUpdate(new Date());
                            composant.getMastersProducts().add(found);
                            productsDAO.saveAndFlush(composant);// !Attention , il faut pas changer l'ordre si non probleme de persistence
                            components.add(found);
                            pR.setLastUpdate(new Date());
                            pR.setComponentsProducts(components);
                            productsDAO.saveAndFlush(pR);
                            ok++;
                        }
                    }

                }
            }
        }

        retour = String.format("%d détails de compositions insérés ", ok);
        return retour;

    }

    private String importTarrifPricecsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<POSTariffAreaPriceDTO> list = new ArrayList<POSTariffAreaPriceDTO>();
        POSTariffAreaPriceDTO price;
        int ok = 0;
        for (String ligne : content.infos_cat_price) {
            elements = ligne.split(";");
            if (elements.length == 4) {
                price = new POSTariffAreaPriceDTO();
                price.setArreaId(Long.parseLong(elements[1]));
                price.setProductId(elements[2]);
                price.setPrice(Double.parseDouble(elements[3]));

                list.add(price);

            }
        }
        ok = saveTarifPrice(list);
        retour = String.format("%d Prix catalogues insérés ", ok);

        return retour;
    }

    private String importDefaultPricecsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<Products> product;
        Products pR;

        int ok = 0;
        for (String ligne : content.infos_prix_defaut) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                product = productsDAO.findByReference(elements[1]);
                if (!product.isEmpty()) {
                    pR = product.get(0);
                    if (pR.setPriceSell(Double.parseDouble(elements[2]))) {
                        pR.setLastUpdate(new Date());
                        productsDAO.save(pR);

                        ok++;
                    }
                }
            }
        }
        retour = String.format("%d Prix par défaut de produits insérés ", ok);

        return retour;
    }

    private String importLastPriceBuycsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<Products> product;
        Products pR;

        int ok = 0;
        for (String ligne : content.infos_pa) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                product = productsDAO.findByReference(elements[1]);
                if (!product.isEmpty()) {
                    pR = product.get(0);
                    if (pR.setPriceBuy(Double.parseDouble(elements[2]))) {
                        pR.setLastUpdate(new Date());
                        productsDAO.save(pR);

                        ok++;
                    }
                }
            }
        }
        retour = String.format("%d PA de produits insérés ", ok);

        return retour;
    }

    private String importLastPMPAcsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<Products> product;
        Products pR;

        int ok = 0;

        for (String ligne : content.infos_pmpa) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                product = productsDAO.findByReference(elements[1]);
                if (!product.isEmpty()) {
                    pR = product.get(0);
                    if (pR.setStockCost(Double.parseDouble(elements[2]))) {
                        pR.setLastUpdate(new Date());
                        productsDAO.save(pR);

                        ok++;
                    }
                }
            }
        }

        retour = String.format("%d PMPA de produits insérés ", ok);

        return retour;
    }

    private String importReferencesCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<Products> listProducts;
        Products produit;
        ProductReferences ref;
        List<ProductReferences> refList;
        Map<String, ProductReferences> newRef = new HashMap<String, ProductReferences>();
        Map<String, Products> updProd = new HashMap<String, Products>();

        int ok = 0;

        for (String ligne : content.references) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                listProducts = productsDAO.findByReference(elements[1]);
                if (!listProducts.isEmpty()) {
                    produit = listProducts.get(0);
                    refList = productReferencesDAO.findByProductAndReference(produit, elements[2]);
                    if (refList.isEmpty()) {
                        ref = new ProductReferences();
                        /*
                         * if((product.get(0).getCode() == null ) ||( product.get(0).getCode() == product.get(0).getReference())) {
                         * product.get(0).setCode(elements[2]); productsDAO.save(product.get(0)); }
                         */
                        ref.setProduct(produit);
                        ref.setReference(elements[2]);

                        // productReferencesDAO.save(ref);
                        newRef.put(produit.getId().concat(elements[2]), ref);
                        produit.setLastUpdate(new Date());
                        // productsDAO.save(produit);
                        updProd.put(produit.getId(), produit);
                        ok++;
                    }
                }
            }
        }
        if (ok > 0) {
            productReferencesDAO.saveAll(new ArrayList<ProductReferences>(newRef.values()));
            productsDAO.saveAll(new ArrayList<Products>(updProd.values()));
        }
        retour = String.format("%d références de produits insérés ", ok);
        return retour;

    }

    private String importSalesConstraintsCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        Products pR;
        List<Products> pRList;
        int ok = 0;

        for (String ligne : content.blocages) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                pRList = productsDAO.findByReference(elements[1]);
                if (!pRList.isEmpty()) {
                    pR = pRList.get(0);
                    if (pR.setCom("0".equals(elements[2]))) {
                        pR.setLastUpdate(new Date());
                        productsDAO.save(pR);
                    }

                    ok++;
                }
            }
        }
        retour = String.format("%d infos de blocages insérés ", ok);
        return retour;

    }

    private String importSalesSessionsCsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<SalesLocations> removalList;

        SalesLocations sL;
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.set(Calendar.MILLISECOND, 0);
        c2.set(Calendar.MILLISECOND, 0);
        int ok = 0;

        for (String ligne : content.deballages) {
            elements = ligne.split(";");
            Tours tour = toursDAO.findById(Long.parseLong(elements[6])).orElse(null);
            if (elements.length == 10 && tour != null) {
                c1.set(Integer.parseInt(elements[4]), Integer.parseInt(elements[5]) - 1, Integer.parseInt(elements[7]),
                        Integer.parseInt(elements[8].substring(elements[8].length() - 4, elements[8].length() - 2)),
                        Integer.parseInt(elements[8].substring(elements[8].length() - 2)), 0);
                c2.set(Integer.parseInt(elements[4]), Integer.parseInt(elements[5]) - 1, Integer.parseInt(elements[7]),
                        Integer.parseInt(elements[8].substring(0, 2)), Integer.parseInt(elements[8].substring(2, 4)), 0);

                // On part du principe que si on a un autre déballage prévu dans le futur sur la même tournée
                // avec lequel ce déballage présent empiète, alors celui qui est en base doit être écrasé
                if (c1.after(Calendar.getInstance())) {
                    MultiValueMap<String ,String> criteres = new LinkedMultiValueMap<>();
                    criteres.add(SalesLocations.Fields.TOUR , elements[6]);
                    criteres.add(SalesLocations.SearchCritria.TO , c1.toInstant().toString());
                    criteres.add(SalesLocations.SearchCritria.FROM , c2.toInstant().toString());
                    removalList = salesLocationsService.getByCriteria(criteres, null);
                    for (SalesLocations removeLocation : removalList) {
                        salesLocationsDAO.delete(removeLocation);
                    }

                    sL = new SalesLocations();
                    sL.setCity(elements[2]);
                    sL.setEndDate(c1.getTime());
                    // sL.setFee(fee);
                    sL.setInsee(inseeDAO.findById(elements[1]).orElse(null));
                    // sL.setPaid(false);
                    sL.setPubNb(Integer.parseInt(elements[9]));
                    sL.setStartDate(c2.getTime());
                    sL.setTour(tour);
                    sL.createId();
                    salesLocationsDAO.save(sL);
                    ok++;
                }
            }
        }

        retour = String.format("%d déballages insérés ", ok);
        return retour;

    }

    /**
     * 
     * @param customer
     *            le client dont on souhaite obtenir la représentation
     * @return la chaine représentant ce client
     */
    // private String getCustomerExportString(CustomersDTO customer) {
    // String response = "";
    //
    // return response;
    // }

    /**
     * 
     * @param ticket
     *            le ticket dont on souhaite obtenir la représentation
     * @return la chaine représentant le ticket Un exemple : T/B ( K/L quand non validé )
     *
     *         T;005164;15:52;A;22042015;1;16001830;0.00;0;49.97;2;3;33540;0.00;49.97;0.00;0.00;0.00;0.00;0;; B;1844;1;0;0;0;29.99;0.00 B;0269;2;0;0;0;9.99;0.00
     *         A;BUSSIERE JEAN LOUIS A; A;12 CHEMIN DU LOC DU GRAND RAMON A;33590 GRAYAN ET LHOPITAL A;0557752413
     */
    //EDU modification - On ne tient pas compte du caractère commande pour certaines lignes du ticket
    //TODO voir pour ne le faire QUE Dans les conditions suivantes :
    //Tickets liés 
    //Même client
    //même prouit
    //même qté
    private String getTicketExportString(Tickets ticket , boolean ignoreOrder) {
        String response = "";
        String separator = System.getProperty("line.separator");
        String cmc7 = "";
        SimpleDateFormat hhMM = new SimpleDateFormat("HH:mm");
        SimpleDateFormat ddMMyyyy = new SimpleDateFormat("ddMMyyyy");
        ArrayList<String> tmpelements = new ArrayList<String>();
        List<Payments> reglements = ticket.getReceipts().getPayments();
        List<TicketLines> detail = ticket.getLines();
        SalesLocations deballage = null;
        Customers client = null;
        BigDecimal quantite;
        BigDecimal chk, cash, cc, tot, div, promo, back, articles;

        tot = new BigDecimal("0.0");
        chk = new BigDecimal("0.0");
        cash = new BigDecimal("0.0");
        cc = new BigDecimal("0.0");
        div = new BigDecimal("0.0");
        promo = new BigDecimal("0.0");
        back = new BigDecimal("0.0");
        articles = new BigDecimal("0.0");

        // On fait le tour des listes reglement et détail
        // TODO Virer les chaines en dur
        // TODO gérer le cas des promos ! = remise directes
        // TODO trouver comment est géré le rendu de monnaie
        // TODO Attention pas de notion de Prix total TTC donc on fait Sigma payments
        for (Payments regl : reglements) {
            BigDecimal amount = BigDecimal.valueOf(regl.getTotal());
            tot = tot.add(amount);
            switch (regl.getPayment()) {
            case Payments.Type.MAGCARD:
                cc = cc.add(amount);
                break;
            case Payments.Type.CHEQUE:
                chk = chk.add(amount);
                if (regl.getTransId() != null) {
                    // if(cmc7.length() > 0) {
                    // cmc7 = cmc7.concat("--");
                    // }
                    // cmc7 = cmc7.concat(regl.getTransId());
                    // Modif EDU 16/08/2017 -> il y a une taille maxi pour le cmc7 dans ofbiz
                    if (cmc7.length() == 0) {
                        cmc7 = cmc7.concat(regl.getTransId());
                    }
                }
                break;
            case Payments.Type.CASH:
                cash = cash.add(amount);
                break;
            default:
                div = div.add(amount);
            }
        }

        promo = BigDecimal.valueOf(ticket.getTicketsManager().getDiscountAmount());
        // EDU 2016 06 : le total s'entend promo incluses
        tot = tot.add(promo);

        for (TicketLines ligne : detail) {
            articles = articles.add(BigDecimal.valueOf(ligne.getUnits()));

        }
        //

        // Identifiant T pour un ticket normal B si il n'a pas été validé / pas imprimé
        tmpelements.add("T");
        // N° Ticket
        // Modif EDU 08/07/2016 -> SYG n'accept que 6 CHAR sur les tickets
        String ticketId = String.valueOf(ticket.getTicketId());
        ticketId = ticketId.substring(ticketId.length() - 6);
        tmpelements.add(ticketId);
        // heure
        tmpelements.add(hhMM.format(ticket.getReceipts().getDateNew()));
        // TypeVente toujours A ?
        tmpelements.add("A");
        // Jour
        tmpelements.add(ddMMyyyy.format(ticket.getReceipts().getDateNew()));
        // N° Tournée
        tmpelements.add(ticket.getReceipts().getClosedCash().getCashRegister().getLocation().getId());
        // TODO CodeHoraire - Utilisé ?
        tmpelements.add("");
        // TODO Port - Toujours à 0 mais utilisable
        tmpelements.add("0");
        // TODO Remise - Toujours à 0 mais utilisable
        tmpelements.add("0");
        // Total
        tmpelements.add(tot.setScale(2, RoundingMode.HALF_UP).toString());
        // Nb Lignes = nb de postes
        tmpelements.add(Integer.toString(detail.size()));
        // Nb Articles = sigma quantité
        tmpelements.add(articles.setScale(0, RoundingMode.HALF_UP).toString());
        // NumINSEE
        tmpelements.add("");
        // Regl Chèques
        tmpelements.add(chk.setScale(2, RoundingMode.HALF_UP).toString());
        // Regl Espèces
        tmpelements.add(cash.setScale(2, RoundingMode.HALF_UP).toString());
        // Regl Cartes
        tmpelements.add(cc.setScale(2, RoundingMode.HALF_UP).toString());
        // Regl Divers
        tmpelements.add(div.setScale(2, RoundingMode.HALF_UP).toString());
        // Regl Promo
        tmpelements.add(promo.setScale(2, RoundingMode.HALF_UP).toString());
        // Rendu (signe +)
        tmpelements.add(back.setScale(2, RoundingMode.HALF_UP).toString());
        // Commande ( 0 pour Faux 1 pour Vrai)
        // On mets systématiquement 0 et on gère au niveau des lignes
        tmpelements.add("0");
        // ChqCMC7
        // EDU Modif 2016-08-23 : Ajout CMC7 si dispo
        tmpelements.add(cmc7);

        // TODO Revoir cette modif en temps utile
        // EDU Modif 2016-08-23 : Suppression n° client qui pose peut être problème dans syg
        // NumClient - N° de carte

        client = ticket.getCustomer();
        
        //EDU Modif 2021 04 : Test avec ticket extended si jamais on n'a pas de client
        if(client == null) {
            TicketsExtended extended = ticketsExtendedDAO.findById(ticket.getId()).orElse(null);
            if(extended != null) {
                client = customersDAO.findById(extended.getCustomerId()).orElse(null);
            }
        }

        /*
         * if(client != null) { tmpelements.add(client.getCard()); } else { tmpelements.add(""); }
         */
        tmpelements.add("");

        // Au cas où on met les Code Insee et Autres Info à leur place
        deballage = ticket.getReceipts().getClosedCash().getSalesLocation();
        if (deballage != null) {
            tmpelements.set(12, deballage.getInseeNum());
            tmpelements.set(5, String.valueOf(ticket.getReceipts().getClosedCash().getSalesLocation().getTour().getId()));
        }
        response = String.join(";" , tmpelements);

        if (client != null) {
            response += separator.concat("A;".concat(client.getName()));
            response += separator.concat("A;");
            if (client.getAddress() != null) {
                response += client.getAddress();
            }
            response += separator.concat("A;");
            if (client.getAddress2() != null) {
                response += client.getAddress2();
            }
            response += separator.concat("A;");
            if (client.getZipCode() != null) {
                response += client.getZipCode();
            }
            if (client.getCity() != null) {
                response += " ".concat(client.getCity());
            }
            response += separator.concat("A;");
            if (client.getPhone() != null) {
                response += client.getPhone();
            } else {
                if (client.getPhone2() != null) {
                    response += client.getPhone2();
                }
            }
        }

        if ((detail == null || detail.isEmpty()) && tot.compareTo(new BigDecimal(0)) != 0) {
            return null;
        }

        for (TicketLines ligne : detail) {

            if (client == null && ligne.isOrder()) {
                //Voir ici si il y a autre chose à faire dans le cas de commande sans client pour le ticket
                return null;
            }

            quantite = BigDecimal.valueOf(ligne.getUnits());

            tmpelements = new ArrayList<String>();
            // B ou L si ticket non validé
            tmpelements.add("B");
            // Ref Article
            tmpelements.add(ligne.getProduct() == null ? "9999" : ligne.getProduct().getReference());
            // Qté - Livré
            tmpelements.add("0");
            // Qté - Commandé
            tmpelements.add("0");
            // Qté - Retourné
            tmpelements.add("0");
            // Qté Repris sur commande
            tmpelements.add("0");
            // Prix - TTC
            tmpelements.add(BigDecimal.valueOf(ticketsServiceApi.getTaxedUnitPrice(ligne)).setScale(2, RoundingMode.HALF_UP).toString());
            // Ecotaxe - Montant
            tmpelements.add(BigDecimal.valueOf(ligne.getUnits() * productsService.getFullAdditionnalTaxes(ligne.getProduct())).setScale(2, RoundingMode.HALF_UP).toString());

            // On va mettre au bon endroit la quantité
            if (quantite.doubleValue() < 0) {
                quantite = quantite.negate();
                if ( !ignoreOrder && ligne.isOrder()) {
                    tmpelements.set(5, quantite.setScale(0, RoundingMode.HALF_UP).toString());
                } else {
                    tmpelements.set(4, quantite.setScale(0, RoundingMode.HALF_UP).toString());
                }
            } else {
                if (!ignoreOrder && ligne.isOrder()) {
                    tmpelements.set(3, quantite.setScale(0, RoundingMode.HALF_UP).toString());
                } else {
                    tmpelements.set(2, quantite.setScale(0, RoundingMode.HALF_UP).toString());
                }
            }

            response += separator.concat(String.join(";" , tmpelements));
        }
        /*
         * Header Ticket sqlCeCommand.CommandText = "CREATE TABLE Vente(NumTicket Int PRIMARY KEY, 0 NumINSEE Int NOT NULL, 1 DateHeure DateTime, 2 TypeVente
         * NVarChar(1), 3 CodeHoraire NVarChar(16), 4 NumTournee Int, 5 NbLignes Int, 6 NbArticles Int, 7 Port Money, 8 Remise Float, 9 NomClient NText, 10
         * ChqCMC7 NVarChar(40), 11 ReglChq Money, 12 ReglEsp Money, 13 ReglCB Money, 14 ReglPromo Money, 15 ReglDivers Money, 16 Rendu Money, 17 Total Money,
         * 18 FlagCmde Bit, 19 Adr1Client NText, 20 Adr2Client NText, 21 Adr3Client NText, 22 Annule Bit, 23 NumClient NVarChar(13), 24 numTel NVarChar(15), 25
         * imprAndValid Bit)"; 26 if (sqlCeDataReader3.GetBoolean(26)) = imprimé et validé { text6 = "T"; text7 = "B"; } else { text6 = "K"; text7 = "L"; }
         * streamWriter.WriteLine(string.Concat(new string[] { text6, text, sqlCeDataReader3.GetInt32(0).ToString("d06"), text,
         * sqlCeDataReader3.GetDateTime(2).ToString("HH:mm", null), text, sqlCeDataReader3.GetString(3), text,
         * sqlCeDataReader3.GetDateTime(2).ToString("ddMMyyyy", null), text, sqlCeDataReader3.GetInt32(5).ToString(), text, sqlCeDataReader3.GetString(4), text,
         * sqlCeDataReader3.GetSqlMoney(8).ToDouble().ToString("f02", this.NbFrmtInf), text, sqlCeDataReader3.GetDouble(9).ToString("f00", this.NbFrmtInf),
         * text, sqlCeDataReader3.GetSqlMoney(18).ToDouble().ToString("f02", this.NbFrmtInf), text, sqlCeDataReader3.GetInt32(6).ToString(), text,
         * sqlCeDataReader3.GetInt32(7).ToString(), text, sqlCeDataReader3.GetInt32(1).ToString("d05"), text,
         * sqlCeDataReader3.GetSqlMoney(12).ToDouble().ToString("f02", this.NbFrmtInf), text, sqlCeDataReader3.GetSqlMoney(13).ToDouble().ToString("f02",
         * this.NbFrmtInf), text, sqlCeDataReader3.GetSqlMoney(14).ToDouble().ToString("f02", this.NbFrmtInf), text,
         * sqlCeDataReader3.GetSqlMoney(16).ToDouble().ToString("f02", this.NbFrmtInf), text, sqlCeDataReader3.GetSqlMoney(15).ToDouble().ToString("f02",
         * this.NbFrmtInf), text, num.ToString("f02", this.NbFrmtInf), text, num2.ToString(), text, text5, text, sqlCeDataReader3.GetString(24) })); if (text6
         * == "T" && (!sqlCeDataReader3.GetString(10).Equals("") || !sqlCeDataReader3.GetString(20).Equals("") || !sqlCeDataReader3.GetString(21).Equals("") ||
         * !sqlCeDataReader3.GetString(22).Equals(""))) { streamWriter.WriteLine("A" + text + sqlCeDataReader3.GetString(10).Trim()); streamWriter.WriteLine("A"
         * + text + sqlCeDataReader3.GetString(20).Trim()); streamWriter.WriteLine("A" + text + sqlCeDataReader3.GetString(21).Trim());
         * streamWriter.WriteLine("A" + text + sqlCeDataReader3.GetString(22).Trim()); streamWriter.WriteLine("A" + text +
         * sqlCeDataReader3.GetString(25).Trim()); }
         */
        // response = StringUtils.join(tmpelements, ';');
        /**
         * Détail Ticket sqlCeCommand.CommandText = "CREATE TABLE LigneVente( NumTicket Int NOT NULL, 0 NumLigne Int NOT NULL, 1 CodeArticle NVarChar(13), 2
         * LibArticle NVarChar(50), 3 QteArticle Int, 4 PrixTTC Money, 5 TauxTVA Float, 6 CodeTVA Int, 7 Livre Bit, 8 Commande Bit, 9 Retourne Bit, 10 EcoTaxe
         * Money)"; 11 int num3 = 0; int num4 = 0; int num5 = 0; int num6 = 0; if (sqlCeDataReader4.GetBoolean(10)) { if (sqlCeDataReader4.GetBoolean(9)) { num6
         * = sqlCeDataReader4.GetInt32(4); } else { num5 = sqlCeDataReader4.GetInt32(4); } } else { if (sqlCeDataReader4.GetBoolean(9)) { num4 =
         * sqlCeDataReader4.GetInt32(4); } else { num3 = sqlCeDataReader4.GetInt32(4); } } streamWriter.WriteLine(string.Concat(new string[] { text7, text,
         * sqlCeDataReader4.GetString(2), text, num3.ToString(), text, num4.ToString(), text, num5.ToString(), text, num6.ToString(), text,
         * sqlCeDataReader4.GetSqlMoney(5).ToDouble().ToString("f02", this.NbFrmtInf), text, sqlCeDataReader4.GetSqlMoney(11).ToDouble().ToString("f02",
         * this.NbFrmtInf) }));
         */
        return response;
    }

    /**
     * Il faudra gérer le récap des déballages
     * C;33540;A;2015;04;1;22;16001830;18:43;18:44;3;105.95;9;308.86;15;597.58;0;0.00;0;0.00;1110;68;1012.39;9999;27;149.50;310.00
     * 
     * La fin du fichier ****FIN_DU_FICHIER****
     * 
     * Les annulations qui vont ailleurs
     */
    @Override
    public Object exportFile(String file, ExportConstraintsDTO dto) {
        // TODO Finir d'implémenter
        String filecontent;
        // long mostRecent = 0 , tmpMostRecent = 0;
        ArrayList<String> str = new ArrayList<String>();
        Map< String, ArrayList<String> > closedCashStr = new HashMap<String, ArrayList<String>>();
        ArrayList<String> closedCashErrors = new ArrayList<String>();
        Date dateStart, dateStop;
        String dateStartString, dateStopString;
        String separator = System.getProperty("line.separator");
        List<Tickets> tickets;
        Locations defaultLocation = locationDAO.findTopByServerDefaultTrue();

        // il faut exporter les ventes
        dateStart = dto.getSalesFrom();
        if (dateStart != null) {
            dateStartString = Long.toString((long) (dateStart.getTime()));
        } else {
            dateStartString = null;
        }
        dateStop = dto.getSalesTo();
        if (dateStop != null) {
            dateStopString = Long.toString((long) (dateStop.getTime()));
        } else {
            dateStopString = null;
        }

        // Je lis mes contraintes
        // J'applique pour retrouver les objets qui vont bien
        // Je les écris dans le fichier
        if (dto.isExportAll() || dto.isExportCust()) {
            // il faut exporter les clients
            List<Customers> localListCustomers = null;
            if ((dateStartString != null && !dateStartString.isEmpty()) || (dto.getCustCodeRegex() != null && !dto.getCustCodeRegex().isEmpty())
                    || (dateStopString != null && !dateStopString.isEmpty())) {
                localListCustomers = customersDAO.findByRegexSearchKey(dto.getCustCodeRegex(), dateStartString, dateStopString);
            } else {
                localListCustomers = customersDAO.findAll();
            }
            dto.setCustomers(AdaptableHelper.getAdapter(localListCustomers, CustomersDetailDTO.class));
            // TODO en passer par là si on souhaite faire un export sous forme de ficier plat
            if (dto.getCustomers() != null && !dto.isJsonFormat()) {
                boolean first = true;
                for (CustomersDetailDTO customer : dto.getCustomers()) {
                    if (first && !dto.isWithoutHeaders()) {
                        str.add(String.join(";" , customer.header()));
                        first = false;
                    }
                    if (!dto.isEmailsOnly() || customer.getEmail() != null) {
                        str.add(String.join(";" , customer.CSV()));
                    }
                }
            }
        }

        if (dto.isExportAll() || dto.isExportCashRegisters()) {

            if (defaultLocation != null) {
                dto.setCashes(AdaptableHelper.getAdapter(cashRegistersService.findByNameAndLocation(null, defaultLocation.getId()), CashRegistersSimpleDTO.class));
            } else {
                dto.setCashes(AdaptableHelper.getAdapter(cashRegistersDAO.findAll(), CashRegistersSimpleDTO.class));
            }

        }

        String limitedLocation = dto.getLocationId() == null ? defaultLocation == null ? null : defaultLocation.getId() : dto.getLocationId();

        // TODO Implémenter celui ci
        // Messages sens caisse -> centre + Emplacements pdv
        if (dto.isExportAll() || dto.isExportMessages()) {
            dto.setMessages(
                    AdaptableHelper.getAdapter(posMessagesDAO.findChangeByCriteria(limitedLocation, null, null, null, dateStart, dateStop), POSMessagesDTO.class));
        }

        if (dto.isExportAll() || dto.isExportSalesLocations()) {
            // TODO y revenir lorsque la recherche associée existera
            dto.setSalesLocations(salesLocationsService.getAdapter(salesLocationsDAO.findAll(), POSSalesLocationsDTO.class));
        }

        if (dto.isExportAll() || dto.isExportOrders()) {

            dto.setOrders(AdaptableHelper.getAdapter(ordersDAO.findChangesByCriteria(limitedLocation, null, dateStart, dateStop, null), OrderDTO.class));
            dto.setPalettes(AdaptableHelper.getAdapter(palettesDAO.findByCriteria(limitedLocation, dateStart, dateStop), PaletteDTO.class));
        }

        if (dto.isExportAll() || dto.isExportStocks()) {
            // TODO y revenir lorsque la recherche associée existera
            List<StockDiary> stockDiary = null;
            Set<StockCurrent> stockCurrent = new HashSet<StockCurrent>();
            if (limitedLocation == null) {
                //dto.setStockLevel(AdaptableHelper.getAdapter(stockLevelDAO.findListByCriteria(null, null, dateStart, dateStop), StockLevelDTO.class));
                dto.setStockLevel(stocksService.getStockLevelByCritria(null, null, dateStart, dateStop));
                stockDiary = stockDiaryDAO.findByCriteria(null,null,null,null,dateStartString, dateStopString,null,null,null);
                
                if (dateStop == null && dateStart == null) {
                    dto.setStockCurrent(AdaptableHelper.getAdapter(stockCurrentDAO.findAll(), StockCurrentDTO.class));
                }

            } else {
                //dto.setStockLevel(AdaptableHelper.getAdapter(
                //        stockLevelDAO.findListByCriteria(null, limitedLocation == null ? null : Arrays.asList(limitedLocation), dateStart, dateStop),
                //        StockLevelDTO.class));
                List<LocationsInfoDTO> locationsRestriction = new ArrayList<LocationsInfoDTO>();
                LocationsInfoDTO tmp = new LocationsInfoDTO();
                tmp.setId(limitedLocation);
                dto.setStockLevel(stocksService.getStockLevelByCritria(null, locationsRestriction, dateStart, dateStop));
                stockDiary = stockDiaryDAO.findByCriteria(limitedLocation, null, null, null, dateStart == null ? null : String.valueOf(dateStart.getTime()),
                        dateStop == null ? null : String.valueOf(dateStop.getTime()), 0L, 9999L, null);
                if (dateStop == null && dateStart == null) {
                    dto.setStockCurrent(AdaptableHelper.getAdapter(stocksService.findListByProductAndLocation(null, limitedLocation, null, null), StockCurrentDTO.class));
                }
            }
            if (dto.getStockCurrent() == null || dto.getStockCurrent().isEmpty()) {
                for (StockDiary element : stockDiary) {
                    List<StockCurrent> tmpstockCurrent = stocksService.findListByProductAndLocation( element.getProducts().getId() , null , null , null);
                    for (int index = tmpstockCurrent.size() - 1; index >= 0; index--) {
                        if (!tmpstockCurrent.get(index).getLocationsId().equals(element.getLocationsId())) {
                            tmpstockCurrent.remove(index);
                        }
                    }
                    stockCurrent.addAll(tmpstockCurrent);
                }
                dto.setStockCurrent(AdaptableHelper.getAdapter(new ArrayList<StockCurrent>(stockCurrent), StockCurrentDTO.class));
            }
            dto.setStockDiary(AdaptableHelper.getAdapter(stockDiary, StockDiaryDTO.class));
            if (!dto.isJsonFormat() && !(dto.getStockCurrent() == null)) {
                // TODO voir à passer sur un DTO spécifique ?
                DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
                otherSymbols.setDecimalSeparator(',');
                DecimalFormat decf = new DecimalFormat("#.##", otherSymbols);
                for (StockCurrentDTO stock : dto.getStockCurrent()) {
                    str.add(stock.getProductsId().concat(";").concat(decf.format(stock.getUnits())));
                }
            }
        }

        // TODO Il faut exploser/aggréger par lieu de déballage si fichier(s) plat
        // Il faut fournir les informations sur les sessions de ventes
        if (dto.isExportAll() || dto.isExportSales()) {
            // TODO : Atention location et CashId ne sont pas forcément synonymes
            dto.setClosedCash(AdaptableHelper.getAdapter(closedCashDAO.findByCriteria(limitedLocation, dateStartString, dateStopString), POSClosedCashDTO.class));
            if (dto.isJsonFormat()) {
                tickets = ticketsDAO.findByCriteria(null, null, null, dateStartString, dateStopString, null, null, limitedLocation, null, null, null, null,
                        null);
                dto.setTickets(AdaptableHelper.getAdapter(tickets, POSTicketsDTO.class));
            } else {

                for (POSClosedCashDTO closedCah : dto.getClosedCash()) {
                    // Attention on ne génère pas de fichier pour une session pas encore terminée
                    if (closedCah.getCloseDate() != null) {
                        //On stocke les string associées aux tickets
                        Map<Long , String> tmpstr = new HashMap<Long , String>();
                        //On stocke les tickets normaux par client en multivaluemap
                        MultiValueMap<String , Tickets> tmptickets = new LinkedMultiValueMap<String , Tickets>();
                        //On stocke les tickets correspondant à des commandes négtives
                        List<Tickets> tmpOrders = new ArrayList<Tickets>();
                        
                        Boolean ok = true;
                        tickets = ticketsDAO.findByCriteria(null, null, closedCah.getId(), null, null, null, null, null, null, null, null, null, null);
                        // TODO check si il faut gérer les DTO TIckets
                        tmpstr.put(0L , getClosedCashExportString(closedCah, tickets.isEmpty() ? "" : tickets.get(0).getPerson().getId()));
                        // TODO ajouter la ligne de récap qui va bien
                        tickets.sort(new Comparator<Tickets>() {
                            @Override 
                            public int compare(Tickets a , Tickets b) {
                                return a.getTicketId()< b.getTicketId() ? -1 : 1; 
                            }
                            
                        });
                        for (Tickets ticket : tickets) {
                            
                            //On veut pouvoir gérer les retours de commandes
                            //comme un retour classique si jamais la commande est dans la liste
                            //on va devoir pouvoir trouver les paires de tickets commande retour ... par définition, si on trait dans l'ordre on va d'abord avoir la commande puis le retour.
                            //Dans ce cas si on a une map des chaines on peut savoir si la commande est dans la liste et la refaire
                            //Donc map des tickets + map des chaines liées
                            //Si commande avec retour alors on regarde si la commande d'origine est là, si oui on refait la chaine avec le param + on fait la chaine du retour avec le param
                            //On va passer un param de plus pour indiquer que l'on ne considère pas le param commande
                            
                            //EDU 2021 10 Réécriture de la gestion des commandes négatives suite bug sur les parent ticket id
                            //On va :
                            // - stocker les commande négatives dans une map à part
                            // - une fois tous les tickets parcourus, regarder pour chque commande négative si il existe un ticket correspondant 
                            // 1 Ticket correspondant : même contenu au signe prêt , même client
                            Boolean ignoreOrder = false;
                            for(TicketLines ligne  : ticket.getLines()) {
                                if(!ignoreOrder && ligne.isOrder() && ligne.getUnits() < 0) {
                                    ignoreOrder = true;
                                }
                            }
                            
                            if(ignoreOrder) {
                                //si commande négative on ajoute dans la liste
                                tmpOrders.add(ticket);
                            } else {
                                // si non si il y a un client on stocke
                                if(ticket.getCustomerId() != null ) {
                                    tmptickets.add(ticket.getCustomerId(), ticket);
                                }
                            }
                            
                            String tmp = tmpstr.get(ticket.getTicketId());
                            
                            if(tmp == null ) {
                                tmp = getTicketExportString(ticket , false);
                            }
                            
                            if (tmp == null) {
                                ok = false;
                                break;
                            }
                            
                            
                            tmpstr.put(ticket.getTicketId() , tmp);
                        }
                        for(Tickets commande : tmpOrders) {
                            Tickets parent = null;
                            //On parcoure les commande négatives
                            //pour chacune on va chercher si il y a un ticket disponible qui correspond
                            if(commande.getCustomerId() != null) {
                                List<Tickets> listeCandidats = tmptickets.get(commande.getCustomerId());
                                for(int index = 0 ; listeCandidats != null && parent == null && index < listeCandidats.size() ; index++) {
                                    Tickets candidat = listeCandidats.get(index);
                                    Boolean valide = commande.getLines().size() == candidat.getLines().size();
                                    for(TicketLines ligne  : commande.getLines()) {
                                        if(valide) {
                                            valide = false;
                                            for(TicketLines lignecandidate  : candidat.getLines()) {
                                                valide = valide || (( ligne.getType() == lignecandidate.getType()) &&
                                                        ( ligne.getUnits() +lignecandidate.getUnits() == 0D) &&
                                                        ((ligne.getProduct() ==null && lignecandidate.getProduct() == null) || ( ligne.getProduct() !=null && lignecandidate.getProduct() != null  && (ligne.getProduct().getId() == lignecandidate.getProduct().getId()))));
                                            }
                                        }
                                    }
                                    if(valide) {
                                        //Toutes les lignes de notre commande on trouvé preneuse dans notre ticket
                                        parent = candidat;
                                        listeCandidats.remove(index);
                                        tmptickets.remove(commande.getCustomerId(), candidat);
                                    }
                                }
                            }
                            //Si il y en a un 
                            //on l'enlève des tickets disponibles
                            //on le réécrit en ignorant le caractère commande
                            //on réécrit notre commande en ignorant le caractère commande
                            if(parent != null) {
                                tmpstr.put(parent.getTicketId(), getTicketExportString(parent , true));
                                tmpstr.put(commande.getTicketId(), getTicketExportString(commande , true));
                                
                            }else {
                                //Sinon
                                //On ne touche rien à ce qui est écrit
                                //On taggue la session comme problématique
                                closedCashErrors.add(closedCah.getId());                                
                            }
                        
                        }
                        if (ok) {
                            //une map closedCashId - chaines
                            //comme ça pus de pb de décalage
//                            str.addAll(tmpstr.values());
//                            str.add("****FIN_DU_FICHIER****");
                            ArrayList<String> value = new ArrayList<String>();
                            value.addAll(tmpstr.values());
                            value.add("****FIN_DU_FICHIER****");
                            closedCashStr.put(closedCah.getId(), value);
                            
                        }
                    }
                }
            }
        }

        if (dto.isExportAll() || dto.isExportCashMovements()) {
            if (dto.isJsonFormat()) {
                List<Payments> listPayments;

                listPayments = paymentsDAO.getPayments(limitedLocation, null, dateStartString, dateStopString, null, null, Payments.Categ.MOUVEMENT, null);
                dto.setCashMovements(AdaptableHelper.getAdapter(listPayments, PaymentsIndexDTO.class));
                //TODO - A REPRENDRE TOTALEMENT LE JOUR OU ON FAIT DES EXPORTS JSON
                //NE fonctionne plus (retourne null) depuis remonté PaymentsIndexDTO depuis pasteque-api vers ose-backend
                //donc plus disponible dans le get adapter
            }
        }

        try {
            URL url = new URL(file);
            if (!"file".equals(url.getProtocol())) { // .openConnection().getDoOutput()
                DataOutputStream dous = new DataOutputStream(new URL(file).openConnection().getOutputStream());
                if (dto.isJsonFormat()) {
                    ObjectMapper mapper = new ObjectMapper();
                    dous.writeUTF(mapper.writeValueAsString(dto));
                    dous.flush();
                    dous.close();
                } else {
                    // TODO séparer en plusieurs fichiers si besoin
                    // dous.writeUTF(StringUtils.join(str, separator));

                    if (dto.getClosedCash() != null && !dto.getClosedCash().isEmpty()) {
                        dous.flush();
                        dous.close();
                        File file_tmp = new File(url.getPath());
                        String parentPath = file_tmp.getParent();

                        SimpleDateFormat hhMM = new SimpleDateFormat("HHmm");
                        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
                        //int fromIndex = 0;
                        //int toIndex = 0;
                        // int tmpMostRecent = 0;
                        for (POSClosedCashDTO closedCash : dto.getClosedCash()) {
                            // Générer les fichiers pour chaque POSClosedCashDTO fermé
                            //et qui n'a pas eu de pb de génération
                            if (closedCash.getCloseDate() != null && closedCashStr.get(closedCash.getId()) != null) {
                                // TODO
                                Boolean exists = false;
                                List<String> sub_str = closedCashStr.get(closedCash.getId());
                                if (closedCash.getSalesLocation() != null) {
                                    
                                    String myfile = parentPath.replace('\\', '/')
                                            .concat(String.format("/Ventes_%02d_%s_%s_%s_%s.txt", closedCash.getSalesLocation().getTourId(),
                                                    yyyyMMdd.format(closedCash.getOpenDate()),
                                                    // URLEncoder.encode(closedCash.getSalesLocation().getCity(), "UTF-8") ,
                                                    // closedCash.getSalesLocation().getCity().replace(' ', '_') ,
                                                    closedCash.getSalesLocation().getCity().replaceAll(" ", "%20"),
                                                    // EDU 2016 06 20 On tente avec des espaces dans le nom des villes
                                                    // closedCash.getSalesLocation().getCity() ,
                                                    hhMM.format(closedCash.getOpenDate()), hhMM.format(closedCash.getCloseDate())));
                                    if(closedCashErrors.contains(closedCash.getId())) {
                                        myfile = myfile.substring(0, myfile.length() - 4).concat(".err");
                                    }
                                    myfile = url.getProtocol().concat("://").concat(url.getAuthority()).concat(myfile);
                                    URL myURL = new URL(myfile.substring(0, myfile.length() - 4).concat(".old"));
                                    try {
                                        InputStream input = myURL.openStream();
                                        input.close();
                                        exists = true;
                                    } catch (Exception ftpEx) {

                                    }
                                    myURL = new URL(myfile);
                                    try {
                                        InputStream input = myURL.openStream();
                                        input.close();
                                        exists = true;
                                    } catch (Exception ftpEx) {

                                    }
                                    if (!exists) {
                                        dous = new DataOutputStream(myURL.openConnection().getOutputStream());
                                        Writer out = new BufferedWriter(new OutputStreamWriter(dous, "UTF8"));
                                        out.append(String.join(separator , sub_str));

                                        out.flush();
                                        out.close();
                                    }
                                }
                                //fromIndex = toIndex;
                            }
                        }
                    } else {
                        Writer out = new BufferedWriter(new OutputStreamWriter(dous, "UTF8"));
                        out.append(String.join(separator , str));

                        out.flush();
                        out.close();
                    }
                }
            } else {
                if (dto.isJsonFormat()) {
                    OutputStream out = Files.newOutputStream(FileSystems.getDefault().provider().getPath(URI.create(file)));
                    new ObjectMapper().writeValue(out, dto);
                    out.flush();
                    out.close();
                } else {
                    // TODO séparer en plusieurs fichiers si besoin
                    // Path FileSystems.getDefault().provider().getPath(URI.save(file)).getParent()
                    // Générer les fichiers pour chaque POSClosedCashDTO
                    //
                    if (dto.getClosedCash() != null && !dto.getClosedCash().isEmpty()) {
                        Path parent = FileSystems.getDefault().provider().getPath(URI.create(file)).getParent();
                        SimpleDateFormat hhMM = new SimpleDateFormat("HHmm");
                        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");

                        for (POSClosedCashDTO closedCash : dto.getClosedCash()) {
                            // Générer les fichiers pour chaque POSClosedCashDTO
                            
                            if (closedCash.getSalesLocation() != null && closedCash.getCloseDate() != null) {

                            List<String> sub_str = closedCashStr.get(closedCash.getId());
                            
                                String myfile = "file:///".concat(parent.toString().replace('\\', '/'))
                                        .concat(String.format("/Ventes_%02d_%s_%s_%s_%s.txt", closedCash.getSalesLocation().getTourId(),
                                                yyyyMMdd.format(closedCash.getOpenDate()),
                                                // URLEncoder.encode(closedCash.getSalesLocation().getCity(), "UTF-8") ,
                                                // closedCash.getSalesLocation().getCity().replace(' ', '_') ,
                                                closedCash.getSalesLocation().getCity().replaceAll(" ", "%20"),
                                                // EDU 2016 06 20 On tente avec des espaces dans le nom des villes
                                                // closedCash.getSalesLocation().getCity() ,
                                                hhMM.format(closedCash.getOpenDate()), hhMM.format(closedCash.getCloseDate())));
                                if(closedCashErrors.contains(closedCash.getId())) {
                                    myfile = myfile.substring(0, myfile.length() - 4).concat(".err");
                                }
                                Files.write(FileSystems.getDefault().provider().getPath(URI.create(myfile)), sub_str, StandardCharsets.UTF_8);
                            }
                        }
                    } else {
                        Files.write(FileSystems.getDefault().provider().getPath(URI.create(file)), str, StandardCharsets.UTF_8);
                    }
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            // TODO gérer autrement les URL qui ne permettent pas une écriture directe
            e.printStackTrace();
        }

        try (InputStream ins = new URL(file).openStream()) {

            Scanner scanner = new Scanner(new InputStreamReader(ins));
            filecontent = scanner.useDelimiter("\\Z").next();
            scanner.close();

        } catch (Exception ex) {
            filecontent = ex.getMessage();
        }
        return filecontent;
    }

    /**
     * 
     * @param vendorId
     * @param closedCah
     *            La session de vente concernée
     * @return une chaine decrivant le résumé de la session Exemple
     *         C;33071;A;2015;12;1;24;08301230;11:48;12:06;12;453.71;9;427.77;9;511.79;0;0.00;0;0.00;3572;36;1393.27;666;30;0.00;430.00
     */
    private String getClosedCashExportString(POSClosedCashDTO closedCash, String vendorId) {
        String response = "";
        SimpleDateFormat hhMM = new SimpleDateFormat("HH:mm");
        SimpleDateFormat hhMM_string = new SimpleDateFormat("HHmm");
        ArrayList<String> tmpelements = new ArrayList<String>();
        Calendar openDate = Calendar.getInstance();
        Calendar closeDate = Calendar.getInstance();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.FRANCE);
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat money = new DecimalFormat("0.00", otherSymbols);
        List<Object[]> result;
        List<Object[]> listTaxes;
        double sales = 0D;
        double taxesAmount = 0D;
        long discountCount = 0;
        double discount = 0D;
        // int custCount = 0;
        long nbTickets = 0;
        List<Object[]> listPayments;
        // long paymentsCount = 0;
        POSPaymentsDTO paymentsCB = new POSPaymentsDTO();
        POSPaymentsDTO paymentsChk = new POSPaymentsDTO();
        POSPaymentsDTO paymentsDiv = new POSPaymentsDTO();
        POSPaymentsDTO paymentsCash = new POSPaymentsDTO();

        openDate.setTime(closedCash.getOpenDate());
        closeDate.setTime(closedCash.getCloseDate());

        if (closedCash.getSalesLocation() != null) {
            result = ticketsDAO.findTicketsByClosedCash(closedCash.getId());
            listPayments = paymentsApiDAO.getPaymentsByClosedCash(closedCash.getId(), null);
            listTaxes = taxesDAO.getTaxesByClosedCash(closedCash.getId());
            // result ne doit avoir qu'une ligne....
            for (Object[] objects : result) {
                if (objects[0] != null) {
                    nbTickets = (int) objects[0];
                }
                if (objects[1] != null) {
                    sales += (double) objects[1];
                }
                // if (objects[2] != null) {
                // custCount += (int) objects[2];
                // }
                if (objects[3] != null) {
                    discountCount += (double) objects[3] != 0D ? 1 : 0;
                    discount += (double) objects[3];
                }
            }

            paymentsCB.setPaymentsNumber(new Long(0));
            paymentsCash.setPaymentsNumber(new Long(0));
            paymentsChk.setPaymentsNumber(new Long(0));
            paymentsDiv.setPaymentsNumber(new Long(0));
            for (Object[] objects : listPayments) {

                POSPaymentsDTO POSPaymentsDTO = new POSPaymentsDTO();
                POSPaymentsDTO.setPaymentsNumber((long) objects[3]);
                POSPaymentsDTO.setAmount((double) objects[2]);
                POSPaymentsDTO.setCurrencyAmount((double) objects[4]);
                POSPaymentsDTO.setCurrencyId((Long) objects[1]);
                POSPaymentsDTO.setType((String) objects[0]);
                POSPaymentsDTO.setPaymentLabel(Payments.getPaymentsLabel((String) objects[0]));
                POSPaymentsDTO.setPaymentCateg(Payments.getPaymentsCateg((String) objects[0]));

                // paymentsCount += POSPaymentsDTO.getPaymentsNumber();

                switch (POSPaymentsDTO.getType()) {
                case Payments.Type.PAPER_IN:
                case Payments.Type.PAPER_OUT:
                case Payments.Type.TRANSFER:
                case Payments.Type.DROIT_DE_PLACE:    
                    paymentsDiv.setAmount(paymentsDiv.getAmount() + POSPaymentsDTO.getAmount());
                    paymentsDiv.setPaymentsNumber(paymentsDiv.getPaymentsNumber() + POSPaymentsDTO.getPaymentsNumber());
                    break;
                case Payments.Type.MAGCARD:
                    paymentsCB.setAmount(paymentsCB.getAmount() + POSPaymentsDTO.getAmount());
                    paymentsCB.setPaymentsNumber(paymentsCB.getPaymentsNumber() + POSPaymentsDTO.getPaymentsNumber());
                    break;
                case Payments.Type.CHEQUE:
                    paymentsChk.setAmount(paymentsChk.getAmount() + POSPaymentsDTO.getAmount());
                    paymentsChk.setPaymentsNumber(paymentsChk.getPaymentsNumber() + POSPaymentsDTO.getPaymentsNumber());
                    break;
                case Payments.Type.CASH:
                    paymentsCash.setAmount(paymentsCash.getAmount() + POSPaymentsDTO.getAmount());
                    paymentsCash.setPaymentsNumber(paymentsCash.getPaymentsNumber() + POSPaymentsDTO.getPaymentsNumber());
                    break;
                default:
                }
            }

            for (Object[] objects : listTaxes) {
                taxesAmount += ((double) objects[2]);
            }

            tmpelements.add("C");
            tmpelements.add(closedCash.getSalesLocation().getInseeNb());
            // Type de vente toujours A ?
            tmpelements.add("A");
            tmpelements.add(Integer.toString(openDate.get(Calendar.YEAR)));
            tmpelements.add(String.format("%02d", openDate.get(Calendar.MONTH) + 1));
            tmpelements.add(String.format("%d", closedCash.getSalesLocation().getTourId()));
            tmpelements.add(String.format("%02d", openDate.get(Calendar.DAY_OF_MONTH)));
            tmpelements.add(String.format("%s%s", hhMM_string.format(closedCash.getSalesLocation().getStartDate()),
                    hhMM_string.format(closedCash.getSalesLocation().getEndDate())));
            tmpelements.add(hhMM.format(openDate.getTime()));
            tmpelements.add(hhMM.format(closeDate.getTime()));
            // NbChq Int,
            tmpelements.add(Long.toString(paymentsChk.getPaymentsNumber()));
            // MntChq Money,
            tmpelements.add(money.format(paymentsChk.getAmount()));
            // NbEsp Int,
            tmpelements.add(Long.toString(paymentsCash.getPaymentsNumber()));
            // MntEsp Money,
            tmpelements.add(money.format(paymentsCash.getAmount()));
            // NbCB Int,
            tmpelements.add(Long.toString(paymentsCB.getPaymentsNumber()));
            // MntCB Money,
            tmpelements.add(money.format(paymentsCB.getAmount()));
            // NbDivers Int,
            tmpelements.add(Long.toString(paymentsDiv.getPaymentsNumber()));
            // MntDivers Money,
            tmpelements.add(money.format(paymentsDiv.getAmount()));
            // NbPromo Int,
            tmpelements.add(Long.toString(discountCount));
            // MntPromo Money,
            tmpelements.add(money.format(discount));

            tmpelements.add(Integer.toString(closedCash.getSalesLocation().getPubNb()));
            // NumLivreur Int,
            tmpelements.add(vendorId.replaceAll("V", ""));
            // CAFTotal Money,
            tmpelements.add(money.format(sales - discount + taxesAmount));
            // KmCamion Int,
            tmpelements.add("0");
            // NbTickets Int,
            tmpelements.add(Long.toString(nbTickets));

            // FondCaisse Money,
            tmpelements.add(closedCash.getOpenCash());
            // RetraitCaisse Money,
            tmpelements.add(closedCash.getCloseCash());

            response = String.join(";" , tmpelements);
        }
        return response;

    }

    @Override
    public String importWorkbook(Workbook wb, String locationId) {

        String retour = "Import d'un fichier au format tableur";
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
        // ExportConstraintsDTO dto = new ExportConstraintsDTO();
        SortedMap<String, Integer> triFeuille = new TreeMap<String, Integer>();
        for (int rang = 0; rang < wb.getNumberOfSheets(); rang++) {
            String Nom = wb.getSheetName(rang);
            triFeuille.put(Nom, rang);
        }

        for (Map.Entry<String, Integer> feuille : triFeuille.entrySet()) {
            Sheet sheet = wb.getSheetAt(feuille.getValue());
            switch (sheet.getSheetName()) {
            case Sheets.CUST:
                retour = customerEdiService.importCustomerSheet(sheet, retour, locationId);
                break;
            case Sheets.DOUBLON:
                retour = customerEdiService.importCustomerDeduplicationSheet(sheet, retour, locationId);
                break;
            case Sheets.TICKET_CLIENT:
                retour = customerEdiService.importCustomerTicket(sheet, retour);
                break;
            case Sheets.TARRIFAREAS:
                retour = importTarrifSheet(sheet, retour, evaluator);
                break;
            case Sheets.TARRIFAREAS_LOC:
                retour = importTarrif_LocSheet(sheet, retour, evaluator);
                break;
            case Sheets.TARRIFAREAS_PROD:
                retour = importTarrif_PricesSheet(sheet, retour, evaluator);
                break;
            case Sheets.LOCATIONS:
                retour = importLocationsSheet(sheet, retour, evaluator);
                break;
            case Sheets.LOCATIONS_TOURS:
                retour = importLocationsToursSheet(sheet, retour, evaluator);
                break;
            case Sheets.ORDERS:
                retour = ordersEdiService.importOrdersSheet(sheet, retour, evaluator);
                break;
            case Sheets.STOCK_TYPE:
                retour = importStockTypeSheet(sheet, retour, evaluator);
                break;
            default:
                retour += " Feuille nommée : " + sheet.getSheetName() + " non prise en compte ! ";
            }

        }

        try {
            wb.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return retour;
    }

    private String importStockTypeSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données stock type : ";
        HashMap<String, StockLevel> stockLevels = new HashMap<String, StockLevel>();
        Map<Integer, String> indexes = new HashMap<>();
        Map<String, StockLevel> mapAllStockLevels = new HashMap<>();
        // on recupere la ligne ou se trouve les pdv
        Row rowPdvs = sheet.getRow(StockLevelInfo.NUM_LIGNE_PDV);
        double defaultSecuritylevel = applicationDAO.getDefaultSecurityLevel();
        // Creation des indexes
        indexes = createIndex(rowPdvs, evaluator);

        String productRef = null;
        StockLevel stockLevel = null;
        Products product = null;
        int nbligne = 0;
        //On récupère tous les stocks levels de notre fichier et on les met dans une Map
        for (Row row : sheet) {
            if (row.getRowNum() >= StockLevelInfo.NUM_LIGNE_DATA) {
                nbligne++;
                productRef = getValueOfCell(row.getCell(0), evaluator);
                if (productRef != null) {
                    product = productsDAO.findById(productRef).orElse(null);
                    if (product != null) {
                        for (int i = 0; i < row.getLastCellNum(); i++) {

                            if (indexes.containsKey(i)) {
                                String locationId = indexes.get(i);
                                String index = locationId + "-" + productRef;
                                String stockValue = getValueOfCell(row.getCell(i), evaluator) == null ? "0" : getValueOfCell(row.getCell(i), evaluator);
                                if (stockLevels.containsKey(index)) {
                                    //La seconde colonne pour un point de vente sert à donner le stock de sécurité
                                    stockLevels.get(index).setStockSecurity(Double.valueOf(stockValue));
                                } else {
                                    stockLevel = new StockLevel();
                                    stockLevel.setProducts(product);
                                    stockLevel.setLocations(locationDAO.findById(locationId).orElse(null));
                                    stockLevel.setId();

                                    stockLevel.setStockMaximum(Double.valueOf(stockValue));
                                    stockLevel.setStockSecurity(stockLevel.getStockMaximum()*defaultSecuritylevel);
                                    stockLevel.setLastUpdate(new Date());
                                    stockLevels.put(index, stockLevel);

                                }
                            }
                        }
                    }
                }
            }

        }
        // on recupere tout les stocklevels des pdvs qui se trouvent dans le fichier excel
        List<StockLevel> stockLevelLocations = stockLevelDAO.findListByCriteria(null, new ArrayList<String>(indexes.values()), null, null);
        for (StockLevel stockLevelLocation : stockLevelLocations) {
            mapAllStockLevels.put(stockLevelLocation.getId(), stockLevelLocation);
        }
        List<StockLevel> ListToCreate = new ArrayList<StockLevel>();
        List<StockLevel> ListToUpdate = new ArrayList<StockLevel>();
        // creation 2 listes To create To update
        for (Map.Entry<String, StockLevel> entry : stockLevels.entrySet()) {
            String cle = entry.getKey();
            StockLevel valeur = entry.getValue();
            //on enlève du map global.
            StockLevel mapValue = mapAllStockLevels.remove(cle);
            if (mapValue == null) {
                //Une valeur null implique que la clef était absente
                ListToCreate.add(valeur);
            } else {
                //Si il y a un changement on ajoute aux modifs à faire
                if(mapValue.getStockMaximum() != valeur.getStockMaximum() 
                        || mapValue.getStockSecurity() != valeur.getStockSecurity()) {
                    ListToUpdate.add(valeur);
                }
            }
            
        }
        //Si on a le flag INITIALISE_STYPE_TYPE 
        //alors il faut remettre à zero tous les élements absents de notre fichier
        //ie ceux qui restent dans notre map
        if (StockLevelInfo.INITIALISE_STYPE_TYPE) {
            for (StockLevel stckLevel : mapAllStockLevels.values()) {
                if(stckLevel.getStockMaximum() != 0D && stckLevel.getStockSecurity() != 0D ) {
                    stckLevel.setStockMaximum(0);
                    stckLevel.setStockSecurity(0);
                    stckLevel.setLastUpdate(new Date());
                    ListToUpdate.add(stckLevel);
                }
            }
        }
        //TODO EDU génération des jobs qui vont bien 
        if (!ListToCreate.isEmpty())
            stocksService.createStockLevels(ListToCreate);

        if (!ListToUpdate.isEmpty())
            stocksService.updateStockLevels(ListToUpdate);

        retour += String.valueOf(nbligne) + " lignes de stock type importées - ";
        return retour;
    }

    private Map<Integer, String> createIndex(Row row, FormulaEvaluator evaluator) {
        List<String> listPdvsiIds = new ArrayList<String>();
        List<Locations> pdvs = locationsService.findAllParentLocations(true, null, false, false);
        for (Locations pdv : pdvs) {
            listPdvsiIds.add(pdv.getId());
        }
        Map<Integer, String> indexes = new HashMap<>();
        String previousValue = null;

        for (int i = 0; i < row.getLastCellNum(); i++) {
            String value = getValueOfCell(row.getCell(i), evaluator);
            if (value != null && value.equals(StockLevelInfo.CHAR_NOT_PDV)) {
                continue;
            } else {
                if (value != null && !value.isEmpty() && listPdvsiIds.contains(value)) {
                    indexes.put(i, value);
                } else if (!listPdvsiIds.contains(value) && previousValue != null) {
                    indexes.put(i, previousValue);
                }
            }
            previousValue = value;
        }
        return indexes;
    }

    private String getValueOfCell(Cell cell, FormulaEvaluator evaluator) {
        CellValue primCellValue = null;
        String cellValue = null;
        primCellValue = evaluator.evaluate(cell);
        switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
        case STRING:
            cellValue = primCellValue.getStringValue();
            break;
        case NUMERIC:
            if (DateUtil.isCellDateFormatted(cell)) {
                cellValue = cell.getDateCellValue().toString();
            } else {
                cellValue = String.valueOf((long) primCellValue.getNumberValue());
            }
            break;
        case BOOLEAN:
            cellValue = String.valueOf(primCellValue.getBooleanValue());
            break;
        case FORMULA:
            cellValue = String.valueOf(cell.getCellFormula());
            break;
        default:
            cellValue = null;
        }
        return cellValue;
    }

    private String importLocationsToursSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données tournées - points de vente : ";
        int nbLoc = 0, validityLevel;
        List<POSToursLocationsDTO> list = new ArrayList<POSToursLocationsDTO>();
        String cellValue = null;
        CellValue primCellValue = null;
        LocationsToursColumns disposition = new LocationsToursColumns();
        disposition.columns = new HashMap<Integer, String>();

        for (Row row : sheet) {
            validityLevel = 1111;
            POSToursLocationsDTO current = new POSToursLocationsDTO();

            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);
                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf((long) primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {
                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case LocationsToursColumns.LOC_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1000;
                                LocationsInfoDTO location = new LocationsInfoDTO();
                                location.setId(cellValue);
                                current.setLocation(location);
                                ;
                            }
                            break;
                        case LocationsToursColumns.TOUR_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 100;
                                TourIndexDTO tour = new TourIndexDTO();
                                tour.setId(Long.parseLong(cellValue));
                                current.setTour(tour);
                            }
                            break;
                        case LocationsToursColumns.START:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                validityLevel -= 10;
                                current.setStartDate(cell.getDateCellValue());
                            }
                            break;
                        case LocationsToursColumns.END:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                validityLevel -= 1;
                                current.setEndDate(cell.getDateCellValue());
                                ;
                            }
                            break;

                        }

                    }
                }
            }

            if (row.getRowNum() > 0 && validityLevel == 0) {
                nbLoc++;
                list.add(current);
            }
        }
        saveLocationsTours(list);
        retour += String.valueOf(nbLoc) + " infos tournées - point de vente importées - ";
        return retour;
    }

    private String importLocationsSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données points de vente : ";
        int nbLoc = 0, validityLevel;
        List<String> etatsOk = locationsService.getEtats();
        List<POSLocationsDTO> list = new ArrayList<POSLocationsDTO>();
        String cellValue = null;
        CellValue primCellValue = null;
        LocationsColumns disposition = new LocationsColumns();
        disposition.columns = new HashMap<Integer, String>();

        for (Row row : sheet) {
            validityLevel = 11;
            POSLocationsDTO current = new POSLocationsDTO();

            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);
                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf((long) primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {
                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case LocationsColumns.LOC_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 10;
                                current.setId(cellValue);
                            }
                            break;
                        case LocationsColumns.LOC_NOM:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                current.setLabel(cellValue);
                                validityLevel--;
                            }
                            break;
                        case LocationsColumns.LOC_INSEENNUM:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                current.setInseeNum(cellValue);
                            }
                            break;
                        case LocationsColumns.LOC_ADDRESS:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                current.setAddress(cellValue);
                            }
                            break;
                        case LocationsColumns.LOC_ETAT:
                            if (cellValue != null && !cellValue.isEmpty() && etatsOk.contains(cellValue)) {
                                current.setEtat(cellValue);
                            }
                            break;
                        case LocationsColumns.LOC_PARENT_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                current.setParentId(cellValue);
                            }
                            break;
                        case LocationsColumns.LOC_TAXCUST_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                // current.set(cellValue);
                                // TODO gérer proprement ce cas !
                            }
                            break;
                        case LocationsColumns.LOC_CATEG_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                POSCategoriesDTO categ = new POSCategoriesDTO();
                                categ.setId(cellValue);
                                current.setCategory(categ);
                                // current.set(cellValue);
                                // TODO gérer proprement ce cas !
                            }
                            break;
                        }
                    }
                }
            }
            if (row.getRowNum() > 0 && validityLevel == 0) {
                nbLoc++;
                if(current.getEtat()==null) {
                    current.setEtat(Locations.Etats.ACTIF);
                }
                list.add(current);
            }
        }
        saveLocations(list);
        retour += String.valueOf(nbLoc) + " lieux de stockage importés - ";
        return retour;
    }

    private String importTarrifSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données catalogues : ";
        int nbTarrif = 0, validityLevel;
        List<POSTariffAreasDTO> list = new ArrayList<POSTariffAreasDTO>();
        String cellValue = null;
        CellValue primCellValue = null;
        TarrifColumns disposition = new TarrifColumns();
        disposition.columns = new HashMap<Integer, String>();

        for (Row row : sheet) {
            validityLevel = 11;
            POSTariffAreasDTO current = new POSTariffAreasDTO();
            current.setDispOrder(Integer.parseInt(DefaultValues.TariffAreas.ORDER_CATALOGUE_PAPIER));
            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);
                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf((long) primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {
                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case TarrifColumns.CAT_ID:
                            current.setId(Long.parseLong(cellValue));
                            validityLevel -= 10;
                            break;
                        case TarrifColumns.CAT_NOM:
                            current.setLabel(cellValue);
                            validityLevel--;
                            break;
                        case TarrifColumns.CAT_RANK:
                            current.setDispOrder(Integer.parseInt(cellValue));
                            break;
                        }
                    }
                }
            }
            if (row.getRowNum() > 0 && validityLevel == 0) {
                nbTarrif++;
                list.add(current);
            }
        }
        saveTarrif(list);
        retour += String.valueOf(nbTarrif) + " catalogues importés - ";
        return retour;
    }

    private String importCatalogcsv(EdiContentDTO content) {
        String retour = "Import données catalogues : ";
        int nbTarrif = 0;
        List<POSTariffAreasDTO> list = new ArrayList<POSTariffAreasDTO>();
        String[] elements;

        for (String ligne : content.catalog) {
            elements = ligne.split(";");

            if (elements.length == 3) {
                POSTariffAreasDTO current = new POSTariffAreasDTO();
                if (elements[2].charAt(0) == 'O') {// Car tout les catalogue commence par OSE_xxxx
                    current.setDispOrder(Integer.parseInt(DefaultValues.TariffAreas.ORDER_CATALOGUE_PAPIER));
                } else {
                    if (elements[2].charAt(7) == 'S') { // et les sous catalogues (solde) SHOPIX_SOLDE_xxxxx ? CE QUI EXPLIQUE elements[2].charAt(7)
                        current.setDispOrder(Integer.parseInt(DefaultValues.TariffAreas.ORDER_CATALOGUE_SOLDES));
                    } else {
                        current.setDispOrder(Integer.parseInt(DefaultValues.TariffAreas.ORDER_CATALOGUE_PROMOS));
                    }
                }
                current.setId(Long.parseLong(elements[1]));
                current.setLabel(elements[2]);
                list.add(current);
                nbTarrif++;
            }
        }

        saveTarrif(list);
        retour += String.valueOf(nbTarrif) + " catalogues importés - ";
        return retour;
    }

    private String importTarrif_LocSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données validité catalogues : ";
        int nbTarrif = 0, validityLevel;
        List<POSTariffAreasValidityDTO> list = new ArrayList<POSTariffAreasValidityDTO>();
        String cellValue = null;
        CellValue primCellValue = null;
        TarrifColumns disposition = new TarrifColumns();
        disposition.columns = new HashMap<Integer, String>();

        for (Row row : sheet) {
            validityLevel = 111;
            POSTariffAreasValidityDTO current = new POSTariffAreasValidityDTO();
            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);

                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf((long) primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {
                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case TarrifColumns.CAT_ID:
                            validityLevel -= 100;
                            current.setAreaId(Long.parseLong(cellValue));
                            break;
                        case TarrifColumns.LOC_ID:
                            validityLevel -= 10;
                            current.setLocationId(cellValue);
                            break;
                        case TarrifColumns.DATE_FROM:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                validityLevel--;
                                current.setStartDate(cell.getDateCellValue().getTime());
                            }
                            break;
                        case TarrifColumns.DATE_TO:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                current.setEndDate(cell.getDateCellValue().getTime());
                            }
                            break;
                        }
                    }
                }
            }
            if (row.getRowNum() > 0 && validityLevel == 0) {
                nbTarrif++;
                list.add(current);
            }
        }
        saveTarrifValidity(list);
        retour += String.valueOf(nbTarrif) + " validités catalogues importés - ";
        return retour;
    }

    private String importTarrif_PricesSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données prix catalogues : ";
        int nbTarrif = 0, validityLevel;
        double tax, price;
        List<POSTariffAreaPriceDTO> list = new ArrayList<POSTariffAreaPriceDTO>();
        String cellValue = null;
        CellValue primCellValue = null;
        TarrifColumns disposition = new TarrifColumns();
        disposition.columns = new HashMap<Integer, String>();

        for (Row row : sheet) {
            validityLevel = 111;
            tax = 0;
            price = 0;
            POSTariffAreaPriceDTO current = new POSTariffAreaPriceDTO();
            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);
                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf(primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (primCellValue.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {
                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case TarrifColumns.CAT_ID:
                            validityLevel -= 100;
                            current.setArreaId((long) Double.parseDouble(cellValue));
                            break;
                        case TarrifColumns.PROD_ID:
                            validityLevel -= 10;
                            current.setProductId(cellValue);
                            break;
                        case TarrifColumns.PRICE_WITH_TAX:
                            price = Double.parseDouble(cellValue);
                            break;
                        case TarrifColumns.TAX_PERCENTAGE:
                            tax = Double.parseDouble(cellValue);
                            break;
                        }
                    }
                }
            }
            if (price > 0) {
                validityLevel--;
                current.setPrice(price / (1 + tax / 100));
            }
            if (row.getRowNum() > 0 && validityLevel == 0) {
                nbTarrif++;
                list.add(current);
            }
        }
        saveTarifPrice(list);
        retour += String.valueOf(nbTarrif) + " prix catalogues importés - ";
        return retour;
    }

   

    @Override
    public String importRessource(URL url, String locationId) throws IOException {
        // TODO Auto-generated method stub
        InputStream is = null;
        String response = "";
        String[] tab = null;
        byte[] targetArray = null;
        try {
            is = url.openStream();
            // TODO pas chouette - il n'est pas recommandé de s'appuyer sur available
            targetArray = new byte[is.available()];
            is.read(targetArray);

            is.close();
            response = FilenameUtils.getBaseName(url.getPath());
            tab = response.split("_");
            if (tab.length == 2) {
                response = "Fichier : ".concat(response);
                Resources resource = resourcesDAO.findByName(tab[0]);

                if (resource != null) {

                    resource.setRestype(Integer.parseInt(tab[1]));
                    resource.setContent(targetArray);
                    resourcesDAO.save(resource);

                } else {
                    resource = new Resources();
                    resource.setName(tab[0]);
                    resource.setId(tab[0]);
                    resource.setContent(targetArray);
                    resource.setRestype(Integer.parseInt(tab[1]));
                    resourcesDAO.save(resource);
                }
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (is != null) {
                is.close();
            }

            response = e.getMessage();
        }
        return response;
    }

    @Override
    public String importRole(URL url, String locationId) throws IOException {
        InputStream is = null;
        String response = "";
        byte[] targetArray = null;
        try {
            is = url.openStream();
            // TODO pas chouette - il n'est pas recommandé de s'appuyer sur available
            targetArray = new byte[is.available()];
            is.read(targetArray);

            is.close();
            response = FilenameUtils.getBaseName(url.getPath());

            Roles role = rolesDAO.findByName(response);

            if (role != null) {
                role.setPermissions(targetArray);
                rolesDAO.save(role);

            } else {
                role = new Roles();
                role.setName(response);
                role.setId(response);
                role.setPermissions(targetArray);
                rolesDAO.save(role);
            }

            response = "Fichier : ".concat(response);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            if (is != null) {
                is.close();
            }

            response = e.getMessage();
        }
        return response;
    }

    private String importUserscsv(EdiContentDTO content) {
        String retour = "Import données users : ";
        int nbUsers = 0;
        List<POSUsersDTO> list = new ArrayList<POSUsersDTO>();
        String[] elements;

        for (String ligne : content.user) {
            elements = ligne.split(";");

            if (elements.length == 7) {
                POSUsersDTO current = new POSUsersDTO();

                current.setId(elements[1]);
                current.setName(elements[2]);
                current.setNom(elements[3]);
                current.setPrenom(elements[4]);
                current.setRoleId(elements[5]);
                current.setVisible("1".equals(elements[6]));
                current.setPassword("");
                list.add(current);
            }
        }

        nbUsers = saveUsers(list);
        retour += String.valueOf(nbUsers) + " users importés - ";
        return retour;
    }

    private String importDeleteTarrifPricecsv(EdiContentDTO content) {
        String retour = "";
        String[] elements;
        List<POSTariffAreaPriceDTO> list = new ArrayList<POSTariffAreaPriceDTO>();
        POSTariffAreaPriceDTO price;
        int ok = 0;
        for (String ligne : content.infos_del_cat_price) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                price = new POSTariffAreaPriceDTO();
                price.setArreaId(Long.parseLong(elements[1]));
                price.setProductId(elements[2]);

                list.add(price);

            }
        }
           ok = delTarifPrice(list);
        retour = String.format("%d Prix catalogues supprimées ", ok);

        return retour;
    }

    private String importDeleteReferencesCsv(EdiContentDTO content) {

        String retour = "";
        String[] elements;
        List<Products> listProducts;
        Products produit;
        ProductReferences ref;
        List<ProductReferences> refList;

        int ok = 0;

        for (String ligne : content.del_references) {
            elements = ligne.split(";");
            if (elements.length == 3) {
                listProducts = productsDAO.findByReference(elements[1]);
                if (!listProducts.isEmpty()) {
                    produit = listProducts.get(0);
                    refList = productReferencesDAO.findByProductAndReference(produit, elements[2]);
                    if (!refList.isEmpty()) {
                        ref = refList.get(0);

                        productReferencesDAO.delete(ref);
                        produit.setLastUpdate(new Date());
                        productsDAO.save(produit);
                        ok++;
                    }
                }
            }
        }
        retour = String.format("%d références de produits supprimées ", ok);
        return retour;
    }

}
