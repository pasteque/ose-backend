package com.ose.backend.service.edi;

import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;

public interface IOrdersEdiService {

    String importOrdersSheet(Sheet sheet, String retour, FormulaEvaluator evaluator);
}
