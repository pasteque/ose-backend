package com.ose.backend.service.edi.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.security.POSUsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.service.edi.IOrdersEdiService;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.orders.IOrdersService;
import com.ose.backend.service.orders.impl.OrdersService;
import com.ose.backend.service.stock.IStocksServiceOse;

@Service
public class OrdersEdiService implements IOrdersEdiService {

    @Autowired
    IStocksServiceOse stockService;
    @Autowired
    private IJobsService jobService;
    @Autowired
    private IOrdersService ordersService;

    // Nom des headers connus pour les commandes
    public class OrderColumns {
        public static final String REF = "Ref_Commande";
        public static final String DATE = "Date";
        public static final String STATUS = "Statut";
        public static final String NOTE = "Message";
        public static final String ORIG_ID = "ID_Origine";
        public static final String DEST_ID = "ID_Destination";
        public static final String PRODUCT_ID = "Ref_Produit";
        public static final String ORDERED_QTY = "Qté_Commandée";
        public static final String VALID_QTY = "Qté_Validée";
        public static final String PREP_QTY = "Qté_Préparée";
        public static final String DELIV_QTY = "Qté_Expédiée";
        public static final String RECEPT_QTY = "Qté_Reçue";
        public static final String PALET_NUM = "Num_Palette";
        public static final String USER_ID = "Opérateur";
        public static final String INT_ORDER_ID = "ID_Commande";
        public static final String INT_ODETAIL_ID = "ID_Ligne";
        public static final String INT_PAL_ID = "ID_Palette";
        public static final String INT_PDETAIL_ID = "ID_LignePalette";
        public static final String ORDERED_USER = "Utilisateur_Commande";
        public static final String VALID_USER = "Utilisateur_Validation";
        public static final String PREP_USER = "Utilisateur_Préparation";
        public static final String DELIV_USER = "Utilisateur_Expédition";
        public static final String RECEPT_USER = "Utilisateur_Reception";
        public static final String ORDERED_DATE = "Date_Commande";
        public static final String VALID_DATE = "Date_Validation";
        public static final String PREP_DATE = "Date_Préparation";
        public static final String DELIV_DATE = "Date_Expédition";
        public static final String RECEPT_DATE = "Date_Reception";

        public Map<Integer, String> columns;
    }
    /**
     * Il s'agit d'importer la feuille excel trouvée si elle contient la description d'e ligne de commandes d'appro
     * Ces lignes peuent être plus ou moins complètes et aller jusqu'à contenir le détail de palettes 
     * 
     * Nouvelle approche pour la sauvegarde : d'abord les mouvements de stock
     *  On check au passage que le mouvement n'est pas déjà dans le système
     *  Méchanisme pour retrouver le mouvement : couple produit - palette ( <- stockée dans la ref de mouvemet au lieu de la commande )
     * Ensuite les infos de la commandes
     */
    @Override
    public String importOrdersSheet(Sheet sheet, String retour, FormulaEvaluator evaluator) {
        retour += " - Import données commandes : ";
        int nbOrders = 0, validityLevel, validityQty;
        String cellValue = null, orderId = null, status = null, note = null;
        String palette = null, orig = null, dest = null;
        String internalOrderId = null, internalODetailId = null, internalPalId = null, internalPDetailId = null;
        Date movementDate = null, orderDate = null, validDate = null, prepDate = null, delivDate = null, receptDate = null;
        Double ordered_qty = null, valid_qty = null, prep_qty = null, deliv_qty = null, recept_qty = null;
        Integer statusLevel = null;
        CellValue primCellValue = null;

        List<PaletteDetailDTO> paletteDetails = new ArrayList<PaletteDetailDTO>();

        OrderDTO currentOrder = null;
        PaletteDTO currentPalette = null;
        int currentOrderStatus = 0;

        PaletteDetailDTO current = null;

        String paletteName = null;

        LocationsInfoDTO orig_location = new LocationsInfoDTO();
        LocationsInfoDTO dest_location = new LocationsInfoDTO();
        POSUsersDTO people = new POSUsersDTO();
        POSUsersDTO oUser = new POSUsersDTO();
        POSUsersDTO vUser = new POSUsersDTO();
        POSUsersDTO pUser = new POSUsersDTO();
        POSUsersDTO rUser = new POSUsersDTO();
        POSUsersDTO dUser = new POSUsersDTO();

        ProductsResultDTO productResultDTO = null;

        OrderColumns disposition = new OrderColumns();
        disposition.columns = new HashMap<Integer, String>();

        Map<String, OrderDTO> ordersMap = new HashMap<String, OrderDTO>();
        Map<String, PaletteDTO> palettesMap = new HashMap<String, PaletteDTO>();
        Map<String, OrderDetailDTO> currentOrderODetailsMap = null;
        Map<String , Map<String, OrderDetailDTO>> ordersOrdersDetailsMap = new HashMap<String , Map<String, OrderDetailDTO>>();
        //Map<String , List<PaletteDetailDTO>> ordersPaletteDetailsMap = new HashMap<String , List<PaletteDetailDTO>>();
        Map<String , List<PaletteDetailDTO>> palettePaletteDetailsMap = new HashMap<String , List<PaletteDetailDTO>>();
        Map<String, String> ordersId = new HashMap<String, String>();
        Map<String, Boolean> ordersAlreadyReceived = new HashMap<String, Boolean>();

        long lStartTime = System.nanoTime();

        for (Row row : sheet) {
            validityLevel = (1 << 7) - 1;
            validityQty = 0;
            orderId = null;
            status = null;
            movementDate = null;
            note = null;
            ordered_qty = null;
            valid_qty = null;
            prep_qty = null;
            deliv_qty = null;
            recept_qty = null;
            orig = null;
            dest = null;
            internalOrderId = null;
            internalODetailId = null;
            internalPalId = null;
            internalPDetailId = null;
            orderDate = null;
            validDate = null;
            prepDate = null;
            delivDate = null;
            receptDate = null;
            // TODO : changer cette vilaine assignation en dur!
            // Si pas d'opérateur, on considère que c'est l'admin
            people.setId("0");
            oUser.setId(null);
            vUser.setId(null);
            pUser.setId(null);
            rUser.setId(null);
            dUser.setId(null);

            for (Cell cell : row) {
                primCellValue = evaluator.evaluate(cell);
                switch (primCellValue == null ? CellType._NONE : primCellValue.getCellType()) {
                case STRING:
                    cellValue = primCellValue.getStringValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                    } else {
                        cellValue = String.valueOf((long) primCellValue.getNumberValue());
                    }
                    break;
                case BOOLEAN:
                    cellValue = String.valueOf(primCellValue.getBooleanValue());
                    break;
                case FORMULA:
                    cellValue = String.valueOf(cell.getCellFormula());

                    break;
                default:
                    cellValue = null;
                }
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cellValue);
                    }
                } else {

                    current = new PaletteDetailDTO();

                    if (cellValue != null && !cellValue.isEmpty()) {
                        String col = disposition.columns.get(cell.getColumnIndex());
                        if (col == null) {
                            col = "";
                        }
                        switch (col) {
                        case OrderColumns.REF:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1;
                                orderId = cellValue;
                            }
                            break;
                        case OrderColumns.DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                validityLevel -= 1 << 1;
                                movementDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    movementDate = df.parse(test);
                                    validityLevel -= 1 << 1;
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case OrderColumns.STATUS:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1 << 2;
                                status = cellValue;
                                statusLevel = Integer.parseInt(Orders.Status.getStatusFromLabel(status));
                            }
                            break;
                        case OrderColumns.NOTE:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                // validityLevel -= 1<<3;
                                note = cellValue;

                            }
                            break;
                        case OrderColumns.ORIG_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1 << 3;
                                orig = cellValue;

                            }
                            break;
                        case OrderColumns.DEST_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1 << 4;
                                dest = cellValue;
                                //                                locations.setId(dest);
                            }
                            break;
                        case OrderColumns.PRODUCT_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityLevel -= 1 << 5;
                                current.setProductCode(cellValue);
                                //                                products.setId(cellValue);
                                productResultDTO = new ProductsResultDTO();
                                productResultDTO.setId(cellValue);
                            }
                            break;
                        case OrderColumns.ORDERED_QTY:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityQty = 1;
                                ordered_qty = Double.parseDouble(cellValue);
                                current.setQuantityFromOrderDetail(ordered_qty);

                            }
                            break;
                        case OrderColumns.VALID_QTY:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityQty = 2;
                                valid_qty = Double.parseDouble(cellValue);
                            }
                            break;
                        case OrderColumns.PREP_QTY:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityQty = 3;
                                prep_qty = Double.parseDouble(cellValue);
                            }
                            break;
                        case OrderColumns.DELIV_QTY:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityQty = 5;
                                deliv_qty = Double.parseDouble(cellValue);
                            }
                            break;
                        case OrderColumns.RECEPT_QTY:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                validityQty = 6;
                                recept_qty = Double.parseDouble(cellValue);
                            }
                            break;
                        case OrderColumns.PALET_NUM:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                palette = cellValue;
                            }
                            break;
                        case OrderColumns.USER_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                people.setId(cellValue);

                            }
                            break;
                        case OrderColumns.INT_ORDER_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                internalOrderId = cellValue;

                            }
                            break;
                        case OrderColumns.INT_ODETAIL_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                internalODetailId = cellValue;

                            }
                            break;
                        case OrderColumns.INT_PAL_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                internalPalId = cellValue;

                            }
                            break;
                        case OrderColumns.INT_PDETAIL_ID:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                internalPDetailId = cellValue;

                            }
                            break;
                        case OrderColumns.ORDERED_USER:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                oUser.setId(cellValue);

                            }
                            break;
                        case OrderColumns.VALID_USER:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                vUser.setId(cellValue);

                            }
                            break;
                        case OrderColumns.PREP_USER:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                pUser.setId(cellValue);

                            }
                            break;
                        case OrderColumns.DELIV_USER:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                dUser.setId(cellValue);

                            }
                            break;
                        case OrderColumns.RECEPT_USER:
                            if (cellValue != null && !cellValue.isEmpty()) {
                                rUser.setId(cellValue);

                            }
                            break;
                        case OrderColumns.ORDERED_DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                orderDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    orderDate = df.parse(test);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case OrderColumns.VALID_DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                validDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    validDate = df.parse(test);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case OrderColumns.PREP_DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                prepDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    prepDate = df.parse(test);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case OrderColumns.DELIV_DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                delivDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    delivDate = df.parse(test);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case OrderColumns.RECEPT_DATE:
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                receptDate = cell.getDateCellValue();
                            }
                            if (cell.getCellType() == CellType.STRING) {
                                //
                                String test = "01/01/2017 00:00";
                                test = cell.getStringCellValue();
                                String format = "dd/MM/yyyy HH:mm";
                                DateFormat df = new SimpleDateFormat(format);
                                try {
                                    receptDate = df.parse(test);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            break;

                        }
                    }
                }
            }

            if (row.getRowNum() > 0 && validityQty >= statusLevel) {
                validityLevel -= 1 << 6;
                // on reconstruit ce qu'il manque sur les quantités :
                if (deliv_qty == null && recept_qty != null) {
                    deliv_qty = recept_qty;
                }
                if (prep_qty == null && deliv_qty != null) {
                    prep_qty = deliv_qty;
                }
                if (valid_qty == null && prep_qty != null) {
                    valid_qty = prep_qty;
                }
                if (ordered_qty == null && valid_qty != null) {
                    ordered_qty = valid_qty;
                }

            }

            //Si la ligne est valide on la traite
            //on va avoir une liste d'objets dans notre map pour la clef correspondant à l'iD de la commande
            // Map orderId (nom commande)-> liste palette detail <- on injecte notre objet on ne le modifiera jamais
            // Map orderDetailId -> ordersdetail <- on veut pouvoir le modifier
            // Map orderId -> order
            // Map paletteId -> palette

            //Faut il un moyen de tout regrouper sous une commande  ou alors la description de notre fichier suffit ? 
            if (row.getRowNum() > 0 && validityLevel == 0) {
                //D'abord le nom de la palette
                if (palette != null && palette.startsWith(orderId)) {
                    paletteName = palette;
                } else {
                    paletteName = orderId.concat(palette == null ? "" : " - ".concat(palette));
                }

                //Avant on imposait d'avoir les éléments d'une même commande consécutifs pas d'intérêt tout de suite


                nbOrders++;

                currentOrder = ordersMap.get(orderId);
                if(currentOrder == null) {
                    //Pas trouvé dans notre map on va le mettre dedans
                    currentOrder = new OrderDTO();
                    if (internalOrderId == null || internalOrderId.isEmpty()) {
                        internalOrderId = OrdersService.createOrderId(dest);
                    } 
                    currentOrder.setId(internalOrderId);
                    ordersMap.put(orderId , currentOrder);

                    ordersId.put(internalOrderId , orderId);
                    ordersAlreadyReceived.put(internalOrderId , false);

                    orig_location.setId(orig);
                    currentOrder.setLocationFrom(orig_location);
                    orig_location = new LocationsInfoDTO();
                    orig_location.setId(orig);
                    dest_location.setId(dest);
                    currentOrder.setLocationTo(dest_location);
                    dest_location = new LocationsInfoDTO();
                    dest_location.setId(dest);
                    currentOrder.setReference(orderId);
                    currentOrder.setUsernameId(oUser.getId() == null ? people.getId() : oUser.getId());
                    currentOrder.setCreationDate(orderDate == null ? movementDate : orderDate);
                    currentOrderStatus = 0;
                    currentOrderODetailsMap = new HashMap<String,OrderDetailDTO>();
                    ordersOrdersDetailsMap.put(orderId, currentOrderODetailsMap);
                    //                    paletteDetails = new ArrayList<PaletteDetailDTO>();
                    //                    ordersPaletteDetailsMap.put(orderId, paletteDetails);
                    //Avant on sauvait la commande
                    //Maintenant ce sera fait en fin de parcours
                } else {
                    currentOrderStatus = currentOrder.getStatusOrder();
                    currentOrderODetailsMap = ordersOrdersDetailsMap.get(orderId);
                    //                    paletteDetails = ordersPaletteDetailsMap.get(orderId);
                }

                //Avant on ne faisait rien si la commande était déjà réceptionné
                //Maintenant ce n'est pas à ce stade qu'on décide
                //TODO maintenir le garde fou lors de l'opération sauvegarde

                //                    currentTransit = locationDAO.findById(currentOrder.getLocationToId() + "-TRANSIT").orElse(null);
                //                    
                //                    // On ne fait rien si la commande est déjà réceptionnée
                //                    toBeUpdated = (currentOrderStatus != Integer.parseInt(Orders.Status.RECEPTED));


                //En gros on va passer ici à chaque ligne donc on ne rempli les infos sur les dates et les personnes au niveau commande
                // - que la première fois
                // - que si on a de quoi compléter
                if (statusLevel != null && statusLevel > currentOrderStatus) {

                    // les users
                    // les dates
                    if (statusLevel > Integer.parseInt(Orders.Status.CREATION) && currentOrder.getPlanifDate() == null) {
                        currentOrder.setPlanifDate(validDate == null ? movementDate : validDate);
                        currentOrder.setPlanifUserId(vUser.getId() == null ? people.getId() : vUser.getId());
                    }

                    if (statusLevel > Integer.parseInt(Orders.Status.PLANIFICATION) && currentOrder.getPrepDate() == null) {
                        currentOrder.setPrepDate(prepDate == null ? movementDate : prepDate);
                        currentOrder.setPrepUserId(pUser.getId() == null ? people.getId() : pUser.getId());
                    }

                    if (statusLevel > Integer.parseInt(Orders.Status.PREPARATION) && currentOrder.getControlDate() == null) {
                        currentOrder.setControlDate(delivDate == null ? movementDate : delivDate);
                        currentOrder.setControlUserId(dUser.getId() == null ? people.getId() : dUser.getId());
                    }

                    if (statusLevel > Integer.parseInt(Orders.Status.CONTROLE) && currentOrder.getDelivDate() == null) {
                        currentOrder.setDelivDate(delivDate == null ? movementDate : delivDate);
                        currentOrder.setDelivUserId(dUser.getId() == null ? people.getId() : dUser.getId());
                    }

                    if (statusLevel > Integer.parseInt(Orders.Status.EXPEDITION) && currentOrder.getReceptDate() == null) {
                        currentOrder.setReceptDate(receptDate == null ? movementDate : receptDate);
                        currentOrder.setReceptUserId(rUser.getId() == null ? people.getId() : rUser.getId());
                    }

                    // le status
                    currentOrder.setStatus(status);
                    currentOrder.setStatusOrder(statusLevel);
                }

                //il nous faut l'orderdetail on peut le chopper via commande + produit
                OrderDetailDTO oDetail = currentOrderODetailsMap.get(productResultDTO.getId());


                // Cas des Palette Detail et du lien avec les orderDetail
                // Qté Commandée = Order
                // Qté Validée = Order
                // Qté Préparée = Palette sigma => order
                // Qté Expédiée = Palette sigma => order
                // Qté Reçue = palette sigma => order

                if (oDetail == null) {
                    oDetail = new OrderDetailDTO();
                    oDetail.setProduct(productResultDTO);
                    currentOrderODetailsMap.put(productResultDTO.getId(), oDetail);

                    if (internalODetailId == null || internalODetailId.isEmpty()) {
                        internalODetailId =  OrdersService.createOrderDetailId(dest ,  productResultDTO.getId());
                    }
                    oDetail.setId(internalODetailId);
                    oDetail.setOrderId(currentOrder.getId());
                    oDetail.setQuantityAsked(ordered_qty);
                    oDetail.setQuantityToPrepare(valid_qty);
                }

                if(statusLevel != null && statusLevel > Integer.parseInt(Orders.Status.PLANIFICATION)) {
                    //Nous avons forcement des palettes à gérer à ce stade
                    currentPalette = palettesMap.get(paletteName);
                    //idem que les ordres avant il fallait que les lignes d'une palette donnée se suivent ... plus utile
                    if(currentPalette == null) {
                        currentPalette = new PaletteDTO();
                        if (internalPalId == null || internalPalId.isEmpty()) {
                            internalPalId = OrdersService.createPaletteId(currentOrder.getId());
                        }
                        currentPalette.setId(internalPalId);
                        palettesMap.put(paletteName , currentPalette);
                        currentPalette.setOrderId(currentOrder.getId());
                        currentPalette.setName(paletteName);
                        currentPalette.setCreationUser(oUser.getId() == null ? people : oUser);
                        currentPalette.setPlanificationDate(validDate == null ? movementDate : validDate);
                        currentPalette.setPlanificationUser(vUser.getId() == null ? people : vUser);
                        currentPalette.setCreationDate(orderDate == null ? movementDate : orderDate);
                        paletteDetails = new ArrayList<PaletteDetailDTO>();
                        palettePaletteDetailsMap.put(internalPalId, paletteDetails);
                    } else {
                        paletteDetails = palettePaletteDetailsMap.get(currentPalette.getId());
                    }


                    // update données palette
                    if (statusLevel != null && statusLevel > currentPalette.getStatusOrder()) {

                        // les users
                        // les dates
                        if (statusLevel > Integer.parseInt(Orders.Status.CREATION) && currentPalette.getPlanificationDate() == null) {
                            currentPalette.upgPlanificationDate(validDate == null ? movementDate : validDate);
                            currentPalette.setPlanificationUserId(vUser.getId() == null ? people.getId() : vUser.getId());
                        }

                        if (statusLevel > Integer.parseInt(Orders.Status.PLANIFICATION) && currentPalette.getPreparationDate() == null) {
                            currentPalette.upgPreparationDate(prepDate == null ? movementDate : prepDate);
                            currentPalette.setPreparationUserId(pUser.getId() == null ? people.getId() : pUser.getId());
                        }

                        if (statusLevel > Integer.parseInt(Orders.Status.PREPARATION) && currentPalette.getControlDate() == null) {
                            currentPalette.upgControlDate(delivDate == null ? movementDate : delivDate);
                            currentPalette.setControlUserId(dUser.getId() == null ? people.getId() : dUser.getId());
                        }

                        if (statusLevel > Integer.parseInt(Orders.Status.CONTROLE) && currentPalette.getDelivDate() == null) {
                            currentPalette.upgDelivDate(delivDate == null ? movementDate : delivDate);
                            currentPalette.setDelivUserId(dUser.getId() == null ? people.getId() : dUser.getId());
                        }

                        if (statusLevel > Integer.parseInt(Orders.Status.EXPEDITION) && currentPalette.getReceptDate() == null) {
                            currentPalette.upgReceptDate(receptDate == null ? movementDate : receptDate);
                            currentPalette.setReceptUserId(rUser.getId() == null ? people.getId() : rUser.getId());
                        }

                        // le status
                        currentPalette.setStatus(status);
                        currentPalette.setStatusOrder(statusLevel);
                    }




                    //Par construction nous n'avons pas déjà traité cette ligne de palette 
                    //Sinon ça veut dire deux lignes pour le même produit même palette dans un fichier donné 


                    // On choppe le detail palette
                    PaletteDetailDTO pDetail = null;

                    if (pDetail == null) {
                        pDetail = new PaletteDetailDTO();

                        pDetail.setProductCode(productResultDTO.getId());

                        if (internalPDetailId == null || internalPDetailId.isEmpty()) {
                            internalPDetailId = OrdersService.createPaletteDetailId(internalODetailId);
                        }
                        pDetail.setId(internalPDetailId);
                        pDetail.setOrderDetailId(oDetail.getId());
                        pDetail.setPaletteId(currentPalette.getId());
                        pDetail.setQuantityFromOrderDetail(oDetail.getQuantityAsked());
                        pDetail.setQuantityToPrepare(oDetail.getQuantityToPrepare());
                        paletteDetails.add(pDetail);
                    }



                    // combo entre orderStatus et palette Status ?
                    // Dans quel cas on va avoir un delta ?
                    // Le cas ou la commande en entre et qu'on decouvre la palette
                    // Dans ce cas

                    //EDU 2020-10 Désormais il faut passer à chaque fois le statut palette ne provenant pas de la base 
                    //Et les détails de la commande ne sont plus préremplis
                    //TODO check Historiquement on était compatible avec plusieurs intégration successives avec des données différentes 
                    //Integ 1 qté préparée 1
                    //Integ 2 qté préparée 2 ( =+1)
                    //Pour pouvoir le faire il est nécessaire de consulter l'tat de la palette dans le système 
                    //or désormais on ne consulte plus du tout les données pendant le parcours du fichier en input
                    //On laisse les calculs tels-quels ça permet de s'affranchir de doublons éventuels à l'intérieur du fichier

                    oDetail.setQuantityAsked(ordered_qty);
                    oDetail.setQuantityToPrepare(oDetail.getQuantityToPrepare() + valid_qty - pDetail.getQuantityToPrepare());

                    pDetail.setQuantityFromOrderDetail(oDetail.getQuantityAsked());

                    pDetail.setQuantityToPrepare(valid_qty);
                    oDetail.setQuantityPrepared(oDetail.getQuantityPrepared() - pDetail.getQuantityPrepared() + prep_qty);
                    pDetail.setQuantityPrepared(prep_qty);


                    Double deltas = deliv_qty - pDetail.getQuantitySent();
                    oDetail.setQuantitySent(oDetail.getQuantitySent() + deltas);
                    pDetail.setQuantitySent(deliv_qty);

                    Double deltar = recept_qty - pDetail.getQuantityReceived();
                    oDetail.setQuantityReceived(oDetail.getQuantityReceived() + deltar);
                    pDetail.setQuantityReceived(recept_qty);


                    //TODO bien faire gaffe aux calculs impliquant palette et ligne de commande
                    //Tel quel notre oDetail n'est pas complet


                    if (note != null && !note.isEmpty()) {
                        pDetail.setNote(note);
                    }
                    pDetail.setStatus(Orders.Status.getStatusFromLabel(status));

                    // On retranscrit toutes les modifications de detail palette dans detail commande
                    pDetail.setPreparationDate(prepDate == null ? movementDate : prepDate);
                    pDetail.setPreparationUserId(pUser.getId() == null ? people.getId() : pUser.getId());

                } else {
                    //Sinon nous n'avons 'que' des OrdersDetails



                    // On traite le détail au vu de ces infos
                    // On déduit le status antérieur de cette ligne de commande
                    // on update toutes les lignes suivantes jusqu'au nouveau status

                    // Si on est là on ne parle pas de palette donc ça fonctionne
                    //EDU 2020-10 Désormais il faut passer à chaque fois le statut palette ne provenant pas de la base 
                    //Et les détails de la commande ne sont plus préremplis
                    //TODO check
                    //Avant on ne mettait à jour que les valeurs 'postérieures' au status de la palette
                    //Comme désormais à ce stade nous ne possédons pas cette info, 

                    oDetail.setQuantityAsked(ordered_qty);
                    oDetail.setQuantityToPrepare(valid_qty);
                    oDetail.setQuantityPrepared(prep_qty);
                    oDetail.setQuantityPrepared(prep_qty);
                    oDetail.setQuantitySent(deliv_qty);
                    oDetail.setQuantityReceived(recept_qty);


                    // Pas de note sur les détails de commande donc on remonte sur la commande elle même
                    if (note != null && !note.isEmpty()) {
                        currentOrder.setNote(note);
                    }
                }
                // FIN ELSE PALETTE


            }
        }

        long lEndTime = System.nanoTime();
        System.out.println("Durée parcours Excel : " + (lEndTime - lStartTime)/1000000);
        lStartTime = System.nanoTime();

        //On va générer les mouvements de stocks liés à cette palette

        //Il faut faire départ -> arrivée-TRANSIT si on est en expédié
        //Il faut faire arrivée TRANSIT -> Arrivée si on est en livrée

        //        Version naive : on parcours tous les éléments de notre liste de palette details
        //        pour les trois mouvements potentiels
        //            si on trouve le mouvmeent associé on ne fait rien
        //            sinon on le crée et on modifie stock current
        //            
        //            Bonus pouvoir faire les deux en transactionnel

        //       Version complémentaire que cette fonction soit lancée depuis le job

        //Attention à le faire commande par commande
        for( PaletteDTO paletteDto : palettesMap.values()) {
            OrderDTO order = ordersMap.get(ordersId.get(paletteDto.getOrderId()));
            if(!ordersAlreadyReceived.get(order.getId())) {
                if(!stockService.createOrderStockMoves(palettePaletteDetailsMap.get(paletteDto.getId()) , order , paletteDto)) {
                    //On a une info sur la commande - déjà réceptionnée
                    ordersAlreadyReceived.put(order.getId() , true);
                }
            }
        }



        lEndTime = System.nanoTime();
        System.out.println("Durée Sauvegarde Mvts : " + (lEndTime - lStartTime)/1000000);

        lStartTime = System.nanoTime();

        for (OrderDTO orderDto : ordersMap.values()) {
            if(!ordersAlreadyReceived.get(orderDto.getId())) {
                Job job = null;
                List<OrderDetailDTO> ordersDetailsDto = ordersOrdersDetailsMap.get(orderDto.getReference()).values().stream().collect(Collectors.toList());
                /**
                 * EDU 2020 Si on importe une commande dans un état moins égal à Planification alors il faut en informer le back central = création d'un job
                 * d'upload vers le back central
                 */

                orderDto.setOrderDetails(new ArrayList<OrderDetailDTO>());
                orderDto = ordersService.saveOrder(orderDto);

                if(orderDto != null) {
                    
                    for(OrderDetailDTO detail : ordersDetailsDto) {
                        detail.setOrderId(orderDto.getId());
                    }
                    
                    //TODO Vérifier l'état des palettes + palettes details associées
                    //TODO Vérifier qu'on a toutes les infos nécessaires car désormais on n'a plus de lien avec la base à ce moment
                    if (orderDto.getStatusOrder() > Integer.parseInt(Orders.Status.CREATION)) {
                        orderDto.setOrderDetails(ordersDetailsDto);
                        try {
                            job = JobHelper.initJobCommandePoussee();
                            JobHelper.fillJobCommandes(job, orderDto, null);
                        } catch (JsonProcessingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    
                    ordersService.saveOrderDetails(ordersDetailsDto);
    
                    if (job != null) {
                        jobService.save(job);
                    }
                }
            }
        } 
        lEndTime = System.nanoTime();
        System.out.println("Durée Sauvegarde Order : " + (lEndTime - lStartTime)/1000000);

        // EDU 2020 02 On balance toutes les palettes chargées dans notre fichier d'un coup
        // Si on a une palette c'est qu'on a forcément passé le cap de la planification
        // Donc ça remonte

        List<PaletteDTO> palettesMapCopy = new ArrayList<PaletteDTO>();
        palettesMapCopy.addAll(palettesMap.values());
        for (PaletteDTO paletteDto : palettesMapCopy) {
            if(!ordersAlreadyReceived.get(paletteDto.getOrderId())) {
                List<PaletteDetailDTO> palettesDetailsDto = palettePaletteDetailsMap.get(paletteDto.getId());
                Object palRetour  = ordersService.savePalette(paletteDto);
                if(palRetour != null) {
                    ordersService.savePaletteDetails(palettesDetailsDto);
                    paletteDto.setPaletteDetails(new ArrayList<PaletteDetailDTO>());
                } else {
                    palettesMap.remove(paletteDto.getName());
                }
            } else {
                palettesMap.remove(paletteDto.getName());
            }
        }

        lEndTime = System.nanoTime();
        System.out.println("Durée sauv Palettes : " + (lEndTime - lStartTime)/1000000);

        if (!palettesMap.isEmpty()) {
            Job job = null;
            try {
                job = JobHelper.initJobCommandePoussee();
                JobHelper.fillJobCommandes(job, null, palettesMap.values().stream().collect(Collectors.toList()));
            } catch (JsonProcessingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (job != null) {
                jobService.save(job);
            }
        }


        retour += String.valueOf(nbOrders) + " lignes de commandes importées - ";
        return retour;

    }



}
