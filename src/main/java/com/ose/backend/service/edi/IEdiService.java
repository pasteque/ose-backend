package com.ose.backend.service.edi;

import java.io.IOException;
import java.net.URL;

import org.apache.poi.ss.usermodel.Workbook;

import com.ose.backend.dto.utils.ExportConstraintsDTO;

public interface IEdiService {
    
    String importFile(String filecontent, String locationId);
    
    String importWorkbook(Workbook wb, String locationId);

    Object exportFile(String file, ExportConstraintsDTO dto);

    String importRessource(URL url, String locationId) throws IOException;
    
    String importRole(URL url, String locationId) throws IOException;

}
