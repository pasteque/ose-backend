package com.ose.backend.service.edi.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.pasteque.api.dao.customers.ICustomersApiDAO;
import org.pasteque.api.dao.customers.IDiscountProfilApiDAO;
import org.pasteque.api.dao.locations.IInseeApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.tickets.ITicketsApiDAO;
import org.pasteque.api.dto.customers.CustomersDetailDTO;
import org.pasteque.api.dto.customers.CustomersParentDTO;
import org.pasteque.api.dto.customers.POSDiscountProfilDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.model.customers.Customers;
import org.pasteque.api.model.customers.DiscountProfil;
import org.pasteque.api.model.saleslocations.Insee;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.tickets.Tickets;
import org.pasteque.api.service.customers.ICustomersService;
import org.pasteque.api.util.DamerauLevenshteinDistance;
import org.pasteque.api.util.FilteringAndFormattingData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.application.IApplicationDAO;
import com.ose.backend.dao.locations.IPaysDAO;
import com.ose.backend.dto.customers.TicketCustomerDTO;
import com.ose.backend.model.locations.Pays;
import com.ose.backend.service.edi.ICustomerEdiService;
import com.ose.backend.service.tickets.ITicketsServiceOSE;

@Service
public class CustomerEdiService implements ICustomerEdiService {

    @Autowired
    private IDiscountProfilApiDAO discountProfilDAO;
    @Autowired
    private IPaysDAO paysDAO;
    @Autowired
    private ILocationsApiDAO locationDAO;
    @Autowired
    private IInseeApiDAO inseeDAO;
    @Autowired
    private ICustomersApiDAO customersDAO;
    @Autowired
    private ITicketsApiDAO ticketsDAO;
    @Autowired
    private ICustomersService customersService;
    @Autowired
    private IApplicationDAO applicationDAO;
    @Autowired
    private ITicketsServiceOSE ticketsService;

    // Le nom des colonnes connues
    public class CustomerColumns {
        public static final String NOM = "Nom";
        public static final String PRENOM = "Prenom";
        public static final String CODEMAG = "CodeMagasinOrigine";
        public static final String CODECLIENT = "CodeClient";
        public static final String CODECARTE = "NumCarte";
        public static final String CREATION = "DateInscription";
        public static final String MODIFICATION = "DateModification";
        public static final String ZIPCODE = "CodePostal";
        public static final String CITY = "Ville";
        public static final String COUNTRY = "Pays";
        public static final String ADDRESS = "Adresse";
        public static final String ADDRESS2 = "ComplementAdresse";
        public static final String MOBILE = "TelephonePortable";
        public static final String PHONE = "TelephoneFixe";
        public static final String BIRTH = "DateNaissance";
        public static final String MAIL = "Email";
        public static final String CIV = "Civilite";
        public static final String NEWS = "InscNewsletter";
        public static final String PART = "OffrePartenaire";
        public static final String TYPE = "TypeContact";
        public static final String SOURCE = "SourceContact";
        public static final String CODE = "CodeClient";
        public static final String DOUBLON = "Parent";
        public static final String IDCLIENT = "IdClient";
        public static final String PROFIL = "Profil";
        public static final String INSEE = "Insee";
        public static final String CODEMAGRAT = "CodeMagasinRattachement";
        public static final String MAGRAT = "MagasinRattachement";
        public static final String CODEPARENT = "CodeParent";
        public Map<Integer, String> columns;
    }

    // Le nom des préfixes connus pour les colonnes de la déduplication des clients
    public class CustomerDeduplicationColumns {
        public static final String CODECARTE = "CodeClient";
        public static final String CODECARTEPARENT = "Parent";
        public static final String MODIF = "Modification";

        public Map<Integer, String> columns;
    }

    // Le nom des préfixes connus pour les colonnes de la déduplication des clients
    public class CustomerTicketColumns {
        public static final String NUMERO_TICKET = "N° Ticket";
        public static final String NUMERO_CLIENT = "N° Client";

        public Map<Integer, String> columns = new HashMap<Integer, String>();
    }

    @SuppressWarnings("deprecation")
    @Override
    public String importCustomerSheet(Sheet sheet, String retour, String locationId) {
        retour += " - Import fiches clients : ";
        int nbCust = 0;
        boolean isValidCust = false;
        discountProfilDAO.findAll();
        List<CustomersDetailDTO> listCustomers = new ArrayList<CustomersDetailDTO>();
        String cellValue = null, city = null, zip = null , insee = null;
        CustomerColumns disposition = new CustomerColumns();
        disposition.columns = new HashMap<Integer, String>();
        for (Row row : sheet) {
            isValidCust = false;
            CustomersDetailDTO currentCust = new CustomersDetailDTO();
            currentCust.setCivilite("STE");
            zip = null;
            city = null;
            insee = null;
            for (Cell cell : row) {
                if (row.getRowNum() == 0) {
                    // Première Ligne on récupère la disposition
                    if (cell.getCellType() == CellType.STRING) {
                        disposition.columns.put(cell.getColumnIndex(), cell.getStringCellValue());
                    }
                } else {
                    switch (cell.getCellType()) {
                    case STRING:
                        cellValue = cell.getStringCellValue();
                        break;
                    case NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            cellValue = cell.getDateCellValue().toString();
                        } else {
                            cellValue = String.valueOf((long) cell.getNumericCellValue());
                        }
                        break;
                    case BOOLEAN:
                        cellValue = String.valueOf(cell.getBooleanCellValue());
                        break;
                    case FORMULA:
                        cellValue = String.valueOf(cell.getCellFormula());
                        break;
                    default:
                        cellValue = null;
                    }
                    if (cellValue != null && !cellValue.isEmpty()) {
                        switch (disposition.columns.get(cell.getColumnIndex())) {
                        case CustomerColumns.IDCLIENT:
                            currentCust.setId(cellValue.toUpperCase());
                            break;
                        case CustomerColumns.ADDRESS:
                            currentCust.setAddress1(cellValue.toUpperCase());
                            break;
                        case CustomerColumns.ADDRESS2:
                            currentCust.setAddress2(cellValue.toUpperCase());
                            break;
                        case CustomerColumns.BIRTH:
                            if (cell.getCellType() == CellType.STRING) {
                                try {
                                    cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").parse(cell.toString()));
                                    CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                                    CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
                                    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
                                    cell.setCellStyle(cellStyle);
                                    cell.setCellType(CellType.NUMERIC);
                                } catch (ParseException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                currentCust.setDateOfBirth(cell.getDateCellValue());
                            }
                            break;
                        case CustomerColumns.CITY:
                            city = cellValue.toUpperCase();
                            break;
                        case CustomerColumns.CIV:
                            switch (cellValue) {
                            case "Monsieur":
                                currentCust.setCivilite("M");
                                break;
                            case "Madame":
                                currentCust.setCivilite("MME");
                                break;
                            default:
                                currentCust.setCivilite("STE");
                            }
                            break;
                        case CustomerColumns.CODECLIENT:
                            currentCust.setSearchKey(cellValue);
                            currentCust.setCardNumber("c" + cellValue);
                            isValidCust = currentCust.getLastName() != null;
                            break;
                        case CustomerColumns.CODECARTE:
                            currentCust.setSearchKey(cellValue);
                            currentCust.setCardNumber("c" + cellValue);
                            isValidCust = currentCust.getLastName() != null;
                            break;
                        case CustomerColumns.COUNTRY:
                            Pays pays = paysDAO.findByNom(cellValue.toUpperCase());
                            if (pays != null) {
                                currentCust.setCountry(pays.getNom());
                            }
                            break;
                        case CustomerColumns.CREATION:
                            if (cell.getCellType() == CellType.STRING) {
                                try {
                                    cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").parse(cell.toString()));
                                } catch (ParseException e) {
                                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    Date date = new Date();
                                    try {
                                        cell.setCellValue(dateFormat.parse(dateFormat.format(date)));
                                    } catch (ParseException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                                CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
                                cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
                                cell.setCellStyle(cellStyle);
                                cell.setCellType(CellType.NUMERIC);
                            }
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                currentCust.setCreationDate(cell.getDateCellValue());
                            }
                            break;
                        case CustomerColumns.MODIFICATION:
                            if (cell.getCellType() == CellType.STRING) {
                                try {
                                    cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").parse(cell.toString()));
                                } catch (ParseException e) {
                                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    Date date = new Date();
                                    try {
                                        cell.setCellValue(dateFormat.parse(dateFormat.format(date)));
                                    } catch (ParseException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                    e.printStackTrace();
                                }
                                CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
                                CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
                                cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
                                cell.setCellStyle(cellStyle);
                                cell.setCellType(CellType.NUMERIC);
                            }
                            if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                                currentCust.setLastUpdate(cell.getDateCellValue());
                            }
                            break;
                        case CustomerColumns.MAIL:
                            currentCust.setEmail(cellValue);
                            break;
                        case CustomerColumns.MOBILE:
                            if (cellValue.indexOf("+") == 0) {
                                cellValue = "+" + cellValue.replaceAll("\\D", "");
                            } else {
                                cellValue = cellValue.replaceAll("\\D", "");
                            }
                            currentCust.setPhone1(cellValue);
                            break;
                        case CustomerColumns.NEWS:
                            currentCust.setNewsletter("1".equals(cellValue));
                            break;
                        case CustomerColumns.NOM:
                            currentCust.setLastName(cellValue.toUpperCase());
                            isValidCust = currentCust.getSearchKey() != null;
                            break;
                        case CustomerColumns.PART:
                            currentCust.setPartenaires("1".equals(cellValue));
                            break;
                        case CustomerColumns.PHONE:
                            if (cellValue.indexOf("+") == 0) {
                                cellValue = "+" + cellValue.replaceAll("\\D", "");
                            } else {
                                cellValue = cellValue.replaceAll("\\D", "");
                            }
                            currentCust.setPhone2(cellValue);
                            break;
                        case CustomerColumns.PRENOM:
                            currentCust.setFirstName(cellValue.toUpperCase());
                            break;
                        case CustomerColumns.ZIPCODE:
                            zip = cellValue;
                            break;
                        case CustomerColumns.INSEE:
                            insee = cellValue;
                            break;
                        case CustomerColumns.PROFIL:
                            DiscountProfil discountProfil = discountProfilDAO.findByName(cellValue);
                            if (discountProfil != null) {
                                currentCust.setDiscountProfile(discountProfil.getAdapter(POSDiscountProfilDTO.class));
                            }
                            break;
                        case CustomerColumns.CODEMAGRAT:
                            Locations location = locationDAO.findById(cellValue).orElse(null);
                            if (location != null) {
                                currentCust.setLocation(location.getAdapter(LocationsInfoDTO.class));
                            }
                            break;
                        case CustomerColumns.CODEPARENT:
                            // currentCust.setLocation(location.getAdapter(LocationsInfoDTO.class));
                            break;
                        default:

                        }
                    }
                    /*
                     * switch (cell.getCellType()) { case CellType.STRING:
                     * System.out.println(cell.getRichStringCellValue().getString()); break; case CellType.NUMERIC: if
                     * (DateUtil.isCellDateFormatted(cell)) { System.out.println(cell.getDateCellValue()); } else {
                     * System.out.println(cell.getNumericCellValue()); } break; case CellType.BOOLEAN:
                     * System.out.println(cell.getBooleanCellValue()); break; case CellType.FORMULA:
                     * System.out.println(cell.getCellFormula()); break; default: System.out.println(); }
                     */
                }
            }
            //EDU 2021 - 03 On incorpore l'import sleon INSEE
            //dans la logique si insee présent on prend
            //on verifie si le cp est dans la liste si oui -> ok
            //si non ou pas de cp on regarde la commune
            //si tout est ko, on regarde selon commune + cp
            //
            //nouvelle règle contient et start with on vire les ST SAINT les - et les '
            //les - et ' remplacées par espaces
            //STE -> SAINT 
            //ST -> SAINTE
            if (insee != null || zip != null || city != null) {
                // Trouver les infos qui collent
                // On part du principe que le problème vient de la ville
                // Et on attribue toujours une ville - celle dont le nom est le plus proche
                // Au sens de la distance de Damereau Livenshtein
                List<Insee> candidates = null ; 
                String finalCity = city;
                String finalInsee = null;
                if(insee != null) {
                    candidates = inseeDAO.findByZipCodeAndInseeNum(zip, insee);
                    if (candidates.size() == 1) {
                        currentCust.setCity(candidates.get(0).getCommune());
                        currentCust.setZipCode(zip);
                        currentCust.setInsee(candidates.get(0).getId());
                        finalInsee = insee;
                    } 

                    if(finalInsee == null) {
                        candidates = inseeDAO.findByInseeNum(insee);
                        Insee inseeElem = findMatchingInsee(candidates , finalCity);
                        if(inseeElem != null) {
                            currentCust.setCity(inseeElem.getCommune());
                            currentCust.setZipCode(inseeElem.getZipCode());
                            currentCust.setInsee(inseeElem.getId());
                            finalInsee = insee;
                        }

                    }
                }

                if(finalInsee == null) {
                    candidates = inseeDAO.findByZipCode(zip);
                    Insee inseeElem = findMatchingInsee(candidates , finalCity);
                    if(inseeElem != null) {
                        currentCust.setCity(inseeElem.getCommune());
                        currentCust.setZipCode(inseeElem.getZipCode());
                        currentCust.setInsee(inseeElem.getId());
                        finalInsee = inseeElem.getInseeNum();
                    }

                }
            }
            if (row.getRowNum() > 0 && isValidCust) {
                nbCust++;
                if (currentCust.getCountry() == null) {
                    currentCust.setCountry("FRANCE");
                }
                listCustomers.add(currentCust);

            }
        }
        saveCustomers(listCustomers);
        retour += String.valueOf(nbCust) + " Clients importés - ";
        return retour;
    }

    //nouvelle règle contient et start with on vire les ST SAINT les - et les ' 
    //les - et ' remplacées par espaces
    //STE -> SAINT 
    //ST -> SAINTE
    //é -> E
    //é -> E
    //ê -> E
    //à -> A
    //ô -> O
    //â -> A
    //Ces remplacements sont confiés à FilteringAndFormattingData.filterCityName
    //TODO Attention si modifications ultérieures 
    //Retrouver dans une liste d'insee celui qui correspond exactement ou le mieux au nom de ville proposé
    private Insee findMatchingInsee(List<Insee> candidates, String city) {
        Insee retour = null;
        if (candidates.size() > 0) {
            if (candidates.size() == 1 || city == null) {
                retour = candidates.get(0);
            } else {
                int minDistance = city.length();
                //on modifie le nom de la ville
                String altCity = FilteringAndFormattingData.filterCityName(city);
                for (Insee currentInsee : candidates) {
                    if (currentInsee.getCommune().equals(city)) {
                        retour = currentInsee;
                        break;
                    }
                    String altCommune = FilteringAndFormattingData.filterCityName(currentInsee.getCommune());
                    if(altCommune.startsWith(altCity)) {
                        retour = currentInsee;
                        break;
                    }
                    int dist = new DamerauLevenshteinDistance().calculate(city,altCommune );
                    if (dist < minDistance) {
                        minDistance = dist;
                        retour = currentInsee;
                    }
                }
            }
        }
        return retour;
    }

    @Override
    public String importCustomerDeduplicationSheet(Sheet sheet, String retour, String locationId) {
        retour += " Import déduplication clients : ";
        int nbCust = 0;
        List<CustomersParentDTO> listCustomersParent = new ArrayList<CustomersParentDTO>();
        Map<String,CustomersParentDTO> listCustomersParentDedup = new HashMap<String,CustomersParentDTO>();
        String cellValue = null;
        CustomerDeduplicationColumns disposition = new CustomerDeduplicationColumns();
        disposition.columns = new HashMap<Integer, String>();
        boolean clientModifie = true;
        HashMap<String, Integer> columns = new HashMap<String, Integer>();

        for (Row row : sheet) {
            CustomersParentDTO customerParent = new CustomersParentDTO();
            if (row.getRowNum() > 0) {
                clientModifie = testLigneAModifier(columns, row) && testValiditeParentEnfant(columns, row);

            } else {
                for (int rang = 0; rang <= row.getPhysicalNumberOfCells() - 1; rang++) {
                    String Nom = row.getCell(rang).getStringCellValue();
                    columns.put(Nom, rang);
                }
            }

            if (clientModifie ) {

                for (Cell cell : row) {
                    if (row.getRowNum() == 0) {
                        // Première Ligne on récupère la disposition
                        if (cell.getCellType() == CellType.STRING) {
                            disposition.columns.put(cell.getColumnIndex(), cell.getStringCellValue());
                        }

                    } else {
                        switch (cell.getCellType()) {
                        case STRING:
                            cellValue = cell.getStringCellValue();
                            break;
                        case NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)) {
                                cellValue = cell.getDateCellValue().toString();
                            } else {
                                cellValue = String.valueOf((long) cell.getNumericCellValue());
                            }
                            break;
                        case BOOLEAN:
                            cellValue = String.valueOf(cell.getBooleanCellValue());
                            break;
                        case FORMULA:
                            cellValue = String.valueOf(cell.getCellFormula());
                            break;
                        default:
                            cellValue = null;
                        }

                        switch (disposition.columns.get(cell.getColumnIndex())) {
                        case CustomerDeduplicationColumns.CODECARTE:
                            customerParent.setSearchKey(cellValue);
                            break;
                        case CustomerDeduplicationColumns.CODECARTEPARENT:
                            customerParent.setSearchKeyParent(cellValue);
                            break;
                        }
                    }
                }
            }

            if (row.getRowNum() > 0 && clientModifie ) {
                nbCust++;
                listCustomersParent.add(customerParent);
            }
        }
        for (CustomersParentDTO customerParentDTO : listCustomersParent) {
            
            if (customerParentDTO.getSearchKey().equals(customerParentDTO.getSearchKeyParent()) || customerParentDTO.getSearchKeyParent().isEmpty()) {
                customerParentDTO.setSearchKeyParent(null);
            }
            //normalement le cas du searchkey vide ou null est écarté par le test initial 
                listCustomersParentDedup.put(customerParentDTO.getSearchKey(), customerParentDTO);
        }

        customersService.saveCustomerParent(listCustomersParentDedup, true);
        retour += String.valueOf(nbCust) + " lignes du fichier ont été traitées - ";
        return retour;
    }

    @Override
    public String importCustomerTicket(Sheet sheet, String retour) {
        retour += " Import enrichissement ticket/client : ";
        // InputSource sheetSource = new InputSource(sheet);
        CustomerTicketColumns mapping = new CustomerTicketColumns();
        List<TicketCustomerDTO> listData = new ArrayList<>();

        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                // Première Ligne on récupère le mapping des informations
                for (Cell cell : row) {
                    if (cell.getCellType() == CellType.STRING) {
                        mapping.columns.put(cell.getColumnIndex(), cell.getStringCellValue().trim());
                    }
                }
            } else {
                TicketCustomerDTO dto = new TicketCustomerDTO();
                for (Cell cell : row) {
                    // C'est un peu moche mais ca ne doit pas être autre chose
                    if (cell.getCellType() == CellType.STRING) {
                        switch (mapping.columns.get(cell.getColumnIndex())) {
                        case CustomerTicketColumns.NUMERO_CLIENT:
                            dto.setCustomerSearchKey(cell.getStringCellValue());
                            break;
                        case CustomerTicketColumns.NUMERO_TICKET:
                            dto.setTicketNumber(cell.getStringCellValue());
                            break;
                        default:
                            break;
                        }
                    }
                    if (cell.getCellType() == CellType.NUMERIC) {
                        String value = String.format("%.0f", cell.getNumericCellValue());
                        switch (mapping.columns.get(cell.getColumnIndex())) {
                        case CustomerTicketColumns.NUMERO_CLIENT:
                            dto.setCustomerSearchKey(value);
                            break;
                        case CustomerTicketColumns.NUMERO_TICKET:
                            dto.setTicketNumber(value);
                            break;
                        default:
                            break;
                        }
                    }
                }
                listData.add(dto);
            }
        }
        retour = saveTicketsExtended(listData);
        return retour;
    }

    private String saveTicketsExtended(List<TicketCustomerDTO> listData) {
        int nbTicket = 0;
        int nbOK = 0;
        File rejet = null;
        BufferedWriter writer = null;
        try {
            rejet = new File(applicationDAO.getFichierRejetImportClient());
            writer = new BufferedWriter(new FileWriter(rejet));
        } catch (Exception exception) {
            // Exception
            exception.printStackTrace();
        } 

        for (TicketCustomerDTO dto : listData) {
            nbTicket++;
            Tickets ticket = null ;
            //TODO si on a besoin d'accélérer, on pourra n'extraire que l'info qui nous intéresse plutôt que l'objet entier
            //Soit l'id qui correspond au num de ticket
            //et l'id qui correspond au num de client
            if(dto.getTicketNumber() != null) {
                ticket = ticketsDAO.findFirstByTicketId(Long.valueOf(dto.getTicketNumber())).orElse(null);
            }

            if (ticket != null) {
                if (ticket.getCustomerSearchKey() != null) {
                    // on fait rien ??
                    if (!ticket.getCustomerSearchKey().equals(dto.getCustomerSearchKey())) {
                        // client renseigné sur le ticket mais différent du fichier d'import
                    }
                } else {
                    Customers customers = customersDAO.findFirstBySearchKey(dto.getCustomerSearchKey()).orElse(null);
                    if (customers != null) {
                        ticketsService.updateTicketExtended(ticket.getId(),customers.getId());
                        // Enrichissement des données
                        nbOK++;
                    } else {
                        // Client inexistant
                        if( writer != null) {
                            try {
                                writer.write(String.format("%s;%s\n", dto.getTicketNumber(), dto.getCustomerSearchKey()));
                            } catch (IOException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                // Ticket inexistant
                if( writer != null) {
                    try {
                        writer.write(String.format("%s;%s\n", dto.getTicketNumber(), dto.getCustomerSearchKey()));
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

        if( writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return String.format(" %s/%s lignes du fichier ont été traitées - ", String.valueOf(nbOK), String.valueOf(nbTicket));
    }

    private void saveCustomers(List<CustomersDetailDTO> listCustomers) {
        HashMap<String, Customers> newClient = new HashMap<String, Customers>();
        HashMap<String, Customers> updClient = new HashMap<String, Customers>();
        List<Customers> clientList;
        if (listCustomers != null) {
            for (CustomersDetailDTO customersDto : listCustomers) {
                boolean existingEntry = true;
                Customers customer = null;

                if (updClient.size() > 0) {
                    customer = updClient.get(customersDto.getSearchKey());
                }

                if (customer == null && newClient.size() > 0) {
                    customer = newClient.get(customersDto.getSearchKey());
                    if (customer != null) {
                        existingEntry = false;
                    }

                }

                if (customer == null) {

                    customer = customersDAO.findFirstBySearchKey(customersDto.getSearchKey()).orElse(null);

                    if (customer == null) {
                        customer = customersDto.getId() != null ? customersDAO.findByPrimaryKey(customersDto.getId()) : null;
                    }
                }

                if (customer == null) {
                    existingEntry = false;
                    customer = new Customers();
                    customer.setId(customersDto.getId());
                    if (customer.getId() == null) {
                        customer.setId(customersService.createId(customersDto));
                    }
                }

                if (customersService.loadFromDTO(customersDto , customer)) {
                    // EDU - peut etre vérifier ici les objets Location et DiscountProfil
                    if (customer.getDiscountProfil() != null && customer.getDiscountProfil().getName() == null) {
                        customer.setDiscountProfil(
                                discountProfilDAO.findById(Long.valueOf(customersDto.getDiscountProfile().getId())).orElse(null));
                    }
                    if (customer.getLocation() != null && customer.getLocation().getName() == null) {
                        customer.setLocation(locationDAO.findById(customersDto.getLocation().getId()).orElse(null));
                    }
                    customer.setDateUpdate(new Date());
                    if (existingEntry) {
                        updClient.put(customersDto.getSearchKey(), customer);

                    } else {
                        if (customer.getDateCreation() == null) {
                            customer.setDateCreation(new Date());
                        }
                        newClient.put(customersDto.getSearchKey(), customer);
                    }
                }

            }

            clientList = new ArrayList<Customers>(newClient.values());
            if (clientList.size() > 0) {
                customersDAO.create(clientList);
            }
            clientList = new ArrayList<Customers>(updClient.values());
            if (clientList.size() > 0) {
                customersDAO.update(clientList);
            }
        }
    }

    private boolean testLigneAModifier(HashMap<String, Integer> columns, Row row) {
        //soit on n'a pas la colonne Modification dans le fichier et alors on prend tout
        //soit on l'a et dans note ligne elle est remplie avec x ou X
        try {
            return !columns.containsKey("Modification") || (
                    row.getPhysicalNumberOfCells()> columns.get("Modification") && 
                    row.getCell(columns.get("Modification").intValue()).toString().toUpperCase().equals("X")) ;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //EDU 2021 06
    //la seule condition désormais pour accepter une entrée est que les deux colonnes soient connues
    //et que la colonne CodeClient ne soit pas vide
    private boolean testValiditeParentEnfant(HashMap<String, Integer> columns, Row row) {
        try {
            return (columns.containsKey("CodeClient") && columns.containsKey("Parent") 
                    && row.getPhysicalNumberOfCells()> columns.get("Parent")
                    && row.getPhysicalNumberOfCells()> columns.get("CodeClient")
                    && !row.getCell(columns.get("CodeClient").intValue()).toString().trim().isEmpty()) ;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
