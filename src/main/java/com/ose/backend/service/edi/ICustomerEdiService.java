package com.ose.backend.service.edi;

import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;

@Service
public interface ICustomerEdiService {

    String importCustomerSheet(Sheet sheet, String retour, String locationId);

    String importCustomerDeduplicationSheet(Sheet sheet, String retour, String locationId);

    String importCustomerTicket(Sheet sheet, String retour);

}
