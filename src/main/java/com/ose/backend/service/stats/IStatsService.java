package com.ose.backend.service.stats;

import java.util.ArrayList;
import java.util.List;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;

import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockDiaryStatsDTO;


public interface IStatsService {

    ArrayList<StockDiaryStatsDTO> getStocksList(List<LocationsInfoDTO> locationList, List<ProductsResultDTO> productsList, List<POSCategoriesDTO> categoryList, String from, String to, Boolean detailed, String group);

    Object getAggregates();

    

}
