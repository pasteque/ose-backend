package com.ose.backend.service.stats.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.products.POSCategoriesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.stocks.IStockCurrentDAO;
import com.ose.backend.dao.stocks.IStockDiaryDAO;
import com.ose.backend.dto.products.ProductsResultDTO;
import com.ose.backend.dto.stocks.StockCurrentInfoDTO;
import com.ose.backend.dto.stocks.StockDiaryStatsDTO;
import com.ose.backend.dto.utils.EnumDTO;
import com.ose.backend.service.stats.IStatsService;

@Service
@Cacheable(value = false)
public class StatsService implements IStatsService {

    @Autowired
    private IStockCurrentDAO stockCurrentDAO;
    @Autowired
    private IStockDiaryDAO stockDiaryDAO;
    
    public static enum GroupType {
        YEAR(1 , "Annuel"), MONTH(2 , "Mensuel"), WEEK(3 , "Hebdomadaire"), DAY(4 , "Quotidien"), HOUR(5 , "Horaire");
        int value;
        String french;
        private GroupType(int value ,String french) {
            this.value = value;
            this.french = french;
        }
        public int getValue() {
            return value;
        }

        public String getFrench() {
            return french;
        }

        /*
        public GroupType valueOf(String value) {
            return GroupType.valueOf(GroupType.class, value);
        }
         */
    }

    @Override
    public ArrayList<StockDiaryStatsDTO> getStocksList(List<LocationsInfoDTO> locationList, List<ProductsResultDTO> productsList, List<POSCategoriesDTO> categoryList, String from, String to,
            Boolean detailed, String group) {
        ArrayList<StockDiaryStatsDTO> retour = new ArrayList<StockDiaryStatsDTO>();
        StockDiaryStatsDTO tampon = null , current = null;
        List<StockDiaryStatsDTO> tmpListProdLoc;
        List<StockCurrentInfoDTO> stockCurrentList;
        StockCurrentInfoDTO currentElement;
        String locationsIdList = null;
        String productsIdList = null;
        String locationsCatList = null;
        String productsCatList = null;
        int index = 0 ;
        boolean change = true;
        String previous = null;
        String previousLocId = null;
        if(productsList != null && !productsList.isEmpty()) {
            for(ProductsResultDTO product : productsList){
                String productsId = product == null ? null : product.getId();
                if(productsId != null ) {
                    productsIdList = productsIdList == null ? "'"+productsId+"'" : productsIdList + ",'"+productsId+"'";
                }
            }
        }
        if(categoryList != null && !categoryList.isEmpty()) {
            for(POSCategoriesDTO category : categoryList){
                String categoryId = category == null ? null : category.getId();
                if(categoryId != null ) {
                    productsCatList = productsCatList == null ? "'"+category.getId()+"'" : productsCatList + ",'"+category.getId()+"'";
                }
            }
        }

        if(locationList != null && !locationList.isEmpty()) {
            for(LocationsInfoDTO location : locationList){
                String locationId = location == null ? null : location.getId();
                if(locationId != null ) {
                    locationsIdList = locationsIdList == null ? "'"+locationId+"'" : locationsIdList + ",'"+locationId+"'";
                } else {
                    locationsCatList = locationsCatList == null ? "'"+location.getCategory()+"'" : locationsCatList + ",'"+location.getCategory()+"'";
                }
            }
        }

        stockCurrentList = (List<StockCurrentInfoDTO>) stockCurrentDAO.findDTOListByCriteria(productsIdList, locationsIdList, productsCatList, locationsCatList, StockCurrentInfoDTO.class).getResult(StockCurrentInfoDTO.class);
        tmpListProdLoc = (List<StockDiaryStatsDTO>) stockDiaryDAO.findByCriteriaAggr(locationsIdList, productsIdList, productsCatList, locationsCatList, null, null, from, group , StockDiaryStatsDTO.class).getResult(StockDiaryStatsDTO.class);
        //On fait le tour des retours, on agrège tous ceux qui dépassent dateTo avec un timeStamp à 0
        index = retour.size();
        tampon = null;
        //EDU - La liste de StockCurrent est exaustive et contient tous les produits ayant eu du stock à un 
        //moment ou à un autre dans un point de stockage donné
        //La construction est telle que il sont triés par lieu puis par article
        //
        //Pour les tmpListProdLoc on n'aura pas les articles n'ayant pas eu de mouvement depuis from
        //ils sont triés selon les mêmes principaux critères
        change = true;
        previous = null;
        previousLocId = null;
        for(StockDiaryStatsDTO ligne : tmpListProdLoc ) {
            //initialiser les infos sur le précédent élément
            if(previous == null) {
                previous = ligne.getProductId() ;
                previousLocId = ligne.getLocationId() ;
            }
            //vérifier si on n'a pas changé de (lieu , produit)
            if(tampon != null && 
                    (! tampon.getProductId().equals(ligne.getProductId()) || ! tampon.getLocationId().equals(ligne.getLocationId()))) {
                //le cas échéant stocker le tampon actuel
                retour.add(index, tampon);
                tampon = null;
                index = retour.size();
                previous = ligne.getProductId() ;
                previousLocId = ligne.getLocationId() ;
                change = true;
            } else {
                if(! previous.equals(ligne.getProductId()) || ! previousLocId.equals(ligne.getLocationId())) {
                    index = retour.size();
                    previous = ligne.getProductId() ;
                    previousLocId = ligne.getLocationId() ;
                    change = true;
                }
            }

            //Si on a changé et qu'il nous reste une reserve de stocks 'nus'
            //On insère ceux qui doivent l'être
            if(change && !stockCurrentList.isEmpty()) {
                change = false;
                currentElement = stockCurrentList.get(0);
                stockCurrentList.remove(0);
                while( !change && (!ligne.getLocationId().equals(currentElement.getLocations().getId())
                        || !(ligne.getProductId()).equals(currentElement.getProductsId()))) {
                    current = new StockDiaryStatsDTO();
                    current.setCurrentStock(currentElement);
                    current.setTimeStamp(new Date(0));

                    retour.add(index,current);
                    index++;
                    if(stockCurrentList.isEmpty()) {

                        change = true ;
                    } else {
                        currentElement = stockCurrentList.get(0);
                        stockCurrentList.remove(0);
                    }
                }
            }

            if(to == null || (ligne.getTimeStamp().getTime() < Long.parseLong(to))) {
                retour.add(ligne);
            } else {
                if(tampon == null) {
                    tampon = ligne;
                    tampon.setTimeStamp(new Date(0));
                } else {
                    tampon.add(ligne);
                }
            }

        }
        if(tampon != null) {
            retour.add(index, tampon);
            tampon = null;
        }
        if(! stockCurrentList.isEmpty()) {
            for(StockCurrentInfoDTO element : stockCurrentList) {
                current = new StockDiaryStatsDTO();
                current.setCurrentStock(element);
                current.setTimeStamp(new Date(0));
                retour.add(current);
            }
        }

        return retour;
    }

    @Override
    public Object getAggregates() {
        List<EnumDTO> reasons = new ArrayList<EnumDTO>();
        for( GroupType gt : GroupType.values()) {
            EnumDTO dto = new EnumDTO();
            dto.setId(gt.name());
            dto.setLabel(gt.getFrench());
            reasons.add(dto);
        }
        return reasons;
    }

}
