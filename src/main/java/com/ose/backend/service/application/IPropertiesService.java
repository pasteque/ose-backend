package com.ose.backend.service.application;

import com.ose.backend.dto.application.SupervisionIntervalDTO;

public interface IPropertiesService {

    Object getDetailedExtractLimit();

    Object getStockHistoryLimit();

    SupervisionIntervalDTO getSupervisionInterval();

    Object getProductRenflouementInterval();

}
