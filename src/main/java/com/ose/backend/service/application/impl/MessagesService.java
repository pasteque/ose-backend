package com.ose.backend.service.application.impl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.application.IPosMessageTypeApiDAO;
import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.dao.products.IProductsApiDAO;
import org.pasteque.api.dao.security.IPeopleApiDAO;
import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.model.application.PosMessageType;
import org.pasteque.api.model.application.PosMessages;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;
import org.pasteque.api.specs.SearchCriteria;
import org.pasteque.api.specs.SearchOperation;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.application.IPosMessagesDAO;
import com.ose.backend.dto.application.MessagesDTO;
import com.ose.backend.dto.application.MessagesInfoDTO;
import com.ose.backend.service.application.IMessagesService;
import com.ose.backend.specs.application.PosMessagesSpecification;

@Service
public class MessagesService implements IMessagesService {

    @Autowired
    IPosMessagesDAO posMessagesDAO;
    @Autowired
    IPeopleApiDAO peopleDAO;
    @Autowired
    IProductsApiDAO productsDAO;
    @Autowired
    ILocationsApiDAO locationsDAO;
    @Autowired
    IPosMessageTypeApiDAO posMessageTypeDAO;

    @Override
    public Object updateMessages(List<MessagesDTO> messages, String locationId, String productId, String userId, String typeId) {

        PosMessageType type = typeId == null ? null : posMessageTypeDAO.findById(typeId).orElse(null);
        People people = userId == null ? null : peopleDAO.findById(userId).orElse(null);
        Products product = productId == null ? null : productsDAO.findById(productId).orElse(null);
        Locations location = locationId == null ? null : locationsDAO.findById(locationId).orElse(null);
        Locations serverLocation = locationsDAO.findTopByServerDefaultTrue();
        
        Date currentDate = new Date();

        
        List<PosMessages> origine = findByCriteria(locationId, productId, userId , typeId , null,null,null,null);
        ArrayList<PosMessages> nouveaux = new ArrayList<PosMessages>();

        for(MessagesDTO message : messages) {
            PosMessages tmp = new PosMessages();
            tmp.setContent(message.getContent());
            tmp.setLocation(location);
            tmp.setProduct(product);
            tmp.setPeople(people);
            tmp.setType(type);
            if(!origine.remove(tmp)) {
                tmp.setStartDate(currentDate);
                //tmp.createId();
                nouveaux.add(tmp);
            }
        }

        for(PosMessages tmp : origine) {
            //EDU 2021 11 : On ne supprime plus de messages
            if(tmp.getEndDate() == null ) {
                tmp.setEndDate(currentDate);
                posMessagesDAO.save(tmp);
                if(product != null) {
                    product.setLastUpdate(currentDate);
                    product.getMessages().remove(tmp);
                    productsDAO.save(product);
                }
            }
        }
        
        for(PosMessages tmp : nouveaux) {
            tmp.setId(posMessagesDAO.getNextId(serverLocation == null ? null : serverLocation.getId()));

            posMessagesDAO.save(tmp);
            //TODO idem pour les autres people et locations ?
            if(product != null) {
                product.setLastUpdate(currentDate);
                product.getMessages().add(tmp);
                productsDAO.save(product);
            }
        }

        return "Ok";
    }

    private List<PosMessages> findByCriteria(String locationId,
            String productId, String userId, String typeId, String message,
            Boolean activeOnly, String from , String to) {

        PosMessagesSpecification specs = new PosMessagesSpecification();
        specs.add(new SearchCriteria(PosMessages.Fields.TYPE, typeId, SearchOperation.EQUAL));
        specs.add(new SearchCriteria(PosMessages.Fields.LOCATIONS, locationId, SearchOperation.EQUAL));
        specs.add(new SearchCriteria(PosMessages.Fields.PRODUCTS, productId, SearchOperation.EQUAL));
        specs.add(new SearchCriteria(PosMessages.Fields.PEOPLE, userId, SearchOperation.EQUAL));
        specs.add(new SearchCriteria(PosMessages.Fields.CONTENT, message, SearchOperation.EQUAL));

        if(activeOnly != null) {
            SearchOperation op = activeOnly ? SearchOperation.IS_NULL : SearchOperation.IS_NOT_NULL ;
            specs.add(new SearchCriteria(PosMessages.Fields.END_DATE, null, op));
        }
        
        if (from != null) {
            Date dFrom = new Date(Long.parseLong(from));
            specs.add(new SearchCriteria(PosMessages.Fields.START_DATE, DateHelper.formatSQLTimeStamp(dFrom), SearchOperation.GREATER_THAN_EQUAL));
        }
        
        if (to != null) {
            Date dTo = new Date(Long.parseLong(to));
            specs.add(new SearchCriteria(PosMessages.Fields.START_DATE, DateHelper.formatSQLTimeStamp(dTo), SearchOperation.LESS_THAN_EQUAL));
        }
        
        return posMessagesDAO.findAll(specs);
    }

    @Override
    public Object getMessages(String locationId, String productId, String userId, String typeId, Boolean activeOnly,String from,String to) {
        return getAdapter(findByCriteria(locationId, productId, userId , typeId ,null, activeOnly,from,to),MessagesDTO.class , false);
    }

    @Override
    public Object initMessage(String locationId, String productId, String userId, String typeId, String content) {
        POSMessagesDTO retour = new POSMessagesDTO();  
        retour.setContent(content);
        retour.setLocationId(locationId);
        retour.setPeopleId(userId);
        retour.setProductId(productId);
        retour.setType(typeId);        
        return retour;
    }

    @Override
    public Object createRenflouement(MessagesDTO messagesDTO) {
        PosMessages posMsg = null;
        //Vérifier si le produit à cet emplacement est déja signalé en rupture
        List<PosMessages> posMessages = findByCriteria(null, messagesDTO.getProductId(), null, PosMessageType.Values.RENFLOUEMENT,messagesDTO.getContent(), true,null,null);
        PosMessageType type = posMessageTypeDAO.findById(PosMessageType.Values.RENFLOUEMENT).orElse(null);
        if (posMessages.size()==0 && type != null)
        {
            Locations serverLocation = locationsDAO.findTopByServerDefaultTrue();
            Products product = productsDAO.findById(messagesDTO.getProductId()).orElse(null);
            List<PosMessages> msgList = product.getMessages();
            posMsg =  new PosMessages();
            posMsg.setProduct(product);
            posMsg.setLocation(serverLocation);
            posMsg.setStartDate(new Date());
            product.setLastUpdate(new Date());
            posMsg.setContent(messagesDTO.getContent());
            posMsg.setType(type);
            posMsg.setId(posMessagesDAO.getNextId(serverLocation == null ? null : serverLocation.getId()));
            msgList.add(posMsg);
            product.setMessages(msgList);
            productsDAO.save(product);

        }
        return posMessages.size() > 0? posMessages.get(0).getId() : posMsg!=null ? posMsg.getId():null;
    }

    @Override
    public Object updateRenflouement(String userName, MessagesDTO messagesDTO) {
        People currentUser = peopleDAO.findByUsername(userName);
        PosMessages posMessages = posMessagesDAO.findById(messagesDTO.getId()).orElse(null);
        if (currentUser !=null && posMessages!=null && posMessages.getPeople() !=null 
                && !currentUser.getId().equals(posMessages.getPeople().getId())) 
        {// seul l'utilisateur à qui est attribué le renflouement peut le terminer.
            return "KO";
        }else {

            if (posMessages.getPeople()==null) {
                posMessages.setPeople(peopleDAO.findByUsername(userName));
            }else {
                posMessages.setEndDate(new Date());
            }
            posMessagesDAO.saveAndFlush(posMessages);
            return "OK";
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(PosMessages message , Class<T> selector, boolean filtered) {
        if (selector.equals(MessagesDTO.class)) {
            MessagesDTO dto = new MessagesDTO();
            dto.setId(message.getId());
            dto.setContent(message.getContent());
            dto.setStartDate(message.getStartDate());
            dto.setEndDate(message.getEndDate());
            dto.setType(message.getType().getType());
            dto.setPeopleId(message.getPeopleId());
            if (message.getPeople() != null) {
                dto.setPeopleName(message.getPeople().getUsername());
            }
            if (message.getProduct() != null) {
                dto.setProductId(message.getProduct().getId());
                dto.setProductName(message.getProduct().getName());
            }
            if (message.getLocation() != null) {
                dto.setLocationId(message.getLocation().getId());
            }
            return (T) dto;
        } else if (selector.equals(MessagesInfoDTO.class)) {
            MessagesInfoDTO dto = new MessagesInfoDTO();
            dto.setType(message.getType().getDescription());
            dto.setContent(message.getContent());
            return (T) dto;
        }
        return null;
    }
    
    @Override
    public <T> List<T> getAdapter( List<PosMessages> liste , Class<T> selector , Boolean filtered) {
        List<T> listDTO = new ArrayList<>();
        for (PosMessages adaptable : liste) {
            listDTO.add((T) getAdapter(adaptable , selector , filtered));
        }
        return listDTO;
    }
    
    @Override
    public boolean loadFromDTO(PosMessages posMessages , POSMessagesDTO source) {
        boolean retour = false;
        if (POSMessagesDTO.class.isInstance(source)) {
            POSMessagesDTO dto = (POSMessagesDTO) source;
            
            if(dto.getContent().equals(posMessages.getContent())) {
                retour = true;
                posMessages.setContent(dto.getContent());
            }
            if(dto.getId().equals(posMessages.getId())) {
                retour = true;
                posMessages.setId(dto.getId());
            }
            if(dto.getType().equals(posMessages.getTypeId())) {
                retour = true;
                posMessages.setType(posMessageTypeDAO.findById(dto.getId()).orElse(null));
            }
            if(dto.getLocationId().equals(posMessages.getLocationId())) {
                retour = true;
                posMessages.setLocation(locationsDAO.findById(dto.getLocationId()).orElse(null));
            }
            if(dto.getPeopleId().equals(posMessages.getPeopleId())) {
                retour = true;
                posMessages.setPeople(peopleDAO.findById(dto.getPeopleId()).orElse(null));
            }
            if(dto.getProductId().equals(posMessages.getProductId())) {
                retour = true;
                posMessages.setProduct(productsDAO.findById(dto.getProductId()).orElse(null));
            }
            if(dto.getStartDate().equals(posMessages.getStartDate())) {
                retour = true;
                posMessages.setStartDate(dto.getStartDate());
            }
            if(dto.getEndDate().equals(posMessages.getEndDate())) {
                retour = true;
                posMessages.setEndDate(dto.getEndDate());
            }
        }
        return retour;
    }

}
