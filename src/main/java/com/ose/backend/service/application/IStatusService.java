package com.ose.backend.service.application;

import java.util.List;


public interface IStatusService {
	
	Object getGeneralStatus();
	List<Object> getCashesStatus(String locationId);

}
