package com.ose.backend.service.application.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.application.IApplicationDAO;
import com.ose.backend.service.application.IStatusService;


@Service
public class StatusService implements IStatusService {

    @Autowired
    private IApplicationDAO applicationDAO;

    @Override
    public Object getGeneralStatus() {
        return applicationDAO.getGeneralStatus();
    }

    @Override
    public List<Object> getCashesStatus(String locationId) {
        return applicationDAO.getCashesStatus(locationId);
    }

}
