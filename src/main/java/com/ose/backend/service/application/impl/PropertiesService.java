package com.ose.backend.service.application.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ose.backend.dao.application.IApplicationDAO;
import com.ose.backend.dto.application.SupervisionIntervalDTO;
import com.ose.backend.service.application.IPropertiesService;


@Service
public class PropertiesService implements IPropertiesService {


    @Autowired
    private IApplicationDAO applicationDAO;

    @Override
    public Object getStockHistoryLimit() {
        return applicationDAO.getStockHistoryLimit();
    }

    @Override
    public Object getDetailedExtractLimit() {
        return applicationDAO.getDetailedExtractLimit();
    }

    @Override
    public SupervisionIntervalDTO getSupervisionInterval() {
        long order = applicationDAO.getSupervisionOrdersInterval();
        long details = applicationDAO.getSupervisionDetailsInterval();
        return new SupervisionIntervalDTO(order, details); 
    }

    @Override
    public Object getProductRenflouementInterval() {
        return applicationDAO.getProductRenflouementInterval();
    }

}
