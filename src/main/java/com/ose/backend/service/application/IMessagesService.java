package com.ose.backend.service.application;

import java.util.List;

import org.pasteque.api.dto.application.POSMessagesDTO;
import org.pasteque.api.model.application.PosMessages;

import com.ose.backend.dto.application.MessagesDTO;

public interface IMessagesService {

    Object updateMessages(List<MessagesDTO> messages, String locationId, String productId, String userId, String typeId);

    Object getMessages(String locationId, String productId, String userId, String typeId, Boolean activeOnly, String from , String to);

    Object initMessage(String locationId, String productId, String userId, String typeId, String content);

    Object createRenflouement(MessagesDTO messagesDTO);

    Object updateRenflouement(String name, MessagesDTO messagesDTO);

    <T> T getAdapter(PosMessages message, Class<T> selector, boolean filtered);

    <T> List<T> getAdapter(List<PosMessages> liste, Class<T> selector,
            Boolean filtered);

    boolean loadFromDTO(PosMessages posMessages, POSMessagesDTO source);
    

}
