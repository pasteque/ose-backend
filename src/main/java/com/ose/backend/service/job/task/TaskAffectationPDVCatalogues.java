package com.ose.backend.service.job.task;

import java.util.Map;

import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.products.ITariffAreasServiceOSE;

@Component
public class TaskAffectationPDVCatalogues extends Task {

    @Autowired
    private ITariffAreasServiceOSE tariffAreasService;

    @Override
    public JobLog doIt(JobLog retour) {
        TariffAreasLocationsDTO dto = null;
        String action = null;
        ObjectMapper mapper = new ObjectMapper();
        String message = job.getMessage();
        retour.setLevel(JobLog.Level.OK);        
        try {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(message, Map.class);
            action = mapper.readValue(map.get(Job.Parametres.ACTION), String.class);
            dto = mapper.readValue(map.get(Job.Parametres.LIAISON), TariffAreasLocationsDTO.class);
            if(Job.Actions.DELETE.equals(action)) {
                tariffAreasService.deleteLink(dto);
                retour.setMessage("Ok - Suppression Lien : " + dto.getId());
            } else {
                if(Job.Actions.INSERT.equals(action) || Job.Actions.UPDATE.equals(action)) {
                    tariffAreasService.saveLink(dto);
                    retour.setMessage("Ok - Maj Lien : " + dto.getId());
                }
            }
        } catch (Exception e) {
            retour.setLevel(JobLog.Level.ERREUR);
            retour.setMessage("Erreur - Message : " + e.getMessage());
        }

        return retour;
    }
}
