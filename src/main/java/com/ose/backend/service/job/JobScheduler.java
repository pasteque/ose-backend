package com.ose.backend.service.job;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.ose.backend.dao.job.IJobDAO;
import com.ose.backend.service.job.schedulable.SchedulableJob;

@Component
public class JobScheduler {

    @Autowired 
    private IJobsService jobService;
    
    @Autowired 
    private IJobDAO jobDAO;
    
    private static String NOCRON = "-";


    private static JobScheduler JobSchedulerinstance = new JobScheduler();



    private ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
    private Map<Object, ScheduledFuture<?>> scheduledTasks = new IdentityHashMap<>();
    private boolean isRunning = false;

    private Map<String, String> jobDefinition;

    @PostConstruct
    private void init() {
        this.taskScheduler.setPoolSize(10);
        this.taskScheduler.initialize();
        if(jobDAO != null) {
            this.jobDefinition = jobDAO.getJobsDefinition();
            start();
        }
    }



    public static final JobScheduler getInstance() {
        return JobSchedulerinstance;
    }


    public void start() {
        if (!isRunning && jobDefinition != null) {
            // 1. Récupération de la liste des interfaces 'a lancer' AUTOMATIQUE

            try {
                //On choppe tout ce qu'il faut lancer en auto avec la récurrence



                // 2. Boucle sur l'ensemble des services du fichier de config
                for (Map.Entry<String , String> mapentry : jobDefinition.entrySet()) {

                    
                    SchedulableJob runnable = jobService.getJob(mapentry.getKey());

                    String cron = mapentry.getValue();
                    if(runnable != null && runnable.isActive() && !NOCRON.equals(cron)) {
                        CronTrigger cronTrigger = new CronTrigger(cron);
                        ScheduledFuture<?> future = taskScheduler.schedule(runnable, cronTrigger);

                        scheduledTasks.put(runnable.hashCode(), future);
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            isRunning = true;
        } else {
            // Exception a gérer + tard
        }
    }

    public void stop() {
        if (isRunning) {
            for (Map.Entry<Object, ScheduledFuture<?>> entry : scheduledTasks.entrySet()) {
                entry.getValue().cancel(true);
            }
            scheduledTasks.clear();
            isRunning = false;
        }
    }

    public void restart() {
        stop();
        start();
    }

}
