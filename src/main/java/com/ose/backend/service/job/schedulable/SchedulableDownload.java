package com.ose.backend.service.job.schedulable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ose.backend.dao.job.IJobLogDAO;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.job.task.TaskDownload;

@Component
public class SchedulableDownload extends SchedulableJob {
    
    @Autowired
    IJobLogDAO jobLogDAO;
    @Autowired
    private TaskDownload taskDownload;
    @Override
    public void run() {
        
        JobLog log = taskDownload.execute(null);

        if(log != null) {
            jobLogDAO.save(log);
        }
    }

}
