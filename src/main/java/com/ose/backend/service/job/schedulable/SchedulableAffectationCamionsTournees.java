package com.ose.backend.service.job.schedulable;

import org.springframework.stereotype.Component;

import com.ose.backend.model.job.Job;

@Component
public class SchedulableAffectationCamionsTournees extends SchedulableJob {
    
    @Override
    public void run() {
        String type = Job.Type.AFFECTATION_CAMIONS_TOURNEES;
        executeTaches(type);
    }

}
