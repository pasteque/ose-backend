package com.ose.backend.service.job.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ose.backend.model.job.Job;
import com.ose.backend.service.job.IExecutable;

@Component
public class TaskFactory {
    
    @Autowired 
    private TaskDummy taskDummy;
    @Autowired 
    private TaskAffectationCamionsTournees taskAffectationCamionsTournees;
    @Autowired
    private TaskCommande taskCommande;
    @Autowired
    private TaskUpload taskUpload;
    @Autowired
    private TaskDownload taskDownload;
    @Autowired
    private TaskUtilisateur taskUtilisateur;
    @Autowired
    private TaskAffectationPDVCatalogues taskAffectationPDVCatalogues;
    @Autowired
    private TaskStockType taskStockType;
    @Autowired
    private Task task;

    public IExecutable getInstance(Job job) {
        switch (job.getType()) {
        case Job.Type.DUMMY:
            return taskDummy;
        case Job.Type.AFFECTATION_CAMIONS_TOURNEES:
            return taskAffectationCamionsTournees;
        case Job.Type.COMMANDE:
            return taskCommande;
        case Job.Type.UPLOAD:
            return taskUpload;
        case Job.Type.DOWNLOAD:
            return taskDownload;
        case Job.Type.UTILISATEUR:
            return taskUtilisateur;
        case Job.Type.AFFECTATION_PDV_CATALOGUE:
            return taskAffectationPDVCatalogues;
        case Job.Type.STOCKTYPE:
            return taskStockType;
        default:
            return task;
        }
    }

}
