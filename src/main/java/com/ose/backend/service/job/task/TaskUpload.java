package com.ose.backend.service.job.task;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.ose.backend.controller.jobs.JobsController;
import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;

@Component
public class TaskUpload extends Task {

    @Autowired
    private ILocationsApiDAO locationDAO;

    public TaskUpload() {
        type = Job.Type.UPLOAD;
    }

    @Override
    public JobLog doIt(JobLog retour) {
        retour.setJobId(type + "-" + DateHelper.formatSQLTimeStamp(retour.getDate()));
        // On récupère la liste des tâches prêtes à être envoyées
        Map<String, Object> map = new HashMap<String, Object>();
        Locations serverLocation = locationDAO.findTopByServerDefaultTrue();
        String location = serverLocation == null ? "0" : serverLocation.getId();

        map.put("sens", Job.Sens.UPLOAD);
        map.put("etat", Job.Etat.PRET);

        List<Job> listedi = jobDAO.findByCriteria(map);
        // Pour chaque tâche on la pousse sur le serveur distant

        for (Job edi : listedi) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                Locations destLocation = edi.getDestination() == null ? null : locationDAO.findById(edi.getDestination()).orElse(null);
                String resourceUrl = destLocation == null ? null : destLocation.getUrl();
                ResponseEntity<String> response = null;
                // Faire les modifs en locales nécessaires sur les tâches uploadées
                // Date upload

                edi.setDenvoi(new Date());
                JobDTO dto = edi.getAdapter(JobDTO.class);
                edi.setEtat(Job.Etat.EN_COURS);
                jobService.save(edi);

                // faire les modifs sur la tâches envoyée ( destinataire - expéditeur )
                dto.setDestinataire(location);
                dto.setSens(Job.Sens.INTERNE);

                response = restTemplate.postForEntity(resourceUrl + JobsController.BASE_URL + "/save", dto, String.class);

                // TODO consommation service distant
                //System.out.println(response.getStatusCode().name());
                //System.out.println(response.getBody());
                if (HttpStatus.OK.equals(response.getStatusCode())) {

                    // Date reception une fois ACQ
                    // Status en cours si OK
                } else {
                    // Mettre le message pour les logs à jour en fonction
                    edi.setEtat(Job.Etat.PRET);
                    jobService.save(edi);
                }
            } catch (Exception e) {
                retour.setMessage(e.toString());
                edi.setEtat(Job.Etat.PRET);
                jobService.save(edi);
            }

        }
        return retour;
    }

}
