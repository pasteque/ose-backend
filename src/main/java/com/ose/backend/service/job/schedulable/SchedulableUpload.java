package com.ose.backend.service.job.schedulable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ose.backend.dao.job.IJobLogDAO;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.job.task.TaskUpload;

@Component
public class SchedulableUpload extends SchedulableJob {
    
    @Autowired
    IJobLogDAO jobLogDAO;
    @Autowired
    private TaskUpload taskUpload;
    @Override
    public void run() {        
        JobLog log = taskUpload.execute(null);

        if(log != null) {
            jobLogDAO.save(log);
        }
    }


}
