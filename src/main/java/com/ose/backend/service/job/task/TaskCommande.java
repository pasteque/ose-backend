package com.ose.backend.service.job.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.OrderDetailDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobHelper;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.orders.IOrdersService;

@Component
public class TaskCommande extends Task {

    @Autowired
    private IOrdersService ordersService;
    @Autowired
    private ILocationsApiDAO locationsDAO;


    @Override
    public JobLog doIt(JobLog retour) {
        OrderDTO orderDto = null;
        List<PaletteDTO> paletteDtos = null;
        ObjectMapper mapper = new ObjectMapper();
        String message = job.getMessage();
        retour.setLevel(JobLog.Level.OK);

        try {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(message, Map.class);
            orderDto = mapper.readValue(map.get(Job.Parametres.COMMANDE), OrderDTO.class);
            paletteDtos = mapper.readValue(map.get(Job.Parametres.PALETTES), new TypeReference<List<PaletteDTO>>() {
            });

            if (orderDto != null) {
                List<OrderDetailDTO> ordersDetailsDto = orderDto.getOrderDetails();
                orderDto.setOrderDetails(new ArrayList<OrderDetailDTO>());

                orderDto = ordersService.saveOrder(orderDto);
                if(orderDto != null) {
                    ordersService.saveOrderDetails(ordersDetailsDto);
                }
            }

            if(paletteDtos != null) {
                for (PaletteDTO paletteDto : paletteDtos) {
                    retour.setMessage("Ok - Palettes ");
                    List<PaletteDetailDTO> palettesDetailsDto = paletteDto.getPaletteDetails();
                    paletteDto.setPaletteDetails(new ArrayList<PaletteDetailDTO>());
                    if (paletteDto != null) {
                        retour.setMessage(retour.getMessage() + " - " + paletteDto.getName());
                        Object palRetour = ordersService.savePalette(paletteDto);
                        if(palRetour != null) {
                            ordersService.savePaletteDetails(palettesDetailsDto);
                        }
                    }
                }
            }

            if (orderDto != null) {
                retour.setMessage("Ok - Commande : " + orderDto.getId());
            }

        } catch (Exception e) {
            retour.setLevel(JobLog.Level.ERREUR);
            retour.setMessage("Erreur - Message : " + e.getMessage());
        }

        //EDU - 2020 02 
        /**
         * Gestions de flux 'potentiellement tirés'
         * Les détails des commandes circulent entre le site approvisionneur et le site approvisionné en passant par un serveur central
         * 1/  L'approvisionné ( resp approvisionneur) envoi l'info au central - Déclencheur : evenêment commande
         * 2/  Le central envoie l'info ( ou la met à disposition de - cas IP hors réseau / inconnue / dynamique ) à l'approvisionneur (resp L'approvisionné)
         * 
         * Dans ce cas 2 le déclencheur est l'arrivée de l'info au central 
         * Celle-ci ne peut se faire que via un job ( postulat - en théorie ça pourrait aussi être l'injection d'un fichier excel en EDI ou alors une remonté SQL ) 
         *  - Le cas de l'injection en EDI est écartée -> elle n'est pas sensée avoir lieu sur le back central
         *  - Le cas de l'injection de données directement en base aussi : ce n'est pas interceptable par le serveur / ce n'est pas une bonne pratique 
         *  
         *  Etape 1 : déterminer si le job se déroule sur le back central -> création
         *  Etape 2 : Déterminer l'interlocuteur à avertir -> destinataire = opposé du destinataire du job en cours. On consulte la commande
         *  Etape 3 : déterminer si cet interlocureur est joignable -> sens du job OUI : UPLOAD non : READY_TO_DOWNLOAD
         *  Etape 4 : On le valide
         */
        if(locationsDAO.findTopByServerDefaultTrue() == null) {
            /**
             * Par convention si on est dans le serveur de la log on aura placé le default sur 0 -> Id du siège
             * Donc si nous sommes là c'est que nous sommes bien sur le serveur central
             * Step 1 OK
             */
            String sourceJobCommande = job.getDestination() ;
            String interlocuteur = null;
            if(orderDto == null && paletteDtos != null && paletteDtos.size() > 0) {
                /**
                 * EDU - On prend l'info sur la première palette de la liste
                 */
                orderDto = ordersService.getOrder(paletteDtos.get(0).getOrderId());
            }
            if(orderDto != null) {
                interlocuteur = sourceJobCommande.equals(orderDto.getLocationFromId()) ? orderDto.getLocationToId() : orderDto.getLocationFromId()  ;
            }

            if(interlocuteur != null) {
                /**
                 * Nous savons à qui nous adresser
                 * Step 2 Ok
                 */
                Locations destination = locationsDAO.findById(interlocuteur).orElse(null);

                if(destination != null && ! Locations.Etats.VIRTUEL.equals(destination.getEtat()) && ! Locations.Etats.FERME.equals(destination.getEtat())) {
                    /**
                     * On construit notre Job 
                     */
                    Job nextStep = JobHelper.initJobCommandePoussee(interlocuteur) ;
                    if(destination.getUrl() == null ) {
                        nextStep.setSens(Job.Sens.READY_TO_DOWNLOAD);
                    }
                    /**
                     * Step 3 Ok
                     */
                    String idMessage ="-" + orderDto.getId();
                    try {
                        JobHelper.fillJobCommandes(nextStep, job.getMessage(), idMessage);
                    } catch (JsonProcessingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    jobDAO.save(nextStep);
                }
            }

        }

        return retour;
    }

}
