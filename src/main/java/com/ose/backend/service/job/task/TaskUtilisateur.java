package com.ose.backend.service.job.task;

import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.service.security.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.model.job.JobLog;

@Component
public class TaskUtilisateur extends Task {
    @Autowired
    private IUsersService usersService;

    @Override
    public JobLog doIt(JobLog retour) {        
        POSUsersDTO dto = null;
        ObjectMapper mapper = new ObjectMapper();
        String message = job.getMessage();
        retour.setLevel(JobLog.Level.OK);        
        try {
            dto = mapper.readValue(message, POSUsersDTO.class);
            usersService.save(dto);
            retour.setMessage("Ok - Maj User : " + dto.getId());

        } catch (Exception e) {
            retour.setLevel(JobLog.Level.ERREUR);
            retour.setMessage("Erreur - Message : " + e.getMessage());
        }

        return retour;
    }

}
