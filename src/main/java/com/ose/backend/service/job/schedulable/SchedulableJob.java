package com.ose.backend.service.job.schedulable;

import java.util.Date;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.model.saleslocations.Locations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.controller.jobs.JobsController;
import com.ose.backend.dao.job.IJobDAO;
import com.ose.backend.dao.job.IJobLogDAO;
import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.job.IExecutable;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.job.task.TaskFactory;

@Component
public class SchedulableJob implements Runnable{

    @Autowired
    private ILocationsApiDAO locationDAO;
    @Autowired
    private IJobDAO jobDAO;  
    @Autowired
    private IJobLogDAO jobLogDAO;  
    @Autowired
    private IJobsService jobsService;
    @Autowired
    private TaskFactory taskFactory;

    @Override
    public void run() {
    }
    
    public boolean isActive() {
        return jobDAO != null ;
    }

    public void executeTaches(String type) {
        if(jobDAO != null) {
            List<Job> liste = jobDAO.findTodoListByType(type);
            //Question tâches Alertes et Tâches Erreur ?

            for(Job tache : liste) {
                //Pour chaque tâche on fait ce qu'il y a à faire
                //Et on enregistre le résultat
                //            System.out.println( "Tâche : " + tache.toString() + " - à éxécuter");

                if(jobsService.isExecutable(tache)) {
                    JobLog log = executeTache(tache);

                    //Une fonction d'éxécution qui retourne un JobLog
                    //Et génère les tâches suivantes si besoin :
                    //- la petite soeur pour une tâche récurrente 
                    //- la tache suivante de remonté d'infos pour certaines
                    //- les tâches DL ...
                    //On enregistre le JobLog
                    if(log != null) {
                        jobLogDAO.save(log);
                        tache.setEtat(log.getLevel());

                        if(Job.Etat.OK.equals(tache.getEtat())) {
                            tache.setDintegration(new Date());
                            //TODO remontee vers parent si besoin
                            //En gros on remonte vers le parent si on est en sens INTERNE et qu'il y a un destinataire
                            updateTacheParent(tache);
                        }
                        jobsService.save(tache);
                        //On update la tâche en fonction de le JobLog ( dates et état )
                        //TODO arbitrage on enregistre en une fois ou 1 par 1 ?
                    }            
                    //TODO arbitrage on enregistre en une fois ou 1 par 1 ?     
                }
            }
        }

    }

    private void updateTacheParent(Job tache) {
        if(Job.Sens.INTERNE.equals(tache.getSens()) && ! Job.Type.DOWNLOAD.equals(tache.getType()) && ! Job.Type.UPLOAD.equals(tache.getType()) ){
            RestTemplate restTemplate = new RestTemplate();
            ObjectMapper mapper = new ObjectMapper();

            Locations destLocation = tache.getDestination() == null ? null : locationDAO.findById(tache.getDestination()).orElse(null);
            String resourceUrl  = destLocation == null ? null : destLocation.getUrl() ;
            ResponseEntity<JsonNode> response = null ;


            if(resourceUrl != null) {
                try {
                    response = restTemplate.getForEntity(resourceUrl +  JobsController.BASE_URL +"/get?id={location}", JsonNode.class , tache.getId());
                    if( HttpStatus.OK.equals(response.getStatusCode())) {

                        //                        System.out.println(response.getStatusCode().name());
                        //                        System.out.println(response.getBody());

                        //Dans le body on choppe l'objet dto qui va bien
                        //Ensuite on modifie la date d'intégration 
                        //Ensuite on sauve

                        JobDTO dto;
                        try {
                            dto = mapper.treeToValue(response.getBody().get("content"),JobDTO.class);

                            if(dto != null) {
                                dto.setEtat(Job.Etat.OK);
                                dto.setDateIntegration(tache.getDintegration());        
                                response = restTemplate.postForEntity(resourceUrl + JobsController.BASE_URL + "/save" , dto , JsonNode.class);

                                //TODO consommation service distant
                                //                                System.out.println(response.getStatusCode().name());
                                //                                System.out.println(response.getBody());
                                if( HttpStatus.OK.equals(response.getStatusCode())) {

                                } else {
                                    //Mettre le message pour les logs à jour en fonction                
                                }
                            }

                        } catch (JsonProcessingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //TODO aviser sur ce cas
                    resourceUrl = null ;
                }
            }
            //TODO si on n'a pas de moyen de le faire on laisse quelque chose en ready to download
            //inventer un nouveau type de job
            //sans dout qu'on va tout remonter dans la tâche
            if(resourceUrl == null) {

            }
        }        
    }

    private JobLog executeTache(Job tache) {
        if(Job.Etat.PRET.equals(tache.getEtat())) {
            IExecutable job = taskFactory.getInstance(tache);
            return job.execute(tache);
        }
        return null;
    }

}
