package com.ose.backend.service.job.task;

import org.springframework.stereotype.Component;

import com.ose.backend.model.job.JobLog;

@Component
public class TaskDummy extends Task {


    @Override
    public JobLog doIt(JobLog retour) {
        retour.setLevel(JobLog.Level.OK);
        retour.setMessage("Ok - Message : " + job.getMessage());

        return retour;
    }

}
