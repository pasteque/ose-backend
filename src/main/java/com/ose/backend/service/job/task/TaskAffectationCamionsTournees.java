package com.ose.backend.service.job.task;

import java.util.Map;

import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.locations.IToursLocationsService;

@Component
public class TaskAffectationCamionsTournees extends Task {

    @Autowired
    private IToursLocationsService toursLocationsService;

    @Override
    public JobLog doIt(JobLog retour) {
        POSToursLocationsDTO toursLocationsDTO = null;
        String action = null;
        ObjectMapper mapper = new ObjectMapper();
        String message = job.getMessage();
        retour.setLevel(JobLog.Level.OK);        
        try {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(message, Map.class);
            action = mapper.readValue(map.get(Job.Parametres.ACTION), String.class);
            toursLocationsDTO = mapper.readValue(map.get(Job.Parametres.LIAISON), POSToursLocationsDTO.class);
            if(Job.Actions.DELETE.equals(action)) {
                toursLocationsService.delete(toursLocationsDTO);
                retour.setMessage("Ok - Suppression Lien : " + toursLocationsDTO.getId());
            } else {
                if(Job.Actions.INSERT.equals(action) || Job.Actions.UPDATE.equals(action)) {
                    toursLocationsService.save(toursLocationsDTO);
                    retour.setMessage("Ok - Maj Lien : " + toursLocationsDTO.getId());
                }
            }
        } catch (Exception e) {
            retour.setLevel(JobLog.Level.ERREUR);
            retour.setMessage("Erreur - Message : " + e.getMessage());
        }

        return retour;
    }
}
