package com.ose.backend.service.job.task;

import java.util.Arrays;
import java.util.List;

import org.pasteque.api.dao.locations.ILocationsApiDAO;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.controller.jobs.JobsController;
import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.locations.ILocationsServiceOSE;

@Component
public class TaskDownload extends Task {

    @Autowired
    private ILocationsApiDAO locationDAO;

    @Autowired
    private ILocationsServiceOSE locationService;

    public TaskDownload() {
        type = Job.Type.DOWNLOAD;
    }

    @Override
    public JobLog doIt(JobLog retour) {
        List<JobDTO> listedi = null;
        retour.setJobId(type + "-" + DateHelper.formatSQLTimeStamp(retour.getDate()));
        // On récupère les tâches disponibles auprès du serveur distant ( celui de destinataire )
        // et qui nous sont adressées

        // TODO consommation du service distant
        RestTemplate restTemplate = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();
        Locations serverLocation = locationDAO.findTopByServerDefaultTrue();
        String jobMessage = "";

        String location = serverLocation == null ? "0" : serverLocation.getId();

        //Pour tous les points de ventes avec une url on va interroger
        List<Locations> destLocations = locationService.findAllParentLocations(false, location, false, false);

        for(Locations destLocation : destLocations) {
            String resourceUrl = destLocation == null ? null : destLocation.getUrl();
            ResponseEntity<JsonNode> response = null;

            if (location != null && resourceUrl != null) {
                // TODO : construction auto de la chaine // surcharge du get for entity ?
                // Pour pouvoir en gros passer la methode + une map et zou
                // TODO : passer en static les chaines méthodes et critères


                try {
                    response = restTemplate.getForEntity(resourceUrl + JobsController.BASE_URL + "/getCriteria?etat=" + Job.Etat.PRET + "&sens="
                            + Job.Sens.READY_TO_DOWNLOAD + "&destination={location}", JsonNode.class, location);
                    if (HttpStatus.OK.equals(response.getStatusCode())) {


                        listedi = Arrays.asList(mapper.treeToValue(response.getBody().get("content"), JobDTO[].class));

                        if (listedi != null) {
                            // On intègre les tâches.
                            for (JobDTO JobDTO : listedi) {

                                // Faire les modifs en locales nécessaires sur les tâches downloadées
                                JobDTO.setSens(Job.Sens.INTERNE);
                                JobDTO.setDestinataire(destLocation.getId());
                                // Date reception
                                // Status en cours si OK
                                JobDTO = jobService.save(JobDTO);
                                // Avertir le destinataire de la bonne réception

                                // TODO service ? ou alors = upload màj ?

                                // Mettre le message pour les logs à jour en fonction
                            }
                        }
                    } else {
                        // TODO on a eu un pb
                    }
                } catch (Exception e) {
                    jobMessage = jobMessage.concat(location + " : ").concat(e.toString() + "\n");
                }

            } else {
                // TODO on a eu un pb
            }
        }
        retour.setMessage(jobMessage);
        return retour;
    }

}
