package com.ose.backend.service.job;

import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;

public interface IExecutable {

    JobLog execute(Job tache);

}
