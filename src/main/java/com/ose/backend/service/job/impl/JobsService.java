package com.ose.backend.service.job.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dao.job.IJobDAO;
import com.ose.backend.dao.job.IJobLogDAO;
import com.ose.backend.dao.orders.IOrdersDAO;
import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.dto.job.JobLogDTO;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.service.job.IJobsService;
import com.ose.backend.service.job.schedulable.SchedulableAffectationCamionsTournees;
import com.ose.backend.service.job.schedulable.SchedulableAffectationPDVCatalaogue;
import com.ose.backend.service.job.schedulable.SchedulableCommandes;
import com.ose.backend.service.job.schedulable.SchedulableDownload;
import com.ose.backend.service.job.schedulable.SchedulableJob;
import com.ose.backend.service.job.schedulable.SchedulableStockType;
import com.ose.backend.service.job.schedulable.SchedulableUpload;
import com.ose.backend.service.job.schedulable.SchedulableUtilisateurs;

@Service
public class JobsService implements IJobsService {

    @Autowired
    private IJobDAO jobDAO;  
    @Autowired
    private IJobLogDAO jobLogDAO;
    @Autowired
    private IOrdersDAO ordersDAO; 
    @Autowired
    private SchedulableAffectationCamionsTournees schedulableAffectationCamionsTournees;
    @Autowired
    private SchedulableCommandes schedulableCommandes;
    @Autowired
    private SchedulableAffectationPDVCatalaogue schedulableAffectationPDVCatalaogue;
    @Autowired
    private SchedulableStockType schedulableStockType;
    @Autowired
    private SchedulableUtilisateurs schedulableUtilisateurs;
    @Autowired
    private SchedulableDownload schedulableDownload;
    @Autowired
    private SchedulableUpload schedulableUpload;
    
    //TODO question JOB ACK et JOB DUMMY

    @Override
    public JobDTO getById(String id) {
        return jobDAO.findByPrimaryKey(id).getAdapter(JobDTO.class);
    }

    @Override
    public List<JobDTO> getByCriteria(Map<String, Object> parameters) {
        return AdaptableHelper.getAdapter(jobDAO.findByCriteria(parameters), JobDTO.class);
    }

    @Override
    public JobLogDTO getLogById(String id) {
        JobLog log = jobLogDAO.findById(id).orElse(null);
        return log == null ? null : log.getAdapter(JobLogDTO.class);
    }

    @Override
    public List<JobLogDTO> getLogByCriteria(Map<String, Object> parameters) {
        return AdaptableHelper.getAdapter(jobLogDAO.findByCriteria(parameters), JobLogDTO.class);
    }

    @Override
    public
    List<String> getSens(Map<String, Object> parameters){
        Job jobject = new Job();
        Job.Sens object = jobject.new Sens();
        List<String> retour = new ArrayList<String>();
        Field[] fields = Job.Sens.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }

    @Override
    public
    List<String> getTypes(Map<String, Object> parameters){
        Job jobject = new Job();
        Job.Type object = jobject.new Type();
        List<String> retour = new ArrayList<String>();
        Field[] fields = Job.Type.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }

    @Override
    public
    List<String> getEtats(Map<String, Object> parameters){
        Job jobject = new Job();
        Job.Etat object = jobject.new Etat();
        List<String> retour = new ArrayList<String>();
        Field[] fields = Job.Etat.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }

    @Override
    public
    List<String> getLevels(Map<String, Object> parameters){
        JobLog jobject = new JobLog();
        JobLog.Level object = jobject.new Level();
        List<String> retour = new ArrayList<String>();
        Field[] fields = JobLog.Level.class.getDeclaredFields();
        for (Field f : fields) {
            if (Modifier.isStatic(f.getModifiers()) && f.getType() == String.class) {
                try {
                    retour.add((String)f.get(object));
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 
        }
        return retour;
    }

    @Override
    public SchedulableJob getJob(String type) {
        switch(type) {
        case Job.Type.AFFECTATION_CAMIONS_TOURNEES :
            return schedulableAffectationCamionsTournees;
        case Job.Type.AFFECTATION_PDV_CATALOGUE :
            return schedulableAffectationPDVCatalaogue;
        case Job.Type.COMMANDE :
            return schedulableCommandes;
        case Job.Type.STOCKTYPE :
            return schedulableStockType;
        case Job.Type.UTILISATEUR :
            return schedulableUtilisateurs;
        case Job.Type.DOWNLOAD :
            return schedulableDownload;
        case Job.Type.UPLOAD :
            return schedulableUpload;
        case Job.Type.ACK :
            break;    
        case Job.Type.DUMMY :
            break;  
        }
        return null;
    } 
    
    @Override
    public boolean isExecutable(JobDTO dto) {
        List<String> includeLocationList = jobDAO.getIncludeLocationList();
        Map<String, String> jobDefinition = jobDAO.getJobsDefinition();
        //TODO c'est ici qu'on peut ajouter des contraintes sur les Jobs
        boolean retour = (jobDefinition != null && jobDefinition.containsKey(dto.getType()) 
                &&  includeLocationList != null && includeLocationList.contains(dto.getDestinataire()));
        if(retour && Job.Type.COMMANDE.equals(dto.getType())) {
            try {
                OrderDTO orderDto = null;
                List<PaletteDTO> paletteDtos = null;
                ObjectMapper mapper = new ObjectMapper();
                String message = dto.getMessage();
                @SuppressWarnings("unchecked")
                Map<String, String> map = mapper.readValue(message, Map.class);
                orderDto = mapper.readValue(map.get(Job.Parametres.COMMANDE), OrderDTO.class);
                paletteDtos = mapper.readValue(map.get(Job.Parametres.PALETTES), new TypeReference<List<PaletteDTO>>() {
                });

                if (orderDto != null) {
                    retour = includeLocationList.contains(orderDto.getLocationFromId()) && includeLocationList.contains(orderDto.getLocationToId());
                }

                if(orderDto == null && paletteDtos != null) {
                    PaletteDTO palette = paletteDtos.get(0);
                    Orders order = ordersDAO.findByPrimaryKey(palette.getOrderId());
                    retour = order != null && includeLocationList.contains(order.getLocationFromId()) && includeLocationList.contains(order.getLocationToId());
                }

            } catch (Exception e) {
                e.printStackTrace();
                retour = false;
            }
        }
        return retour;
    }

    @Override
    public boolean isExecutable(Job object) {
        List<String> includeLocationList = jobDAO.getIncludeLocationList();
        Map<String, String> jobDefinition = jobDAO.getJobsDefinition();
        
        //TODO c'est ici qu'on peut ajouter des contraintes sur les Jobs
        boolean retour = (jobDefinition != null && jobDefinition.containsKey(object.getType()) 
                &&  includeLocationList != null && includeLocationList.contains(object.getDestination()));
        if(retour && Job.Type.COMMANDE.equals(object.getType())) {
            try {
                OrderDTO orderDto = null;
                List<PaletteDTO> paletteDtos = null;
                ObjectMapper mapper = new ObjectMapper();
                String message = object.getMessage();
                @SuppressWarnings("unchecked")
                Map<String, String> map = mapper.readValue(message, Map.class);
                orderDto = mapper.readValue(map.get(Job.Parametres.COMMANDE), OrderDTO.class);
                paletteDtos = mapper.readValue(map.get(Job.Parametres.PALETTES), new TypeReference<List<PaletteDTO>>() {
                });

                if (orderDto != null) {
                    retour = includeLocationList.contains(orderDto.getLocationFromId()) && includeLocationList.contains(orderDto.getLocationToId());
                }

                if(orderDto == null && paletteDtos != null) {
                    PaletteDTO palette = paletteDtos.get(0);
                    Orders order = ordersDAO.findByPrimaryKey(palette.getOrderId());
                    retour = order != null && includeLocationList.contains(order.getLocationFromId()) && includeLocationList.contains(order.getLocationToId());
                }



            } catch (Exception e) {
                e.printStackTrace();
                retour = false;

            }
        }
        return retour;
    }
    
    @Override
    @Transactional
    public Job save(Job object) {
        if(isExecutable(object)) {
            return jobDAO.save(object);
        }
        return object;
    }

    @Override
    @Transactional
    public JobDTO save(JobDTO dto) {
        Job edi = null ;
        boolean update = false ;
        boolean nouveau = false ;
        if(isExecutable(dto)) {
            if(dto.getId() != null) {
                edi = jobDAO.findByPrimaryKey(dto.getId());
            }
            if(edi == null) {
                edi = new Job(); 
                nouveau = true;
            }

            update = edi.loadFromDTO(dto);

            if(nouveau || update) {
                update = false;
                if(edi.getId() == null) {
                    edi.createId("import");
                }
                return save(edi).getAdapter(JobDTO.class);
            }

        }

        return dto;
    }


}
