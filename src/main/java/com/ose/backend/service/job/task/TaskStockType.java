package com.ose.backend.service.job.task;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.stocks.StockLevelDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.stock.IStocksServiceOse;

@Component
public class TaskStockType extends Task {

    @Autowired
    private IStocksServiceOse stocksService;

    @Override
    public JobLog doIt(JobLog retour) {
        List<StockLevelDTO> stockLevelDTO = null;
        String action = null;
        ObjectMapper mapper = new ObjectMapper();
        String message = job.getMessage();
        retour.setLevel(JobLog.Level.OK);        
        try {
            @SuppressWarnings("unchecked")
            Map<String, String> map = mapper.readValue(message, Map.class);
            action = mapper.readValue(map.get(Job.Parametres.ACTION), String.class);
            stockLevelDTO = mapper.readValue(map.get(Job.Parametres.LIAISON), new TypeReference<List<StockLevelDTO>>() {
            });

            if(Job.Actions.DELETE.equals(action)) {
                for(StockLevelDTO dto : stockLevelDTO) {  
                    stocksService.deleteStockLevel(dto);
                }
                retour.setMessage("Ok - Suppression de Stock Type Nb = : " + stockLevelDTO.size());
            } else {
                if(Job.Actions.INSERT.equals(action) || Job.Actions.UPDATE.equals(action)) {
                    for(StockLevelDTO dto : stockLevelDTO) {  
                        stocksService.saveStockLevel(dto);
                    }
                    retour.setMessage("Ok - Maj du stock type Nb = : " + stockLevelDTO.size());
                }
            }
        } catch (Exception e) {
            retour.setLevel(JobLog.Level.ERREUR);
            retour.setMessage("Erreur - Message : " + e.getMessage());
        }

        return retour;
    }
}
