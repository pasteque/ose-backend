package com.ose.backend.service.job.task;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ose.backend.dao.job.IJobDAO;
import com.ose.backend.model.job.Job;
import com.ose.backend.model.job.JobLog;
import com.ose.backend.service.job.IExecutable;
import com.ose.backend.service.job.IJobsService;

@Component
public class Task implements IExecutable {

    @Autowired
    protected IJobDAO jobDAO;
    @Autowired
    protected IJobsService jobService;

    protected Job job;
    protected String type;
    protected String description;

    public Task() {
        this.type = Job.Type.DUMMY;
        this.description = "Empty";
    }

    protected void init(Job job) {
        if(job != null) {
            this.job = job;
            this.type = job.getType();
            this.description = job.toString();
        }
    }

    private JobLog launch() {
        String jobId = null ;
        if (job != null) {
            job.setEtat(Job.Etat.EN_COURS);
            job.setDenvoi(new Date());
            jobService.save(job); 
            jobId = job.getId();
        }

        JobLog retour = new JobLog();
        retour.setDate(new Date());
        retour.setJobId(jobId);

        return retour;
    }

    public JobLog doIt(JobLog retour) {
        retour.setLevel(JobLog.Level.ERREUR);
        retour.setMessage("Type de tâche non implementé - " + type + " - tache : " + description);
        return retour;
    }

    @Override
    public JobLog execute(Job tache) {
        init(tache);
        JobLog retour = launch();
        retour = doIt(retour);
        retour.createId();
        return retour;
    }
}
