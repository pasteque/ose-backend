package com.ose.backend.service.job;

import java.util.List;
import java.util.Map;

import com.ose.backend.dto.job.JobDTO;
import com.ose.backend.dto.job.JobLogDTO;
import com.ose.backend.model.job.Job;
import com.ose.backend.service.job.schedulable.SchedulableJob;


public interface IJobsService {
    
    JobDTO getById(String id);

    JobDTO save(JobDTO dto);

    List<JobDTO> getByCriteria(Map<String, Object> parameters);

    JobLogDTO getLogById(String id);
    
    List<JobLogDTO> getLogByCriteria(Map<String, Object> parameters);

    List<String> getSens(Map<String, Object> parameters);
    
    List<String> getTypes(Map<String, Object> parameters);

    List<String> getEtats(Map<String, Object> parameters);

    List<String> getLevels(Map<String, Object> parameters);

    SchedulableJob getJob(String type);

    Job save(Job object);

    boolean isExecutable(Job object);

    boolean isExecutable(JobDTO dto);

}
