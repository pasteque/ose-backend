package com.ose.backend.service.upgrade;

public interface IInitDataBaseService {
    
    public void init();
    
}
