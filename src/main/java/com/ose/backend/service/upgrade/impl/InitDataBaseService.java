package com.ose.backend.service.upgrade.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.pasteque.api.dao.cash.ICashRegistersApiDAO;
import org.pasteque.api.dao.cash.IClosedCashApiDAO;
import org.pasteque.api.model.cash.CashRegisters;
import org.pasteque.api.model.cash.ClosedCash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ose.backend.constants.RequestMappingNames;
import com.ose.backend.dao.tickets.ITicketsDAO;

@Service
@Profile("update")
public class InitDataBaseService {

    @Autowired
    private ICashRegistersApiDAO cashRegistersDAO;

    @Autowired
    private ITicketsDAO ticketsDAO;
    
    @Autowired
    private IClosedCashApiDAO closedCashDAO;

    @PostConstruct
    public void init() {
        this.updateNextsTicketsIds();
    }

    private void updateNextsTicketsIds() {
        List<CashRegisters> cashRegistersList = cashRegistersDAO.findAll();

        for (CashRegisters cashRegister : cashRegistersList) {

            Map<String, Object> lastTicket = ticketsDAO
                    .getLastTicketInformations(cashRegister.getId(), null);
            if (lastTicket != null && lastTicket
                    .get(RequestMappingNames.Tickets.TICKETID) != null) {
                cashRegister.setNextTicketId((Long) (lastTicket
                        .get(RequestMappingNames.Tickets.TICKETID)) + 1);
                cashRegistersDAO.save(cashRegister);
            }
            
            ClosedCash lastSession = closedCashDAO.findFirstByCashRegisterOrderByHostSequenceDesc(cashRegister);
            if (lastSession != null && lastSession.getHostSequence() >= cashRegister.getNextSessionId()) {
                cashRegister.setNextSessionId(lastSession.getHostSequence() + 1);
                cashRegistersDAO.save(cashRegister);
            }
        }

        System.out.println("Mise à jour nexts tickets ids OK");
    }
}