package com.ose.backend.service.monitoring.impl;

import java.net.PasswordAuthentication;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.pasteque.api.dao.locations.ISalesLocationsApiDAO;
import org.pasteque.api.dao.locations.IToursApiDAO;
import org.pasteque.api.dto.cash.POSClosedCashDTO;
import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSSalesLocationsDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.saleslocations.SalesLocations;
import org.pasteque.api.model.saleslocations.Tours;
import org.pasteque.api.model.saleslocations.ToursLocations;
import org.pasteque.api.service.saleslocations.ISalesLocationsService;
import org.pasteque.api.util.DateHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.ose.backend.config.Monitoring;
import com.ose.backend.config.Monitoring.JourLivraison;
import com.ose.backend.constants.DefaultValues;
import com.ose.backend.constants.RequestMappingNames;
import com.ose.backend.dao.cash.IClosedCashDAO;
import com.ose.backend.dao.orders.IOrdersDAO;
import com.ose.backend.dao.sqlresult.SQLResultNativeQuery;
import com.ose.backend.dao.tickets.ITicketsDAO;
import com.ose.backend.dto.monitoring.MonitoringCamionsDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringCataloguesDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringClosedCachesDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringDTO;
import com.ose.backend.dto.monitoring.MonitoringDeballagesDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringLocationsAffectedToManyToursDTO;
import com.ose.backend.dto.monitoring.MonitoringMagasinsDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringOrderNotReceivedDetailDTO;
import com.ose.backend.dto.monitoring.MonitoringOrdersWithoutCostumersDTO;
import com.ose.backend.dto.monitoring.MonitoringTourWhithoutLocationDTO;
import com.ose.backend.model.orders.Orders;
import com.ose.backend.service.application.IStatusService;
import com.ose.backend.service.locations.ILocationsServiceOSE;
import com.ose.backend.service.locations.IToursLocationsService;
import com.ose.backend.service.locations.IToursService;
import com.ose.backend.service.monitoring.IMonitoringService;
import com.ose.backend.service.products.ITariffAreasServiceOSE;
import com.sun.mail.smtp.SMTPTransport;

@Service
public class MonitoringService implements IMonitoringService {

	@Autowired
	private IStatusService statusService;
	@Autowired
	private ILocationsServiceOSE locationServiceOSE;
	@Autowired
	private IToursService toursService;
	@Autowired
	private ITariffAreasServiceOSE tariffAreasService;
	@Autowired
	private ITicketsDAO ticketsDAO;
	@Autowired
	private ISalesLocationsService salesLocationsService;
	@Autowired
	private IToursLocationsService toursLocationService;
	@Autowired
	private ISalesLocationsApiDAO salesLocationsDAO;
	@Autowired
	private IClosedCashDAO closedCashDAO;
	@Autowired
	private IToursApiDAO toursDAO;
	@Autowired
	private IOrdersDAO ordersDAO;
	@Autowired
	private Monitoring monitoringConfig;

	public static final int NB_JOURS_CTRL_CATALOGUE_EXPIRATION = 7;
	public static final int NB_JOURS_CTRL_DEBALLAGES_CHARGES = 14;
	public static final int NB_JOURS_CTRL_SESSIONS_OUVERTES = 30;
	public static final int NB_JOURS_CTRL_COMMANDES_SANS_CLIENT = 30;
	public static final int NB_JOURS_CTRL_COMMANDES_RECEPTIONNEES = 3;
	
	public static final String ERREUR_DATA_INCOMPLETE = "Données incomplètes";
	public static final String ERREUR_LIVRAISON = "Attention, livraison Aujourd'hui , données incomplètes";
	public static final String ERREUR_CATALOGUE_EXPIRE_DANS = "Catalogue va expirer dans " + NB_JOURS_CTRL_CATALOGUE_EXPIRATION
			+ " jours";
	public static final String ERREUR_CATALOGUE_MULTIPLE_SUR_PERIODE = "Attention, plusieurs catalogues sont affectés sur la même période";
	public static final String ERREUR_CATALOGUE_NON_AFFECTE = "Attention, aucun catalogue affecté";

	Calendar calendar = Calendar.getInstance(Locale.FRANCE);

	LinkedHashMap<String, String> destinatairesByType;

	@Scheduled(cron = "${monitoring.cron:-}")
	public void validateData() {
		MonitoringDTO monitoringDTO = new MonitoringDTO();
		monitoringDTO.setDate(DateHelper.today());
		List<MonitoringDetailDTO> dataOSEMagasins = ValidateDataOSEMagasins();
		List<MonitoringDetailDTO> dataOSECamions = ValidateDataOSECamions();
		List<MonitoringDetailDTO> dataCatalogues = ValidateDataCatalogues();
		List<MonitoringDetailDTO> dataDeballages = validateDataDeballages();
		List<MonitoringDetailDTO> dataNotClosedCashes = validateDataNotClosedCaches();
		List<MonitoringDetailDTO> dataOrdersWithoutCustomers = validateDataOrdersWithoutCustomers();
		List<MonitoringDetailDTO> dataToursAffectedToManyLocation = validateDataLocationAffectedToManyTours();
		List<MonitoringDetailDTO> dataToursWhithoutLocation = validateDataTourWhithoutLocation();
		List<MonitoringDetailDTO> DataOrders = ValidateDataReceiptOrders();

		for (MonitoringDetailDTO dataOSEMagasin : dataOSEMagasins) {
			monitoringDTO.addDetail(dataOSEMagasin);
		}
		for (MonitoringDetailDTO dataOSECamion : dataOSECamions) {
			monitoringDTO.addDetail(dataOSECamion);
		}
		for (MonitoringDetailDTO dataCatalogue : dataCatalogues) {
			monitoringDTO.addDetail(dataCatalogue);
		}
		for (MonitoringDetailDTO dataDeballage : dataDeballages) {
			monitoringDTO.addDetail(dataDeballage);
		}
		for (MonitoringDetailDTO dataNotClosedCashe : dataNotClosedCashes) {
			monitoringDTO.addDetail(dataNotClosedCashe);
		}

		for (MonitoringDetailDTO dataOrdersWithoutCustomer : dataOrdersWithoutCustomers) {
			monitoringDTO.addDetail(dataOrdersWithoutCustomer);
		}
		for (MonitoringDetailDTO dateTourAffectedToManyLocation : dataToursAffectedToManyLocation) {
			monitoringDTO.addDetail(dateTourAffectedToManyLocation);
		}
		for (MonitoringDetailDTO dataTourWhithoutLocation : dataToursWhithoutLocation) {
			monitoringDTO.addDetail(dataTourWhithoutLocation);
		}
		for (MonitoringDetailDTO DataOrder : DataOrders) {
			monitoringDTO.addDetail(DataOrder);
		}
		processMonitoring(monitoringDTO);
	}

	private void processMonitoring(MonitoringDTO monitoringDTO) {
		if (monitoringDTO.isHasAlertes() || monitoringDTO.isHasErreurs()) {
			sendMail(monitoringDTO);
		}
	}

	private void sendMail(MonitoringDTO monitoringDTO) {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", monitoringConfig.getSmtp().getHost());
		properties.put("mail.smtp.port", monitoringConfig.getSmtp().getPort());

		Session session = Session.getInstance(properties, new Authenticator() {
			@SuppressWarnings("unused")
			protected PasswordAuthentication getPasswordAuthenticator() {
				return new PasswordAuthentication(monitoringConfig.getSmtp().getLogin(),
						monitoringConfig.getSmtp().getPassword().toCharArray());
			}
		});

		MimeMessage message;
		try {
			HashMap<String, String> HTMLMessages = new HashMap<String, String>();
			LinkedHashMap<String, String> HTMLMessagesByType = monitoringDTO.getHTMLMessageByType();

			HashMap<String, String> destsTypes = getDestinataireByType();
			for (Entry<String, String> destType : destsTypes.entrySet()) {
				String key = destType.getKey();
				if (HTMLMessagesByType.containsKey(key)) {
					String[] lstDestinataires = destType.getValue().split(";");
					for (String destinataire : lstDestinataires) {
						if (HTMLMessages.containsKey(destinataire)) {
							HTMLMessages.put(destinataire,
									HTMLMessages.get(destinataire).concat(HTMLMessagesByType.get(key)));
						} else {
							HTMLMessages.put(destinataire,
									monitoringDTO.getTitre() == null ? "".concat(HTMLMessagesByType.get(key))
											: monitoringDTO.getTitre().concat(HTMLMessagesByType.get(key)));
						}
					}
				}
			}

			for (Entry<String, String> HTMLMessage : HTMLMessages.entrySet()) {
				message = new MimeMessage(session);
				message.setFrom(new InternetAddress(monitoringConfig.getSmtp().getLogin()));
				message.addRecipients(Message.RecipientType.TO, HTMLMessage.getKey());
				message.setSubject("Rapport d'intégration des flux Back Office Central");
				message.setContent(HTMLMessage.getValue(), "text/html; charset=utf-8");

				SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
				t.connect(monitoringConfig.getSmtp().getHost(), monitoringConfig.getSmtp().getLogin(),
						monitoringConfig.getSmtp().getPassword());
				t.sendMessage(message, message.getAllRecipients());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<MonitoringDetailDTO> ValidateDataOSEMagasins() {
		Date currentDate = new Date();
		int currentJour = DateHelper.dayOfWeek(currentDate);
		MonitoringMagasinsDetailDTO detailDTO = null;
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		List<LocationsInfoDTO> locations = AdaptableHelper.getAdapter(
				locationServiceOSE.findAllParentLocations(true, null, false, false), LocationsInfoDTO.class);
		for (LocationsInfoDTO location : locations) {
			if (location.getCategory() != null && location.getCategory().equals(DefaultValues.Categories.MAGASINS)) {
				List<Object> cachesStatus = statusService.getCashesStatus(location.getId());
				if (cachesStatus != null) {

					if (cachesStatus.isEmpty()) {
						continue;
					} else {

						Object[] resultObj = (Object[]) cachesStatus.get(0); // On doit n'avoir qu'une ligne
						Timestamp lastTicketHour = (Timestamp) resultObj[7];
						String formattedDate = DateHelper.format(lastTicketHour, DateHelper.DD_MM_YYYY_HH_MM_SS);
						;
						String yesterday = DateHelper.format(DateHelper.yesterday(), "yyyy-MM-dd 16:00:00");
						Boolean yesterdayIsDimanche = DateHelper.isDimanche(DateHelper.yesterday());
						if (yesterdayIsDimanche) {
							yesterday = DateHelper.format(DateHelper.addDays(DateHelper.yesterday(), -1),
									"yyyy-MM-dd 16:00:00");
						}
						if (lastTicketHour.before(Timestamp.valueOf(yesterday))) {
							detailDTO = new MonitoringMagasinsDetailDTO((String) resultObj[1], formattedDate,
									MonitoringMagasinsDetailDTO.Type.VENTES_MAGASINS);
							detailDTO.addErreur(String.format(ERREUR_DATA_INCOMPLETE));
							// LocationsLivraison locationsLivraison =
							// getJourLivraison(String.valueOf(location.getId()));
							JourLivraison locationsLivraison = monitoringConfig.getCalendrierLivraisons()
									.get(location.getId());
							if (locationsLivraison != null && currentJour == locationsLivraison.getJour()
									&& (locationsLivraison.isSemainePaire() == null
											|| (DateHelper.weekOfYear(currentDate) % 2 == 0
													&& locationsLivraison.isSemainePaire() != null
													&& locationsLivraison.isSemainePaire())
											|| (DateHelper.weekOfYear(currentDate) % 2 != 0
													&& locationsLivraison.isSemainePaire() != null
													&& !locationsLivraison.isSemainePaire()))) {
								detailDTO.addAlerte(String.format(ERREUR_LIVRAISON));
							}
							detailsDTO.add(detailDTO);
						}
					}
				}
			}
		}

		return detailsDTO;
	}

	private List<MonitoringDetailDTO> ValidateDataOSECamions() {
		int currentJour = DateHelper.dayOfWeek(new Date());
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		MonitoringCamionsDetailDTO detailDTO = null;
		List<Tours> tours = toursDAO.findAll();
		for (Tours tour : tours) {

			Map<String, Object> lastTicket = ticketsDAO.getLastTicketInformations(null, String.valueOf(tour.getId()));
			if (lastTicket != null && lastTicket.get(RequestMappingNames.Tickets.TICKETID) != null) {
				// DATELASTTICKET
				Timestamp lastTicketHour = (Timestamp) lastTicket.get(RequestMappingNames.Tickets.TICKETDATE);
				String formattedDate = new SimpleDateFormat("dd-MM-yyyy HH:ss:mm").format(lastTicketHour);
				// Location Id
				detailDTO = new MonitoringCamionsDetailDTO(
						(String) lastTicket.get(RequestMappingNames.Locations.LOCATIONID), tour.getName(),
						formattedDate, MonitoringCamionsDetailDTO.Type.VENTES_CAMIONS);
				String yesterday = DateHelper.format(DateHelper.yesterday(), "yyyy-MM-dd 16:00:00");
				if (lastTicketHour.before(Timestamp.valueOf(yesterday))) {
					// Location Name
					detailDTO = new MonitoringCamionsDetailDTO(
							(String) lastTicket.get(RequestMappingNames.Locations.LOCATIONNAME), tour.getName(),
							formattedDate, MonitoringCamionsDetailDTO.Type.VENTES_CAMIONS);
					detailDTO.addErreur(String.format(ERREUR_DATA_INCOMPLETE));
					JourLivraison locationsLivraison = monitoringConfig.getCalendrierLivraisons()
							.get(String.valueOf(tour.getId()));
					if (locationsLivraison != null && currentJour == locationsLivraison.getJour()) {
						detailDTO.addAlerte(String.format(ERREUR_LIVRAISON));
					}
					detailDTO.setOrderBy(tour.getId());
					detailsDTO.add(detailDTO);
				}

			}

		}
		Collections.sort(detailsDTO);
		return detailsDTO;
	}

	private List<MonitoringDetailDTO> ValidateDataReceiptOrders() {
		MonitoringOrderNotReceivedDetailDTO detailDTO = null;
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		Date currentDate = new Date();
		String pdvId = null;
		Map<String, Locations> pdvs = new HashMap<String, Locations>();
		// Concrêtement on recherche les points de vente qui appartiennent à la
		// catégorie Magasin et qui soient des parents
		// Utilisé uniquement ici
		// TODO à reprendre

		List<Locations> locationsMag = locationServiceOSE.findAllParentLocations(true, null, false, false);
		for (Locations location : locationsMag) {
			if (DefaultValues.Categories.IDMAGASINS.equals(location.getCategoryId())) {
				pdvs.put(location.getId(), location);
			}
		}
		List<Tours> toursCam = toursDAO.findAll();
		for (Tours tour : toursCam) {
			List<ToursLocations> toursLocations = toursLocationService.findCurrent(null, String.valueOf(tour.getId()),
					currentDate, null);
			if (toursLocations != null && !toursLocations.isEmpty()) {
				pdvs.put(String.valueOf(tour.getId()), toursLocations.get(0).getLocation());
			}
		}

		Locations location;
		for (Map.Entry<String, Locations> entry : pdvs.entrySet()) {
			pdvId = entry.getKey();
			location = entry.getValue();
			Date dateOflastDelivery = getLastDateOfDelivery(pdvId);
			Date datelastDelivery = dateOflastDelivery == null ? null
					: DateHelper.parse(DateHelper.format(dateOflastDelivery, "dd-MM-yyyy 00:00:00"),
							DateHelper.DD_MM_YYYY_HH_MM_SS);
			List<Orders> order = dateOflastDelivery == null ? null
					: ordersDAO.findOrdersRecievedByCriteria(location.getId(), null, datelastDelivery, null);
			if ((order != null && order.isEmpty())
					&& DateHelper.daysBetween(currentDate, datelastDelivery) >= NB_JOURS_CTRL_COMMANDES_RECEPTIONNEES) {
				String pdv = location.getId().concat(" - ").concat(location.getName());
				String dateDelivery = DateHelper.format(dateOflastDelivery, DateHelper.DD_MM_YYYY);
				detailDTO = new MonitoringOrderNotReceivedDetailDTO(pdv, location.getCategory().getName(), dateDelivery,
						MonitoringOrderNotReceivedDetailDTO.Type.COMMANDES_NON_RECEPTIONNEES);
				detailDTO.addAlerte("La commande en date du: " + dateDelivery + " n'est pas  réceptionnée ");
				detailsDTO.add(detailDTO);
			}
		}

		return detailsDTO;
	}

	private List<MonitoringDetailDTO> ValidateDataCatalogues() {
		MonitoringCataloguesDetailDTO detailDTO = null;
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		List<TariffAreasLocationsDTO> currentTarifsArea = null;
		TariffAreasLocationsDTO currentTarifArea = null;
		List<LocationsInfoDTO> locations = AdaptableHelper.getAdapter(
				locationServiceOSE.findAllParentLocations(true, null, false, false), LocationsInfoDTO.class);

		for (LocationsInfoDTO location : locations) {
			String nextCatalogue = "";
			String dateDebutNextCatalogue = "";
			Date currentDate = new Date();
			String pdv = location.getId().concat(" - ").concat(location.getLabel());
			currentTarifsArea = tariffAreasService.getCurrentTariffAreasLinks(location.getId(), null, null,
					DefaultValues.TariffAreas.ORDER_CATALOGUE_PAPIER, currentDate, currentDate,
					TariffAreasLocationsDTO.class);
			if (currentTarifsArea != null && !currentTarifsArea.isEmpty()) {
				currentTarifArea = currentTarifsArea.get(0);
				String dateTarifArea = DateHelper.format(currentTarifArea.getEndDate(), DateHelper.DD_MM_YYYY_HH_MM_SS);
				detailDTO = new MonitoringCataloguesDetailDTO(pdv, location.getCategory(),
						currentTarifArea.getTariffArea().getNom(), dateTarifArea, nextCatalogue, dateDebutNextCatalogue,
						MonitoringCataloguesDetailDTO.Type.CATALOGUE);

				Date currentTarifAreaDate = DateHelper.parse(
						DateHelper.format(currentTarifArea.getEndDate(), DateHelper.DD_MM_YYYY), DateHelper.DD_MM_YYYY);
				Date dateToCompareTo = DateHelper.parse(DateHelper
						.format(DateHelper.addDays(currentDate, NB_JOURS_CTRL_CATALOGUE_EXPIRATION), DateHelper.DD_MM_YYYY),
						DateHelper.DD_MM_YYYY);
				List<TariffAreasLocationsDTO> nextTariffAreasLocationsDTO = null;
				if (currentTarifAreaDate.compareTo(dateToCompareTo) <= 0) {
					nextTariffAreasLocationsDTO = tariffAreasService.getCurrentTariffAreasLinks(location.getId(), null,
							null, DefaultValues.TariffAreas.ORDER_CATALOGUE_PAPIER,
							DateHelper.addDays(currentTarifArea.getEndDate(), 1), null, TariffAreasLocationsDTO.class);
					if (nextTariffAreasLocationsDTO != null && nextTariffAreasLocationsDTO.size() > 0) {
						TariffAreasLocationsDTO nextTariffAreaLocationsDTO = nextTariffAreasLocationsDTO.get(0);
						nextCatalogue = nextTariffAreaLocationsDTO.getTariffArea().getNom();
						dateDebutNextCatalogue = DateHelper.format(nextTariffAreaLocationsDTO.getStartDate(),
								DateHelper.DD_MM_YYYY_HH_MM_SS);
						detailDTO.setNextCatalogue(nextCatalogue);
						detailDTO.setDateDebutNextCatalogue(dateDebutNextCatalogue);

					}
					long diffJour = DateHelper.daysBetween(currentTarifArea.getEndDate(), currentDate);
					detailDTO.addAlerte(String.format("Catalogue va expirer dans " + diffJour + " jours"));

				}
				if (currentTarifsArea.size() > 1) {
					String tarifAreasNom = null;
					String tarifAreasDate = null;
					for (TariffAreasLocationsDTO tarif : currentTarifsArea) {
						if (tarifAreasNom == null && tarifAreasDate == null) {
							tarifAreasNom = tarif.getTariffArea().getNom();
							tarifAreasDate = DateHelper.format(tarif.getEndDate(), DateHelper.DD_MM_YYYY_HH_MM_SS);
						} else {
							tarifAreasNom += "</br>" + tarif.getTariffArea().getNom();
							tarifAreasDate += "</br>"
									+ DateHelper.format(tarif.getEndDate(), DateHelper.DD_MM_YYYY_HH_MM_SS);
						}
						detailDTO.setCurrentCatalogue(tarifAreasNom);
						detailDTO.setDateFinCurrentCatalogue(tarifAreasDate);

					}
					detailDTO.addErreur(String.format(ERREUR_CATALOGUE_MULTIPLE_SUR_PERIODE));
				}
			} else {
				detailDTO = new MonitoringCataloguesDetailDTO(pdv, location.getCategory(), "Pas de catalogue", "",
						"Pas de catalogue", "", MonitoringCataloguesDetailDTO.Type.CATALOGUE);
				detailDTO.addErreur(String.format(ERREUR_CATALOGUE_NON_AFFECTE));
			}

			if (!detailDTO.getAlerteList().isEmpty() || !detailDTO.getErreurList().isEmpty()) {
				detailsDTO.add(detailDTO);
			}

		}

		return detailsDTO;
	}

	/**
	 * cf http://192.168.42.15/mediawiki/index.php/BO_Monitoring_des_flux#Liste_des_d.C3.A9ballages
	 * 
	 * @return
	 */
	private List<MonitoringDetailDTO> validateDataDeballages() {
		MonitoringDeballagesDetailDTO detailDTO = null;
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		List<LocationsInfoDTO> locations = AdaptableHelper.getAdapter(
				locationServiceOSE.findAllParentLocations(true, null, false, false), LocationsInfoDTO.class);
		Date dateStartDay = DateHelper.todayStartOfDay();
		Date dateEndDay = DateHelper.todayEndOfDay();
		Date startDateBefore = DateHelper.addDays(dateStartDay, NB_JOURS_CTRL_DEBALLAGES_CHARGES);
		String now = String.valueOf(DateHelper.today().getTime());
		Integer saleLocationDay;
		for (LocationsInfoDTO location : locations) {

			if (location.getCategory() != null && location.getCategory().equals(DefaultValues.Categories.CAMIONS)) {
				int nbJoursSalesLocation = 0;
				int nbJoursManquantes = 0;
				Date dateCompareTo = dateEndDay;
				List<POSToursLocationsDTO> currentsTours = toursService.getCurrentToursLinks(location.getId(), null,
						null, now, null, POSToursLocationsDTO.class);
				if (currentsTours != null && !currentsTours.isEmpty()) {
					POSToursLocationsDTO currentTour = currentsTours.get(0);
					detailDTO = new MonitoringDeballagesDetailDTO(location.getLabel(), currentTour.getTour().getNom(),
							MonitoringDeballagesDetailDTO.Type.DEBALLAGES);
					saleLocationDay = null;
					MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
					map.add(SalesLocations.Fields.TOUR, String.valueOf(currentTour.getTour().getId()));
					map.add(SalesLocations.SearchCritria.FROM, DateHelper.formatSQLTimeStamp(dateStartDay));
					map.add(SalesLocations.SearchCritria.STARTBEFORE, DateHelper.formatSQLTimeStamp(startDateBefore));
					List<POSSalesLocationsDTO> salesLocations = salesLocationsService.getDTOByCriteria(map, null);
					for (POSSalesLocationsDTO salesLocation : salesLocations) {

						if (salesLocation.getStartDate().after(dateCompareTo)) {

							detailDTO.addAlerte("Alert il manque la journée de déballage du "
									+ DateHelper.format(dateCompareTo, DateHelper.ddMMyyyy));
							dateCompareTo = DateHelper.parse(
									DateHelper.format(salesLocation.getStartDate(), "dd-MM-yyyy 19:00:00"),
									"dd-MM-yyyy HH:mm:ss");
							nbJoursManquantes++;
						}

						if (saleLocationDay == null
								|| !saleLocationDay.equals(DateHelper.dayOfMonth(salesLocation.getStartDate()))) {
							saleLocationDay = DateHelper.dayOfMonth(salesLocation.getStartDate());
							dateCompareTo = DateHelper.addDays(dateCompareTo, 1);
							nbJoursSalesLocation++;
						}

					}

					if (salesLocations.isEmpty()
							|| nbJoursSalesLocation < NB_JOURS_CTRL_DEBALLAGES_CHARGES - nbJoursManquantes) {
						// Utilisé aussi pour la recherche de déballage c'est tout
						MultiValueMap<String, String> localmap = new LinkedMultiValueMap<String, String>();
						localmap.add(SalesLocations.Fields.TOUR, String.valueOf(currentTour.getTour().getId()));
						salesLocations = salesLocationsService.getDTOByCriteria(localmap, 1);
						detailDTO.addAlerte(
								"Déballages manquantes pour les " + NB_JOURS_CTRL_DEBALLAGES_CHARGES + " jours à venir ");
					}
					if (!salesLocations.isEmpty()
							&& (!detailDTO.getAlerteList().isEmpty() || !detailDTO.getErreurList().isEmpty())) {
						POSSalesLocationsDTO lastSalesLocations = salesLocations.get(salesLocations.size() - 1);
						detailDTO.setDeballage(lastSalesLocations.getCity() + " "
								+ DateHelper.format(lastSalesLocations.getStartDate(), DateHelper.DD_MM_YYYY_HH_MM_SS)
								+ " "
								+ DateHelper.format(lastSalesLocations.getEndDate(), DateHelper.DD_MM_YYYY_HH_MM_SS));
					}
					detailDTO.setOrderBy(currentTour.getTour().getId());
				}

				if (detailDTO != null && !detailDTO.getAlerteList().isEmpty() && !detailDTO.getErreurList().isEmpty()) {

					detailsDTO.add(detailDTO);
				}
			}

		}
		Collections.sort(detailsDTO);

		return detailsDTO;
	}

	private List<MonitoringDetailDTO> validateDataNotClosedCaches() {
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		MonitoringClosedCachesDetailDTO detailDTO = null;
		Date currrentDate = new Date();
		List<POSClosedCashDTO> notClosedCashes = AdaptableHelper.getAdapter(
				closedCashDAO.findNotClosedCash(
						String.valueOf(DateHelper.addDays(currrentDate, -NB_JOURS_CTRL_SESSIONS_OUVERTES).getTime())),
				POSClosedCashDTO.class);
		POSSalesLocationsDTO saleLocation;
		List<Object> cachesStatus;
		Object[] cacheStatus;
		for (POSClosedCashDTO notClosedCash : notClosedCashes) {
			saleLocation = notClosedCash.getSalesLocation();
			cachesStatus = statusService.getCashesStatus(notClosedCash.getCashRegisterId());
			if (cachesStatus != null && !cachesStatus.isEmpty()) {
				cacheStatus = (Object[]) cachesStatus.get(0); // On doit n'avoir qu'une ligne
				Timestamp lastTicketHour = (Timestamp) cacheStatus[7];
				if (lastTicketHour.compareTo(saleLocation.getEndDate()) >= 0) {
					List<POSToursLocationsDTO> ToursLocationsDTO = toursService.getCurrentToursLinks(null,
							String.valueOf(saleLocation.getTourId()), null, String.valueOf(currrentDate.getTime()),
							null, POSToursLocationsDTO.class);
					String deballage = saleLocation.getCity() + " "
							+ DateHelper.format(saleLocation.getStartDate(), DateHelper.DD_MM_YYYY_HH_MM_SS) + " - "
							+ DateHelper.format(saleLocation.getEndDate(), DateHelper.DD_MM_YYYY_HH_MM_SS);
					detailDTO = new MonitoringClosedCachesDetailDTO(ToursLocationsDTO.get(0).getLocation().getLabel(),
							saleLocation.getTourName(), deballage, MonitoringDetailDTO.Type.DEBALLAGES_NON_CLOTURES);
					detailDTO.addAlerte(MonitoringDetailDTO.Type.DEBALLAGES_NON_CLOTURES);
					detailsDTO.add(detailDTO);
				}
			}

		}
		return detailsDTO;
	}

	private List<MonitoringDetailDTO> validateDataOrdersWithoutCustomers() {
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		MonitoringOrdersWithoutCostumersDTO detailDTO = null;
		Date currrentDate = new Date();
		String dateStart = DateHelper
				.formatSQLTimeStamp(DateHelper.addDays(currrentDate, -NB_JOURS_CTRL_COMMANDES_SANS_CLIENT));
		SQLResultNativeQuery tickets = ticketsDAO.findTicketOrderWhithoutCustomers(dateStart);
		for (int index = 0; index < tickets.getListSize(); index++) {
			detailDTO = new MonitoringOrdersWithoutCostumersDTO(tickets.getValueAsString(index, "location"),
					tickets.getValueAsString(index, "nomTournee"), tickets.getValueAsLong(index, "ticket").toString(),
					MonitoringDetailDTO.Type.COMMANDES_SANS_CLIENT);
			detailDTO.addAlerte(MonitoringDetailDTO.Type.COMMANDES_SANS_CLIENT);
			detailDTO.setOrderBy(tickets.getValueAsLong(index, "numTournee"));
			detailsDTO.add(detailDTO);
		}
		Collections.sort(detailsDTO);
		return detailsDTO;
	}

	private List<MonitoringDetailDTO> validateDataLocationAffectedToManyTours() {
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		MonitoringLocationsAffectedToManyToursDTO detailDTO = null;
		List<Map<String, String>> sqlResults = salesLocationsDAO.findLocationAffectedToManyTours();
		String oldLocationId = null;
		String tournee = null;
		String dateDebut = null;
		String dateFin = null;
		for (Map<String, String> locationTournee : sqlResults) {

			String location_id = getValueAsString(locationTournee, "location_id");
			String location = getValueAsString(locationTournee, "location_name");
			if (oldLocationId == null) {
				detailDTO = new MonitoringLocationsAffectedToManyToursDTO(
						MonitoringMagasinsDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE);
				detailDTO.setLocation(location);
				detailDTO.addAlerte(MonitoringMagasinsDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE);
				oldLocationId = location_id;
			} else if (!oldLocationId.equals(location_id)) {
				oldLocationId = location_id;
				detailDTO.setLocation(location);
				detailDTO.setDateDebut(dateDebut);
				detailDTO.setDateFin(dateFin);
				detailsDTO.add(detailDTO);
				detailDTO = new MonitoringLocationsAffectedToManyToursDTO(
						MonitoringMagasinsDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE);
				detailDTO.setLocation(location);
				detailDTO.addAlerte(MonitoringMagasinsDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE);
				tournee = null;
				dateDebut = null;
				dateFin = null;
			}

			String tour = getValueAsString(locationTournee, "tournee_name");
			String dDebut = DateHelper.format((Timestamp.valueOf(getValueAsString(locationTournee, "debut"))),
					DateHelper.DD_MM_YYYY_HH_MM_SS);
			String dFin = DateHelper.format((Timestamp.valueOf(getValueAsString(locationTournee, "fin"))),
					DateHelper.DD_MM_YYYY_HH_MM_SS);

			if (tournee == null && dateDebut == null && dateFin == null) {
				tournee = tour;
				dateDebut = dDebut;
				dateFin = dFin;
			} else {
				tournee += "</br>" + tour;
				dateDebut += "</br>" + dDebut;
				dateFin += "</br>" + dFin;
			}
		}
		if (detailDTO != null) {
			detailDTO.setTournee(tournee);
			detailDTO.setDateDebut(dateDebut);
			detailDTO.setDateFin(dateFin);
			detailsDTO.add(detailDTO);
		}
		return detailsDTO;
	}

	private List<MonitoringDetailDTO> validateDataTourWhithoutLocation() {
		List<MonitoringDetailDTO> detailsDTO = new ArrayList<MonitoringDetailDTO>();
		MonitoringTourWhithoutLocationDTO detailDTO = null;
		List<Tours> tours = toursDAO.findAll();
		for (Tours tour : tours) {
			List<ToursLocations> ToursLocations = toursLocationService.findCurrent(null, String.valueOf(tour.getId()),
					new Date(), null);
			if (ToursLocations != null && ToursLocations.size() == 0) {
				detailDTO = new MonitoringTourWhithoutLocationDTO(tour.getName(),
						MonitoringMagasinsDetailDTO.Type.TOURS_WITHOUT_LOCATION);
				detailsDTO.add(detailDTO);
			}

		}
		return detailsDTO;
	}

	private Date getLastDateOfDelivery(String id) {
		JourLivraison location = monitoringConfig.getCalendrierLivraisons().get(id);
		if (location != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_WEEK, location.getJour());
			calendar.add(Calendar.DATE, -7);
			if (location.isSemainePaire() != null
					&& ((DateHelper.weekOfYear(calendar.getTime()) % 2 != 0 && location.isSemainePaire())
							|| (DateHelper.weekOfYear(calendar.getTime()) % 2 == 0 && !location.isSemainePaire()))) {
				calendar.add(Calendar.DATE, -7);
			}
			return calendar.getTime();
		}
		return null;

	}

	private HashMap<String, String> getDestinataireByType() {
		if (destinatairesByType == null) {
			destinatairesByType = new LinkedHashMap<String, String>();
			destinatairesByType.put(MonitoringDetailDTO.Type.VENTES_MAGASINS,
					monitoringConfig.getDestinataires().getMagasins());
			destinatairesByType.put(MonitoringDetailDTO.Type.VENTES_CAMIONS,
					monitoringConfig.getDestinataires().getCamions());
			destinatairesByType.put(MonitoringDetailDTO.Type.CATALOGUE,
					monitoringConfig.getDestinataires().getCatalogues());
			destinatairesByType.put(MonitoringDetailDTO.Type.DEBALLAGES,
					monitoringConfig.getDestinataires().getDeballages());
			destinatairesByType.put(MonitoringDetailDTO.Type.DEBALLAGES_NON_CLOTURES,
					monitoringConfig.getDestinataires().getCaisseNonFermee());
			destinatairesByType.put(MonitoringDetailDTO.Type.COMMANDES_SANS_CLIENT,
					monitoringConfig.getDestinataires().getCommandesSansClient());
			destinatairesByType.put(MonitoringDetailDTO.Type.AFFECTATIONS_MULTIPLES_POINT_VENTE,
					monitoringConfig.getDestinataires().getTourneePlusieursAffectations());
			destinatairesByType.put(MonitoringDetailDTO.Type.TOURS_WITHOUT_LOCATION,
					monitoringConfig.getDestinataires().getTourneeSansCamion());
			destinatairesByType.put(MonitoringDetailDTO.Type.COMMANDES_NON_RECEPTIONNEES,
					monitoringConfig.getDestinataires().getCommandeNonRecue());
		}
		return destinatairesByType;
	}

	private String getValueAsString(Map<String, String> map, String key) {
		if (map.containsKey(key)) {
			if (map.get(key) != null) {
				return String.valueOf(map.get(key));
			}
		}
		return null;
	}

}
