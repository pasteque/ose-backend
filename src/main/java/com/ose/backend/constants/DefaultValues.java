package com.ose.backend.constants;

public final class DefaultValues {    


    public final class TariffAreas {
        public static final String ORDER_CATALOGUE_PAPIER = "100";
        public static final String ORDER_CATALOGUE_SOLDES = "50";
        public static final String ORDER_CATALOGUE_PROMOS = "400";
    }

    public final class Categories {
        public static final String MAGASINS = "Magasins";
        public static final String IDMAGASINS = "1000";
        public static final String CAMIONS = "Camions";
        public static final String IDCAMIONS = "2000";
    }

}
