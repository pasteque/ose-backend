package com.ose.backend.constants;

public final class RequestMappingNames {    
    
    
public final class Tournees {
    public static final String TOURID = "TourId";
    public static final String TOURNAME = "TourName";
}

public final class Locations {
    public static final String LOCATIONID = "LocationId";
    public static final String LOCATIONNAME = "LocationName";
}

public final class Tickets {
    public static final String TICKETID = "TicketId";
    public static final String TICKETDATE = "TicketDate";
}
    
}
