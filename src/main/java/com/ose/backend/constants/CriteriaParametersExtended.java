package com.ose.backend.constants;

public final class CriteriaParametersExtended {    
    //Pour les JobsLogs
    public static final String JOB = "job";
    public static final String LEVEL = "level";
    
    //Pour les Jobs
    public static final String SUFFIXE_ENVOI = "Envoi";
    public static final String SUFFIXE_INTEGRATION = "Integ";
    public static final String SUFFIXE_RECEPTION = "Recep";
    public static final String DESTINATION = "destination";
    public static final String SENS = "sens";
    public static final String TYPE = "type";
    public static final String ETAT = "etat";
    
    
}
