package com.ose.backend.config;

import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "monitoring")
public class Monitoring {

    public static class SMTP {

        private String host = "smtp.gmail.com";
        private String port = "587";
        private String login = "serinfose@gmail.com";
        private String password = "-8sc$@eAhO";
        
        public String getHost() {
            return host;
        }
        public void setHost(String host) {
            this.host = host;
        }
        public String getPort() {
            return port;
        }
        public void setPort(String port) {
            this.port = port;
        }
        public String getLogin() {
            return login;
        }
        public void setLogin(String login) {
            this.login = login;
        }
        public String getPassword() {
            return password;
        }
        public void setPassword(String password) {
            this.password = password;
        }
        
    }
    
    public static class Destinataires {
        
        private String defaut ="serinfose@gmail.com";
        
        private String magasins;
        
        private String camions;
        
        private String catalogues;
        
        private String deballages;
        
        private String caisseNonFermee;
        
        private String commandesSansClient;
        
        private String tourneePlusieursAffectations;

        private String tourneeSansCamion;
        
        private String commandeNonRecue;

        public String getDefaut() {
            return defaut;
        }

        public void setDefaut(String defaut) {
            this.defaut = defaut;
        }

        public String getMagasins() {
            return magasins == null ? defaut : magasins;
        }

        public void setMagasins(String magasins) {
            this.magasins = magasins;
        }

        public String getCamions() {
            return camions  == null ? defaut : camions;
        }

        public void setCamions(String camions) {
            this.camions = camions;
        }

        public String getCatalogues() {
            return catalogues  == null ? defaut : catalogues;
        }

        public void setCatalogues(String catalogues) {
            this.catalogues = catalogues;
        }

        public String getDeballages() {
            return deballages  == null ? defaut : deballages;
        }

        public void setDeballages(String deballages) {
            this.deballages = deballages;
        }

        public String getCaisseNonFermee() {
            return caisseNonFermee  == null ? defaut : caisseNonFermee;
        }

        public void setCaisseNonFermee(String caisseNonFermee) {
            this.caisseNonFermee = caisseNonFermee;
        }

        public String getCommandesSansClient() {
            return commandesSansClient  == null ? defaut : commandesSansClient;
        }

        public void setCommandesSansClient(String commandesSansClient) {
            this.commandesSansClient = commandesSansClient;
        }

        public String getTourneePlusieursAffectations() {
            return tourneePlusieursAffectations  == null ? defaut : tourneePlusieursAffectations;
        }

        public void setTourneePlusieursAffectations(
                String tourneePlusieursAffectations) {
            this.tourneePlusieursAffectations = tourneePlusieursAffectations;
        }

        public String getTourneeSansCamion() {
            return tourneeSansCamion  == null ? defaut : tourneeSansCamion;
        }

        public void setTourneeSansCamion(String tourneeSansCamion) {
            this.tourneeSansCamion = tourneeSansCamion;
        }

        public String getCommandeNonRecue() {
            return commandeNonRecue  == null ? defaut : commandeNonRecue;
        }

        public void setCommandeNonRecue(String commandeNonRecue) {
            this.commandeNonRecue = commandeNonRecue;
        }
        
        
    }
    
    public static class JourLivraison {
        private int jour = 1; //Dimanche
        private Boolean semainePaire = null; //Toutes les semaines
        
        public JourLivraison(int jour, Boolean object) {
            this.jour = jour;
            this.semainePaire = object;
        }
        public JourLivraison() {
            // TODO Auto-generated constructor stub
        }
        public int getJour() {
            return jour;
        }
        public void setJour(int jour) {
            this.jour = jour;
        }
        public Boolean isSemainePaire() {
            return semainePaire;
        }
        public void setSemainePaire(Boolean semainePaire) {
            this.semainePaire = semainePaire;
        }
        
        
    }
    
    private SMTP smtp = new SMTP();
    private Destinataires destinataires = new Destinataires();
    private Map<String, JourLivraison> calendrierLivraisons ;
//    private Map<String, JourLivraison> calendrierLivraisonsDefaut  = Stream.of(new Object[][] {
//        //CAMIONS
//        {"1", new JourLivraison(Calendar.MONDAY, null)}, // => Calendar.MONDAY = 2
//        {"15", new JourLivraison(Calendar.MONDAY, null)},
//        {"5", new JourLivraison(Calendar.MONDAY, null)},
//        {"21", new JourLivraison(Calendar.MONDAY, null)},
//        {"19", new JourLivraison(Calendar.MONDAY, null)},
//
//        {"11", new JourLivraison(Calendar.TUESDAY, null)},// => Calendar.TUESDAY = 3
//        {"8", new JourLivraison(Calendar.TUESDAY, null)},
//        {"13", new JourLivraison(Calendar.TUESDAY, null)},
//        {"18", new JourLivraison(Calendar.TUESDAY, null)},
//
//        {"2", new JourLivraison(Calendar.WEDNESDAY, null)},// => Calendar.WEDNESDAY = 4
//        {"12", new JourLivraison(Calendar.WEDNESDAY, null)},
//        {"17", new JourLivraison(Calendar.WEDNESDAY, null)},
//        {"9", new JourLivraison(Calendar.WEDNESDAY, null)},
//        {"16", new JourLivraison(Calendar.WEDNESDAY, null)},
//
//        {"3", new JourLivraison(Calendar.THURSDAY, null)},//  => Calendar.THURSDAY = 5
//        {"4", new JourLivraison(Calendar.THURSDAY, null)},
//        {"10", new JourLivraison(Calendar.THURSDAY, null)},
//        {"20", new JourLivraison(Calendar.THURSDAY, null)},
//        {"22", new JourLivraison(Calendar.THURSDAY, null)},
//
//        {"7", new JourLivraison(Calendar.FRIDAY, null)}, // => Calendar.FRIDAY = 6
//        {"6", new JourLivraison(Calendar.FRIDAY, null)},
//        {"14",new JourLivraison(Calendar.FRIDAY, null)},
//
//        //MAGASINS
//        {"1013", new JourLivraison(Calendar.MONDAY, null)},// => Calendar.MONDAY = 2
//        {"1014", new JourLivraison(Calendar.MONDAY, null)},
//        {"1020", new JourLivraison(Calendar.MONDAY, null)},
//
//        {"1002", new JourLivraison(Calendar.TUESDAY, null)},// => Calendar.TUESDAY = 3
//        {"1006", new JourLivraison(Calendar.TUESDAY, null)},
//        {"1018", new JourLivraison(Calendar.TUESDAY, false)},
//
//        {"1003", new JourLivraison(Calendar.WEDNESDAY, null)},// => Calendar.WEDNESDAY = 4
//        {"1005", new JourLivraison(Calendar.WEDNESDAY, null)},
//        {"1015", new JourLivraison(Calendar.WEDNESDAY, true)},
//
//        {"1008", new JourLivraison(Calendar.THURSDAY, true)},//  => Calendar.THURSDAY = 5
//        {"1009", new JourLivraison(Calendar.THURSDAY, false)},
//        {"1010", new JourLivraison(Calendar.THURSDAY, true)},
//        {"1012", new JourLivraison(Calendar.THURSDAY, false)},
//        {"1004", new JourLivraison(Calendar.THURSDAY, null)},
//
//        {"1001", new JourLivraison(Calendar.FRIDAY, null)},// => Calendar.FRIDAY = 6
//        {"1017", new JourLivraison(Calendar.FRIDAY, null)},
//    }).collect(Collectors.toMap(data -> (String) data[0], data -> (JourLivraison) data[1]));
    
    private String cron = "-";

    public SMTP getSmtp() {
        return smtp;
    }

    public void setSmtp(SMTP smtp) {
        this.smtp = smtp;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Destinataires getDestinataires() {
        return destinataires;
    }

    public void setDestinataires(Destinataires destinataires) {
        this.destinataires = destinataires;
    }

    public Map<String, JourLivraison> getCalendrierLivraisons() {
        return calendrierLivraisons;
    }

    public void setCalendrierLivraisons(Map<String, JourLivraison> calendrierLivraisons) {
        this.calendrierLivraisons = calendrierLivraisons;
    }
    
}
