package com.ose.backend.model.orders;

public class OrdersManager {

    private Orders orders;

    public OrdersManager(Orders orders) {
        this.orders = orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public OrdersDetails getOrdersDetails(String productId) {
        for (OrdersDetails ordersDetails : orders.getOrdersDetails()) {
            if (ordersDetails.getProduct().getId().equals(productId)) {
                return ordersDetails;
            }
        }
        return null;
    }
  

    
}
