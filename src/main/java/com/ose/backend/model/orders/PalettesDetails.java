package com.ose.backend.model.orders;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.security.People;

import com.ose.backend.dto.orders.PaletteDetailDTO;

@Entity
@Table(name = "PALETTES_DETAIL")
@Cacheable(value = false)
public class PalettesDetails implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String PALETTE = "palette";
        public static final String ORDER_DETAIL = "orderDetail";

        public static final String QUANTITY_TO_PREPARE = "quantityToPrepare";
        public static final String QUANTITY_SENT = "quantitySent";
        public static final String QUANTITY_RECEIVED = "quantityReceived";

        public static final String STATUS = "status";
        public static final String STOCK_LOCATION_NAME = "stockLocationName";
        public static final String NOTE = "note";
        public static final String PREPARATION_DATE = "preparationDate";
        public static final String PREPARATION_USER = "preparationUser";
        public static final String INPUT_ORIGINE = "inputOrigine";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "PALETTE_ID")
    private Palettes palette;
    @ManyToOne
    @JoinColumn(name = "ORDER_DETAIL_ID")
    private OrdersDetails orderDetail;

    @Column(name = "QUANTITY_TO_PREPARE")
    private double quantityToPrepare;
    @Column(name = "QUANTITY_PREPARED")
    private double quantityPrepared;
    @Column(name = "QUANTITY_SENT")
    private double quantitySent;
    @Column(name = "QUANTITY_RECEIVED")
    private double quantityReceived;

    private String status;
    @Column(name = "STOCK_LOCATION_NAME")
    private String stockLocationName;
    private String note;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DPREPARATION")
    private Date preparationDate;
    @ManyToOne
    @JoinColumn(name = "USER_PREPARATION")
    private People preparationUser;
    
    @Column(name = "INPUT_ORIGINE")
    private String  inputOrigine;
    
    public PalettesDetails() {
        this.status = Orders.Status.PREPARATION;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Palettes getPalette() {
        return palette;
    }

    public void setPalette(Palettes palette) {
        this.palette = palette;
    }

    public OrdersDetails getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(OrdersDetails orderDetail) {
        this.orderDetail = orderDetail;
    }

    public double getQuantityToPrepare() {
        return quantityToPrepare;
    }

    public void setQuantityToPrepare(double quantityToPrepare) {
        this.quantityToPrepare = quantityToPrepare;
    }

    public double getQuantityPrepared() {
        return quantityPrepared;
    }

    public void setQuantityPrepared(double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }

    public double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStockLocationName() {
        return stockLocationName;
    }

    public void setStockLocationName(String stockLocationName) {
        this.stockLocationName = stockLocationName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(Date preparationDate) {
        this.preparationDate = preparationDate;
    }

    public People getPreparationUser() {
        return preparationUser;
    }

    public void setPreparationUser(People preparationUser) {
        this.preparationUser = preparationUser;
    }

    public String getInputOrigine() {
		return inputOrigine;
	}

	public void setInputOrigine(String inputOrigine) {
		this.inputOrigine = inputOrigine;
	}

	public void createId() {
        this.setId(String.format("%d-%s", new Date().getTime(), this.getOrderDetail() == null ? "" : this.getOrderDetail().getId()));
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PaletteDetailDTO.class)) {
            PaletteDetailDTO dto = new PaletteDetailDTO();
            dto.setId(getId());
            if (getOrderDetail() != null) {
                dto.setOrderDetailId(getOrderDetail().getId());
                dto.setProductCode(getOrderDetail().getProduct().getId());
                dto.setProductName(getOrderDetail().getProduct().getName());
                dto.setQuantityFromOrderDetail(getOrderDetail().getQuantityAsked());
            }
            
            dto.setPreparationDate(getPreparationDate());
            if (getPreparationUser() != null) {
                dto.setPreparationUser(getPreparationUser().getAdapter(POSUsersDTO.class));
            }
            
            dto.setQuantityToPrepare(getQuantityToPrepare());
            dto.setQuantityPrepared(getQuantityPrepared());
            dto.setQuantitySent(getQuantitySent());
            dto.setQuantityReceived(getQuantityReceived());
            dto.setStockLocationName(getStockLocationName());
            dto.setInputOrigine(getInputOrigine());
            dto.setNote(getNote());
            dto.setStatus(getStatus());
            dto.setPaletteId(getPalette().getId());
            if (Integer.parseInt(getStatus()) > Integer.parseInt(getPalette().getStatus())) {
                dto.setClose(true);
            } else {
                dto.setClose(false);
            }
            return (T) dto;
        }
        return null;
    }

}
