package com.ose.backend.model.orders;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.model.adaptable.AdaptableHelper;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.security.People;

import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.orders.PaletteDetailDTO;
import com.ose.backend.dto.orders.PaletteInfoDTO;
import com.ose.backend.dto.orders.PaletteStatusInfoDTO;
import com.ose.backend.service.orders.impl.OrdersService;

@Entity
@Cacheable(value = false)
public class Palettes implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CREATION_DATE = "creationDate";
        public static final String VALIDATION_DATE = "planificationDate";
        public static final String PREPARATION_DATE = "preparationDate";
        public static final String CONTROL_DATE = "controleDate";
        public static final String EXPEDITION_DATE = "expeditionDate";
        public static final String RECEPTION_DATE = "receptionDate";
        public static final String CREATION_USER = "creationUser";
        public static final String VALIDATION_USER = "validationUser";
        public static final String PREPARATION_USER = "preparationUser";
        public static final String RECEPTION_USER = "receptionUser";
        public static final String STATUS = "status";
        public static final String NOTE = "note";

        public static final String ORDER = "order";
        public static final String PALETTES_DETAILS = "palettesDetails";
    }

//Historique - On se réfère désormais systématiquement aux Orders.Status    
//    public final class Status {
//        public static final String CREATION = "1";
//        public static final String VALIDATION = "2";
//        public static final String PREPARATION = "3";
//        public static final String RECEPTION = "4";
//    }

    @Id
    private String id;
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DCREATION")
    private Date creationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DPLANIFICATION")
    private Date planificationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DPREPARATION")
    private Date preparationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DCONTROLE")
    private Date controleDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DEXPEDITION")
    private Date expeditionDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DRECEPTION")
    private Date receptionDate;
    @ManyToOne
    @JoinColumn(name = "USER_CREATION")
    private People creationUser;
    @ManyToOne
    @JoinColumn(name = "USER_PLANIFICATION")
    private People planificationUser;
    @ManyToOne
    @JoinColumn(name = "USER_PREPARATION")
    private People preparationUser;
    @ManyToOne
    @JoinColumn(name = "USER_CONTROLE")
    private People controleUser;
    @ManyToOne
    @JoinColumn(name = "USER_EXPEDITION")
    private People expeditionUser;
    @ManyToOne
    @JoinColumn(name = "USER_RECEPTION")
    private People receptionUser;
    private String status;
    private String note;

    @ManyToOne
    @JoinColumn(name = "ORDER_ID")
    private Orders order;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = PalettesDetails.Fields.PALETTE, orphanRemoval = true)
    private List<PalettesDetails> palettesDetails;

    public Palettes() {
        this.status = Orders.Status.PREPARATION;
        this.palettesDetails = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getPlanificationDate() {
        return planificationDate;
    }

    public void setPlanificationDate(Date planificationDate) {
        this.planificationDate = planificationDate;
    }

    public Date getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(Date preparationDate) {
        this.preparationDate = preparationDate;
    }

    public Date getControleDate() {
        return controleDate == null ? order.getControleDate(): controleDate;
    }

    public void setControleDate(Date controleDate) {
        this.controleDate = controleDate;
    }

    public Date getExpeditionDate() {
        return expeditionDate == null? order.getExpeditionDate() : expeditionDate;
    }

    public void setExpeditionDate(Date expeditionDate) {
    	if (this.expeditionDate == null){
    		this.expeditionDate = expeditionDate;
    	}
    }

    public Date getReceptionDate() {
        return receptionDate == null ? order.getReceptionDate() : receptionDate;
    }

    public void setReceptionDate(Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    public People getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(People creationUser) {
        this.creationUser = creationUser;
    }

    public People getPlanificationUser() {
        return planificationUser;
    }

    public void setPlanificationUser(People planificationUser) {
        this.planificationUser = planificationUser;
    }

    public People getPreparationUser() {
        return preparationUser;
    }

    public void setPreparationUser(People preparationUser) {
        this.preparationUser = preparationUser;
    }

    public People getControleUser() {
        return controleUser  == null ? order.getControleUser() : controleUser;
    }

    public void setControleUser(People controleUser) {
        this.controleUser = controleUser;
    }

    public People getExpeditionUser() {
        return expeditionUser == null ? order.getExpeditionUser() : expeditionUser;
    }

    public void setExpeditionUser(People expeditionUser) {
        this.expeditionUser = expeditionUser;
    }

    public People getReceptionUser() {
        return receptionUser == null ? order.getReceptionUser() : receptionUser;
    }

    public void setReceptionUser(People receptionUser) {
        this.receptionUser = receptionUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    public List<PalettesDetails> getSortedPalettesDetails() {
        Comparator<PalettesDetails> byProductLocation = new Comparator<PalettesDetails>() {
            @Override
            public int compare(PalettesDetails object1, PalettesDetails object2) {
                return object1.getOrderDetail().getProduct().getProductLocation(getOrder().getLocationFromId())
                        .compareTo(object2.getOrderDetail().getProduct().getProductLocation(getOrder().getLocationFromId()));
            }
        };
        List<PalettesDetails> orderedPalettesDetails = palettesDetails.stream().sorted(byProductLocation).collect(Collectors.toList());
        return orderedPalettesDetails;
    }

    public List<PalettesDetails> getPalettesDetails() {
        return palettesDetails;
    }

    public void setPalettesDetails(List<PalettesDetails> palettesDetails) {
        this.palettesDetails = palettesDetails;
    }

    public void createId() {
        this.setId(OrdersService.createPaletteId(this.getOrder() == null ? "" : this.getOrder().getId()));
    }

    public PalettesManager getPalettesManager() {
        return new PalettesManager(this);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PaletteDTO.class)) {
            PaletteDTO dto = new PaletteDTO();
            dto.setId(getId());
            dto.setName(getName());
            dto.setNote(getNote());
            dto.setStatus(Orders.Status.getStatusLabel(getStatus()));
            dto.setStatusOrder(Integer.parseInt(getStatus()));
            if (getOrder() != null) {
                dto.setOrderId(getOrder().getId());
                dto.setDestinationName(getOrder().getLocationTo().getName());
            }
            dto.setCreationDate(getCreationDate());
            if (getCreationUser() != null) {
                dto.setCreationUser(getCreationUser().getAdapter(POSUsersDTO.class));
            }
            dto.setPlanificationDate(getPlanificationDate());
            if (getPlanificationUser() != null) {
                dto.setPlanificationUser(getPlanificationUser().getAdapter(POSUsersDTO.class));
            }
            dto.setPreparationDate(getPreparationDate());
            if (getPreparationUser() != null) {
                dto.setPreparationUser(getPreparationUser().getAdapter(POSUsersDTO.class));
            }
            dto.setControlDate(getControleDate());
            if (getControleUser() != null) {
                dto.setControlUserId(getControleUser().getId());
            }
            dto.setDelivDate(getExpeditionDate());
            if (getExpeditionUser() != null) {
                dto.setDelivUserId(getExpeditionUser().getId());
            }
            dto.setReceptDate(getReceptionDate());
            if (getReceptionUser() != null) {
                dto.setReceptUserId(getReceptionUser().getId());
            }
            dto.setPaletteDetails(AdaptableHelper.getAdapter(getSortedPalettesDetails(), PaletteDetailDTO.class));
            return (T) dto;
        } else if (selector.equals(PaletteStatusInfoDTO.class)) {
            PaletteStatusInfoDTO dto = new PaletteStatusInfoDTO();
            dto.setId(getId());
            dto.setStatusOrder(getStatus());
            dto.setName(getName());
            if (getPreparationUser() != null) {
                dto.setPreparationUsername(getPreparationUser().getUsername());
            }
            dto.setNbLines(getNbProductLocation(null));
            dto.setNbLinesLeft(dto.getNbLines() - getNbProductLocation(Orders.Status.CONTROLE));
            return (T) dto;
        } else if (selector.equals(PaletteInfoDTO.class)) {
            PaletteInfoDTO dto = new PaletteInfoDTO();
            dto.setId(getId());
            dto.setName(getName());
            return (T) dto;
        }
        return null;
    }

    private int getNbProductLocation(String status) {
        List<String> places = new ArrayList<>();
        for (PalettesDetails palettesDetails : getPalettesDetails()) {
            if (!places.contains(palettesDetails.getStockLocationName())) {
                if (status != null) {
                    if (palettesDetails.getStatus().equals(status)) {
                        places.add(palettesDetails.getStockLocationName());
                    }
                } else {
                    places.add(palettesDetails.getStockLocationName());
                }
            }
        }
        return places.size();
    }

    public PalettesDetails getPaletteDetail(String productId) {
        for (PalettesDetails palettesDetails : getPalettesDetails()) {
            if (palettesDetails.getOrderDetail().getProduct().getId().equals(productId)) {
                return palettesDetails;
            }
        }
        return null;
    }

    public String getOrderId() {
        return getOrder() == null ? null : getOrder().getId();
    }

    public String getPreparationUserId() {
        return getPreparationUser() == null ? "" : getPreparationUser().getId();
    }
}
