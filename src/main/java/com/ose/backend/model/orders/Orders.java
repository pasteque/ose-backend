package com.ose.backend.model.orders;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

import com.ose.backend.dto.orders.OrderListDTO;
import com.ose.backend.service.orders.impl.OrdersService;



@Entity
@Cacheable(value = false)
public class Orders implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CREATION_DATE = "creationDate";
        public static final String PLANIFICATION_DATE = "planificationDate";
        public static final String PREPARATION_DATE = "preparationDate";
        public static final String RECEPTION_DATE = "receptionDate";
        public static final String CREATION_USER = "creationUser";
        public static final String PREPARATION_USER = "preparationUser";
        public static final String RECEPTION_USER = "receptionUser";
        public static final String STATUS = "status";
        public static final String LOCATION_FROM = "locationFrom";
        public static final String LOCATION_TO = "locationTo";
        public static final String NOTE = "note";

        public static final String ORDERS_DETAILS = "ordersDetails";
        public static final String PALETTES = "palettes";
    }
    
    public final class Columns {
        public static final String ID = "ID";
        public static final String NAME = "NAME";
        public static final String CREATION_DATE = "DCREATION";
        public static final String PLANIFICATION_DATE = "DPLANIFICATION";
        public static final String PREPARATION_DATE = "DPREPARATION";
        public static final String RECEPTION_DATE = "DRECEPTION";
        public static final String EXPEDITION_DATE = "DEXPEDITION";
        public static final String CONTROLE_DATE = "DCONTROLE";
        public static final String CREATION_USER = "USER_CREATION";
        public static final String PREPARATION_USER = "USER_PREPARATION";
        public static final String RECEPTION_USER = "USER_RECEPTION";
        public static final String STATUS = "STATUS";
        public static final String LOCATION_FROM = "LOCATION_FROM_ID";
        public static final String LOCATION_TO = "LOCATION_TO_ID";
        public static final String NOTE = "NOTE";
        public static final String PLANIFICATION_USER =  "USER_PLANIFICATION";
        public static final String CONTROLE_USER =  "USER_CONTROLE";
        public static final String EXPEDITION_USER = "USER_EXPEDITION";
    }

    public static final Long TEMPS_MAXI_PREPARATION_MN = 15L;

    public static final class Status {
        public static final String CREATION = "1";
        public static final String PLANIFICATION = "2";
        public static final String PREPARATION = "3";
        public static final String CONTROLE = "4";
        public static final String EXPEDITION = "5";
        public static final String RECEPTED = "6";

        public static final String SCREATION = "Création";
        public static final String SPLANIFICATION = "Planification";
        public static final String SPREPARATION = "Préparation";
        public static final String SCONTROLE = "Contrôle";
        public static final String SEXPEDITION = "Expédition";
        public static final String SRECEPTED = "Réceptionnée";

        public static String getStatusLabel(String status) {
            if (status.equals(Orders.Status.CREATION)) {
                return Orders.Status.SCREATION;
            } else if (status.equals(Orders.Status.PLANIFICATION)) {
                return Orders.Status.SPLANIFICATION;
            } else if (status.equals(Orders.Status.PREPARATION)) {
                return Orders.Status.SPREPARATION;
            } else if (status.equals(Orders.Status.CONTROLE)) {
                return Orders.Status.SCONTROLE;
            } else if (status.equals(Orders.Status.EXPEDITION)) {
                return Orders.Status.SEXPEDITION;
            } else if (status.equals(Orders.Status.RECEPTED)) {
                return Orders.Status.SRECEPTED;
            }
            return null;
        }

        public static String getStatusFromLabel(String status) {
            if (status.equals(Orders.Status.SCREATION)) {
                return Orders.Status.CREATION;
            } else if (status.equals(Orders.Status.SPLANIFICATION)) {
                return Orders.Status.PLANIFICATION;
            } else if (status.equals(Orders.Status.SPREPARATION)) {
                return Orders.Status.PREPARATION;
            } else if (status.equals(Orders.Status.SCONTROLE)) {
                return Orders.Status.CONTROLE;
            } else if (status.equals(Orders.Status.SEXPEDITION)) {
                return Orders.Status.EXPEDITION;
            } else if (status.equals(Orders.Status.SRECEPTED)) {
                return Orders.Status.RECEPTED;
            }
            return null;
        }
    }

    @Id
    private String id;
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.CREATION_DATE)
    private Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.PLANIFICATION_DATE)
    private Date planificationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.PREPARATION_DATE)
    private Date preparationDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.CONTROLE_DATE)
    private Date controleDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.EXPEDITION_DATE)
    private Date expeditionDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = Columns.RECEPTION_DATE)
    private Date receptionDate;

    @ManyToOne
    @JoinColumn(name = Columns.CREATION_USER)
    private People creationUser;
    @ManyToOne
    @JoinColumn(name = Columns.PLANIFICATION_USER)
    private People planificationUser;
    @ManyToOne
    @JoinColumn(name = Columns.PREPARATION_USER)
    private People preparationUser;
    @ManyToOne
    @JoinColumn(name = Columns.CONTROLE_USER)
    private People controleUser;
    @ManyToOne
    @JoinColumn(name = Columns.EXPEDITION_USER)
    private People expeditionUser;
    @ManyToOne
    @JoinColumn(name = Columns.RECEPTION_USER)
    private People receptionUser;

    private String status;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = Columns.LOCATION_FROM)
    private Locations locationFrom;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = Columns.LOCATION_TO)
    private Locations locationTo;
    private String note;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = OrdersDetails.Fields.ORDER, orphanRemoval = true)
    private List<OrdersDetails> ordersDetails;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = Palettes.Fields.ORDER, orphanRemoval = true)
    private List<Palettes> palettes;

    public Orders() {
        this.creationDate = new Date();
        this.ordersDetails = new ArrayList<>();
        this.palettes = new ArrayList<>();
        this.status = Orders.Status.CREATION;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getPlanificationDate() {
        return planificationDate;
    }

    public void setPlanificationDate(Date planificationDate) {
        this.planificationDate = planificationDate;
    }

    public Date getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(Date preparationDate) {
        this.preparationDate = preparationDate;
    }

    public Date getControleDate() {
        return controleDate;
    }

    public void setControleDate(Date controleDate) {
        this.controleDate = controleDate;
    }

    public Date getExpeditionDate() {
        return expeditionDate;
    }

    public void setExpeditionDate(Date expeditionDate) {
        this.expeditionDate = expeditionDate;
    }

    public Date getReceptionDate() {
        return receptionDate;
    }

    public void setReceptionDate(Date receptionDate) {
        this.receptionDate = receptionDate;
    }

    public People getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(People creationUser) {
        this.creationUser = creationUser;
    }

    public People getPlanificationUser() {
        return planificationUser;
    }

    public void setPlanificationUser(People planificationUser) {
        this.planificationUser = planificationUser;
    }

    public People getPreparationUser() {
        return preparationUser;
    }

    public void setPreparationUser(People preparationUser) {
        this.preparationUser = preparationUser;
    }

    public People getControleUser() {
        return controleUser;
    }

    public void setControleUser(People controleUser) {
        this.controleUser = controleUser;
    }

    public People getExpeditionUser() {
        return expeditionUser;
    }

    public void setExpeditionUser(People expeditionUser) {
        this.expeditionUser = expeditionUser;
    }

    public People getReceptionUser() {
        return receptionUser;
    }

    public void setReceptionUser(People receptionUser) {
        this.receptionUser = receptionUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Locations getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(Locations locationFrom) {
        this.locationFrom = locationFrom;
    }

    public Locations getLocationTo() {
        return locationTo;
    }

    public void setLocationTo(Locations locationTo) {
        this.locationTo = locationTo;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<OrdersDetails> getOrdersDetails() {
        return ordersDetails;
    }

    public List<OrdersDetails> getSortedOrdersDetails() {
        Comparator<OrdersDetails> byProductLocation = new Comparator<OrdersDetails>() {
            @Override
            public int compare(OrdersDetails object1, OrdersDetails object2) {
                return object1.getProduct().getProductLocation(getLocationFromId())
                        .compareTo(object2.getProduct().getProductLocation(getLocationFromId()));
            }
        };
        ;
        List<OrdersDetails> orderedOrdersDetails = ordersDetails.stream().sorted(byProductLocation).collect(Collectors.toList());
        return orderedOrdersDetails;
    }

    public void setOrdersDetails(List<OrdersDetails> ordersDetails) {
        this.ordersDetails = ordersDetails;
    }

    public OrdersManager getOrdersManager() {
        return new OrdersManager(this);
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(OrderListDTO.class)) {
            OrderListDTO dto = new OrderListDTO();
            dto.setId(getId());
            dto.setCreationDate(getCreationDate());
            dto.setReference(getName());
            if (getLocationTo() != null) {
                dto.setLocationTo(getLocationTo().getName());
            }
            if (getLocationFrom() != null) {
                dto.setLocationFrom(getLocationFrom().getName());
            }
            dto.setCodeStatus(getStatus());
            dto.setStatus(Orders.Status.getStatusLabel(getStatus()));
            return (T) dto;
        }

        return null;
    }

    public String getCreationUserId() {
        return getCreationUser() == null ? "" : getCreationUser().getId();
    }

    public void createId() {
        this.setId(OrdersService.createOrderId( this.getLocationTo() == null ? "" : this.getLocationTo().getId()));
    }

    public List<Palettes> getPalettes() {
        return palettes;
    }

    public void setPalettes(List<Palettes> palettes) {
        this.palettes = palettes;
    }

    public double getQuantityPalettized(String ordersDetailsId) {
        double qty = 0D;
        for (Palettes palettes : getPalettes()) {
            for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                if ( palettesDetails.getOrderDetail().getId().equals(ordersDetailsId)) {
                    if(Integer.parseInt(Orders.Status.PREPARATION) >= Integer.parseInt(palettesDetails.getStatus()) ){
                        qty += palettesDetails.getQuantityToPrepare();
                    } else {
                        qty += palettesDetails.getQuantityPrepared();
                    }
                }
            }
        }
        return qty;
    }

    public double getQuantityPrepared(String ordersDetailsId) {
        double qty = 0D;
        for (Palettes palettes : getPalettes()) {
            for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                if (palettesDetails.getOrderDetail().getId().equals(ordersDetailsId)) {
                    qty += palettesDetails.getQuantityPrepared();
                }
            }
        }
        return qty;
    }

    public double getQuantitySent(String ordersDetailsId) {
        double qty = 0D;
        for (Palettes palettes : getPalettes()) {
            for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                if (palettesDetails.getOrderDetail().getId().equals(ordersDetailsId)) {
                    qty += palettesDetails.getQuantitySent();
                }
            }
        }
        return qty;
    }

    public double getQuantityReceived(String ordersDetailsId) {
        double qty = 0D;
        for (Palettes palettes : getPalettes()) {
            for (PalettesDetails palettesDetails : palettes.getPalettesDetails()) {
                if (palettesDetails.getOrderDetail().getId().equals(ordersDetailsId)) {
                    qty += palettesDetails.getQuantityReceived();
                }
            }
        }
        return qty;
    }

    public String getLocationFromId() {
        return getLocationFrom() == null ? "" : getLocationFrom().getId();
    }

    public String getLocationToId() {
        return getLocationTo() == null ? "" : getLocationTo().getId();
    }

    public String getPlanificationUserId() {
        return getPlanificationUser() == null ? "" : getPlanificationUser().getId();
    }

    public String getPreparationUserId() {
        return getPreparationUser() == null ? "" : getPreparationUser().getId();
    }

    public String getControleUserId() {
        return getControleUser() == null ? "" : getControleUser().getId();
    }

    public String getExpeditionUserId() {
        return getExpeditionUser() == null ? "" : getExpeditionUser().getId();
    }

    public String getReceptionUserId() {
        return getReceptionUser() == null ? "" : getReceptionUser().getId();
    }
}
