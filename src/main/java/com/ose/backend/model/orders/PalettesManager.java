package com.ose.backend.model.orders;

import java.util.List;

public class PalettesManager {

    private Palettes palettes;

    public PalettesManager(Palettes palettes) {
        this.palettes = palettes;
    }

    public PalettesDetails deletePaletteDetails(String paletteDetailId) {
        List<PalettesDetails> paletteDetail = palettes.getPalettesDetails();
        int index = 0;
        PalettesDetails retour = null ;
        
        while( index < paletteDetail.size() && !paletteDetailId.equals(paletteDetail.get(index).getId())) {
                index++ ;
            }
        
        if(index < paletteDetail.size()){
            retour = paletteDetail.get(index) ;
            paletteDetail.remove(index);
        }
        
        palettes.setPalettesDetails(paletteDetail);
        
        return retour;
    }

}
