package com.ose.backend.model.orders;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;


@Entity
@Table(name = "ORDERS_DETAIL")
@Cacheable(value = false)
public class OrdersDetails implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String ORDER = "order";
        public static final String PRODUCT = "product";
        public static final String NOTE = "note";
        public static final String QUANTITY_ASKED = "quantityAsked";
        public static final String QUANTITY_TO_PREPARE = "quantityToPrepare";
        public static final String QUANTITY_PREPARED = "quantityPrepared";
        public static final String QUANTITY_SENT = "quantitySent";
        public static final String QUANTITY_RECEIVED = "quantityReceived";
    }
    
    public final class Columns {
        public static final String ID = "ID";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "ORDERS_ID")
    private Orders order;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinFetch(JoinFetchType.INNER)
    @JoinColumn(name = "PRODUCT_ID")
    private Products product;
    private String note;
    @Column(name = "QUANTITY_ASKED")
    private double quantityAsked;
    @Column(name = "QUANTITY_TO_PREPARE")
    private double quantityToPrepare;
    @Column(name = "QUANTITY_PREPARED")
    private double quantityPrepared;
    @Column(name = "QUANTITY_SENT")
    private double quantitySent;
    @Column(name = "QUANTITY_RECEIVED")
    private double quantityReceived;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Orders getOrder() {
        return order;
    }

    public void setOrder(Orders order) {
        this.order = order;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public double getQuantityAsked() {
        return quantityAsked;
    }

    public void setQuantityAsked(double quantityAsked) {
        this.quantityAsked = quantityAsked;
    }

    public double getQuantityToPrepare() {
        return quantityToPrepare;
    }

    public void setQuantityToPrepare(double quantityToPrepare) {
        this.quantityToPrepare = quantityToPrepare;
    }

    public double getQuantityPrepared() {
        return quantityPrepared;
    }

    public void setQuantityPrepared(double quantityPrepared) {
        this.quantityPrepared = quantityPrepared;
    }

    public double getQuantitySent() {
        return quantitySent;
    }

    public void setQuantitySent(double quantitySent) {
        this.quantitySent = quantitySent;
    }

    public double getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(double quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    public void createId() {
        this.setId(String.format("%d-%s-%s", new Date().getTime(), this.getOrder().getLocationToId(), getProductId()));
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        return null;
    }

    public String getProductId() {
        return getProduct() == null ? "" : getProduct().getId();
    }

}
