package com.ose.backend.model.stocks;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;

import com.ose.backend.dto.stocks.StockLevelDTO;

@Entity
public class StockLevel implements IAdaptable {
    public final class Fields {
        public static final String LOCATION = "locations";
        public static final String PRODUCT = "products";
        public static final String STOCK_SECURITY = "stockSecurity";
        public static final String STOCK_MAXIMUM = "stockMaximum";
        public static final String LAST_UPDATE = "lastUpdate";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "location")
    private Locations locations;
    @ManyToOne
    @JoinColumn(name = "product")
    private Products products;
    private double stockSecurity;
    private double stockMaximum;
    private Date lastUpdate;
    
    public static final String createId(String locationId , String productId) {
        return locationId.concat("-").concat(productId);
    }

    public String getId() {
    	if (this.getLocationsId() !=null && this.getProductsId()!=null) {
    		this.id = createId(this.getLocationsId() , this.getProductsId());
    	}
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public double getStockSecurity() {
        return stockSecurity;
    }

    public void setStockSecurity(double stockSecurity) {
        this.stockSecurity = stockSecurity;
    }

    public double getStockMaximum() {
        return stockMaximum;
    }

    public void setStockMaximum(double stockMaximum) {
        this.stockMaximum = stockMaximum;
    }
    
    public void setId() {
    	if (this.getLocationsId() !=null && this.getProductsId()!=null) {
    		this.id = this.getLocationsId().concat("-").concat(this.getProductsId());
    	}
    }
    
    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(StockLevelDTO.class)) {
            StockLevelDTO stockLevelDTO = new StockLevelDTO();
            stockLevelDTO.setLocationsId(this.getLocations().getId());
            stockLevelDTO.setProductsId(this.getProducts().getId());
            stockLevelDTO.setId(this.getId());
            stockLevelDTO.setStockSecurity(this.getStockSecurity());
            stockLevelDTO.setStockMaximum(this.getStockMaximum());
            return (T) stockLevelDTO;
        }
        return null;
    }

    public String getLocationsId() {
        return (locations == null ? null : locations.getId());
    }

    public String getProductsId() {
        return (products == null ? null : products.getId());
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
