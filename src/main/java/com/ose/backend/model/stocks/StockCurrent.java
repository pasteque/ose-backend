package com.ose.backend.model.stocks;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;

import com.ose.backend.dto.stocks.StockCurrentDTO;
import com.ose.backend.dto.stocks.StockProductDetailDTO;

@Entity
@IdClass(StockCurrentId.class)
public class StockCurrent implements IAdaptable {

    public final class Fields {
        public static final String LOCATION = "locations";
        public static final String PRODUCT = "products";
        public static final String ATTRIBUTE_SET_INSTANCE_ID = "attributeSetInstanceId";
        public static final String UNITS = "units";
    }


    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location")
    private Locations locations;
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product")
    private Products products;
    @Column(name = "attributesetinstance_id")
    private String attributeSetInstanceId;
    private double units;

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(StockCurrentDTO.class)) {
            StockCurrentDTO stockCurrentDTO = new StockCurrentDTO();
            stockCurrentDTO.setLocationsId(this.getLocations().getId());
            stockCurrentDTO.setProductsId(this.getProducts().getId());
            stockCurrentDTO.setAttributeSetInstanceId(this.getAttributeSetInstanceId());
            stockCurrentDTO.setUnits(this.getUnits());
            return (T) stockCurrentDTO;
        }
        if(selector.equals(StockProductDetailDTO.class)) {
            StockProductDetailDTO stockProductDetailDTO = new StockProductDetailDTO();
            stockProductDetailDTO.setCategoryId(this.getProducts().getCategory().getId());
            stockProductDetailDTO.setCategoryLabel(this.getProducts().getCategory().getName());
            stockProductDetailDTO.setPriceBuy(this.getProducts().getPriceBuy() != null ? this.getProducts().getPriceBuy() : 0D);
            stockProductDetailDTO.setPriceStock(this.getProducts().getStockCost());
            stockProductDetailDTO.setProductCode(this.getProducts().getCode());
            stockProductDetailDTO.setProductId(this.getProducts().getId());
            stockProductDetailDTO.setProductLabel(this.getProducts().getName());
            //stockProductDetailDTO.setProductPlace(this.getProducts().);
            stockProductDetailDTO.setUnits(this.getUnits());
            return (T) stockProductDetailDTO;
        }
        return null;
    }

    public String getLocationsId() {
        return (locations == null ? null : locations.getId());
    }

    public String getProductsId() {
        return (products == null ? null : products.getId());
    }

}
