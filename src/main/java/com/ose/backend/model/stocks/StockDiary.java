package com.ose.backend.model.stocks;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

import com.ose.backend.dto.stocks.StockDiaryDTO;

@Entity
@Cacheable(value = false)
public class StockDiary implements IAdaptable {

    public final class Fields {
        public static final String LOCATION = "locations";
        public static final String DESTINATION = "destination";
        public static final String PRODUCT = "products";
        public static final String ATTRIBUTE_SET_INSTANCE_ID = "attributeSetInstanceId";
        public static final String UNITS = "units";
        public static final String PRICE = "price";
        public static final String DATE_NEW = "dateNew";
        public static final String REASON = "reason";
        public static final String NOTE = "note";
        public static final String REFERENCE = "reference";
        public static final String STATE = "state";
        public static final String STARTINGSTOCK = "startingStock";
        public static final String USER = "user";
        public static final String TIMESTAMP = "timeStampCreate";
    }

    public final class Type {
        public static final int REASON_IN_BUY = 1; // Entree en stock
        public static final int REASON_IN_REFUND = 2; // retour client/fournisseur
        public static final int REASON_IN_MOVEMENT = 4; // Transfert interne
        public static final int REASON_IN_INVENTORY = 100; // Correction inventaire + 
        public static final int REASON_IN_TRANSFERT = 1000; // Transfert externe
        public static final int REASON_OUT_SELL = -1; // Vente
        public static final int REASON_OUT_REFUND = -2; // retour client/fournisseur
        public static final int REASON_OUT_BACK = -3; // retour fournisseur ---> Non utilisé
        public static final int REASON_OUT_MOVEMENT = -4; // Transfert interne - au groupe
        public static final int REASON_OUT_INVENTORY = -100; // Correction inventaire - 
        public static final int REASON_OUT_TRANSFERT = -1000; // Transfert dans le même lieu
        public static final int REASON_RESET = 0;
    }

    public final class ReferenceType {
        public static final String TICKET = "T";
        public static final String MOVEMENT = "M";
        public static final String UNKNOW = "U";
    }
    
    @Id
    private String id;
    
    private Date dateNew;
    private int reason;
    @ManyToOne
    @JoinColumn(name = "location")
    private Locations locations;
    @ManyToOne
    @JoinColumn(name = "product")
    private Products products;
    @Column(name = "attributesetinstance_id")
    private String attributeSetInstanceId;
    private double units;
    private double price;
    private String note;
    private String reference;
    private String state;
    @ManyToOne
    @JoinColumn(name = "destination")
    private Locations destination;
    @Column(name = "stock_before")
    private Double startingStock;
    @ManyToOne
    @JoinColumn(name = "USER")
    private People user;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dcreation" , columnDefinition="DATETIME DEFAULT CURRENT_TIMESTAMP(6)" , insertable = false, updatable = false)
    private Date timeStampCreate;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateNew() {
        return dateNew;
    }

    public void setDateNew(Date dateNew) {
        this.dateNew = dateNew;
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReasonLabel() {
        if (reason == StockDiary.Type.REASON_IN_BUY) {
            return "Entree - Stock";
        } else if (reason == StockDiary.Type.REASON_IN_REFUND) {
            return "Entree - Retour client/fournisseur";
        } else if (reason == StockDiary.Type.REASON_IN_MOVEMENT) {
            return "Entree - Transfert groupe";
        } else if (reason == StockDiary.Type.REASON_OUT_SELL) {
            return "Sortie - Vente";
        } else if (reason == StockDiary.Type.REASON_OUT_REFUND) {
            return "Sortie - Sortie client/fournisseur";
        } else if (reason == StockDiary.Type.REASON_OUT_BACK) {
            return "Sortie - Stock";
        } else if (reason == StockDiary.Type.REASON_OUT_MOVEMENT) {
            return "Sortie - Transfert groupe";
        } else if (reason == StockDiary.Type.REASON_OUT_TRANSFERT) {
            return "Sortie - Transfert interne";
        } else if (reason == StockDiary.Type.REASON_RESET) {
            return "Remise à zéro du stock";
        } else if (reason == StockDiary.Type.REASON_IN_TRANSFERT) {
            return "Entree - Transfert interne";
        } else if (reason == StockDiary.Type.REASON_IN_INVENTORY) {
            return "Entree - Correction d'inventaire";
        } else if (reason == StockDiary.Type.REASON_OUT_INVENTORY) {
            return "Sortie - Correction d'inventaire";
        }
            return "Mouvement de stock inconnue";
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(StockDiaryDTO.class)) {
            StockDiaryDTO stockDiaryDTO = new StockDiaryDTO();
            stockDiaryDTO.setLocationsId(this.getLocationsId());
            stockDiaryDTO.setProductsId(this.getProductsId());
            stockDiaryDTO.setAttributeSetInstanceId(this.getAttributeSetInstanceId());
            stockDiaryDTO.setUnits(this.getUnits());
            stockDiaryDTO.setDateNew(this.getDateNew());
            stockDiaryDTO.setId(this.getId());
            stockDiaryDTO.setPrice(this.getPrice());
            stockDiaryDTO.setReason(this.getReason());
            stockDiaryDTO.setNote(this.getNote());
            stockDiaryDTO.setDestination(this.getDestinationId());
            stockDiaryDTO.setReference(this.getReference());
            stockDiaryDTO.setState(this.getState());
            stockDiaryDTO.setStockBefore(this.getStartingStock());
            return (T) stockDiaryDTO;
        }
        return null;
    }

    public String getLocationsId() {
        return (locations == null ? null : locations.getId());
    }
    
    public String getDestinationId() {
        return (destination == null ? null : destination.getId());
    }

    public String getProductsId() {
        return (products == null ? null : products.getId());
    }

    public void createId(Double stock) {
        this.setId(String.format("%d-%f-%s-%s", new Date().getTime(), stock, this.locations == null ? "" : this.locations.getId(),
                this.products == null ? "" : this.products.getId()));

    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Locations getDestination() {
        return destination;
    }

    public void setDestination(Locations destination) {
        this.destination = destination;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getStartingStock() {
        return startingStock;
    }

    public void setStartingStock(Double startingStock) {
        this.startingStock = startingStock;
    }

    public void setEndingStock(Double endingStock) {
        if(endingStock != null) {
            this.startingStock = endingStock - this.units ;
        }
    }

    public People getUser() {
        return user;
    }

    public void setUser(People user) {
        this.user = user;
    }

    public Date getTimeStampCreate() {
        return timeStampCreate;
    }

}
