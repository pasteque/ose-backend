package com.ose.backend.model.stocks;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.security.People;


@Entity
@Table(name = "INVENTORY_DETAILS")
public class InventoryDetails {

    public final class Fields {
        public static final String ID = "id";
        public static final String INVENTORY = "inventory";
        public static final String USER = "user";
        public static final String DATE = "date";
        public static final String PRODUCT = "product";
        public static final String QUANTITY = "quantity";
    }

    @Id
    private String id;
    @ManyToOne
    @JoinColumn(name = "inventory")
    private Inventory inventory;
    @ManyToOne
    @JoinColumn(name = "user")
    private People user;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @ManyToOne
    @JoinColumn(name = "product")
    private Products product;
    private long quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public People getUser() {
        return user;
    }

    public void setUser(People user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
    
    public void createId() {
        this.setId(String.format("%s-%d-%s-%s", this.inventory.getId() , new Date().getTime(), this.getUser() == null ? "" : this.getUser().getId(),this.getProduct().getId()));
    }
}
