package com.ose.backend.model.stocks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.model.adaptable.IAdaptable;
import org.pasteque.api.model.saleslocations.Locations;
import org.pasteque.api.model.security.People;

import com.ose.backend.dto.stocks.InventoryDTO;


@Entity
public class Inventory implements IAdaptable {

    public final class Fields {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String START_DATE = "startDate";
        public static final String END_DATE = "endDate";
        public static final String USER = "user";
        public static final String STATUS = "status";
        public static final String LOCATION = "location";
        public static final String NOTE = "note";
        public static final String TYPE = "type";

        public static final String INVENTORY_DETAILS = "inventoryDetails";
    }

    public final class Status {
        public static final String CREATED = "C";
        public static final String IN_PROGRESS = "P";
        public static final String FINISHED = "F";
    }

    public final class Type {
        public static final String COMPLETE = "COMPLET";
        public static final String PARTIAL = "PARTIEL";
        public static final String RAZ = "RAZ";
    }

    @Id
    private String id;
    private String name;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DSTART")
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DEND")
    private Date endDate;
    @ManyToOne
    @JoinColumn(name = "USER")
    private People user;
    private String status = Inventory.Status.CREATED;
    @ManyToOne
    @JoinColumn(name = "LOCATION")
    private Locations location;
    private String note;
    @Column(name = "CTYPE")
    private String type;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = InventoryDetails.Fields.INVENTORY, orphanRemoval = true)
    private List<InventoryDetails> inventoryDetails;

    public Inventory() {
        this.startDate = new Date();
        this.inventoryDetails = new ArrayList<InventoryDetails>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public People getUser() {
        return user;
    }

    public void setUser(People user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Locations getLocation() {
        return location;
    }

    public void setLocation(Locations locations) {
        this.location = locations;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(InventoryDTO.class)) {
            InventoryDTO dto = new InventoryDTO();
            dto.setId(getId());
            if (getUser() != null) {
                dto.setUsernameId(getUser().getId());
                dto.setUsername(getUser().getUsername());
            }
            if (getLocation() != null) {
                dto.setLocationId(getLocation().getId());
                dto.setLocationName(getLocation().getName());
            }
            dto.setStartDate(getStartDate());
            dto.setEndDate(getEndDate());
            dto.setName(getName());
            dto.setNote(getNote());
            dto.setStatusCode(getStatus());
            dto.setStatusLabel(getStatusLabel());
            dto.setType(getType());
            return (T) dto;
        }
        return null;
    }

    private String getStatusLabel() {
        if (Inventory.Status.CREATED.equals(getStatus())) {
            return "Créé";
        } else if (Inventory.Status.IN_PROGRESS.equals(getStatus())) {
            return "En cours";
        } else if (Inventory.Status.FINISHED.equals(getStatus())) {
            return "Terminé";
        }
        return null;
    }

    public List<InventoryDetails> getInventoryDetails() {
        return inventoryDetails;
    }

    public void setInventoryDetails(List<InventoryDetails> inventoryDetails) {
        this.inventoryDetails = inventoryDetails;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void createId() {
        this.setId(String.format("%d-%s", new Date().getTime(), this.getLocation() == null ? "" : this.getLocation().getId()));
    }

}
