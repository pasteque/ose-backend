package com.ose.backend.model.stocks;

import java.util.Date;

import org.pasteque.api.model.products.Products;
import org.pasteque.api.model.saleslocations.Locations;

public class StockMovement {

    private Date date;
    private int typeMovement;
    private Locations locations;
    private Products products;
    private String attributeSetInstanceId;
    private double units;
    private double price;
    private String note;
    private String reference;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTypeMovement() {
        return typeMovement;
    }

    public void setTypeMovement(int typeMovement) {
        this.typeMovement = typeMovement;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public String getAttributeSetInstanceId() {
        return attributeSetInstanceId;
    }

    public void setAttributeSetInstanceId(String attributeSetInstanceId) {
        this.attributeSetInstanceId = attributeSetInstanceId;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isIn() {
        return typeMovement >= 0;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
