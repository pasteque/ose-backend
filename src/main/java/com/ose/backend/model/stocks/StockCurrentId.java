package com.ose.backend.model.stocks;

import java.io.Serializable;

public class StockCurrentId implements Serializable{

    private static final long serialVersionUID = 5289516080581195236L;
    
    private String locations;
    
    private String products;
    

    public StockCurrentId() {
    }
    
    public StockCurrentId(String locations, String products) {
        this.locations = locations;
        this.products = products;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((locations == null) ? 0 : locations.hashCode());
        result = prime * result
                + ((products == null) ? 0 : products.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        StockCurrentId other = (StockCurrentId) obj;
        if (products == null) {
            if (other.getProducts() != null)
                return false;
        } else if (!products.equals(other.getProducts()))
            return false;
        if (locations == null) {
            if (other.getLocations() != null)
                return false;
        } else if (!locations.equals(other.getLocations()))
            return false;
        return true;
    }
}
