package com.ose.backend.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.model.adaptable.IAdaptable;

import com.ose.backend.dto.job.JobLogDTO;


/**
 * The persistent class for the edi_log database table.
 * 
 */
@Entity
@Table(name=JobLog.Table.NAME)
@NamedQuery(name="JobLog.findAll", query="SELECT e FROM JobLog e")
public class JobLog implements Serializable, IAdaptable {

    private static final long serialVersionUID = 1L;

    public final class Table {
        public static final String NAME = "JOBS_LOGS";
    }

    public final class Fields {
        public static final String DATE = "date";
        public static final String JOB = "jobId";
        public static final String LEVEL = "level";
        public static final String MESSAGE = "message";
    }

    public final class Level {
        public static final String OK = Job.Etat.OK;
        public static final String ERREUR = Job.Etat.ERREUR;
        public static final String ALERTE = Job.Etat.ALERTE;
    }

    @Id
    @Column(unique=true, updatable=false, nullable=false, length=255)
    private String id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    //TODO EDU question lien avec Objet Job ?
    @Column(name="job_id", length=255)
    private String jobId;

    @Column(length=45)
    private String level;

    @Lob
    private String message;

    public JobLog() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getJobId() {
        return this.jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getLevel() {
        return this.level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(JobLogDTO.class)) {
            JobLogDTO JobDTO = new JobLogDTO();
            JobDTO.setDateCreation(date);
            JobDTO.setJobId(jobId);
            JobDTO.setId(id);
            JobDTO.setMessage(message);
            JobDTO.setLevel(level);
            return (T) JobDTO;
        }
        return null;
    }

    public void createId() {
        this.setId(String.format("%d-%s-%s-%d", new Date().getTime(),this.jobId , this.level , this.date == null ? 0 : this.date.getTime()));
    }

}