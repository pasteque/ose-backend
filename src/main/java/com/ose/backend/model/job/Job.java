package com.ose.backend.model.job;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.pasteque.api.model.adaptable.IAdaptable;

import com.ose.backend.dto.job.JobDTO;


/**
 * The persistent class for the edi database table.
 * 
 */
@Entity
@Table(name=Job.Table.NAME)
@NamedQueries({
    @NamedQuery(name="Job.findAll", query="SELECT e FROM Job e"),
    @NamedQuery(name="Job.findTodo", query="SELECT e FROM Job e WHERE e.sens = 'INTERNE' AND e.dcreation <= CURRENT_TIME AND e.etat = 'PRET'")})

public class Job implements Serializable, IAdaptable {

    private static final long serialVersionUID = 1L;

    public final class Table {
        public static final String NAME = "JOBS";
    }

    public final class Fields {
        public static final String DATE_CREATION = "dcreation";
        public static final String DATE_ENVOI = "denvoi";
        public static final String DATE_INTEGRATION = "dintegration";
        public static final String DATE_RECEPTION = "dreception";
        public static final String ETAT = "etat";
        public static final String MESSAGE = "message";
        public static final String SENS = "sens";
        public static final String TYPE = "type";
        public static final String DESTINATION = "destination";

    }

    public final class Type {
        public static final String UPLOAD = "UPLOAD";
        public static final String DOWNLOAD = "DOWNLOAD";
        public static final String COMMANDE = "COMMANDE";
        public static final String AFFECTATION_CAMIONS_TOURNEES = "AFFECTATION_CAMIONS_TOURNEES";
        public static final String UTILISATEUR = "UTILISATEUR";
        public static final String DUMMY = "DUMMY";
        public static final String AFFECTATION_PDV_CATALOGUE = "AFFECTATION_PDV_CATALOGUE";
        public static final String ACK = "ACK";
        public static final String STOCKTYPE = "STOCKTYPE";
    }

    public final class Sens {
        public static final String INTERNE = "INTERNE";
        public static final String UPLOAD = "UPLOAD";
        public static final String READY_TO_DOWNLOAD = "READY_TO_DOWNLOAD";
    }

    public final class Parametres {
        public static final String NB_ESSAIS = "essais";
        public static final String RECURRENCE = "recurrence"; //secondes mini entre deux occurences
        public static final String COMMANDE = "commande";
        public static final String PALETTES = "palettes";
        public static final String ACTION = "action";
        public static final String LIAISON = "liaison";
    }
    
    public final class Actions {
        public static final String INSERT = "insert";
        public static final String DELETE = "delete";
        public static final String UPDATE = "update";
    }

    //TODO Pour la suite, gestion des envois d'alertes ou autre par mail ?

    public final class Etat {
        public static final String PRET = "PRET";
        public static final String EN_COURS = "EN_COURS";
        public static final String ALERTE = "ALERTE";
        public static final String ERREUR = "ERREUR";
        public static final String OK = "OK";
    }

    @Id
    @Column(unique=true, updatable=false, nullable=false, length=255)
    private String id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dcreation;

    @Temporal(TemporalType.TIMESTAMP)
    private Date denvoi;

    @Column(length=45)
    private String destination;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dintegration;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dreception;

    @Column(length=45)
    private String etat;

    @Lob
    private String message;

    @Column(length=45)
    private String sens;

    @Column(length=45)
    private String type;

    //TODO EDU Question liste objets Log associés

    public Job() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDcreation() {
        return this.dcreation;
    }

    public void setDcreation(Date dcreation) {
        this.dcreation = dcreation;
    }

    public Date getDenvoi() {
        return this.denvoi;
    }

    public void setDenvoi(Date denvoi) {
        this.denvoi = denvoi;
    }

    public String getDestination() {
        return this.destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDintegration() {
        return this.dintegration;
    }

    public void setDintegration(Date dintegration) {
        this.dintegration = dintegration;
    }

    public Date getDreception() {
        return this.dreception;
    }

    public void setDreception(Date dreception) {
        this.dreception = dreception;
    }

    public String getEtat() {
        return this.etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSens() {
        return this.sens;
    }

    public void setSens(String sens) {
        this.sens = sens;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(JobDTO.class)) {
            JobDTO JobDTO = new JobDTO();
            JobDTO.setDateCreation(dcreation);
            JobDTO.setDateEnvoi(denvoi);
            JobDTO.setDateIntegration(dintegration);
            JobDTO.setDateReception(dreception);
            JobDTO.setDestinataire(destination);
            JobDTO.setEtat(etat);
            JobDTO.setType(type);
            JobDTO.setId(id);
            JobDTO.setMessage(message);
            JobDTO.setSens(sens);
            return (T) JobDTO;
        }
        return null;
    }

    public void createId(String origine) {
        this.setId(String.format("%d-%s-%s-%s", new Date().getTime(), origine , this.type == null ? "" : this.type,
                this.sens == null ? "" : this.sens));
    }

    public boolean loadFromDTO(JobDTO dto) {
        boolean retour = false;
        if ((dto.getSens() != null && !dto.getSens().equals(
                this.getSens()))) {
            retour = true;
            this.setSens(dto.getSens());
        }
        if ((dto.getType() != null && !dto.getType().equals(
                this.getType()))) {
            retour = true;
            this.setType(dto.getType());
        }
        if ((dto.getEtat() != null && !dto.getEtat().equals(
                this.getEtat()))) {
            retour = true;
            this.setEtat(dto.getEtat());
        }
        if ((dto.getMessage() != null && !dto.getMessage().equals(
                this.getMessage()))) {
            retour = true;
            this.setMessage(dto.getMessage());
        }
        if ((dto.getDestinataire() != null && !dto.getDestinataire().equals(
                this.getDestination()))) {
            retour = true;
            this.setDestination(dto.getDestinataire());
        }

        if ((dto.getDateCreation() != null && !dto.getDateCreation().equals(
                this.getDcreation()))) {
            retour = true;
            this.setDcreation(dto.getDateCreation());
        }
        if ((dto.getDateEnvoi() != null && !dto.getDateEnvoi().equals(
                this.getDenvoi()))) {
            retour = true;
            this.setDenvoi(dto.getDateEnvoi());
        }
        if ((dto.getDateIntegration() != null && !dto.getDateIntegration().equals(
                this.getDintegration()))) {
            retour = true;
            this.setDintegration(dto.getDateIntegration());
        }
        if ((dto.getDateReception() != null && !dto.getDateReception().equals(
                this.getDreception()))) {
            retour = true;
            this.setDreception(dto.getDateReception());
        }
        if ((dto.getId() != null && !dto.getId().equals(
                this.getId()))) {
            retour = true;
            this.setId(dto.getId());
        }

        return retour;
    }
    
    protected Job clone() {
        Job retour = new Job();
        retour.setDcreation(dcreation);
        retour.setDenvoi(denvoi);
        retour.setDestination(destination);
        retour.setDintegration(dintegration);
        retour.setDreception(dreception);
        retour.setEtat(etat);
        retour.setId(id);
        retour.setMessage(message);
        retour.setSens(sens);
        retour.setType(type);
        
        return retour;
    }

}