package com.ose.backend.model.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pasteque.api.dto.locations.LocationsInfoDTO;
import org.pasteque.api.dto.locations.POSToursLocationsDTO;
import org.pasteque.api.dto.products.TariffAreasLocationsDTO;
import org.pasteque.api.dto.security.POSUsersDTO;
import org.pasteque.api.model.saleslocations.Locations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ose.backend.dto.orders.OrderDTO;
import com.ose.backend.dto.orders.PaletteDTO;
import com.ose.backend.dto.stocks.StockLevelDTO;

public class JobHelper {

    public static Job initJobCommandePoussee() {
        return initJobCommandePoussee(Locations.SIEGE_ID);
    }

    public static Job initJobCommandePoussee(String destination) {
        Job job = new Job();
        job.setEtat(Job.Etat.PRET);
        job.setSens(Job.Sens.UPLOAD);
        job.setType(Job.Type.COMMANDE);
        job.setDestination(destination);
        return job;
    }

    public static void fillJobCommandes(Job job , OrderDTO order , List<PaletteDTO> paletteDtos ) throws JsonProcessingException{
        String idMessage = "-";
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> message = new HashMap<String,String>();
        message.put(Job.Parametres.COMMANDE, mapper.writeValueAsString(order));
        message.put(Job.Parametres.PALETTES, mapper.writeValueAsString(paletteDtos));

        if(order != null && ! order.getId().isEmpty()) {
            idMessage += order.getId();
        } else if(paletteDtos != null && paletteDtos.size()>0 && !paletteDtos.get(0).getId().isEmpty()){
            idMessage += " Palettes " + paletteDtos.get(0).getId();
        }

        fillJobCommandes(job , mapper.writeValueAsString(message) , idMessage);
    }

    public static void fillJob(Job job , String message , String idMessage , String jobType) throws JsonProcessingException{

        job.setMessage(message);
        job.setDcreation(new Date());

        job.createId(jobType + idMessage );
    }

    public static void fillJobStockType(Job job , String message , String idMessage ) throws JsonProcessingException{
        fillJob(job , message , idMessage , Job.Type.STOCKTYPE );
    }

    public static void fillJobAffectationCamionsTournees(Job job , String message , String idMessage ) throws JsonProcessingException{
        fillJob(job , message , idMessage , Job.Type.AFFECTATION_CAMIONS_TOURNEES );
    }
    
    public static void fillJobAffectationPDVCatalogues(Job job , String message , String idMessage ) throws JsonProcessingException{
        fillJob(job , message , idMessage , Job.Type.AFFECTATION_PDV_CATALOGUE );
    }

    public static void fillJobCommandes(Job job , String message , String idMessage ) throws JsonProcessingException{
        fillJob(job , message , idMessage , Job.Type.COMMANDE );
    }

    public static void fillJobAffectationPDVCatalogues(Job job , String action , TariffAreasLocationsDTO dto ) throws JsonProcessingException{
        String idMessage = String.format("%d-", new Date().getTime());
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> message = new HashMap<String,String>();
        message.put(Job.Parametres.ACTION, mapper.writeValueAsString(action));
        message.put(Job.Parametres.LIAISON, mapper.writeValueAsString(dto));

        if(dto != null &&  dto.getId() != 0D) {
            idMessage += String.valueOf(dto.getId());
        }
        idMessage+= "-"+dto.getLocation() == null ? "" : dto.getLocation().getId();
        idMessage+= "-"+dto.getTariffArea() == null ? "" : String.valueOf(dto.getTariffArea().getId());

        fillJobAffectationPDVCatalogues(job , mapper.writeValueAsString(message) , idMessage);
    }
    
    public static void fillJobAffectationCamionsTournees(Job job , String action , POSToursLocationsDTO dto ) throws JsonProcessingException{
        String idMessage = String.format("%d-", new Date().getTime());
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> message = new HashMap<String,String>();
        message.put(Job.Parametres.ACTION, mapper.writeValueAsString(action));
        message.put(Job.Parametres.LIAISON, mapper.writeValueAsString(dto));

        if(dto != null &&  dto.getId() != null &&  dto.getId() != 0D) {
            idMessage += String.valueOf(dto.getId());
        }
        idMessage+= "-"+dto.getLocation() == null ? "" : dto.getLocation().getId();
        idMessage+= "-"+dto.getTour() == null ? "" : String.valueOf(dto.getTour().getId());

        fillJobAffectationCamionsTournees(job , mapper.writeValueAsString(message) , idMessage);
    }
    
    public static void fillJobStockType(Job job , String action , List<StockLevelDTO> dtos ) throws JsonProcessingException{
        String idMessage = String.format("%d-", new Date().getTime());
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> message = new HashMap<String,String>();
        message.put(Job.Parametres.ACTION, mapper.writeValueAsString(action));
        message.put(Job.Parametres.LIAISON, mapper.writeValueAsString(dtos));
        
        if(dtos.size() > 0) {
            
            StockLevelDTO dto = dtos.get(0);
            idMessage+= "-NbLignes:"+String.valueOf(dtos.size());
            
            if(dto != null &&  dto.getId() != null) {
                idMessage += dto.getId();
            }
            idMessage+= "-"+dto.getLocationsId() == null ? "" : dto.getLocationsId();
            idMessage+= "-"+dto.getProductsId() == null ? "" : dto.getProductsId();
        }
        fillJobStockType(job , mapper.writeValueAsString(message) , idMessage);
    }

    public static Job createJobAffectationCamionsTournees(POSToursLocationsDTO dto ,  LocationsInfoDTO local) {
        // générer 1 job AFFECTATION_CAMIONS_TOURNEES de sens READY_TO_DOWNLOAD à l'état PRET pour la location concernée
        //job à destination du point de vente concerné en ready to download
        //ou si on y est à destination du central en UPLOAD
        //si on est sur celui de la log alors retour = null


        Job job = new Job();
        job.setEtat(Job.Etat.PRET);
        job.setSens(local == null ? Job.Sens.READY_TO_DOWNLOAD : Job.Sens.UPLOAD);
        job.setType(Job.Type.AFFECTATION_CAMIONS_TOURNEES);
        job.setDestination(local == null ? dto.getLocation().getId() : Locations.SIEGE_ID);

        return local != null && local.getId().equals(Locations.SIEGE_ID) ? null : job;
    }
    
    public static Job createJobAffectationPDVCatalogues(TariffAreasLocationsDTO dto ,  LocationsInfoDTO local) {
        // générer 1 job AFFECTATION_PDV_CATALOGUE de sens READY_TO_DOWNLOAD à l'état PRET pour la location concernée
        //job à destination du point de vente concerné en ready to download
        //ou si on y est à destination du central en UPLOAD
        //si on est sur celui de la log alors retour = null

        Job job = new Job();
        job.setEtat(Job.Etat.PRET);
        job.setSens(local == null ? Job.Sens.READY_TO_DOWNLOAD : Job.Sens.UPLOAD);
        job.setType(Job.Type.AFFECTATION_PDV_CATALOGUE);
        job.setDestination(local == null ? dto.getLocation().getId() : Locations.SIEGE_ID);

        return local != null && local.getId().equals(Locations.SIEGE_ID) ? null : job;
    }

    public static List<Job> createJobForEveryBack(Job baseJob, List<LocationsInfoDTO> dtos) {
        List<Job> retour = new ArrayList<Job>();
        for(LocationsInfoDTO dto : dtos) {
            Job job = baseJob.clone();
            job.setDestination(dto.getId());
            job.setId(job.getId().concat("-fw-"+dto.getId()));
            retour.add(job);
        }
        return retour;
    }
    
    public static Job createJobUtilisateur(POSUsersDTO dto ,  LocationsInfoDTO local) {
        //générer 1 job UTILISATEUR de sens UPLOAD à l'état PRET pour le back central
        ObjectMapper mapper = new ObjectMapper();
        Job job = new Job();
        job.setEtat(Job.Etat.PRET);
        job.setSens(Job.Sens.UPLOAD);
        job.setType(Job.Type.UTILISATEUR);
        job.setDestination(Locations.SIEGE_ID);
        
        try {
            fillJob(job, mapper.writeValueAsString(dto), dto.getId(), Job.Type.UTILISATEUR);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            job = null;
        }

        return job;
    }
    
    public static Job createJobStockType(LocationsInfoDTO local) {

        /**
         * On ne crée le job que si nous sommes sur le back central
         */
        Job job = new Job();
        job.setEtat(Job.Etat.PRET);
        job.setSens(Job.Sens.READY_TO_DOWNLOAD);
        job.setType(Job.Type.STOCKTYPE);
        job.setDestination(Locations.SIEGE_ID);

        return local != null ? null : job;
    }
}
