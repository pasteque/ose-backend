package com.ose.backend.model.locations;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.pasteque.api.model.adaptable.IAdaptable;

import com.ose.backend.dto.locations.PaysDTO;




@Entity
@Table(name = Pays.Table.NAME)
public class Pays implements IAdaptable {
    public final class Table {
        public static final String NAME = "PAYS";
    }

    public final class Fields {
        public static final String PAYS_ID = "paysId";
        public static final String NOM = "nom";
        public static final String ABREVIATION = "abreviation";        
        public static final String CODE_DAE = "codeDAE";
    }
    @Id
    @Column(name = "pays_id")
    private Long paysId;
    private String nom;
    private String abreviation;
    @Column(name = "code_dae")
    private String codeDAE;
    
    
    public Long getPaysId() {
        return paysId;
    }

    public void setPaysId(Long paysId) {
        this.paysId = paysId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAbreviation() {
        return abreviation;
    }

    public void setAbreviation(String abreviation) {
        this.abreviation = abreviation;
    }

    public String getCodeDAE() {
        return codeDAE;
    }

    public void setCodeDAE(String codeDAE) {
        this.codeDAE = codeDAE;
    }

    @Override
    public <T> T getAdapter(Class<T> selector) {
        return getAdapter(selector, false);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> selector, boolean filtered) {
        if (selector.equals(PaysDTO.class)) {

            PaysDTO paysDTO = new PaysDTO();
            paysDTO.setId(getPaysId());
            paysDTO.setNom(getNom());
            paysDTO.setCodeDAE(getCodeDAE());
            paysDTO.setAbreviation(getAbreviation());
            return (T) paysDTO;
        }
        return null;
    }
        
    public String toString() {
        return String.format("%s-%s", this.abreviation , this.nom );
    }
    
    
}
