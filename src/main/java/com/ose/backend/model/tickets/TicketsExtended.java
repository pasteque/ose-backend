package com.ose.backend.model.tickets;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = TicketsExtended.Table.NAME)
public class TicketsExtended {

    public final class Table {
        public static final String NAME = "TICKETS_EXTENDED";
    }

    public final class Fields {
        public static final String TICKET_ID = "ticketId";
        public static final String CUSTOMER_ID = "customerId";
    }

    @Id
    @Column(name = "TICKET_ID")
    private String ticketId;
    @Column(name = "CUSTOMER_ID")
    private String customerId;

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
