package com.ose.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.ose.backend.config.JobsDefinitionConfig;



@SpringBootApplication
@EnableAutoConfiguration
//@EnableConfigurationProperties({JobsDefinitionConfig.class , Monitoring.class})
@EnableConfigurationProperties(JobsDefinitionConfig.class)
@EntityScan(basePackages = {"org.pasteque.api.model","com.ose.backend.model",})
@EnableJpaRepositories(basePackages = {"org.pasteque.api.dao","com.ose.backend.dao",})
@ComponentScan(basePackages = {"org.pasteque.api.controller", "org.pasteque.api.service","com.ose.backend","com.ose.backend.service","com.ose.backend.model",})
@EnableScheduling
public class OseBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(OseBackendApplication.class, args);
    }

}
