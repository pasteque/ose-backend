package com.ose.backend.security;

import org.pasteque.api.service.security.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ose.backend.controller.edi.EdiController;
import com.ose.backend.controller.jobs.JobsController;
import com.ose.backend.controller.view.application.LoginViewController;

@Configuration
@EnableWebSecurity
public class BackendSecurityConfiguration extends WebSecurityConfigurerAdapter {

    /*
     * The below paths will get ignored by the filter
     */
    public static final String ROOT_MATCHER = "/";
    public static final String CSS_MATCHER = "/css/**";
    public static final String JS_MATCHER = "/js/**";
    public static final String IMG_MATCHER = "/images/*";
    public static final String API_MATCHER = "/api/**";
    public static final String LOGIN_MATCHER = LoginViewController.LOGIN_URL;
    public static final String LOGOUT_MATCHER = "/logout";
    public static final String LOGINHTML_MATCHER = "/login.html";
    public static final String LOGINAPI_MATCHER = "/api/login";
    public static final String JOBS_MATCHER = JobsController.BASE_URL + "/**";
    public static final String EDI_MATCHER = EdiController.BASE_URL + "/**";

    private String[] pathsToSkip = {
            ROOT_MATCHER,
            CSS_MATCHER,
            JS_MATCHER,
            IMG_MATCHER,
            LOGIN_MATCHER,
            LOGOUT_MATCHER,
            LOGINAPI_MATCHER,
            LOGINHTML_MATCHER,
            API_MATCHER,
            JOBS_MATCHER,
            EDI_MATCHER
    };

    @Bean
    public PasswordEncoder passwordEncoder() {
        DelegatingPasswordEncoder delPasswordEncoder=  (DelegatingPasswordEncoder)PasswordEncoderFactories.createDelegatingPasswordEncoder();
        BCryptPasswordEncoder bcryptPasswordEncoder = new BCryptPasswordEncoder();
        delPasswordEncoder.setDefaultPasswordEncoderForMatches(bcryptPasswordEncoder);
        return delPasswordEncoder;
    }

    @Autowired
    private IUsersService userService;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
        .userDetailsService( userService )
        .passwordEncoder( passwordEncoder() );

    }
    
    @Bean
    public UrlParametersAuthenticationFilter authenticationFilter() throws Exception {
        UrlParametersAuthenticationFilter authenticationFilter = new UrlParametersAuthenticationFilter();
        return authenticationFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .authorizeRequests()
        .antMatchers( pathsToSkip).permitAll()
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage(LoginViewController.LOGIN_URL)
        .defaultSuccessUrl("/#/", true)
        .failureUrl("/#/?error=true")
        .and()
        .logout()
        .logoutSuccessUrl("/")
        .permitAll();

        //TODO EDU reprendre plus proprement avec gestion CSRF propre
        //En attendant désactiver sinon les POST ne passent pas
        http.csrf().disable();
        
        http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    protected UserDetailsService userDetailsService() {
        return userService;
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        BackendAuthenticationManager retour = new BackendAuthenticationManager();
        retour.setPasswordEncoder(passwordEncoder());
        return retour;
    }

}
