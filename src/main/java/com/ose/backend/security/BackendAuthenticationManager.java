package com.ose.backend.security;

import javax.annotation.PostConstruct;

import org.pasteque.api.service.security.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;


@Component
public class BackendAuthenticationManager extends DaoAuthenticationProvider implements AuthenticationManager {

    @Autowired
    private WebApplicationContext applicationContext;
    @Autowired
    private IUsersService userService;
    
    @PostConstruct
    public void completeSetup() {
        userService = applicationContext.getBean(IUsersService.class);
        setUserDetailsService(userService);
    }
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
       Authentication auth = super.authenticate(authentication);
       return auth;
    }
    
    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
