package com.ose.backend.security;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pasteque.api.service.security.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ose.backend.util.EncryptDecrypt;



public class UrlParametersAuthenticationFilter     extends
OncePerRequestFilter {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    @Autowired
    private IUsersService usersService;

    private String getToken(String token) {

        String userName = null;
        String dateInString;
        String decryptToken;
        try {
            decryptToken = EncryptDecrypt.decrypt(token);
            if (decryptToken != null){
                String[] tab = decryptToken.split("_");
                dateInString = tab[1];
                Date dateConnect = formatter.parse(dateInString);
                Date today = Calendar.getInstance().getTime();
                long diffMin = Math.abs(today.getTime() - dateConnect.getTime()) / 60000l;
                long min = diffMin%60l;
                long hour = diffMin/60l;
                if (hour > 3 || (hour == 3 && min > 0)){ // on verifie si la demmande de connexion est > 3 h
                    userName= null;
                }else{
                    userName =  tab[0];
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userName;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
                    throws ServletException, IOException {
        if (request.getParameter("token")!=null) {
            String token = request.getParameter("token");


            UserDetails userDetails = usersService.loadUserByUsername(getToken(token));
            if(userDetails != null) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }

        }

        filterChain.doFilter(request, response);

    }


}
