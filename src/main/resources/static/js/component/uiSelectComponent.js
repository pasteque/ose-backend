oseModule.component('uiSelectAdresse', {
    bindings : {
        params : '=?',
        selectedItems : '=',
        readOnly : '=?',
        required : '=?',
        multiple : '=?',
        placeholder : '=?',
    },
    controller : 'uiSelectAdresseCtrl',
    templateUrl : 'partial/templates/ui-select-template.html'
});


oseModule.controller('uiSelectAdresseCtrl', function uiSelectAdresseCtrl($scope, $timeout, AdresseAPIDAO) {
    var ctrl = this;
    if (angular.isUndefined(ctrl.params)) {
        ctrl.params = {
                limit : 20
        		};
    }
    if (angular.isUndefined(ctrl.required)) {
        ctrl.required = false;
    }
    if (angular.isUndefined(ctrl.readOnly)) {
        ctrl.readOnly = false;
    }
    if (angular.isUndefined(ctrl.multiple)) {
        ctrl.multiple = false;
    }

    ctrl.getItems = function(value) {
    	var tmp = undefined ;
    	var shouldIcontinue = false ;
    	
		if (ctrl.requeteAdress != undefined) {
			ctrl.requeteAdress.$cancelRequest(); 		
		}
		if (ctrl.requeteAdressPostal != undefined) {
			ctrl.requeteAdressPostal.$cancelRequest(); 		
		}
        if (value != '') {
        	if(value.length > 3 ) {
               
                ctrl.params.q = value;
                
                shouldIcontinue = true ;
            }
        	
        } else {
            ctrl.params.q = undefined;
        }
        
        if(shouldIcontinue){
        	ctrl.items = [] ;
	        if(ctrl.params.q !== undefined) {
	        	ctrl.requeteAdressPostal = AdresseAPIDAO.search(ctrl.params);
	            ctrl.requeteAdressPostal.$promise.then(function(response) {
	            	if(response.length > 0) {
		        		if(ctrl.items != undefined && ctrl.items.length > 0) {
		        			var tmpArray = ctrl.items.slice().concat(response);
		        			ctrl.items = tmpArray ;
		        		} else {
		        			ctrl.items = response;
		        		}
	            	}
	            },function(httpResponse){
	    			//console.log("Annulée ou erreur");
	    		});
	        }
	        
	        ctrl.requeteAdress = AdresseAPIDAO.search(ctrl.params);
	        
	        ctrl.requeteAdress.$promise.then(function(response) {
	        	if(response.features.length > 0) {
	        		if(ctrl.items != undefined && ctrl.items.length > 0) {
	            		var tmpArray = response.features.concat(ctrl.items.slice());
	        			ctrl.items = tmpArray ;
	        		} else {
	        			ctrl.items = response.features;
	        		}
	        	}
	        },function(httpResponse){
				console.log("Annulée ou erreur");
			});
        }
    };
        
});
