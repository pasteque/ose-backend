'use strict';

var oseModule = angular.module('oseModule', [ 'ngResource', 'ngRoute', 'ngSanitize','ui.select', 'monospaced.qrcode', 'ui.bootstrap', 'io-barcode' ,
        'ui.chart' , 'nvd3' , 'toggle-switch' , 'infinite-scroll' , 'ui.grid','ui.grid.pinning','ui.grid.resizeColumns', 'ui.grid.moveColumns' , 
        'nzToggle' , 'ngFileUpload' , 'scrollable-table']);
        
var loginModule = angular.module('loginModule', [ 'monospaced.qrcode', 'ui.bootstrap', 'io-barcode']);