
oseModule.factory('AdresseAPIDAO', function AdresseAPIDAO($resource) {
    return $resource('https://api-adresse.data.gouv.fr/search/:param1', {
    	param1 : '@param1'
    }, {
        search : {
            method : 'GET',
            params : {
            	
            },
            isArray : false,
            cancellable: true,
        },
//        searchDepartements : {
//            method : 'GET',
//            params : {
//            	service : 'departement',
//            },
//            isArray : true,
//            cancellable: true,
//        },
//        searchRegion : {
//            method : 'GET',
//            params : {
//            	service : 'regions'
//            },
//            isArray : true,
//            cancellable: true,
//        },
    });
});
