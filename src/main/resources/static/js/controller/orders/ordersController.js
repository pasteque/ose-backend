oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/orders/list', {
        templateUrl : '/partial/orders/orders-list.html',
        controller : 'ordersListController',
        reloadOnSearch : false
    });
    $routeProvider.when('/orders/form', {
        templateUrl : '/partial/orders/orders-form.html',
        controller : 'ordersFormController'
    });
    $routeProvider.when('/orders/:id/form', {
        templateUrl : '/partial/orders/orders-form.html',
        controller : 'ordersFormController'
    });
    $routeProvider.when('/palettes/form', {
        templateUrl : '/partial/orders/palettes-form.html',
        controller : 'palettesFormController'
    });
    $routeProvider.when('/orders/supervision-dashboard', {
        templateUrl : '/partial/orders/supervision-dashboard.html',
        controller : 'supervisionDashboardController'
    });
    $routeProvider.when('/orders/renflouement-dashboard', {
        templateUrl : '/partial/orders/renflouement-dashboard.html',
        controller : 'renflouementDashboardController'
    });

} ]);

oseModule.factory('OrdersService', function OrdersService($resource) {
    return $resource('/orders/:param1:id/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getAllByCriteria : {
            method : 'GET',
            params : {
                param1 : 'getAllByCriteria'
            },
            isArray : false,
            cancellable : true
        },
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        },
        getProductsLocations : {
            method : 'GET',
            params : {
                param1 : 'getProductsLocations'
            },
            isArray : false
        },
        getProductsPaletteInfo : {
            method : 'GET',
            params : {
                param1 : 'getProductsPaletteInfo'
            },
            isArray : false
        },
        initialize : {
            method : 'GET',
            params : {
                param1 : 'initialize'
            },
            isArray : false
        },
        addProductToOrder : {
            method : 'GET',
            params : {
                param1 : 'addProductToOrder'
            },
            isArray : false
        },
        removeProductToOrder : {
            method : 'GET',
            params : {
                param1 : 'removeProductToOrder'
            },
            isArray : false
        },
        saveOrderDetail : {
            method : 'POST',
            params : {
                param1 : 'saveOrderDetail'
            },
            isArray : false
        },
        saveOrder : {
            method : 'POST',
            params : {
                param1 : 'saveOrder'
            },
            isArray : false
        },
        generateOrder : {
            method : 'GET',
            params : {
                param1 : 'generateOrder'
            },
            isArray : false
        },
        deleteOrder : {
            method : 'GET',
            params : {
                param1 : 'deleteOrder'
            },
            isArray : false
        },
        canValidateOrder : {
            method : 'GET',
            params : {
                param1 : 'canValidateOrder'
            },
            isArray : false
        },
        canModifyOrder : {
            method : 'GET',
            params : {
                param1 : 'canModifyOrder'
            },
            isArray : false
        },
        canDeleteOrder : {
            method : 'GET',
            params : {
                param1 : 'canDeleteOrder'
            },
            isArray : false
        },
        nextOrderStep : {
            method : 'GET',
            params : {
                param1 : 'nextOrderStep'
            },
            isArray : false
        },
        getPalette : {
            method : 'GET',
            params : {
                param1 : 'getPalette'
            },
            isArray : false
        },
        getPalettes : {
            method : 'GET',
            params : {
                param1 : 'getPalettes'
            },
            isArray : false,
            cancellable : true
        },
        getPalettePreparationInfo : {
            method : 'GET',
            params : {
                param1 : 'getPalettePreparationInfo'
            },
            isArray : false,
            cancellable : true
        },
        getPalettesByCriteria : {
            method : 'GET',
            params : {
                param1 : 'getPalettesByCriteria'
            },
            isArray : false,
            cancellable : true
        },
        addToPalette : {
            method : 'GET',
            params : {
                param1 : 'addToPalette'
            },
            isArray : false
        },
        updatePaletteDetail : {
            method : 'GET',
            params : {
                param1 : 'updatePaletteDetail'
            },
            isArray : false
        },
        canAddToPalette : {
            method : 'GET',
            params : {
                param1 : 'canAddToPalette'
            },
            isArray : false
        },
        canAddToOrder : {
            method : 'GET',
            params : {
                param1 : 'canAddToOrder'
            },
            isArray : false
        },
        generatePalette : {
            method : 'GET',
            params : {
                param1 : 'generatePalette'
            },
            isArray : false
        },
        savePalette : {
            method : 'POST',
            params : {
                param1 : 'savePalette'
            },
            isArray : false
        },
        fillPaletteQuantity : {
            method : 'GET',
            params : {
                param1 : 'fillPaletteQuantity'
            },
            isArray : false
        },
        deletePalette : {
            method : 'GET',
            params : {
                param1 : 'deletePalette'
            },
            isArray : false
        },
        deletePaletteDetail : {
            method : 'GET',
            params : {
                param1 : 'deletePaletteDetail'
            },
            isArray : false
        },
        nextPaletteStep : {
            method : 'GET',
            params : {
                param1 : 'nextPaletteStep'
            },
            isArray : false
        },
        receivePalettes : {
            method : 'GET',
            params : {
                param1 : 'receivePalettes'
            },
            isArray : false
        },
        getProductsByEmplacement : {
            method : 'GET',
            params : {
                param1 : 'getProductsByEmplacement'
            },
            isArray : false
        },
        resyncJob : {
            method : 'GET',
            params : {
                param1 : 'resyncJob'
            },
            isArray : false
        },
        download : {
            method : 'GET',
            params : {
                value1 : 'download'
            },
            responseType : 'arraybuffer',
            cancellable : true,
            transformResponse : function(data, headers) {
                var response = {};
                response.headers = headers();
                response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');

                response.contentType = headers('content-type');
                if (data) {
                    response.blob = new Blob([ data ], {
                        type : response.contentType
                    });
                }
                return response;
            }
        },
    bonDeLivraison : {
            method : 'GET',
            params : {
                value1 : 'bonDeLivraison'
            },
            responseType : 'arraybuffer',
            cancellable : true,
            transformResponse : function(data, headers) {
                var response = {};
                response.headers = headers();
                response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');

                response.contentType = headers('content-type');
                if (data) {
                    response.blob = new Blob([ data ], {
                        type : response.contentType
                    });
                }
                return response;
            }
        },
    });
});

oseModule.controller('ordersListController', function ordersListController($scope, $window, $location , OrdersService, LocationsService , CriteriaService) {

    $scope.nbResults = 25;
    $scope.isLoading = true;
    $scope.canCreate = true;

    $scope.sortType = 'creationDate';
    $scope.sortReverse = true;

    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
            formatYear : "yyyy",
            startingDay : 1
    };

    $scope.resetHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }
    
    $scope.reSync = function(order) {
    OrdersService.resyncJob({
            orderId : order.id
        }, function(response) {
            console.log(response);
        });
    }
    
   initializeQueryParams();

   function initializeQueryParams() {
      if (CriteriaService.getParamsUrl($scope)) {
          findOrders();
      } else {          
      var dTo = moment().toDate();
      var dFrom = moment().subtract(7, 'days').toDate();
    
      LocationsService.getDefaultSaleLocation(function(response) {
        if (response.content != null && response.content.id != null) {
            $scope.criteria = {
                    location : response.content,
                    from : dFrom,
                    to : dTo,
            }
            $scope.searchOrders();
        } else {
            $scope.criteria = {
                    from : dFrom,
                    to : dTo,
            }
        }
        $scope.isLoading = false;
        $scope.nbResults = 9999;
        }); 
      }
   }
   
     $scope.searchOrders = function() {
          $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
          findOrders();
      }

    // $scope.sortType = 'productCode';
    $scope.sortReverse = false;

    LocationsService.getDefault(function(response) {
        $scope.canCreate = (response.content != null && response.content.id != null);
    });

    LocationsService.getParentLocations({
        avecVirtuels : true
    }, function(response) {
        $scope.locations = response.content;
    });

    function findOrders() {
        $scope.isLoading = true;
        $scope.criteria.from = $scope.resetHours($scope.criteria.from);
        $scope.criteria.to = $scope.upgHours($scope.criteria.to);

        var locationId = $scope.criteria.location != undefined ? $scope.criteria.location.id : undefined;
        var reference = $scope.criteria.reference != undefined && $scope.criteria.reference.length > 0 ? $scope.criteria.reference : undefined;

        OrdersService.getAllByCriteria({
            locationId : locationId,
            likereference : reference,
            from : $scope.criteria.from.getTime(),
            to : $scope.criteria.to.getTime(),
            nbResults : $scope.nbResults
        }, function(response) {
            $scope.orders = response.content;
            $scope.isLoading = false;
        });
    }

});

oseModule.controller('ordersFormController', function ordersFormController($scope, $location, $routeParams, $route, $rootScope, $uibModal,
        $timeout, Upload, LocationsService, ProductsService, StocksService, OrdersService) {

    var _action = $location.search()['_action'];
    var locationId = $location.search()['locationId'];

    $scope.file = undefined;

    $scope.isLoading = true;
    $scope.edit = (_action == "create" || _action == "edit");

    $scope.sortType = 'product.barcode';
    $scope.sortReverse = false;

    $scope.sortType2 = 'name';
    $scope.sortReverse2 = true;

    $scope.canEdit = true;

    $scope.isLoading = false;
    $scope.isDownLoading = false;
    
    //EDU - Permet de savoir qu'il y a eu une modification rendant la valo fausse
    $scope.incertain = false;

    $scope.canValidate = false;

    $scope.orderDetail = {
            product : undefined,
            quantityAsked : 1
    };
    
    $scope.value = 0;

    if (_action == "create") {
        OrdersService.initialize({
            locationId : locationId,
        }, function(response) {
            $scope.order = response.content;
            if ($scope.locations) {
                $scope.order.locationFrom = $scope.locations[0];
            }
            $scope.canModify = true;
            $scope.canDelete = true;
            $scope.isLoading = false;
            $scope.files = [];
        });
    } else {
        $scope.isLoading = true;

        function updateCanCreateNewPalette() {
            OrdersService.getProductsLocations({
                orderId : $routeParams.id
            }, function(response) {
                $scope.canCreateNewPalette = response.content.length > 0;
            })
        }
        updateCanCreateNewPalette();

        OrdersService.get({
            id : $routeParams.id
        }, function(response) {
            $scope.order = response.content;
            //EDU - On regarde pour récupérer la valeur de la commande
            //cf wiki http://192.168.42.15/mediawiki/index.php/Gestion_des_commandes#Affichage_de_la_valorisation_de_la_commande
            $scope.fillValue();
            $scope.isLoading = false;
        });

        OrdersService.canValidateOrder({
            orderId : $routeParams.id
        }, function(response) {
            $scope.canValidate = response.content;
        });        

        OrdersService.canModifyOrder({
            orderId : $routeParams.id
        }, function(response) {
            $scope.canModify = response.content;
        });
        
        OrdersService.canDeleteOrder({
            orderId : $routeParams.id
        }, function(response) {
            $scope.canDelete = response.content;
        });

    }
    LocationsService.getParentLocations({
        locationId : locationId,
        avecVirtuels : true
    }, function(response) {
        $scope.locations = response.content;
        if ($scope.order) {
            $scope.order.locationFrom = response.content[0];
        }
    });

    $scope.log = '';
    $scope.loaded = false;

    $scope.upload = function(file) {
        $scope.file = file;
        if (file && !file.$error) {
            Upload.upload({
                url : '/edi/upload',
                data : {
                    file : file
                }
            }).then(function(resp) {
                $timeout(function() {
                    $scope.log = 'file: ' + resp.config.data.file.name + ', Response: ' + JSON.stringify(resp.data) + '\n' + $scope.log;

                    // reloc
                    $scope.loaded = true;
                });
            }, null, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n' + $scope.log;
            });
        }
    };
    
    $scope.fillValue = function() {
        if($scope.order != undefined && $scope.order.orderDetails.length!=0) {
            if($scope.order.statusOrder > 1 ) {
	            if($scope.order.statusOrder > 4 ) {
	               for(var element in $scope.order.orderDetails) {
	                   $scope.value+= $scope.order.orderDetails[element].quantitySent * $scope.order.orderDetails[element].product.currentPMPA;
	               }
	            } else {
	                for(var element in $scope.order.orderDetails) {
                       $scope.value+= $scope.order.orderDetails[element].quantityToPrepare * $scope.order.orderDetails[element].product.currentPMPA;
                   }
	            }
            }  
        }
    };

    $scope.getProducts = function(value) {
        if ($scope.requeteProduits != undefined) {
            $scope.requeteProduits.$cancelRequest();
        }
        if (value != "") {
            if ($scope.paletteDetail && $scope.paletteDetail.product) {
                $scope.paletteDetail.product = undefined;
            }
            // $scope.products = undefined;
            $scope.requeteProduits = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : false
            });
            $scope.requeteProduits.$promise.then(function(response) {
                $scope.products = response.content;
                if ($scope.products.length == 1) {
                    $scope.orderDetail.product = $scope.products[0];
                }
            }, function(httpResponse) {
                // console.log("Annulée ou
                // erreur");
            });
        }
    }

    $scope.getStockInfo = function(value) {
        StocksService.getCriteria({
            locationId : $scope.order.locationFrom.id,
            reference : $scope.orderDetail.product.id
        }, function(response) {
            if (response.content.length > 0) {
                $scope.orderDetail.stockFrom = response.content[0].units;
            } else {
                $scope.orderDetail.stockFrom = 0;
            }
        });
        StocksService.getCriteria({
            locationId : $scope.order.locationTo.id,
            reference : $scope.orderDetail.product.id
        }, function(response) {
            if (response.content.length > 0) {
                $scope.orderDetail.stockTo = response.content[0].units;
            } else {
                $scope.orderDetail.stockTo = 0;
            }
        });
    }

    $scope.addOrderDetail = function() {
        if ($scope.orderDetail.product && $scope.orderDetail.quantityAsked) {

            OrdersService.canAddToOrder({
                orderId : $scope.order.id,
                productId : $scope.orderDetail.product.id,
                quantity : $scope.orderDetail.quantityAsked
            },
            function(response) {
                if (response.content) {
                    OrdersService.addProductToOrder({
                        orderId : $scope.order.id,
                        productId : $scope.orderDetail.product.id,
                        quantity : $scope.orderDetail.quantityAsked
                    }, function(response) {
                        $scope.order = response.content;
                        updateCanCreateNewPalette();
                    });

                    $scope.orderDetail = {
                            product : undefined,
                            quantityAsked : 1
                    };
                    $scope.$broadcast('SetFocus');
                    $scope.incertain = true;
                } else {
                    alert('Impossible d\'ajouter ' + $scope.orderDetail.quantityAsked
                            + ', car l\'emplacement est ramasse ou en cours de ramassage !');
                }

            });
        }
    }

    $scope.removeOrderDetail = function(orderDetail) {
        $scope.isLoading = true;
        OrdersService.removeProductToOrder({
            orderId : $scope.order.id,
            productId : orderDetail.product.id,
        }, function(response) {
            $scope.order = response.content;
            $scope.isLoading = false;
        });

    }

    $scope.editOrder = function() {
        $location.search({
            _action : "edit"
        });
    }

    $scope.cancelOrder = function() {
        $location.search({
            _action : "view"
        });
    }

    $scope.nextOrderStep = function() {
        $scope.canValidate = false;
        $scope.isLoading = true;
        OrdersService.nextOrderStep({
            orderId : $routeParams.id
        }, function(response) {
            $scope.canValidate = true;
            $scope.isLoading = false;
            $route.reload();
        });
    }

    $scope.createPalette = function() {

        var modalScope = $rootScope.$new();
        modalScope.orderId = $scope.order.id;

        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/orders/palettes-modal-form.html',
            controller : 'palettesModalFormController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.isLoading = true;
            OrdersService.get({
                id : $routeParams.id
            }, function(response) {
                $scope.order = response.content;
                updateCanCreateNewPalette();
                $scope.isLoading = false;
            });
        });

    };

    $scope.editPalette = function(value) {
        var modalScope = $rootScope.$new();

        modalScope.paletteId = value;

        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/orders/palettes-modal-form.html',
            controller : 'palettesModalFormController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.isLoading = true;
            OrdersService.get({
                id : $routeParams.id
            }, function(response) {
                $scope.order = response.content;
                $scope.isLoading = false;
            });
        });
    };

    $scope.deletePalette = function(value) {
        $scope.isLoading = true;
        OrdersService.deletePalette({
            paletteId : value
        }, function(response) {
            $scope.order = response.content;
            updateCanCreateNewPalette();
            $scope.isLoading = false;
        })
    };

    $scope.addToPalette = function(value, sortType, sortReverse) {
        var modalScope = $rootScope.$new();

        modalScope.orderDetailId = value;
        modalScope.orderId = $routeParams.id;

        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/orders/palettes-select-form.html',
            controller : 'selectPalettesFormController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.isLoading = true;
            OrdersService.get({
                id : $routeParams.id
            }, function(response) {
                $scope.order = response.content;
                updateCanCreateNewPalette();
                $scope.sortType = sortType;
                $scope.sortReverse = sortReverse;
                $scope.isLoading = false;
            });
        });
    }

    $scope.saveOrder = function() {
        OrdersService.saveOrder($scope.order, function(response) {
            $location.url("/orders/" + response.content.id + "/form?_action=view");
        });
    };

    $scope.isGenerating = false;
    $scope.generateOrder = function() {
        $scope.isGenerating = true;
        OrdersService.generateOrder({
            orderId : $scope.order.id
        }, function(response) {
            $scope.isGenerating = false;
            $scope.order = response.content;
        });
    }

    $scope.deleteOrder = function() {
        if (confirm("Attention, la suppression d'une commande est irreversible !\n\n\t\t\tVoulez-vous confirmer ?")) {
            OrdersService.deleteOrder({
                orderId : $scope.order.id
            }, function(response) {
                $location.url("/orders/list");
            });
        }
    }

    $scope.receivePalettes = function() {
        if (confirm("Attention, c'est une reception globale de la commande !\n\n\t\t\tVoulez-vous confirmer ?")) {
            $scope.isLoading = true;
            OrdersService.receivePalettes({
                orderId : $scope.order.id
            }, function(response) {
                $scope.isLoading = false;
                $location.url("/orders/list");
            });
        }
    }

    $scope.$on('startEditQuantity', function(event) {
        $scope.canEdit = false;
    });
    $scope.$on('stopEditQuantity', function(event) {
        $scope.canEdit = true;
        updateCanCreateNewPalette();
    });

    $scope.downloadOfbiz = function() {

        $scope.isDownLoading = true;
        OrdersService.download({
            id : $scope.order.id,
            _format : 'csv'
        }, function(response) {

            // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            var linkElement = document.createElement('a');
            try {

                // var blob = new Blob(response, { type:
                // contentType });
                var url = window.URL.createObjectURL(response.blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", response.filename);

                var clickEvent = new MouseEvent("click", {
                    "view" : window,
                    "bubbles" : true,
                    "cancelable" : false
                });
                linkElement.dispatchEvent(clickEvent);

                $scope.isDownLoading = false;

            } catch (ex) {
                console.log(ex);
                $scope.isDownLoading = false;
            }
        });

    }

    $scope.downloadXLS = function() {

        $scope.isDownLoading = true;
        OrdersService.download({
            id : $scope.order.id,
            _format : 'xls'
        }, function(response) {

            // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            var linkElement = document.createElement('a');
            try {

                // var blob = new Blob(response, { type:
                // contentType });
                var url = window.URL.createObjectURL(response.blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", response.filename);

                var clickEvent = new MouseEvent("click", {
                    "view" : window,
                    "bubbles" : true,
                    "cancelable" : false
                });
                linkElement.dispatchEvent(clickEvent);

                $scope.isDownLoading = false;

            } catch (ex) {
                console.log(ex);
                $scope.isDownLoading = false;
            }
        });
    }
        
    $scope.downloadBL = function() {

        $scope.isDownLoading = true;
        OrdersService.bonDeLivraison({
            id : $scope.order.id
        }, function(response) {

            // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            var linkElement = document.createElement('a');
            try {

                // var blob = new Blob(response, { type:
                // contentType });
                var url = window.URL.createObjectURL(response.blob);

                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", response.filename);

                var clickEvent = new MouseEvent("click", {
                    "view" : window,
                    "bubbles" : true,
                    "cancelable" : false
                });
                linkElement.dispatchEvent(clickEvent);

                $scope.isDownLoading = false;

            } catch (ex) {
                console.log(ex);
                $scope.isDownLoading = false;
            }
        });

    }

    $scope.getDate = function() {
        var retour = "";
        if ($scope.order) {
            switch ($scope.order.statusOrder) {
            case 0:
                retour = $scope.order.creationDate;
                break;
            case 1:
                retour = $scope.order.creationDate;
                break;
            case 2:
                retour = $scope.order.planifDate;
                break;
            case 3:
                retour = $scope.order.prepDate;
                break;
            case 4:
                retour = $scope.order.controlDate;
                break;
            case 5:
                retour = $scope.order.delivDate;
                break;
            case 6:
                retour = $scope.order.receptDate;
                break;
            }
        }
        return retour;
    }

    $scope.getUser = function() {
        var retour = "";
        if ($scope.order) {
            switch ($scope.order.statusOrder) {
            case 0:
                retour = $scope.order.username;
                break;
            case 1:
                retour = $scope.order.username;
                break;
            case 2:
                retour = $scope.order.planifUserName;
                break;
            case 3:
                retour = $scope.order.prepUserName;
                break;
            case 4:
                retour = $scope.order.controlUserName;
                break;
            case 5:
                retour = $scope.order.delivUserName;
                break;
            case 6:
                retour = $scope.order.receptUserName;
                break;
            }
        }
        return retour;
    }
    
});

oseModule.controller('orderDetailController', function orderDetailController($rootScope, $scope, OrdersService, LocationsService) {

    $scope.editOrderDetail = function(value) {
        $rootScope.$broadcast('startEditQuantity');
        $scope.editQuantity = true;
        $scope.incertain = true;
    }

    $scope.saveOrderDetail = function(value) {
        OrdersService.saveOrderDetail($scope.orderDetail, function(response) {
            $scope.orderDetail = response.content;
            $rootScope.$broadcast('stopEditQuantity');
            $scope.editQuantity = false;
            $scope.incertain = true;
        })
    }

});

oseModule.controller('palettesModalFormController', function palettesModalFormController($scope, $location, $routeParams, $route,
        OrdersService, UsersService) {

    $scope.parameter = {};

    if (angular.isDefined($scope.paletteId)) {
        OrdersService.getPalette({
            paletteId : $scope.paletteId
        }, function(response) {
            $scope.palette = response.content;
        });
    }

    if (angular.isDefined($scope.orderId)) {
        OrdersService.getProductsLocations({
            orderId : $scope.orderId,
        }, function(response) {
            $scope.productsLocations = response.content;
        });
    }

    UsersService.getAllByPermission({
        permission : "PERM_COMMANDE_PREPARATION"
    }, function(response) {
        $scope.users = response.content;
    });

    $scope.generatePalette = function() {
        OrdersService.generatePalette({
            orderId : $scope.orderId,
            locationFrom : $scope.parameter.locationFrom,
            numberOfLocation : $scope.parameter.numberOfLocation
        }, function(response) {
            $scope.palette = response.content;
        });
    };

    $scope.savePalette = function() {
        OrdersService.savePalette($scope.palette, function(response) {
            $scope.modalInstance.close(response.content.id);
        })
    };

    $scope.deletePaletteDetail = function(v1) {
        OrdersService.deletePaletteDetail({
            paletteId : $scope.palette.id,
            paletteDetailId : v1
        }, function(response) {
            $scope.palette = response.content;
        })
    };

    $scope.cancelPalette = function() {
        $scope.modalInstance.dismiss("cancel");
    };

});

oseModule.controller('selectPalettesFormController', function selectPalettesFormController($scope, $location, $routeParams, $route,
        OrdersService) {

    OrdersService.getPalettes({
        orderId : $scope.orderId
    }, function(response) {
        $scope.palettes = response.content;
    });

    $scope.addToPalette = function(value) {
        OrdersService.addToPalette({
            paletteId : $scope.palette.id,
            orderDetailId : $scope.orderDetailId
        }, function(response) {
            $scope.modalInstance.close(response.content);
        })
    };

    $scope.cancel = function() {
        $scope.modalInstance.dismiss("cancel");
    };

});

oseModule.directive('singletonSelect', function singletonSelect() {
    return {
        require : 'uiSelect',
        link : function(scope, element, attrs, $select) {
            scope.$watch('$select.items', function() {
                if ($select.items.length == 1) {
                    $select.select($select.items[0], true);
                }
            });
        }
    };
});

oseModule.controller('palettesFormController', function palettesFormController($scope, $location, $routeParams, $route, OrdersService,
        ProductsService,MessagesService) {

    $scope.criteria = {}
    $scope.isLoading = true;
    $scope.status = $location.search()["status"];
    $scope.statusPalette = $location.search()["statusPalette"];
    $scope.paletteId = $location.search()["paletteId"];

    $scope.statusString = "";
    $scope.paletteStatusString = "";

    $scope.edit = false;

    $scope.noValidation = false;

    if ($scope.status) {
        $scope.statusString = "status=" + $scope.status + "&";
    }
    if ($scope.statusPalette) {
        $scope.paletteStatusString = "statusPalette=" + $scope.statusPalette + "&";
    }

    if (!angular.isDefined($scope.paletteId)) {
        OrdersService.getPalettesByCriteria({
            statusOrder : $scope.status,
            statusPalette : $scope.statusPalette
        }, function(response) {
            $scope.palettes = response.content;
            // console.log($scope.palettes );
            $scope.isLoading = false;
            $scope.$broadcast('SetFocus');
        });
    } else {
        OrdersService.getPalette({
            paletteId : $scope.paletteId
        }, function(response) {
            $scope.palette = response.content;
            $scope.isLoading = false;
            $scope.$broadcast('SetFocus');
        });
    }

    $scope.$watch('selected', function(fac) {
        $scope.$broadcast("rowSelected", fac);
    });

    $scope.editPalette = function() {
        $scope.edit = true;
    }

    $scope.savePalette = function() {
        OrdersService.savePalette($scope.palette, function(response) {
            $scope.edit = false;
        })

    }

    $scope.getPalette = function() {
        $location.url("/palettes/form?" + $scope.statusString + $scope.paletteStatusString + "paletteId=" + $scope.criteria.palette.id);
    };

    $scope.getProducts = function(value) {
        $scope.renflouementId  = undefined;
        if ($scope.requeteProduits != undefined) {
            $scope.requeteProduits.$cancelRequest();
        }
        if (value != "") {
            $scope.isScanLocation = false;
            $scope.paletteDetail.product = undefined;
            $scope.products = undefined;
            $scope.requeteProduits = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : false
            });
            $scope.requeteProduits.$promise.then(function(response) {
                if (response.content != null && response.content.length > 0) {
                    $scope.products = response.content;
                    $scope.inputValue = value;
                } else {
                    $scope.requetePalettes = OrdersService.getProductsByEmplacement({
                        paletteId : $scope.palette.id,
                        emplacement : value,
                        limit : 15
                    });
                    $scope.requetePalettes.$promise.then(function(response) {
                        $scope.products = response.content;
                        $scope.isScanLocation = true;
                        $scope.inputValue = value;
                    }, function(httpResponse) {
                        // console.log("Annulée ou erreur");
                    });

                }
            }, function(httpResponse) {
                // console.log("Annulée ou erreur");
            });

        }
    }

    $scope.paletteDetail = {};

    $scope.fillPaletteQuantity = function() {
        var msg = "ATTENTION, le preremplissage ecrasera vos saisies manuelles deja realisees.\n\n\t\t\t\t\tVoulez-vous continuer ?";
        if (confirm(msg)) {
            OrdersService.fillPaletteQuantity({
                paletteId : $scope.palette.id,
            }, function(response) {
                $scope.palette = response.content;
            });
        }
    }

    $scope.sortPaletteDetail = function(v1, v2) {
        // var coef = $scope.LastStockLocationName <
        // $scope.stockLocationName? 1 : -1;
        var coef = $scope.LastStockLocationName && $scope.stockLocationName
        && ($scope.LastStockLocationName[0] === $scope.stockLocationName[0]) ? $scope.stockLocationName
                .localeCompare($scope.LastStockLocationName) : 1;

                if (v1.type === 'boolean' || v2.type === 'boolean') {
                    return v1.value === v2.value ? 0 : (v1.value < v2.value) ? -1 : 1;
                }

                if (v1.type !== 'string' || v2.type !== 'string') {
                    return (v1.index < v2.index) ? -1 : 1;
                }

                // Compare strings alphabetically, taking locale into
                // account

                // si on est dans la même allée on et que l'on est dans
                // l'ordre inverse
                // on inverse

                /*
                 * if($scope.stockLocationName && coef < 0 && (v1.value[0] === v2.value[0]) &&
                 * (v1.value[0] === $scope.stockLocationName[0])) { return
                 * v2.value.localeCompare(v1.value); }
                 */

                // si on est avant le dernier emplacement visité et
                // qu'on compare avec un emplacement après, on inverse
                if ($scope.stockLocationName && v1.value.localeCompare(v2.value) < 0 && v1.value.localeCompare($scope.stockLocationName) < 0) {
                    // console.log($scope.stockLocationName);
                    return v2.value.localeCompare(v1.value)
                }

                return v1.value.localeCompare(v2.value);

    }

    $scope.addPaletteDetail = function(value, inputValue) {
        if ((!value && $scope.paletteDetail.product && $scope.paletteDetail.quantity) || (value && $scope.paletteDetail.product)) {
            $scope.noValidation = true;
            OrdersService.canAddToPalette({
                status : $scope.status,
                statusPalette : $scope.statusPalette,
                paletteId : $scope.palette.id,
                productId : $scope.paletteDetail.product.id,
                quantity : $scope.paletteDetail.quantity,
            }, function(response) {
                if (response.content < 0) {
                    var msg = 'ATTENTION CE PRODUIT NE DOIT PAS ETRE AJOUTE A LA PALETTE.\n\n\nVEUILLEZ LE RETIRER.';
                    alert(msg);
                    $scope.paletteDetail = {
                            product : undefined
                    };
                    $scope.products = [];
                    $scope.noValidation = false;
                    $scope.$broadcast('SetFocus');
                }
                if($scope.paletteDetail.product.alertePreparation) {
                    var msg = $scope.paletteDetail.product.alertePreparation;
                    confirm(msg);
                }
                if (response.content == 0) {
                    OrdersService.updatePaletteDetail({
                        status : $scope.status,
                        statusPalette : $scope.statusPalette,
                        paletteId : $scope.palette.id,
                        productId : $scope.paletteDetail.product.id,
                        quantity : $scope.paletteDetail.quantity,
                        close : value,
                        inputOrigine : inputValue
                    }, function(response) {
                        $scope.palette = response.content;
                        $scope.LastStockLocationName = $scope.stockLocationName;
                        $scope.stockLocationName = $scope.paletteDetail.stockLocationName;
                        $scope.paletteDetail = {
                                product : undefined
                        };
                        $scope.products = [];
                        if ($scope.preSelected != undefined) {
                            $scope.selected = $scope.preSelected;
                            $scope.preSelected = undefined;
                        }
                        $scope.noValidation = false;
                        $scope.$broadcast('SetFocus');
                    })
                }
                if (response.content > 0) {
                    var msg = "ATTENTION, vous avez mis sur la palette une quantite plus importante que prevu."
                        + "\n\n\t\t\t\t\tVoulez-vous continuer ?";
                    if (confirm(msg)) {
                        OrdersService.updatePaletteDetail({
                            status : $scope.status,
                            statusPalette : $scope.statusPalette,
                            paletteId : $scope.palette.id,
                            productId : $scope.paletteDetail.product.id,
                            quantity : $scope.paletteDetail.quantity,
                            close : value,
                            inputOrigine : inputValue
                        }, function(response) {
                            $scope.palette = response.content;
                            $scope.LastStockLocationName = $scope.stockLocationName;
                            $scope.stockLocationName = $scope.paletteDetail.stockLocationName;
                            $scope.paletteDetail = {
                                    product : undefined
                            };
                            $scope.products = [];
                            if ($scope.preSelected != undefined) {
                                $scope.selected = $scope.preSelected;
                                $scope.preSelected = undefined;
                            }
                            $scope.noValidation = false;
                            $scope.$broadcast('SetFocus');
                        })
                    } else {
                        $scope.noValidation = false;
                    }
                }
            });
        }
    }

    $scope.nextPaletteStep = function() {
        var msg = "Vous tentez de terminer la palette.\n\n\tVoulez-vous continuer ?";
        var tmp = $scope.statusString + $scope.paletteStatusString;
        if (tmp.length > 0) {
            // On enlève le & de liaison
            tmp = tmp.substring(0, tmp.length - 1);
        }
        if (confirm(msg)) {
            $scope.isLoading = true;
            OrdersService.nextPaletteStep({
                status : $scope.status,
                statusPalette : $scope.statusPalette,
                paletteId : $scope.palette.id
            }, function(response) {
                $scope.isLoading = false;
                if ($scope.palette.id != response.content.id) {
                    if (confirm("Une nouvelle palette a ete cree avec le reliquat.\n\nVoulez-vous la preparer maintenant ?")) {
                        $location.url("/palettes/form?" + $scope.statusString + $scope.paletteStatusString + "paletteId=" + response.content.id);
                    } else {
                        $location.url("/palettes/form?" + tmp);
                    }
                } else {
                    $location.url("/palettes/form?" + tmp);
                }
            });
        }
    }

    $scope.updateQuantity = function(product, isScanLocation) {
        if ($scope.palette.paletteDetails) {
            for (var i = 0; i < $scope.palette.paletteDetails.length; i++) {
                if ($scope.palette.paletteDetails[i].productCode == $scope.paletteDetail.product.barcode) {
                    $scope.paletteDetail.quantity = 1;
                    if ($scope.status == 3 || $scope.statusPalette == 3) {
                        if (isScanLocation) {
                            $scope.paletteDetail.quantity = 0;
                        } else {
                            $scope.paletteDetail.quantity = $scope.palette.paletteDetails[i].quantityToPrepare
                            - $scope.palette.paletteDetails[i].quantityPrepared;
                        }
                        $scope.paletteDetail.stockLocationName = $scope.palette.paletteDetails[i].stockLocationName;
                    }
                    if ($scope.status == 5 || $scope.statusPalette == 5) {
                        $scope.paletteDetail.quantity = $scope.palette.paletteDetails[i].quantitySent
                        - $scope.palette.paletteDetails[i].quantityReceived;
                        $scope.paletteDetail.stockLocationName = $scope.palette.paletteDetails[i].stockLocationName;
                    }
                    if (i +1 < $scope.palette.paletteDetails.length) {
                        $scope.preSelected = $scope.palette.paletteDetails[i + 1].productCode;
                    }
                    break;
                }
            }
            qtyIsFocused = true;
        }
    }
    $scope.createRenflouement = function(inputValue) {
        if($scope.paletteDetail.product) {
        //$scope.addPaletteDetail(true,inputValue)
        //On ne fait rien d'autre que déclarer un besoin de renflouement
        //On décommentera si il faut un jour marquer comme fait
        MessagesService.createRenflouement({
            productId : $scope.paletteDetail.product.id,
            content : inputValue
        }, function(response) {
            $scope.renflouementId = response;
            var button = $(".change"); 
            button.addClass("btn-success"); 
            button.removeClass("btn-primary"); 
           
        })
        }
    }

});

oseModule.controller('supervisionDashboardController', function supervisionDashboardController($rootScope, $scope, $interval,
        PropertiesService, OrdersService, LocationsService) {

    $scope.locationOptions = [ {
        id : 1000,
        nom : "Magasins",
        checked : true
    }, {
        id : 2000,
        nom : "Camions",
        checked : true
    }, {
        id : 4000,
        nom : "Grands comptes",
        checked : true
    }, {
        id : 5000,
        nom : "Diamant",
        checked : true
    } ];

    $scope.statusOptions = [ {
        id : 2,
        nom : "Planification",
        checked : true
    }, {
        id : 3,
        nom : "Preparation",
        checked : true
    }, ];

    var refresh = {};

    PropertiesService.getSupervisionInterval(function(response) {
        $scope.interval = response.content;
        refresh = $interval(updateOrders, $scope.interval.ordersInterval);
		updateOrders();
    });

    $scope.orders = [];
    $scope.criteria = {
            listCategoryIn : [ 1000, 2000, 4000, 5000 ],
            listStatusNotIn : [ 1, 4, 5, 6 ],
    }

    $scope.updateOrders = function() {
        $scope.criteria.listCategoryIn = [];
        $scope.criteria.listStatusNotIn = [ 1, 4, 5, 6 ];
        updateCriteria($scope.criteria.listCategoryIn, $scope.locationOptions, true);
        updateCriteria($scope.criteria.listStatusNotIn, $scope.statusOptions, false);
        updateOrders();
    }

    function updateCriteria(criteriaList, values, value) {
        for (var i = 0; i < values.length; i++) {
            var option = values[i];
            if (option.checked === value) {
                criteriaList.push(option.id);
            }
        }
        if (criteriaList.length == 0) {
            criteriaList.push('empty')
        }
    }

    function updateOrders() {
        if ($scope.request != undefined) {
            $scope.request.$cancelRequest();
        }
        $scope.request = OrdersService.getAllByCriteria({
            listCategoryIn : $scope.criteria.listCategoryIn,
            listStatusNotIn : $scope.criteria.listStatusNotIn,
        });
        $scope.request.$promise.then(function(response) {
            if ($scope.orders.length != 0) {
                removeOrders(response.content);
                pushNewOrders(response.content);
            } else {
                $scope.orders = response.content;
            }
            $scope.isLoading = false;
            $rootScope.$broadcast('updateCards');
        });
    }

    function removeOrders(values) {
        for (var i = 0; i < $scope.orders.length; i++) {
            var order = $scope.orders[i];
            if (!contains(values, order)) {
                $scope.orders.splice(i, 1);
                i--;
            }
        }
    }

    function pushNewOrders(values) {
        for (var i = 0; i < values.length; i++) {
            var order = values[i];
            if (!contains($scope.orders, order)) {
                $scope.orders.unshift(order);
            }
        }
    }

    function contains(array, value, property) {
        if (property === undefined) {
            property = "id";
        }
        for (var i = 0; i < array.length; i++) {
            if (array[i][property] === value[property]) {
                return true;
            }
        }
        return false;
    }

    $scope.$on('$destroy', function() {
        $interval.cancel(refresh)
    });
});

oseModule.controller('orderCardController', function orderCardController($scope, $interval, OrdersService, LocationsService) {

    $scope.details = [];

    $scope.$on('updateCards', updateOrderDetail());

    var refresh = $interval(updateOrderDetail, $scope.interval.detailsInterval);

    function updateOrderDetail() {
        if ($scope.order.codeStatus > 2) {
            OrdersService.getPalettePreparationInfo({
                orderId : $scope.order.id,
            }, function(response) {
                $scope.preparationDetails = response.content;
                updateAvancement();
            });
        }
    }

    function updateAvancement() {
        var total = 0;
        var raf = 0;
        var pal = 0;
        for (var i = 0; i < $scope.preparationDetails.length; i++) {
            var detail = $scope.preparationDetails[i];
            total += detail.nombreEmplacement;
            raf += detail.nombreEmplacementRestant;
            if (detail.nombrePalette != undefined) {
                pal += new Number(detail.nombrePalette);
            }
        }
        for (var i = $scope.preparationDetails.length; i < 6; i++) {
            $scope.preparationDetails.push({});
        }
        $scope.order.avancement = (total - raf) / total * 100;
        $scope.order.nbEmplOK = total - raf;
        $scope.order.nbEmplTotal = total;
        $scope.order.prepared = ($scope.order.avancement >= 100);

        $scope.order.nbPalettes = pal;
    }

    $scope.$on('$destroy', function() {
        $interval.cancel(refresh)
    });
});

oseModule.controller('renflouementDashboardController', function ruptureDashboardController($rootScope, $scope, $interval,PropertiesService,
         MessagesService,ProductsService,UsersService) {
    var refresh = {};

    $scope.criteria = {}

    $scope.typerenfloument = "A renflouer";
    $scope.statusArenflouer = true;
    $scope.types =  [ 
        {
            statusArenflouer : true,
            label : "A renflouer"
        },
        {
            statusArenflouer : false,
            label : "Renfloué"
        },
        {
            statusArenflouer : null,
            label : "Tout"
        },
        ]

    //Il y a un sur ose-server $scope.getMessages mais qui fait la même chose
    //Je ne porte pas du coup
    $scope.searchMessages = function() {
        
        if (!angular.isDefined($scope.criteria.from)  && !angular.isDefined($scope.criteria.to)){
            $scope.criteria.from = $scope.resetHours(moment().subtract(30, 'days')).getTime();
        }
        if (angular.isDefined($scope.criteria.product) && $scope.criteria.product != null) {
            $scope.productId =  $scope.criteria.product.id;
        }
        if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
            $scope.from = $scope.resetHours($scope.criteria.from).getTime();
        }
        if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
            $scope.to = $scope.upgHours($scope.criteria.to).getTime();
        }
        MessagesService.getMessages({
            typeId : 'RENFLOUEMENT',
            activeOnly : $scope.statusArenflouer,
            productId : $scope.productId,
            from : $scope.from,
            to : $scope.to
          }, function(response) {
            $scope.renflouements = response.content;
            $scope.from = undefined;
            $scope.to = undefined;
          });
    }
    
    PropertiesService.getProdcutRenflouementInterval(function(response) {
        $scope.interval = response.content;
        refresh = $interval($scope.searchMessages, $scope.interval);
    });
    
    $scope.updateRenflouement = function(renfloument) {
        MessagesService.updateRenflouement({
            id : renfloument.id
        }, function(response) {
            if (response.content == 'KO'){ // demander à aurelien en cas ce n'est aps le meme user qui va cloturer le renfloument
                alert("Vous pouvez pas cl\u00f4turer ce renflouement , si vous n'\352tes pas la personne l'ayant commenc\351!!");
            } else {
                $scope.searchMessages();   
            }    
        }
        )
    }
    
    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
      };

      $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
      };
      
      $scope.resetHours = function(theDate) {
          var mmt = moment(theDate);
          mmt.seconds(0);
          mmt.minutes(0);
          mmt.hours(0);
          return mmt.toDate();
      }
      
      $scope.upgHours = function(theDate) {
          var mmt = moment(theDate);
          mmt.seconds(59);
          mmt.minutes(59);
          mmt.hours(23);
          return mmt.toDate();
      }

      $scope.getProducts = function(value) {
          if ($scope.requeteProduits != undefined) {
              $scope.requeteProduits.$cancelRequest();
          }
          if (value != "") {
              $scope.criteria.product = undefined;
              $scope.requeteProduits = ProductsService.getCriteria({
                  reference : value,
                  limit : 15,
                  composition : false
              });
              $scope.requeteProduits.$promise.then(function(response) {
                  $scope.products = response.content;
                  if ($scope.products.length == 1) {
                      $scope.criteria.product = $scope.products[0];
                  }
              }, function(httpResponse) {
              });
          }
      }
      $scope.$on('$destroy', function() {
          $interval.cancel(refresh)
      });
      
     $scope.searchMessages();
    
});
