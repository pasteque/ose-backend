
oseModule.factory('PropertiesService', function($resource) {
	return $resource('/properties/:param1/:value1', {
		param1 : '@param1',
		value1 : '@value1'
	}, {
		stockHistory : {
			method : 'GET',
			params : {
			  param1: 'limit',
				value1 : 'stockHistory'
			},
			isArray : false
		},
		detailedExtract : {
			method : 'GET',
			params : {
        param1: 'limit',
				value1 : 'detailedExtract'
			},
			isArray : false
		},
		getSupervisionInterval : {
		  method : 'GET',
      params : {
        param1 : 'supervisionInterval'
      },
      isArray : false
		},
		getProdcutRenflouementInterval : {
	          method : 'GET',
	          params : {
	              param1 : 'productRenflouementInterval'
	              },
	          isArray : false
	    }
	});
});
