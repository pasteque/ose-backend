oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/', {
        templateUrl : '/partial/admin/dashboard-status.html',
        controller : 'indexController'
    });

} ]);

oseModule.directive('horloge', function () {
	return {
		link: function (scope, element, attrs) {
			function afficheHeure(){
				scope.currentTime = new Date();
				scope.$apply();
			}
			var interval = setInterval(afficheHeure,1000);
		}
	};
});

oseModule.factory('StatusService', function($resource) {
	return $resource('/status/:param1/:value1', {
		param1 : '@param1',
		value1 : '@value1'
	}, {
		general : {
			method : 'GET',
			params : {
				param1 : 'general'
			},
			isArray : false
		},
		cashes : {
			method : 'GET',
			params : {
				param1 : 'cashes'
			},
			isArray : false
		}
	});
});

oseModule.controller('indexController', function($scope, $http , $location , StatusService , LocationsService) {

    $scope.sortType = 'locationId';
    $scope.sortReverse = false;
    
    $scope.location = 'test';
    $scope.error = '' ;
    if ($location.search()['error']) {
        $scope.error = 'L\'identifiant/mot de passe est incorrect.';
    }
    
    
	$scope.diffDateSeconds = function (d1,d2,abs){
		if (d2 != undefined && d1 != undefined && typeof d2.getTime === "function" && typeof d1.getTime === "function") { 
		var WNbms = d2.getTime() - d1.getTime();
		WNbms = Math.ceil(WNbms/(1000));
		if(abs) {
			WNbms = Math.abs(WNbms)
		}
		return WNbms;
		} else {
			//console.log(d2);
			//console.log(d1);
			return 0;
		}
	}
		
	
	$scope.currentTime = new Date();
	$scope.dateProd = undefined;
	$scope.dateStock = undefined;
	$scope.dateDeb = undefined;
	$scope.dateTick = undefined;
	
	$scope.cashesInfo = [];
	
	LocationsService.getParentLocations({
        excludeBase : true},function(response) {
        	$scope.salesLocations = []

        if(response.content && response.content.length > 0){
            for (i=0; i < response.content.length; i++) {    
            	$scope.searchSalesOverView((response.content[i]).id);
             };  
         }
    });
	
	$scope.searchSalesOverView =  function( locationId) {
		StatusService.cashes({
            locationId : locationId
        },  function(response) {
			if (response.content != null) {
				var noDefault = true;
				angular.forEach(response.content, function(foo) {
				    var lastDate = new Date(foo[4]);
				    var now = new Date();
					noDefault = noDefault && !foo[9] ;
					if(now.getTime() - lastDate.getTime() < 1000*3600*24*100){
    					$scope.cashesInfo.push({
    						locationId :foo[0],
    						locationName :foo[1],
    						cashIdId:foo[2],
    						cashName :foo[3],
    						lastOpen : lastDate,
    						lastClose : new Date(foo[5]),
    						lastTicket :foo[6],
    						lastTicketHour : new Date(foo[7]),
    						url : ( noDefault ? foo[8] : null ),
    						available : foo[10]
    					})
					}
					
	            });
				
				if(!noDefault){
					angular.forEach($scope.cashesInfo, function(foo) {
						foo.url = null;
					});
					
				}
			}
		});
	}
	
	StatusService.general( function(response) {
		
		if (response.content != null ) {
			$scope.dateProd = new Date(response.content[0]);
			$scope.dateStock = new Date(response.content[1]);
			$scope.dateDeb = new Date(response.content[2]);
			$scope.dateTick = new Date(response.content[3]);
		}
	});
	
	
	
});
