'use strict';

oseModule.directive('hasPermission', function(PermissionsService) {
  return {
    link : function(scope, element, attrs) {
      // if(!_.isString(attrs.hasPermission)) {
      // throw 'hasPermission value must be a string'
      // }
      var value = attrs.hasPermission.trim();
      var notPermissionFlag = value[0] === '!';
      if (notPermissionFlag) {
        value = value.slice(1).trim();
      }

      function toggleVisibilityBasedOnPermission() {
        var hasPermission = PermissionsService.hasPermission(value);

        if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
          element.show();
        } else {
          element.hide();
        }
      }

      toggleVisibilityBasedOnPermission();
      scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
    }
  };
});

/**
 * Directive pour gérer l'affichage du menu, un peu basique car basé sur le code
 * des permissions du menu
 * 
 * @param PermissionsService
 * @returns
 */
oseModule.directive('hasMenuPermission', function(PermissionsService) {
  return {
    link : function(scope, element, attrs) {
      // if(!_.isString(attrs.hasPermission)) {
      // throw 'hasPermission value must be a string'
      // }
      var value = attrs.hasMenuPermission.trim();
      var notPermissionFlag = value[0] === '!';
      if (notPermissionFlag) {
        value = value.slice(1).trim();
      }

      function toggleVisibilityBasedOnPermission() {
        var hasPermission = PermissionsService.hasMenuPermission(value);

        if (hasPermission && !notPermissionFlag || !hasPermission && notPermissionFlag) {
          element.show();
        } else {
          element.hide();
        }
      }

      toggleVisibilityBasedOnPermission();
      scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
    }
  };
});

oseModule.factory('PermissionsService', function($rootScope) {
  var permissionList;
  return {
    setPermissions : function(permissions) {
      permissionList = permissions;
      $rootScope.$broadcast('permissionsChanged');
    },
    hasPermission : function(permission) {
      permission = permission.trim();
      var response = false;
      for (var i = 0; !response && permissionList != null && i < permissionList.length; i++) {
        var item = permissionList[i];

        response = (item.trim() === permission)
      }

      return response;
      // return _.some(permissionList, function(item) {
      // if(_.isString(item.Name)) {
      // return item.Name.trim() === permission
      // }
      // });
    },
    hasMenuPermission : function(permission) {
      permission = permission.trim();
      var response = false;
      for (var i = 0; !response && permissionList != null && i < permissionList.length; i++) {
        var item = permissionList[i];

        response = (item.trim().startsWith(permission))
      }

      return response;
    }
  };
});

oseModule.controller('habilitationController', function($scope, $http, $location, $interval, $rootScope, $uibModal, UsersService,
    PermissionsService) {
  $scope.name = "habilitationController";
  $scope.url = $location.protocol() + "://" + $location.host() + ":" + $location.port();

  $scope.location = undefined;

  $http.get('/authentication/user').success(function(response) {
    if (response == "logout") {
      $location.url('/login');
    } else {
      $scope.user = response;
      PermissionsService.setPermissions($scope.user.authorizations);
      $scope.alive = true;
    }
  }).error(function(response) {
  });

  $http.get('/locations/getDefault').success(function(response) {
    $scope.location = response.content ? response.content.label : undefined;

  }).error(function(response) {

    $scope.location = undefined;
  });

  var alive = $interval(function() {
    $http.get('/authentication/alive').then(function(response) {
      $scope.alive = response;
    }, function(response) {
      $scope.alive = false;
    });
  }, 60000);

  var menuModalScope = $rootScope.$new();
  $scope.openPwChangeNSave = function($user) {
    menuModalScope.users = undefined;
    menuModalScope.user = $user;
    menuModalScope.pass = {
      old : "",
      newpw : "",
      repeatnewpw : "",
      alert : undefined
    };
    menuModalScope.modalInstance = $uibModal.open({
      templateUrl : '/partial/admin/users-change-password.html',
      controller : 'usersPwController',
      scope : menuModalScope
    });

    menuModalScope.modalInstance.result.then(function(selectedItem) {
      if (selectedItem) {
        $user.password = selectedItem.newpw;
        UsersService.updatePassword({
          id : $user.id,
          oldPwd : selectedItem.old,
          newPwd : selectedItem.newpw
        }, function(response) {
          if (response.status == "ok") {
            alert("Le mot de passe pour " + $user.name + " a été changé !");
          }
          if (response.status == "ko") {
            alert("Erreur Lors de la sauvegarde du Mot de Passe - Aucun changement !");
          }
        });
      }
    }, function() {
      // $log.info('Modal dismissed at: '
      // + new Date());
    });
  };

});
