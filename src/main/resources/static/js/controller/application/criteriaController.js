'use strict';
oseModule.factory('CriteriaService', function CriteriaService($location) {
    function getParamsUrl(scope) {
        if (angular.isDefined($location.search()['criteria'])) {
            try { 
                scope.criteria = angular.fromJson(atob($location.search()['criteria']));
            } catch (err) {
                // au cas ou ce n'est pas encodé
                scope.criteria =  angular.fromJson($location.search()['criteria']);
            }
            // pour garder le format date, pour les select date recoit la bon format
            Object.keys(scope.criteria).forEach(function(key){
                var value = scope.criteria[key];
                //console.log(key + ':' + value +' parse: '+ Date.parse(value));
                if (isNaN(value) && !isNaN(new Date(value)) ){
                    scope.criteria[key] = new Date(value);
                }
                
            });
            return true;
        }else{
            return false;
        }
    }
    return {
       
        getParamsUrl : function(scope) {
            return getParamsUrl(scope);
        }
    };

});