oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/tickets/list', {
    templateUrl : '/partial/tickets/tickets-list.html',
    controller : 'ticketsListController',
    reloadOnSearch : false
  });
  $routeProvider.when('/tickets/:id/form', {
    templateUrl : '/partial/tickets/tickets-form.html',
    controller : 'ticketsFormController'
  });
  $routeProvider.when('/tickets/:id/z', {
    templateUrl : '/partial/tickets/ztickets-form.html',
    controller : 'ticketsZController'
  });

} ]);

oseModule.factory('TicketsService', function TicketsService($resource) {
  return $resource('/tickets/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    search : {
      method : 'GET',
      params : {
        param1 : 'search'
      },
      isArray : false,
      cancellable : true
    },
    getTickets : {
      method : 'GET',
      params : {
        param1 : 'get'
      },
      isArray : false
    },
    findCustomerTickets : {
      method : 'GET',
      params : {
        param1 : 'findCustomerTickets'
      },
      isArray : false
    },
    findTicketExtendedCustomer : {
      method : 'GET',
      params : {
        param1 : 'findTicketExtendedCustomer'
      },
      isArray : false
    },
    updateTicketExtended : {
          method : 'GET',
      params : {
        param1 : 'updateTicketExtended'
      },
      isArray : false
    },
    download : {
      method : 'GET',
      params : {
        param1 : 'search',
        value1 : 'download'
      },
      responseType : 'arraybuffer',
      cancellable : true,
      transformResponse : function(data, headers) {
        var response = {};
        response.headers = headers();
        response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');

        response.contentType = headers('content-type');
        if (data) {
          response.blob = new Blob([ data ], {
            type : response.contentType
          });
        }
        return response;
      }
    },
  });
});

oseModule.factory('CashesService', function CashesService($resource) {
  return $resource('/cashes/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    search : {
      method : 'GET',
      params : {
        param1 : 'search'
      },
      isArray : false,
      cancellable : true
    },
    getZTickets : {
      method : 'GET',
      params : {
        param1 : 'zticket'
      },
      isArray : false
    },
    getCashes : {
      method : 'GET',
      params : {
        param1 : 'get'
      },
      isArray : false
    }
  });
});

oseModule.factory('CashRegisterService', function CashRegisterService($resource) {
  return $resource('/cash-registers/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getById : {
      method : 'GET',
      params : {
        param1 : 'getById'
      },
      isArray : false,
      cancellable : true
    },
    getByCriteria : {
      method : 'GET',
      params : {
        param1 : 'getByCriteria'
      },
      isArray : false,
      cancellable : true
    }
  });
});

oseModule.controller('ticketsListController', function ticketsListController($scope, $window, $location , PropertiesService, TicketsService, LocationsService,
    ToursService, SalesLocationsService, CriteriaService) {
  $scope.isLoading = false;
  $scope.isDownLoading = false;
  $scope.minDate = undefined;

  $scope.sortType = undefined;
  $scope.sortReverse = false;

  PropertiesService.detailedExtract({}, function(response) {
    var mnt = moment().subtract(response.content, 'days').hours(0).minutes(0).seconds(0);
    $scope.minDate = mnt;

  });

  $scope.options = [ {
    id : 0,
    label : "Journ\351es",
    type : "SalesDayOverviewDTO"
  }, {
    id : 1,
    label : "D\351ballages",
    type : "SalesLocationSessionOverviewDTO"
  }, {
    id : 2,
    label : "Tickets Simplifi\351s",
    type : "TicketsResultDTO"
  }, {
    id : 3,
    label : "Tickets D\351taill\351s",
    type : "TicketsIndexDTO"
  }, {
    id : 4,
    label : "R\350glements",
    type : "PaymentsIndexDTO"
  }, {
    id : 5,
    label : "Lignes de tickets",
    type : "TicketLinesIndexDTO"
  }

  ];

   function initializeQueryParams() {
      if (CriteriaService.getParamsUrl($scope)) {
          findTickets();
      } else {  
	      $scope.criteria = {
	           from : moment().subtract(7, 'days').toDate(),
	           to : moment().toDate(),
	           tour : {
	             id : undefined
	           },
	           pdv : {
	             id : undefined
	           },
	           detail : $scope.options[2]
	          }
	          
		     LocationsService.getDefaultSaleLocation(function(response) {
		       if (response.content != null) {
		            $scope.criteria.pdv = { id : response.content.id,
		                    label : response.content.label};
		        }
		      });
	      }
      };
      


  $scope.activate = function(option) {
    $scope.criteria.detail = option;
    if (option.id > 2 && $scope.minDate !== undefined) {

      $scope.dateOptions.minDate = $scope.minDate;
      $scope.criteria.from = $scope.dateOptions.minDate.isBefore($scope.criteria.from)
          || $scope.dateOptions.minDate.isSame($scope.criteria.from, 'day') ? $scope.criteria.from : undefined;
      $scope.criteria.to = $scope.dateOptions.minDate.isBefore($scope.criteria.to)
          || $scope.dateOptions.minDate.isSame($scope.criteria.to, 'day') ? $scope.criteria.to : undefined;
    } else {
      $scope.dateOptions.minDate = null;
    }
  };

  $scope.barLimit = 100;

  $scope.deb_search = "";

  $scope.status = "ok";

  $scope.increaseLimit = function() {
    $scope.barLimit += 50;
  }

  ToursService.getAll({
    minimal : true
  }, function(response) {
    $scope.tours = response.content;
    $scope.tours.unshift({
      id : undefined,
      nom : "-"
    });
  });

  LocationsService.getParentLocations(function(response) {
    $scope.pdvs = response.content;
    var arr = [];
    for (var i = 0; i < $scope.pdvs.length; i++) {
      if (!arr.includes($scope.pdvs[i].category)) {
        arr.push($scope.pdvs[i].category);
      }
    }
    for (var i = 0; i < arr.length; i++) {
      $scope.pdvs.unshift({
        id : undefined,
        label : arr[i],
        category : arr[i]
      });
    }
    $scope.pdvs.unshift({
      id : undefined,
      label : "-",
      category : undefined

    });

  });

  $scope.openFrom = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedFrom = true;
  };

  $scope.openTo = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedTo = true;
  };

  $scope.dateOptions = {
    formatYear : "yyyy",
    startingDay : 1,
    minDate : null,
  };

  $scope.resetHours = function(theDate) {
    var mmt = moment(theDate);
    mmt.seconds(0);
    mmt.minutes(0);
    mmt.hours(0);
    return mmt.toDate();
  }

  $scope.upgHours = function(theDate) {
    var mmt = moment(theDate);
    mmt.seconds(59);
    mmt.minutes(59);
    mmt.hours(23);
    return mmt.toDate();
  }

  $scope.searchTickets = function() {
    $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
    findTickets();
  }
  
  function findTickets(){
    if (! angular.isDefined($scope.myForm) || $scope.myForm.$valid) {
      if ($scope.requeteTicket != undefined) {
        $scope.requeteTicket.$cancelRequest();
      }

      var from = undefined;
      if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
        from = $scope.resetHours($scope.criteria.from).getTime();
      }
      var to = undefined;
      if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
        to = $scope.upgHours($scope.criteria.to).getTime();
      }
      $scope.isLoading = true;

      var saleLocationId = null;
      if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
        saleLocationId = $scope.criteria.location.id;
      }
      
      if (!angular.isDefined($scope.criteria.pdv)) {
        $scope.criteria.pdv = {
         id : undefined,
         category : undefined
        };
      }

      var detail = $scope.criteria.detail == undefined ? $scope.options[2].type : $scope.criteria.detail.type;

      $scope.requeteTicket = TicketsService.search({
        ticketId : $scope.criteria.reference,
        dateStart : from,
        dateStop : to,
        tourId : $scope.criteria.tour.id,
        locationId : $scope.criteria.pdv.id,
        saleLocationId : saleLocationId,
        fromValue : $scope.criteria.fromValue,
        toValue : $scope.criteria.toValue,
        resultType : detail,
        orderConstraint : $scope.criteria.orderConstraint,
        locationCategoryName : $scope.criteria.pdv.id == undefined ? $scope.criteria.pdv.category : undefined
      // resultType : "TicketsIndexDTO",
      });

      $scope.requeteTicket.$promise.then(function(response) {
        $scope.tickets = response.content;
        $scope.status = response.status;
        $scope.isLoading = false;

      }, function(httpResponse) {
        // console.log("Annulée ou erreur");
      });

    }
  }

  $scope.updateDeballages = function() {
    $scope.getDeballages($scope.deb_search);
  }

  $scope.getDeballages = function(value) {

      if (!angular.isDefined($scope.criteria.pdv)) {
        $scope.criteria.pdv = {
         id : undefined,
         category : undefined
        };
      }
        
    if (value != "" || $scope.criteria.from != undefined || $scope.criteria.to != undefined 
        || $scope.criteria.pdv.id != undefined
        || $scope.criteria.tour.id) {
      if ($scope.requeteDeb != undefined) {
        $scope.requeteDeb.$cancelRequest();
      }
      $scope.deb_search = value;
      
      $scope.requeteDeb = SalesLocationsService.getCriteria({
        reference : value,
        from : $scope.criteria.from,
        to : $scope.criteria.to,
        tour : $scope.criteria.tour.id,
        location : $scope.criteria.pdv.id,
        limit : 50
      });
      $scope.requeteDeb.$promise.then(function(response) {
        $scope.locations = response.content;
        if ($scope.locations.length == 1) {
          $scope.criteria.location = $scope.locations[0];
        }
      }, function(httpResponse) {
        // console.log("Annulée ou erreur");
      });
    }

  }

  $scope.download = function(format) {
    if ($scope.myForm.$valid) {
      var from = undefined;
      if (angular.isDefined($scope.criteria.from)) {
        from = $scope.resetHours($scope.criteria.from).getTime();
      }
      var to = undefined;
      if (angular.isDefined($scope.criteria.to)) {
        to = $scope.upgHours($scope.criteria.to).getTime();
      }
      $scope.isDownLoading = true;

      var saleLocationId = undefined;
      if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
        saleLocationId = $scope.criteria.location.id;
      }
      var locationId = undefined;
      if (angular.isDefined($scope.criteria.pdv) && angular.isDefined($scope.criteria.pdv.id)) {
        locationId = $scope.criteria.pdv.id;
      }

      var tourId = undefined;
      if (angular.isDefined($scope.criteria.tour) && angular.isDefined($scope.criteria.tour.id)) {
        tourId = $scope.criteria.tour.id;
      }

      var categoryName = undefined;
      if (angular.isDefined($scope.criteria.pdv) && angular.isDefined($scope.criteria.pdv.category)
          && !angular.isDefined($scope.criteria.pdv.id)) {
        categoryName = $scope.criteria.pdv.category;
      }

      var detail = $scope.criteria.detail == undefined ? $scope.options[2].type : $scope.criteria.detail.type;

      // var url = '/tickets/search?_format=' + format + reference
      // + from + to + tourId + orderConstraint + saleLocationId + locationId +
      // fromValue + toValue + categoryName + "&resultType=" + detail ;
      // console.log(url);
      // window.downloadFile(url);

      TicketsService.download({
        _format : format,
        ticketId : $scope.criteria.reference,
        dateStart : from,
        dateStop : to,
        tourId : tourId,
        locationId : locationId,
        saleLocationId : saleLocationId,
        fromValue : $scope.criteria.fromValue,
        toValue : $scope.criteria.toValue,
        resultType : detail,
        orderConstraint : $scope.criteria.orderConstraint,
        locationCategoryName : categoryName
      }, function(response) {

        // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        var linkElement = document.createElement('a');
        try {

          var url = window.URL.createObjectURL(response.blob);

          linkElement.setAttribute('href', url);
          linkElement.setAttribute("download", response.filename);

          var clickEvent = new MouseEvent("click", {
            "view" : window,
            "bubbles" : true,
            "cancelable" : false
          });
          linkElement.dispatchEvent(clickEvent);

          $scope.isDownLoading = false;

        } catch (ex) {
          console.log(ex);
          $scope.isDownLoading = false;
        }
      });

    }
  }
  
     initializeQueryParams();

});

oseModule.controller('ticketsCustomerController', function ticketsCustomerController($scope, $location, $routeParams,$uibModalInstance, TicketsService, CustomersService) {

    $scope;criteria = {} ;
    $scope.change = false;
    $scope.removeClient = function() {
        $scope.client = undefined;
        $scope.change = true;
    };
    
     $scope.affectationClient = function($customer){
        $scope.client = { 
            customersId : $customer.id,
            customersLabel : $customer.lastName
            };
        if($customer.civilite != 'STE') {
            $scope.client.customersLabel = $customer.firstName + " " + $customer.lastName ;
        }
        $scope.change = true;
     }
    
    $scope.searchCustomers = function() {
          
    var parentConstraint = false;

    var lastname = undefined;
    if (angular.isDefined($scope.criteria.lastname) && $scope.criteria.lastname != null && $scope.criteria.lastname.length > 0) {
      lastname = "{\"name\": \"Nom\",\"column\": \"lastName\",\"searchvalue\": [\"" + $scope.criteria.lastname + "\"]}";
    }

    var firstname = undefined;
    if (angular.isDefined($scope.criteria.firstname) && $scope.criteria.firstname != null && $scope.criteria.firstname.length > 0) {
      firstname = "{\"name\": \"Prénon\",\"column\": \"firstName\",\"searchvalue\": [\"" + $scope.criteria.firstname + "\"]}";
    }
    
    var searchkey = undefined;
    if (angular.isDefined($scope.criteria.searchkey) && $scope.criteria.searchkey != null && $scope.criteria.searchkey.length > 0) {
      searchkey = $scope.criteria.searchkey ;
    }
    
    var email = undefined;
    if (angular.isDefined($scope.criteria.searchmail) && $scope.criteria.searchmail != null && $scope.criteria.searchmail.length > 0) {
      email = "{\"name\": \"e-Mail\",\"column\": \"email\",\"searchvalue\": [\"" + $scope.criteria.searchmail + "\"]}";
    }
    var phone = undefined;
    if (angular.isDefined($scope.criteria.searchphone) && $scope.criteria.searchphone != null && $scope.criteria.searchphone.length > 0) {
      phone = "{\"name\": \"Tel\",\"column\": \"phone\",\"searchvalue\": [\"" + $scope.criteria.searchphone + "\"]}";
    }
    
    $scope.isLoading = true;
    CustomersService.getCriteria({
      _format : 'list',
      lastName : lastname,
      firstName : firstname,
      email : email,
      phone : phone,
      reference : searchkey,
      parentConstraint : parentConstraint,
    }, function(response) {
      $scope.customers = response.content;
      $scope.isLoading = false;
    });
  };
  
  $scope.ok = function() {
    var customer = null;
    if($scope.client && $scope.client.customersId ) {
        customer = $scope.client.customersId;
    }
    TicketsService.updateTicketExtended({
      ticketId : $scope.ticket.ticketId,
      customerId : customer,
    }, function(response) {
      $uibModalInstance.close($scope.change);
    });

  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

});

oseModule.controller('ticketsFormController', function ticketsFormController($scope, $location, $routeParams, $rootScope, $uibModal, TicketsService) {

  var _action = $location.search()['_action'];
  $scope.edit = (_action == "create" || _action == "edit")
  $scope.extended = undefined ;
  
  $scope.getTicketExtendedCustomer = function(){
  TicketsService.findTicketExtendedCustomer({
            ticketId : $routeParams.id,
        }, function(response) {
            $scope.extended = response.content;
        });
  
  }
  
  
  if (_action == "create") {
    $scope.ticket = {};
  } else {
    TicketsService.getTickets({
      ticketId : $routeParams.id,
    }, function(response) {
      $scope.ticket = response.content;
      if(!$scope.ticket.customersId) {
        $scope.getTicketExtendedCustomer();
      }
      if ($scope.ticket.lines && $scope.ticket.lines.length > 0) {
        for (var index = 0; index < $scope.ticket.lines.length; index++) {
          if (!$scope.ticket.lines[index].product && $scope.ticket.lines[index].note) {
            if ($scope.ticket.lines[index].note.indexOf("Produit : ") >= 0) {
              var start = $scope.ticket.lines[index].note.indexOf("Produit : ") + 10;
              $scope.ticket.lines[index].product = {
                label : $scope.ticket.lines[index].note.substring(start)
              };
              var end = $scope.ticket.lines[index].product.label.indexOf("\n");
              if (end >= 0) {
                $scope.ticket.lines[index].product.label = $scope.ticket.lines[index].product.label.substring(0, end)
              }
              var start_string = $scope.ticket.lines[index].note.substring(0, start - 10);
              var end_string = $scope.ticket.lines[index].note.substring(start + $scope.ticket.lines[index].product.label.length + 1);
              $scope.ticket.lines[index].note = start_string + end_string;
            }
          }
        }
      }
    });
  }
  


  $scope.printInvoice = function() {
    var url = "report/generate?__report=/invoice/invoice&ticketId=" + $scope.ticket.ticketId + "&__format=pdf";
    window.open(url, '_blank');
  }
  
  var modalScope = $rootScope.$new();
  $scope.open = function() {
    modalScope.client = $scope.extended;
    modalScope.ticket = $scope.ticket;
    modalScope.modalInstance = $uibModal.open({
      templateUrl : '/partial/tickets/set-ticket-customer-form.html',
      controller : 'ticketsCustomerController',
      scope : modalScope
    });

    modalScope.modalInstance.result.then(function(selectedItem) {
      if (selectedItem) {
        $scope.getTicketExtendedCustomer();
        modalScope.change = false;
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };
  

});

oseModule.controller('ticketsZController', function($scope, $location, $routeParams, CashesService, CashRegisterService) {

  $scope.TotalOverView = {
    mvtAmount : 0,
    incAmount : 0,
    mvt : 0,
    inc : 0,
    cashAmount : 0,
    expCashAmount : 0,
  };

  CashesService.getZTickets({
    id : $routeParams.id,
  }, function(response) {
    $scope.zticket = response.content;
    $scope.TotalOverView = {
      mvtAmount : 0,
      incAmount : 0,
      mvt : 0,
      inc : 0,
      cashAmount : 0,
      expCashAmount : 0,
    };
    for (i = 0; i < $scope.zticket.payments.length; i++) {

      if ($scope.zticket.payments[i].paymentCateg == "Encaissements") {
        $scope.TotalOverView.incAmount += $scope.zticket.payments[i].amount;
        $scope.TotalOverView.inc += $scope.zticket.payments[i].paymentsNumber;
      } else {
        $scope.TotalOverView.mvtAmount += $scope.zticket.payments[i].amount;
        $scope.TotalOverView.mvt += $scope.zticket.payments[i].paymentsNumber;
      }
      if ($scope.zticket.payments[i].type == "cash") {
        $scope.TotalOverView.cashAmount += $scope.zticket.payments[i].amount;
      }
    }

    if (response.content.cashId) {
      CashesService.getCashes({
        id : response.content.cashId,
      }, function(response) {
        $scope.cash = response.content;
        $scope.TotalOverView.expCashAmount = parseFloat($scope.cash.openCash) + $scope.TotalOverView.mvtAmount
            + $scope.TotalOverView.cashAmount;
        CashRegisterService.getById({
          id : response.content.cashRegisterId,
        }, function(response) {
          $scope.cashRegister = response.content;
        });
      });
    }
  });

  $scope.getTaxeName = function(taxName) {
    var retour = "--";
    switch (taxName) {
    case "004":
      retour = "TVA 20%";
      break;
    }
    return (retour);
  }
});
