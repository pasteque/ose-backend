oseModule.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider.when('/admin/jobs/list', {
        templateUrl: '/partial/admin/jobs-list.html',
        controller: 'jobsListController'
    });
    $routeProvider.when('/admin/jobs/form', {
        templateUrl: '/partial/admin/jobs-form.html',
        controller: 'jobsFormController'
    });
    $routeProvider.when('/admin/jobs/:id/form', {
        templateUrl: '/partial/admin/jobs-form.html',
        controller: 'jobsFormController'
    });
    $routeProvider.when('/admin/jobs/logs/list', {
        templateUrl: '/partial/admin/jobs-logs-list.html',
        controller: 'jobsLogsListController'
    });
    $routeProvider.when('/admin/jobs/logs/form', {
        templateUrl: '/partial/admin/jobs-logs-form.html',
        controller: 'jobsLogsFormController'
    });
    $routeProvider.when('/admin/jobs/logs/:id/form', {
        templateUrl: '/partial/admin/jobs-logs-form.html',
        controller: 'jobsLogsFormController'
    });

}]);

oseModule.factory('JobsService', function JobsService($resource) {
    return $resource('/jobs/:param1:id/:value1', {
        param1: '@param1',
        value1: '@value1'
    }, {
            getSens: {
                method: 'GET',
                params: {
                    param1: 'getSens'
                },
                isArray: false
            },
            getTypes: {
                method: 'GET',
                params: {
                    param1: 'getTypes'
                },
                isArray: false
            },
            getEtats: {
                method: 'GET',
                params: {
                    param1: 'getEtats'
                },
                isArray: false
            },
            getCriteria: {
                method: 'GET',
                params: {
                    param1: 'getCriteria'
                },
                isArray: false,
                cancellable: true
            }
        });
});

oseModule.factory('JobsLogsService', function JobsLogsService($resource) {
    return $resource('/jobs/log/:param1:id/:value1', {
        param1: '@param1',
        value1: '@value1'
    }, {
            getLevels: {
                method: 'GET',
                params: {
                    param1: 'getLevels'
                },
                isArray: false
            },
            getCriteria: {
                method: 'GET',
                params: {
                    param1: 'getCriteria'
                },
                isArray: false,
                cancellable: true
            }
        });
});

oseModule.controller('jobsListController', function jobsListController($scope, JobsService, JobsLogsService, LocationsService) {

    $scope.types = undefined;
    $scope.sens = undefined;
    $scope.etats = undefined;
    $scope.isLoading = false;
    $scope.sortType = 'dateCreation';
    $scope.sortReverse = true;

    JobsService.getEtats({}, function (response) {
        $scope.etats = response.content;
    });

    JobsService.getSens({}, function (response) {
        $scope.sens = response.content;
    });

    JobsService.getTypes({}, function (response) {
        $scope.types = response.content;
    });

    $scope.openFrom = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    var dTo = moment().add(1, 'days').toDate();
    var dFrom = moment().subtract(1, 'days').toDate();

    $scope.criteria = {
        etat: 'PRET',
        sens: 'INTERNE',
        dateDeb: dFrom,
        dateFin: dTo,
    }

    $scope.resetHours = function (theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function (theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }

    $scope.searchJobs = function () {
        $scope.isLoading = true;
        $scope.criteria.dateDeb = $scope.resetHours($scope.criteria.dateDeb);
        $scope.criteria.dateFin = $scope.upgHours($scope.criteria.dateFin);

        JobsService.getCriteria($scope.criteria, function (response) {
            $scope.jobs = response.content;
            $scope.isLoading = false;
        });

    };

    $scope.searchJobs();

});

oseModule.controller('jobsFormController', function jobsFormController($scope,  $routeParams, JobsService, JobsLogsService, LocationsService) {
    JobsService.get({
        id : $routeParams.id,
      }, function(response) {
        $scope.job = response.content;
        $scope.destination = undefined;
        LocationsService.get({
            id : $scope.job.destinataire,
        }, function(response) {
            $scope.destination = response.content;
        });
      });
    
});

oseModule.controller('jobsLogsListController', function jobsLogsListController($scope, JobsLogsService) {

    $scope.levels = undefined;
    $scope.isLoading = false;

    JobsLogsService.getLevels({}, function (response) {
        $scope.levels = response.content;
    });

    $scope.openFrom = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    var dTo = moment().add(1, 'days').toDate();
    var dFrom = moment().subtract(1, 'days').toDate();

    $scope.criteria = {
        level: 'ERREUR',
        dateDeb: dFrom,
        dateFin: dTo,
    }

    $scope.resetHours = function (theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function (theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }

    $scope.searchJobsLogs = function () {
        $scope.isLoading = true;
        $scope.criteria.dateDeb = $scope.resetHours($scope.criteria.dateDeb);
        $scope.criteria.dateFin = $scope.upgHours($scope.criteria.dateFin);

        JobsLogsService.getCriteria($scope.criteria, function (response) {
            $scope.jobs = response.content;
            $scope.isLoading = false;
        });

    };
    $scope.searchJobsLogs();

});

oseModule.controller('jobsLogsFormController', function jobsLogsFormController($scope, $routeParams, JobsService, JobsLogsService, LocationsService) {
    JobsLogsService.get({
        id : $routeParams.id,
      }, function(response) {
        $scope.job = response.content;
      });
});

