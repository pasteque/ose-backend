oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/admin/tariff-areas/list', {
    templateUrl : '/partial/admin/tariff-areas-list.html',
    controller : 'tariffAreasListController'
  });

} ]);

oseModule.factory('TariffAreasService', function TariffAreasService($resource) {
  return $resource('/tariff-areas/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    },
    getCurrent : {
      method : 'GET',
      params : {
        param1 : 'getCurrent'
      },
      isArray : false
    },
    getCurrentLinks : {
      method : 'GET',
      params : {
        param1 : 'getCurrentLinks'
      },
      isArray : false
    },
    saveTariffArea : {
      method : 'POST',
      params : {
        param1 : 'saveLink'
      },
      isArray : false
    },
    deleteTariffArea : {
      method : 'POST',
      params : {
        param1 : 'deleteLink'
      },
      isArray : false
    },
  });
});

oseModule
    .controller('tariffAreasListController',
        function tariffAreasListController($scope, $rootScope, $uibModal, $filter, TariffAreasService, LocationsService) {
          $scope.isLoading = true;

          $scope.sortType = 'endDate';
          $scope.sortReverse = true;
          $scope.tariffAreas_def = undefined;

          $scope.criteria = {
            tariffArea : undefined,
            location : undefined,
            from: new Date()
          }

          $scope.openFrom2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            if (typeof ($scope.mydp) === 'undefined') {
              $scope.mydp = {};
            }
            $scope.mydp.openedFrom2 = true;
          };

          $scope.openTo2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            if (typeof ($scope.mydp) === 'undefined') {
              $scope.mydp = {};
            }
            $scope.mydp.openedTo2 = true;
          };

          $scope.openFrom = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedFrom = true;
          };

          $scope.openTo = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.openedTo = true;
          };

          $scope.dateOptions = {
            formatYear : "yyyy",
            startingDay : 1
          };

          $scope.resetHours = function(theDate) {
            var mmt = moment(theDate);
            mmt.seconds(0);
            mmt.minutes(0);
            mmt.hours(0);
            return mmt.toDate();
          }

          $scope.upgHours = function(theDate) {
            var mmt = moment(theDate);
            mmt.seconds(59);
            mmt.minutes(59);
            mmt.hours(23);
            return mmt.toDate();
          }

          TariffAreasService.getAll({
            indexFormat : true
          }, function(response) {
            $scope.tariffAreas_def = response.content;
          });
          
          $scope.searchTariffAreas = function() {
              var tariffAreaId = $scope.criteria == undefined || $scope.criteria.tariffArea == undefined
                  || $scope.criteria.tariffArea.id == undefined ? undefined : $scope.criteria.tariffArea.id;
              var locationId = $scope.criteria == undefined || $scope.criteria.location == undefined
                  || $scope.criteria.location.id == undefined ? undefined : $scope.criteria.location.id;
              var categoryId = $scope.criteria == undefined || $scope.criteria.location == undefined
                  || $scope.criteria.location.id == undefined ? undefined : $scope.criteria.location.category;

              var from = null;
              if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
                from = $scope.resetHours($scope.criteria.from).getTime();
              }
              var to = null;
              if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
                to = $scope.resetHours($scope.criteria.to).getTime();
              }
              $scope.isLoading = true;
              TariffAreasService.getCurrentLinks({
                locationId : locationId,
                tariffAreaId : tariffAreaId,
                categoryId : categoryId,
                from : from,
                to : to
              }, function(response) {
                $scope.tariffAreas = response.content;
                $scope.isLoading = false;
              });
            };


          LocationsService.getParentLocations({
            excludeBase : true,
          }, function(response) {
        	$scope.locations2 = response.content;
            $scope.locations = response.content;
            var arr = [];
            for (var i = 0; i < $scope.locations.length; i++) {
              if (!arr.includes($scope.locations[i].category)) {
                arr.push($scope.locations[i].category);
              }
            }
            for (var i = 0; i < arr.length; i++) {
              $scope.locations.unshift({
                id : undefined,
                label : arr[i],
                category : arr[i]
              });
            }
            LocationsService.getDefaultSaleLocation(function(response) {
                $scope.criteria.location = response.content;
                if ($scope.criteria.location != undefined) {
                  $scope.criteria.location = {
                    id : response.content.id,
                    label : response.content.label
                  };
                }
                $scope.searchTariffAreas();
              });

          });


          $scope.editTariffArea = function($TariffArea) {
            $TariffArea.isEdit = true;
          };

          $scope.newTariffArea = function() {
            $TariffArea = {
              isEdit : true,
              isNew : true,
              location : $scope.criteria.location
            }
            if (typeof $scope.tariffAreas == 'undefined') {
              $scope.tariffAreas = [];
            }
            $scope.tariffAreas.unshift($TariffArea);
          };

          $scope.isEdit = function($TariffArea) {
            return $TariffArea.isEdit;
          };

          $scope.canEdit = function($TariffArea) {
            var retour = $TariffArea == undefined ? true : !$TariffArea.isEdit;
            var index = $scope.tariffArea == undefined ? 0 : $scope.tariffArea.length;
            while (retour && index > 0) {
              index--;
              retour = $scope.tariffArea[index].isEdit == undefined ? true : !$scope.tariffArea[index].isEdit;

            }
            return retour;
          };

          $scope.isNew = function($TariffArea) {
            return (typeof $TariffArea.isNew !== 'undefined' && $TariffArea.isNew);
          };

          $scope.removeTariffArea = function($TariffArea) {
            if ($TariffArea.isNew) {
              var i = 0;
              var reponse = false;
              for (i = 0; $scope.tariffAreas != null && !reponse && i < $scope.tariffAreas.length; i++) {
                reponse = ($scope.tariffAreas[i].id == $TariffArea.id);
              }
              if (reponse) {
                $scope.tariffAreas.splice(i - 1, 1);
              }
            } else {
              TariffAreasService.deleteTariffArea($TariffArea, function(response) {
                var i = 0;
                var reponse = false;
                for (i = 0; $scope.tariffAreas != null && !reponse && i < $scope.tariffAreas.length; i++) {
                  reponse = ($scope.tariffAreas[i].id == $TariffArea.id);
                }
                if (reponse) {
                  $scope.tariffAreas.splice(i - 1, 1);
                }
              });
            }
            return false;
          };

          $scope.saveTariffArea = function($TariffArea) {
            $TariffArea.isEdit = undefined;
            $TariffArea.endDate = $scope.upgHours($TariffArea.endDate);

            TariffAreasService.saveTariffArea($TariffArea, function(response) {
              if (response.content != undefined) {
                if ($TariffArea.isNew) {
                  $TariffArea.id = response.content.id;
                }
                $TariffArea.isNew = undefined;
              } else {
                $TariffArea.isEdit = true;
              }
            });

            $TariffArea.isEdit = false;

            return false;
          };

          var modalScopeAlert = $rootScope.$new();
          $scope.openDeleteAlert = function($TariffArea) {
            modalScopeAlert.tariffArea = $TariffArea;
            modalScopeAlert.modalInstance = $uibModal.open({
              templateUrl : 'partial/admin/delete-tariff-areas-alert.html',
              controller : 'tariffAreasAlertController',
              scope : modalScopeAlert
            });

            modalScopeAlert.modalInstance.result.then(function($selectedItem) {
              if ($selectedItem) {
                $scope.removeTariffArea($selectedItem);
              }
            }, function() {
              // $log.info('Modal dismissed at: ' + new Date());
            });
          };

        });

oseModule.controller('tariffAreasAlertController', function tariffAreasAlertController($scope, $uibModalInstance) {

  $scope.ok = function() {
    $uibModalInstance.close($scope.tariffArea);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
