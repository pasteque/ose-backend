oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/admin/tours/list', {
    templateUrl : '/partial/admin/tours-list.html',
    controller : 'toursListController'
  });

} ]);

oseModule.factory('ToursService', function ToursService($resource) {
  return $resource('/tours/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    },
    getCurrent : {
      method : 'GET',
      params : {
        param1 : 'getCurrent'
      },
      isArray : false
    },
    getCurrentLinks : {
      method : 'GET',
      params : {
        param1 : 'getCurrentLinks'
      },
      isArray : false
    },
    saveTour : {
      method : 'POST',
      params : {
        param1 : 'saveLink'
      },
      isArray : false
    },
    deleteTour : {
      method : 'POST',
      params : {
        param1 : 'deleteLink'
      },
      isArray : false
    },
  });
});

oseModule
    .controller(
        'toursListController',
        function toursListController($scope, $rootScope, $uibModal, ToursService, LocationsService) {

          $scope.isLoading = false;
          $scope.sortType = 'startDate';
          $scope.sortReverse = true;
          $scope.criteria = {
            tour : undefined,
            location : undefined,
            from : new Date()
          }
          
          $scope.resetHours = function(theDate) {
              var mmt = moment(theDate);
              mmt.seconds(0);
              mmt.minutes(0);
              mmt.hours(0);
              return mmt.toDate();
            }

            $scope.upgHours = function(theDate) {
              var mmt = moment(theDate);
              mmt.seconds(59);
              mmt.minutes(59);
              mmt.hours(23);
              return mmt.toDate();
            }

          $scope.openFrom = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            if (typeof ($scope.mydp) === 'undefined') {
              $scope.mydp = {};
            }
            $scope.mydp.openedFrom = true;
          };

          $scope.openTo = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            if (typeof ($scope.mydp) === 'undefined') {
              $scope.mydp = {};
            }
            $scope.mydp.openedTo = true;
          };
          
          $scope.openSearchFrom = function($event) {
              $event.preventDefault();
              $event.stopPropagation();
              $scope.openedFrom = true;
            };

            $scope.openSearchTo = function($event) {
              $event.preventDefault();
              $event.stopPropagation();
              $scope.openedTo = true;
            };

          $scope.dateOptions = {
            formatYear : "yyyy",
            formatDay : "dd",
            startingDay : 1
          };

          ToursService.getAll({
            minimal : true
          }, function(response) {
            $scope.tours_def = response.content;
          });

          LocationsService.getCriteria({
            parentOnly : true,
            category : '2000'
          }, function(response) {
            $scope.locations = response.content;
          });

          $scope.isLoading = true;
          ToursService.getCurrentLinks({
            futur : true
          }, function(response) {
            $scope.tours = response.content;
            $scope.isLoading = false;
          });

          $scope.searchTours = function() {
            var tourId = $scope.criteria == undefined || $scope.criteria.tour == undefined || $scope.criteria.tour.id == undefined ? undefined
                : $scope.criteria.tour.id;
            var locationId = $scope.criteria == undefined || $scope.criteria.location == undefined
                || $scope.criteria.location.id == undefined ? undefined : $scope.criteria.location.id;

            var from = null;
            if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
              from = $scope.resetHours($scope.criteria.from).getTime();
            }
            var to = null;
            if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
              to = $scope.upgHours($scope.criteria.to).getTime();
            }
            
            $scope.isLoading = true;
            ToursService.getCurrentLinks({
              locationId : locationId,
              tourId : tourId,
              from : from ,
              to : to
            }, function(response) {
              $scope.tours = response.content;
              $scope.isLoading = false;
            });
          };

          $scope.editTour = function($tour) {
            $tour.isEdit = true;
          };

          $scope.newTour = function() {
            $tour = {
              isEdit : true,
              isNew : true
            }
            if (typeof $scope.tours == 'undefined') {
              $scope.tours = [];
            }
            $scope.tours.unshift($tour);
          };

          $scope.isEdit = function($tour) {
            return $tour.isEdit;
          };

          $scope.canEdit = function($tour) {
            var retour = $tour == undefined ? true : !$tour.isEdit;
            var index = $scope.tours == undefined ? 0 : $scope.tours.length;
            while (retour && index > 0) {
              index--;
              retour = $scope.tours[index].isEdit == undefined ? true : !$scope.tours[index].isEdit;

            }
            return retour;
          };

          $scope.isNew = function($tour) {
            return (typeof $tour.isNew !== 'undefined' && $tour.isNew);
          };

          $scope.removeTour = function($tour) {
            if ($tour.isNew) {
              var i = 0;
              var reponse = false;
              for (i = 0; $scope.tours != null && !reponse && i < $scope.tours.length; i++) {
                reponse = ($scope.tours[i].id == $tour.id);
              }
              if (reponse) {
                $scope.tours.splice(i - 1, 1);
              }
            } else {
              ToursService.deleteTour($tour, function(response) {
                var i = 0;
                var reponse = false;
                for (i = 0; $scope.tours != null && !reponse && i < $scope.tours.length; i++) {
                  reponse = ($scope.tours[i].id == $tour.id);
                }
                if (reponse) {
                  $scope.tours.splice(i - 1, 1);
                }
              });
            }
            return false;
          };

          $scope.saveTour = function($tour) {
            $tour.isEdit = undefined;
            ToursService.saveTour($tour, function(response) {
              if (response.content != undefined) {
                if ($tour.isNew) {
                  $tour.id = response.content.id;
                }
                $tour.isNew = undefined;
              } else {
                $tour.isEdit = true;
              }
            });

            $tour.isEdit = false;

            return false;
          };

          var modalScopeAlert = $rootScope.$new();
          $scope.openDeleteAlert = function(tour) {
            modalScopeAlert.tour = tour;
            modalScopeAlert.modalInstance = $uibModal.open({
              templateUrl : '/partial/admin/delete-tour-alert.html',
              controller : 'toursAlertController',
              scope : modalScopeAlert
            });

            modalScopeAlert.modalInstance.result.then(function($selectedItem) {
              if ($selectedItem) {
                $scope.removeTour($selectedItem);
              }
            }, function() {
              // $log.info('Modal dismissed at: ' + new Date());
            });
          };

        });

oseModule.controller('toursAlertController', function toursAlertController($scope, $uibModalInstance) {

  $scope.ok = function() {
    $uibModalInstance.close($scope.tour);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
