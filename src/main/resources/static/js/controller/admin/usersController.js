oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider.when('/admin/users/list', {
		templateUrl : '/partial/admin/users-list.html',
		controller : 'usersListController'
	});

} ]);

oseModule.factory('UsersService', function($resource) {
	return $resource('/users/:param1:id/:value1', {
		param1 : '@param1',
		value1 : '@value1'
	}, {
		getAll : {
			method : 'GET',
			params : {
				param1 : 'getAll'
			},
			isArray : false
		},
		getAllByPermission : {
			method : 'GET',
			params : {
				param1 : 'getAllByPermission'
			},
			isArray : false
		},
		saveUser : {
			method : 'POST',
			params : {
				param1 : 'saveUser'
			},
			isArray : false
		},
		deleteUser : {
			method : 'POST',
			params : {
				param1 : 'deleteUser'
			},
			isArray : false
		},
		updatePassword : {
			method : 'POST',
			params : {
				param1 : 'updPwdBack'
			},
			isArray : false
		},
	});
});

oseModule.factory('RolesService', function($resource) {
	return $resource('/roles/:param1:id/:value1', {
		param1 : '@param1',
		value1 : '@value1'
	}, {
		getAll : {
			method : 'GET',
			params : {
				param1 : 'getAll'
			},
			isArray : false
		},
		saveRole : {
			method : 'POST',
			params : {
				param1 : 'saveRole'
			},
			isArray : false
		},
		deleteRole : {
			method : 'POST',
			params : {
				param1 : 'deleteRole'
			},
			isArray : false
		}
	});
});

oseModule.controller('usersListController', function($scope, $rootScope, $uibModal, UsersService, RolesService) {

	UsersService.getAll(function(response) {
		$scope.users = response.content;
		for (var i = 0; $scope.users != null && i < $scope.users.length; i++) {
			$scope.users[i].isEdit = false;
			$scope.users[i].password = undefined;
		}
	});

	RolesService.getAll(function(response) {
		$scope.roles = response.content;
	});

	$scope.editUser = function($user) {
		$user.isEdit = true;
	};

	$scope.newUser = function() {

		$user = {
				hasImage : false,
				isEdit : true,
				isNew : true,
				ddebut : new Date()
		}
		if ($scope.search.actif!=undefined ){
			$user.actif = $scope.search.actif;
		}
		$scope.users.unshift($user);
	};

	$scope.isEdit = function($user) {
		return $user.isEdit;
	};

	$scope.isNew = function($user) {
		return (typeof $user.isNew !== 'undefined' && $user.isNew);
	};

	$scope.removeUser = function($user) {
		UsersService.deleteUser($user, function(response) {
			var i = 0;
			var reponse = false;
			for (i = 0; $scope.users != null && !reponse && i < $scope.users.length; i++) {
				reponse = ($scope.users[i].id == $user.id);
			}
			if (reponse) {
				$scope.users.splice(i - 1, 1);
			}
		});
		return false;
	};

	$scope.updateActif = function($user) {
		var maintenant = new Date();
		$user.actif = true ;
		if ($user.dfin && $user.dfin < maintenant ) {
			$user.actif = false ;
		}
		if ($user.ddebut && $user.ddebut > maintenant ) {
			$user.actif = false ;
		}
	}

	$scope.saveUser = function($user) {
		$user.isEdit = undefined;
		UsersService.saveUser($user, function(response) {
			$user.password = undefined;
			$user.isNew = undefined;
		});

		$user.isEdit = false;
		$scope.updateActif($user)
		//desactivation des filtres
		//$scope.search = [];
		//pourquoi ?
		return false;
	};

	var modalScope = $rootScope.$new();
	$scope.openPwChange = function($user) {
		modalScope.users = $scope.users;
		modalScope.user = $user;
		modalScope.pass = {
				old : "",
				newpw : "",
				repeatnewpw : "",
				alert : undefined
		};
		modalScope.modalInstance = $uibModal.open({
			templateUrl : '/partial/admin/users-change-password.html',
			controller : 'usersPwController',
			scope : modalScope
		});

		modalScope.modalInstance.result.then(function(selectedItem) {
			if (selectedItem) {
				$user.password = selectedItem.newpw;
			}
		}, function() {
			// $log.info('Modal dismissed at: ' + new Date());
		});
	};

	var modalScopeAlert = $rootScope.$new();
	$scope.openDeleteAlert = function($user) {
		modalScopeAlert.user = $user;
		modalScopeAlert.modalInstance = $uibModal.open({
			templateUrl : '/partial/admin/delete-user-alert.html',
			controller : 'usersAlertController',
			scope : modalScopeAlert
		});

		modalScopeAlert.modalInstance.result.then(function($selectedItem) {
			if ($selectedItem) {
				$scope.removeUser($selectedItem);
			}
		}, function() {
			// $log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.openFrom2 = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		if (typeof ($scope.mydp) === 'undefined') {
			$scope.mydp = {};
		}
		$scope.mydp.openedFrom2 = true;
	};

	$scope.showUsers = function(value) {
		if (value == null){
			$scope.search = []
		}

	};
});

oseModule.controller('usersPwController', function($scope, $uibModalInstance) {

	$scope.ok = function() {
		if ($scope.pass.newpw == $scope.pass.repeatnewpw) {
			$uibModalInstance.close($scope.pass);
		} else {
			$scope.pass.alert = "Les deux mots de passe doivent etre identiques !";
		}

	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
});

oseModule.controller('usersAlertController', function($scope, $uibModalInstance) {

	$scope.ok = function() {
		$uibModalInstance.close($scope.user);
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});
