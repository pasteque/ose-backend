oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/admin/resources/list', {
    templateUrl : '/partial/admin/resources-list.html',
    controller : 'resourcesListController'
  });

} ]);

// EDU : gestion async de la récupération du contenu d'un fichier
oseModule.directive("ngFileSelect", function() {

  return {
    link : function($scope, el) {

      el.bind("change", function(e) {

        $scope.file = (e.srcElement || e.target).files[0];
        $scope.getFile($scope.file);
      })

    }

  }

});

oseModule.factory("FileReaderService", function($q, $log) {

  var onLoad = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.resolve(reader.result);
      });
    };
  };

  var onError = function(reader, deferred, scope) {
    return function() {
      scope.$apply(function() {
        deferred.reject(reader.result);
      });
    };
  };

  var onProgress = function(reader, scope) {
    return function(event) {
      scope.$broadcast("fileProgress", {
        total : event.total,
        loaded : event.loaded
      });
    };
  };

  var getReader = function(deferred, scope) {
    var reader = new FileReader();
    reader.onload = onLoad(reader, deferred, scope);
    reader.onerror = onError(reader, deferred, scope);
    reader.onprogress = onProgress(reader, scope);
    return reader;
  };

  var readAsDataURL = function(file, scope) {
    var deferred = $q.defer();

    var reader = getReader(deferred, scope);
    reader.readAsDataURL(file);

    return deferred.promise;
  };

  return {
    readAsDataUrl : readAsDataURL
  };
});

oseModule.factory('ResourcesService', function($resource) {
  return $resource('/resources/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    },
    saveResource : {
      method : 'POST',
      params : {
        param1 : 'save'
      },
      isArray : false
    },
    deleteResource : {
      method : 'POST',
      params : {
        param1 : 'delete'
      },
      isArray : false
    }
  });
});

oseModule.controller('resourcesListController', function($scope, $rootScope, $uibModal, ResourcesService) {

  ResourcesService.getAll(function(response) {
    $scope.resources = response.content;
  });

  $scope.editResource = function($resource) {
    $resource.isEdit = true;
  };

  $scope.newResource = function() {
    $resource = {
      isEdit : true,
      isNew : true
    }
    $scope.resources.unshift($resource);
  };

  $scope.isEdit = function($resource) {
    return $resource.isEdit;
  };

  $scope.isNew = function($resource) {
    return (typeof $resource.isNew !== 'undefined' && $resource.isNew);
  };

  $scope.removeResource = function($resource) {
    ResourcesService.deleteResource($resource, function(response) {
      var i = 0;
      var reponse = false;
      for (i = 0; $scope.resources != null && !reponse && i < $scope.resources.length; i++) {
        reponse = ($scope.resources[i].id == $resource.id);
      }
      if (reponse) {
        $scope.resources.splice(i - 1, 1);
      }
    });
    return false;
  };

  $scope.saveResource = function($resource) {
    $resource.isEdit = undefined;
    ResourcesService.saveResource($resource, function(response) {
      $resource.isNew = undefined;
    });

    $resource.isEdit = false;

    return false;
  };

  var modalScope = $rootScope.$new();
  $scope.openContentChange = function($resource) {
    modalScope.resources = $scope.resources;
    modalScope.resource = $resource;
    modalScope.content = {
      content : $resource.content,
      alert : undefined
    };
    modalScope.modalInstance = $uibModal.open({
      templateUrl : '/partial/admin/resources-change-content.html',
      controller : 'resourcesContentController',
      scope : modalScope
    });

    modalScope.modalInstance.result.then(function(selectedItem) {
      if (selectedItem) {
        $resource.content = selectedItem.content;
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

  var modalScopeAlert = $rootScope.$new();
  $scope.openDeleteAlert = function($resource) {
    modalScopeAlert.resource = $resource;
    modalScopeAlert.modalInstance = $uibModal.open({
      templateUrl : '/partial/admin/delete-resource-alert.html',
      controller : 'resourcesAlertController',
      scope : modalScopeAlert
    });

    modalScopeAlert.modalInstance.result.then(function($selectedItem) {
      if ($selectedItem) {
        $scope.removeResource($selectedItem);
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

});

oseModule.controller('resourcesContentController', function($scope, $uibModalInstance, FileReaderService) {

  $scope.getFile = function(file) {
    $scope.progress = 0;
    $scope.file = file;
    FileReaderService.readAsDataUrl($scope.file, $scope).then(function(result) {
      index = result.indexOf(";base64,")
      if (index != -1) {
        $scope.content.content = result.substring(index + 8);
      }

    });
  };

  $scope.$on("fileProgress", function(e, progress) {
    $scope.progress = progress.loaded / progress.total;
  });

  $scope.ok = function() {
    $uibModalInstance.close($scope.content);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});

oseModule.controller('resourcesAlertController', function($scope, $uibModalInstance) {

  $scope.ok = function() {
    $uibModalInstance.close($scope.resource);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
