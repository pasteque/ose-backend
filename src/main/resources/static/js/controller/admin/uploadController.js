oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider.when('/admin/upload/form', {
		templateUrl : '/partial/admin/upload-form.html',
		controller : 'uploadFormController'
	});

} ]);

oseModule.controller('uploadFormController', function($scope, $rootScope, $timeout , Upload , LocationsService ) {

	$scope.criteria = {
			file : undefined,
			location : undefined
	}
	
	$scope.logs = '';

	LocationsService.getParentLocations(function(response) {
		$scope.locations = response.content;
		$scope.locations.unshift({
			id : undefined,
			label : "--"
		});
	});
	
    $scope.upload = function (files) {
    	$scope.files = files;
    	var locationId = $scope.criteria.location && $scope.criteria.location.id ? $scope.criteria.location.id : undefined ;
        if($scope.files && $scope.files.length > 0){
        	for (var i = 0; i < $scope.files.length; i++) {
		    	$scope.criteria.file = $scope.files[i];
		    	if ($scope.criteria.file && !$scope.criteria.file.$error) {
		            Upload.upload({
		                url: '/edi/upload',
		                data: {
		                  locationId: locationId,
		                  file: $scope.criteria.file  
		                }
		            }).then(function (resp) {
		                $timeout(function() {
		                    $scope.logs = 'file: ' +
		                    resp.config.data.file.name +
		                    ', Response: ' + JSON.stringify(resp.data) +
		                    '\n' + $scope.logs;
		                });
		            }, null, function (evt) {
		                var progressPercentage = parseInt(100.0 *
		                		evt.loaded / evt.total);
		                $scope.logs = 'progress: ' + progressPercentage + 
		                	'% ' + evt.config.data.file.name + '\n' + 
		                  $scope.logs;
		            });
		        }
        	}
        }
    };


});
