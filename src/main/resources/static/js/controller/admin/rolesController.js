oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/admin/roles/list', {
    templateUrl : '/partial/admin/roles-list.html',
    controller : 'rolesListController'
  });

} ]);

oseModule.controller('rolesListController', function($scope, $rootScope, $uibModal, UsersService, RolesService) {

  RolesService.getAll(function(response) {
    $scope.roles = response.content;
  });

  $scope.editRole = function($role) {
    $role.isEdit = true;
  };

  $scope.newRole = function() {
    $role = {
      isEdit : true,
      isNew : true
    }
    $scope.roles.push($role);
  };

  $scope.isEdit = function($role) {
    return $role.isEdit;
  };

  $scope.isNew = function($role) {
    return (typeof $role.isNew !== 'undefined' && $role.isNew);
  };

  $scope.removeRole = function($role) {
    RolesService.deleteRole($role, function(response) {
      var i = 0;
      var reponse = false;
      for (i = 0; $scope.roles != null && !reponse && i < $scope.roles.length; i++) {
        reponse = ($scope.roles[i].id == $role.id);
      }
      if (reponse) {
        $scope.roles.splice(i - 1, 1);
      }
    });
    return false;
  };

  $scope.saveRole = function($role) {
    $role.isEdit = undefined;

    RolesService.saveRole($role, function(response) {
      $role.isNew = undefined;
    });

    $role.isEdit = false;

    return false;
  };

  var modalScope = $rootScope.$new();
  $scope.openContentChange = function($role) {
    modalScope.roles = $scope.roles;
    modalScope.role = $role;
    modalScope.content = {
      content : $role.permissions,
      alert : undefined
    };
    modalScope.modalInstance = $uibModal.open({
      templateUrl : '/partial/admin/roles-change-content.html',
      controller : 'rolesContentController',
      scope : modalScope
    });

    modalScope.modalInstance.result.then(function(selectedItem) {
      if (selectedItem) {
        $role.permissions = selectedItem.content;
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

  var modalScopeAlert = $rootScope.$new();
  $scope.openDeleteAlert = function($role) {
    modalScopeAlert.role = $role;
    modalScopeAlert.modalInstance = $uibModal.open({
      templateUrl : '/partial/admin/delete-role-alert.html',
      controller : 'rolesAlertController',
      scope : modalScopeAlert
    });

    modalScopeAlert.modalInstance.result.then(function($selectedItem) {
      if ($selectedItem) {
        $scope.removeRole($selectedItem);
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

});

oseModule.controller('rolesContentController', function($scope, $uibModalInstance) {

  $scope.ok = function() {
    $uibModalInstance.close($scope.content);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});

oseModule.controller('rolesAlertController', function($scope, $uibModalInstance) {

  $scope.ok = function() {
    $uibModalInstance.close($scope.role);
  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});
