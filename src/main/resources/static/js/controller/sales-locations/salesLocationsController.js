oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/sales-locations/list', {
        templateUrl : '/partial/sales-locations/sales-locations-list.html',
        controller : 'salesLocationsListController'
    });
    $routeProvider.when('/sales-locations/:id/form', {
        templateUrl : '/partial/sales-locations/sales-locations-form.html',
        controller : 'salesLocationsFormController'
    });

} ]);

oseModule.factory('SalesLocationsService', function($resource) {
    return $resource('/sales-locations/:param1:id/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        },
        saveSalesLocations : {
            method : 'POST',
            params : {
                param1 : 'saveSalesLocations'
            },
            isArray : false
        }
    });
});

oseModule.controller('salesLocationsListController', function($scope, SalesLocationsService , ToursService , LocationsService) {
	
	$scope.sortType     = undefined; 
	$scope.sortReverse  = false;
	
    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (typeof($scope.mydp) === 'undefined'){
            $scope.mydp = {};
        }
        $scope.mydp.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        if (typeof($scope.mydp) === 'undefined'){
            $scope.mydp = {};
        }
        $scope.mydp.openedTo = true;
    };

    $scope.dateOptions = {
        formatYear : "yyyy",
        formatDay : "dd",
        startingDay : 1
    };
    
    $scope.tours_def = [];
    
    var dFrom = moment().subtract(1, 'days').toDate();
    var dTo = moment().add(7, 'days').toDate();

    
    LocationsService.getDefaultSaleLocation(function(response) {
        $scope.criteria = {
                location : response.content ,
                from : dFrom ,
                to : dTo
            };
        if ( $scope.criteria.location != undefined) {
        	$scope.criteria.location = { id : response.content.id,
    		        label : response.content.label};
        	$scope.tours_def = undefined;
        	ToursService.getCurrent({
            	minimal : true,
            	locationId : $scope.criteria.location.id
            } , function(response) {
                $scope.tours_def = response.content;
                if($scope.tours_def.length > 0) {
                	$scope.criteria.tour = $scope.tours_def[0];
                }
            });
        } else {
        	$scope.criteria.location = { id : undefined };
            ToursService.getAll({
            	minimal : true
            } , function(response) {
                $scope.tours_def = response.content;
                $scope.tours_def.unshift({
                    id : undefined,
                    nom : ""
                });
            });
            LocationsService.getParentLocations(function(response) {
                $scope.locations = response.content;
                $scope.locations.unshift({
                    id : undefined,
                    label : "-"
                });
                
            });
        }
        
    });
    
    LocationsService.getParentLocations(function(response) {
        $scope.locations = response.content;
        $scope.locations.unshift({
            id : undefined,
            label : ""
        });
    });
    
    $scope.searchSalesLocations = function() {
        SalesLocationsService.getCriteria({
            reference : $scope.criteria.reference,
            from : $scope.criteria.from,
            to : $scope.criteria.to,
            tour : angular.isDefined($scope.criteria.tour) ? $scope.criteria.tour.id : undefined,
            location : $scope.criteria.location.id
        }, function(response) {
            $scope.salesLocations = response.content;
        });
    }
});

oseModule.controller('salesLocationsFormController', function($scope, $location, $routeParams, SalesLocationsService) {
    var _action = $location.search()['_action'];
    $scope.edit = (_action == "create" || _action == "edit")
    if (_action == "create") {
        $scope.salesLocation = {};

    } else {
        SalesLocationsService.get({
            id : $routeParams.id,
        }, function(response) {
            $scope.salesLocation = response.content;
            $scope.salesLocation.startDate = new Date($scope.salesLocation.startDate);
            $scope.salesLocation.endDate = new Date($scope.salesLocation.endDate);
        });
    }

    $scope.editLocation = function() {
        $location.search({
            _action : "edit"
        });
    }

    $scope.cancelLocation = function() {
        $location.search({
            _action : "view"
        });
    }

    $scope.saveLocation = function() {
        SalesLocationsService.saveSalesLocations($scope.salesLocation, function(response) {
            $location.url("/sales-locations/" + response.content.id + "/form?_action=view");
        });
    }
});
