oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/inventory/list', {
        templateUrl : '/partial/inventory/inventory-list.html',
        controller : 'inventoryListController'
    });
    $routeProvider.when('/inventory/form', {
        templateUrl : '/partial/inventory/inventory-form.html',
        controller : 'inventoryFormController'
    });
    $routeProvider.when('/inventory/:id/form', {
        templateUrl : '/partial/inventory/inventory-form.html',
        controller : 'inventoryFormController'
    });

} ]);

oseModule.factory('InventoryService', function($resource) {
    return $resource('/inventory/:param1:id/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        canCreateNewInventory : {
            method : 'GET',
            params : {
                param1 : 'canCreateNewInventory'
            },
            isArray : false
        },
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        },
        initialize : {
            method : 'GET',
            params : {
                param1 : 'initialize'
            },
            isArray : false
        },
        getDetails : {
            method : 'GET',
            params : {
                param1 : 'inventoryDetails'
            },
            isArray : false,
            cancellable : true
        },
        getInventoryDetailsGrouped : {
            method : 'GET',
            params : {
                param1 : 'inventoryDetailsGrouped'
            },
            isArray : false,
            cancellable : true
        },
        getInventoryInfo : {
            method : 'GET',
            params : {
                param1 : 'inventoryInfo'
            },
            isArray : false,
            cancellable : true
        },
        saveInventory : {
            method : 'POST',
            params : {
                param1 : 'saveInventory'
            },
            isArray : false
        },
        saveInventoryDetails : {
            method : 'POST',
            params : {
                param1 : 'saveInventoryDetails'
            },
            isArray : false
        },
        closeInventory : {
            method : 'GET',
            params : {
                param1 : 'closeInventory'
            },
            isArray : false
        },
    })
});

oseModule.controller('inventoryListController', function($scope,$location, InventoryService, LocationsService,CriteriaService) {
    $scope.sortType     = undefined; 
    $scope.sortReverse  = false;
    
    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };
    
    $scope.dateOptions = {
        formatYear : "yyyy",
        startingDay : 1
    };
    $scope.hasQueryParams = false;
    
    $scope.criteria = {};
    
    initializeQueryParams();
    
    
    function initializeQueryParams() {
	    LocationsService.getDefaultSaleLocation(function(response) {
	        
	        if (response.content != undefined) {
	            $scope.criteria.location = { 
	                    id : response.content.id,
	                    label : response.content.label
	            };
	            $scope.locations = [$scope.criteria.location];
	            LocationsService.getSubLocations({
	                locationId : $scope.criteria.location.id,
	            }, function(response) {
	                $scope.locations = $scope.locations.concat(response.content);
	            });
	            $scope.locationUpdated();
	        } else {
	            LocationsService.getAllInfo(function(response) {
	                $scope.locations = response.content;
	            });
	        }
	        
	        if (CriteriaService.getParamsUrl($scope)) {
            $scope.hasQueryParams = true;
            findInventory();
            }
	
	    });
        
    }
    
    $scope.searchInventory = function() {
        $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
        findInventory();
    }
    
    function findInventory() {
        var long_from = $scope.criteria.from == undefined ? undefined : $scope.criteria.from.valueOf();
        var long_to = $scope.criteria.to == undefined ? undefined : $scope.criteria.to.valueOf();
        
        InventoryService.getCriteria({
            locationId : $scope.criteria.location.id,
            from :  long_from,
            to : long_to ,
        }, function(response) {
            $scope.inventories = response.content;
        });
    }

    $scope.locationUpdated = function() {
        InventoryService.canCreateNewInventory({
            locationId : $scope.criteria.location.id
        }, function(response) {
            $scope.canCreateNewInventory = response.content;
        });
    }


});

oseModule.controller('inventoryFormController', function($scope, $location, $routeParams, InventoryService, ProductsService , MessagesService) {

    var _action = $location.search()['_action'];
    
    $scope.openDate = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedDate = true;
    };
    
    $scope.dateOptions = {
            formatYear : "yyyy",
            startingDay : 1
        };
    
    $scope.isLoading = true ;
    $scope.isClosing = false;
    
    $scope.saveLoc = function($inventoryDetail) {
        var messagesList = [];
        if($inventoryDetail.product.stockLocation != '_NR' && $inventoryDetail.product.stockLocation.trim() != '') {
            messagesList.push({ content : $inventoryDetail.product.stockLocation.trim() ,
                locationId : $scope.inventory.locationId,
                productId : $inventoryDetail.product.barcode,
                type : 'EMPL'});
        }
        MessagesService.updateMessages({
            messages : messagesList,
            locationId : $scope.inventory.locationId,
            productId : $inventoryDetail.product.barcode,
            typeId : 'EMPL'
        }, function(response) {
            $inventoryDetail.isEdit = false;
        });
    };
    
    $scope.editUser = function($inventoryDetail) {
        $inventoryDetail.isEdit = true;
    };
    
    $scope.isEdit = function($inventoryDetail) {
        return $inventoryDetail.isEdit;
    };
    
    $scope.edit = (_action == "create" || _action == "edit")
    if (_action == "create") {
        InventoryService.initialize({
            locationId : $location.search()['locationId'],
        }, function(response) {
            $scope.inventory = response.content;
        });
        $scope.types = [ "COMPLET", "PARTIEL" , "RAZ"];
    } else {
        $scope.isLoading = true ;
        InventoryService.get({
            id : $routeParams.id
        }, function(response) {
            $scope.inventory = response.content;
            $scope.inventoryDetail = {
                inventoryId : $scope.inventory.id,
                quantity : 1
            };
            InventoryService.getDetails({
                inventoryId : $scope.inventory.id
            }, function(response) {
                $scope.inventoryDetails = response.content;
                $scope.isLoading = false ;
            });
        });
    }

    $scope.display = "detail";

    $scope.getProducts = function(value) {
        if ($scope.requeteProduits != undefined) {
            $scope.requeteProduits.$cancelRequest();        
        }
        if (value != "") {
            $scope.products = undefined;
            $scope.inventoryDetail.product = undefined;
            $scope.requeteProduits = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : false
            });
            
            $scope.requeteProduits.$promise.then(function(response) {
                $scope.products = response.content;
                if($scope.products.length == 1) {
                    $scope.inventoryDetail.product = $scope.products[0];
                }
            },function(httpResponse){
                //console.log("Annulée ou erreur");
            });
        }
    }

    $scope.addInventoryDetail = function() {
        $scope.isLoading = true ;
        if ($scope.inventoryDetail.product && $scope.inventoryDetail.quantity) {
            InventoryService.saveInventoryDetails($scope.inventoryDetail, function(response) {
                $scope.getDetails();
                $scope.inventoryDetail = {
                        inventoryId : $scope.inventory.id,
                        quantity : 1,
                        products : undefined
                };
             $scope.inventoryDetail.product = undefined;
             $scope.getProducts(undefined);
             $scope.isLoading = false ;
            });
        }
        $scope.$broadcast('SetFocus');
    }

    $scope.saveInventory = function() {
        InventoryService.canCreateNewInventory({
            locationId : $scope.inventory.locationId
        }, function(response) {
            if(response.content) {
                InventoryService.saveInventory($scope.inventory, function(response) {
                    $location.url("/inventory/" + response.content.id + "/form?_action=view");
                });
            } else {
                alert("Vous ne pouvez pas initier un nouvel inventaire car il y en a un en cours.");
            }
        })

    }

    $scope.startInventory = function() {
        $scope.inventory.statusCode = "P";
        InventoryService.saveInventory($scope.inventory);
    }

    $scope.getDetails = function() {
        $scope.isLoading = true ;
        InventoryService.getDetails({
            inventoryId : $scope.inventory.id
        }, function(response) {
            $scope.display = "detail";
            $scope.inventoryDetails = response.content;
            $scope.isLoading = false ;
        });
    }

    $scope.getSummary = function() {
        $scope.isLoading = true ;
        InventoryService.getInventoryInfo({
            inventoryId : $scope.inventory.id
        }, function(response) {
            $scope.inventoryInfos = response.content;
        });
        
        InventoryService.getInventoryDetailsGrouped({
            inventoryId : $scope.inventory.id
        }, function(response) {
            $scope.display = "summary";
            $scope.inventoryDetails = response.content;
            $scope.isLoading = false ;
        })
    }

    $scope.closeInventory = function() {
        $scope.isLoading = true ;
        $scope.isClosing = true;
        InventoryService.closeInventory({
            inventoryId : $scope.inventory.id
        }, function(response) {
            $scope.isLoading = false ;
        });
    }
    
    $scope.downloadCSV = function(value) {
        var url = '/inventory/'+value+'/download?_format=csv&inventoryId=' + $scope.inventory.id ;

        $scope.isLoading = true;
        window.downloadFile( url );
        $scope.isLoading = false;
    }
    
    
    
    $scope.downloadXLS = function(value) {
        var url = '/inventory/'+value+'/download?_format=xls&inventoryId=' + $scope.inventory.id ;

        $scope.isLoading = true;
        window.downloadFile( url );
        $scope.isLoading = false;
    }

});
