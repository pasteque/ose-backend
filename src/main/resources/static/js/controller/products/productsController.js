oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/products/list', {
    templateUrl : '/partial/products/products-list.html',
    controller : 'productsListController'
  });
  $routeProvider.when('/products/:id/form', {
    templateUrl : '/partial/products/products-form.html',
    controller : 'productsFormController'
  });

} ]);

oseModule.factory('MessagesService', function MessagesService($resource) {
  return $resource('/messages/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    updateMessages : {
      method : 'POST',
      params : {
        param1 : 'updateMessages'
      },
      isArray : false
    },
    getMessages : {
      method : 'GET',
      params : {
        param1 : 'getMessages'
      },
      isArray : false
    },
    initMessage : {
      method : 'GET',
      params : {
        param1 : 'initMessage'
      },
      isArray : false
    },
    createRenflouement : {
        method : 'POST',
        params : {
            param1 : 'createRenflouement'
        },
        isArray : false
    },
    updateRenflouement : {
        method : 'POST',
        params : {
            param1 : 'updateRenflouement'
        },
        isArray : false
    }
  });
});

oseModule.factory('ProductsService', function ProductsService($resource) {
  return $resource('/products/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    },
    get : {
      method : 'GET',
      params : {
        param1 : 'get'
      },
      isArray : false
    },
    getLinks : {
      method : 'GET',
      params : {
        param1 : 'getLinks'
      },
      isArray : false
    },
    getCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria'
      },
      isArray : false,
      cancellable : true
    },
    DLByCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria',
        value1 : 'download'
      },
      cancellable : true,
      responseType : 'arraybuffer',
      transformResponse : function(data, headers) {
        var response = {};
        response.headers = headers();

        if (headers('content-disposition')) {
          response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');
          response.contentType = headers('content-type');
          if (data) {
            response.blob = new Blob([ data ], {
              type : response.contentType
            });
          }
        }

        return response;
      }
    }
  });
});

oseModule.factory('CategoriesService', function  CategoriesService($resource) {
  return $resource('/categories/:param1/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    },
    get : {
      method : 'GET',
      params : {
        param1 : 'get'
      },
      isArray : false
    },
    getCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria'
      },
      isArray : false,
      cancellable : true
    }
  });
});

oseModule.controller('productsListController', function productsListController($scope, $location, ProductsService) {
  $scope.isLoading = false;
  $scope.barLimit = 100;
  $scope.isDownLoading = false;

  $scope.sortType = undefined;
  $scope.sortReverse = false;

  $scope.increaseLimit = function() {
    $scope.barLimit += 50;
  }

  $scope.searchProducts = function() {
    if ($scope.requeteProduits != undefined) {
      $scope.requeteProduits.$cancelRequest();
    }
    $scope.isLoading = true;
    if (angular.isDefined($scope.criteria) && angular.isDefined($scope.criteria.reference)) {
      $scope.requeteProduits = ProductsService.getCriteria({
        reference : $scope.criteria.reference
      });
    } else {
      $scope.requeteProduits = ProductsService.getCriteria({});
    }

    $scope.requeteProduits.$promise.then(function(response) {
      $scope.products = response.content;
      $scope.isLoading = false;
      if ($scope.products.length && $scope.products.length == 1) {
        $location.url("/products/" + $scope.products[0].id + "/form");
      }

    }, function(httpResponse) {
      // console.log("Annulée ou erreur");
    });
  };

  $scope.downloadXLS = function() {
    var reference = undefined;
    if (angular.isDefined($scope.criteria) && angular.isDefined($scope.criteria.reference) && $scope.criteria.reference != null
        && $scope.criteria.reference.length > 0) {
      reference = $scope.criteria.reference;
    }

    $scope.isDownLoading = true;
    ProductsService.DLByCriteria({
      _format : 'xls',
      reference : reference
    }, function(response) {

      // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      var linkElement = document.createElement('a');
      try {

        var url = window.URL.createObjectURL(response.blob);

        linkElement.setAttribute('href', url);
        linkElement.setAttribute("download", response.filename);

        var clickEvent = new MouseEvent("click", {
          "view" : window,
          "bubbles" : true,
          "cancelable" : false
        });
        linkElement.dispatchEvent(clickEvent);

        $scope.isDownLoading = false;

      } catch (ex) {
        console.log(ex);
        $scope.isDownLoading = false;
      }
    });
  }
});

oseModule.controller('productsFormController', function productsFormController($scope, $rootScope, $uibModal, $routeParams, ProductsService, LocationsService,
    PermissionsService) {

  $scope.emplList = [];
  $scope.gestEmpl = false;
  $scope.location = undefined;
  $scope.toggleList = [];
  $scope.isLoading = true;

  $scope.getProduct = function(light) {
    type = 'ProductsDetailDTO';
    if (light) {
      type = 'ProductsLightDTO';
    }

    ProductsService.get({
      id : $routeParams.id,
      _type : type
    }, function(response) {
      $scope.product = response.content;
      $scope.isLoading = false;
    });
  };

  LocationsService.getDefaultSaleLocation(function(response) {
    if (response.content != null) {
      $scope.location = response.content;
      $scope.gestEmpl = true;
    } else {
      $scope.gestEmpl = false;
    }
  })

  // Pourrait paraitre choquant,
  // -> la première requete permet de ramener les informations produits
  // -> la seconde permet de remplir le détail des ventes
  $scope.getProduct(true);
  if (PermissionsService.hasPermission('PERM_PRODUIT_VENTE')) {
    $scope.getProduct(false);
  }

  $scope.isVisible = function(location) {
    if (location && (!location.parentId || $scope.toggleList.indexOf(location.parentId) >= 0)) {
      return true;
    }

    return false;
  };

  $scope.toggle = function(locationId) {
    var index = $scope.toggleList.indexOf(locationId);
    if (index < 0) {
      $scope.toggleList.push(locationId);
    } else {
      $scope.toggleList.splice(index, 1);
    }
  };

  var modalScope = $rootScope.$new();
  $scope.open = function() {
    modalScope.items = $scope.emplList;
    modalScope.location = $scope.location;
    modalScope.product = {
      code : $scope.product.code,
      label : $scope.product.label
    };
    modalScope.modalInstance = $uibModal.open({
      templateUrl : '/partial/products/set-product-empl-form.html',
      controller : 'productsEmplController',
      scope : modalScope
    });

    modalScope.modalInstance.result.then(function(selectedItem) {
      if (selectedItem) {
        $scope.getProduct(false);
        modalScope.change = false;
        // Rechargement de la def produit
      }
    }, function() {
      // $log.info('Modal dismissed at: ' + new Date());
    });
  };

});

oseModule.controller('productsEmplController', function productsEmplController($scope, $uibModalInstance, MessagesService) {

  $scope.empl = undefined;
  $scope.change = false;

  MessagesService.getMessages({
    locationId : $scope.location.id,
    productId : $scope.product.code,
    typeId : 'EMPL',
    activeOnly : true,
  }, function(response) {
    $scope.items = response.content;
  });

  $scope.ok = function() {
    MessagesService.updateMessages({
      messages : $scope.items,
      locationId : $scope.location.id,
      productId : $scope.product.code,
      typeId : 'EMPL'
    }, function(response) {
      $uibModalInstance.close($scope.change);
    });

  };

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.addEmpl = function() {
    if (angular.isDefined($scope.empl) && notAlreadySelected()) {
      MessagesService.initMessage({
        locationId : $scope.location.id,
        productId : $scope.product.code,
        typeId : 'EMPL',
        content : $scope.empl
      }, function(response) {
        $scope.items.push(response.content);
        $scope.change = true;
      });
    }
    $scope.empl = undefined;
  }

  $scope.removeEmpl = function(value) {
    for (var i = 0; i < $scope.items.length; i++) {
      if ($scope.items[i] == value) {
        $scope.change = true;
        break;
      }
    }
    $scope.items.splice(i, 1);
  }

  function notAlreadySelected() {
    for (var i = 0; i < $scope.items.length; i++) {
      if ($scope.items[i].content == $scope.empl) {
        return false;
      }
    }
    return true;
  }
});
