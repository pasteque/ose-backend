oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/stocks/list', {
        templateUrl : '/partial/stocks/stocks-list.html',
        controller : 'stocksListController'
    });

    $routeProvider.when('/stocks/movements/list', {
        templateUrl : '/partial/stocks/stocks-movements-list.html',
        controller : 'stocksMovementsListController',
        reloadOnSearch : false
    });

    $routeProvider.when('/stocks/movements/form', {
        templateUrl : '/partial/stocks/stocks-movements-form.html',
        controller : 'stocksMovementsFormController'
    });

    $routeProvider.when('/stocks/stats/list', {
        templateUrl : '/partial/stats/stocks-stats-list.html',
        controller : 'stocksStatsListController'
    });

    $routeProvider.when('/stocks/levels/list', {
        templateUrl : '/partial/stocks/stocks-levels-list.html',
        controller : 'stocksControlsListController'
    });

} ]);

oseModule.factory('StocksService', function($resource) {
    return $resource('/stocks/:param1:id/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        },
        getMovements : {
            method : 'GET',
            params : {
                param1 : 'getMovements'
            },
            isArray : false,
            cancellable : true
        },
        DLMovements : {
            method : 'GET',
            params : {
                param1 : 'getMovements'
            },
            cancellable : true,
            responseType: 'arraybuffer',
            transformResponse: function (data , headers) {
                var response = {};
                response.headers = headers();
                
                if(headers('content-disposition')) {
                    response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');
                    response.contentType = headers('content-type');
                    if (data) {
                        response.blob = new Blob([data], {
                            type: response.contentType
                        });
                    }
                }

                return response;
            }
        },
        getReasons : {
            method : 'GET',
            params : {
                param1 : 'getReasons'
            },
            isArray : false
        },
        initializeMovement : {
            method : 'GET',
            params : {
                param1 : 'initializeMovement'
            },
            isArray : false
        },
        saveMovement : {
            method : 'POST',
            params : {
                param1 : 'saveMovement'
            },
            isArray : false
        },
        getStockLevelByCriteria : {
            method : 'GET',
            params : {
                param1 : 'getStockLevelByCriteria'
            },
            isArray : false,
            cancellable : true
        },
        getAll : {
            method : 'GET',
            params : {
                param1 : 'findAllStockLevel'
            },
            isArray : false,
            cancellable : true
        },
        getStockLevelTableByCriteria : {
            method : 'GET',
            params : {
                param1 : 'getStockLevelTableByCriteria'
            },
            isArray : false,
            cancellable : true
        },
        deleteStockLevel : {
            method : 'POST',
            params : {
                param1 : 'deleteStockLevel'
            },
            isArray : false
        },
        saveStockLevel : {
            method : 'POST',
            params : {
                param1 : 'saveStockLevel'
            },
            isArray : false
        }
    });
});

oseModule.factory('StatsService', function($resource) {
    return $resource('/stats/:param1:id/:param2/:param3', {
        param1 : '@param1',
        param2 : '@param2'
    }, {
        getStockList : {
            method : 'GET',
            params : {
                param1 : 'stock',
                param2 : 'getList'
            },
            isArray : false,
            cancellable : true
        },
        DLStockList : {
            method : 'GET',
            params : {
                param1 : 'stock',
                param2 : 'getList',
                param3 : 'download'
            },
            cancellable : true,
            responseType: 'arraybuffer',
            transformResponse: function (data , headers) {
                var response = {};
                response.headers = headers();
                
                if(headers('content-disposition')) {
                    response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');
                    response.contentType = headers('content-type');
                    if (data) {
                        response.blob = new Blob([data], {
                            type: response.contentType
                        });
                    }
                }

                return response;
            }
        },
        getAggregates : {
            method : 'GET',
            params : {
                param1 : 'base',
                param2 : 'getAggregates'
            },
            isArray : false,
            cancellable : true
        }
    });
});

oseModule.factory('LocationsService', function($resource) {
    return $resource('/locations/:param1/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getAllInfo : {
            method : 'GET',
            params : {
                param1 : 'getAllInfo'
            },
            isArray : false
        },
        get : {
            method : 'GET',
            params : {
                param1 : 'get'
            },
            isArray : false
        },
        getDefault : {
            method : 'GET',
            params : {
                param1 : 'getDefault'
            },
            isArray : false
        },
        getDefaultSaleLocation : {
            method : 'GET',
            params : {
                param1 : 'getDefaultSaleLocation'
            },
            isArray : false
        },
        getSubLocations : {
            method : 'GET',
            params : {
                param1 : 'getSubLocations'
            },
            isArray : false
        },
        getSistersLocations : {
            method : 'GET',
            params : {
                param1 : 'getSistersLocations'
            },
            isArray : false
        },
        getParentLocations : {
            method : 'GET',
            params : {
                param1 : 'getParentLocations'
            },
            isArray : false,
            cancellable : true
        },
        getRemoteLocations : {
            method : 'GET',
            params : {
                param1 : 'getRemoteLocations'
            },
            isArray : false,
            cancellable : true
        },
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        }
    });
});

oseModule.controller('stocksListController', function stocksListController($scope,$location, StocksService, LocationsService,CriteriaService) {

    $scope.nbResults = 25;
    $scope.isLoading = false;

    $scope.sortType = 'productCode';
    $scope.sortReverse = false;
    
    $scope.hasQueryParams = false;
    
    initializeQueryParams();
    function initializeQueryParams() {
        if (CriteriaService.getParamsUrl($scope)) {
            $scope.hasQueryParams = true;
            findSalesLocations();
        }
    }
   
    LocationsService.getDefaultSaleLocation(function(response) {
         var location = undefined;
        
        if ($scope.criteria == undefined){
            $scope.criteria = {
                    location : response.content,
                }
        }
        
        if(response.content != undefined && response.content.id != undefined) {
            location = { id : response.content.id,
                    label : response.content.label};
            $scope.locations = [location];
            LocationsService.getSubLocations({
                locationId : location.id,
            }, function(response) {
                $scope.locations = $scope.locations.concat(response.content);
            });
        } else {
            LocationsService.getAllInfo(function(response) {
                $scope.locations = response.content;
                $scope.nbResults = 9999;
            });
        }
        /*
        Modif EDU - 10-2021 On ne fait plus de recherche systématique des stocks
        if(!$scope.hasQueryParams) {
            $scope.criteria.location = location;
            $scope.searchSalesLocations();
            $scope.nbResults = 9999;
            $scope.isLoading = false;
        }
        */
        
    });
    
    $scope.searchSalesLocations = function() {
        $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
        findSalesLocations();
    }
    
    function findSalesLocations() {
        var locationId = undefined;
        
        if( angular.isDefined($scope.criteria.location) && $scope.criteria.location != null) {
            locationId = $scope.criteria.location.id;
        
            if ($scope.requeteStocks != undefined) {
                $scope.requeteStocks.$cancelRequest();      
            }
            $scope.isLoading = true;
            $scope.requeteStocks = StocksService.getCriteria({
                locationId : locationId,
                reference : $scope.criteria.reference,
                nbResults : $scope.nbResults
            });
            
            $scope.requeteStocks.$promise.then( function(response) {
                $scope.stocks = response.content;
                $scope.isLoading = false;
            },function(httpResponse){
                //console.log("Annulée ou erreur");
            });
        }
    }

    $scope.downloadCSV = function() {
        var locationId = undefined;
        
        if( angular.isDefined($scope.criteria.location)) {
            locationId = $scope.criteria.location.id;
        
            window.downloadFile('/stocks/getCriteria/download?_format=csv&locationId=' + locationId);
        }
    }

});

oseModule.controller('stocksMovementsListController', function stocksMovementsListController($scope, $window, $location, StocksService, ProductsService, LocationsService, CriteriaService) {

    $scope.nbResults = 25;
    $scope.isLoading = false;

    $scope.sortType = 'date';
    $scope.sortReverse = true;

    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
        formatYear : "yyyy",
        startingDay : 1
    };

    $scope.resetHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }

    $scope.mvtTypes = [ {
        id : undefined,
        label : ""
    }, {
        id : 1,
        label : "Achats/Ventes"
    }, {
        id : 2,
        label : "Retours"
    }, {
        id : 3,
        label : "Retours Fournisseurs"
    }, {
        id : 4,
        label : "Mouvements Groupe"
    }, {
        id : 100,
        label : "Inventaire"
    }, {
        id : 1000,
        label : "Mouvements Internes"
    } ];
    
   $scope.hasQueryParams = false;

   
   initializeQueryParams();
    
   function initializeQueryParams() {
      if (CriteriaService.getParamsUrl($scope)) {
          if (angular.isDefined($scope.criteria.nbResults)) {
            $scope.nbResults = $scope.criteria.nbResults;
          }
          $scope.hasQueryParams = true;
          findMovements();
      } else { 
      

        var dFrom = $location.search()['from'] ;
        if(dFrom === undefined) {
          dFrom = moment().subtract(7, 'days').toDate() ;
        }
        var dTo = $location.search()['to'] ;
        if(dTo === undefined) {
          dTo = moment().toDate() ;
        }      
        
        $scope.criteria = {
        locationId : $location.search()['locationId'],
        reference : $location.search()['productId'],
        mvtReference : $location.search()['refId'],
        mvtType : $location.search()['type'],
        from : dFrom,
        to : dTo,
        nbResults : $location.search()['nbResults']
        };
        
        if (angular.isDefined($scope.criteria.nbResults)) {
          $scope.nbResults = $scope.criteria.nbResults;
        }
        
        
      
      }
    } 

   
    LocationsService.getDefaultSaleLocation(function(response) {
        $scope.criteria.location = response.content;
        
        if($scope.criteria.location != undefined) {
            $scope.criteria.location = { id : response.content.id,
                    label : response.content.label};
            $scope.locations = [$scope.criteria.location];
            $scope.criteria.locationId = $scope.criteria.location.id;
            LocationsService.getSubLocations({
                locationId : $scope.criteria.location.id,
            }, function(response) {
                $scope.locations = $scope.locations.concat(response.content);
            });
        } else {
            LocationsService.getAllInfo(function(response) {
                $scope.locations = response.content;
                $scope.locations.unshift({
                    id : undefined,
                    label : ""
                });
            });
        }
        LocationsService.get({
            id : $scope.criteria.locationId
        }, function(response) {
            $scope.criteria.location = response.content;
            ProductsService.get({
                id : $scope.criteria.reference
            }, function(response) {
                $scope.product = response.content;
                $scope.criteria.product = response.content;
                if(!$scope.hasQueryParams) {
                    $scope.getMovements();
                    $scope.nbResults = 9999;
                }
            })
        })
    });

    $scope.getMovements = function() {
      $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
      findMovements();
    }
    
    function findMovements() {
        var from = null;
        if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
            from = $scope.resetHours($scope.criteria.from).getTime();
        }
        var to = null;
        if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
            to = $scope.upgHours($scope.criteria.to).getTime();
        }
        var barcode = null;
        var label = "";
        if (angular.isDefined($scope.criteria.product) && $scope.criteria.product != null) {
            barcode = $scope.criteria.product.barcode;
            label = $scope.criteria.product.label;
        }
        var locationId = null;
        var locationName = "";
        if (angular.isDefined($scope.criteria.location) && $scope.criteria.location != null) {
            locationId = $scope.criteria.location.id;
            locationName = $scope.criteria.location.label;
        }
        
        var mvtReference = null;
        if (angular.isDefined($scope.criteria.mvtReference) && $scope.criteria.mvtReference != "") {
            mvtReference = $scope.criteria.mvtReference;
        }
        var mvtType = null;
        if (angular.isDefined($scope.criteria.mvtType) && $scope.criteria.mvtType != "") {
            mvtType = $scope.criteria.mvtType;
        }
        
        if ($scope.requeteMovements != undefined) {
            $scope.requeteMovements.$cancelRequest();       
        }
        
        if (locationId != null || mvtReference != null || (mvtType != null && from !=null && to !=null)){
            $scope.isLoading = true;
            $scope.requeteMovements = StocksService.getMovements({
                locationId : locationId,
                productId : barcode,
                reference : mvtReference,
                type : mvtType,
                datefrom : from,
                dateto : to,
                nbResults : $scope.nbResults,
                sort : "desc"
            });
            
            $scope.requeteMovements.$promise.then( function(response) {
                $scope.libelle = label + " - " + locationName;
                $scope.movements = response.content;
                $scope.isLoading = false;
            },function(httpResponse){
                //console.log("Annulée ou erreur");
            });
        }

    };

    $scope.getProducts = function(value) {
        if ($scope.requeteProduits != undefined) {
            $scope.requeteProduits.$cancelRequest();
        }
        if (value != "") {
            $scope.criteria.product = undefined;
            // AME : A mon avis il ne fauut pas bouger la liste, ni le loading :
            // interférence avec le tableau des mvts...
            // $scope.products = undefined;
            // $scope.isLoading = true;
            $scope.requeteProduits = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : false
            });
            $scope.requeteProduits.$promise.then(function(response) {
                $scope.products = response.content;
                if ($scope.products.length == 1) {
                    $scope.criteria.product = $scope.products[0];
                }
                // $scope.isLoading = false;
            }, function(httpResponse) {
                // console.log("Annulée ou erreur");
            });
        }
    }

    $scope.downloadCSV = function() {
        var url = '/stocks/getMovements/download?_format=csv&sort=desc&nbResults=0';
        var from = null;
        $scope.isLoading = true;
        if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
            from = $scope.resetHours($scope.criteria.from).getTime();
            url += '&datefrom=' + from;
        }
        var to = null;
        if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
            to = $scope.upgHours($scope.criteria.to).getTime();
            url += '&dateto=' + to;
        }
        var barcode = null;
        if (angular.isDefined($scope.criteria.product) && $scope.criteria.product != null) {
            barcode = $scope.criteria.product.barcode;
            url += '&productId=' + barcode;
        }
        var locationId = null;
        if (angular.isDefined($scope.criteria.location) && $scope.criteria.location != null) {
            locationId = $scope.criteria.location.id;
            url += '&locationId=' + locationId;
        }

        if (angular.isDefined($scope.criteria.mvtReference) && $scope.criteria.mvtReference != null) {
            url += '&reference=' + $scope.criteria.mvtReference;
        }

        if (angular.isDefined($scope.criteria.mvtType) && $scope.criteria.mvtType != null) {
            url += '&type=' + $scope.criteria.mvtType;
        }

        window.downloadFile(url);

        $scope.isLoading = false;
    }

});

oseModule.controller('stocksMovementsFormController', function stocksMovementsFormController($scope, $location, StocksService, LocationsService, ProductsService, CategoriesService) {

    var _action = $location.search()['_action'];
    var locationId = $location.search()['locationId'];
    var productId = $location.search()['productId'];
    var reference = $location.search()['reference'];

    $scope.edit = (_action == "create" || _action == "edit");
    $scope.productMovement = {};
    $scope.isFocused = false;
    $scope.nbResults = 9999;
    $scope.isLoading = true;
    $scope.isDownLoading = false;
    $scope.recents = [];
    $scope.categs = [];
    $scope.categ = {id:"",
            label:"Toutes"};
    
    
    $scope.productList = [];
    $scope.inRepeat = false;
    
  
    CategoriesService.getAll(function(response) {
        if(response.content && response.content.length >0) {
            $scope.categs = response.content;
            $scope.categs.unshift($scope.categ);
        }
    });
    
    $scope.loadModel = function() {
        for (var i = 0, c = $scope.recents.length; i < c; i++) {
            if($scope.recents[i].reference == $scope.movement.reference) {
                //console.log( $scope.movement) ;
                //console.log( $scope.recents[i]) ;
                if(! $scope.movement.note ) {
                    $scope.movement.note = $scope.recents[i].note ;
                }
                if(! $scope.movement.destination ) {
                    $scope.movement.destination = $scope.recents[i].destination ;
                }
                if(! $scope.movement.reason ) {
                    $scope.movement.reason = $scope.recents[i].reason ;
                    $scope.sens = $scope.movement.reason > 0 ? "E" : "S" ;
                    $scope.updateReason();
                    $scope.movement.reasonLabel = $scope.recents[i].reasonLabel ;
                    $scope.reasonUpdated();
                }
                //console.log("Trouvéé!");
                return;
            }
        }
            $scope.movement.note = undefined ;
            $scope.movement.destination = undefined ;
            $scope.movement.reason = undefined ;
            $scope.sens = undefined ;
        
    };
    
    $scope.getRecentMovements = function( type) {
        //Juste les mouvements internes au magasin ou les mouvements internes au groupe
        var debut =new Date();
        debut.setDate(debut.getDate()-7);
        
        StocksService.getMovements({
            datefrom : debut.getTime(),
            resultType : "StockMovementHeaderDTO",
            locationId : locationId,
            type: type,
            nbResults : $scope.nbResults
        }, function(response) {
            var cache = {};
            for (var i = 0, c = response.content.length; i < c; i++) {
                if(cache[response.content[i].reference]) {
                    //On a déjà
                    if(cache[response.content[i].reference].note != response.content[i].note ) {
                        cache[response.content[i].reference].note = undefined ;
                    }
                    if(cache[response.content[i].reference].destination.id != response.content[i].destination.id ) {
                        cache[response.content[i].reference].destination = undefined ;
                    }
                    if(cache[response.content[i].reference].reason != response.content[i].reason ) {
                        cache[response.content[i].reference].reason = undefined ;
                    }
                } else {
                    cache[response.content[i].reference] = response.content[i] ; 
                }
                
            }
            
            for( elem in cache) {
                $scope.recents.push(cache[elem]);
            }
            //console.log($scope.recents);
        });
    }

    if (_action == "create") {
        StocksService.initializeMovement({
            locationId : locationId,
            productId : productId,
        }, function(response) {
            $scope.movement = response.content;
            //$scope.isLoading = false;
            $scope.getRecentMovements(4);
            $scope.getRecentMovements(1000);
            
            $scope.updateProductList(locationId);
        });
        
        

        $scope.productMovement = {
            product : undefined,
            units : 1
        };
    } else {
        $scope.isLoading = true;
        StocksService.getMovements({
            reference : reference,
            nbResults : $scope.nbResults
        }, function(response) {
            $scope.movement = response.content
            if ($scope.movement.reason < 0) {
                $scope.sens = "S";
            } else {
                $scope.sens = "E";
            }
            $scope.isLoading = false;
        });
    }

    $scope.saveMovement = function() {
        $scope.isLoading = true;
        if ($scope.movement.products.length != 0 && ($scope.sens == 'E' || $scope.sens == 'S')) {
            StocksService.saveMovement($scope.movement, function(response) {
                $scope.isLoading = false;
                $location.url("/stocks/movements/form?_action=view&reference=" + response.content.reference);
            });
        }
    };
    
    $scope.updateProductList = function(value) {
    $scope.isLoading = true;
    StocksService.getCriteria({
                locationId : value,
                nbResults : -1
            }, function(response) {
                if(response.content && response.content.length >0) {
                    $scope.productList = response.content;
                }
                $scope.isLoading = false;
            });
            
    }
    
    $scope.locationUpdated = function() {
        if($scope.sens == 'E' && $scope.movement.destination) {
            $scope.updateProductList($scope.movement.destination.id);
        }
    }
    
    $scope.updateReason = function() {
        if($scope.sens == 'S') {
            $scope.updateProductList(locationId);
        } else {
            if($scope.movement.destination) {
                $scope.updateProductList($scope.movement.destination.id);
            }
        }
        StocksService.getReasons({
            sens : $scope.sens
        }, function(response) {
            $scope.reasons = response.content;
        })
    };

    $scope.reasonUpdated = function() {
        if ($scope.movement.reason == -1000 | $scope.movement.reason == 1000) {
            LocationsService.getSubLocations({
                locationId : $scope.movement.location.id,
            }, function(response) {
                $scope.locations = response.content;
                if(response.content.length === 0) {
                    LocationsService.getSistersLocations({
                        locationId : $scope.movement.location.id,
                    }, function(response) {
                        $scope.locations = response.content;

                    });
                }
            });
        } else {
            if ($scope.movement.reason == 4 | $scope.movement.reason == -4) {
                LocationsService.getRemoteLocations({
                    locationId : $scope.movement.location.id,
                }, function(response) {
                    $scope.locations = response.content;
                });
            } else {
                $scope.locations = [];
            }
        }
    };

    $scope.getProducts = function(value) {
        if ($scope.requeteProduits != undefined) {
            $scope.requeteProduits.$cancelRequest();
        }
        if (value != "") {
            $scope.productMovement.product = undefined;
            $scope.products = undefined;
            $scope.requeteProduits = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : false
            });
            $scope.requeteProduits.$promise.then(function(response) {
                $scope.products = response.content;
                if ($scope.products.length == 1) {
                    $scope.productMovement.product = $scope.products[0];
                }
            }, function(httpResponse) {
                // console.log("Annulée ou erreur");
            });
        }
    };
    
    $scope.updateCategs = function(selectedItem) {
        /*
         * 
        console.log(angular.copy($scope.categ));
        console.log(angular.copy($scope.categorie));
        console.log(selectedItem);
        */
        $scope.categ = selectedItem;
    };
    
    $scope.finished = function() {
        $scope.inRepeat = false;
    };

    $scope.addProduct = function() {
        $scope.isLoading = true;
        console.log($scope);
        if ($scope.productMovement.product && $scope.productMovement.units) {
            $scope.movement.products.unshift(angular.copy($scope.productMovement));
            $scope.productMovement = {
                product : undefined,
                units : 1
            };
            $scope.$broadcast('SetFocus');
        }
        $scope.isLoading = false;
    };
    
    $scope.addProducts = function() {
        $scope.isLoading = true;
        var stockelements = angular.copy($scope.productList);
        //console.log(stockelements.length);
        //console.log($scope.categ);
        //console.log($scope);
        
        for(i = stockelements.length ; i-- ; ) {
            stockelement = stockelements[i];
            //console.log(stockelement);
            if($scope.categ == undefined || $scope.categ.id == '' || $scope.categ.id == stockelement.categoryId){
                if(stockelement.units != 0 ){
                    $scope.movement.products.unshift({
                        product : { id :stockelement.productId,
                            barcode: stockelement.productCode,
                            label: stockelement.productLabel
                            },
                        units : stockelement.units
                    });
                }
                //on remove de la base
                $scope.productList.splice(i ,1);
            }
        }
        //console.log($scope.productList.length);
        $scope.isLoading = false;
        $scope.inRepeat = true;
    };

    $scope.removeProductMovement = function(index) {
        $scope.movement.products.splice(index, 1);
    };

    $scope.downloadCSV = function() {
        
        $scope.isDownLoading = true;
        StocksService.DLMovements({
            _format: 'csv' ,
            sort: 'asc' ,
            reference: reference,
            nbResults: $scope.nbResults
        }, function(response) {            

            //'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            var linkElement = document.createElement('a');
            try {
     
                var url = window.URL.createObjectURL(response.blob);
                
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", response.filename);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
                
                $scope.isDownLoading = false;
                
            } catch (ex) {
                console.log(ex);
                $scope.isDownLoading = false;
            }
        });
        
        //window.downloadFile('/stocks/getMovements?_format=csv&sort=asc&reference=' + reference + '&nbResults='
        //        + $scope.nbResults);
    }

});

oseModule.controller('stocksStatsListController', function stocksStatsListController($scope, $rootScope, $uibModal, PropertiesService , StocksService, LocationsService, StatsService) {
    StatsService.getAggregates({}, function(response) {
        $scope.agregates = response.content;
        $scope.aggregateLevel = 'MONTH';
    });

    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
        formatYear : "yyyy",
        formatDay : "dd",
        startingDay : 1,
        minDate : moment().hours(0).minutes(0).seconds(0),
        maxDate : null,
    };

    $scope.isLoading = false;
    $scope.selectedProducts = [];
    $scope.selectedLabel = "";
    $scope.selectedLocations = [];
    $scope.selectedLocationLabel = "";
    $scope.movements = [];
    $scope.nvObjLoc = {};
    $scope.nvObjProd = {};
    $scope.detailed = false;
    
    $scope.isDownLoading = false;
    
    $scope.barLimit = 100;
    
    $scope.increaseLimit = function (value) {
        if(value) {
            $scope.barLimit += 50;
        }
    }

    $scope.optionsMoney = {

        chart : {
            type : 'stackedAreaChart',
            height : 450,
            margin : {
                top : 20,
                right : 20,
                bottom : 50,
                left : 75
            },
            x : function(d) {
                return d[0];
            },
            y : function(d) {
                return d[1];
            },

            color : d3.scale.category10().range(),
            duration : 300,
            useInteractiveGuideline : true,
            clipVoronoi : false,

            xAxis : {
                axisLabel : 'Date',
                tickFormat : function(d) {
                    return d3.time.format('%Y/%m/%d')(new Date(d))
                },
                showMaxMin : false,
                staggerLabels : true
            },

            yAxis : {
                axisLabel : 'Valeur',
                tickFormat : function(d) {
                    return d3.format('.02f')(d) + ' \u20AC';
                },
                showMaxMin : true,

            },

            zoom : {
                enabled : true,
                scaleExtent : [ 1, 10 ],
                useFixedDomain : false,
                useNiceScale : true,
                horizontalOff : false,
                verticalOff : true,
                unzoomEventType : 'dblclick.zoom'
            }
        }
    };

    $scope.optionsQty = {

        chart : {
            type : 'stackedAreaChart',
            height : 450,
            margin : {
                top : 20,
                right : 20,
                bottom : 50,
                left : 65
            },
            x : function(d) {
                return d[0];
            },
            y : function(d) {
                return d[1];
            },

            color : d3.scale.category10().range(),
            duration : 300,
            useInteractiveGuideline : true,
            clipVoronoi : false,

            xAxis : {
                axisLabel : 'Date',
                tickFormat : function(d) {
                    return d3.time.format('%Y/%m/%d')(new Date(d))
                },
                showMaxMin : false,
                staggerLabels : true
            },

            yAxis : {
                axisLabel : 'Quantit\u00E9',
                tickFormat : function(d) {
                    return d3.format('d')(d);
                },
                showMaxMin : true,

            },

            zoom : {
                enabled : true,
                scaleExtent : [ 1, 10 ],
                useFixedDomain : false,
                useNiceScale : true,
                horizontalOff : false,
                verticalOff : true,
                unzoomEventType : 'dblclick.zoom'
            }
        }
    };
    
    $scope.updMinDate = function(theDate){
        $scope.dateOptions.minDate = theDate ;
    };
    
    PropertiesService.stockHistory({}, function(response) {
        var mnt = moment().subtract(response.content , 'days').hours(0).minutes(0).seconds(0);
        $scope.updMinDate(mnt); 
    });
    
    var dTo = moment().toDate();
    
    //EDU : modif 20180813 - trop d'erreurs de requêtes résultant en une surcharge du serveur et un freeze pour les users
    // 1 j par défaut limité à x jours (contrainte spring.data.extractlimit.stockhistory.maxdays dans la config serveur) 
    var dFrom = moment().toDate();

    $scope.resetHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }

    var modalScope = $rootScope.$new();
    $scope.selectProductsList = function() {
        modalScope.selectedProducts = $scope.selectedProducts;
        modalScope.composition = false ;
        modalScope.categories = true;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-product-form.html',
            controller : 'selectProductsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.selectedProducts = data;
            $scope.selectedLabel = "";
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                $scope.selectedLabel += $scope.selectedProducts[i].id;
                if ($scope.selectedProducts.length > 1 && i < $scope.selectedProducts.length - 1) {
                    $scope.selectedLabel += ", "
                }
            }
            //console.log($scope.selectedLabel);
        });
    }

    $scope.selectLocationsList = function() {
        modalScope.selectedLocations = $scope.selectedLocations;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-location-form.html',
            controller : 'selectLocationsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.selectedLocations = data;
            $scope.selectedLocationLabel = "";
            for (var i = 0; i < $scope.selectedLocations.length; i++) {
                $scope.selectedLocationLabel += $scope.selectedLocations[i].label;
                if ($scope.selectedLocations.length > 1 && i < $scope.selectedLocations.length - 1) {
                    $scope.selectedLocationLabel += ", "
                }
            }
            //console.log($scope.selectedLocationLabel);
        });
    }

    $scope.clearProductsList = function() {
        $scope.selectedProducts = [];
    }

    $scope.clearLocationsList = function() {
        $scope.selectedLocations = [];
    }

    $scope.criteria = {
        from : dFrom,
        to : dTo
    };

    // Chart data
    $scope.locationGraphVisible = function(value) {
        var retour = true;
        var test = {
            testId : value
        };

        var indice = $scope.retour.locationsGraph.filter(function(el) {
            return el.hiddenKey == this.testId;
        }, test);

        for (var i = 0; i < indice.length; i++) {
            // TODO Peut être ajouter un test afin de savoir si il y a eu une
            // modification de la visibilité manuelle sur le graph
            // // Assuming your chart is called 'chart'
            // var state = chart.state();
            //
            // for(var i=0; i < state.disabled.length; i++) {
            // state.disabled[i] = ...LOGIC RETURNING TRUE OR FALSE...;
            // }
            //
            // chart.dispatch.changeState(state);
            // chart.update();
            retour = retour && !indice[i].disabled;
        }
        return retour;
    }

    // TODO On conserve deux fonctions ? peut être qu'on voudra jouer differement
    // par exemple location + product ?
    // tout dépend aussi de ce qu'on pose dans hidenKey
    $scope.detailsGraphVisible = function(value) {
        var retour = true;
        var test = {
            testId : value
        };

        var indice = $scope.retour.detailsGraph.filter(function(el) {
            return el.hiddenKey == this.testId;
        }, test);

        for (var i = 0; i < indice.length; i++) {
            retour = retour && !indice[i].disabled;
        }
        return retour;
    }

    $scope.onChange = function(locId, type) {
        var test = {
            testId : locId
        };
        var tested = undefined;
        var visibility_function = undefined;
        var new_visibility = true;
        if (type == "location") {
            tested = $scope.retour.locationsGraph;
            visibility_function = $scope.locationGraphVisible;
        }
        if (type == "product") {
            tested = $scope.retour.detailsGraph;
            visibility_function = $scope.detailsGraphVisible;
        }

        var indice = tested.filter(function(el) {
            return el.hiddenKey == this.testId;
        }, test);
        // On regarde l'état actuel de l'affichage
        new_visibility = visibility_function(locId);

        for (var i = 0; i < indice.length; i++) {
            // On l'applique à tous
            indice[i].disabled = new_visibility;
        }
    }

    $scope.getStats = function() {
        var from = null;
        $scope.isLoading = true;
        if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
            from = $scope.resetHours($scope.criteria.from).getTime();
        }
        var to = null;
        if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
            to = $scope.upgHours($scope.criteria.to).getTime();
        }
        StatsService.getStockList({
            products : angular.toJson($scope.selectedProducts),
            locations : angular.toJson($scope.selectedLocations),
            from : from,
            to : to,
            _groupBy : $scope.aggregateLevel,
            detailed : $scope.detailed
        }, function(response) {
            if ($scope.nvObjLoc.api) {
                $scope.nvObjLoc.api.clearElement();
                // console.log("in clear Loc");
                $scope.nvObjLoc = {};
            }
            if ($scope.nvObjProd.api) {
                $scope.nvObjProd.api.clearElement();
                // console.log("in clear prod");
                $scope.nvObjProd = {};
            }

            $scope.criteria.from = response.content.startDate;
            $scope.criteria.to = response.content.endDate;
            $scope.retour = response.content;
            $scope.movements = response.content.details;

            if ($scope.nvObjProd.api) {
                // TODO voir par là inhibition en dur du graph par produit
                // $scope.retour.detailsGraph = [] ;
            }
            $scope.isLoading = false;
        });
    }

    $scope.download = function(format) {
        
        $scope.isDownLoading = true;
        
        var from = undefined;
        if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
            from = $scope.resetHours($scope.criteria.from).getTime();
        }
        var to = undefined;
        if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
            to = $scope.upgHours($scope.criteria.to).getTime();
        }
        StatsService.DLStockList({
            _format : format,
            products : angular.toJson($scope.selectedProducts),
            locations : angular.toJson($scope.selectedLocations),
            from : from,
            to : to,
            _groupBy : $scope.aggregateLevel,
            detailed : $scope.detailed
        }, function(response) {

            //'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            var linkElement = document.createElement('a');
            try {
     
                var url = window.URL.createObjectURL(response.blob);
                
                linkElement.setAttribute('href', url);
                linkElement.setAttribute("download", response.filename);
     
                var clickEvent = new MouseEvent("click", {
                    "view": window,
                    "bubbles": true,
                    "cancelable": false
                });
                linkElement.dispatchEvent(clickEvent);
                
                $scope.isDownLoading = false;
                
            } catch (ex) {
                console.log(ex);
                $scope.isDownLoading = false;
            }
        });
        //var url = '/stats/stock/getList?_format=' + format + '&_groupBy=' + $scope.aggregateLevel + '&detailed='
        //        + $scope.detailed + '&from=' + $scope.criteria.from.getTime() + '&to=' + $scope.criteria.to.getTime() + '&products='
        //        + angular.toJson($scope.selectedProducts) + '&locations=' + angular.toJson($scope.selectedLocations);
        // console.log(url);
        //window.downloadFile(url);
    }

    // TODO Ajuster les bornes temporelles afin qu'elles coincident avec le niveau de détail choisi :
    // from doit être un lundi si on parle en semaines, premier du mois si on parle de mois et 01/01 si on parle en année
});

oseModule.controller('stocksControlsListController', function stocksControlsListController($scope, $rootScope, $uibModal, $location, StocksService, LocationsService,
        ProductsService) {
    // L'ensemble des fonctions pour gérer les stock type

    // Obtenir les stocks Level par produit, par magasin et par tariffaire

    // Par date d'activité ?

    // sauver les modifications

    // Pagination ?

    // Filtre ?

    // Fonctions complémentaires (ventes , stock , suggestion )

    $scope.isOpen = true;
    $scope.isLoading = false;
    $scope.selectedProducts = [];
    $scope.selectedLabel = "";
    $scope.selectedLocations = [];
    $scope.selectedLocationLabel = "";
    $scope.selectedTariffs = [];
    $scope.selectedTariffLabel = "";
    $scope.grid = {};
    $scope.nbEdit = 0;

    var modalScope = $rootScope.$new();
    $scope.selectProductsList = function() {
        modalScope.selectedProducts = $scope.selectedProducts;
        modalScope.composition = false;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-product-form.html',
            controller : 'selectProductsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.selectedProducts = data;
            $scope.selectedLabel = "";
            for (var i = 0; i < $scope.selectedProducts.length; i++) {
                $scope.selectedLabel += $scope.selectedProducts[i].id + "-" + $scope.selectedProducts[i].label;
                if ($scope.selectedProducts.length > 1 && i < $scope.selectedProducts.length - 1) {
                    $scope.selectedLabel += ", "
                }
            }
            // console.log($scope.selectedLabel);
        });
    }

    $scope.selectLocationsList = function() {
        modalScope.selectedLocations = $scope.selectedLocations;
        modalScope.parentOnly = true;
        modalScope.limit = 30;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-location-form.html',
            controller : 'selectLocationsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.selectedLocations = data;
            $scope.selectedLocationLabel = "";
            for (var i = 0; i < $scope.selectedLocations.length; i++) {
                $scope.selectedLocationLabel += $scope.selectedLocations[i].label;
                if ($scope.selectedLocations.length > 1 && i < $scope.selectedLocations.length - 1) {
                    $scope.selectedLocationLabel += ", "
                }
            }
            // console.log($scope.selectedLocationLabel);
        });
    }

    $scope.selectTariffsList = function() {
        modalScope.selectedTariffs = $scope.selectedTariffs;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-tariff-form.html',
            controller : 'selectTariffsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.selectedTariffs = data;
            $scope.selectedTariffLabel = "";
            $scope.unSelectedTariffLabel = "";
            for (var i = 0; i < $scope.selectedTariffs.length; i++) {

                if ($scope.selectedTariffs[i].exclu == undefined || !$scope.selectedTariffs[i].exclu) {
                    if ($scope.selectedTariffLabel.length > 1) {
                        $scope.selectedTariffLabel += ", "
                    }
                    $scope.selectedTariffLabel += $scope.selectedTariffs[i].id + "-" + $scope.selectedTariffs[i].label;
                } else {
                    if ($scope.unSelectedTariffLabel.length > 1) {
                        $scope.unSelectedTariffLabel += ", "
                    }
                    $scope.unSelectedTariffLabel += $scope.selectedTariffs[i].id + "-" + $scope.selectedTariffs[i].label;

                }

            }
            // console.log($scope.selectedTariffLabel);
        });
    }

    $scope.clearProductsList = function() {
        $scope.selectedProducts = [];
    }

    $scope.clearTariffsList = function() {
        $scope.selectedTariffs = [];
        $scope.selectedTariffLabel = "";
        $scope.unSelectedTariffLabel = "";
    }

    $scope.clearLocationsList = function() {
        $scope.selectedLocations = [];
    }

    $scope.editStockLevel = function($stockLevel) {
        $stockLevel.isEdit = true;
        $scope.nbEdit++;
    };

    $scope.isEdit = function($stockLevel) {
        return $stockLevel.isEdit != undefined && $stockLevel.isEdit;
    };

    $scope.isNew = function($stockLevel) {
        return $stockLevel.id == null || (typeof $stockLevel.isNew !== 'undefined' && $stockLevel.isNew);
    };

    $scope.razStockLevel = function($stockLevel) {
        if ($scope.isNew($stockLevel)) {
            var i = 0;
            var reponse = false;
            if ($scope.isEdit($stockLevel)) {
                $scope.nbEdit--;
            }
            $stockLevel.isEdit = false;
            $stockLevel.stockSecurity = 0;
            $stockLevel.stockMaximum = 0;
        } else {
            $stockLevel.stockSecurity = 0;
            $stockLevel.stockMaximum = 0;
            if (!$scope.isEdit($stockLevel)) {
                $scope.nbEdit++;
            }
            $stockLevel.isEdit = true;
            $stockLevel.isNew = undefined;
        }
        return false;
    };

    $scope.saveStockLevel = function($stockLevel) {
        $stockLevel.isEdit = undefined;

        StocksService.saveStockLevel($stockLevel, function(response) {
            $stockLevel.isNew = undefined;
        });

        $stockLevel.isEdit = false;
        $scope.nbEdit--;
        return false;
    };

    $scope.saveAll = function() {

        Object.keys($scope.stockLevels[0]).forEach(function(key, index) {
            for (var i = 0; i < $scope.stockLevels.length; i++) {
                if (key != 'product') {
                    if ($scope.isEdit($scope.stockLevels[i][key])) {
                        $scope.saveStockLevel($scope.stockLevels[i][key]);
                    }
                }
            }
        });
    };

    $scope.clearAll = function() {
        Object.keys($scope.stockLevels[0]).forEach(function(key, index) {
            for (var i = 0; i < $scope.stockLevels.length; i++) {
                if (key != 'product') {
                    $scope.razStockLevel($scope.stockLevels[i][key]);
                }
            }
        });
    };

    $scope.getStocksLevel = function() {
        $scope.isLoading = true;

        StocksService.getStockLevelTableByCriteria({
            locations : angular.toJson($scope.selectedLocations),
            products : angular.toJson($scope.selectedProducts),
            tariffs : angular.toJson($scope.selectedTariffs)
        }, function(response) {
            $scope.stockLevels = response.content;
            $scope.updateGrid();
            $scope.updateHeaders();
            $scope.isLoading = false;
            $scope.nbEdit = 0;
        });
    };

    $scope.highlightFilteredHeader = function(row, rowRenderIndex, col, colRenderIndex) {
        if (col.filters[0].term) {
            return 'header-filtered';
        } else {
            return '';
        }
    };

    $scope.setdisplayName = function(key) {
        var dispKey = key;
        if (dispKey.charAt(0) == '_') {
            dispKey = key.substring(1);
        }
        dispKey = dispKey.replace('_', '\n');
        dispKey = dispKey.replace(/(_+)/g, ' ');
        if (dispKey.charAt(0).match(/[a-z]/)) {
            dispKey = dispKey.charAt(0).toUpperCase() + dispKey.substring(1);
        }
        return dispKey;
    }

    $scope.headerLine1 = function(col) {
        var position = col.displayName.indexOf('\n');
        if (position > 0) {
            return col.displayName.substr(0, position);
        }
        return col.displayName;
    }

    $scope.multiLineHeader = function(col) {
        return col.displayName.indexOf('\n') > 0;
    }

    $scope.headerLine2 = function(col) {
        var position = col.displayName.indexOf('\n');
        if (position > 0) {
            return col.displayName.substr(position);
        }
        return '';
    }

    $scope.sortStockLevels = function(a, b, rowA, rowB, direction) {
        var nulls = $scope.gridApi.core.sortHandleNulls(a, b);
        if (nulls !== null) {
            return nulls;
        } else {
            if (a.stockSecurity == b.stockSecurity) {
                if (a.stockMaximum > b.stockMaximum) {
                    return 1;
                }
                if (a.stockMaximum < b.stockMaximum) {
                    return -1;
                }
                return 0;
            }
            if (a.stockSecurity > b.stockSecurity) {
                return 1;
            }
            if (a.stockSecurity < b.stockSecurity) {
                return 1;
            }
            return 0;
        }
    }

    $scope.sortProducts = function(a, b, rowA, rowB, direction) {
        var nulls = $scope.gridApi.core.sortHandleNulls(a, b);
        if (nulls !== null) {
            return nulls;
        } else {
            if (a.label == b.label) {
                if (a.id > b.id) {
                    return 1;
                }
                if (a.id < b.id) {
                    return -1;
                }
                return 0;
            }
            if (a.label > b.label) {
                return 1;
            }
            if (a.label < b.label) {
                return -1;
            }
            return 0;
        }
    }
    $scope.updateHeaders = function() {
          if ($scope.stockLevels != undefined && $scope.stockLevels.length > 0) {
              $scope.entetes = [];
              angular.forEach($scope.stockLevels[0].stockLevelLocationsDTO, function (value, key) {
                      if ($scope.entetes.indexOf(value.enteteColonne)==-1){
                        $scope.entetes.push(value.enteteColonne);
                      }
              });
          }
    }
    $scope.updateGrid = function() {
        $scope.grid.columnDefs = [];
        $scope.grid.enableFiltering = true;
        $scope.grid.rowHeight = 90;
        $scope.grid.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
        };

        if ($scope.stockLevels != undefined && $scope.stockLevels.length > 0) {

            $scope.grid.minRowsToShow = Math.min(10, $scope.stockLevels.length);
            Object.keys($scope.stockLevels[0]).sort().forEach(function(key, index) {
                if (key != 'product') {
                    $scope.grid.columnDefs.push({
                        name : key,
                        displayName : $scope.setdisplayName(key),
                        headerCellTemplate : '/partial/templates/uiGridHeaderCell-template.html',
                        cellTemplate : '/partial/templates/uiGridCellStockLevel-template.html',
                        minWidth : 120,
                        maxWidth : 350,
                        enablePinning : false,
                        headerCellClass : $scope.highlightFilteredHeader,
                        enableFiltering : false,
                        sortingAlgorithm : $scope.sortStockLevels
                    });
                } else {
                    $scope.grid.columnDefs.push({
                        name : key,
                        displayName : $scope.setdisplayName(key),
                        cellTemplate : '/partial/templates/uiGridCellProduct-template.html',
                        minWidth : 150,
                        maxWidth : 350,
                        pinnedLeft : true,
                        headerCellClass : $scope.highlightFilteredHeader,
                        sortingAlgorithm : $scope.sortProducts,
                        filter : {
                            condition : function(searchTerm, cellValue) {
                                return cellValue.label.match(new RegExp(searchTerm, 'i')) || cellValue.id.match(searchTerm);
                            }
                        },
                        enableFiltering : true
                    });
                }
            });
        }
        
        $scope.grid.data = $scope.stockLevels;

    };

});

oseModule.directive('dynamicColspan', ['$compile', dynamicColspanDirective]);

function dynamicColspanDirective($compile) {
return {
restrict: 'A',
template: '',
link: function(scope, elem, attr) {
        function setColspan() {
          var colsToSpan = scope.$eval(attr.dynamicColspan);
          if(angular.isObject(colsToSpan)) {
            angular.forEach(colsToSpan, function(condition, key) {
              if(condition && isFinite(key)) {
                colsToSpan = key;
              }
            }); 
          }
          
          elem.attr('colspan', parseInt(colsToSpan, 10));
        }
        setColspan();
        scope.$watch(attr.dynamicColspan, setColspan);
      }
};
}