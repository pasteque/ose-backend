oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $routeProvider.when('/customers/list', {
    templateUrl : '/partial/customers/customers-list.html',
    controller : 'customersListController',
    reloadOnSearch : false
  });
  $routeProvider.when('/customers/:id/form', {
    templateUrl : '/partial/customers/customers-form.html',
    controller : 'customersFormController'
  });
  $routeProvider.when('/customers/form', {
    templateUrl : '/partial/customers/customers-form.html',
    controller : 'customersFormController'
  });

} ]);

oseModule.factory('CustomersService', function CustomersService($resource) {
  return $resource('/customers/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria'
      },
      isArray : false,
      cancellable : true
    },
    DLByCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria'
      },
      cancellable : true,
      responseType : 'arraybuffer',
      transformResponse : function(data, headers) {
        var response = {};
        response.headers = headers();

        if (headers('content-disposition')) {
          response.filename = headers('content-disposition').split(';')[1].trim().split('=')[1].replace(/"/g, '');
          response.contentType = headers('content-type');
          if (data) {
            response.blob = new Blob([ data ], {
              type : response.contentType
            });
          }
        }

        return response;
      }
    },
    getNextCode : {
      method : 'GET',
      params : {
        param1 : 'getNextCode'
      },
      isArray : false
    },
    saveCustomer : {
      method : 'POST',
      params : {
        param1 : 'saveCustomer'
      },
      isArray : false
    },
    deleteCustomerParent : {
      method : 'POST',
      params : {
        param1 : 'deleteCustomerParent'
      },
      isArray : false
    },
    saveCustomerParent : {
      method : 'POST',
      params : {
        param1 : 'saveCustomerParent'
      },
      isArray : false
    },
  });
});

oseModule.factory('InseeService', function InseeService($resource) {
  return $resource('/insee/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getCriteria : {
      method : 'GET',
      params : {
        param1 : 'getCriteria'
      },
      isArray : false,
      cancellable : true
    }
  });
});

oseModule.factory('PaysService', function PaysService($resource) {
  return $resource('/pays/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    }
  });
});

oseModule.factory('DiscountProfilService', function DiscountProfilService($resource) {
  return $resource('/discount-profiles/:param1:id/:value1', {
    param1 : '@param1',
    value1 : '@value1'
  }, {
    getAll : {
      method : 'GET',
      params : {
        param1 : 'getAll'
      },
      isArray : false
    }
  });
});

oseModule.controller('customersListController', function customersListController($scope, $window, $location, CustomersService, LocationsService, InseeService,
    DiscountProfilService,CriteriaService) {
  $scope.isLoading = false;
  $scope.sortType = undefined;
  $scope.sortReverse = false;

  $scope.openFrom = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedFrom = true;
  };

  $scope.openTo = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedTo = true;
  };

  $scope.dateOptions = {
    formatYear : "yyyy",
    startingDay : 1
  };

  $scope.xx = [ 5, 10, 15, 25, 50, 100, 200 ];

  $scope.isDownLoading = false;

  $scope.criteria = {
    from : undefined,
    to : undefined,
    pdv : {
      id : undefined,
      label : "-"
    },
    reference : undefined,
    insee : undefined,
    radius : 5,
    profilConstraint : true,
    parentConstraint : false
  }
  
  initializeQueryParams();

  function initializeQueryParams() {
      if (angular.isDefined($location.search()['phone'])) {
          $scope.criteria.searchphone = $location.search()['phone'];
          $scope.hasQueryParams = true
      }
      if (angular.isDefined($location.search()['sviChoice'])) {
          $scope.sviChoice = $location.search()['sviChoice'];
      }
      if (CriteriaService.getParamsUrl($scope) || $scope.hasQueryParams == true) {
          findCustomers();
      }
  }
  
  function resetHours (theDate) {
      var mmt = moment(theDate);
      mmt.seconds(0);
      mmt.minutes(0);
      mmt.hours(0);
      return mmt.toDate();
  }
  
  LocationsService.getParentLocations(function(response) {
    $scope.pdvs = response.content;
    $scope.pdvs.unshift({
      id : undefined,
      label : "-"
    });
    LocationsService.getDefaultSaleLocation(function(response) {
      $scope.criteria.pdv = response.content;
      if ($scope.criteria.pdv != undefined) {
        $scope.criteria.pdv = {
          id : response.content.id,
          label : response.content.label
        };
      }
    });
  });

  DiscountProfilService.getAll(function(response) {
    $scope.profils = response.content;
  });
  
  $scope.passCriteria = function() {
          return btoa(JSON.stringify($scope.criteria));
      }

  $scope.searchCustomers = function() {
          $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
          findCustomers();
      }
          
  function findCustomers() {
    var from = null;
    if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
      from = resetHours($scope.criteria.from).getTime();
    }
    var to = null;
    if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
      to = resetHours($scope.criteria.to).getTime();
    }

    var pdvId = undefined;
    if (angular.isDefined($scope.criteria.pdv) && $scope.criteria.pdv != null) {
      pdvId = $scope.criteria.pdv.id;
    }

    var emailConstraint = undefined;
    if (angular.isDefined($scope.criteria.emailConstraint) && $scope.criteria.emailConstraint != null) {
      emailConstraint = $scope.criteria.emailConstraint;
    }

    var parentConstraint = undefined;
    if (angular.isDefined($scope.criteria.parentConstraint) && $scope.criteria.parentConstraint != null) {
      parentConstraint = $scope.criteria.parentConstraint;
    }

    var profilConstraint = undefined;
    if (angular.isDefined($scope.criteria.profilConstraint) && $scope.criteria.profilConstraint != null) {
      profilConstraint = $scope.criteria.profilConstraint;
    }

    var profilId = undefined;
    if (angular.isDefined($scope.criteria.profil) && $scope.criteria.profil != null) {
      profilId = $scope.criteria.profil.id;
    }

    var inseeId = undefined;
    if (angular.isDefined($scope.criteria.insee) && $scope.criteria.insee != null) {
      inseeId = $scope.criteria.insee.insee;
    }

    var lastname = undefined;
    if (angular.isDefined($scope.criteria.lastname) && $scope.criteria.lastname != null && $scope.criteria.lastname.length > 0) {
      lastname = "{\"name\": \"Nom\",\"column\": \"lastName\",\"searchvalue\": [\"" + $scope.criteria.lastname + "\"]}";
    }

    var firstname = undefined;
    if (angular.isDefined($scope.criteria.firstname) && $scope.criteria.firstname != null && $scope.criteria.firstname.length > 0) {
      firstname = "{\"name\": \"Prénon\",\"column\": \"firstName\",\"searchvalue\": [\"" + $scope.criteria.firstname + "\"]}";
    }

    
    var searchkey = undefined;
    if (angular.isDefined($scope.criteria.searchkey) && $scope.criteria.searchkey != null && $scope.criteria.searchkey.length > 0) {
      // searchkey = "{\"name\": \"Numéro client\",\"column\": \"card\",\"searchvalue\": [\"" + $scope.criteria.searchkey + "\"]}";
       searchkey = $scope.criteria.searchkey ;
    }
    
    var email = undefined;
    if (angular.isDefined($scope.criteria.searchmail) && $scope.criteria.searchmail != null && $scope.criteria.searchmail.length > 0) {
      email = "{\"name\": \"e-Mail\",\"column\": \"email\",\"searchvalue\": [\"" + $scope.criteria.searchmail + "\"]}";
    }
    var phone = undefined;
    if (angular.isDefined($scope.criteria.searchphone) && $scope.criteria.searchphone != null && $scope.criteria.searchphone.length > 0) {
      phone = "{\"name\": \"Tel\",\"column\": \"phone\",\"searchvalue\": [\"" + $scope.criteria.searchphone + "\"]}";
    }
    
    $scope.isLoading = true;
    CustomersService.getCriteria({
      _format : 'list',
      lastName : lastname,
      firstName : firstname,
      email : email,
      phone : phone,
      reference : searchkey,
      from : from,
      to : to,
      pdvId : pdvId,
      emailConstraint : emailConstraint,
      parentConstraint : parentConstraint,
      insee : inseeId,
      radius : $scope.criteria.radius,
      profilId : profilId,
      profilConstraint : profilConstraint
    }, function(response) {
      $scope.customers = response.content;
      $scope.isLoading = false;
      if (angular.isDefined($scope.sviChoice) && $scope.customers.length==1) {
        $window.location.href = "customers/" + $scope.customers[0].id + "/form?_action=view"
      }
    });
  };

  $scope.downloadCSV = function() {
    var lastname = undefined;
    var firstname = undefined;
    var searchkey = undefined;
    var to = null;
    var from = null;
    var pdvId = undefined;
    var inseeId = undefined;
    var emailConstraint = undefined;
    var parentConstraint = undefined;

    var radius = undefined;

    if (angular.isDefined($scope.criteria.lastname) && $scope.criteria.lastname != null && $scope.criteria.lastname.length > 0) {
      lastname = "{\"name\": \"Nom\",\"column\": \"lastName\",\"searchvalue\": [\"" + $scope.criteria.lastname + "\"]}";
    }

    if (angular.isDefined($scope.criteria.firstname) && $scope.criteria.firstname != null && $scope.criteria.firstname.length > 0) {
      firstname = "{\"name\": \"Prénon\",\"column\": \"firstName\",\"searchvalue\": [\"" + $scope.criteria.firstname + "\"]}";
    }

    if (angular.isDefined($scope.criteria.searchkey) && $scope.criteria.searchkey != null && $scope.criteria.searchkey.length > 0) {
      searchkey = "{\"name\": \"Numéro client\",\"column\": \"card\",\"searchvalue\": [\"" + $scope.criteria.searchkey + "\"]}";
    }

    if (angular.isDefined($scope.criteria.from) && $scope.criteria.from != null) {
      from = resetHours($scope.criteria.from).getTime();
    }
    if (angular.isDefined($scope.criteria.to) && $scope.criteria.to != null) {
      to = resetHours($scope.criteria.to).getTime();
    }
    if (angular.isDefined($scope.criteria.pdv) && $scope.criteria.pdv != null && angular.isDefined($scope.criteria.pdv.id)) {

      pdvId = $scope.criteria.pdv.id;
    }
    if (angular.isDefined($scope.criteria.insee) && $scope.criteria.insee != null) {

      inseeId = $scope.criteria.insee.insee;
      radius = $scope.criteria.radius;
    }
    if (angular.isDefined($scope.criteria.emailConstraint) && $scope.criteria.emailConstraint != null) {

      emailConstraint = $scope.criteria.emailConstraint;
    }
    if (angular.isDefined($scope.criteria.parentConstraint) && $scope.criteria.parentConstraint != null) {

      parentConstraint = $scope.criteria.parentConstraint;
    }
    
    var email = undefined;
    if (angular.isDefined($scope.criteria.searchmail) && $scope.criteria.searchmail != null && $scope.criteria.searchmail.length > 0) {
      email = "{\"name\": \"e-Mail\",\"column\": \"email\",\"searchvalue\": [\"" + $scope.criteria.searchmail + "\"]}";
    }
    var phone = undefined;
    if (angular.isDefined($scope.criteria.searchphone) && $scope.criteria.searchphone != null && $scope.criteria.searchphone.length > 0) {
      phone = "{\"name\": \"Tel\",\"column\": \"phone\",\"searchvalue\": [\"" + $scope.criteria.searchphone + "\"]}";
    }
    
    var profilConstraint = undefined;
    if (angular.isDefined($scope.criteria.profilConstraint) && $scope.criteria.profilConstraint != null) {
      profilConstraint = $scope.criteria.profilConstraint;
    }

    var profilId = undefined;
    if (angular.isDefined($scope.criteria.profil) && $scope.criteria.profil != null) {
      profilId = $scope.criteria.profil.id;
    }

    $scope.isLoading = true;
    $scope.isDownLoading = true;
    CustomersService.DLByCriteria({
      _format : 'xls',
      lastName : lastname,
      firstName : firstname,
      email : email,
      phone : phone,
      card : searchkey,
      from : from,
      to : to,
      pdvId : pdvId,
      insee : inseeId,
      radius : radius,
      emailConstraint : emailConstraint,
      parentConstraint : parentConstraint,
      profilId : profilId,
      profilConstraint : profilConstraint
    }, function(response) {

      // 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
      var linkElement = document.createElement('a');
      try {

        var url = window.URL.createObjectURL(response.blob);

        linkElement.setAttribute('href', url);
        linkElement.setAttribute("download", response.filename);

        var clickEvent = new MouseEvent("click", {
          "view" : window,
          "bubbles" : true,
          "cancelable" : false
        });
        linkElement.dispatchEvent(clickEvent);
        $scope.isLoading = false;
        $scope.isDownLoading = false;

      } catch (ex) {
        console.log(ex);
        $scope.isLoading = false;
        $scope.isDownLoading = false;
      }
    });
  }

  $scope.getCities = function(value) {
    if ($scope.requete != undefined) {
      $scope.requete.$cancelRequest();
    }
    if (value != "") {
      $scope.requete = InseeService.getCriteria({
        reference : value,
        limit : 55,
      });
      $scope.requete.$promise.then(function(response) {
        $scope.insees = response.content;
      }, function(httpResponse) {
        // console.log("Annulée ou erreur");
      });
    }
  }
});

oseModule.controller('customersFormController', function customersFormController($scope, $location, $routeParams, CustomersService,
    InseeService, LocationsService, PaysService, DiscountProfilService, CriteriaService, TicketsService, PermissionsService) {

  $scope.sortType = undefined;
  $scope.sortReverse = false;
  $scope.criteria = [];
  
  $scope.canEditSearchkey = PermissionsService.hasPermission("PERM_CLIENT_MODIF_CODE");
  
  LocationsService.getParentLocations(function(response) {
    $scope.pdvs = response.content;
    $scope.pdvs.unshift({
      id : undefined,
      label : "-"
    });
    LocationsService.getDefaultSaleLocation(function(response) {
      if (response.content != undefined) {
        $scope.criteria.pdv = {
          id : response.content.id,
          label : response.content.label
        };
      }
    });
  });

  PaysService.getAll(function(response) {
    $scope.pays = response.content;
  });

  DiscountProfilService.getAll(function(response) {
    $scope.profils = response.content;
  });

  var _action = $location.search()['_action'];
  $scope.edit = (_action == "create" || _action == "edit");
  $scope.customer = {};
  // $scope.customer.selectedInsee = {};
  // $scope.customer.selectedCountry = {};
  $scope.insees = [];

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    if (typeof ($scope.mydp) === 'undefined') {
      $scope.mydp = {};
    }
    $scope.mydp.opened = true;
  };
  $scope.dateOptions = {
    formatYear : "yyyy",
    formatDay : "dd",
    startingDay : 1,
    maxDate : moment()
  };

  if (_action == "create") {
    CustomersService.getNextCode(function(response) {
      $scope.customer = {
        searchKey : response.content,
        cardNumber : "c" + response.content,
        country : 'FRANCE',
        selectedCountry : {
          nom : 'FRANCE'
        },
      };
      if ($scope.profils && $scope.profils.length > 0) {
        $scope.customer.discountProfile = $scope.profils[0]
      }

      if (CriteriaService.getParamsUrl($scope) ) {
        //c'est ici que ça se passe pour initialiser les valeurs depuis le paramètre criteria
        $scope.customer.lastName = $scope.criteria.lastname;
        $scope.customer.firstName = $scope.criteria.firstname;
        $scope.customer.phone1 = $scope.criteria.searchphone;
        $scope.customer.email = $scope.criteria.searchmail;
      }
    });

  } else {
    CustomersService.get({
      id : $routeParams.id,
    }, function(response) {
      $scope.customer = response.content;
      if ($scope.customer.zipCode) {
        $scope.customer.selectedInsee = {
          zipCode : $scope.customer.zipCode,
          commune : $scope.customer.city,
          insee : $scope.customer.insee
        }
        $scope.insees.push($scope.customer.selectedInsee);
      }
      $scope.customer.selectedCountry = {
        nom : $scope.customer.country
      }
    });
    TicketsService.findCustomerTickets({
      customerId : $routeParams.id,
    }, function(response) {
      $scope.tickets = response.content;
      });
  }

  $scope.editCustomer = function() {
    $location.search({
      _action : "edit"
    });
  }

  $scope.cancelCustomer = function() {
    $location.search({
      _action : "view"
    });
  }

  $scope.checkFormValidity = function() {
    var _code_invalid = $scope.customer.civilite != 'MME' && $scope.customer.civilite != 'M' && $scope.customer.civilite != 'STE';
    if (_code_invalid) {
      return false;
    }
    if (!$scope.customersForm.$valid) {
      if ($scope.customersForm.$error.date !== undefined && $scope.customersForm.$error.date.length > 0) {
        // ok on a un pb de date
        if ($scope.customersForm.dateOfBirth.$viewValue == "") {
          $scope.customer.dateOfBirth = null;
          return true;
        }
        if ($scope.customersForm.$error.date.indexOf($scope.customersForm.dateOfBirth
            && $scope.customersForm.dateOfBirth.$viewValue.length > 0)) {
          var dateString = $scope.customersForm.dateOfBirth.$viewValue.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
          $scope.customer.dateOfBirth = new Date(dateString[3], dateString[2] - 1, dateString[1]);
          return ($scope.customer.dateOfBirth instanceof Date && isFinite($scope.customer.dateOfBirth));
        }
      }

      return false;
    }
    return true;
  }
  $scope.saveCustomer = function() {
    if ($scope.checkFormValidity()) {
      // AME : Vérif avant save pour blinder le truc... car l'init du départ
      // pourrait mal se passer
      // evite de jouer avec des promesses
      if (!$scope.customer.discountProfile && $scope.profils && $scope.profils.length > 0) {
        $scope.customer.discountProfile = $scope.profils[0]
      }
      var isConfirmed = true;
      if ($scope.customer.selectedAdress !== undefined && $scope.customer.selectedAdress.properties.housenumber == undefined){
          var isConfirmed = confirm('Attention! adresse sans num\351ro, voulez vous continuer ? ');
      }
      if ($scope.customer.address1 !== undefined && $scope.customer.zipCode == undefined){
          alert("Attention! veuillez saisir un code postal.");
          isConfirmed =  false;
      }
      if (isConfirmed){
          CustomersService.saveCustomer($scope.customer, function(response) {
            $location.url("/customers/" + response.content.id + "/form?_action=view");
          });
      }
    }
  }

  $scope.sources = [ "Internet", "Autres" ];

  $scope.getCities = function(value) {
    if ($scope.requete != undefined) {
      $scope.requete.$cancelRequest();
    }
    if (value != "") {
      $scope.requete = InseeService.getCriteria({
        reference : value,
        limit : 55,
      });
      $scope.requete.$promise.then(function(response) {
        $scope.insees = response.content;
      }, function(httpResponse) {
        // console.log("Annulée ou erreur");
      });
    }
  }

  $scope.inseeChanged = function() {
    if ($scope.customer.selectedInsee !== undefined) {
      $scope.customer.zipCode = $scope.customer.selectedInsee.zipCode;
      $scope.customer.city = $scope.customer.selectedInsee.commune;
      $scope.customer.insee = $scope.customer.selectedInsee.insee;
    } else {
      $scope.customer.zipCode = undefined;
      $scope.customer.city = undefined;
      $scope.customer.insee = undefined;
    }
  }

  $scope.countryChanged = function() {
    $scope.customer.country = $scope.customer.selectedCountry.nom;
  }
  
  $scope.$watch(function() {
      return $scope.customer.selectedAdress;
   }, function(){
       //if ($scope.adresse !== undefined && $scope.adresse.length >0){
       if ($scope.customer.selectedAdress !== undefined){
           $scope.customer.address1 = $scope.customer.selectedAdress.properties.name;
           $scope.customer.zipCode = $scope.customer.selectedAdress.properties.postcode;
           $scope.customer.selectedInsee = {
                   zipCode : $scope.customer.selectedAdress.properties.postcode
           }
           $scope.customer.city = $scope.customer.selectedAdress.properties.city;
           $scope.customer.insee = $scope.customer.selectedAdress.properties.citycode;
           $scope.customer.country ='FRANCE';
           $scope.customer.selectedCountry = {
                 nom : 'FRANCE'
            }
       } 
   });

  $scope.removeCustomerParent = function() {
    var isConfirmed = confirm("Etes-vous sur de vouloir supprimer cette liaison parent/enfant ?");
    if (isConfirmed) {
      CustomersService.deleteCustomerParent({
        id : $scope.customer.id,
        searchKey : $scope.customer.searchKey,
        idParent : $scope.customer.idParent
      }, function(response) {
        window.location.reload(true);
      });
    }
  }

  $scope.editIdParent = function() {
    $scope.isEditIdParent = true;
  };

  $scope.isEditIdParent = function() {
    return $scope.isEditIdParent;
  };

  $scope.stopEditIdParent = function() {
    $scope.isEditIdParent = false;
    $scope.customer.idParent = undefined;
    $scope.FoundIdParent = true;
  }

  $scope.isEditIdParent = false;
  $scope.FoundIdParent = true;

  $scope.saveIdParentCustomer = function() {
    $scope.customer.customerParent = {searchKeyParent : $scope.customer.idParent };  
    CustomersService.saveCustomerParent({
      id : $scope.customer.id,
      searchKey : $scope.customer.searchKey,
      searchKeyParent : $scope.customer.idParent
    }, function(response) {
      if (response.content == null) {
        $scope.FoundIdParent = false;
      } else if (response.content.idParent != null) {
        window.location.reload(true);
        $scope.FoundIdParent = true;
        $scope.isEditIdParent = false;
      } else {
        $scope.FoundIdParent = false;
        $scope.customer.customerParent = undefined ;
      }
    });
  }

});

oseModule.filter('phonenumber', function phonenumber() {

  return function(number) {
    if (!number) {
      return '';
    }

    number = String(number);
    var formattedNumber = number;
    var c = (number[0] + number[1] == '00') ? '00' + number[2] + number[3] : '' | (number[0] == '0') ? '0' + number[1] : ''
        | (number[0] == '+') ? '+' + number[1] + number[2] : '';

    var end = number.substring(c.length, number.length);
    var newEnd = '';
    var impaire = false;
    var decoupe = 0;

    if (end.length % 2 == 00) {

      for (var i = 0; i < end.length; i += 2) {
        newEnd += ' ' + end.substring(decoupe, (decoupe + 2));
        decoupe += 2;
      }

    } else {

      for (var i = 0; i < end.length; i += 2) {
        if (!impaire) {
          newEnd += ' ' + end.substring(decoupe, (decoupe + 1));
          decoupe += 1;
          impaire = true;
        } else {
          newEnd += ' ' + end.substring(decoupe, (decoupe + 2));
          decoupe += 2;
        }
      }

    }

    if (newEnd) {
      formattedNumber = (c + newEnd);
    }

    return formattedNumber;
  };
});
