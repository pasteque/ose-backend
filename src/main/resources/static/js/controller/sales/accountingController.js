oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/accounting/list', {
        templateUrl : '/partial/sales/accounting-list.html',
        controller : 'accountingListController'
    });

} ]);

oseModule.controller('accountingListController', function($scope, $location, LocationsService, SalesLocationsService , ToursService, SalesService,CriteriaService) {
    $scope.sortType     = undefined; 
    $scope.sortReverse  = false;

    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
        formatYear : "yyyy",
        formatDay : "dd",
        startingDay : 1
    };

    var dTo = moment().toDate();
    var dFrom = moment().subtract(7, 'days').toDate();
    $scope.saleLocationId  = $location.search()['_pdvId'];
    
    $scope.barLimit = 100;
    
    $scope.increaseLimit = function () {
        $scope.barLimit += 50;
    }

    $scope.resetHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }
    
    $scope.criteria = {
            from : dFrom,
            to : dTo,
            tour : { id : undefined},
            pdv : { id :  undefined}
    }
    
    $scope.sumpayment = 0;
    $scope.deb_search = "";
    
    LocationsService.getDefaultSaleLocation(function(response) {
        $scope.criteria.pdv = response.content;
        if ( $scope.criteria.pdv != undefined) {
            $scope.searchPaiementInfo();
        } else {
            $scope.criteria.pdv = { id : undefined };
            LocationsService.getParentLocations(function(response) {
                $scope.pdvs = response.content;
                $scope.pdvs.unshift({
                    id : undefined,
                    label : "-"
                });
                
            });
        }
        
    });
    
    
    ToursService.getAll({
        minimal : true
    },
            function(response) {
        $scope.tours = response.content;
        $scope.tours.unshift({id : undefined , nom : "-"});
    });
    
    initializeQueryParams();
    
    function initializeQueryParams() {
        if (CriteriaService.getParamsUrl($scope)) {
            $scope.hasQueryParams = true;
            findPaiementInfo();
        }
    }
    
    $scope.searchPaiementInfo = function() {
        $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
        findPaiementInfo();
    }

    function findPaiementInfo() {
        var timeFrom = undefined;
        var timeTo = undefined;
        if($scope.criteria.from != undefined) {
            $scope.criteria.from = $scope.resetHours($scope.criteria.from);
            timeFrom = $scope.criteria.from.getTime()
        }
        if($scope.criteria.to != undefined) {
            $scope.criteria.to = $scope.upgHours($scope.criteria.to);
            timeTo = $scope.criteria.to.getTime()
        }
        
        var sum = 0 ;
        var saleLocationId = null;
        if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
            saleLocationId = $scope.criteria.location.id;
        }
        
        $scope.details = null;

        SalesService.getPaymentsInfo({
            from : timeFrom,
            to : timeTo,
            tourId : $scope.criteria.tour.id,
            locationId : $scope.criteria.pdv.id,
            saleLocationId :saleLocationId,
        }, function(response) {
            $scope.payments = response.content;
            
            for (i=0; i < response.content.length; i++) {            
                sum = sum + (response.content[i]).amount ;  
             };          

            $scope.sumpayment = sum;
        });
    }

    $scope.getDetailsPayment = function(value) {
        var timeFrom = undefined;
        var timeTo = undefined;
        if($scope.criteria.from != undefined) {
            $scope.criteria.from = $scope.resetHours($scope.criteria.from);
            timeFrom = $scope.criteria.from.getTime()
        }
        if($scope.criteria.to != undefined) {
            $scope.criteria.to = $scope.upgHours($scope.criteria.to);
            timeTo = $scope.criteria.to.getTime()
        }
        
        var saleLocationId = null;
        if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
            saleLocationId = $scope.criteria.location.id;
        }
        
        SalesService.getDetailsPayment({
            payment : value,
            from : timeFrom,
            to : timeTo,
            tourId : $scope.criteria.tour.id,
            locationId : $scope.criteria.pdv.id,
            saleLocationId :saleLocationId,
        }, function(response) {
            $scope.payment = value;
            $scope.details = response.content;
        });
    }

    $scope.downloadPaymentsInfoCSV = function() {
        var timeFrom = "";
        var timeTo = "";
        if($scope.criteria.from != undefined) {
            $scope.criteria.from = $scope.resetHours($scope.criteria.from);
            timeFrom = '&from=' + $scope.criteria.from.getTime()
        }
        if($scope.criteria.to != undefined) {
            $scope.criteria.to = $scope.upgHours($scope.criteria.to);
            timeTo = '&to=' + $scope.criteria.to.getTime()
        }
        
        var saleLocationId = "";
        if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
            saleLocationId = '&saleLocationId=' + $scope.criteria.location.id;
        }
        
        var locationId = "";
        if (angular.isDefined($scope.criteria.pdv) && angular.isDefined($scope.criteria.pdv.id)) {
            locationId = '&locationId=' + $scope.criteria.pdv.id;
        }
        
        var tourId = "";
        if (angular.isDefined($scope.criteria.tour) && angular.isDefined($scope.criteria.tour.id)) {
            tourId = '&tourId=' + $scope.criteria.tour.id;
        }
        
        var url = '/sales/getPaymentsInfo?_format=csv' + locationId + timeFrom +  timeTo + saleLocationId + tourId;
        
        window.downloadFile(url);
    }

    $scope.downloadDetailsPaymentCSV = function() {
        
        var timeFrom = "";
        var timeTo = "";
        if($scope.criteria.from != undefined) {
            $scope.criteria.from = $scope.resetHours($scope.criteria.from);
            timeFrom = '&from=' + $scope.criteria.from.getTime()
        }
        if($scope.criteria.to != undefined) {
            $scope.criteria.to = $scope.upgHours($scope.criteria.to);
            timeTo = '&to=' + $scope.criteria.to.getTime()
        }
        
        var saleLocationId = "";
        if (angular.isDefined($scope.criteria.location) && angular.isDefined($scope.criteria.location.id)) {
            saleLocationId = '&saleLocationId=' + $scope.criteria.location.id;
        }
        
        var locationId = "";
        if (angular.isDefined($scope.criteria.pdv) && angular.isDefined($scope.criteria.pdv.id)) {
            locationId = '&locationId=' + $scope.criteria.pdv.id;
        }
        
        var tourId = "";
        if (angular.isDefined($scope.criteria.tour) && angular.isDefined($scope.criteria.tour.id)) {
            tourId = '&tourId=' + $scope.criteria.tour.id;
        }

        
        var url = '/sales/getDetailsPayment/download?_format=csv'+ locationId + timeFrom + timeTo +saleLocationId + tourId + '&payment=' + $scope.payment;
        window.downloadFile(url);
    }
    
    $scope.updateDeballages = function() {
        $scope.getDeballages($scope.deb_search);
    }
    
    $scope.getDeballages = function(value) {
        if ( $scope.criteria.pdv == undefined ) {
            $scope.criteria.pdv = { id : undefined };
        }
        
        if ( value != "" || $scope.criteria.from != undefined 
                || $scope.criteria.to != undefined
                || $scope.criteria.pdv.id != undefined
                || $scope.criteria.tour.id ) {
            if ($scope.requeteDeb != undefined) {
                $scope.requeteDeb.$cancelRequest();         
            }
        $scope.deb_search = value;
        $scope.requete = SalesLocationsService.getCriteria({
            reference : value,
            from : $scope.criteria.from,
            to : $scope.criteria.to,
            tour : $scope.criteria.tour.id,
            location : $scope.criteria.pdv.id
        });
        $scope.requete.$promise.then(function(response) {
            $scope.locations = response.content;
            if ($scope.locations.length == 1) {
                $scope.criteria.location = $scope.locations[0];
            }
        },function(httpResponse){
            //console.log("Annulée ou erreur");
        });
        }

    }

    if($scope.saleLocationId  == undefined ) {
        //console.log("saleLocationId Id indefini");
        LocationsService.getDefaultSaleLocation(function(response) {
            $scope.criteria.pdv = response.content;
            if ( $scope.criteria.pdv != undefined) {
                $scope.searchPaiementInfo();
            } else {
                $scope.criteria.pdv = { id : undefined };
            }
            
        });
    } else {
        //Si on a un déballage passé en paramètre on fait la recherche 
        //console.log("saleLocationId PAS defini");
        $scope.criteria.location = { id: $scope.saleLocationId};
        $scope.singleLocation = false;
        $scope.criteria.from = undefined;
        $scope.criteria.to = undefined;
        $scope.searchPaiementInfo();
        
        SalesLocationsService.get({
            id : $scope.saleLocationId,
        }, function(response) {
            $scope.criteria.location = response.content;
            $scope.criteria.from = new Date($scope.criteria.location.startDate);
            $scope.criteria.to = new Date($scope.criteria.location.endDate);
            $scope.criteria.tour.nom = $scope.criteria.location.tourName;
            $scope.criteria.pdv.label = $scope.criteria.location.locationName;
        });
    }
});
