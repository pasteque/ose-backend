oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/top-x/list', {
        templateUrl : '/partial/sales/top-x-list.html',
        controller : 'topXListController'
    });
    $routeProvider.when('/top-x/:id/form', {
        templateUrl : '/partial/sales/top-x-form.html',
        controller : 'topXFormController'
    });
    $routeProvider.when('/top-x/form', {
        templateUrl : '/partial/sales/top-x-form.html',
        controller : 'topXFormController'
    });

} ]);

oseModule.factory('TariffsService', function($resource) {
    return $resource('/tariff-areas/:param1:id/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getCriteria : {
            method : 'GET',
            params : {
                param1 : 'getCriteria'
            },
            isArray : false,
            cancellable : true
        },
        getAll : {
            method : 'GET',
            params : {
                param1 : 'getAll'
            },
            isArray : false,
            cancellable : false
        }
    });
});

oseModule.controller('topXListController', function($scope, $location, LocationsService, SalesService) {

    $scope.loaded = false;
    LocationsService.getDefaultSaleLocation(function(response) {
        if (response.content != null) {
            $location.url("/top-x/" + response.content.id + "/form");
        } else {
            $scope.loaded = true;
        }
    })

    LocationsService.getParentLocations({
        excludeBase : true},function(response) {
            $scope.salesLocations = response.content;
        });


    $scope.searchSales = function() {
        SalesService.getCriteria({
            reference : $scope.criteria.reference
        }, function(response) {
            $scope.sales = response.content;
        });
    }
});

oseModule.controller('topXFormController', function($scope, $rootScope, $location, $filter, $routeParams, $uibModal, SalesService, LocationsService, ToursService, TariffsService,CriteriaService) {

    $scope.sortType     = undefined; 
    $scope.sortReverse  = false;

    $scope.statusValue = {isOpen: false};
    $scope.statusUnits = {isOpen: false};

    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
            formatYear : "yyyy",
            formatDay : "dd",
            startingDay : 1
    };

    $scope.isLoading = true;


    var dTo = moment().toDate();
    var dFrom = moment().subtract(7, 'days').toDate();

    $scope.xx = [ 5, 10, 15, 20, 25, 50 , 'Total'];

    $scope.resetHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(0);
        mmt.minutes(0);
        mmt.hours(0);
        return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
        var mmt = moment(theDate);
        mmt.seconds(59);
        mmt.minutes(59);
        mmt.hours(23);
        return mmt.toDate();
    }


    
    $scope.hasQueryParams = false;
    initializeQueryParams();
    
    function initializeQueryParams() {
        if (CriteriaService.getParamsUrl($scope)) {
            $scope.hasQueryParams = true;
            findTopX();
        } else {
            $scope.criteria = {
	            locationId : $routeParams.id,
	            tourId : undefined,
	            from : dFrom,
	            to : dTo,
	            x : 10,
	            selectedProducts : [],
                selectedLabel : "",
            };
        }
        
    }
    
    $scope.getTopX = function() {        
        $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
        findTopX()
    }
    
    function findTopX() {
        var pricelistId = $scope.criteria.pricelist ? $scope.criteria.pricelist.id : undefined ;
        
        $scope.X = $scope.criteria.x == 'Total' ? 0 : $scope.criteria.x;

        $scope.criteria = {
                locationId : $scope.criteria.locationId,
                tourId : $scope.criteria.tourId,
                pdv : $scope.criteria.pdv,
                tour : $scope.criteria.tour,
                from : $scope.resetHours($scope.criteria.from),
                to : $scope.upgHours($scope.criteria.to),
                x : $scope.criteria.x,
                pricelist : $scope.criteria.pricelist,
                selectedProducts : $scope.criteria.selectedProducts,
                selectedLabel : $scope.criteria.selectedLabel,
                
        };

        $scope.isLoading = true;

    
        SalesService.getTopX({
            locationId : $scope.criteria.locationId,
            tourId : $scope.criteria.tourId,
            x : $scope.X,
            from : $scope.criteria.from.getTime(),
            to : $scope.criteria.to.getTime(),
            products : angular.toJson($scope.criteria.selectedProducts),
            pricelistId : pricelistId
        }, function(response) {
            $scope.topXUnits = response.content;
            $scope.isLoading = false;
            $scope.totalUnits = 0;
            angular.forEach($scope.topXUnits, function(foo) {
                $scope.totalUnits += foo.units;
            });
            $scope.statusUnits.isOpen = $scope.topXUnits.length < 30 ;
        });

        SalesService.getTopX({
            locationId : $scope.criteria.locationId,
            tourId : $scope.criteria.tourId,
            x : $scope.X,
            dimension : "value",
            from : $scope.criteria.from.getTime(),
            to : $scope.criteria.to.getTime(),
            products : angular.toJson($scope.criteria.selectedProducts),
            pricelistId : pricelistId
        }, function(response) {
            $scope.topXValue = response.content;
            $scope.totalValue = 0;
            angular.forEach($scope.topXValue, function(foo) {
                $scope.totalValue += foo.units;
            });
            $scope.statusValue.isOpen = $scope.topXValue.length < 30 ;
        });
    };


    $scope.downloadUnits = function(format) {
        $scope.criteria = {
                locationId : $scope.criteria.locationId,
                tourId : $scope.criteria.tourId,
                pdv : $scope.criteria.pdv,
                tour : $scope.criteria.tour,
                from : $scope.resetHours($scope.criteria.from),
                to : $scope.upgHours($scope.criteria.to),
                x : $scope.criteria.x,
                pricelist : $scope.criteria.pricelist,
                selectedProducts : $scope.criteria.selectedProducts,
                selectedLabel : $scope.criteria.selectedLabel,
        };
        $scope.X = $scope.criteria.x == 'Total' ? 0 : $scope.criteria.x;
        var pricelistIdConstraint = $scope.criteria.pricelist == undefined ? "" : '&pricelistId=' + $scope.criteria.pricelist.id;
        var locationIdConstraint = $scope.criteria.locationId == undefined ? "" : '&locationId=' + $scope.criteria.locationId;
        var tourIdConstraint =  $scope.criteria.tourId == undefined ? "" : '&tourId=' + $scope.criteria.tourId;
        var url = '/sales/getTopX/download?_format='+format + locationIdConstraint + tourIdConstraint + pricelistIdConstraint +'&x=' + $scope.X + '&from='
        + $scope.criteria.from.getTime() + '&to=' + $scope.criteria.to.getTime() + '&products='
        + encodeURI(angular.toJson($scope.criteria.selectedProducts));

        window.downloadFile(url);
    }

    $scope.downloadValue = function(format) {
        $scope.criteria = {
                locationId : $scope.criteria.locationId,
                tourId : $scope.criteria.tourId,
                pdv : $scope.criteria.pdv,
                tour : $scope.criteria.tour,
                from : $scope.resetHours($scope.criteria.from),
                to : $scope.upgHours($scope.criteria.to),
                x : $scope.criteria.x,
                pricelist : $scope.criteria.pricelist,
                selectedProducts : $scope.criteria.selectedProducts,
                selectedLabel : $scope.criteria.selectedLabel,
        };
        $scope.X = $scope.criteria.x == 'Total' ? 0 : $scope.criteria.x;
        var pricelistIdConstraint = $scope.criteria.pricelist == undefined ? "" : '&pricelistId=' + $scope.criteria.pricelist.id;
        var locationIdConstraint = $scope.criteria.locationId == undefined ? "" : '&locationId=' + $scope.criteria.locationId;
        var tourIdConstraint =  $scope.criteria.tourId == undefined ? "" : '&tourId=' + $scope.criteria.tourId;
        var url = '/sales/getTopX/download?_format='+format+'&dimension=value'+ locationIdConstraint + tourIdConstraint + pricelistIdConstraint +'&x='
        + $scope.X + '&from=' + $scope.criteria.from.getTime() + '&to=' + $scope.criteria.to.getTime() + '&products='
        + encodeURI(angular.toJson($scope.criteria.selectedProducts));

        window.downloadFile(url);
    }

    var modalScope = $rootScope.$new();
    $scope.selectProductsList = function() {
        modalScope.selectedProducts = $scope.criteria.selectedProducts;
        modalScope.categories = true;
        modalScope.links = true;
        modalScope.modalInstance = $uibModal.open({
            templateUrl : '/partial/sales/select-product-form.html',
            controller : 'selectProductsController',
            scope : modalScope,
        });

        modalScope.modalInstance.result.then(function(data) {
            $scope.criteria.selectedProducts = data;
            $scope.criteria.selectedLabel = "";
            for (var i = 0; i < $scope.criteria.selectedProducts.length; i++) {
                $scope.criteria.selectedLabel += $scope.criteria.selectedProducts[i].id;
                if ($scope.criteria.selectedProducts.length > 1 && i < $scope.criteria.selectedProducts.length - 1) {
                    $scope.criteria.selectedLabel += ", "
                }
            }
        });
    }
    $scope.clearProductsList = function() {
        $scope.criteria.selectedProducts = [];
    }

    $scope.updatePdv = function() {
        $scope.criteria.locationId = $scope.criteria.pdv == undefined ? undefined : $scope.criteria.pdv.id == undefined ? $scope.criteria.pdv.category : $scope.criteria.pdv.id;
    }

    $scope.updateTour = function() {
        $scope.criteria.tourId = $scope.criteria.tour == undefined ? undefined : $scope.criteria.tour.id;
    }

    TariffsService.getAll({
        indexFormat : true
    },
    function(response) {
        $scope.pricelists = response.content;
    });

    LocationsService.getDefaultSaleLocation(function(response) {
        if (response.content != null) {
            $scope.criteria.locationId = response.content.id;
            $scope.criteria.pdv = response;
            $scope.getTopX();
        } else {
            $scope.getTopX();
            ToursService.getAll({
                minimal : true
            },
            function(response) {
                $scope.tours = response.content;
            });

            LocationsService.getParentLocations(function(response) {
                $scope.pdvs = response.content;
                var arr = [];
                for(var i = 0; i < $scope.pdvs.length; i++) {
                    if(!arr.includes($scope.pdvs[i].category)) {
                        arr.push($scope.pdvs[i].category);
                    }
                }
                for(var i = 0; i < arr.length; i++) {
                    $scope.pdvs.unshift({
                        id : undefined,
                        label : arr[i],
                        category : arr[i]
                    });
                }

            });
        }
    })

});

oseModule.controller('selectProductsController', function($scope, $location, $filter, $routeParams, ProductsService, CategoriesService) {
    
    if($scope.selectedProducts === undefined) {
    
        $scope.selectedProducts = [];
    
    }

    $scope.getProducts = function(value) {
        if ($scope.requete != undefined) {
            $scope.requete.$cancelRequest(); 		
        }
        if (value != "") {

            $scope.requete = ProductsService.getCriteria({
                reference : value,
                limit : 15,
                composition : $scope.composition
            });

            $scope.requete.$promise.then(function success(response){
                $scope.products = response.content;
                if ($scope.products.length == 1) {
                    $scope.product = $scope.products[0];
                }
                if($scope.categories){
                    CategoriesService.getCriteria({
                        reference : value,
                        limit : 15
                    }, function(response) {
                        if(response.content && response.content.length >0) {
                            $scope.products = response.content.concat($scope.products);
                        }
                    });
                }
            },function(httpResponse){
                //console.log("Annulée ou erreur");
            });

        }
    }

    $scope.addProduct = function() {
        if (angular.isDefined($scope.product) && notAlreadySelected()) {
            $scope.selectedProducts.push($scope.product);
            if($scope.links) {
                var value = $scope.product.id;
                ProductsService.getLinks({
                    id : value,
                }, function(response) {
                    if(response.content && response.content.length >0) {
                        for (var i = 0; i < response.content.length; i++) {
                            $scope.product = response.content[i];
                            if (angular.isDefined($scope.product) && notAlreadySelected()) {
                                $scope.selectedProducts.push($scope.product);
                            }
                        }
                    }
                });
            }
            $scope.product = undefined;
        }
    }

    $scope.removeProduct = function(value) {
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].id == value) {
                break;
            }
        }
        $scope.selectedProducts.splice(i, 1);
    }

    $scope.closeModal = function() {
        $scope.modalInstance.close($scope.selectedProducts);
    }

    function notAlreadySelected() {
        for (var i = 0; i < $scope.selectedProducts.length; i++) {
            if ($scope.selectedProducts[i].label == $scope.product.label) {
                return false;
            }
        }
        return true;
    }

});


oseModule.controller('selectLocationsController', function selectLocationsController($scope, $location, $filter, $routeParams, LocationsService) {

    LocationsService.getDefaultSaleLocation(function(response) {
        $scope.location = response.content;
        if($scope.location != undefined) {
            $scope.location = { id : response.content.id,
                    label : response.content.label};

            $scope.locations = [$scope.location];
            if(!$scope.parentOnly) {
                LocationsService.getSubLocations({
                    locationId : $scope.location.id,
                }, function(response) {
                    $scope.locations = $scope.locations.concat(response.content);
                });
            }
        } else {
            if(!$scope.parentOnly) {
                LocationsService.getAllInfo(function(response) {
                    $scope.locations = response.content;
                    var arr = [];
                    for(var i = 0; i < $scope.locations.length; i++) {
                        if(!arr.includes($scope.locations[i].category)) {
                            arr.push($scope.locations[i].category);
                        }
                    }
                    for(var i = 0; i < arr.length; i++) {
                        $scope.locations.unshift({
                            id : undefined,
                            label : arr[i],
                            category : arr[i]
                        });
                    }
                });
            } else {
                LocationsService.getParentLocations(function(response) {
                    $scope.locations = response.content;
                    var arr = [];
                    for(var i = 0; i < $scope.locations.length; i++) {
                        if(!arr.includes($scope.locations[i].category)) {
                            arr.push($scope.locations[i].category);
                        }
                    }
                    for(var i = 0; i < arr.length; i++) {
                        $scope.locations.unshift({
                            id : undefined,
                            label : arr[i],
                            category : arr[i]
                        });
                    }
                });
            }
        }

    });

    $scope.addLocation = function() {
        if (angular.isDefined($scope.location) && notAlreadySelected()) {
            $scope.selectedLocations.push($scope.location);
            $scope.location = undefined;
        }
    }

    $scope.removeLocation = function(value) {
        for (var i = 0; i < $scope.selectedLocations.length; i++) {
            if ($scope.selectedLocations[i].id == value) {
                break;
            }
        }
        $scope.selectedLocations.splice(i, 1);
    }

    $scope.closeModal = function() {
        $scope.modalInstance.close($scope.selectedLocations);
    }

    function notAlreadySelected() {
        for (var i = 0; i < $scope.selectedLocations.length; i++) {
            if ($scope.selectedLocations[i].label == $scope.location.label) {
                return false;
            }
        }
        return true;
    }

});

oseModule.controller('selectTariffsController', function($scope, $location, $filter, $routeParams, TariffsService) {
    
    TariffsService.getAll({
        indexFormat : true
    },
    function(response) {
        $scope.tariffs = response.content;
        if ($scope.tariffs.length > 0) {
            $scope.tariff = $scope.tariffs[0];
        }
    });

    $scope.addTariff = function() {
        if (angular.isDefined($scope.tariff) && notAlreadySelected()) {
            $scope.selectedTariffs.push($scope.tariff);
            $scope.tariff = undefined;
        }
    }

    $scope.removeTariff = function(value) {
        for (var i = 0; i < $scope.selectedTariffs.length; i++) {
            if ($scope.selectedTariffs[i].id == value) {
                break;
            }
        }
        $scope.selectedTariffs.splice(i, 1);
    }

    $scope.closeModal = function() {
        $scope.modalInstance.close($scope.selectedTariffs);
    }

    function notAlreadySelected() {
        for (var i = 0; i < $scope.selectedTariffs.length; i++) {
            if ($scope.selectedTariffs[i].id == $scope.tariff.id) {
                return false;
            }
        }
        return true;
    }

});
