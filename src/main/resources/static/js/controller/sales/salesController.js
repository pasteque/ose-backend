oseModule.config([ '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider.when('/sales/list', {
        templateUrl : '/partial/sales/sales-list.html',
        controller : 'salesListController'
    });
    $routeProvider.when('/sales/:id/form', {
        templateUrl : '/partial/sales/sales-form.html',
        controller : 'salesFormController'
    });

} ]);

oseModule.factory('SalesService', function($resource) {
    return $resource('/sales/:param1/:value1', {
        param1 : '@param1',
        value1 : '@value1'
    }, {
        getDetails : {
            method : 'GET',
            params : {
                param1 : 'getDetails'
            },
            isArray : false,
            cancellable : true
        },
        getDetailsOverview : {
            method : 'GET',
            params : {
                param1 : 'getDetailsOverview'
            },
            isArray : false,
            cancellable : true
        },
        getTopX : {
            method : 'GET',
            params : {
                param1 : 'getTopX'
            },
            isArray : false,
            cancellable : true
        },
        getPaymentsInfo :{
            method : 'GET',
            params : {
                param1 : 'getPaymentsInfo'
            },
            isArray : false,
            cancellable : true
        },
        getDetailsPayment :{
            method : 'GET',
            params : {
                param1 : 'getDetailsPayment'
            },
            isArray : false,
            cancellable : true
        }
    });
});

oseModule.controller('salesListController', function salesListController($scope, $location, LocationsService, SalesService) {

    $scope.sortType = 'locationLabel';
    $scope.sortReverse = false;
	
	$scope.TotalOverView = {
		    locationId : "Total",
		    locationLabel : "" ,
		    dimension : "date" ,
		    turnoverN0 : 0 ,
		    salesCountN0 : 0,
		    averageBasketN0 : 0,
		    turnoverN1 : 0,
		    salesCountN1 : 0,
		    averageBasketN1 : 0
	};
	
    $scope.loaded = false;
    LocationsService.getDefaultSaleLocation(function(response) {
        if (response.content != null) {
            $location.url("/sales/" + response.content.id + "/form");
        } else {
            $scope.loaded = true;
        }
    })
    
        LocationsService.getParentLocations({
        excludeBase : true},function(response) {
        	$scope.salesLocations = []

        for (i=0; i < response.content.length; i++) {    
        	$scope.searchSalesOverView((response.content[i]).id);
         };  
         
    });
    

    $scope.searchSalesOverView = function( locationId) {
        SalesService.getDetailsOverview({
            locationId : locationId,
            dimension : 'date'
        }, function(response) {
            $scope.salesLocations.push(response.content) ;
        	
            $scope.TotalOverView = {
        		    locationId : "Total",
        		    locationLabel : "" ,
        		    dimension : "date" ,
        		    turnoverN0 : 0 ,
        		    salesCountN0 : 0,
        		    averageBasketN0 : 0,
        		    turnoverN1 : 0,
        		    salesCountN1 : 0,
        		    averageBasketN1 : 0
        	};
        	
            for (i=0; i < $scope.salesLocations.length; i++) {    
            	$scope.TotalOverView.turnoverN0 = $scope.TotalOverView.turnoverN0 + ($scope.salesLocations[i]).turnoverN0 ; 
            	$scope.TotalOverView.turnoverN1 = $scope.TotalOverView.turnoverN1 + ($scope.salesLocations[i]).turnoverN1 ;
            	$scope.TotalOverView.salesCountN0 = $scope.TotalOverView.salesCountN0 + ($scope.salesLocations[i]).salesCountN0 ;
            	$scope.TotalOverView.salesCountN1 = $scope.TotalOverView.salesCountN1 + ($scope.salesLocations[i]).salesCountN1 ;
              };  
              
              if($scope.TotalOverView.salesCountN1 != 0) {
            	  $scope.TotalOverView.averageBasketN1 = $scope.TotalOverView.turnoverN1 / $scope.TotalOverView.salesCountN1 ;
              }
              
              if($scope.TotalOverView.salesCountN0 != 0) {
            	  $scope.TotalOverView.averageBasketN0 = $scope.TotalOverView.turnoverN0 / $scope.TotalOverView.salesCountN0 ;
              }
        });
    }
});

oseModule.controller('salesFormController', function salesFormController($scope, $location, $filter, $routeParams, SalesService, CashesService, CashRegisterService,CriteriaService) {
	$scope.sortType     = undefined; 
	$scope.sortReverse  = false;
	
    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedFrom = true;
    };

    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.openedTo = true;
    };

    $scope.dateOptions = {
        formatYear : "yyyy",
        formatDay : "dd",
        startingDay : 1
    };

    $scope.isLoading = true ;
    $scope.locationId = $routeParams.id ;
    
    $scope.totalturnover = 0 ;
    $scope.totalsalescount = 0 ;
    
    var dTo = moment().toDate();
    var dFrom = moment().subtract(7, 'days').toDate();
    
    $scope.resetHours = function(theDate) {
    	var mmt = moment(theDate);
    	mmt.seconds(0);
    	mmt.minutes(0);
    	mmt.hours(0);
    	return mmt.toDate();
    }

    $scope.upgHours = function(theDate) {
    	var mmt = moment(theDate);
    	mmt.seconds(59);
    	mmt.minutes(59);
    	mmt.hours(23);
    	return mmt.toDate();
    }
   

    $scope.criteria = {
        from : dFrom,
        to : dTo,
    };
    
    initializeQueryParams();
    
    function initializeQueryParams() {
        if (CriteriaService.getParamsUrl($scope)) {
            $scope.hasQueryParams = true;
            findSalesDetails();
        }
    }
    $scope.getSalesDetails = function() {
        $location.search('criteria',  btoa(JSON.stringify($scope.criteria)));
        findSalesDetails();
    }

    function findSalesDetails(){
    	var sum = 0;
    	var sumsales = 0;
    	$scope.criteria = {
    			from : $scope.resetHours($scope.criteria.from),
    			to : $scope.upgHours($scope.criteria.to),
    	};
    	
    	$scope.isLoading = true ;
    	
        SalesService.getDetails({
            locationId : $routeParams.id,
            dimension : 'deballage',
            from : $scope.criteria.from.getTime(),
            to : $scope.criteria.to.getTime(),
        }, function(response) {
            $scope.sales = response.content;
            
            for (i=0; i < response.content.length; i++) {            
            	sum = sum + (response.content[i]).turnover ; 
            	sumsales = sumsales + (response.content[i]).salesCount ; 
             };          

            $scope.totalturnover = sum;
            $scope.totalsalescount = sumsales;
            
            $scope.dataHistoCA = initChart(response.content , true);
            $scope.isLoading = false ;
            
            //Gestion des Z
            CashRegisterService.getByCriteria({
        		location : $routeParams.id,
        	}, function(response) {
        		$scope.cashRegisters = response.content;
        		$scope.cashSessions = [] ;
                for (i=0; $scope.cashRegisters && i < $scope.cashRegisters.length; i++) {            
                	CashesService.search({
                		cashRegisterId : $scope.cashRegisters[i].id,
                		dateStart : $scope.resetHours($scope.criteria.from).getTime()-86400000,
                		dateStop : $scope.upgHours($scope.criteria.to).getTime()+86400000
                	}, function(response) {
                		$scope.cashSessions.push(response.content);
                		//je cherche à quelle ligne on l'ajoute
                		for(j=0 ; j<response.content.length ; j++){
                			$scope.pushZtickets(response.content[j]);
                		}
                	}) ;
                 };
        	});
        });

        SalesService.getDetails({
            locationId : $routeParams.id,
            dimension : 'zipcode',
            from : $scope.criteria.from.getTime(),
            to : $scope.criteria.to.getTime(),
        }, function(response) {
            $scope.dataZipcodeCA = initChart(response.content , false);
        });
    };
    $scope.getSalesDetails();

    $scope.dataHistoCA = [ [ [] ] ];
    $scope.dataZipcodeCA = [ [ [] ] ];
    
    
    $scope.getZtickets = function(dimension) {
		var dimensionDate = dimension.split(' ');
		var parts = dimensionDate[0].split('/');
		var retour = [];
		var tmpvalue = $scope.resetHours(Date.parse(parts[2]+"-"+parts[1]+"-"+parts[0])) ;
		if(dimensionDate.length > 1) {
			var city = dimension.substring(dimensionDate[0].length);
			for(i = 0 ; $scope.cashSessions && i< $scope.cashSessions.length ; i++){
				if($scope.cashSessions[i].salesLocation.city == city && 
						(($scope.cashSessions[i].openDate > tmpvalue && $scope.cashSessions[i].openDate < max)
								|| ($scope.cashSessions[i].closeDate > tmpvalue && $scope.cashSessions[i].closeDate < max))){
					retour.push($scope.cashSessions[i].id);
				}
			}
		} else {
			var max = $scope.upgHours(Date.parse(parts[2]+"-"+parts[1]+"-"+parts[0]));
			for(i = 0 ; $scope.cashSessions && i< $scope.cashSessions.length ; i++){
				if(($scope.cashSessions[i].openDate > tmpvalue && $scope.cashSessions[i].openDate < max)
						|| ($scope.cashSessions[i].closeDate > tmpvalue && $scope.cashSessions[i].closeDate < max)){
					retour.push($scope.cashSessions[i].id);
				}
			}
		}
		
		return retour;
    };
    
    $scope.pushZtickets = function(cashSession) {
    	for(i = 0 ; $scope.sales && i< $scope.sales.length ; i++){
    		
	    	var dimensionDate = $scope.sales[i].dimension.split(' ');
			var parts = dimensionDate[0].split('/');
			
			var tmpvalue = $scope.resetHours(Date.parse(parts[2]+"-"+parts[1]+"-"+parts[0])).getTime() ;
			var max = $scope.upgHours(Date.parse(parts[2]+"-"+parts[1]+"-"+parts[0])).getTime();
			if( ! $scope.sales[i].sessions){
				$scope.sales[i].sessions = [];
			}
			if(dimensionDate.length > 1) {
				var city = $scope.sales[i].dimension.substring(dimensionDate[0].length+1);
					if(cashSession.salesLocation.city == city && 
							((cashSession.openDate > tmpvalue && cashSession.openDate < max)
									|| (cashSession.closeDate > tmpvalue && cashSession.closeDate < max))){
						$scope.sales[i].sessions.push(cashSession.id);
					}
				
			} else {
				
					if((cashSession.openDate > tmpvalue && cashSession.openDate < max)
							|| (cashSession.closeDate > tmpvalue && cashSession.closeDate < max)){
						$scope.sales[i].sessions.push(cashSession.id);
					}
				
			}
    	}
    };

    $scope.downloadCSV = function() {
        var url = '/sales/getDetails/download?_format=csv&dimension=deballage&locationId=' + $routeParams.id + '&from='
                + $scope.criteria.from.getTime() + '&to=' + $scope.criteria.to.getTime();
        window.downloadFile(url);
    }

    $scope.chartOptionsHistoCA = {
        title : 'Evolution du chiffre d\'affaires sur la p&eacute;riode',
        seriesDefaults : {
            renderer : jQuery.jqplot.BarRenderer,
            rendererOptions : {
                barWidth : 30,
            },
        },
        axes : {
            xaxis : {
                renderer : jQuery.jqplot.DateAxisRenderer,
                rendererOptions: {
                    tickInset: 0.5
                },
                tickOptions : {
                    angle : -45,
                    fontSize : '10pt',
                    formatString:'%d/%m/%Y'
                }
            },
            yaxis : {
                tickOptions : {
                    formatString : "%'d &euro;"
                }
            },
        },
        highlighter : {
            show : true,
            tooltipContentEditor: function (str, seriesIndex, pointIndex, plot) {

                var date = plot.data[seriesIndex][pointIndex][0];
                var parts = date.split('/');
                var ca = parseFloat(plot.data[seriesIndex][pointIndex][1]).toFixed(2);

                var html = "<div>";
                html += parts[1]+"/"+parts[0]+"/"+parts[2];
                html += "  - C.A. : ";
                html += ca;
                html += "  &euro;</div>";

                return html;
            }
        },
    };
    $scope.chartOptionsZipcodeCA = {
        title : 'R&eacute;partition du chiffre d\'affaires par ville',
        seriesDefaults : {
            renderer : jQuery.jqplot.PieRenderer,
            rendererOptions : {
                showDataLabels : true
            }
        },
        highlighter : {
            show : true,
            formatString : "C.A. : %s, C.A. : %'d &euro;",
            useAxesFormatters : false,
        },
        legend : {
            show : true
        }
    };

});

function initChart(data , dateDimension) {
    if (data.length > 0) {
        var serie = [];
        //EDU : ordre chronologique gauche -> droite
        //Dimension -> date
        if(dateDimension) {
        	var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
        	var day = 7 ;
        	var toPush = false;
        	var turnOver = 0 ;
        	var value = undefined ;
        	var tmpvalue = undefined ;
        	for (var i = data.length; i-- ; day++) {
        		var sale = data[i];
        		var dimensionDate = sale.dimension.split(' ');
        		var parts = dimensionDate[0].split('/');
        		tmpvalue = parts[1]+"/"+parts[0]+"/"+parts[2] ;
        		
        		if(value == undefined) {
        			value = tmpvalue ;
        			turnOver = 0 ;
        		} else {
        			toPush =  ( value != tmpvalue) ;
        		}
        		
        		if(toPush) {
        			serie.push([ value,  turnOver]);
        			value = tmpvalue ;
        			turnOver = sale.turnover ;
        		} else {
        			turnOver += sale.turnover ;
        		}
        	}
        	if(value != undefined) {
        		serie.push([ value,  turnOver]);
        	}
        	
        } else {
        	for (var i = 0 ; i< data.length; i++ ) {
        		var sale = data[i];
        		serie.push([ sale.dimension, sale.turnover ]);
        	}
        }
        return [ serie ];
    } else {
        return undefined;// [ [ [] ] ]
    }
}
