
CREATE DATABASE IF NOT EXISTS `pastequeserver-v8S` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */; USE `pastequeserver-v8S`; -- MySQL dump 10.13 Distrib 8.0.11, for Win64 (x86_64) -- -- Host: 127.0.0.1 Database: challans-20200122 -- ------------------------------------------------------ -- Server version 8.0.11
 
 /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */; /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */; /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 
  SET NAMES utf8 ;
 
 /*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */; /*!40103 SET TIME_ZONE='+00:00' */; /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */; /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */; /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */; /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
 
 -- -- Table structure for table `applications` --
 
 DROP TABLE IF EXISTS `applications`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `applications` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `VERSION` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attribute` --
 
 DROP TABLE IF EXISTS `attribute`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attribute` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attributeinstance` --
 
 DROP TABLE IF EXISTS `attributeinstance`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attributeinstance` (
 
   `ID` varchar(255) NOT NULL,
   `ATTRIBUTESETINSTANCE_ID` varchar(255) NOT NULL,
   `ATTRIBUTE_ID` varchar(255) NOT NULL,
   `VALUE` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `ATTINST_SET` (`ATTRIBUTESETINSTANCE_ID`),
   KEY `ATTINST_ATT` (`ATTRIBUTE_ID`),
   CONSTRAINT `ATTINST_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`id`),
   CONSTRAINT `ATTINST_SET` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attributeset` --
 
 DROP TABLE IF EXISTS `attributeset`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attributeset` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attributesetinstance` --
 
 DROP TABLE IF EXISTS `attributesetinstance`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attributesetinstance` (
 
   `ID` varchar(255) NOT NULL,
   `ATTRIBUTESET_ID` varchar(255) NOT NULL,
   `DESCRIPTION` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `ATTSETINST_SET` (`ATTRIBUTESET_ID`),
   CONSTRAINT `ATTSETINST_SET` FOREIGN KEY (`ATTRIBUTESET_ID`) REFERENCES `attributeset` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attributeuse` --
 
 DROP TABLE IF EXISTS `attributeuse`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attributeuse` (
 
   `ID` varchar(255) NOT NULL,
   `ATTRIBUTESET_ID` varchar(255) NOT NULL,
   `ATTRIBUTE_ID` varchar(255) NOT NULL,
   `LINENO` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `ATTUSE_LINE` (`ATTRIBUTESET_ID`,`LINENO`),
   KEY `ATTUSE_ATT` (`ATTRIBUTE_ID`),
   CONSTRAINT `ATTUSE_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`id`),
   CONSTRAINT `ATTUSE_SET` FOREIGN KEY (`ATTRIBUTESET_ID`) REFERENCES `attributeset` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `attributevalue` --
 
 DROP TABLE IF EXISTS `attributevalue`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `attributevalue` (
 
   `ID` varchar(255) NOT NULL,
   `ATTRIBUTE_ID` varchar(255) NOT NULL,
   `VALUE` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `ATTVAL_ATT` (`ATTRIBUTE_ID`),
   CONSTRAINT `ATTVAL_ATT` FOREIGN KEY (`ATTRIBUTE_ID`) REFERENCES `attribute` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `cashregisters` --
 
 DROP TABLE IF EXISTS `cashregisters`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `cashregisters` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `NAME` varchar(255) NOT NULL,
   `LOCATION_ID` varchar(255) NOT NULL,
   `NEXTTICKETID` bigint(20) NOT NULL DEFAULT '1',

   `nextSessionId` int(11) NOT NULL DEFAULT '1',

   PRIMARY KEY (`ID`),
   KEY `CASHREGISTER_FK_LOCATION` (`LOCATION_ID`),
   CONSTRAINT `CASHREGISTER_FK_LOCATION` FOREIGN KEY (`LOCATION_ID`) REFERENCES `locations` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `categories` --
 
 DROP TABLE IF EXISTS `categories`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `categories` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `PARENTID` varchar(255) DEFAULT NULL,
   `IMAGE` mediumblob,
   `DISPORDER` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `CATEGORIES_NAME_INX` (`NAME`),
   KEY `CATEGORIES_FK_1` (`PARENTID`),
   CONSTRAINT `CATEGORIES_FK_1` FOREIGN KEY (`PARENTID`) REFERENCES `categories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `closedcash` --
 
 DROP TABLE IF EXISTS `closedcash`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `closedcash` (
 
   `MONEY` varchar(255) NOT NULL,
   `CASHREGISTER_ID` int(11) NOT NULL,
   `HOSTSEQUENCE` int(11) NOT NULL,
   `DATESTART` datetime DEFAULT NULL,
   `DATEEND` datetime DEFAULT NULL,
   `OPENCASH` double DEFAULT NULL,
   `CLOSECASH` double DEFAULT NULL,
   `EXPECTEDCASH` double DEFAULT NULL,
   `DEBALLAGE_ID` int(11) DEFAULT NULL,

 

   `continuous` tinyint(1) NOT NULL,
   `ticketCount` int(11) DEFAULT NULL,
   `custCount` int(11) DEFAULT NULL,
   `cs` double DEFAULT NULL,
   `csPeriod` double NOT NULL,
   `csFYear` double NOT NULL,
   `csPerpetual` double NOT NULL,

 

   PRIMARY KEY (`MONEY`),
   UNIQUE KEY `CLOSEDCASH_INX_SEQ` (`CASHREGISTER_ID`,`HOSTSEQUENCE`),
   KEY `CLOSEDCASH_INX_1` (`DATESTART`),
   KEY `fk_closedcash_DEBALLAGES1_idx` (`DEBALLAGE_ID`),
   KEY `CLOSEDCASH_CRDAT` (`CASHREGISTER_ID`,`DATESTART`,`DATEEND`),
   CONSTRAINT `CLOSEDCASH_FK_CASHREGISTER` FOREIGN KEY (`CASHREGISTER_ID`) REFERENCES `cashregisters` (`id`),
   CONSTRAINT `fk_closedcash_DEBALLAGES1` FOREIGN KEY (`DEBALLAGE_ID`) REFERENCES `deballages` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `currencies` --
 
 DROP TABLE IF EXISTS `currencies`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `currencies` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `NAME` varchar(255) NOT NULL,
   `SYMBOL` varchar(10) DEFAULT NULL,
   `DECIMALSEP` varchar(1) DEFAULT NULL,
   `THOUSANDSSEP` varchar(1) DEFAULT NULL,
   `RATE` double NOT NULL DEFAULT '1',
   `FORMAT` varchar(20) NOT NULL DEFAULT '#0.00 $',
   `MAIN` bit(1) NOT NULL,
   `ACTIVE` bit(1) NOT NULL DEFAULT b'1',
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `customers` --
 
 DROP TABLE IF EXISTS `customers`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `customers` (
 
   `ID` varchar(255) NOT NULL,
   `LOCATION_ID` varchar(255) DEFAULT NULL,
   `SEARCHKEY` varchar(255) NOT NULL,
   `TAXID` varchar(255) DEFAULT NULL,
   `NAME` varchar(255) NOT NULL,
   `TAXCATEGORY` varchar(255) DEFAULT NULL,
   `DISCOUNTPROFILE_ID` int(11) DEFAULT NULL,
   `CARD` varchar(255) DEFAULT NULL,
   `MAXDEBT` double NOT NULL DEFAULT '0',
   `ADDRESS` varchar(255) DEFAULT NULL,
   `ADDRESS2` varchar(255) DEFAULT NULL,
   `POSTAL` varchar(255) DEFAULT NULL,
   `INSEE` varchar(255) DEFAULT NULL,
   `CITY` varchar(255) DEFAULT NULL,
   `REGION` varchar(255) DEFAULT NULL,
   `COUNTRY` varchar(255) DEFAULT NULL,
   `FIRSTNAME` varchar(255) DEFAULT NULL,
   `LASTNAME` varchar(255) DEFAULT NULL,
   `EMAIL` varchar(255) DEFAULT NULL,
   `PHONE` varchar(255) DEFAULT NULL,
   `PHONE2` varchar(255) DEFAULT NULL,
   `FAX` varchar(255) DEFAULT NULL,
   `NOTES` varchar(255) DEFAULT NULL,
   `VISIBLE` bit(1) NOT NULL DEFAULT b'1',
   `CURDATE` datetime DEFAULT NULL,
   `CURDEBT` double DEFAULT NULL,
   `PREPAID` double NOT NULL DEFAULT '0',
   `DATEOFBIRTH` date DEFAULT NULL,
   `CREATION_DATE` datetime DEFAULT NULL,
   `LAST_UPDATE` datetime DEFAULT NULL,
   `TYPE` varchar(45) DEFAULT NULL COMMENT 'Type d''entité particulier / pro par exempl',
   `SOURCE` varchar(45) DEFAULT NULL,
   `NEWSLETTER` bit(1) DEFAULT b'0' COMMENT 'Sollicitations par newsletter?',
   `PARTENAIRES` bit(1) DEFAULT b'0' COMMENT 'Sollicitations par nos partenaires',
   `CIVILITE` varchar(45) DEFAULT NULL,
   `ID_PARENT` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `CUSTOMERS_SKEY_INX` (`SEARCHKEY`),
   KEY `CUSTOMERS_TAXCAT` (`TAXCATEGORY`),
   KEY `CUSTOMERS_DISCOUNTPROFILE` (`DISCOUNTPROFILE_ID`),
   KEY `CUSTOMERS_TAXID_INX` (`TAXID`),
   KEY `CUSTOMERS_NAME_INX` (`NAME`),
   KEY `CUSTOMERS_CARD_INX` (`CARD`),
   KEY `CUSTOMERS_FIRSTNAME` (`FIRSTNAME`),
   KEY `CUSTOMERS_LASTNAME` (`LASTNAME`),
   KEY `CUSTOMERS_ID_PARENT` (`ID_PARENT`),
   KEY `customers_locations` (`LOCATION_ID`),
   KEY `FK_CUSTOMERS_INSEE` (`INSEE`),
   CONSTRAINT `CUSTOMERS_DISCOUNTPROFILE` FOREIGN KEY (`DISCOUNTPROFILE_ID`) REFERENCES `discountprofiles` (`id`),
   CONSTRAINT `CUSTOMERS_TAXCAT` FOREIGN KEY (`TAXCATEGORY`) REFERENCES `taxcustcategories` (`id`),
   CONSTRAINT `FK_CUSTOMERS_INSEE` FOREIGN KEY (`INSEE`) REFERENCES `insee` (`id`),
   CONSTRAINT `customers_locations` FOREIGN KEY (`LOCATION_ID`) REFERENCES `locations` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `deballages` --
 
 DROP TABLE IF EXISTS `deballages`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `deballages` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `TOURNEES_ID` int(11) NOT NULL,
   `NUM_INSEE` varchar(64) DEFAULT NULL,
   `COMMUNE` varchar(255) DEFAULT NULL,
   `DEBUT` datetime DEFAULT NULL,
   `FIN` datetime DEFAULT NULL,
   `NB_PUB` int(11) DEFAULT NULL,
   `DROIT_PLACE` double DEFAULT NULL,
   `PAYE` bit(1) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_DEBALLAGES_TOURNEES1_idx` (`TOURNEES_ID`),
   KEY `fk_DEBALLAGES_INSEE1_idx` (`NUM_INSEE`),
   KEY `DATEDEB_INDEX` (`DEBUT`),
   KEY `DATEFIN_INDEX` (`FIN`),
   CONSTRAINT `fk_DEBALLAGES_INSEE1` FOREIGN KEY (`NUM_INSEE`) REFERENCES `insee` (`id`),
   CONSTRAINT `fk_DEBALLAGES_TOURNEES1` FOREIGN KEY (`TOURNEES_ID`) REFERENCES `tournees` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `discountprofiles` --
 
 DROP TABLE IF EXISTS `discountprofiles`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `discountprofiles` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `NAME` varchar(255) NOT NULL,
   `RATE` double NOT NULL,
   `DISPORDER` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `floors` --
 
 DROP TABLE IF EXISTS `floors`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `floors` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `IMAGE` mediumblob,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `FLOORS_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `insee` --
 
 DROP TABLE IF EXISTS `insee`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `insee` (
 
   `ID` varchar(255) NOT NULL,
   `NUM_INSEE` varchar(64) DEFAULT NULL,
   `ZIPCODE` varchar(45) DEFAULT NULL,
   `COMMUNE` varchar(255) DEFAULT NULL,
   `LATITUDE` double DEFAULT NULL,
   `LONGITUDE` double DEFAULT NULL,
   `LASTUPDATE` datetime DEFAULT NULL,
   `NOMREGION` varchar(255) DEFAULT NULL,
   `CODEREGION` varchar(255) DEFAULT NULL,
   `NOMDEPARTEMENT` varchar(255) DEFAULT NULL,
   `CODEDEPARTEMENT` varchar(255) DEFAULT NULL,
   `SURFACE` double DEFAULT NULL,
   `POPULATION` int(11) DEFAULT NULL,
   `ETAT` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `NUMINSEE` (`NUM_INSEE`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Liste des Communes FR'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `inventory` --
 
 DROP TABLE IF EXISTS `inventory`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `inventory` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) DEFAULT NULL,
   `DSTART` datetime DEFAULT NULL,
   `DEND` datetime DEFAULT NULL,
   `USER` varchar(255) DEFAULT NULL,
   `STATUS` varchar(1) DEFAULT NULL,
   `LOCATION` varchar(255) DEFAULT NULL,
   `NOTE` longtext,
   `CTYPE` varchar(64) DEFAULT 'COMPLET',
   PRIMARY KEY (`ID`),
   KEY `fk_INVENTORY_locations1_idx` (`LOCATION`),
   KEY `fk_INVENTORY_people1_idx` (`USER`),
   CONSTRAINT `fk_INVENTORY_locations1` FOREIGN KEY (`LOCATION`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_INVENTORY_people1` FOREIGN KEY (`USER`) REFERENCES `people` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `inventory_details` --
 
 DROP TABLE IF EXISTS `inventory_details`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `inventory_details` (
 
   `ID` varchar(255) NOT NULL,
   `INVENTORY` varchar(255) DEFAULT NULL,
   `USER` varchar(255) DEFAULT NULL,
   `DATE` datetime DEFAULT NULL,
   `PRODUCT` varchar(255) DEFAULT NULL,
   `QUANTITY` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_INVENTORY_DETAILS_products1_idx` (`PRODUCT`),
   KEY `fk_INVENTORY_DETAILS_INVENTORY1_idx` (`INVENTORY`),
   KEY `fk_INVENTORY_DETAILS_people1_idx` (`USER`),
   CONSTRAINT `fk_INVENTORY_DETAILS_INVENTORY1` FOREIGN KEY (`INVENTORY`) REFERENCES `inventory` (`id`),
   CONSTRAINT `fk_INVENTORY_DETAILS_people1` FOREIGN KEY (`USER`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_INVENTORY_DETAILS_products1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `jobs` --
 
 DROP TABLE IF EXISTS `jobs`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `jobs` (
 
   `id` varchar(255) NOT NULL,
   `sens` varchar(45) DEFAULT NULL COMMENT 'Sens du flux - Poussé / Tiré Entrant Sortant ?\nEn gros poussé : on met le message à disposition du tiers et le job va consister à envoyer le message au système suivant\nTiré : on  met le message à disposition du tiers et celui-ci est chargé de venir le chercherCe sont des flux Sortants\nun flux entrant doit être traité en local suivant le type et le message',
   `type` varchar(45) DEFAULT NULL COMMENT 'Type, de cette valeur dépendra l action',
   `destination` varchar(45) DEFAULT NULL COMMENT 'Si envoi à un tiers, id du tiers',
   `etat` varchar(45) DEFAULT NULL COMMENT 'Etat - A Faire - Dispo -  Envoyé - En cours - Fait Ok - Fait Erreur ...',
   `message` longtext COMMENT 'Contenu du message - Instruction',
   `dcreation` datetime DEFAULT NULL COMMENT 'Date de création - Mise à disposition',
   `denvoi` datetime DEFAULT NULL COMMENT 'Dernière Date envoi',
   `dreception` datetime DEFAULT NULL COMMENT 'Date de réception - prise en compte - Accusé de réception',
   `dintegration` datetime DEFAULT NULL COMMENT 'Date intégration - Fin de traitement',
   PRIMARY KEY (`id`),
   KEY `IDX_EDI_SENS` (`sens`,`etat`),
   KEY `IDX_EDI_TYPE` (`type`,`etat`),
   KEY `IDX_EDI_DEST` (`destination`,`etat`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='Table pour stocker les données d échanges'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `jobs_logs` --
 
 DROP TABLE IF EXISTS `jobs_logs`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `jobs_logs` (
 
   `id` varchar(255) NOT NULL,
   `job_id` varchar(255) DEFAULT NULL COMMENT 'Identifiant objet Id concerné',
   `level` varchar(45) DEFAULT NULL COMMENT 'Niveau de Log',
   `message` longtext COMMENT 'Message',
   `date` datetime DEFAULT NULL COMMENT 'Horodatage',
   PRIMARY KEY (`id`),
   KEY `IDX_EDILOG_EDI` (`job_id`),
   KEY `IDX_EDILOG_DATE` (`date` DESC)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `locations` --
 
 DROP TABLE IF EXISTS `locations`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `locations` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `ADDRESS` varchar(255) DEFAULT NULL,
   `TAXCUSTCATEG_ID` varchar(255) DEFAULT NULL,
   `NUM_INSEE` varchar(64) DEFAULT NULL,
   `PARENT_LOCATION_ID` varchar(255) DEFAULT NULL COMMENT 'lien vers la location parente',
   `SERVER_DEFAULT` bit(1) DEFAULT b'0' COMMENT 'Location par défaut pour ce serveur ?',
   `CATEG_ID` varchar(255) DEFAULT NULL,
   `URL_TEXT` varchar(255) DEFAULT NULL,
   `ETAT` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `LOCATIONS_NAME_INX` (`NAME`),
   KEY `fk_locations_taxcustcategories1_idx` (`TAXCUSTCATEG_ID`),
   KEY `FK_LOCATIONS_INSEE_idx` (`NUM_INSEE`),
   KEY `FK_LOCATION_PARENT_idx` (`PARENT_LOCATION_ID`),
   KEY `FK_LOCATIONS_CATEG_idx` (`CATEG_ID`),
   CONSTRAINT `FK_LOCATIONS_CATEG` FOREIGN KEY (`CATEG_ID`) REFERENCES `categories` (`id`),
   CONSTRAINT `FK_LOCATIONS_INSEE` FOREIGN KEY (`NUM_INSEE`) REFERENCES `insee` (`id`),
   CONSTRAINT `FK_LOCATION_PARENT` FOREIGN KEY (`PARENT_LOCATION_ID`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_locations_taxcustcategories1` FOREIGN KEY (`TAXCUSTCATEG_ID`) REFERENCES `taxcustcategories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='= Centre de profit = Entrepot'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `locations_tournees` --
 
 DROP TABLE IF EXISTS `locations_tournees`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `locations_tournees` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `LOCATION_ID` varchar(255) DEFAULT NULL,
   `TOURNEES_ID` int(11) DEFAULT NULL,
   `DEBUT` datetime DEFAULT NULL,
   `FIN` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_LOCATIONS_TOURNEES_locations1_idx` (`LOCATION_ID`),
   KEY `fk_LOCATIONS_TOURNEES_TOURNEES1_idx` (`TOURNEES_ID`),
   CONSTRAINT `fk_LOCATIONS_TOURNEES_TOURNEES1` FOREIGN KEY (`TOURNEES_ID`) REFERENCES `tournees` (`id`),
   CONSTRAINT `fk_LOCATIONS_TOURNEES_locations1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `locations` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Liaison entre les camions = locations et les tournée'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `messages` --
 
 DROP TABLE IF EXISTS `messages`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `messages` (
 
   `ID` varchar(45) NOT NULL,
   `DEBUT` datetime DEFAULT NULL,
   `FIN` datetime DEFAULT NULL,
   `TYPE` varchar(45) DEFAULT NULL,
   `CONTENU` varchar(255) DEFAULT NULL,
   `LOCATION` varchar(255) DEFAULT NULL,
   `PEOPLE` varchar(255) DEFAULT NULL,
   `PRODUCT` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_MESSAGES_people1_idx` (`PEOPLE`),
   KEY `fk_MESSAGES_products1_idx` (`PRODUCT`),
   KEY `fk_MESSAGES_locations1_idx` (`LOCATION`),
   KEY `fk_MESSAGES_type1` (`TYPE`),
   CONSTRAINT `fk_MESSAGES_locations1` FOREIGN KEY (`LOCATION`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_MESSAGES_people1` FOREIGN KEY (`PEOPLE`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_MESSAGES_products1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `fk_MESSAGES_type1` FOREIGN KEY (`TYPE`) REFERENCES `messages_types` (`type`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Possibilité de pousser une série de messages vers les caisse'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `messages_types` --
 
 DROP TABLE IF EXISTS `messages_types`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `messages_types` (
 
   `TYPE` varchar(45) NOT NULL,
   `DESCRIPTION` varchar(255) NOT NULL,
   PRIMARY KEY (`TYPE`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `orders` --
 
 DROP TABLE IF EXISTS `orders`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `orders` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) DEFAULT NULL COMMENT 'REFERENCE DE MVT DE STOCK',
   `USER_CREATION` varchar(255) DEFAULT NULL,
   `DCREATION` datetime DEFAULT NULL,
   `DPLANIFICATION` datetime DEFAULT NULL,
   `DPREPARATION` datetime DEFAULT NULL,
   `DCONTROLE` datetime DEFAULT NULL,
   `DEXPEDITION` datetime DEFAULT NULL,
   `DRECEPTION` datetime DEFAULT NULL,
   `USER_PLANIFICATION` varchar(255) DEFAULT NULL,
   `USER_PREPARATION` varchar(255) DEFAULT NULL,
   `USER_CONTROLE` varchar(255) DEFAULT NULL,
   `USER_EXPEDITION` varchar(255) DEFAULT NULL,
   `USER_RECEPTION` varchar(255) DEFAULT NULL,
   `STATUS` varchar(45) DEFAULT NULL,
   `LOCATION_FROM_ID` varchar(255) DEFAULT NULL COMMENT 'LIEU OU ON PREND LA CAME',
   `LOCATION_TO_ID` varchar(255) DEFAULT NULL COMMENT 'LIEU OU ON MET LA CAME',
   `THIRDPARTIES_ID` varchar(255) DEFAULT NULL COMMENT 'FOURNISSEUR',
   `NOTE` longtext COMMENT 'En-tête des commandes avec le suivi tout au long du cycle de vie.',
   PRIMARY KEY (`ID`),
   KEY `fk_ORDERS_people1_idx` (`USER_PLANIFICATION`),
   KEY `fk_ORDERS_people2_idx` (`USER_PREPARATION`),
   KEY `fk_ORDERS_people3_idx` (`USER_CREATION`),
   KEY `fk_ORDERS_people4_idx` (`USER_RECEPTION`),
   KEY `fk_ORDERS_locations1_idx` (`LOCATION_FROM_ID`),
   KEY `fk_ORDERS_locations2_idx` (`LOCATION_TO_ID`),
   KEY `fk_ORDERS_thirdparties1_idx` (`THIRDPARTIES_ID`),
   CONSTRAINT `fk_ORDERS_locations1` FOREIGN KEY (`LOCATION_FROM_ID`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_ORDERS_locations2` FOREIGN KEY (`LOCATION_TO_ID`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_ORDERS_people1` FOREIGN KEY (`USER_PLANIFICATION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_ORDERS_people2` FOREIGN KEY (`USER_PREPARATION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_ORDERS_people3` FOREIGN KEY (`USER_CREATION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_ORDERS_people4` FOREIGN KEY (`USER_RECEPTION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_ORDERS_thirdparties1` FOREIGN KEY (`THIRDPARTIES_ID`) REFERENCES `thirdparties` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `orders_detail` --
 
 DROP TABLE IF EXISTS `orders_detail`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `orders_detail` (
 
   `ID` varchar(255) NOT NULL COMMENT 'ORDERS_ID - PRODUCT_ID',
   `ORDERS_ID` varchar(255) NOT NULL,
   `PRODUCT_ID` varchar(255) NOT NULL,
   `QUANTITY_ASKED` double DEFAULT NULL,
   `QUANTITY_TO_PREPARE` double DEFAULT NULL,
   `QUANTITY_PREPARED` double DEFAULT NULL,
   `QUANTITY_SENT` double DEFAULT NULL,
   `QUANTITY_RECEIVED` double DEFAULT NULL,
   `NOTE` longtext,
   PRIMARY KEY (`ID`),
   KEY `fk_ORDERS_DETAIL_ORDERS1_idx` (`ORDERS_ID`),
   KEY `fk_ORDERS_DETAIL_products1_idx` (`PRODUCT_ID`),
   CONSTRAINT `fk_ORDERS_DETAIL_ORDERS1` FOREIGN KEY (`ORDERS_ID`) REFERENCES `orders` (`id`),
   CONSTRAINT `fk_ORDERS_DETAIL_products1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `palettes` --
 
 DROP TABLE IF EXISTS `palettes`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `palettes` (
 
   `ID` varchar(255) NOT NULL DEFAULT '',
   `ORDER_ID` varchar(255) NOT NULL COMMENT 'Description d''une palette',
   `NAME` varchar(255) DEFAULT NULL,
   `DCREATION` datetime DEFAULT NULL,
   `DPLANIFICATION` datetime DEFAULT NULL,
   `DPREPARATION` datetime DEFAULT NULL,
   `DCONTROLE` datetime DEFAULT NULL,
   `DEXPEDITION` datetime DEFAULT NULL,
   `DRECEPTION` datetime DEFAULT NULL,
   `USER_CREATION` varchar(255) DEFAULT NULL,
   `USER_PLANIFICATION` varchar(255) DEFAULT NULL,
   `USER_PREPARATION` varchar(255) DEFAULT NULL,
   `USER_CONTROLE` varchar(255) DEFAULT NULL,
   `USER_EXPEDITION` varchar(255) DEFAULT NULL,
   `USER_RECEPTION` varchar(255) DEFAULT NULL,
   `STATUS` varchar(45) DEFAULT NULL,
   `NOTE` longtext,
   PRIMARY KEY (`ID`),
   KEY `fk_PALETTE_ORDERS1_idx` (`ORDER_ID`),
   KEY `fk_PALETTE_people1_idx` (`USER_CREATION`),
   KEY `fk_PALETTE_people2_idx` (`USER_PREPARATION`),
   KEY `fk_PALETTE_people3_idx` (`USER_RECEPTION`),
   CONSTRAINT `fk_PALETTE_ORDERS1` FOREIGN KEY (`ORDER_ID`) REFERENCES `orders` (`id`),
   CONSTRAINT `fk_PALETTE_people1` FOREIGN KEY (`USER_CREATION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_PALETTE_people2` FOREIGN KEY (`USER_PREPARATION`) REFERENCES `people` (`id`),
   CONSTRAINT `fk_PALETTE_people3` FOREIGN KEY (`USER_RECEPTION`) REFERENCES `people` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `palettes_detail` --
 
 DROP TABLE IF EXISTS `palettes_detail`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `palettes_detail` (
 
   `ID` varchar(255) NOT NULL,
   `PALETTE_ID` varchar(255) NOT NULL,
   `ORDER_DETAIL_ID` varchar(255) NOT NULL,
   `QUANTITY_TO_PREPARE` double DEFAULT NULL,
   `QUANTITY_PREPARED` double DEFAULT NULL,
   `QUANTITY_SENT` double DEFAULT NULL,
   `QUANTITY_RECEIVED` double DEFAULT NULL,
   `STATUS` varchar(45) DEFAULT NULL,
   `STOCK_LOCATION_NAME` varchar(45) DEFAULT NULL COMMENT 'L''emplacement où ramasser',
   `NOTE` longtext,
   `DPREPARATION` datetime DEFAULT NULL,
   `USER_PREPARATION` varchar(255) DEFAULT NULL,
   `INPUT_ORIGINE` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_PALETTES_DETAIL_PALETTES1_idx` (`PALETTE_ID`),
   KEY `fk_PALETTES_DETAIL_ORDERS_DETAIL1_idx` (`ORDER_DETAIL_ID`),
   CONSTRAINT `fk_PALETTES_DETAIL_ORDERS_DETAIL1` FOREIGN KEY (`ORDER_DETAIL_ID`) REFERENCES `orders_detail` (`id`),
   CONSTRAINT `fk_PALETTES_DETAIL_PALETTES1` FOREIGN KEY (`PALETTE_ID`) REFERENCES `palettes` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `payments` --
 
 DROP TABLE IF EXISTS `payments`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `payments` (
 
   `ID` varchar(255) NOT NULL,
   `RECEIPT` varchar(255) NOT NULL,
   `PAYMENT` varchar(255) NOT NULL,
   `TOTAL` double NOT NULL,
   `CURRENCY` int(11) NOT NULL,
   `TOTALCURRENCY` double NOT NULL,
   `REFERENCE` varchar(255) DEFAULT NULL,
   `RETURNMSG` mediumblob,
   `NOTE` text,
   `ECHEANCE` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `PAYMENTS_FK_RECEIPT` (`RECEIPT`),
   KEY `PAYMENTS_FK_CURRENCY` (`CURRENCY`),
   KEY `PAYMENTS_INX_1` (`PAYMENT`),
   KEY `PAYMENTS_INX_2` (`RECEIPT`) USING BTREE,
   CONSTRAINT `PAYMENTS_FK_CURRENCY` FOREIGN KEY (`CURRENCY`) REFERENCES `currencies` (`id`),
   CONSTRAINT `PAYMENTS_FK_RECEIPT` FOREIGN KEY (`RECEIPT`) REFERENCES `receipts` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `pays` --
 
 DROP TABLE IF EXISTS `pays`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `pays` (
 
   `PAYS_ID` int(11) NOT NULL AUTO_INCREMENT,
   `NOM` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
   `ABREVIATION` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
   `CODE_DAE` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
   PRIMARY KEY (`PAYS_ID`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `people` --
 
 DROP TABLE IF EXISTS `people`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `people` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `APPPASSWORD` varchar(255) DEFAULT NULL,
   `CARD` varchar(255) DEFAULT NULL,
   `ROLE` varchar(255) NOT NULL,
   `VISIBLE` bit(1) NOT NULL,
   `IMAGE` mediumblob,
   `NOM` varchar(255) DEFAULT NULL,
   `PRENOM` varchar(255) DEFAULT NULL,
   `DDEBUT` datetime DEFAULT NULL,
   `DFIN` datetime DEFAULT NULL,
   `LASTUPDATE` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PEOPLE_NAME_INX` (`NAME`),
   KEY `PEOPLE_FK_1` (`ROLE`),
   KEY `PEOPLE_CARD_INX` (`CARD`),
   CONSTRAINT `PEOPLE_FK_1` FOREIGN KEY (`ROLE`) REFERENCES `roles` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `places` --
 
 DROP TABLE IF EXISTS `places`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `places` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `X` int(11) NOT NULL,
   `Y` int(11) NOT NULL,
   `FLOOR` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PLACES_NAME_INX` (`NAME`),
   KEY `PLACES_FK_1` (`FLOOR`),
   CONSTRAINT `PLACES_FK_1` FOREIGN KEY (`FLOOR`) REFERENCES `floors` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `product_tax_categ` --
 
 DROP TABLE IF EXISTS `product_tax_categ`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `product_tax_categ` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `PRODUCT_ID` varchar(255) NOT NULL,
   `TAX_CATEG_ID` varchar(255) NOT NULL,
   `ORDER` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_PRODUCT_TAX_CATEG_products1_idx` (`PRODUCT_ID`),
   KEY `fk_PRODUCT_TAX_CATEG_taxcategories1_idx` (`TAX_CATEG_ID`),
   CONSTRAINT `fk_PRODUCT_TAX_CATEG_products1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`id`),
   CONSTRAINT `fk_PRODUCT_TAX_CATEG_taxcategories1` FOREIGN KEY (`TAX_CATEG_ID`) REFERENCES `taxcategories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='taxcat dans le produit = Catégorie de TVA applicabl'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `products` --
 
 DROP TABLE IF EXISTS `products`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `products` (
 
   `ID` varchar(255) NOT NULL,
   `REFERENCE` varchar(255) NOT NULL,
   `CODE` varchar(255) DEFAULT NULL,
   `CODETYPE` varchar(255) DEFAULT NULL,
   `NAME` varchar(255) NOT NULL,
   `PRICEBUY` double DEFAULT NULL,
   `PRICESELL` double NOT NULL,
   `CATEGORY` varchar(255) NOT NULL,
   `TAXCAT` varchar(255) NOT NULL,
   `ATTRIBUTESET_ID` varchar(255) DEFAULT NULL,
   `STOCKCOST` double DEFAULT NULL,
   `STOCKVOLUME` double DEFAULT NULL,
   `IMAGE` mediumblob,
   `ISCOM` bit(1) NOT NULL DEFAULT b'0',
   `ISSCALE` bit(1) NOT NULL DEFAULT b'0',
   `ATTRIBUTES` mediumblob,
   `DISCOUNTENABLED` bit(1) NOT NULL DEFAULT b'0',
   `DISCOUNTRATE` double NOT NULL DEFAULT '0',
   `DELETED` bit(1) NOT NULL DEFAULT b'0',
   `STOCKABLE` bit(1) NOT NULL DEFAULT b'1' COMMENT 'Permet de savoir si le produit est géré en stock',
   `LASTUPDATE` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PRODUCTS_INX_0` (`REFERENCE`),
   KEY `PRODUCTS_FK_1` (`CATEGORY`),
   KEY `PRODUCTS_TAXCAT_FK` (`TAXCAT`),
   KEY `PRODUCTS_ATTRSET_FK` (`ATTRIBUTESET_ID`),
   KEY `PRODUCTS_NAME_INX` (`NAME`),
   KEY `PRODUCTS_CODE` (`CODE`),
   KEY `PRODUCTS_LASTUPDATE_INX` (`LASTUPDATE`),
   CONSTRAINT `PRODUCTS_ATTRSET_FK` FOREIGN KEY (`ATTRIBUTESET_ID`) REFERENCES `attributeset` (`id`),
   CONSTRAINT `PRODUCTS_FK_1` FOREIGN KEY (`CATEGORY`) REFERENCES `categories` (`id`),
   CONSTRAINT `PRODUCTS_TAXCAT_FK` FOREIGN KEY (`TAXCAT`) REFERENCES `taxcategories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='<double-click to overwrite multiple objects>'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `products_cat` --
 
 DROP TABLE IF EXISTS `products_cat`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `products_cat` (
 
   `PRODUCT` varchar(255) NOT NULL,
   `CATORDER` int(11) DEFAULT NULL,
   PRIMARY KEY (`PRODUCT`),
   KEY `PRODUCTS_CAT_INX_1` (`CATORDER`),
   CONSTRAINT `PRODUCTS_CAT_FK_1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `products_com` --
 
 DROP TABLE IF EXISTS `products_com`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `products_com` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `PRODUCT` varchar(255) NOT NULL COMMENT 'Référence de l''offre',
   `PRODUCT2` varchar(255) NOT NULL COMMENT 'Reference du composant',
   `QUANTITY` double NOT NULL DEFAULT '1' COMMENT 'Quantité du composant entrant dans la composition',
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PCOM_INX_PROD` (`PRODUCT`,`PRODUCT2`),
   KEY `PRODUCTS_COM_FK_2` (`PRODUCT2`),
   CONSTRAINT `PRODUCTS_COM_FK_1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `PRODUCTS_COM_FK_2` FOREIGN KEY (`PRODUCT2`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `products_ref` --
 
 DROP TABLE IF EXISTS `products_ref`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `products_ref` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `PRODUCT_ID` varchar(255) NOT NULL,
   `REFERENCE` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_products_ref_products1_idx` (`PRODUCT_ID`),
   KEY `IND_REF` (`REFERENCE`),
   CONSTRAINT `fk_products_ref_products1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Table pour stocker les multiples références d un même produ'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `receipts` --
 
 DROP TABLE IF EXISTS `receipts`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `receipts` (
 
   `ID` varchar(255) NOT NULL,
   `MONEY` varchar(255) NOT NULL,
   `DATENEW` datetime NOT NULL,
   `ATTRIBUTES` mediumblob,
   PRIMARY KEY (`ID`),
   KEY `RECEIPTS_FK_MONEY` (`MONEY`),
   KEY `RECEIPTS_INX_1` (`DATENEW`),
   CONSTRAINT `RECEIPTS_FK_MONEY` FOREIGN KEY (`MONEY`) REFERENCES `closedcash` (`money`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `reservation_customers` --
 
 DROP TABLE IF EXISTS `reservation_customers`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `reservation_customers` (
 
   `ID` varchar(255) NOT NULL,
   `CUSTOMER` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `RES_CUST_FK_2` (`CUSTOMER`),
   CONSTRAINT `RES_CUST_FK_1` FOREIGN KEY (`ID`) REFERENCES `reservations` (`id`),
   CONSTRAINT `RES_CUST_FK_2` FOREIGN KEY (`CUSTOMER`) REFERENCES `customers` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `reservations` --
 
 DROP TABLE IF EXISTS `reservations`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `reservations` (
 
   `ID` varchar(255) NOT NULL,
   `CREATED` datetime NOT NULL,
   `DATENEW` datetime NOT NULL DEFAULT '2001-01-01 00:00:00',
   `TITLE` varchar(255) NOT NULL,
   `CHAIRS` int(11) NOT NULL,
   `ISDONE` bit(1) NOT NULL,
   `DESCRIPTION` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `RESERVATIONS_INX_1` (`DATENEW`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `resources` --
 
 DROP TABLE IF EXISTS `resources`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `resources` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `RESTYPE` int(11) NOT NULL,
   `CONTENT` mediumblob,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `RESOURCES_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `roles` --
 
 DROP TABLE IF EXISTS `roles`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `roles` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `PERMISSIONS` mediumblob,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `ROLES_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `sharedtickets` --
 
 DROP TABLE IF EXISTS `sharedtickets`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `sharedtickets` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `CONTENT` mediumblob,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `stockcurrent` --
 
 DROP TABLE IF EXISTS `stockcurrent`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `stockcurrent` (
 
   `LOCATION` varchar(255) NOT NULL,
   `PRODUCT` varchar(255) NOT NULL,
   `ATTRIBUTESETINSTANCE_ID` varchar(255) DEFAULT NULL,
   `UNITS` double NOT NULL,
   PRIMARY KEY (`PRODUCT`,`LOCATION`),
   UNIQUE KEY `STOCKCURRENT_INX` (`LOCATION`,`PRODUCT`,`ATTRIBUTESETINSTANCE_ID`),
   KEY `STOCKCURRENT_ATTSETINST` (`ATTRIBUTESETINSTANCE_ID`),
   CONSTRAINT `STOCKCURRENT_ATTSETINST` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`id`),
   CONSTRAINT `STOCKCURRENT_FK_1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `STOCKCURRENT_FK_2` FOREIGN KEY (`LOCATION`) REFERENCES `locations` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `stockdiary` --
 
 DROP TABLE IF EXISTS `stockdiary`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `stockdiary` (
 
   `ID` varchar(255) NOT NULL,
   `DATENEW` datetime NOT NULL,
   `REASON` int(11) NOT NULL,
   `LOCATION` varchar(255) NOT NULL,
   `PRODUCT` varchar(255) NOT NULL,
   `ATTRIBUTESETINSTANCE_ID` varchar(255) DEFAULT NULL,
   `UNITS` double NOT NULL,
   `PRICE` double NOT NULL,
   `NOTE` longtext,
   `DESTINATION` varchar(255) DEFAULT NULL,
   `REFERENCE` varchar(255) DEFAULT NULL,
   `STATE` varchar(45) DEFAULT NULL,
   `STOCK_BEFORE` double DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `STOCKDIARY_FK_1` (`PRODUCT`),
   KEY `STOCKDIARY_ATTSETINST` (`ATTRIBUTESETINSTANCE_ID`),
   KEY `STOCKDIARY_FK_2` (`LOCATION`),
   KEY `STOCKDIARY_INX_1` (`DATENEW`),
   KEY `STOCKDIARY_FK_DESTINATION_idx` (`DESTINATION`),
   KEY `STOCKDIARY_INX_2` (`REFERENCE`) USING BTREE,
   KEY `STOCKDIARY_RLD_INX` (`LOCATION`,`REASON`,`DATENEW`),
   CONSTRAINT `STOCKDIARY_ATTSETINST` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`id`),
   CONSTRAINT `STOCKDIARY_FK_1` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `STOCKDIARY_FK_2` FOREIGN KEY (`LOCATION`) REFERENCES `locations` (`id`),
   CONSTRAINT `STOCKDIARY_FK_DESTINATION` FOREIGN KEY (`DESTINATION`) REFERENCES `locations` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `stocklevel` --
 
 DROP TABLE IF EXISTS `stocklevel`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `stocklevel` (
 
   `ID` varchar(255) NOT NULL,
   `LOCATION` varchar(255) NOT NULL,
   `PRODUCT` varchar(255) NOT NULL,
   `STOCKSECURITY` double DEFAULT NULL,
   `STOCKMAXIMUM` double DEFAULT NULL,
   `LASTUPDATE` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `STOCKLEVEL_PRODUCT` (`PRODUCT`),
   KEY `STOCKLEVEL_LOCATION` (`LOCATION`),
   CONSTRAINT `STOCKLEVEL_LOCATION` FOREIGN KEY (`LOCATION`) REFERENCES `locations` (`id`),
   CONSTRAINT `STOCKLEVEL_PRODUCT` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `subgroups` --
 
 DROP TABLE IF EXISTS `subgroups`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `subgroups` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `COMPOSITION` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `REQUIRED` bit(1) NOT NULL DEFAULT b'1',
   `IMAGE` mediumblob,
   `DISPORDER` int(11) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `SUBGROUPS_FK_1` (`COMPOSITION`),
   CONSTRAINT `SUBGROUPS_FK_1` FOREIGN KEY (`COMPOSITION`) REFERENCES `products` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `subgroups_prod` --
 
 DROP TABLE IF EXISTS `subgroups_prod`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `subgroups_prod` (
 
   `SUBGROUP` int(11) NOT NULL,
   `PRODUCT` varchar(255) NOT NULL,
   `DISPORDER` int(11) NOT NULL,
   PRIMARY KEY (`SUBGROUP`,`PRODUCT`),
   KEY `SUBGROUPS_PROD_FK_2` (`PRODUCT`),
   CONSTRAINT `SUBGROUPS_PROD_FK_1` FOREIGN KEY (`SUBGROUP`) REFERENCES `subgroups` (`id`) ON DELETE CASCADE,
   CONSTRAINT `SUBGROUPS_PROD_FK_2` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `tariffareas` --
 
 DROP TABLE IF EXISTS `tariffareas`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `tariffareas` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `NAME` varchar(255) NOT NULL,
   `TARIFFORDER` int(11) DEFAULT '0',
   PRIMARY KEY (`ID`),
   UNIQUE KEY `TARIFFAREAS_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Catalogue/liste de prix (donc plutôt tarrifaire) pour associer un prix à un produit'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `tariffareas_location` --
 
 DROP TABLE IF EXISTS `tariffareas_location`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `tariffareas_location` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `TARIFFAREAS_ID` int(11) DEFAULT NULL,
   `LOCATION_ID` varchar(255) DEFAULT NULL,
   `DATE_FROM` datetime DEFAULT NULL,
   `DATE_TO` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `fk_tariffareas_location_locations_idx` (`LOCATION_ID`),
   KEY `fk_tariffareas_location_tariffareas1_idx` (`TARIFFAREAS_ID`),
   CONSTRAINT `fk_tariffareas_location_locations` FOREIGN KEY (`LOCATION_ID`) REFERENCES `locations` (`id`),
   CONSTRAINT `fk_tariffareas_location_tariffareas1` FOREIGN KEY (`TARIFFAREAS_ID`) REFERENCES `tariffareas` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Associer un intervalle de validité dans le temps entre un tariffaire / catalogue et un point de vente'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `tariffareas_prod` --
 
 DROP TABLE IF EXISTS `tariffareas_prod`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `tariffareas_prod` (
 
   `TARIFFID` int(11) NOT NULL AUTO_INCREMENT,
   `PRODUCTID` varchar(255) NOT NULL,
   `PRICESELL` double NOT NULL,
   PRIMARY KEY (`TARIFFID`,`PRODUCTID`),
   KEY `TARIFFAREAS_PROD_FK_2` (`PRODUCTID`),
   CONSTRAINT `TARIFFAREAS_PROD_FK_1` FOREIGN KEY (`TARIFFID`) REFERENCES `tariffareas` (`id`) ON DELETE CASCADE,
   CONSTRAINT `TARIFFAREAS_PROD_FK_2` FOREIGN KEY (`PRODUCTID`) REFERENCES `products` (`id`) ON DELETE CASCADE
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `taxcategories` --
 
 DROP TABLE IF EXISTS `taxcategories`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `taxcategories` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `TAXCAT_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `taxcustcategories` --
 
 DROP TABLE IF EXISTS `taxcustcategories`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `taxcustcategories` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `TAXCUSTCAT_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `taxes` --
 
 DROP TABLE IF EXISTS `taxes`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `taxes` (
 
   `ID` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `VALIDFROM` datetime NOT NULL DEFAULT '2001-01-01 00:00:00',
   `CATEGORY` varchar(255) NOT NULL,
   `CUSTCATEGORY` varchar(255) DEFAULT NULL,
   `PARENTID` varchar(255) DEFAULT NULL,
   `RATE` double DEFAULT NULL,
   `RATECASCADE` bit(1) NOT NULL DEFAULT b'0',
   `RATEORDER` int(11) DEFAULT NULL,
   `VALIDTO` datetime DEFAULT NULL,
   `AMOUNT` double DEFAULT '0',
   PRIMARY KEY (`ID`),
   UNIQUE KEY `TAXES_NAME_INX` (`NAME`),
   KEY `TAXES_CAT_FK` (`CATEGORY`),
   KEY `TAXES_CUSTCAT_FK` (`CUSTCATEGORY`),
   KEY `TAXES_TAXES_FK` (`PARENTID`),
   CONSTRAINT `TAXES_CAT_FK` FOREIGN KEY (`CATEGORY`) REFERENCES `taxcategories` (`id`),
   CONSTRAINT `TAXES_CUSTCAT_FK` FOREIGN KEY (`CUSTCATEGORY`) REFERENCES `taxcustcategories` (`id`),
   CONSTRAINT `TAXES_TAXES_FK` FOREIGN KEY (`PARENTID`) REFERENCES `taxes` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catégorie = Taux plei'; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `taxlines` --
 
 DROP TABLE IF EXISTS `taxlines`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `taxlines` (
 
   `ID` varchar(255) NOT NULL,
   `RECEIPT` varchar(255) NOT NULL,
   `TAXID` varchar(255) NOT NULL,
   `BASE` double NOT NULL,
   `AMOUNT` double NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `TAXLINES_TAX` (`TAXID`),
   KEY `TAXLINES_RECEIPT` (`RECEIPT`),
   CONSTRAINT `TAXLINES_RECEIPT` FOREIGN KEY (`RECEIPT`) REFERENCES `receipts` (`id`),
   CONSTRAINT `TAXLINES_TAX` FOREIGN KEY (`TAXID`) REFERENCES `taxes` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `thirdparties` --
 
 DROP TABLE IF EXISTS `thirdparties`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `thirdparties` (
 
   `ID` varchar(255) NOT NULL,
   `CIF` varchar(255) NOT NULL,
   `NAME` varchar(255) NOT NULL,
   `ADDRESS` varchar(255) DEFAULT NULL,
   `CONTACTCOMM` varchar(255) DEFAULT NULL,
   `CONTACTFACT` varchar(255) DEFAULT NULL,
   `PAYRULE` varchar(255) DEFAULT NULL,
   `FAXNUMBER` varchar(255) DEFAULT NULL,
   `PHONENUMBER` varchar(255) DEFAULT NULL,
   `MOBILENUMBER` varchar(255) DEFAULT NULL,
   `EMAIL` varchar(255) DEFAULT NULL,
   `WEBPAGE` varchar(255) DEFAULT NULL,
   `NOTES` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `THIRDPARTIES_CIF_INX` (`CIF`),
   UNIQUE KEY `THIRDPARTIES_NAME_INX` (`NAME`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `ticketlines` --
 
 DROP TABLE IF EXISTS `ticketlines`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `ticketlines` (
 
   `TICKET` varchar(255) NOT NULL,
   `LINE` int(11) NOT NULL,
   `PRODUCT` varchar(255) DEFAULT NULL,
   `ATTRIBUTESETINSTANCE_ID` varchar(255) DEFAULT NULL,
   `UNITS` double NOT NULL,
   `PRICE` double NOT NULL,
   `TAXID` varchar(255) NOT NULL,
   `DISCOUNTRATE` double NOT NULL DEFAULT '0',
   `ATTRIBUTES` mediumblob,
   `TYPE` varchar(1) DEFAULT 'S' COMMENT 'Type de ligne de vente',
   `TARIFFAREA` int(11) DEFAULT NULL,
   `NOTE` text,
   `PARENT_TICKET_LINE` int(11) DEFAULT NULL,
   `PARENT_TICKET_ID` varchar(255) DEFAULT NULL,

   `unitPrice` double DEFAULT NULL,
   `taxedUnitPrice` double DEFAULT NULL,
   `taxedPrice` double NOT NULL,
   `taxRate` double NOT NULL,
   `finalPrice` double DEFAULT NULL,
   `finalTaxedPrice` double DEFAULT NULL,

   PRIMARY KEY (`TICKET`,`LINE`),
   KEY `TICKETLINES_FK_2` (`PRODUCT`),
   KEY `TICKETLINES_ATTSETINST` (`ATTRIBUTESETINSTANCE_ID`),
   KEY `TICKETLINES_FK_3` (`TAXID`),
   KEY `TICKETLINES_FK_TARIFFAREA_idx` (`TARIFFAREA`),
   KEY `TICKETLINES_FK_PARENT_idx` (`PARENT_TICKET_LINE`,`PARENT_TICKET_ID`),
   CONSTRAINT `TICKETLINES_ATTSETINST` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`id`),
   CONSTRAINT `TICKETLINES_FK_2` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `TICKETLINES_FK_3` FOREIGN KEY (`TAXID`) REFERENCES `taxes` (`id`),
   CONSTRAINT `TICKETLINES_FK_TARIFFAREA` FOREIGN KEY (`TARIFFAREA`) REFERENCES `tariffareas` (`id`),
   CONSTRAINT `TICKETLINES_FK_TICKET` FOREIGN KEY (`TICKET`) REFERENCES `tickets` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 -- -- Table structure for table `ticketlinesdeleted` --
 
 DROP TABLE IF EXISTS `ticketlinesdeleted`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `ticketlinesdeleted` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `TICKET` varchar(255) NOT NULL,
   `LINE` int(11) NOT NULL,
   `PRODUCT` varchar(255) DEFAULT NULL,
   `ATTRIBUTESETINSTANCE_ID` varchar(255) DEFAULT NULL,
   `UNITS` double NOT NULL,
   `PRICE` double NOT NULL,
   `TAXID` varchar(255) NOT NULL,
   `DISCOUNTRATE` double NOT NULL DEFAULT '0',
   `ATTRIBUTES` mediumblob,
   `TYPE` varchar(1) DEFAULT 'S' COMMENT 'Type de ligne de vente',
   `TARIFFAREA` int(11) DEFAULT NULL,
   `NOTE` text,
   `IS_OPENED_PAYMENT` bit(1) NOT NULL DEFAULT b'0',
   `CREATED_DATE` datetime NOT NULL,
   `DELETED_DATE` datetime NOT NULL,
   PRIMARY KEY (`ID`),
   KEY `TICKETLINESDELETED_FK_2` (`PRODUCT`),
   KEY `TICKETLINESDELETED_ATTSETINST` (`ATTRIBUTESETINSTANCE_ID`),
   KEY `TICKETLINESDELETED_FK_3` (`TAXID`),
   KEY `TICKETLINESDELETED_FK_TARIFFAREA_idx` (`TARIFFAREA`),
   CONSTRAINT `TICKETLINESDELETED_ATTSETINST` FOREIGN KEY (`ATTRIBUTESETINSTANCE_ID`) REFERENCES `attributesetinstance` (`id`),
   CONSTRAINT `TICKETLINESDELETED_FK_2` FOREIGN KEY (`PRODUCT`) REFERENCES `products` (`id`),
   CONSTRAINT `TICKETLINESDELETED_FK_3` FOREIGN KEY (`TAXID`) REFERENCES `taxes` (`id`),
   CONSTRAINT `TICKETLINESDELETED_FK_TARIFFAREA` FOREIGN KEY (`TARIFFAREA`) REFERENCES `tariffareas` (`id`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 
 DROP TABLE IF EXISTS `tickets`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `tickets` (
 
   `ID` varchar(255) NOT NULL,
   `TICKETTYPE` int(11) NOT NULL DEFAULT '0',
   `TICKETID` bigint(20) NOT NULL,
   `PERSON` varchar(255) NOT NULL,
   `CUSTOMER` varchar(255) DEFAULT NULL,
   `STATUS` int(11) NOT NULL DEFAULT '0',
   `CUSTCOUNT` int(11) DEFAULT NULL,
   `TARIFFAREA` int(11) DEFAULT NULL,
   `DISCOUNTRATE` double NOT NULL DEFAULT '0',
   `DISCOUNTPROFILE_ID` int(11) DEFAULT NULL,
   `CUSTZIPCODE` varchar(64) DEFAULT NULL COMMENT 'Code postal - Code INSEE pour identifier localisation goégraphique de provenence du client',
   `PARENT_TICKET_ID` varchar(255) DEFAULT NULL,
   `NOTE` text,

   `sequence` int(11) NOT NULL,
   `number` int(11) NOT NULL,
   `date` datetime NOT NULL,
   `price` double DEFAULT NULL,
   `taxedPrice` double DEFAULT NULL,
   `finalPrice` double NOT NULL,
   `finalTaxedPrice` double NOT NULL,
   `custBalance` double NOT NULL,

 

   PRIMARY KEY (`ID`),
   KEY `TICKETS_FK_2` (`PERSON`),
   KEY `TICKETS_CUSTOMERS_FK` (`CUSTOMER`),
   KEY `TICKETS_TARIFFAREA_FK` (`TARIFFAREA`),
   KEY `TICKETS_DISCOUNTPROFILE` (`DISCOUNTPROFILE_ID`),
   KEY `TICKETS_TICKETID` (`TICKETTYPE`,`TICKETID`),
   KEY `TICKETS_FK_PARENT_idx` (`PARENT_TICKET_ID`),
   KEY `TICKETS_TICKETNMR` (`TICKETID`),
   CONSTRAINT `TICKETS_CUSTOMERS_FK` FOREIGN KEY (`CUSTOMER`) REFERENCES `customers` (`id`),
   CONSTRAINT `TICKETS_DISCOUNTPROFILE` FOREIGN KEY (`DISCOUNTPROFILE_ID`) REFERENCES `discountprofiles` (`id`),
   CONSTRAINT `TICKETS_FK_2` FOREIGN KEY (`PERSON`) REFERENCES `people` (`id`),
   CONSTRAINT `TICKETS_FK_ID` FOREIGN KEY (`ID`) REFERENCES `receipts` (`id`),
   CONSTRAINT `TICKETS_FK_PARENT` FOREIGN KEY (`PARENT_TICKET_ID`) REFERENCES `tickets` (`id`),
   CONSTRAINT `TICKETS_TARIFFAREA_FK` FOREIGN KEY (`TARIFFAREA`) REFERENCES `tariffareas` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 
 DROP TABLE IF EXISTS `tournees`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `tournees` (
 
   `ID` int(11) NOT NULL AUTO_INCREMENT,
   `NOM` varchar(255) DEFAULT NULL,
   PRIMARY KEY (`ID`)
 
 ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `archives`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `archives` (
 
   number INT NOT NULL,
   info LONGTEXT NOT NULL,
   content LONGBLOB NOT NULL,
   contentHash VARCHAR(255) NOT NULL,
   signature VARCHAR(255) NOT NULL,
   PRIMARY KEY(number)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `archiverequests`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `archiverequests` (
 
   id INT AUTO_INCREMENT NOT NULL,
   startDate DATETIME NOT NULL,
   stopDate DATETIME NOT NULL,
   processing TINYINT(1) NOT NULL,
   PRIMARY KEY(id)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `fiscaltickets`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `fiscaltickets` (
 
   `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `sequence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `number` int(11) NOT NULL,
   `date` datetime NOT NULL,
   `content` longtext COLLATE utf8_unicode_ci NOT NULL,
   `signature` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   PRIMARY KEY (`type`,`sequence`,`number`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; /*!40101 SET character_set_client = @saved_cs_client */;
 


 DROP TABLE IF EXISTS `periods`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `periods` (
 
   `id` varchar(255) NOT NULL,
   `CASHREGISTER_ID` int(11) NOT NULL,
   `SEQUENCE` int(11) NOT NULL,
   `TYPE` varchar(255) NOT NULL,
   `DATESTART` datetime DEFAULT NULL,
   `DATEEND` datetime DEFAULT NULL,
   `continuous` tinyint(1) NOT NULL,
   `ticketCount` int(11) DEFAULT NULL,
   `custCount` int(11) DEFAULT NULL,
   `cs` double DEFAULT NULL,
   `csPeriod` double NOT NULL,
   `csFYear` double NOT NULL,
   `csPerpetual` double NOT NULL,
   `closed` tinyint(1) NOT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PERIODS_INX_SEQ` (`CASHREGISTER_ID`,`SEQUENCE`,`TYPE`),
   KEY `PERIODS_INX_1` (`DATESTART`),
   KEY `PERIODS_CRTDAT` (`CASHREGISTER_ID`,`TYPE`,`DATESTART`,`DATEEND`),
   KEY `PERIODS_CROPEN` (`CASHREGISTER_ID`,`closed`),
   CONSTRAINT `PERIODS_FK_CASHREGISTER` FOREIGN KEY (`CASHREGISTER_ID`) REFERENCES `cashregisters` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;


 DROP TABLE IF EXISTS `periodcats`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `periodcats` (
 
   `period_id` varchar(255) NOT NULL,
   `categ_id` varchar(255) NOT NULL,
   `label` varchar(255) NOT NULL,
   `amount` double NOT NULL,
   PRIMARY KEY (`period_id`,`categ_id`),
   KEY `IDX_F32F3B52C914703E` (`period_id`),
   KEY `FK_PERIOD_CATEG_idx` (`categ_id`),
   CONSTRAINT `FK_F32F3B52C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`),
   CONSTRAINT `FK_PERIOD_CATEG` FOREIGN KEY (`categ_id`) REFERENCES `categories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `periodcattaxes`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `periodcattaxes` (
 
   `period_id` varchar(255) NOT NULL,
   `categ_id` varchar(255) NOT NULL,
   `tax_id` varchar(255)  NOT NULL,
   `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `base` double NOT NULL,
   `amount` double NOT NULL,
   PRIMARY KEY (`period_id`,`categ_id`,`tax_id`),
   KEY `IDX_E4084375C914703E` (`period_id`),
   KEY `IDX_E4084375B2A824D8` (`tax_id`),
   KEY `FK_PERIOD_CATEG_1_idx` (`categ_id`),
   CONSTRAINT `FK_E4084375B2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `taxes` (`id`),
   CONSTRAINT `FK_E4084375C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`),
   CONSTRAINT `FK_PERIOD_CATEG_1` FOREIGN KEY (`categ_id`) REFERENCES `categories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `periodcustbalances`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4 ;
 
 CREATE TABLE `periodcustbalances` (
 
   `period_id` varchar(255) NOT NULL,
   `customer_id` varchar(255) NOT NULL,
   `balance` double NOT NULL,
   PRIMARY KEY (`period_id`,`customer_id`),
   KEY `IDX_9D3CBD8BC914703E` (`period_id`),
   KEY `IDX_9D3CBD8B9395C3F3` (`customer_id`),
   CONSTRAINT `FK_9D3CBD8B9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
   CONSTRAINT `FK_9D3CBD8BC914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `periodpayments`; /*!40101 SET @saved_cs_client = @@character_set_client */;
 
  SET character_set_client = utf8mb4;
 
 CREATE TABLE `periodpayments` (
 
   `period_id` varchar(255) NOT NULL,
   `paymentmode_id` varchar(255) NOT NULL,
   `currency_id` int(11) NOT NULL,
   `amount` double NOT NULL,
   `currencyAmount` double NOT NULL,
   PRIMARY KEY (`period_id`,`paymentmode_id`,`currency_id`),
   KEY `IDX_342F815C914703E` (`period_id`),
   KEY `IDX_342F815ABEEF95F` (`paymentmode_id`),
   KEY `IDX_342F81538248176` (`currency_id`),
   CONSTRAINT `FK_342F81538248176` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
   CONSTRAINT `FK_342F815C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */;
 
 DROP TABLE IF EXISTS `periodtaxes`; /*!40101 SET @saved_cs_client = @@character_set_client */; /*!40101 SET character_set_client = utf8mb4 */; 
 CREATE TABLE `periodtaxes` (
 
   `period_id` varchar(255) NOT NULL,
   `tax_id` varchar(255) NOT NULL,
   `label` varchar(255),
   `taxRate` double NOT NULL,
   `base` double NOT NULL,
   `basePeriod` double NOT NULL,
   `baseFYear` double NOT NULL,
   `amount` double NOT NULL,
   `amountPeriod` double NOT NULL,
   `amountFYear` double NOT NULL,
   PRIMARY KEY (`period_id`,`tax_id`),
   KEY `IDX_59DAD7C9C914703E` (`period_id`),
   KEY `IDX_59DAD7C9B2A824D8` (`tax_id`),
   CONSTRAINT `FK_59DAD7C9B2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `taxes` (`id`),
   CONSTRAINT `FK_59DAD7C9C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; /*!40101 SET character_set_client = @saved_cs_client */; -- -- Dumping routines for database 'challans-20200122' -- /*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
 
 /*!40101 SET SQL_MODE=@OLD_SQL_MODE */; /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */; /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */; /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */; /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */; /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */; /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
 
 -- Dump completed on 2020-06-23 14:10:47
