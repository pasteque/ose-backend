update tickets t set 
t.finalTaxedPrice = ( select sum(p.total) from payments p where p.receipt = t.id) ,
t.taxedPrice = t.finalTaxedPrice ,
t.finalPrice = IFNULL(( select sum(x.base) from taxlines x where x.receipt = t.id) , 0),
t.price = t.finalPrice
where t.price is null ;

update closedcash set ticketCount = (SELECT COUNT(t.ticketId) from receipts join tickets t on t.id = receipts.id where money = closedcash.money )  
, custCount = (SELECT COUNT(t.ticketId) from receipts join tickets t on t.id = receipts.id where money = closedcash.money )  
where closedcash.ticketcount is null;
update closedcash set cs = ( SELECT SUM(p.total) from receipts join tickets t on t.id = receipts.id join payments p on p.receipt = receipts.id where money = closedcash.money )  
where closedcash.cs is null;