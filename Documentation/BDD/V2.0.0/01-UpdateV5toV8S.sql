ALTER TABLE `cashregisters` 
ADD COLUMN `nextSessionId` INT(11) NOT NULL DEFAULT '1' AFTER `NEXTTICKETID`;

ALTER TABLE `closedcash` 
ADD COLUMN  `continuous` tinyint(1) NOT NULL,
ADD COLUMN  `ticketCount` int(11) DEFAULT NULL,
ADD COLUMN  `custCount` int(11) DEFAULT NULL,
ADD COLUMN  `cs` double DEFAULT NULL,
ADD COLUMN  `csPeriod` double NOT NULL,
ADD COLUMN  `csFYear` double NOT NULL,
ADD COLUMN `csPerpetual` double NOT NULL;
 
ALTER TABLE `ticketlines`
ADD COLUMN  `unitPrice` double DEFAULT NULL,
ADD COLUMN  `taxedUnitPrice` double DEFAULT NULL,
ADD COLUMN  `taxedPrice` double NOT NULL,
ADD COLUMN  `taxRate` double NOT NULL,
ADD COLUMN  `finalPrice` double DEFAULT NULL,
ADD COLUMN  `finalTaxedPrice` double DEFAULT NULL;

ALTER TABLE `tickets`  
ADD COLUMN    `sequence` int(11) NOT NULL,
ADD COLUMN  `number` int(11) NOT NULL,
ADD COLUMN  `date` datetime NOT NULL,
ADD COLUMN    `price` double DEFAULT NULL,
ADD COLUMN    `taxedPrice` double DEFAULT NULL,
ADD COLUMN    `finalPrice` double NOT NULL,
ADD COLUMN    `finalTaxedPrice` double NOT NULL,
ADD COLUMN    `custBalance` double NOT NULL;
  
 CREATE TABLE `archives` (
 
   number INT NOT NULL,
   info LONGTEXT NOT NULL,
   content LONGBLOB NOT NULL,
   contentHash VARCHAR(255) NOT NULL,
   signature VARCHAR(255) NOT NULL,
   PRIMARY KEY(number)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 
 
 CREATE TABLE `archiverequests` (
 
   id INT AUTO_INCREMENT NOT NULL,
   startDate DATETIME NOT NULL,
   stopDate DATETIME NOT NULL,
   processing TINYINT(1) NOT NULL,
   PRIMARY KEY(id)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 
 
 CREATE TABLE `fiscaltickets` (
 
   `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `sequence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `number` int(11) NOT NULL,
   `date` datetime NOT NULL,
   `content` longtext COLLATE utf8_unicode_ci NOT NULL,
   `signature` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   PRIMARY KEY (`type`,`sequence`,`number`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci; 
 
 CREATE TABLE `periods` (
 
   `id` varchar(255) NOT NULL,
   `CASHREGISTER_ID` int(11) NOT NULL,
   `SEQUENCE` int(11) NOT NULL,
   `TYPE` varchar(255) NOT NULL,
   `DATESTART` datetime DEFAULT NULL,
   `DATEEND` datetime DEFAULT NULL,
   `continuous` tinyint(1) NOT NULL,
   `ticketCount` int(11) DEFAULT NULL,
   `custCount` int(11) DEFAULT NULL,
   `cs` double DEFAULT NULL,
   `csPeriod` double NOT NULL,
   `csFYear` double NOT NULL,
   `csPerpetual` double NOT NULL,
   `closed` tinyint(1) NOT NULL,
   PRIMARY KEY (`ID`),
   UNIQUE KEY `PERIODS_INX_SEQ` (`CASHREGISTER_ID`,`SEQUENCE`,`TYPE`),
   KEY `PERIODS_INX_1` (`DATESTART`),
   KEY `PERIODS_CRTDAT` (`CASHREGISTER_ID`,`TYPE`,`DATESTART`,`DATEEND`),
   KEY `PERIODS_CROPEN` (`CASHREGISTER_ID`,`closed`),
   CONSTRAINT `PERIODS_FK_CASHREGISTER` FOREIGN KEY (`CASHREGISTER_ID`) REFERENCES `cashregisters` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 
 CREATE TABLE `periodcats` (
 
   `period_id` varchar(255) NOT NULL,
   `categ_id` varchar(255) NOT NULL,
   `label` varchar(255) NOT NULL,
   `amount` double NOT NULL,
   PRIMARY KEY (`period_id`,`categ_id`),
   KEY `IDX_F32F3B52C914703E` (`period_id`),
   KEY `FK_PERIOD_CATEG_idx` (`categ_id`),
   CONSTRAINT `FK_F32F3B52C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`),
   CONSTRAINT `FK_PERIOD_CATEG` FOREIGN KEY (`categ_id`) REFERENCES `categories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
 
 
 CREATE TABLE `periodcattaxes` (
 
   `period_id` varchar(255) NOT NULL,
   `categ_id` varchar(255) NOT NULL,
   `tax_id` varchar(255)  NOT NULL,
   `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
   `base` double NOT NULL,
   `amount` double NOT NULL,
   PRIMARY KEY (`period_id`,`categ_id`,`tax_id`),
   KEY `IDX_E4084375C914703E` (`period_id`),
   KEY `IDX_E4084375B2A824D8` (`tax_id`),
   KEY `FK_PERIOD_CATEG_1_idx` (`categ_id`),
   CONSTRAINT `FK_E4084375B2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `taxes` (`id`),
   CONSTRAINT `FK_E4084375C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`),
   CONSTRAINT `FK_PERIOD_CATEG_1` FOREIGN KEY (`categ_id`) REFERENCES `categories` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ; 
 
 CREATE TABLE `periodcustbalances` (
 
   `period_id` varchar(255) NOT NULL,
   `customer_id` varchar(255) NOT NULL,
   `balance` double NOT NULL,
   PRIMARY KEY (`period_id`,`customer_id`),
   KEY `IDX_9D3CBD8BC914703E` (`period_id`),
   KEY `IDX_9D3CBD8B9395C3F3` (`customer_id`),
   CONSTRAINT `FK_9D3CBD8B9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
   CONSTRAINT `FK_9D3CBD8BC914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 
 CREATE TABLE `periodpayments` (
 
   `period_id` varchar(255) NOT NULL,
   `paymentmode_id` varchar(255) NOT NULL,
   `currency_id` int(11) NOT NULL,
   `amount` double NOT NULL,
   `currencyAmount` double NOT NULL,
   PRIMARY KEY (`period_id`,`paymentmode_id`,`currency_id`),
   KEY `IDX_342F815C914703E` (`period_id`),
   KEY `IDX_342F815ABEEF95F` (`paymentmode_id`),
   KEY `IDX_342F81538248176` (`currency_id`),
   CONSTRAINT `FK_342F81538248176` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`),
   CONSTRAINT `FK_342F815C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
 
 CREATE TABLE `periodtaxes` (
 
   `period_id` varchar(255) NOT NULL,
   `tax_id` varchar(255) NOT NULL,
   `label` varchar(255),
   `taxRate` double NOT NULL,
   `base` double NOT NULL,
   `basePeriod` double NOT NULL,
   `baseFYear` double NOT NULL,
   `amount` double NOT NULL,
   `amountPeriod` double NOT NULL,
   `amountFYear` double NOT NULL,
   PRIMARY KEY (`period_id`,`tax_id`),
   KEY `IDX_59DAD7C9C914703E` (`period_id`),
   KEY `IDX_59DAD7C9B2A824D8` (`tax_id`),
   CONSTRAINT `FK_59DAD7C9B2A824D8` FOREIGN KEY (`tax_id`) REFERENCES `taxes` (`id`),
   CONSTRAINT `FK_59DAD7C9C914703E` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
 
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;