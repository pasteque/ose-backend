update ticketlines tl INNER JOIN taxes tx ON tx.id=tl.taxid JOIN tickets t on t.id = tl.ticket
set tl.taxRate = tx.rate ,
tl.taxedUnitPrice = ROUND( tl.unitPrice * (1 + tx.rate) , 2)  ,
tl.price = tl.unitPrice * tl.units , 
tl.taxedPrice = ROUND(tl.units * tl.unitPrice * (1 + tx.rate) , 2) ,
tl.finalTaxedPrice = ROUND(tl.units * tl.unitPrice * (1 + tx.rate) * (1 -  tl.discountrate) * (1 - t.discountrate) , 2) ,
tl.finalPrice = tl.units * tl.unitPrice  * (1 -  tl.discountrate) * (1 - t.discountrate)
where tl.finalPrice is null ;