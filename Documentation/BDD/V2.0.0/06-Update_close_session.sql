update closedcash c set 
c.dateend = ( select max(r.datenew) from receipts r where r.MONEY = c.MONEY) 
where c.dateend is null 
and DATE(c.datestart) != DATE(now());